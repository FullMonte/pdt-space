/* #ifndef __MY_NLP_HPP__ */
/* #define __MY_NLP_HPP__ */
/*  */
/* #include "IpTNLP.hpp" */
/* #include <vector>  */
/* #include "read_mesh.h" */

/* using namespace Ipopt; */

/* class my_nlp: public TNLP */
/* { */
/* public: */
    /** default constructor */
/*     my_nlp(); */

    /** Regular Constructor **/
/*     my_nlp(unsigned int mesh_size, unsigned int num_sources,  */
/*            const vector<double> & d_max, const vector<double> & d_min, */
/*            const vector<double> & mu_a, const vector<double> & p_max, vector<double> delta, */
/*            const vector<SP_Point*> & mesh_points); */

/*     double compute_distance(Number x, Number y, Number z, SP_Point* p); */
    /** default destructor */
/*     virtual ~my_nlp(); */

/*     virtual bool get_nlp_info(Index& n, Index& m, Index& nnz_jac_g, */
/*                               Index& nnz_h_lag, IndexStyleEnum& index_style); */

    /** Method to return the bounds for my problem */
/*     virtual bool get_bounds_info(Index n, Number* x_l, Number* x_u, */
/*                                Index m, Number* g_l, Number* g_u); */

    /** Method to return the starting point for the algorithm */
/*     virtual bool get_starting_point(Index n, bool init_x, Number* x, */
/*                                   bool init_z, Number* z_L, Number* z_U, */
/*                                   Index m, bool init_lambda, */
/*                                   Number* lambda); */

    /** Method to return the objective value */
/*     virtual bool eval_f(Index n, const Number* x, bool new_x, Number& obj_value); */

    /** Method to return the gradient of the objective */
/*     virtual bool eval_grad_f(Index n, const Number* x, bool new_x, Number* grad_f); */

    /** Method to return the constraint residuals */
/*     virtual bool eval_g(Index n, const Number* x, bool new_x, Index m, Number* g); */

    /** Method to return:
    *   1) The structure of the jacobian (if "values" is NULL)
    *   2) The values of the jacobian (if "values" is not NULL)
    */
/*     virtual bool eval_jac_g(Index n, const Number* x, bool new_x, */
/*                           Index m, Index nele_jac, Index* iRow, Index *jCol, */
/*                           Number* values); */

    /** Method to return:
    *   1) The structure of the hessian of the lagrangian (if "values" is NULL)
    *   2) The values of the hessian of the lagrangian (if "values" is not NULL)
    */
/*     virtual bool eval_h(Index n, const Number* x, bool new_x, */
/*                       Number obj_factor, Index m, const Number* lambda, */
/*                       bool new_lambda, Index nele_hess, Index* iRow, */
/*                       Index* jCol, Number* values); */

    //@}

    /** @name Solution Methods */
    //@{
    /** This method is called when the algorithm is complete so the TNLP can store/write the solution */
/*     virtual void finalize_solution(SolverReturn status, */
/*                                  Index n, const Number* x, const Number* z_L, const Number* z_U, */
/*                                  Index m, const Number* g, const Number* lambda, */
/*                                  Number obj_value, */
/*                  const IpoptData* ip_data, */
/*                  IpoptCalculatedQuantities* ip_cq); */
    //@}

private:
    /**@name Methods to block default compiler methods.
    * The compiler automatically generates the following three methods.
    *  Since the default compiler implementation is generally not what
    *  you want (for all but the most simple classes), we usually 
    *  put the declarations of these methods in the private section
    *  and never implement them. This prevents the compiler from
    *  implementing an incorrect "default" behavior without us
    *  knowing. (See Scott Meyers book, "Effective C++")
    *  
    */
    //@{
    //  my_nlp();
/*     my_nlp(const my_nlp&); */
/*     my_nlp& operator=(const my_nlp&); */
    //@}


    // Private methods to use in the optimization
/*     double compute_phi_r(double r, double mu_a_i, double delta_i, double p_j); */
/*     double compute_p_norm_of(const vector<double> & v, int p); */
/*      */
/*     unsigned int _mesh_size; */
/*     unsigned int _num_sources; */
/*  */
    // Upper and Lower Bounds
/*     vector<SP_Point*> _mesh_positions; // This would specify the coordinates of the tetrahedra centers   */
/*     vector<double> _d_max;  */
/*     vector<double> _d_min; */
/*     vector<double> _p_max; */
/*     vector<double> _mu_a; */
/*      */
/*     vector<double> _source_power; // TODO: for now, the powers are randomly distributed. Later, they are going to be opt. variables */
/*  */
/*     vector<double> _delta; */
/* }; */

/* #endif */
