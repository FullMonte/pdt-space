/**
 * @author: Shuran Wang
 * @date: October 6, 2023
 *
 * \file light_source.h \brief Source Types Header File
 *
 * This file defines all the classes defining source types supported by PDT-SPACE.
 */

#ifndef _LIGHT_SOURCE_H_
#define _LIGHT_SOURCE_H_

// #include "pdt_plan.h"
#include "run_fullmonte.h"
#include "read_mesh.h"

class Src_Abstract {
public:
    virtual ~Src_Abstract() {}

    enum Types {
        Line,
        Point,
        CutEnd
    };
    Types get_type () const { return _type; }
    virtual Source::Abstract* get_fm_source() = 0;

    // outputs for all derived classes
    friend std::ostream& operator<<(std::ostream& os, const Src_Abstract * src);
    // prints a group of srcs to xml format
    friend std::ostream& print_to_xml(std::ostream& os, const vector<Src_Abstract*> &srcs);
protected:
    Types _type;
};

class Src_Line : public SP_Line, public Src_Abstract {
public:
    Src_Line() { _type = Line; } // default constructor
    Src_Line(const SP_Point & pt1, const SP_Point & pt2) : SP_Line (pt1, pt2) { _type = Line; }
    Src_Line(const SP_Line & l) : SP_Line {l} { _type = Line; }

    double get_src_length() { return end0.get_distance_from(end1); }

    Source::Line* get_fm_source();
private:
};

class Src_Point : public SP_Point, public Src_Abstract {
public:
    Src_Point() { _type = Point; } // default constructor
    Src_Point(int id, float xcoord, float ycoord, float zcoord, mesh* parent_mesh) : 
        SP_Point {id, xcoord, ycoord, zcoord, parent_mesh} { _type = Point; }
    Src_Point(float xcoord, float ycoord, float zcoord) :
        SP_Point {xcoord, ycoord, zcoord} { _type = Point; }
    Src_Point(const SP_Point & pt) : SP_Point {pt} { _type = Point; }

    Source::Point* get_fm_source();
};

class Src_CutEnd : public Src_Abstract {
public:
    Src_CutEnd() { _type = CutEnd; } // default constructor
    Src_CutEnd(float r, float na, float xcoord, float ycoord, float zcoord, float xdir, float ydir, float zdir) : 
            _r(r), _na(na) {
        _type = CutEnd;
        _endpoint = SP_Point (xcoord, ycoord, zcoord);
        _dir = SP_Point (xdir, ydir, zdir);
    }
    Src_CutEnd(float r, float na, SP_Point end, SP_Point dir) : 
         _endpoint(end), _dir(dir), _r(r), _na(na) { _type = CutEnd; }

    ~Src_CutEnd() {} // default destructor

    Source::Fiber* get_fm_source();

    // getters
    const SP_Point& get_endpoint() const { return _endpoint; }
    const SP_Point& get_dir() const { return _dir; }
    float get_radius() const { return _r; }
    float get_na() const { return _na; }
private:
    SP_Point _endpoint;
    SP_Point _dir;
    float _r;
    float _na;
};

#endif // _LIGHT_SOURCE_H_