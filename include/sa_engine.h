/**
 * @author: Abed Yassine
 * @date: September 30th, 2019
 *
 * \file sa_engine.h
 * @brief: class that implements a simulated annealing engine for the source placement
 *
 *
 */

#ifndef _SA_ENGINE_H_
#define _SA_ENGINE_H_

//#define SA_DBG
//#define FORCE_DBG

#define SA_LOG

//#define PRINT_SOURCES
//#define LEARNING_BASED_MOVE
//#define USE_INTERPOLATION

#define ADJUST_SPACING	// adjust spacing between sources during generation of initial solution

#include "read_mesh.h"
#include "fixed_sources_convex_program.h"
#include "run_fullmonte.h"
#include "pdt_plan.h"
#include "util.h"
#include "file_parser.h"
#include "oriented_bounding_box.h"

#ifdef USE_INTERPOLATION
#include "interpolation_engine.h"
#endif

#include <vector>
#include <string>
#include <iostream>
#include <utility>
#include <queue>

#define NUM_TOP_PLACEMENTS 5

typedef pair<double, std::vector<SP_Line> > p_t;
struct pairComp {
	constexpr bool operator()(const p_t & p1, const p_t & p2) {
		return p1.first < p2.first;
	}
};
typedef std::priority_queue<p_t, std::vector<p_t>, pairComp> pqueue_t;

class sa_engine;

class sa_engine 
{

protected: 
enum class MoveType {
	RANDOM = 0, // random source location and direction
	DISPLACE, // displace a source randomly in a perpendicular direction 
	FORCE_MULTISOURCE, // force based move that pushes all sources with magnitude inversely proportional to the distance squared
					// and effective penetration depth
	FORCE_CLOSEST, // force based move that pushes the closest source with a random magnitude. 
	RESET_BEST,		// Reset to best solution found so far
	LENGTH, 	// Change one of the source lengths randomly 
	REMOVE_SOURCE, // remove one of the sources
	RELOCATE, // relocate all zero-powered sources to new positions
	NONE     // None of the above
};

// Creating a hash function for the enum class MoveType
struct EnumClassHash
{
	template <typename T>
	std::size_t operator()(T t) const 
	{
		return static_cast<std::size_t>(t); 
	}
};

public:
    //! Regular constructor
    sa_engine(Parser* fparser,
			WrapperFullMonteSW * fm_engine,
			mesh* data,
            const std::vector<double>& min_dose,
            const std::vector<double>& max_dose,
            const std::vector<unsigned>& tumor_indices,
			double tumor_volume,
			bool override_file_path,
			bool early_termination,
			pdt_plan* parent_plan ); 

    //! Destructor
    ~sa_engine() 
    {
		if (_power_allocator) {
			delete _power_allocator; 
		}
    }
    
    //! Method that implements the overall SA algorithm
    void run_sa_engine(int inj_index); 

    //! Method to get the final solution
    std::vector<Src_Abstract*> get_placement_solution() const; 

	//! Helper method: returns if a source is within the current injection point constraint
	bool is_valid_injection(const SP_Line & src);

	//! Helper method: returns if current state is within the current injection point constraint
	bool is_valid_injection();

	//! Helper method: returns the maximum possible distance to displace given the injection point constraint
	void get_rand_dist(const SP_Line & src, const SP_Point & dir, double & rand_dist) const;

	//! Helper method: randomly rotates the source around end0
	void rand_rotate_source(SP_Line & src, double length);

	//! Helper method: force injection constraint to true and get injection constraints
	void enable_injection_constraint();

	//! Helper method: set the current injection point constraint
	void set_injection_point_constraint(unsigned ind);

	//! Helper method: reads SA parameters from _fparser
	void read_parameters();

private: 
    // Helper methods
    //! Helper method: generates an initial solution 
    void create_initial_solution(); 

	//! Helper method to place a source from reference line, returns false if no solution
	bool place_source_from_line(SP_Line &cur_line);

	//! Helper method to constrain source length
	//! source [inout]: source to be constrained, both ends will be shortened by the same amount
	//! length [in]: maximum length allowed for the source
	void constrain_source_length(SP_Line & source, double length);

	//! Helper method: generates a random initial solution aligning to the longest tumor axis
	void generate_random_initial_solution();

	//! Helper method: generates a set of source placement satisfying current injection point constraint (called by create_initial_solution())
	void generate_valid_initial_solution();

    //! Helper method: an adaptive method to compute an initial temperature no matter what the number of sources is
    double get_initial_temperature(); 

	//! Helper method: generates a new move to replace the zero-powered sources
	void generate_zero_powered_source_move();

    //! Helper method: computes the cost of a state of the solution
    double compute_cost(const std::vector<SP_Line>& state, bool write_to_file, std::string) const; 

	//! Helper method: computes the cost based on v100s 
	double compute_cost_v100(const std::vector<SP_Line>& state);
#ifdef RECOMPUTE
	double compute_cost_v100_no_fm();
#endif

    //! Helper method: generates the next move
	//! only_random [in]: If true it only generates random move (random location and rotation)
    //! returs the index of the source that was changed.
    unsigned generate_move(bool only_random); 

    //! Helper method: accepts the next solution with a certain probability
    bool accept_move(double cost_diff);

    //! Helper method: computes whether next solution can be accepted without updating states
    bool is_move_accepted(double cost_diff);

    //! Helper method: computes the acceptance rate
    double get_acceptance_rate(); 

    //! Helper method: updates the temperature
    void update_temperature(); 

    //! Helper method: checks if the new move is legal
	//! force_move_only [in]: if true, then all sources have to be checked if legal, since all of them were moved
    //! change_idx [in]: index of the source that was changed in the move generator
    bool is_legal(bool run_force_move_only, unsigned change_idx); 

	//! Helper method: get tetrahedron id enclosing a point
	unsigned get_tetra_enclosing_point(SP_Point & p);

    //! Helper method: checks if a point is inside the tumor
    bool is_pt_in_tumor(SP_Point & p);

    //! Helper method: checks if a line is inside the tumor 
    bool is_line_in_tumor(SP_Line & l); 

	//! Helper method: checks if the line is inside the mesh
	bool is_in_mesh(SP_Line & l); 

	//! Helper method: checks if the point is inside the mesh
	bool is_in_mesh(SP_Point & p);

    // The following helper methods are to find if two lines intersect TODO: may need to move them to utils
    //! Helper method: checks if two lines intersect
    bool do_intersect(const SP_Line & line1, const SP_Line & line2);

    //! Helper method: get the orientation of 3 points 
    int get_orientation(const SP_Point & p1, const SP_Point & p2, const SP_Point & p3);

    //! Helper method: checks if a point lies on a line (assuming they are collinear)
    bool is_on_line(const SP_Line& line, const SP_Point & pt); 

    //! Helper method: checks if 4 pts are coplanar
    bool is_coplanar(const SP_Point & pt1, const SP_Point & p2, const SP_Point & p3, 
                        const SP_Point & pt4); 

    //! Helper method: returns a random perpendicular unit vector to the given source
    void get_rand_perpendicular(const SP_Line & src, double & dir_x, double & dir_y, double & dir_z); 
   
    //! Helper method: returns the length of the source 
    double get_source_length(const SP_Line & line); 

	//! Helper method: returns the average distance between sources. The distance between 2 lines is computed as the smaller 
	//! perperndicular distance of one of the endpoints on source to the other line
	double get_average_distance_sources(const std::vector<SP_Line>& sources) const ; 

#ifdef USE_INTERPOLATION
    //! Helper method: builds the grid cache of point sources
    void init_grid_sources(); 
#endif

#ifdef LEARNING_BASED_MOVE
	// The following methods are related to a learning based move generator
	// Basically the mesh is initialized with a grid of equal-sized voxels with unit weights
	// Once we try couple of moves, we can then modify the weights based on hotspots in the mesh
	// (Locations that generated better cost previously), and modify the weights accordingly
	//! Helper method: Initialize a grid of weights 
	void create_weight_grid(double voxel_size); // sorted based on x ==> y ==> z
	//! Helper method: update the weight of the voxel containing the current source
	void update_weight(unsigned change_idx, double old_cost, double new_cost); 
	//! Helper method: generate a new move for the source with change_idx that is based on history
	void generate_learned_move(unsigned change_idx, double length); 
#endif

	// The following methods are related to a force-based move generator
	// Basically we find the locations of the underdosed regions in the current config, 
	// and the locations of the overdosed regions so that the next move will pull or push 
	// The sources accordingly. This approach basically approximates the gradient 
	// Every region will control the closest source to it
	// The way we do it is that every OAR tetra will push the sources in the direction perpendicular to it
	// if it is overdosed, and Every tumor tetra will pull the sources in its direction if it is underdosed
	//! Helper method: generates a new move for the sources based on the force-based approach 
	void generate_force_move(); 

	//! This method generates a random length to one of the sources
	void generate_rand_length_move(unsigned change_idx); 

	//! Helper method: returns the next move type based on probabilites
	MoveType get_next_move_type(); 

	//! Helper method: updates the probabilities of the move types 
	void update_move_types_probabilities(); 

	// The following functions are related to adjusting the number of sources
	// used in the solution. 
	//! Helper method: generates a new move by removing the least powered source
	void generate_removal_move();

	// Parent plan 
	pdt_plan*				_parent_plan=nullptr;
	pdt_plan*				_power_allocator; 
	mesh*					_data; // pointer to the data
	vector<double>			_guardbanded_thresholds;
	double					_curr_tumor_weight; // used to save the last tumor weight that resulted in 98% 
	double					_orig_tumor_weight; // used to save the original tumor weight of the power allocator
	vector<unsigned>		_oar_and_tumor_indices; // stores only the indices of the tetra that are important
	vector<double>			_oar_and_tumor_volumes; // stores only the volumes of the tetra that are important
	double					_sum_oar_tumor_volumes;
	double                  _num_packets;

	// Precompute OBB
	OBB                     _obb_engine;

    // SA Engine parameters
    double                  _init_temp; // initial temperature
    double                  _current_temp; // current temperature
    double                  _old_max_move; // used in predicting a new move range (initialized to 1.25*max_tumor_radius)
    double                  _curr_max_move; // used to set a max limit on the range of which we can move the source
    double                  _max_tumor_radius; 
	double					_curr_move_range; // used to report the move range if accepted
    double                  _beta; // used in updating temperature
    double                  _moves_per_temp; // used in assigning number of moves per temperature
	bool					_ignore_move; 
	bool					_chose_force_based; // indicates if the last move was a force based one
	bool					_generate_learning_only; // indicates whether to only generate learning_based moves or not
	double					_tumor_volume; 

	bool					_early_termination;
	bool                    _file_path_override;

	unordered_map<MoveType, double, EnumClassHash> 
							_move_type_probability; // This stores the probability of choosing a certain move type
	double					_lambda_smooth; // a parameter between zero and 1 to smooth the new probability computation
	MoveType				_curr_move_type; // stores the current move type
	unordered_map<MoveType, double, EnumClassHash>
							_move_type_effectiveness; // measures the effectiveness of each move based on the cost difference and 
													// acceptance probability. Used in updating the probabilities. 
	bool					_terminate_based_on_effectiveness; // Used to terminate the annealer of all move types have effectiveness zero

	bool                    _single_change=false; // whether the current move is a single move
	bool                    _change_idx; // index of the source moved in a single move

	// std::string				_init_placement_file; // filename to read initial placement from
	Parser*                 _fparser = NULL;
	std::string 			_sa_placement_file; // Filename to store the source configurations as the annealer progresses 
	bool                    _print_sources;

	bool					_print_top_placements;
	std::string				_final_sa_placement_file; // Filename to store the top 5 placements, empty if not enabled
	pqueue_t 				_top_placements; // container to store the top 5 placements, top is placement with the maximum cost

    std::vector<SP_Line>    _current_state; // current placement
	std::vector<double>		_current_powers; // the power allocation of the current placement
	std::vector<double>		_current_fluence; // stores the fluence values due to the current placement and power allocation
    std::vector<SP_Line>    _next_state;  // next placement
    std::vector<SP_Line>    _final_solution; // final placement solution after algorithm is done
    std::vector<double>     _source_lengths; // stores the current length of all sources
    std::vector<double>     _init_source_lengths; // stores the initial source lengths of all sources
	std::vector<SP_Line> 	_best_placement_so_far; // stores the best result of the placement found so far. In case the final result is worse
													// we fall back to this 
	double					_best_cost_so_far; // Stores the cost of the best placement found so far. 

	// constraint parameters
	bool                    _constrain_injection_point;
	Parser_injection_points _injection_points;
	// PlacementMode           _placement_mode;
	SP_Point                _current_inj_normal;
	SP_Point                _current_inj_center;
	double                  _current_inj_diameter;
	int                     _inj_region_id;
	unsigned                _max_num_sources;

	// constraint parameters on generated initial placement
	double                  _max_src_spacing;
	double                  _min_src_spacing;
	double                  _dist_to_boundary;

    // SA Engine statistics
	unsigned                _num_rejected_inj_point; // number of rejected moves because of invalid injection point
    unsigned                _num_rejected_tumor; // number of rejected moves because of being outside the tumor
    unsigned                _num_rejected_overlaps; // number of rejected moves because of source overlaps
    unsigned                _num_rejected_removals; // number of rejected moves because of source overlaps

    unsigned                _num_moves; // total number of moves 
    unsigned                _num_legal_moves; // number of moves that passed legality check 
    unsigned                _num_accepted_moves; // number of legal moves accepted
    unsigned                _num_accepted_climbs; // number of hill climbs attempts accepted
	unsigned 				_num_accepted_force_based; // number of accepted force_based moves
	unsigned 				_num_reset_to_best_moves;// number of moves taken that reset to best solution 

	unsigned 				_num_unaccepted_since_last_acceptance; // used in terminating the engine

	unsigned 				_max_unaccepted_since_last_acceptance = 200;
	unsigned				_max_num_moves = 800;

    // interface to run fullmonte
    WrapperFullMonteSW*     _fullmonte_engine; 

    // related to optimizer 
    std::vector<double>     _min_dose; 
    std::vector<double>     _max_dose; 

    // related to tumor location 
    std::vector<unsigned>   _tumor_indices; // vector that stores all indices of tumor tetra

    // Related to the mesh
    unsigned                _num_tetra;

#ifdef USE_INTERPOLATION
    // Related to interpolation
    SP_IE::source_cache*    _grid_sources; 
    double                  _delta_x; 
    std::vector<double>     _grid_min; // lower bound on the points in the grid
#endif

#ifdef LEARNING_BASED_MOVE
	// Related to the learning based move 
	std::vector<double>     _history_weights; 
	std::vector<std::pair<unsigned, double>>
						    _max_history;
	double					_mesh_x_min; 
	double					_mesh_x_max; 
	double					_mesh_y_min; 
	double					_mesh_y_max; 
	double					_mesh_z_min; 
	double					_mesh_z_max; 
	double					_grid_voxel_dim;
	unsigned 				_grid_x_size;
	unsigned 				_grid_y_size; 
	unsigned 				_grid_z_size; 
	unsigned				_num_max_regions; 
#endif
};

#endif // _SA_ENGINE_H_
