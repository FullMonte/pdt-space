/**
 * @author: Shuran Wang
 * @date: December 13, 2021
 *
 * \file file_parser.h \brief External file format wrapper Header File
 *
 * This file defines wrappers to external file format (XML) parsers
 */

#ifndef FILE_PARSER_H_
#define FILE_PARSER_H_

#include <vector>
#include <unordered_set>

#include <boost/property_tree/xml_parser.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/foreach.hpp>
#include <boost/lexical_cast.hpp>

#include "light_source.h"

using namespace std;

// struct Parser_source {
//     string type;
//     vector<SP_Point> endpoint;
// };

class Src_Abstract;

struct Parser_sources {
    int max_sources;
    Src_Abstract::Types source_type;
    bool unified_type = true; 
    vector<Src_Abstract*> sources_init;
    vector<double> lengths;
    bool tailored = false;
};

struct Parser_injection_point {
    int id;
    float diameter;
    SP_Point normal;
    SP_Point center;
};

struct Parser_injection_points {
    int region_id;
    vector<Parser_injection_point> injection_points;
};

struct Parser_material {
    int id;
    double mu_a, mu_s, g, n;
    double d_min, d_max;
    double guardband;
    bool is_tumor;
    string name;
};

/**
 * Enum for probe placement modes.
 *   NO_CONSTRAINT - no constraint.
 *   PARALLEL - only allow parallel probe placement.
 *   POINT - all probes must pass through a single injection point.
 */
// enum PlacementMode {
//     PM_NONE,
//     PM_PARALLEL,
//     PM_POINT
// };

class Parser {
public:
    // Default constructor
    Parser();

    // Regular constructor
    Parser(string filename);

    // Destructor
    ~Parser();

    // Parse XML file
    bool read_from_xml(string filename);

    // Write init placement data to csv file (not applicable)
    // void write_to_init_placement_csv(string filename);

    // Getter functions for injection points
    Parser_injection_points get_injection_points();
    void get_injection_point_options(bool &constrain, /*PlacementMode &mode,*/ unsigned &max_num);
    void get_injection_point_options(bool &constrain/*, PlacementMode &mode*/);
    
    // get number of valid injection points
    unsigned get_num_injection_points();

    // Getter function for sources
    void get_sources(vector<SP_Line> &sources, vector<double> &lengths); 
    void get_sources(vector<Src_Abstract*> &sources); 
    string get_source_type();

    // Getter function for materials
    const vector<Parser_material>& get_materials(bool& matched) { matched = _matched_boundary; return _materials; }
    const vector<Parser_material>& get_materials() { return _materials; }
    // set : _tumor_region_number, _d_min_tissues, _d_max_tissues, _guardband_tissues, _tissue_names_ids, opt_properties
    void get_tissue_properties (vector<double>& dmin, vector<double>& dmax, vector<double>& guard, 
        unordered_map<unsigned, string>& names);
    void get_tumor_regions (unordered_set<unsigned>& tumor_regions);

    // Getter for whether line sources are tailored profile
    bool get_source_tailored() { return _sources.tailored; }

    unordered_set <unsigned> get_critical_regions() { return _critical_regions; }

    void get_init_src_params (double &min_dist, double &max_dist, double &bound_dist) {
        min_dist = _min_src_dist;
        max_dist = _max_src_dist;
        bound_dist = _dist_to_bound;
    }

    unordered_map <string, string>& get_fm_params();
    unordered_map <string, string>& get_params();
    unordered_map <string, string>& get_sa_params();

    bool get_run_sa() { return _run_sa; }

private:
    // helper functions to read sources
    void read_line_source  (const boost::property_tree::ptree& token);
    void read_point_source (const boost::property_tree::ptree& token);
    void read_cutend_source(const boost::property_tree::ptree& token);

    // helper function to read fullmonte options
    void read_fm_params(boost::property_tree::ptree token);

    // probe options
    Parser_sources _sources;

    // materials
    vector <Parser_material> _materials;
    bool _matched_boundary = false;

    // FM options
    unordered_map <string, string> _fm_params_map;

    // PDT-SPACE options
    unordered_map <string, string> _params_map;

    // simulated annealing options
    unordered_map <string, string> _sa_params_map;
    bool                           _run_sa;

    // other placement options
    bool _constrain_injection_point;
    Parser_injection_points _injection_points;
    // PlacementMode _placement_mode;
    unordered_set <unsigned> _critical_regions;

    double _min_src_dist = 10.0;
    double _max_src_dist = 15.0;
    double _dist_to_bound = 8.0;
};

#endif // FILE_PARSER_H_