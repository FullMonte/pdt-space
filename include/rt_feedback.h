/**
 * \author: Abed Yassine
 * \date: July 20th, 2020
 * \brief: wrapper class for realtime feedback and online monitoring. 
 *         Responsible for emulating light dosimetry and detection, spectroscopy for mu_eff and recovering absorption and scattering
 *         coefficients
 */

#ifndef _RT_FEEDBACK_H
#define _RT_FEEDBACK_H

/* pdt-space includes */
#include "read_mesh.h"
#include "util.h"
#include "light_source.h"

/* Other includes */
#include <iostream>
#include <string>
#include <vector>
#include <random>
#include <unordered_map>
#include <map>

/* Boost include for hash functions */
/* #include <boost/functional/hash.hpp> */

// Forward declarations
class WrapperFullMonteSW;
class MaterialSet;
class Material;
class Src_Abstract;


// Defining random variables 
/* #define NORMAL */
/* #define LOGNORMAL */
#define MULTIMODAL 

#ifdef NORMAL
typedef std::normal_distribution<float> RNG;
#endif

#ifdef LOGNORMAL
typedef std::lognormal_distribution<float> RNG;
#endif

#ifdef MULTIMODAL
typedef Util::multimodal_distribution<float> RNG;
#endif


class RtFeedback
{
public:
    //! Default constructor: use only for testing purposes 
    RtFeedback() {}

    //! Regular constructor
    RtFeedback(WrapperFullMonteSW*, const std::vector<Src_Abstract*>& curr_placement, 
            long long unsigned packets, unsigned tumor_id, double tumor_volume, unsigned rand_seed, 
            float detector_radius, float detector_na, unsigned num_lut_sims = 100); 

    //! Destructor 
    ~RtFeedback() {};

    // Methods:
    
    void emulate_dosimetry(const std::vector<Src_Abstract*>& sources, const std::vector<float>& source_powers, 
                    std::vector<std::vector<float>>& detected_weights);
   
    void recover_tumor_optical_properties(const std::vector<Src_Abstract*>& sources, 
                    const std::vector<float>& source_powers, 
                    float & new_absorption_coeff, float & new_scattering_coeff);
    std::vector<double> recover_mu_effs(const std::vector<Src_Abstract*> & sources, 
                    const std::vector<std::vector<float>>& detected_weights); 
    std::vector<double> recover_mu_effs_lut(const std::vector<Src_Abstract*> & sources, 
                    const std::vector<std::vector<float>>& detected_weights); 
    
    void build_lut(const std::vector<Src_Abstract*>& sources, const std::vector<float>& source_powers); 
    void build_lut_from_file(string filename, size_t num_sources); 
     
    void prepare_training_data(const std::vector<Src_Abstract*>& sources);  
protected: 
    // Helper methods:
    void get_highest_detected_weights(const std::vector<std::vector<float>> & detected_weights, 
                                       std::vector<std::vector<float>> & highest_detected_weights, 
                                       unsigned num_highest);
    
    // Generate new optical properties (for the tumor) 
    MaterialSet* generate_random_op(); 
    
    // Generate new optical properties (for all tissues) 
    MaterialSet* generate_random_op_all(); 

    /**
     * This helper method returns the feature vector from the given simulations
     */
     vector<double> get_features_vector(const std::vector<std::vector<float>> & detected_weights, 
                                const std::vector<double>& recovered_mu_effs,
                                double new_absorption_coeff, double new_scattering_coeff);

    
private:

    WrapperFullMonteSW*    _fullmonte_wrapper; 
    

    long long unsigned    _num_packets; 

    unsigned              _tumor_material_id; 
    double                _tumor_volume;

    float                 _detector_radius;
    float                 _detector_na;

    std::vector<Src_Abstract*>   _curr_placement;


    Material*             _original_tumor_properties; // stores the original properties of the tumor
    
    // A normal random generator for absorption and scattering coefficients coefficient
    std::mt19937                    _mt_random_generator;
    RNG _scattering_normal_dist;
    RNG _absorption_normal_dist;

    /**  
     * This structs wraps a vector of floats that represent the detected weights by detectors in FullMonte
     * in order to be able to hash them as a key for a look-up table of effective attenutation coefficient
     */
    struct key_detected_weights {
        std::vector<float> detected_weights;
        float epsilon = 0.0001f;
        
        key_detected_weights(const std::vector<float> & dw) {
            for (auto d : dw) {
                detected_weights.push_back(d); 
            }
        }
        
        bool operator==(const key_detected_weights & other) const {
            if (detected_weights.size() != other.detected_weights.size()) {
                return false;
            }

            for (unsigned i = 0; i < detected_weights.size(); i++) {
                if (std::fabs(detected_weights[i] - other.detected_weights[i]) > epsilon) {
                    return false;
                }
            }
            return true;
        }

        bool operator<(const key_detected_weights & other) const {
            if (detected_weights.size() < other.detected_weights.size()) {
                return true;
            }
            else if (detected_weights.size() > other.detected_weights.size()) {
                return false;
            }
            else {
                for (unsigned i = 0; i < detected_weights.size(); i++) {
                    if (std::fabs(detected_weights[i] - other.detected_weights[i]) > epsilon) {
                        if (detected_weights[i] < other.detected_weights[i]) {
                            return true;
                        } else {
                            return false;
                        }
                    }
                }
                return true;
            }
        }

        friend std::size_t hash_value(const key_detected_weights & key) {
            std::size_t seed = 0; 
            for (unsigned i = 0; i < key.detected_weights.size(); i++) {
                boost::hash_combine(seed, key.detected_weights[i]);
            }
            return seed;
        }
    };

    /**
     * This method returns an object of the random distribution based on its type
     */
     inline RNG get_distribution(float param_1, float param_2) {
#ifdef NORMAL
        return RNG(param_1, param_2);
#endif

#ifdef LOGNORMAL
        return RNG(logf(powf(param_1,2)/sqrtf(powf(param_1,2) + powf(param_2,2))), 
                    sqrtf(logf(powf(param_2,2)/powf(param_1,2) + 1))); 
#endif

#ifdef MULTIMODAL 
        return RNG(param_1, param_2); 
#endif 
     }

    /** 
     * This is a look-up table for effective attenuation coefficients. It is built by running _num_lut_sims simulations in 
     * Fullmonte using build_lut() method using _num_lut_sims randomly generated mu_effs. The key for this table is of type
     * key_detected_weights and the value is of type double which is the attenuation coefficient used to run this simulation 
     * The size of the vector is equal to the number of sources
     */ 
/*     std::unordered_map<key_detected_weights, double, boost::hash<key_detected_weights> > _mu_effs_lut; */
    std::vector<std::map<key_detected_weights, double>>                                    _mu_effs_lut;
    unsigned                                                                               _num_lut_sims;
};

#endif
