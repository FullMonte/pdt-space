/**
 * @author: Abdul-Amir Yassine
 * @date: January 11th, 2017
 *
 * \file read_mesh.h \brief Mesh Reader Header File
 *
 * This file defines all the classes related to the tetrahedral mesh
 */

#ifndef _READ_MESH_H_
#define _READ_MESH_H_

#include "placer_defs.h"

#include <vector>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <cstdlib>
#include <sstream>
#include <map>
#include <string>
#include <algorithm>
#include <limits>
#include <assert.h>

#include "vtk_include.h"

using namespace std;

#define _INFINITY_ numeric_limits<float>::max()
#define _NEG_INFINITY_ numeric_limits<float>::lowest()

class mesh;
class SP_Point; // source_placement_point
class SP_Line; 
class tetrahedron;
class material_opt;
class Parser;

class mesh
{
public:
    // Default Constructor
    mesh();

    // Regular Constructor
    mesh(string mesh_file, Parser* fparser, bool read_from_vtk);

    // Destructor
    ~mesh();

    // Getters
    vector<SP_Point*> &         get_list_of_points();
    vector<tetrahedron*> &      get_list_of_tetrahedra();
    vector<material_opt*> &     get_list_of_materials();

    unsigned int                get_number_of_points();
    unsigned int                get_mesh_size();
    unsigned int                get_number_of_materials();

    double                      get_index_of_refraction();

    // Setters
    void                        add_point(SP_Point *p);
    void                        add_tetrahedron(tetrahedron *t);
    void                        add_material(material_opt * m);
    
    void                        set_number_of_points(unsigned int np);
    void                        set_mesh_size(unsigned int ms);
    void                        set_number_of_materials(unsigned int nm);

    // Print a vector to a matlab file 
    void print_vector_matlab(FILE* matlab, 
                             vector<double> &v, 
                             string v_name);
    void print_vector_matlab(FILE* matlab, 
                                     vector<int> &v, 
                                     string v_name);
    
    // compute the volume of a teterahedron
    double compute_tet_volume(tetrahedron* t);

protected:
    // Helper methods
    void read_opt_props(Parser* fparser);
    void read_mesh(string mesh_file);
    void read_vtk(string vtk_file);

    // Members
    vector<SP_Point*>        _list_of_points;
    vector<tetrahedron*>     _list_of_tetrahedra;
    vector<material_opt*>    _list_of_materials;
    
    unsigned int             _number_of_points; // specifies the number of points
    unsigned int             _number_of_tetrahedra; // specifies the mesh size
    unsigned int             _number_of_materials; // specifies the number of different material 
                                                   // optical properties

    double                   _index_of_refraction; 
};

class SP_Point 
{
public:
    // id of enclosing tetrahedron (to save runtime)
    int _tetra;

    // Default Constructor
    SP_Point();

    // Regular Constructor
    SP_Point(int id, float xcoord, float ycoord, float zcoord, mesh* parent_mesh);

    // Regular Constructor
    SP_Point(float xcoord, float ycoord, float zcoord); 

    // Destructor
    ~SP_Point();

    // Getters
    mesh*    get_parent_mesh();

    //! Method to compute the distance from another point 
    double   get_distance_from(const SP_Point & p2) const; 



    float    get_xcoord() const;
    float    get_ycoord() const;
    float    get_zcoord() const;
    int      get_id() const;
    
    // Setters
    void     set_xcoord(float xcoord);
    void     set_ycoord(float ycoord);
    void     set_zcoord(float zcoord);
    void     set_id(int id);

    friend std::ostream& operator<<(std::ostream& os, const SP_Point & pt); 
	
	//! Method to overload addition operator
	friend SP_Point operator+(const SP_Point & p1, const SP_Point& p2); 

	//! Method to overload subtraction operator
	friend SP_Point operator-(const SP_Point & p1, const SP_Point& p2);

	//! Method to perform dot product
	friend double sp_dot(const SP_Point & vec1, const SP_Point & vec2); 

    //! Method to return the l2 norm of a vector 
    friend double sp_norm(const SP_Point & vec); 
	
	//! Method that overloads the divide operator to divide by a constant 
	friend SP_Point operator/(const SP_Point& vec1, double c);

	//! Methid that overloads the multiplication operator to multiply a vector/point by a constant
	friend SP_Point operator*(double c, SP_Point& vec); 

    //! Method to perform cross product 
    friend SP_Point sp_cross(const SP_Point & vec1, const SP_Point & vec2);

    //! Method to normalize a vector represented by SP_Point 
    friend SP_Point sp_normalize(const SP_Point & vec); 

    //! Method to compute the determinant of 3 vectors 
    friend double sp_determinant(const SP_Point & v1, const SP_Point  & v2, const SP_Point & v3); 
protected:
    mesh*    _parent_mesh;
    
    float    _xcoord;
    float    _ycoord;
    float    _zcoord;

    int      _id; // If this is used then the point is part of _parent_mesh
};

class SP_Line 
{
public:
    SP_Point end0; 
    SP_Point end1;

    double length; 
    SP_Point dir; 

    SP_Line() {}

    SP_Line (const SP_Point & pt1, const SP_Point & pt2) {
        end0 = pt1;
        end1 = pt2;

        length = end0.get_distance_from(end1); 

        if (length > DOUBLE_ACCURACY) { 
            dir = sp_normalize(end1 - end0);
        } else {
            dir = SP_Point(0.0f, 0.0f, 0.0f); 
        }

    }

    void update_length_and_dir() {
        length = end0.get_distance_from(end1); 

        if (length > DOUBLE_ACCURACY) { 
            dir = sp_normalize(end1 - end0);
        } else {
            dir = SP_Point(0.0f, 0.0f, 0.0f); 
        }
    }

    friend std::ostream& operator<<(std::ostream& os, const SP_Line & line);
    friend std::ostream& print_to_xml(std::ostream& os, const vector<SP_Line> &lines); // prints a group of lines to xml format

    //! Overloads the > operator
    friend bool operator>(const SP_Line & lhs, const SP_Line & rhs) { return lhs.length > rhs.length; }

	double get_distance_pt_line(const SP_Point & pt) const; // find the perpendicular distance from pt to this line
    double get_distance_from(const SP_Line & line2) const; // finds the distance between two lines 

    void rotate_randomly(); // rotate the source randomly in space

    SP_Point get_rand_perpendicular_vector(); // returns a random vector perpendicular to end1-end0
    
    // stores the intersection between a plane to this line, returns false if no such point exists
    bool get_intersection_with_plane(const SP_Point & n, const SP_Point & p, SP_Point & res) const;
}; // struct that defines a line

class tetrahedron
{
public:
    // Default Constructor
    tetrahedron();

    // Regular Constructor
    tetrahedron(int id, int p1_id, int p2_id, int p3_id, int p4_id,
                    int material_id, mesh* parent_mesh);

    // Destructor
    ~tetrahedron();

    // Getters
    mesh*       get_parent_mesh();
    
    int         get_p1_id();
    int         get_p2_id();
    int         get_p3_id();
    int         get_p4_id();
    int         get_material_id();

    int         get_id();

    // Setters
    void        set_p1_id(int id);
    void        set_p2_id(int id);
    void        set_p3_id(int id);
    void        set_p4_id(int id);
    void        set_material_id(int id);

    void        set_id(int id);

    //! Function to compute centroid of tetrahedron
    SP_Point    compute_centroid_of();

protected:
    mesh*       _parent_mesh;

    int         _p1_id; // ID of the first boundary point
    int         _p2_id; // ID of the second boundary point
    int         _p3_id; // ID of the third boundary point
    int         _p4_id; // ID of the forth boundary point

    int         _material_id; // ID of the tetrahedron's material to know its properties
    
    int         _id; 
};

class material_opt
{
public: 
    // Default Constructor
    material_opt();

    // Regular Constuctor
    material_opt(int id, double mu_a, double mu_s, double g, double n, mesh* parent_mesh);

    // Destructor
    ~material_opt();

    // Getters
    mesh*       get_parent_mesh();

    double      get_mu_a();
    double      get_mu_s();
    double      get_g();
    double      get_n();

    int         get_id();

    // Setters
    void        set_mu_a(double mu_a);
    void        set_mu_s(double mu_s);
    void        set_g(double g);
    void        set_n(double n);

    void         set_id(int i);

protected:
    mesh*       _parent_mesh;

    double      _mu_a; // absorption coefficient
    double      _mu_s; // scattering coefficient
    double      _g;    // anisotropy coefficient
    double      _n;    // deflection angle

    int         _id; 
};
#endif
