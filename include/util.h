/**
 * @author: Abdul-Amir Yassine
 * @date: April 10th, 2017
 *
 * \file util.h \brief Helper methods Header File
 *
 * This file defines some helper methods and structs used in the code
 */

#ifndef _UTIL_H_
#define _UTIL_H_

#include <vector>
#include <unordered_set> 
#include <iostream>
#include <iomanip>
#include <fstream>
#include <cstdlib>
#include <sstream>
#include <map>
#include <string>
#include <algorithm>
#include <typeinfo>
#include <type_traits>
#include <cmath>
#include <unordered_map>
#include "profiler.h"
#include <random>
#include <unistd.h>
#include <fcntl.h>

// For timing measure
#include <chrono>

namespace bpt = boost::posix_time;



//! These two structs are used to sort a vector in descending order
struct element_idx 
{
    double element;
    int index;
};

struct sort_by_element
{
    bool operator()(element_idx const &a, element_idx const &b)
    {
        return a.element > b.element;
    }
};

using namespace std;

/** 
 * This namespace includes utility functions and classes that are used in the code base
 */
namespace Util
{
    extern chrono::steady_clock::time_point BEGIN_TIME;
    extern chrono::steady_clock::time_point END_TIME;


    //! print a vector to a matlab file
    template<typename T> void print_vector_matlab(FILE* matlab, 
                                               vector<T> &v, 
                                               string v_name)
    {
        if (v.size() == 0)
            return;


        fprintf(matlab, "%s = [", v_name.c_str());
        for (unsigned int i = 0; i < v.size(); i++)
        {
            if (is_same<T, float>::value)
                fprintf(matlab, "%-15f", v[i]);
            else if (is_same<T, int>::value)
                fprintf(matlab, "%-15d", v[i]);
            else if (is_same<T, unsigned int>::value)
                fprintf(matlab, "%-15ld", v[i]);
            else if (is_same<T, double>::value)
                fprintf(matlab, "%-15g", v[i]);
            else 
            {
                cerr << "Util::print_vector_matlab<T>::Undefined type!!!" << endl;
                exit(-1);
            }

            if (i < v.size() - 1)
                fprintf(matlab, ";");

            if ((i + 1) % 10 == 0)
                fprintf(matlab, "\n");
        }
        fprintf(matlab, "];\n\n");
    }

    // print raw data of a vector to a file
    template<typename T> void print_raw_vector(FILE* out, 
                                               vector<T> &v)
    {
        if (v.size() == 0)
            return;


        for (unsigned int i = 0; i < v.size(); i++)
        {
            if (is_same<T, float>::value)
                fprintf(out, "%-15f\n", v[i]);
            else if (is_same<T, int>::value)
                fprintf(out, "%-15d\n", v[i]);
            else if (is_same<T, unsigned int>::value)
                fprintf(out, "%-15ld\n", v[i]);
            else if (is_same<T, double>::value)
                fprintf(out, "%-15g\n", v[i]);
            else 
            {
                cerr << "Util::print_raw_vector<T>::Undefined type!!!" << endl;
                exit(-1);
            }

        }
    }


    //! Print a matrix to a matlab file
    void print_matrix_matlab(FILE* matlab, vector<vector<float>>& mat, string M_name);
    
    //! Print a dense matrix to a matlab file
    void print_matrix_matlab_dense(FILE* matlab, vector<vector<float>>& mat, string M_name);

    //! This function prints the runtime and usage statistics from the profiler class
    void print_profiler_stat();

    //! This method returns the current local system time
    bpt::ptime get_local_time();

    //! This method sorts a given vector in descending order and returns the corresponding permutation
    vector<double> get_sorted_vector_idx(const vector<double> & vec_in, vector<int> & permutation_vec_out);

    //! This function returns the slope of a linear fit given data points (x,y) in two vectors
    double compute_slope_regression(const std::vector<double>& x, const std::vector<double>& y); 
	
    //! This function disables stdout and stderr by redirecting outputs to /dev/null
    void redirect_standard_streams_to_DEVNULL(int *_piOriginalSTDIN_FILENO, int *_piOriginalSTDOUT_FILENO, int *_piOriginalSTDERR_FILENO);

    //! This function restores stdout and stderr disabled by redirect_standard_streams_to_DEVNULL()
    void restore_standard_streams(int *_piOriginalSTDIN_FILENO, int *_piOriginalSTDOUT_FILENO, int *_piOriginalSTDERR_FILENO);
	
	/** 
	 * This struct defines a tri-modal distribution of 3 normal distributions with different weighting.
	 * The idea is to skew one normal distribution with mean mu and standard deviation sigma to its 2*sigmas 
	 */
	template<class T> class multimodal_distribution { 
		std::array<std::normal_distribution<T>, 3> _models; 
		std::discrete_distribution<std::size_t> _weights; 

		public:
		/** Default Constructor */
		multimodal_distribution() {}

		/** 
		 * Constructor
		 */
		multimodal_distribution(T mu, T sigma) {
			_models[0] = std::normal_distribution<T>(mu, static_cast<T>(0.5)*sigma); // Center model
			_models[1] = std::normal_distribution<T>(mu - 2*sigma, static_cast<T>(0.5)*sigma); // Left model
			_models[2] = std::normal_distribution<T>(mu + 2*sigma, static_cast<T>(0.5)*sigma); // Right model

			_weights = std::discrete_distribution<std::size_t> {
				0.1, // weight of center model 
				0.45,// weight of left model
				0.45 // weight of right model
			};
		}

		/** This method returns a random number according to the tri-modal distribution*/
		T operator()(std::mt19937 & gen) {
			std::size_t idx = _weights(gen);
			return _models[idx](gen); 
		}
	};
}
#endif
