/**
 * @author: William Kingsford (Last edited by Abed Yassine)
 *
 * @\brief A class that contains the implementation of a fluence interpolation engine for FullMonte
 *
 * High level idea:
 * 1) Evaluate a given function at a set of evenly-spaced sample points
 * 2) Use sinc or raised_cosine interpolation to evaluate the sampled function at points other than the sample points
 */

#ifndef _INTERPOLATION_H_
#define _INTERPOLATION_H_

#include "pdt_plan.h"
#include "read_mesh.h"
#include "run_fullmonte.h"
#include "profiler.h"

#include <Eigen/Core>

#include <iostream>
#include <functional>
#include <utility>
#include <unordered_map>
#include <vector>
#include <sstream>


// wrap a vector of integers, representing source parameters, in a struct that has
// a hash function, so that it can be used as the key for an unordered_map
struct source_params_key {
    vector<int> params;

    // two sets of source parameters are equal if all parameters are equal
    bool operator==(const source_params_key &other) const {
        if (params.size() != other.params.size()) {
            return false;
        }
        else {
            bool ret_val = true;
            for (unsigned i=0; i<params.size(); i++) {
                ret_val = ret_val && (params[i] == other.params[i]);
            }
            return ret_val;
        }
    }
};

// primes for use in obtaining one integer from a vector of integers for the purpose
// of hashing it. Note that a source parameters vector never has more than 6 components
const long long primes_array[6] = { 47, 2767, 11003, 37139, 78301, 102077 };

// include the hash function in the std namespace, so that unordered_map can see it
namespace std {
    template<>
    struct hash<source_params_key> {
        size_t operator()(const source_params_key& k) const {
            long long to_hash = 0; // at least 64 bits
            // summing the parameters, each multiplied by a different prime number,
            // gives a single integer which is unlikely to be the same for different
            // parameter vectors
            for (unsigned  i=0; i<k.params.size(); i++) {
                to_hash += primes_array[i % 6] * k.params[i];
            }
            return hash<long long>{}(to_hash);
        }
    };
}

namespace SP_IE
{

//! This is used in the source cache to avoid storing data with zero
// fluence
typedef std::pair<std::vector<float>, std::vector<unsigned>> fluence_index_pair;

class source_cache; 

// normalized sinc function: sin(pi*x)/(pi*x)
double sinc(double x);

// assumes x is normalized so delta_x=1, so resulting function has magnitude 1 in 
// the frequency domain for |f|<=(1-beta)/2
double raised_cosine(double x, double beta);

// integrate a function f over an interval [l, r] using midpoint rule 
double integrate_midpoint(std::function<double(double)> func, double l,
                double r, unsigned num_points);

// Overload version of the numerical integrator for vector-valued functions
Eigen::RowVectorXd integrate_midpoint(std::function<Eigen::RowVectorXd(Eigen::RowVectorXd)> func, 
        Eigen::RowVectorXd l, Eigen::RowVectorXd r, unsigned num_points);

// evaluates function at a given point, using sampled points and either a sinc or raised_cosine kernel
// x: point to evaluate at
// x_min, x_max: lower/upper bounds of range of sample points
// delta_x: step size for sampling
// f: vector of function values at sample points (matching indices with x_s)
// N: number of sample points to include on each side
// use_raised_cosine: use raised_cosine if true, else use sinc
// beta: width parameter for raised_cosine (unused if use_raised_cosine == false)
double evaluate_interpolation(double x, double x_min, double x_max, double delta_x, 
                              vector<double> &f, int N, bool use_raised_cosine, double beta);

// overloaded version for vector-valued functions of 2 variables
Eigen::RowVectorXd evaluate_interpolation(double x[2], double x_min[2],
                              double delta_x, 
                              source_cache &grid_sources, int N, 
                              bool use_raised_cosine, double beta);

// overloaded version for functions of 3 variables
double evaluate_interpolation(double x[3], double x_min[3], double x_max[3], double delta_x, 
                              vector<vector<vector<double>>> &f, int N, 
                              bool use_raised_cosine, double beta);

// overloaded version for vector-valued functions of 3 variables
Eigen::RowVectorXd evaluate_interpolation(double x[3], double x_min[3], double x_max[3], double delta_x, 
                              vector<vector<vector<Eigen::RowVectorXd>>> &f, int N, 
                              bool use_raised_cosine, double beta);

// TODO: replace double[2] with std::vector
vector<Eigen::RowVectorXd> evaluate_interpolation_gradient(double x[2], double x_min[2],
                              double delta_x, 
                              source_cache &grid_sources, int N, 
                              bool use_raised_cosine, double beta);

//! Interpolates a point source using linear interpolation and source_cache
Eigen::RowVectorXd evaluate_interpolation(const Eigen::RowVectorXd &pt,
                        const Eigen::RowVectorXd & x_min,
                        double delta_x, source_cache & grid_sources);

//! Interpolates the fluence from a light source by interpolating point
//! sources along the line and then numericall integrate
//! line [in]: a line source to interpolate given by two end points
//! x_min [in]: lower bound on sample points for the grid position
//! grid_sources [in/out]: A cache of point source evaluations. Once a
//!                        new point source is simulated, the cache is updated
//! num_pts [in]: The number of points to integrate the line source along.
//! step_size [in]: step size in the grid of point sources
vector<float> interpolate_line_source(const SP_Line & line, const vector<double> & x_min, 
        source_cache & grid_sources, unsigned num_pts, double step_size);

// f: function pointer to sample
// x_min, x_max: sampling range
// step: step size
// func_values: vector of samples values
void sample_function(std::function<double(double)> &f,
                               double x_min,
                               double x_max,
                               double step,
                               vector<double> & func_values);
// overloaded 3d version
void sample_function(std::function<double(double, double, double)> &f,
                               double x_min[3],
                               double x_max[3],
                               double step,
                               vector<vector<vector<double>>> & func_values);

// lookup table for precomputed values of the raised cosine filter
class RaisedCosLUT {
private:
    vector<double> _values;
    int _offset; // the index corresponding to x=0
    double _res; // resolution, i.e. number of points in each interval [n, n+1)
    bool _initialized;
    static RaisedCosLUT* _s_instance;
    RaisedCosLUT() {
        _initialized = false;
    }

public:
    double get(double x) {
        return _values[static_cast<unsigned>(_res*x + _offset)];
    }
    // _bound is the largest absolute value of argument to raised_cosine, _res is the 
    // resolution, beta is the roll-off factor
    void initialize(double bound, double res, double beta) {
        int size = static_cast<int>(2*ceil(bound*res) + 1);
        _offset = size / 2;
        _res = res;
        _values.resize(size);
        for (int i=0; i<size; i++) {
            _values[i] = raised_cosine(((double)(i - _offset)) / res, beta);
        }
        _initialized = true;
    }

    bool isInitialized() {
        return _initialized;
    }

    static RaisedCosLUT *instance() {
        if (!_s_instance) {
            _s_instance = new RaisedCosLUT;
        }
        return _s_instance;
    }
};


// container for FullMonte results for virtual sources, for use in interpolation 
class source_cache {
public:
    source_cache() {
    }

    ~source_cache() {
    }

    // Parameters that mean the same thing for point and line sources:
    //     num_params: the number of parameters (currently only accepts 2 or 3, where 2 is for 
    //                 parallel line sources and 3 is for point sources)
    // for point sources:
    //     p: the point corresponding to parameters of all zero
    //     u, v, w: three orthogonal vectors, of lengths equal to the grid spacing
    // for line sources:
    //     p: the point corresponding to parameters of all zero
    //     u, v: two orthogonal vectors, orthogonal to the line direction, of lengths equal
    //           to the grid spacing
    //     w: the vector that is added to the start point of the line to give the end point
    source_cache(int num_params, unsigned num_tetra, 
            const vector<double> & p, const vector<double>& u, const vector<double>& v, const vector<double>& w,
            WrapperFullMonteSW * fm_engine) :
            _fullmonte_engine(fm_engine)
    {
        if (num_params < 2 || num_params > 3)
        {
            fprintf(stderr, "\033[1;%dm Number of parameters in source cache is invalid.\033[0;m\n", 31); 
            std::exit(-1); 
        }
        
        _num_params = num_params;

        _num_sources = 0;

        _p.resize(3);
        _u.resize(3);
        _v.resize(3);
        _w.resize(3);

        for (unsigned i = 0; i < 3; i++)
        {
            _p[i] = p[i];
            _u[i] = u[i];
            _v[i] = v[i];
            _w[i] = w[i];
        }

        _num_tetra = num_tetra; 
    }

    // returns cached value if available, else runs FullMonte to get result
    // note: cannot return by reference, since the memory location of an element in an
    // unordered_map can change when new elements are added TODO: (Abed) may slow us down 
    Eigen::RowVectorXd get_source(const vector<int> & source_parameters) 
    {
        // check if the number of parameters is valid
        if (source_parameters.size() != _num_params) 
        {
            std::cout << "ERROR: in source_cache::get_source(), source_parameters.size() = "
                 << source_parameters.size() << " != num_params = " << _num_params << std::endl;
            
            // terminate after printing error message
            std::exit(-1);
        }

        // wrap source parameters into key structure for use with unordered_map
        source_params_key key;
        key.params = source_parameters;

        auto it = _fullmonte_results.find(key);
        if (it == _fullmonte_results.end()) 
        {
            std::cout << "Failed to find source in cache, running FullMonte." << std::endl;
            _num_sources++;
            
            vector<float> fluence_vals;
            vector<Source::Abstract*> sources_fm(1);

            if (_num_params == 3)
            {
                Source::Point *p = new Source::Point; 
                Eigen::RowVector3d source_coords = _p + (double)source_parameters[0]*_u +
                                                 (double)source_parameters[1]*_v +
                                                 (double)source_parameters[2]*_w;
                p->position({static_cast<float>(source_coords(0)),
                             static_cast<float>(source_coords(1)), 
                             static_cast<float>(source_coords(2))});
            
                std::cout << "Running point source: (" << source_coords(0) <<
                    ", " << source_coords(1) << ", " << source_coords(2)
                    << ")" << std::endl;
                sources_fm[0] = p;
            }
            else
            {
                Source::Line * line = new Source::Line(); 
                Eigen::RowVector3d source_coords = _p + (double)source_parameters[0]*_u +
                                                 (double)source_parameters[1]*_v;
                std::cout << "Running line source: (" <<
                    source_coords(0) << ", " << source_coords(1) << ", " <<
                    source_coords(2) << ") ---->"; 
                line->endpoint(0, {static_cast<float>(source_coords(0)),
                             static_cast<float>(source_coords(1)), 
                             static_cast<float>(source_coords(2))});

                source_coords = source_coords + _w;
                std::cout << "(" << source_coords(0) << ", " <<
                    source_coords(1) << ", " << source_coords(2) << ")" <<
                    std::endl;
                line->endpoint(1, {static_cast<float>(source_coords(0)),
                             static_cast<float>(source_coords(1)), 
                             static_cast<float>(source_coords(2))});
                sources_fm[0] = line;
            }
            
            std::stringstream f_name; 
            f_name << "/media/yassineab/DATAPART1/PhD_Research_temp_data/fm_interpolation/Colin27_tumor4/packets_1e4/source_num_" <<
                            _num_sources << ".vtk";

            _fullmonte_engine->run_composite_fullmonte(sources_fm, fluence_vals, true, f_name.str(), 10000); 
            std::cerr << "Num sources: " << _num_sources << std::endl;

            if (static_cast<unsigned>(fluence_vals.size()) != _num_tetra)
            {
                std::cerr << "source_cache::Fluence vector size does not match number of tetra" << std::endl;
                std::exit(-1); 
            }

            // call FullMonte to get results for this source
            Eigen::RowVectorXd fluence_vec(_num_tetra);
            std::vector<float> fluence_temp;
            std::vector<unsigned> indices; 
            for (unsigned i = 0; i < _num_tetra; i++)
            {
                fluence_vec(i) = fluence_vals[i]; // TODO: check that indices match
                if (fluence_vals[i] > 1e-9)
                {
                    indices.push_back(i); 
                    fluence_temp.push_back(fluence_vals[i]); 
                }
            }

            // add to hash table
            _fullmonte_results.insert(std::make_pair(key, std::make_pair(fluence_temp, indices))); // fluence_vec));

            delete sources_fm[0];


            return fluence_vec;
        }
        else {
            // return the value corresponding to the key, i.e. the vector of fluences
            //return it->second;
       
            Eigen::RowVectorXd fluence_vec = Eigen::RowVectorXd::Zero(_num_tetra);  
            for (unsigned i = 0; i < it->second.first.size(); i++)
            {
                fluence_vec(it->second.second[i]) = it->second.first[i];
            }

            return fluence_vec; 
       }
    }

    // convenience functions
    // note: cannot return by reference, since the memory location of an element in an
    // unordered_map can change when new elements are added
    Eigen::RowVectorXd get_source(int t, int s) {
        return this->get_source(vector<int>({t, s}));
    }
    Eigen::RowVectorXd get_source(int x, int y, int z) {
        return this->get_source(vector<int>({x, y, z})); 
    }
    

    // only called from outside the class for testing purposes
    void add_source(const vector<int> &position, const Eigen::RowVectorXd &source) {
        assert(position.size() == _num_params);

        source_params_key key;
        key.params = position;

        //auto to_insert = std::make_pair(key, source);
        std::vector<float> fluence;
        std::vector<unsigned> indices; 
        for (unsigned i = 0; i < source.size(); i++)
        {
            if (std::fabs(source(i)) > 1e-9) // not zero
            {
                fluence.push_back(static_cast<float>(source(i))); 
                indices.push_back(i); 
            }
        }
        auto to_insert = std::make_pair(key, std::make_pair(fluence, indices)); 

        _fullmonte_results.insert(to_insert);
    }

    // Return the number of tetra
    unsigned get_num_tetras() { return _num_tetra; }
        
private:
    // number of parameters required to specify a source
    unsigned _num_params;

    // Number of tetra considered 
    unsigned _num_tetra;

    // used only for debugging purposes: stores the number of sources in the cache
    unsigned _num_sources;

    // vectors specifying coordinate frame:
    //      _p is the origin
    //      _u,_v,_w are the three axes for point sources; for line sources w is the vector
    //        from the start of the line to its end
    Eigen::RowVector3d _p;
    Eigen::RowVector3d _u;
    Eigen::RowVector3d _v;
    Eigen::RowVector3d _w;

    // hash table to store results: key is source parameters, value is vector of fluences
    unordered_map<source_params_key, fluence_index_pair> _fullmonte_results;

    // Wrapper Engine to FullMonte
    WrapperFullMonteSW* _fullmonte_engine;
};

};


#endif
