/**
 * \author: Abdul-Amir Yassine
 * \date: March 6th, 2018
 *
 * \file pdt_plan.h \brief Planning Header File
 *
 * This file defines all the elements and/or global variables needed for a plan
 * 
 * - The constructor of this class takes in as input the path to the input parameters file from the user.
 * - Once an object is created, the constructor would read the mesh, the tissue dose thresholds and the output of the light simulator FullMonte.
 * - The user can then call "get_thresholds_with_guardband()" to get the dose thresholds with the required guardband.
 * - Then the user would call "remove_unnecessary_tetra(thresholds)" to remove the tetra that do not contribute to the cost function. 
 * - The user would then call "compute_original_tetra_data()" and "compute_tetra_data(thresholds)" to compute the volumes, importance weights and thresholds of each individual tetrahedron.
 * - After that, the user would call "initialize_lp()" to creat a new instance of the optimization, and "solve_lp(thresholds)" to solve the linear program.
 * 
 * There are several functions to get different outputs from such as "compute_v_alpha_tissues(thresholds, alpha)" which computes the volume of each tissue 
 * at alpha*100% of its dose threshold. Also the user can compute the final fluence distribution with the power allocation, the final power allocation
 * and data about the optimizer such as runtime and cost.
 */

#ifndef _PDT_PLAN_H_
#define _PDT_PLAN_H_

#include "pdt_space_config.h"

#include "util.h"
#include "read_mesh.h"

#include <vector>
#include <unordered_set> 
#include <iostream>
#include <iomanip>
#include <fstream>
#include <cstdlib>
#include <sstream>
#include <map>
#include <unordered_map>
#include <string>
#include <algorithm>

using namespace std;

// Forward declarations
class mesh; 
class SP_Point; 
class SP_Line; 
class src_placer; 
class fixed_sources_cp;
class WrapperFullMonteSW;
class RtFeedback;
class sa_engine;
class Src_Abstract;
class Parser;
struct Parser_injection_points;

class pdt_plan
{
public:
    //! Default Constructor
    pdt_plan();

    //! Regular Constructor
    pdt_plan(string params_file);
    pdt_plan(string params_file, Parser_injection_points& injection_points, unordered_set<unsigned>& critical_regions);

    //! Destructor
    ~pdt_plan();
    
    // methods
    //! This method reads the mesh and stores it in _data
    void read_mesh();
    void read_tissue_properties();
    void read_fullmonte_output_matrix();
	void read_initial_placement(); 

    void run_fullmonte_multiple_sources(const vector<Src_Abstract*> & sources); // method to call the FM wrapper and run one at a time 
    void run_fullmonte_composite_source(const vector<float> & source_powers); // method to call the FM wrapper and run composite 

    void run_fullmonte_single_source(const vector<Src_Abstract*> & sources, unsigned idx); // method to call the FM wrapper for a single source
    void backup_matrix_single_source(unsigned idx); // method to perform backup on fluence matrix for single source
    void restore_matrix_single_source(unsigned idx); // method to restore fluence matrix if single move not accepted
    void reset_matrix_backup_flag();
    void backup_matrix(); // method to perform backup on entire fluence matrix
    void restore_matrix(); // method to restore fluence matrix if multi source move not accepted

    void remove_unnecessary_tetra(const vector<double> & guardbanded_thresholds);

    void compute_tetra_data(const vector<double> & thresholds); // method to compute the volumes, weights and thresholds of all tetras 

    void compute_original_tetra_data(const vector<double> & thresholds); 
            // method to compute volumes, weights and thresholds before removed unnecessary tetra

    vector<double> get_thresholds_with_guardband(); // gets thresholds with guardband
    vector<double> get_thresholds(); // gets original thresholds

    void initialize_lp(); // default number of sources
    void initialize_lp(unsigned int num_sources, double p_max_value); // specified number of sources
    void initialize_lp(unsigned int num_sources, double p_max_value, 
						const std::vector<std::vector<float>> & fluence_matrix); // specified number of sources
    
    void solve_lp(const vector<double> & guardbanded_thresholds);

    void compute_post_opt_fluence();

    vector<double> compute_v_alpha_tissues(const vector<double> & guardbanded_thresholds, double alpha);

    

    // void evaluate_placer(double new_num_packets, string vtk_matrix_file, double guardband); // method to evaluate the place with different number of packets


    void conservative_approach_op_variation(unsigned int num_op_cases); // method that reads all fullmonte output cases for tumor op variation, 
                                                                        // assigns each healthy tissue element fluence to the max of that element,
                                                                        // and each tumor element fluence to the min of that element
    
    void percentile_approach_op_variation(string vtk_file_path, unsigned int num_op_cases, double percentile); // method that reads all fullmonte output cases for tumor op variation, 
                                                                        // assigns each healthy tissue element fluence to the <percentile> of that element 
                                                                        // (value that is largest than <percentile> of the elements),
                                                                        // and each tumor element fluence to the 1-percentile of that element    
                                                                        // 0 < percentile < 1

    vector<vector<double>> compute_v_alpha_tissues_percentile_approach(const vector<double> &
                                          guardbanded_thresholds, double alpha, unsigned int num_op_cases); // method to generate a set of v100 for different cases of OP


    void read_fullmonte_output_diff_op(string vtk_file_path, unsigned int num_op_cases); // This method reads different outputs of FullMonte 
                                                                 // corresponding to different sets of optical properties, and places the results in _placers vector


    void op_variation_feasibility_problem(const vector<double> & guardbanded_thresholds, unsigned int num_op_cases, double delta); // This method creates a feasibility problem that guarantees 
                                                         //that no tetra exceeds its threshold dose with a probability of 1-delta when the optical properties are varying

    void cvar_optimization_op_problem(unsigned int num_op_cases, double beta); // This method minimizes the expected cost while constraining the expected dose
                                                                            // received by the highest 100*(1-beta)% of OAR tissues and lowest 100*(1-beta)% of the tumore 
    
    void compute_tetra_dose_variance(); // computes the variance of the dose at each tetrahedron from each source due to the variation in the optical properties 

    void run_sa_engine(); // creates and runs an SA engine

	void get_diffuser_placement_and_dose_matrix(); // Either runs SA or reads initial placement, and then calls FM to generate dose matrix

    void mesh_sources(); // This method generates a vector of vectors of tetra IDs that enclose the points throug which each source passes through,
                        // Then it runs a python script that tags the tetra of each source with a new material and generates a new vtk mesh

	std::vector<std::vector<Src_Abstract*>> run_virtual_source_reduction(unsigned num_plans, 
												const std::vector<double>& thresholds); // Reduces number of point 
												// sources from a set of virtual sources
												// by choosing the highest powered sources
   
    void print_v100s(const std::vector<double> & v100s); 

    void run_rt_feedback(); // Implements the rt feedback system to recover tumor OPs
    void power_variation_problem(double weight, double uncertainty, vector<double>& thresholds); // This method runs the problem with power variation

    // Getters
    vector<double> &         get_d_max_vec();
    vector<double> &         get_d_min_vec();
    vector<double> &         get_slopes();
    vector<double> &         get_d_max_tissues();
    vector<double> &         get_d_min_tissues();
    vector<double> &         get_guardband_tissues();
    vector<double> &         get_original_tetra_volumes(); 
    vector<double> &         get_tetra_volumes(); 
    vector<double> &         get_tetra_weights(); 
    vector<unsigned int> &   get_original_tetra_material_ids(); 
    vector<unsigned int> &   get_tetra_material_ids(); 
    vector<bool>   &         get_tetra_to_consider(); 
    vector<unsigned int> &   get_tumor_indices(); 
 	vector<Src_Abstract*> &  get_initial_placement();  
    
    unordered_map<unsigned, string> & get_tissue_names(); 

    // getters for results
    vector<double> & get_final_source_powers();
    vector<double> & get_final_fluence();
    vector<double> & get_lp_cost();
    vector<double> & get_lp_runtime();
    vector<vector<Src_Abstract*> > & get_placement_results();

    double           get_last_cost();

    double           get_total_power();
    unsigned int     get_num_non_zero_sources();

    
    src_placer*     get_placer();
    mesh*           get_mesh();

    double          get_num_packets();
    double          get_total_energy();
    unsigned int    get_mesh_size();
    unsigned int    get_actual_mesh_size();
    unsigned int    get_num_sources();
    
	unordered_set<unsigned>    get_tumor_region_number();

    double          get_tumor_volume();
	double			get_tumor_weight(); 
    double          get_total_mesh_volume(); 
    SP_Point        get_tumor_center();


    // getters for input parameters
    string          get_wavelength();
    string          get_source_type();
    string          get_placement_type();
    string          get_fullmonte_output_file();
    bool            is_source_tailored();
	bool			is_running_tests(); 

	int 			get_num_iterations(); 
    string          get_params_file();

    // getters for testing
    sa_engine*      get_annealer_for_testing();

    bool            get_constrain_injection_point();
    unsigned        get_num_injection_points();

    // Setters
    void set_p_max_vector(double p_value);
    void set_num_packets(double num_packets);

	void set_tumor_weight(double weight); 
	void vary_tumor_weight(bool vary); 
	void set_target_tumor_v100(double target); 
	void set_tumor_v100_bounds(double lower, double upper); 
	void set_max_num_lp_iterations(int num); 
    void set_sa_early_termination();

    void set_initial_placement(vector<Src_Abstract*> placement);
    void set_final_source_powers(vector<double> source_powers); // Use with caution
    void reset_source_powers(); // Use with caution, resets source powers to 0

    /** 
     * Add path to project source directory to file paths 
     * 
     * All file paths in input parameter files assumed to be set to be relative paths to the project directory.
    */
    void override_file_paths ();

    // //! setter for _data_dir_path, _data_name, _optical_prop_file, _read_data_from_vtk and _wavelength
    // void set_parameters (string mesh_file, bool read_data_from_vtk, string wavelength);
            
    //! setter for _params_file
    void set_params_file (string params_file);

private:
    // Helper methods
    //! Helper method to read parameters file
    void read_params_file();

    // Members
    src_placer*                     _placer=NULL;
    mesh *                          _data=NULL;
    fixed_sources_cp*               _my_lp=NULL; // used to run an optimization program
    RtFeedback*                     _rt_feedback_system=NULL; // used to run a realtime feedback system for optical properties recovery
    
    // FM Engine
    WrapperFullMonteSW *            _fullmonte_engine=NULL;

    vector<src_placer*>             _placers; // used only in the OP variation study
    vector<vector<float>>           _cvar_expected_dose_matrix; // used only the OP variation study with CVAR constraints
    vector<vector<double>>          _source_element_dose_variance; // stores the variance in the dose at each tetrahedron from each source due to OP variation

    vector<double>                  _p_max; // vector that stores the max power value on all sources 
    vector<double>                  _d_max; // vector that stores the max thresholds for all tetra considered in the optimziation
    vector<double>                  _d_min; // vector that stores the min thresholds for all tetra considered in the optimization
    vector<double>                  _d_max_orig; // vector that stores the max thresholds for all tetra
    vector<double>                  _d_min_orig; // vector that stores the min thresholds for all tetra
    vector<double>                  _slopes; // this is a weight of each tissue type inversely proportional to the threshold
    vector<double>                  _d_max_tissues; // This stores the max threshold per tissue type
    vector<double>                  _d_min_tissues; // This stores the min threshold per tissue type
    unordered_map<unsigned, string> _tissue_names_ids; // Maps tissue name to the tissue ID
    vector<double>                  _guardband_tissues; // This stores the respective guardbands per tissue type

    vector<double>                  _tetra_weights; // weights of the tetra being used in the optimization
    vector<double>                  _original_tetra_volumes; // volumes of all tetra
    vector<double>                  _tetra_volumes; // volumes of the tetra being used in the optimization
    vector<unsigned int>            _original_tet_material_ids; // stores the material ids of all tetra before removing unnecessary tetra
    vector<unsigned int>            _tet_material_ids; // stores the material ids of the tetra considered in the optimization
    vector<unsigned int>            _tumor_indices; // stores the indices of the tumor tetra after ignoring the unnecessary tetra
    vector<unsigned int>            _original_tumor_indices; //stores the indices of the tumor tetra before ignoring the unnecessary tetra
	unordered_map<unsigned, double> _material_volumes; // stores the total volume of each material 

    Parser*                         _fparser=NULL;
    string                          _wavelength; // stored in the format "XXXnm", where XXX is the wavelength
    string                          _source_type;
    string                          _placement_type;
    string                          _mesh_file;
    string                          _fullmonte_output_file; // file that contains the fullmonte output
    string                          _final_distribution_output_file; // stores the vtk mesh with the final fluence distribution after running the composite source with the final power allocation 
    double                          _tumor_weight; // external weight set by the user for the tumor
    bool                            _source_tailored;
	bool						    _running_tests; // indicates if we are running regressions or not
    bool                            _file_path_override;

    bool                           _op_cvar_approach; // parameter that specifies to run the CVAR optimization problem for OP variation
    double                         _beta_cvar; // specifies the highest/lowest percentage of the tissue volume to take the expected dose from in the CVAR constraints

    bool                           _op_variance_constraints; // parameter that specifies to run the variance-constraints approach for OP variation 
    bool                           _use_cuda;
    
    bool                          _power_variance_optimization; // parameter that specifies to run with power uncertainty variation
    double                        _sigma_power; // specifies the uncertainty in power, initially fixed to 20% of max power
    double                        _power_variance_weight;

    double                         _num_packets;
    double                         _total_energy;

    unordered_set<unsigned>        _tumor_region_number; // usually size is 1, but might be more to model different PS regions
    unsigned                       _max_region_index;
    unsigned int                   _mesh_size;
    unsigned int                   _num_sources;

    double                         _tumor_volume;
    double                         _total_mesh_volume;
    SP_Point                       _tumor_center;

    bool                           _ignore_unnecessary_tetra;
    unsigned int                   _actual_mesh_size; // stores the mesh size after removing the unnecessary tetra
    vector<bool>                   _tetra_to_consider; // vector that stores if a tetra is ignored or not. True iof used and false if not

    vector<float>                  _fluence_matrix_column_backup; // vector to store backup copy of one column in the matrix
    vector<vector<float> >         _fluence_matrix_backup; // vector to store backup copy of entire fluence matrix
    bool                           _fluence_matrix_backup_called=false; // TODO: add a check for whether backup_matrix_single_source() is called

	// optimization parameters
    bool                           _vary_tumor_weight; // used to vary the weight to achieve a 98% v100
	double						   _target_tumor_v100;
	double						   _lower_tumor_v100_bound;
	double						   _upper_tumor_v100_bound;
	int 						   _max_num_lp_iterations; // used to stop the solver if max num iterations exceeded
	int							   _num_lp_iterations; 
	bool						   _lp_constraints_defined;
	bool					       _lp_objective_defined;
    
    // Results members
    vector<double>                 _final_source_powers;
    vector<double>                 _final_fluence;
    vector<double>                 _lp_cost;
    vector<double>                 _lp_runtime;

    unsigned int                   _num_non_zero_sources;
    double                         _total_power;


    // Members to read the paramters files
    unordered_map<string, string>  _params_to_values_map;
    string                         _params_file;
    bool                           _read_data_from_vtk;


    // Helper booleans to make sure that the proper functions to set up the inputs were already called
    bool                           _guardband_thresholds_called;
    bool                           _is_p_max_set;
    bool                           _is_unnecessary_removed; // check if remove_unnecessry_tetra has been called
    bool                           _is_tetra_data_called; 
    bool                           _is_lp_solved; 

    // Related to sa engine parameters
	bool						   _run_sa;
    double                         _sa_engine_beta;
	double						   _sa_engine_lambda;
	std::string					   _sa_engine_progress_placement_file; 
    double                         _sa_engine_moves_per_temp;
	// vector<SP_Line>				  _initial_placement; 
	vector<Src_Abstract*>		   _initial_placement; 
    bool                           _sa_engine_early_terminate;
    bool                           _constrain_injection_point;
    unsigned                       _num_injection_points;
    vector<vector<Src_Abstract*> > _placement_results;
    // PlacementMode                  _placement_mode;

    // Parameters related to rt_feedback_system
    bool                           _run_rt_feedback;
    int                            _rt_feedback_rand_seed;
    float                          _rt_feedback_detector_radius;
    float                          _rt_feedback_detector_na;
    unsigned                       _rt_feedback_lut_size;
    string                         _rt_feedback_lut_file_name;
};

#endif
