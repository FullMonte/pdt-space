/**
 * This class define an optimization problem for the fixed candidate source positiions
 * The optimization problem is of the form:
 *         
 *                     min ||cost(x)||_1
 *                      x
 *                      s.t 0 <= x <= P
 *
 * where cost(x) is some defined covex cost
 *
 *  @author Abed Yassine
 *  @date April 4th, 2017
 * 
 */

#ifndef __FIXED_SOURCES_CP_H__
#define __FIXED_SOURCES_CP_H__

#include <vector> 
#include <unordered_map>
#include <unordered_set>

#include "read_mesh.h"

#include "fusion.h"

class fixed_sources_cp
{
public:
    /** default constructor */
    fixed_sources_cp();

    /** Regular Constructor **/
    fixed_sources_cp(unsigned int mesh_size, unsigned int num_sources,
           unsigned int num_tumor_elements,
           const vector<double> & d_max, const vector<double> & d_min,
           const vector<double> & p_max,
           const vector<vector<float>> & source_element_fluence_matrix,
           const vector<double>& tetra_weights,
           const vector<bool> & tetra_to_consider);

    /** default destructor */
    virtual ~fixed_sources_cp();

    /** 
     * This method performs gradient descent on the given initial solution in order to manually 
     * minimize the cost
     * Currently, the derivative is based on the linear formulation of the optimization problem we 
     * defined
     */
    vector<double> gradient_descent(double learning_rate, const vector<double> init_solution);

	/**
	 * This method defines the constraints of the basic LP model
	 */
	virtual bool define_mosek_constraints(); 

	/**
	 * This method defines the objective of the basic LP model
	 */
	virtual bool define_mosek_objective(); 

	/**
	 * This method updates the tumor constraints when changing its weight
	 */
	virtual bool update_tumor_mosek_constraints(double curr_tumor_weight); 

    /**
     * This method performs the optimization using the Mosek Interface
     */
     virtual vector<double> run_mosek_optimization(double & optimizer_time, 
                                                   unsigned int & num_nonzero_powers,
                                                   double & total_power);
    
    /**
     * This method performs the optimization using the Mosek Interface with tailored emission profiles
     */
     virtual vector<double> run_mosek_optimization_with_tailored_profiles(const vector<unsigned int>& fiber_elements,
                                                    double & optimizer_time, 
                                                   unsigned int & num_nonzero_powers,
                                                   double & total_power);

     /**
      * This method is to initialize vectors needed for the CVaR optimization approach for OP variation
      */ 
      virtual void initialize_cvar_parameters(double beta_cvar, const vector<unsigned int> & material_ids);

    /**
     * This method performs the optimization using the Mosek Interface with CVaR constraints
     */
     virtual vector<double> run_mosek_optimization_with_cvar_constraints(double & optimizer_time, 
                                                   unsigned int & num_nonzero_powers,
                                                   double & total_power, const unordered_set<unsigned>& tumor_region_number,
                                                   unsigned int num_tissues, 
                                                   const vector<vector<float>> & expetced_dose_matrix);
     
     /**
      * This method performs the optimization using the Mosek Interface with constraints on the variance of the dose due to OP variation
      */
     virtual vector<double> run_mosek_optimization_with_variance_constraints(double & optimizer_time, 
                                                   unsigned int & num_nonzero_powers,
                                                   double & total_power, const unordered_set<unsigned> & tumor_region_number,
                                                   unsigned int num_tissues, 
                                                   const vector<vector<double>> & variance_dose_matrix);
    
     /**
      * This method performs the optimization using the Mosek Interface with power variation
      */
    virtual vector<double> run_mosek_optimization_with_power_variation(double & optimizer_time, 
                                                        unsigned int & num_sources_non_zero, 
                                                        double & total_source_power,
                                                        double & sigma, double & weight);

    // ! This method is to print results to a matlab file
/*     void print_results_matlab(string file_name, vector<double> results, string vector_name); */

    //! This method computes the current fluence given the source powers
    void compute_current_fluence(const vector<double>& current_source_powers, 
                                vector<double>& current_fluence_out);

    double get_cost();

	void set_tetra_weights(const vector<double>& tet_weights); 
private:
    fixed_sources_cp(const fixed_sources_cp&);
    fixed_sources_cp& operator=(const fixed_sources_cp&);

    // Helper methods
    double compute_p_norm_of(const vector<double> & v, int p);
    double dot_product(const vector<double> & v1, const vector<double>& v2);


    unsigned int _mesh_size;
    unsigned int _num_sources;
    unsigned int _num_tumor_elements;

    //double _tumor_weight;
    //double _non_tumor_weight;

    vector<double> _tetra_weights;

    // Upper and Lower Bounds
    vector<double> _d_max; 
    vector<double> _d_min;
    vector<double> _p_max;
    
    vector<vector<float>> _sources_elements_fluence_matrix;

    double _final_cost; // stores the cost after optimization

    double _beta_cvar; // this is only used in the OP variation CVaR optimization approach. 
                       // specifies the highest/lowest percentage of a tissue volume to take the average dose of in the CVaR constraints
    vector<unsigned int> _material_ids; // a vector of size of the mesh that specifies the material of each tetrahedron
                                        // used only in the cvar and variance methods which has tissue-aware constraints
    unordered_map<unsigned int, unsigned int> _materials_num_elements; // stores the number of elements in each tissue (used only in the CVaR constraints)


	// The following are related to the Mosek LP // TODO do the same for the CVAR, variance and tailored LPs
	mosek::fusion::Model::t 	_lp_model;
	mosek::fusion::Variable::t  _lp_x;
	mosek::fusion::Variable::t  _lp_t;

	vector<unsigned>	_tumor_indices; // used in updating the constraints weights.
	double		_scaling_factor;
};

#endif
