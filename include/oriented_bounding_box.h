/**
 * \author: Shuran Wang
 * \date: May 6, 2022
 *
 * \file oriented_bounding_box.h 
 * 
 * \brief Helper for determining OBB Header File.
 * 
 * This file includes the helper functions to compute the orientation of a specified 
 * region in the mesh.
 * 
 * This implementation uses the Principle Component Analysis algorithm.
 * Complexity is O(N) as analyzed in https://doi.org/10.1145/2019627.2019641.
 * This is chosen because we don't want an exact answer and it is easier to implement.
 * 
 * Implementation is based on https://gamedev.net/forums/topic/708578-aabb-vs-obb/5433482.
 */

#ifndef _ORIENTED_BOUNDING_BOX_H_
#define _ORIENTED_BOUNDING_BOX_H_

#include "read_mesh.h"
#include "pdt_plan.h"
#include "util.h"

#include <unordered_map>

class OBB;
class Quaternion;

/** Defines a 3x3 matrix. */
struct Matrix_3x3
{
    float m00, m01, m02;
    float m10, m11, m12;
    float m20, m21, m22;

    //! Overloads operator +
    Matrix_3x3 operator+(const Matrix_3x3& m) const;

    //! Overloads operator *
    Matrix_3x3 operator*(const Matrix_3x3& m) const;

    //! helper function to transpose a matrix
    Matrix_3x3 transpose_matrix() const;

    //! helper function to do diagonalization
    Quaternion Diagonalize();
};

/** Class that computes the oriented bounding box of a set of tetrahedrons in the mesh. */
class OBB 
{
public:
    //! Default constructor
    OBB(){}

    //! Regular constructor with provided region id
    OBB(mesh * data, int region);

    //! Regular constructor with provided tetra indices
    OBB(mesh * data, vector<unsigned> tetra_indices);

    //! Regular destructor
    ~OBB(){}

    //! Overloads ostream to print the output
    friend std::ostream& operator<<(std::ostream& os, const OBB& obb);
    
    //! Returns rotated bounding box in a given direction
    void get_bounding_box(int z_dir, SP_Point (&result)[8]);

    // Result fields
    SP_Point                       _final_eigen_vectors[3]; // stores all final Eigen vectors
    SP_Point                       _final_bounding_box[8];  // stores the coordinates of the bounding box

private:
    //! helper function to compute the mean from point data
    SP_Point sp_mean() const;

    //! helper function to compute the covariance matrix
    Matrix_3x3 covariance_matrix(SP_Point mean) const;

    //! helper function to do the computation
    void compute_obb();

    // data fields
    vector<SP_Point*>              _data_points;            // all points to consider
};

/**
 * Quaternion class used for diagonolization of symmetric matrix.
 */
class Quaternion
{
public:
    //! Default constructor
    Quaternion(){
        _x = 0.0f;
        _y = 0.0f;
        _z = 0.0f;
        _w = 0.0f;
    }

    //! Regular constructor
    Quaternion(float x, float y, float z, float w) : _x(x), _y(y), _z(z), _w(w){}

    //! Regular destructor
    ~Quaternion(){}

    //! Setter for one field by index
    void set_value(int i, float v);

    //! Overloads operator *
    Quaternion operator*(const Quaternion& q) const;

    //! Helper function to get the rotation matrix represented
    Matrix_3x3 get_matrix() const;

    //! Helper function for normalization
    void normalize();

    float _x;
    float _y;
    float _z;
    float _w;
};

#endif // _ORIENTED_BOUNDING_BOX_H_