/**
 * @author: Abdul-Amir Yassine
 * @date: February 22nd, 2017
 *
 * \file src_placer.h \brief Placer Header File
 *
 * This file defines all the classes related to the light source placement problem
 */

#ifndef _SOURCE_PLACEMENT_H_
#define _SOURCE_PLACEMENT_H_

#include <vector>
#include <unordered_set> 
#include <iostream>
#include <iomanip>
#include <fstream>
#include <cstdlib>
#include <sstream>
#include <map>
#include <string>
#include <algorithm>
#include "read_mesh.h"

#include "placer_defs.h"

using namespace std;

typedef struct Tumor_region
{
    // For now, we will define it as a small cube
    // Later we can make it as a mesh inside the actual mesh TODO

    float x_min, x_max, y_min, y_max, z_min, z_max;

} TRegion;

class src_placer;

class src_placer
{
public:
    //! Default Constructor
    src_placer();
    
    //! Destructor
    ~src_placer();

    //! This function generates a tcl script to run FullMonte with the input 
    //! source positions given and the output is a trace of the light transport
    void make_tcl_script_traces(string out_file_name, string mesh_path, 
                const vector<tetrahedron*> & sources); //const vector<Point*> & sources); 

    //! This function generates a tcl script to run FullMonte with the input 
    //! source positions given and the output is a the energy absorbed in each tetrahedron
    void make_tcl_script_absorption(string out_file_name, string mesh_path, 
                const vector<tetrahedron*> & sources,
                bool read_mesh_from_vtk); //const vector<Point*> & sources); 
    void make_tcl_script_absorption_multiple_sources(string out_file_name, string mesh_path, 
                const vector<tetrahedron*> & sources,
                const vector<float> & source_powers_to_run,
                bool read_mesh_from_vtk); //const vector<Point*> & sources); 

    //! This method reads the output vtk file and extracts the fluence at each element
    void read_vtk_file(string file_path, unsigned int mesh_size);
        
    //! This method returns a boolean vector indicating ignored rows that cannot be affected in the optimizer
    void get_ignored_tetras(const unordered_set<unsigned>& tumor_region_number, 
                mesh* data, const vector<double>& d_max_tissues, unsigned int & actual_mesh_sizei,
                vector<bool>& tetra_to_consider, const vector<double> & last_source_powers);

    //! This method returns the centroid of a tetrahedron
    SP_Point* compute_centroid_of(tetrahedron * t);

    //! This method runs the tcl script specified
    void run_tcl_script(string tcl_file_path);

    //! This method reads the output of the IPOPT optimization (as a vector of source powers)
    //! It then generates a script of the composite sources to run FullMonte
    void read_optimization_output(string filename, const vector<tetrahedron*>& sources, string mesh_path,
                                string script_name, mesh* data, const unordered_set<unsigned>& tumor_region_number,
                                bool read_data_from_vtk);

    //! This function is modified to specify the candidate sources we want
    vector<tetrahedron*> get_candidate_sources(mesh* data, const unordered_set<unsigned> & tumor_region_number,
                            unsigned int number_of_tetra_to_skip);

    // Getters

    //! This method returns the fluence at each element in the mesh
    vector<float>& get_list_of_elements_fluence();

    //! This method returns the matrix of the fluence due to each light source at each element
    vector<vector<float>> & get_source_element_fluence_matrix();

    //! This method returns the vector fiber elements that specifies the fiber each element belongs to
    vector<unsigned int> & get_fiber_elements();

    //! This method returns the number of tumor elements in the mesh
    unsigned int get_num_tumor_elements();

    //! This method returns the total_energy specified
    double get_total_energy();

    //! This method returns the number of photon packets used in FullMonte
    double get_num_packets();

    // Setters
    //! This method appends the computed vector of fluence at each element due to the last source added
    void append_fluence_vector_to_matrix();

    //! This method sets the total_energy
    void set_total_energy(double total_energy);

    //! This method sets the number of photon packets used in FullMonte
    void set_num_packets(double num_packets);

    //! This method sets the power uncertainty factor (variance)
    void set_power_uncertainty(double sigma);

    //! This method computes the current fluence given the source powers
    void compute_current_fluence(unsigned int mesh_size, 
                                const vector<double>& current_source_powers, 
                                vector<double>& current_fluence_out);

    //! This method computes the v_alpha of the different tissue types
    vector<double> compute_v_alpha_tissues(const vector<double>& current_fluences, 
                                       const vector<unsigned int>& tetra_types,
                                       const vector<double>& tetra_volumes,
                                       const vector<double>& dose_thresholds,
                                       const unordered_set<unsigned>& tumor_region_index, double alpha);


private:
    // Helper Methods
    
    //! This method checks if the given point is in the given tumor region in addition to some boundary
    //! This method is used in defining the candidate sources
    bool check_if_point_in_tumor_plus_region(SP_Point* pt, TRegion* reg);

    vector<float> _list_of_elements_fluence; 

    vector<vector<float> > _source_element_fluence_matrix; 

    unsigned int _num_tumor_elements;


    double _total_energy;
    double _num_packets;

    //! Variance of power uncertainty (sigma)
    double _power_uncertainty; 

    //! This vector is used in the tailored emission profiles to see which elements belong to the same fiber
    //! A value j at index i means that element i of the source powers vector is part of fiber j
    //! We fill this vector by naming the Fluence regions in the VTK file as Fluencej_i
    vector<unsigned int> _fiber_elements;
};

#endif
