/**
 * @author: Abed Yassine
 * @date: September 26th, 2019
 * @brief: A wrapper class to run a fullmonte kernel (since most of the options are constant)
 */

#ifndef _RUN_FULL_MONTE_H
#define _RUN_FULL_MONTE_H

/* FullMonte Includes */

#include <FullMonteSW/Config.h> 
#include "pdt_space_config.h"

// Geometry Includes
#include <FullMonteSW/Geometry/Sources/Ball.hpp>
#include <FullMonteSW/Geometry/Sources/Line.hpp>
#include <FullMonteSW/Geometry/Sources/Volume.hpp>
#include <FullMonteSW/Geometry/Sources/Composite.hpp>
#include <FullMonteSW/Geometry/Sources/Fiber.hpp>
#include <FullMonteSW/Geometry/Sources/Point.hpp>
#include <FullMonteSW/Geometry/Sources/PencilBeam.hpp>
#include <FullMonteSW/Geometry/Sources/Cylinder.hpp>
#include <FullMonteSW/Geometry/Sources/CylDetector.hpp>
#include <FullMonteSW/Geometry/Sources/TetraFace.hpp>
#include <FullMonteSW/Geometry/Predicates/VolumeCellInRegionPredicate.hpp>
#include <FullMonteSW/Geometry/Queries/TetraEnclosingPointByLinearSearch.hpp>
#include <FullMonteSW/Geometry/MaterialSet.hpp>
#include <FullMonteSW/Geometry/Material.hpp>

// Kernel Includes
#if USE_CUDA
#include <FullMonteSW/Kernels/CUDA/TetraCUDAVolumeKernel.hpp>
#include <FullMonteSW/Kernels/CUDA/TetraCUDASurfaceKernel.hpp>
#include <FullMonteSW/Kernels/CUDA/TetraCUDAInternalKernel.hpp>
#endif
#include <FullMonteSW/Kernels/Software/TetraVolumeKernel.hpp>
#include <FullMonteSW/Kernels/Software/TetraSurfaceKernel.hpp>
#include <FullMonteSW/Kernels/Software/TetraInternalKernel.hpp>
#include <FullMonteSW/Kernels/SeedSweep.hpp>

// Data types includes
#include <FullMonteSW/OutputTypes/OutputDataCollection.hpp>
#include <FullMonteSW/OutputTypes/SpatialMap2D.hpp>

// Queries Includes
#include <FullMonteSW/Queries/BasicStats.hpp>
#include <FullMonteSW/Queries/EnergyToFluence.hpp>
#include <FullMonteSW/Queries/EventCountComparison.hpp>

// Storage Includes
#include <FullMonteSW/Storage/TIMOS/TIMOSMeshReader.hpp>
#include <FullMonteSW/Storage/TIMOS/TIMOSMaterialReader.hpp>
#include <FullMonteSW/Storage/TIMOS/TIMOSMeshWriter.hpp>
#include <FullMonteSW/Storage/VTK/VTKMeshReader.hpp>
#include <FullMonteSW/Storage/VTK/VTKMeshWriter.hpp>
#include <FullMonteSW/Storage/VTK/VTKSurfaceWriter.hpp>
#include <FullMonteSW/Storage/TextFile/TextFileMatrixWriter.hpp>

class Parser;

class WrapperFullMonteSW
{
public:
    WrapperFullMonteSW(); 
    WrapperFullMonteSW(string mesh_file, Parser* fparser, bool read_vtk, bool use_cuda);

    ~WrapperFullMonteSW() 
    {
        if (_mesh) {
            delete _mesh; 
        }

        if (_mesh_writer) {
           delete _mesh_writer;
        }

        if (_materials) {
            delete _materials; 
        }
        
        if (_kernel) {
            delete _kernel;
        }
#if USE_CUDA
        if (_kernel_cuda) {
            delete _kernel_cuda;
        }
#endif

        if (_tetraCache) {
            delete _tetraCache;
        }

        if (_enclosing) {
            delete _enclosing;
        }
    }

    // run fullmonte helper functions
    void start_fullmonte_async(const Source::Abstract* source, long long unsigned num_packets);
    OutputDataCollection* get_fm_result();
#if USE_CUDA
    void start_fullmonte_async_cuda(const Source::Abstract* source, long long unsigned num_packets);
    OutputDataCollection* get_fm_result_cuda();
#endif

    void run_composite_fullmonte(const vector<Source::Abstract*>& sources, 
                            vector<float>& fluence, bool dump_data_to_file, std::string file_name = "", 
                            long long unsigned num_packets = 1000000);
    void run_multiple_fullmonte(const vector<Source::Abstract*>& sources, 
                        vector<vector<float>> & fluence_matrix, 
                        bool dump_data_to_file, std::string file_name = "", 
                        long long unsigned num_packets = 1000000);
    void run_single_fullmonte(unsigned idx, Source::Abstract* source, 
                        vector<vector<float>> & fluence_matrix, 
                        bool dump_data_to_file, std::string file_name = "", 
                        long long unsigned num_packets = 1000000);

    void run_multiple_fullmonte_with_detectors(const vector<Source::Abstract*> & sources, 
                std::vector<std::vector<float>> & detected_weights, 
                float detected_radius, float detector_na, long long unsigned num_packets = 1000000);
    
    void write_mesh(string file_name);
    
    unsigned get_tetra_enclosing_point(float xcoord, float ycoord, float zcoord); 

    void update_kernel_materials(MaterialSet* new_ms) { _kernel->materials(new_ms); }

    void create_material_set(); // read materials from file parser and update kernel material set
    void read_parameters();

    WrapperFullMonteSW* clone() const;

    TetraMesh*          get_tetra_mesh() { return _mesh; }
    MaterialSet*        get_material_set() { return _materials; }

private: 
    TetraMesh*          _mesh;
    MaterialSet*        _materials = nullptr; 

    // FullMonte parameters
    float               _roulette_pr_win;
    float               _roulette_w_min;
    unsigned            _max_steps;
    unsigned            _max_hits;
    unsigned            _rand_seed;
    
    EnergyToFluence     _EVF;
    VTKMeshWriter*      _mesh_writer;

    TetraVolumeKernel*  _kernel; 
#if USE_CUDA
    TetraCUDAVolumeKernel*  _kernel_cuda; 
#endif

    string              _mesh_file;
    Parser*             _fparser;
    bool                _read_vtk;

    bool                _use_cuda;

    // This used for find enclosing tetra of a point 
    TetraLookupCache*   _tetraCache = nullptr;
    TetraEnclosingPointByLinearSearch*  _enclosing = nullptr;
};
#endif
