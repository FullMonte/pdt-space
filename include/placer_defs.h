/**
 * This file contains definitions of some parameters for the placer
 *
 *  @author Abdul-Amir Yassine
 *  @date_created June 7th, 2017
 */

#ifndef _PLACER_DEFS_H_
#define _PLACER_DEFS_H_


#include "boost/date_time/posix_time/posix_time.hpp"

#define VERSION 1

#define DOUBLE_ACCURACY 1e-7

#define SOURCE_POWER_MIN 0//1e-4

#endif
