/usr/local/cmake-3.17.2/bin/cmake -DCMAKE_CXX_COMPILER=/usr/bin/g++-5 \
     -DCMAKE_C_COMPILER=/usr/bin/gcc-5 \
     -DMOSEK_PREFIX=/usr/local/mosek-9.1.13/9.1/tools/platform/linux64x86 \
     -DVTK_DIR=/usr/local/lib/cmake/vtk-7.1 \
     -DFullMonteSW_DIR=/usr/local/FullMonteSW/include \
     -DCMAKE_BUILD_TYPE=Release \
     -DRUN_NIGHTLY=1 \
     .. \
     && make -j8 
