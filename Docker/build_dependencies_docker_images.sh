#!/bin/bash 
num_threads=${1:-24}

# mosek
docker build --build-arg NUM_THREADS=$num_threads -f ~/pdt-space/Docker/Dockerfile.mosek -t mosek-build:latest 
docker tag mosek-build:latest registry.gitlab.com/fullmonte/pdt-space/mosek-build:latest
docker push registry.gitlab.com/fullmonte/pdt-space/mosek-build:latest

# cmake
docker build --build-arg NUM_THREADS=$num_threads -f ~/pdt-space/Docker/Dockerfile.cmake -t cmake-build:latest 
docker tag cmake-build:latest registry.gitlab.com/fullmonte/pdt-space/cmake-build:latest
docker push registry.gitlab.com/fullmonte/pdt-space/cmake-build:latest

# vtk
docker build --build-arg NUM_THREADS=$num_threads -f ~/pdt-space/Docker/Dockerfile.vtk -t vtk-build:latest 
docker tag vtk-build:latest registry.gitlab.com/fullmonte/pdt-space/vtk-build:latest
docker push registry.gitlab.com/fullmonte/pdt-space/vtk-build:latest

# pdt-space dependencies
docker build --build-arg NUM_THREADS=$num_threads -f ~/pdt-space/Docker/Dockerfile.pdt-space-dependencies -t pdt-space-dependencies:latest 
docker tag pdt-space-dependencies:latest registry.gitlab.com/fullmonte/pdt-space/pdt-space-dependencies:latest
docker push registry.gitlab.com/fullmonte/pdt-space/pdt-space-dependencies:latest

# pdt-space 
docker build --build-arg NUM_THREADS=$num_threads --build-arg DATE_CAHCE="CURR_DATE" -f ~/pdt-space/Docker/Dockerfile.pdt-space -t pdt-space-dev:master 
# docker tag pdt-space-dev:master registry.gitlab.com/fullmonte/pdt-space/pdt-space-dev:master
# docker push registry.gitlab.com/fullmonte/pdt-space/pdt-space-dev:master

docker build --build-arg NUM_THREADS=$num_threads --build-arg DATE_CAHCE="CURR_DATE" -f ~/pdt-space/Docker/Dockerfile.pdt-space-run -t pdt-space-run:master 
docker tag pdt-space-run:master registry.gitlab.com/fullmonte/pdt-space/pdt-space-run:master
docker push registry.gitlab.com/fullmonte/pdt-space/pdt-space-run:master
docker image rm pdt-space-dev:latest
