ARG NUM_THREADS=24

#Install cmake 
FROM ubuntu:18.04 as cmake-build

ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update && apt-get install -y tzdata && \
    ln -fs /usr/share/zoneinfo/America/Toronto /etc/localtime && \
    dpkg-reconfigure --frontend noninteractive tzdata

ARG NUM_THREADS
RUN apt-get update -y && apt-get install -y g++ curl ninja-build make
RUN apt-get update -y && apt-get install -y bzip2 libbz2-dev zlib1g-dev 
RUN apt-get update -y && apt-get install -y libssl-dev

WORKDIR /src

# install cmake
RUN curl -L https://github.com/Kitware/CMake/releases/download/v3.17.2/cmake-3.17.2.tar.gz -o cmake.tar.gz 
RUN tar -xzf cmake.tar.gz

WORKDIR /src/cmake-3.17.2
RUN ./bootstrap --parallel=${NUM_THREADS} --prefix=/usr/local/cmake-3.17.2
RUN make -j${NUM_THREADS} && make install -j${NUM_THREADS}
