#!/bin/bash 
num_threads=${1:-24}

docker pull registry.gitlab.com/fullmonte/pdt-space/pdt-space-run:master
docker build --build-arg NUM_THREADS=$num_threads -f ~/pdt-space/Docker/Dockerfile.install -t registry.gitlab.com/fullmonte/pdt-space/pdt-space-run:master .
docker push registry.gitlab.com/fullmonte/pdt-space/pdt-space-run:master
