#!/bin/bash

if [ "$#" -ne 1 ]; then
	echo "Please provide the path to the directory to mount to docker image"
	
	exit 1
fi

TAG=master
REGISTRY=registry.gitlab.com
IMAGE=$REGISTRY/fullmonte/pdt-space/pdt-space-run:$TAG


# The local path to be mounted to /meshes in the container
MOUNTED_DRIVE_PATH=${1?No mount path given!} #/home/yassineab/PhD_Research/pdt_space_docker

# Pull the Docker image
docker pull $IMAGE

# Allow local access from docker group to X windows server
# (necessary on some hosts, not others - reason unknown)
xhost +local:docker


# Run Docker image
# --rm: Delete container on finish
# -t:   Provide terminal
# -i:   Interactive
# -e:   Set environment variable DISPLAY
# -v:   Mount host path into container <host-path>:<container path>
# --privileged: Allow container access to system sockets (for X)

docker run --rm -ti \
        -v $MOUNTED_DRIVE_PATH:/sims \
        -v /tmp/.X11-unix/X0:/tmp/.X11-unix/X0 \
        --privileged \
        -e DISPLAY=:0 \
        $IMAGE
