#include "pdt_plan.h"

#ifdef USE_INTERPOLATION
#include "interpolation_engine.h"
#endif

#include "gtest/gtest.h"

#include <cmath>

#ifdef USE_INTERPOLATION
GTEST_TEST(InteroplationTest, Integration1DPolyTest)
{
    std::function<double(double)> polynomial = [&](double t)->double {
        return std::pow(t, 5) - 4*std::pow(t, 4) + 3*std::pow(t, 2) + 1;
    };
    double l = -2.5, r = 4.3;

    for (unsigned num_pts = 1140; num_pts < 2000; num_pts += 20)
    {
        double actual = (std::pow(r,6) - std::pow(l, 6))/6 +
                        (std::pow(l,5) - std::pow(r, 5))*4.0/5 +
                        (std::pow(r,3) - std::pow(l, 3)) +
                        (r - l); 
        double midpoint_int = SP_IE::integrate_midpoint(polynomial, l, r, num_pts); 

        EXPECT_NEAR(actual, midpoint_int, 5e-5) << "Polynomial integration::Number of points = " << num_pts;
    }
}

GTEST_TEST(InteroplationTest, Integration1DSinusoidTest)
{
    std::function<double(double)> sinusoid = [&](double t)->double {
        return std::pow(cos(t), 2)*sin(t) - sin(t);
    };
    double l = -2.3*M_PI, r = 3.5*M_PI;

    for (unsigned num_pts = 580; num_pts < 2000; num_pts += 20)
    {
        double actual = (-1.0/3)*(std::pow(cos(r), 3) - std::pow(cos(l), 3)) +
                        (cos(r) - cos(l)); 
        double midpoint_int = SP_IE::integrate_midpoint(sinusoid, l, r, num_pts); 

        EXPECT_NEAR(actual, midpoint_int, 5e-5) << "Sinusoid integration::Number of points = " << num_pts;
            //" actual (" << 
              //  actual << ") and expected (" << midpoint_int << ") differ by more than 1e-6 (" << 
                //    abs(actual - midpoint_int) << ")" << std::endl; 
    }
}

GTEST_TEST(InteroplationTest, Integration3DPolyTest)
{
    std::function<Eigen::RowVectorXd(Eigen::RowVectorXd)> poly = [&](Eigen::RowVectorXd t)->Eigen::RowVectorXd {
        Eigen::RowVectorXd vec(3); 

        vec(0) = std::pow(t(0), 2) + 3*t(1) + std::pow(t(2), 3);
        vec(1) = 2*t(0) + std::pow(t(1), 2) + std::pow(t(2), 2);
        vec(2) = t(0) + t(2);

        return vec;
    };

    Eigen::RowVectorXd l(3), r(3);
    l(0) = -1.3;
    l(1) = 0.5;
    l(2) = 3.2;

    r(0) = 2.6;
    r(1) = 5.1;
    r(2) = 0.2;

    for (unsigned num_pts = 580; num_pts < 600; num_pts += 20)
    {
        Eigen::RowVectorXd actual(3);
        actual(0) = (1.0/3)*(std::pow(r(0), 3) - std::pow(l(0), 3)) + 
                    (3.0/2)*(std::pow(r(1), 2) - std::pow(l(1), 2)) +
                    (1.0/4)*(std::pow(r(2), 4) - std::pow(l(2), 4));
        actual(1) = (std::pow(r(0), 2) - std::pow(l(0), 2)) + 
                    (1.0/3)*(std::pow(r(1), 3) - std::pow(l(1), 3)) +
                    (1.0/3)*(std::pow(r(2), 3) - std::pow(l(2), 3));
        actual(2) = (1.0/2)*(std::pow(r(0), 2) - std::pow(l(0), 2)) + 
                    (1.0/2)*(std::pow(r(2), 2) - std::pow(l(2), 2));

        Eigen::RowVectorXd midpoint_int = SP_IE::integrate_midpoint(poly, l, r, num_pts); 
        
        cout << actual << endl;
        cout << midpoint_int << endl;

        EXPECT_LE((actual - midpoint_int).norm(), 5e-5) << "3D polynomial integration::Number of points = " << num_pts;
            //" actual (" << 
              //  actual << ") and expected (" << midpoint_int << ") differ by more than 1e-6 (" << 
                //    abs(actual - midpoint_int) << ")" << std::endl; 
    }
}
#endif
