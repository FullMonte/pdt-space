/**
 * @file: read_opt_file_test.cxx
 * @brief: unit test to test material metrics read from .opt files
 * Reads all 5 .opt files, reconstructs them and compares with the original file.
 * 
 *      @author: Shuran Wang
 *      @date: August 20, 2020
 */

#include "gtest/gtest.h"
#include "pdt_plan.h"
#include "read_mesh.h"
#include "test_common.h"

#define PARAMS_FILE "%s/unit-tests/input_params/test_params/opt_params/tumor1_opt_test_%u.xml"

#define NUM_TESTS 4
#define BUF_SIZE 255

const string OPTICAL_FILE[NUM_TESTS] = { 
                                         string(PDT_SPACE_SRC_DIR) + "/data/630nm.opt",
                                         string(PDT_SPACE_SRC_DIR) + "/data/635nm.opt",
                                         string(PDT_SPACE_SRC_DIR) + "/data/662nm.opt",
                                         string(PDT_SPACE_SRC_DIR) + "/data/675nm.opt" 
                                        };


vector <double> compute_material_metrics (mesh * data) {
    vector <material_opt*> materials = data->get_list_of_materials();

    vector <double> result;
    result.push_back(1);

    result.push_back(data->get_number_of_materials());

    for (unsigned int i = 0; i < data->get_number_of_materials(); i++) {
        result.push_back(materials[i]->get_mu_a());
        result.push_back(materials[i]->get_mu_s());
        result.push_back(materials[i]->get_g());
        result.push_back(materials[i]->get_n());
    }

    result.push_back(1);
    result.push_back(1.0);

    return result;
}

GTEST_TEST(FileReadTests, OptFileReadTest)
{
    for (unsigned int i = 1; i <= NUM_TESTS; i++) {
        // construct full path name
        char params_file[BUF_SIZE];
        snprintf(params_file, BUF_SIZE, PARAMS_FILE, PDT_SPACE_SRC_DIR, i);

        // read mesh file
        pdt_plan * plan = new pdt_plan(params_file);

        // set the parameters for plan to read the mesh
        plan->read_mesh();
        mesh * data = plan->get_mesh();

        // compute metrics
        vector <double> metrics = compute_material_metrics(data);
        
        // read pre-computed metrics
        vector <double> golden = readGoldenResult(OPTICAL_FILE[i-1]);

        // compares and checks if error is less than 0.001%
        compare_results (golden, metrics, 0.00001, "opt file");

        // free memory
        delete plan;
    }
}
