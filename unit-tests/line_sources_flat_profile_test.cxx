/**
 * @file: line_sources_flat_profile_test.cxx
 * @brief: unit tests to test PDT-SPACE with flat profile line sources.
 * Runs PDT-SPACE with flat profile line sources and compares the result with pre-computed golden results.
 * 
 *      @author: Shuran Wang
 *      @date: July 24, 2020
 */

#include "gtest/gtest.h"
#include "pdt_plan.h"
#include "test_common.h"

#include <vector>

#define FLAT_PROFILE_PARAMS_FILE "/unit-tests/input_params/tumor4_params_630nm_line_flat.xml"
#define FLAT_PROFILE_GOLDEN_V100 "/unit-tests/golden_results/line_sources_flat_v100.txt"
#define FLAT_PROFILE_GOLDEN_FLUENCE "/unit-tests/golden_results/line_sources_flat_final_fluence.txt"

GTEST_TEST(PowerAllocationTests, LineSourcesFlatProfileTest)
{
    /** Run PDT_SPACE */
    pdt_plan* plan = new pdt_plan(string(PDT_SPACE_SRC_DIR) + FLAT_PROFILE_PARAMS_FILE);

    vector<double> v_alpha = run_pdt_space(plan);

    // Get final fluence
    vector<double> final_fluence = plan->get_final_fluence();

    /** Compare Results */
    // Compute v100 average error
    vector<double> golden_v100 = readGoldenResult (string(PDT_SPACE_SRC_DIR) + FLAT_PROFILE_GOLDEN_V100);
    
    compare_results (golden_v100, v_alpha, 0.05, "v100");

    // Compute fluence average error
    vector<double> golden_fluence = readGoldenResult (string(PDT_SPACE_SRC_DIR) + FLAT_PROFILE_GOLDEN_FLUENCE);

    compare_results (golden_fluence, final_fluence, 0.05, "fluence");

    delete plan;
}
