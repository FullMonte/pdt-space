/**
 * @file: sa_regression_tests.cxx
 * @brief: Simulated Annealing engine unit tests.
 * 
 *      @author: Shuran Wang
 *      @date: July 24, 2020
 */

#include "gtest/gtest.h"
#include "pdt_plan.h"
#include "read_mesh.h"
#include "sa_engine.h"
#include "test_common.h"
#include <boost/format.hpp>
//#include "profiler.h"

#include <vector>

#include <cmath>
#include <random>

#define SA_REGRESSION_PARAMS_FILE "/unit-tests/input_params/sa_test_params/sa_tumor%1%_test_params.xml"
#define SA_GOLDEN_V100_FILE "/unit-tests/golden_results/sa_golden/sa_tumor%1%_v100.txt"

/** Run tumor 4 case with a few moves, make sure test finish without error. */
GTEST_TEST(SARegressionTests, SAFuncionalTest)
{
    // Run tumor4 testcase
    int tumor_ind = 4;
    string params_file = str(boost::format(SA_REGRESSION_PARAMS_FILE) % tumor_ind);

    /** Run PDT_SPACE */

    pdt_plan* plan = new pdt_plan(string(PDT_SPACE_SRC_DIR) + params_file);
    plan->vary_tumor_weight(false);
    plan->set_sa_early_termination();

    // double guardband = 0.1; // 0.1 is for 90% guardband on white and grey matter
    vector<double> thresholds = plan->get_thresholds_with_guardband();
    // compute original tetra data (before removing unnecessary tetra)
    plan->compute_original_tetra_data(thresholds); 
    plan->run_sa_engine();

    // Get final fluence
    vector<double> final_fluence = plan->get_final_fluence();

    delete plan;
}

/** Run tumor 4 case, evaluate run time and QoR. */
GTEST_TEST(SARegressionTests, SAPerformanceTest)
{
    if (RUN_NIGHTLY) {   // Only during nightly regression.
        mt19937 mt_random_generator(100); 
        uniform_int_distribution<int> rng_d(1, 9);

        int tumor_ind = 4; //rng_d(mt_random_generator);

        cout << "Running nightly for tumor " << tumor_ind << "...\n";
        string params_file = str(boost::format(SA_REGRESSION_PARAMS_FILE) % tumor_ind);
        string golden_file = str(boost::format(SA_GOLDEN_V100_FILE) % tumor_ind);

        /** Run annealer */
        pdt_plan* plan = new pdt_plan(string(PDT_SPACE_SRC_DIR) + params_file);

        // double guardband = 0.1; // 0.1 is for 90% guardband on white and grey matter
        vector<double> thresholds = plan->get_thresholds_with_guardband();

        // compute original tetra data (before removing unnecessary tetra)
        plan->compute_original_tetra_data(thresholds); 

        plan->get_diffuser_placement_and_dose_matrix();

        /** Print final source placement */
        vector<Src_Abstract*> placement = plan->get_initial_placement();
        for (auto line : placement)
            std::cout << line << std::endl;
    
        /** Run power variation */
        // remove unnecessary tetra that do not contribute to the cost
        plan->remove_unnecessary_tetra(thresholds);

        // compute tetra data such as the volumes, weights and individual thresholds
        plan->compute_tetra_data(thresholds);

        // setting p_max on sources
        plan->set_p_max_vector(_INFINITY_);

        // Solving the lp
        plan->initialize_lp();
        plan->solve_lp(thresholds);
    
        // Compute the final fluene
        plan->compute_post_opt_fluence();

        // Temporary: output profiler status
        //Util::print_profiler_stat();

        // Compute v_alpha
        double alpha = 1.0; // 1 means v100
        vector<double> v_alpha = plan->compute_v_alpha_tissues(thresholds, alpha);

        delete plan;

        // Read golden results
        vector<double> golden_v100 = readGoldenResult (string(PDT_SPACE_SRC_DIR) + golden_file);

        // Check v100s
        for (unsigned i = 0; i < v_alpha.size() - 1; i++) {
            // Non tumor: check v100 doesn't exceed 2x of golden v100
            EXPECT_LE(v_alpha[i], golden_v100[i]*2) << "Testing v100 values of non-tumor tissues to be less than 2x of golden v100, ind " << i << "\n";
        }
        // tumor: check v100 near 98%
        EXPECT_NEAR (v_alpha[4], 98, 0.1) << "Testing v100 of tumor to be near 98%.";
        
    } else {
        cout << "SA performance test disabled.\n";
    }
}

/** Test is_valid_injection function. */
GTEST_TEST(SAGeometryTests, SAIsValidInjectionTest) {
    unsigned num_tests = 100;

    std::mt19937 mt_random_generator(100); 
    std::uniform_real_distribution<float> rng_f(0.0f, 1000.f);

    string params_file = str(boost::format(SA_REGRESSION_PARAMS_FILE) % 4);

    /** Get annealer object */
    pdt_plan* plan = new pdt_plan(string(PDT_SPACE_SRC_DIR) + params_file);
    sa_engine* annealer = plan->get_annealer_for_testing();
    annealer->enable_injection_constraint();

    /** Test valid injecting conditions */
    SP_Point end0(89, 107, 100);
    SP_Point end1(89, 107, 70);
    SP_Line src(end0, end1);

    EXPECT_TRUE(annealer->is_valid_injection(src)) << "Testing is_valid_injection for source " << src;

    end0 = SP_Point(92, 105, 160);
    end1 = SP_Point(89, 107, 70);
    src = SP_Line(end0, end1);

    EXPECT_TRUE(annealer->is_valid_injection(src)) << "Testing is_valid_injection for source " << src;

    /** Test invalid injecting conditions */
    end0 = SP_Point(92, 105, 100);
    end1 = SP_Point(89, 107, 100);
    src = SP_Line(end0, end1);

    EXPECT_FALSE(annealer->is_valid_injection(src)) << "Testing is_valid_injection for source " << src;
    
    end0 = SP_Point(110, 90, 120);
    end1 = SP_Point(89, 107, 100);
    src = SP_Line(end0, end1);

    EXPECT_FALSE(annealer->is_valid_injection(src)) << "Testing is_valid_injection for source " << src;

    /** Randomly generate injecting conditions */
    for (unsigned i = 0; i < num_tests; i++) {
        SP_Point rand_end0(rng_f(mt_random_generator), rng_f(mt_random_generator), rng_f(mt_random_generator));
        SP_Point rand_end1(rng_f(mt_random_generator), rng_f(mt_random_generator), rng_f(mt_random_generator));
        SP_Line src(rand_end0, rand_end1);

        annealer->is_valid_injection(src);
    }

    delete plan;
}

/** Test get_rand_dist function. */
GTEST_TEST(SAGeometryTests, SAGetRandDistTest) {
    unsigned num_tests = 100;

    std::mt19937 mt_random_generator(100); 
    std::uniform_real_distribution<float> rng_f(0.0f, 1000.f);

    string params_file = str(boost::format(SA_REGRESSION_PARAMS_FILE) % 4);

    /** Get annealer object */
    pdt_plan* plan = new pdt_plan(string(PDT_SPACE_SRC_DIR) + params_file);
    sa_engine* annealer = plan->get_annealer_for_testing();
    annealer->enable_injection_constraint();

    /** Perpendicular line passing through center */
    SP_Point end0(89, 107, 100);
    SP_Point end1(89, 107, 70);
    SP_Line src(end0, end1);
    SP_Point dir(0, 1, 0);
    double rand_dist = 10000;
    annealer->get_rand_dist(src, dir, rand_dist);

    EXPECT_NEAR(rand_dist, 15, 0.005) << "Testing get_rand_dist for perpendicular line through center.";

    /** 45 degrees tilted line passing through center */
    end0 = SP_Point(89, 107, 174);
    end1 = SP_Point(89, 100, 167);
    src = SP_Line(end0, end1);
    dir = sp_normalize(SP_Point(0, -1, 1));
    rand_dist = 10000;
    annealer->get_rand_dist(src, dir, rand_dist);

    EXPECT_NEAR(rand_dist, 15/sqrt(2), 0.005) << "Testing get_rand_dist for 45 degrees line through center.";

    end0 = SP_Point(89, 107, 174);
    end1 = SP_Point(89, 100, 167);
    src = SP_Line(end0, end1);
    dir = sp_normalize(SP_Point(1, 0, 0));
    rand_dist = 10000;
    annealer->get_rand_dist(src, dir, rand_dist);

    EXPECT_NEAR(rand_dist, 15, 0.005) << "Testing get_rand_dist for 45 degrees line through center.";

    /** Randomly generate test cases */
    rand_dist = 10000;
    for (unsigned i = 0; i < num_tests; i++) {
        SP_Point rand_end0(rng_f(mt_random_generator), rng_f(mt_random_generator), rng_f(mt_random_generator));
        src = SP_Line(end0, end1);
        annealer->rand_rotate_source(src, rng_f(mt_random_generator)/10);
        dir = sp_normalize(src.get_rand_perpendicular_vector());
        annealer->get_rand_dist(src, dir, rand_dist);

        EXPECT_GE(rand_dist, 0) << "Testing get_rand_dist never returns negative values.";
    }

    delete plan;
}

/** Test rand_rotate_source function. */
GTEST_TEST(SAGeometryTests, SARandRotateSourceTest) {
    unsigned num_tests = 100;

    std::mt19937 mt_random_generator(100); 
    std::uniform_real_distribution<float> rng_f(0.0f, 1000.f);

    string params_file = str(boost::format(SA_REGRESSION_PARAMS_FILE) % 4);

    /** Get annealer object */
    pdt_plan* plan = new pdt_plan(string(PDT_SPACE_SRC_DIR) + params_file);
    sa_engine* annealer = plan->get_annealer_for_testing();
    annealer->enable_injection_constraint();

    /** Randomly generate end0 and length, check whether is valid constraint */
    for (unsigned i = 0; i < num_tests; i++) {
        SP_Point end0(rng_f(mt_random_generator), rng_f(mt_random_generator), rng_f(mt_random_generator));
        SP_Point end1(89, 100, 167);
        SP_Line src(end0, end1);
        annealer->rand_rotate_source(src, rng_f(mt_random_generator)/10);
        
        EXPECT_TRUE(annealer->is_valid_injection(src)) << "Testing rand_rotate_source for source " << src;
    }

    delete plan;
}
