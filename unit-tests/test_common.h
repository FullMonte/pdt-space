/**
 * Common functions for unit testing
 */

#ifndef TEST_COMMON_H
#define TEST_COMMON_H

#include "gtest/gtest.h"
#include "pdt_plan.h"
#include <cmath>

#include <vector>

/** Function to run pdt_space with the pdt_plan */
vector <double> run_pdt_space (pdt_plan* plan);

/** Read golden result from file */
vector <double> readGoldenResult (string filename);

/** Compare results */
void compare_results (vector<double> golden, vector<double> value, double absolute_range, string comp_type);

/** Compute metrics from read mesh file */
vector <double> compute_mesh_metrics (mesh* data);

#endif // TEST_COMMON_H
