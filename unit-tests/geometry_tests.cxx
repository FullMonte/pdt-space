#include "pdt_plan.h"
#include "read_mesh.h"

#include "test_common.h"
#include "gtest/gtest.h"

#include <cmath>
#include <random>

//! This test tests the functionality of generating a random vector that is orthogonal to another vector in any direction
GTEST_TEST(VectorGeometryTests, GenerateRandomPerpendicularVectorTest)
{
    unsigned num_tests = 100;

    std::mt19937 mt_random_generator(100); 
    
    std::uniform_real_distribution<float> rng_f(0.0f, 1000.f);

    for (unsigned i = 0; i < num_tests; i++)
    {
        // generate a random vector
        SP_Point rand_end0(rng_f(mt_random_generator), rng_f(mt_random_generator), rng_f(mt_random_generator));
        SP_Point rand_end1(rng_f(mt_random_generator), rng_f(mt_random_generator), rng_f(mt_random_generator));
        
        SP_Line rand_line(rand_end0, rand_end1); 

        // get the perpendicular vector
        SP_Point rand_perp = rand_line.get_rand_perpendicular_vector();

        double actual = sp_dot(rand_perp, rand_end1 - rand_end0);

        EXPECT_NEAR(actual, 0.0, 1e-4) << "Testing random perpendicular vectors. Iteration: " << i;
    }
}

GTEST_TEST(VectorGeometryTests, PerpendicularDistanceVectorTest)
{
    /** This test tests the functionality of calculating the perpendicular distance of a point from a line */
    unsigned num_tests = 100;
    srand(time(NULL));

    for (unsigned i = 0; i < num_tests; i++)
    {
        // generate a random vector to act as the point of projection of the point onto the line
        SP_Point rand_cross((1000.f*rand())/RAND_MAX, (1000.f*rand())/RAND_MAX, (1000.f*rand())/RAND_MAX);

        // generate a random point
        SP_Point rand_point((1000.f*rand())/RAND_MAX, (1000.f*rand())/RAND_MAX, (1000.f*rand())/RAND_MAX);

        // generate the line from the two points
        SP_Line rand_tmp_line(rand_cross, rand_point);
        SP_Point rand_tmp_vec = rand_tmp_line.get_rand_perpendicular_vector();
        SP_Line rand_line(rand_cross, rand_cross + rand_tmp_vec);

        // get the perpendicular distance
        double actual = rand_line.get_distance_pt_line(rand_point);
        double expected = rand_cross.get_distance_from(rand_point);

        // calculate error
        double err;
        if (actual >= DOUBLE_ACCURACY) {
            err = abs(actual - expected)/actual;
        } else if (expected >= DOUBLE_ACCURACY) {
            err = abs(actual - expected)/expected;
        } else err = 0.0;

        // check error
        EXPECT_NEAR(err, 0.0, 1e-4) << "Testing the perpendicular distance of a point from a line. Iteration: " << i;
    }
}

#define PARAM_FILE string(PDT_SPACE_SRC_DIR) + "/unit-tests/input_params/test_params/tumor1_geometry_params.xml"
#define GOLDEN_FILE string(PDT_SPACE_SRC_DIR) + "/unit-tests/golden_results/Colin27_tagged_tumor_centroids_1.txt"
GTEST_TEST(VectorGeometryTests, ComputeTetraCentroidTest)
{
    /**
     * This test tests the functionality of computing the centroid of a tetrahedron by reading 
     * the Colin27_tagged_tumor_1 mesh, computes and compares all centroids of all tetrahedra.
     */

    // read mesh file
    pdt_plan * plan = new pdt_plan(PARAM_FILE);

    // plan->set_parameters(MESH_FILE_PATH, DATA_NAME, OPTICAL_FILE, true, "");
    // plan->read_mesh();
    mesh * data = plan->get_mesh();

    vector<tetrahedron*> tetra_list = data->get_list_of_tetrahedra();

    // compute centroids of all tetrahedra
    vector<double> actual;
    for (unsigned i = 0; i < tetra_list.size(); i++) {
        SP_Point centroid = tetra_list[i]->compute_centroid_of();
        actual.push_back(centroid.get_xcoord());
        actual.push_back(centroid.get_ycoord());
        actual.push_back(centroid.get_zcoord());
    }

    // read golden file
    vector<double> expected = readGoldenResult(GOLDEN_FILE);

    // compares and checks if error is less than 0.001%
    compare_results (expected, actual, 0.00001, "centroid");
}

//! This test test the functionality of finding the shortest distance between two lines 
GTEST_TEST(VectorGeometryTests, ShortestDistanceLines)
{
    std::mt19937 mt_random_generator(100); 
    
    std::uniform_real_distribution<float> rng_f(-100.0f, 100.f);
    
    // 1- Parallel Lines
    SP_Line l1(SP_Point(125.0f, 73.0f, 50.0f), SP_Point(135.0f, 73.0f, 50.0f)); 
    SP_Line l2(SP_Point(122.0f, 73.0f, 60.0f), SP_Point(142.0f, 73.0f, 60.0f)); 

    double d = l1.get_distance_from(l2); 
    EXPECT_FLOAT_EQ(d, 10.0f) << "Testing distance between lines " << l1 << " and " << l2;

    // 2- Intersecting 
    l1 = SP_Line(SP_Point(-2.0f, 0.0f, 0.0f), SP_Point(1.0f, 0.0f, 0.0f));
    l2 = SP_Line(SP_Point(-1.0f, -1.0f, -1.0f), SP_Point(1.0f, 1.0f, 1.0f));

    EXPECT_FLOAT_EQ(l1.get_distance_from(l2), 0.0f) << "Testing distance between intersecting lines: " << l1 << " and " << l2;

    // 3- Non intersecting and non-parallel lines 
    l2 = SP_Line(SP_Point(-5.0f, 0.0f, 0.0f), SP_Point(1.0f, 5.0f, 5.0f)); 

    EXPECT_NEAR(l1.get_distance_from(l2), 2.28748, 1e-4) << "Testing distance between lines: " << l1 << " and " << l2;

    // 4- Run random inputs to make sure it does not crash
    unsigned num_tests = 100; 
    for (unsigned i = 0; i < num_tests; i++) 
    {
        float x1 = rng_f(mt_random_generator), x2 = rng_f(mt_random_generator);
        float x3 = rng_f(mt_random_generator), x4 = rng_f(mt_random_generator);
        float y1 = rng_f(mt_random_generator), y2 = rng_f(mt_random_generator); 
        float y3 = rng_f(mt_random_generator), y4 = rng_f(mt_random_generator); 
        float z1 = rng_f(mt_random_generator), z2 = rng_f(mt_random_generator); 
        float z3 = rng_f(mt_random_generator), z4 = rng_f(mt_random_generator); 

        l1 = SP_Line(SP_Point(x1,y1,z1), SP_Point(x2,y2,z2));
        l2 = SP_Line(SP_Point(x3,y3,z3), SP_Point(x4,y4,z4));
        
        double dist = l1.get_distance_from(l2); 
        ASSERT_TRUE(dist >= 0.0); 
    }
}

//! This test tests the functionality of getting the intersection of a line with a plane
GTEST_TEST(VectorGeometryTests, IntersectionWithPlaneTest) {
    srand(time(NULL));

    // parallel line with plane
    SP_Line l = SP_Line(SP_Point(1000.0f*rand()/RAND_MAX, 1000.0f*rand()/RAND_MAX, 1000.0f*rand()/RAND_MAX), SP_Point(1000.0f*rand()/RAND_MAX, 1000.0f*rand()/RAND_MAX, 1000.0f*rand()/RAND_MAX));
    SP_Point n = sp_normalize(l.get_rand_perpendicular_vector());
    SP_Point c = SP_Point(1000.0f*rand()/RAND_MAX, 1000.0f*rand()/RAND_MAX, 1000.0f*rand()/RAND_MAX);
    SP_Point res;

    bool success = l.get_intersection_with_plane(n, c, res);

    EXPECT_FALSE(success) << "Testing intersection of parallel line and plane.";

    // single intersection
    SP_Point dir = l.end0 - l.end1;
    do {
        n = sp_normalize(SP_Point(rand(), rand(), rand()));
    } while (sp_dot(n, dir) == 0);
    double dist = 1000.0 * rand()/RAND_MAX;
    dir = sp_normalize(dir);
    SP_Point expected_intersection = l.end0 + dist*dir;
    SP_Line normal = SP_Line(SP_Point(0.0f, 0.0f, 0.0f), n);
    dir = normal.get_rand_perpendicular_vector();
    c = expected_intersection + (rand() * 1000.0/RAND_MAX) * dir;
    success = l.get_intersection_with_plane(n, c, res);

    EXPECT_TRUE(success) << "Testing intersection of line and plane, function returned false.";
    EXPECT_NEAR(res.get_xcoord(), expected_intersection.get_xcoord(), 5e-3) << "Testing intersection of line and plane, x coordinate.";
    EXPECT_NEAR(res.get_ycoord(), expected_intersection.get_ycoord(), 5e-3) << "Testing intersection of line and plane, y coordinate.";
    EXPECT_NEAR(res.get_zcoord(), expected_intersection.get_zcoord(), 5e-3) << "Testing intersection of line and plane, z coordinate.";

    // Run random inputs to make sure it does not crash
    unsigned num_tests = 100; 
    for (unsigned i = 0; i < num_tests; i++) 
    {
        float x1 = 1000.0f*rand()/RAND_MAX, x2 = 1000.0f*rand()/RAND_MAX;
        float x3 = 1000.0f*rand()/RAND_MAX, x4 = 1000.0f*rand()/RAND_MAX;
        float y1 = 1000.0f*rand()/RAND_MAX, y2 = 1000.0f*rand()/RAND_MAX; 
        float y3 = 1000.0f*rand()/RAND_MAX, y4 = 1000.0f*rand()/RAND_MAX; 
        float z1 = 1000.0f*rand()/RAND_MAX, z2 = 1000.0f*rand()/RAND_MAX; 
        float z3 = 1000.0f*rand()/RAND_MAX, z4 = 1000.0f*rand()/RAND_MAX; 

        l = SP_Line(SP_Point(x1,y1,z1), SP_Point(x2,y2,z2));
        n = SP_Point(x3,y3,z3);
        c = SP_Point(x4,y4,z4);
        
        success = l.get_intersection_with_plane(n, c, res);
    }
}
