/** 
 * \author: Abed Yassine 
 * \date: July 29th, 2020
 * \file: util_tests.cxx
 *
 * \brief contains unit tests for functions in Util namespace 
 */

#include "gtest/gtest.h"

#include "util.h"

#include <vector>
#include <random>
#include <cmath>
#include <iostream>


/**
 * This test test the regression functionality under Util that returns the slope of a linear fit given (x,y) data points
 */
GTEST_TEST(UtilTests, SlopeRegression)
{
    unsigned num_tests = 100; 
    std::mt19937 mt_random_generator(100); 

    std::uniform_int_distribution<unsigned> rng_un(2, 100); 
    std::uniform_real_distribution<double> rng_d(0.0, 1.0); 
    std::uniform_real_distribution<double> rand_noise(-2.0, 2.0);

    for (unsigned i = 0; i < num_tests; i++)
    {
        unsigned rand_size = rng_un(mt_random_generator);
        double rand_slope = 100.0*rng_d(mt_random_generator) - 50.0; 
        double rand_intercept = 200.0*rng_d(mt_random_generator) - 200.0;
        vector<double> x(rand_size), y(rand_size); 
        for (unsigned j = 0; j < rand_size; j++) {
            x[j] = j + rand_noise(mt_random_generator); 
            y[j] = rand_slope*x[j] + rand_intercept; 
        }

        double actual = Util::compute_slope_regression(x, y); 

        EXPECT_NEAR(actual, rand_slope, 1e-2) << "Testing regression slope. Iteration: " << i;
    } 
}

/** 
 * This test tests the functionality of the tri-modal normal random distribution class
 */
GTEST_TEST(UtilTests, TriModalDistribution) {
    unsigned num_tests = 10000; 
    std::mt19937 mt_random_generator(100); 
    Util::multimodal_distribution<double> rng_multi(5.0, 1.0); // Center model has a mean value of 5 and sigma of 0.5

    std::vector<int> stars(10,0); 
    for (unsigned i = 0; i < num_tests; i++) {
        size_t temp = static_cast<size_t>(rng_multi(mt_random_generator));
        stars[temp]++;
    }
    
    vector<double> expected_percentages = {0.0, 1.0, 22.0, 20.0, 7.0, 7.0, 20.0, 22.0, 1.0, 0.0};
    for (unsigned i = 0; i < 10; i++) {
        int s = stars[i];
        double percentage = (s*1.0/num_tests)*100.0;
        double expected = expected_percentages[i];
        double tol = 3;

        EXPECT_TRUE(percentage >= expected - tol && percentage <= expected + tol);
    }
}
