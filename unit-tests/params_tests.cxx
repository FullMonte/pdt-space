/**
 * Includes tests related to different input parametes for PDT-SPACE.
 * 
 *      @author: Abed Yassine
 *      @date: August 10th, 2020
 */

#include "gtest/gtest.h"
#include "pdt_plan.h"
#include "test_common.h"

#include <vector>

#define PARAMS_FILES_DIR "/unit-tests/input_params/test_params/"

GTEST_TEST(ParamsTests, VaryTumorWeightTests)
{
    /** Run PDT_SPACE  with target tumor coverage of 95%*/
    pdt_plan* plan = new pdt_plan(string(PDT_SPACE_SRC_DIR) + PARAMS_FILES_DIR + "target_tumor_weight_95.xml");

    vector<double> v_alpha = run_pdt_space(plan);

    // Expecting tumor v100 to be around 95%
    EXPECT_NEAR(v_alpha[4], 95.0, 0.1) << "Testing 95\% tumor target coverage"; 

    delete plan;

    /** Run PDT_SPACE  with target tumor coverage of 98%*/
    plan = new pdt_plan(string(PDT_SPACE_SRC_DIR) + PARAMS_FILES_DIR + "target_tumor_weight_98.xml");

    v_alpha = run_pdt_space(plan);

    // Expecting tumor v100 to be around 98%
    EXPECT_NEAR(v_alpha[4], 98.0, 0.1) << "Testing 98\% tumor target coverage"; 
}
