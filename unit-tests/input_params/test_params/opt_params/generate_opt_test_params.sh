#!/bin/bash

wavelengths=(630
			 635
			 662
			 675)

test_idx=(0 1 2 3)

for t_idx in ${test_idx[@]}
do 
	wavelength=${wavelengths[${t_idx}]}
	i=$((${t_idx}+1))

	# create parameters file 
	file_name="tumor1_opt_test_${i}.op"
	# sa_plan_file="sa_tumor${i}_power_allocator_params.op"

	echo "RUN_TESTS = false" >> $file_name
	echo "OVERRIDE_FILE_PATH = true" >> $file_name
	# echo "RUN_TESTS = true" >> $sa_plan_file

	echo "PNF = 8e12" >> $file_name
	# echo "PNF = 8e12" >> $sa_plan_file

	echo "NUM_PACKETS = 1e6" >> $file_name 
	# echo "NUM_PACKETS = 1e5" >> $sa_plan_file

	echo "WAVELENGTH = ${wavelength}" >> $file_name
	# echo "WAVELENGTH = 635" >> $sa_plan_file

	echo "OPTICAL_FILE = /data/${wavelength}nm.opt" >> $file_name
	# echo "OPTICAL_FILE = /data/635nm.opt" >> $sa_plan_file

	echo "READ_VTK = true" >> $file_name
	# echo "READ_VTK = false" >> $sa_plan_file

	echo "source_type = line" >> $file_name
	# echo "source_type = line" >> $sa_plan_file

	echo "tumor_weight = 4000000" >> $file_name

	echo "placement_type = fixed" >> $file_name
	# echo "placement_type = sa" >> $sa_plan_file

	echo "DATA_DIR = /data/Colin27_tagged_tumor_1" >> $file_name
	# echo "DATA_DIR = /data/Colin27_tagged_tumor_${i}" >> $sa_plan_file

	echo "DATA_NAME = Colin27_tagged_tumor_1" >> $file_name
	# echo "DATA_NAME = Colin27_tagged_tumor_${i}" >> $sa_plan_file
	
	# echo "SA_PLACEMENT_FILE = /build/sa_files/tumor_${i}.txt" >> $file_name
	# echo "SA_PLACEMENT_FILE = /build/sa_files/tumor_${i}.txt" >> $sa_plan_file

	echo $tumor

	echo "INIT_PLACEMENT_FILE = /data/Line_sources/fixed_length/tumor1_sources_parallel_x.xml" >> $file_name
	# echo "INIT_PLACEMENT_FILE = /data/Line_sources/fixed_length/${tumor}.csv" >> $sa_plan_file

	# echo "tumor_weight = ${tumor_weights[${t_idx}]}" >> $sa_plan_file

done
