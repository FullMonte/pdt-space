#!/bin/bash

tumors=(tumor1_sources_parallel_x 
		tumor2_sources_parallel_y 
		tumor3_sources_parallel_y
		tumor4_sources_parallel_z
		tumor5_sources_parallel_y_z
		tumor6_sources_parallel_x 
		tumor7_sources_parallel_x 
		tumor8_sources_parallel_x
		tumor9_sources_parallel_x)

tumor_weights=(500 500 5625 50000000 300000 500 5000000 1500 707500) 

tumor_idx=(0 1 2 3 4 5 6 7 8)

lambdas=(0.3) #0.0 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1.0)

for lambda in ${lambdas[@]}
do
	#lambda=0.3
	beta=0.02

	for t_idx in ${tumor_idx[@]}
	do 
		tumor=${tumors[${t_idx}]}
		i=$((${t_idx}+1))

        # create parameters file 
        file_name="sa_tumor${i}_test_params.op"
        # sa_plan_file="sa_tumor${i}_power_allocator_params.op"

        echo "RUN_TESTS = false" >> $file_name
        echo "OVERRIDE_FILE_PATH = true" >> $file_name
        # echo "RUN_TESTS = true" >> $sa_plan_file

        echo "PNF = 8e12" >> $file_name
        # echo "PNF = 8e12" >> $sa_plan_file

        echo "NUM_PACKETS = 1e6" >> $file_name 
        # echo "NUM_PACKETS = 1e5" >> $sa_plan_file

        echo "WAVELENGTH = 635" >> $file_name
        # echo "WAVELENGTH = 635" >> $sa_plan_file

        echo "OPTICAL_FILE = /data/635nm.opt" >> $file_name
        # echo "OPTICAL_FILE = /data/635nm.opt" >> $sa_plan_file

        echo "READ_VTK = false" >> $file_name
        # echo "READ_VTK = false" >> $sa_plan_file

        echo "source_type = line" >> $file_name
        # echo "source_type = line" >> $sa_plan_file

        echo "tumor_weight = 4000000" >> $file_name

        echo "placement_type = sa" >> $file_name
        # echo "placement_type = sa" >> $sa_plan_file

        echo "SA_ENGINE_BETA = ${beta}" >> $file_name

        echo "SA_ENGINE_LAMBDA = ${lambda}" >> $file_name
        # echo "SA_ENGINE_LAMBDA = ${lambda}" >> $sa_plan_file

		echo "DATA_DIR = /data/Colin27_tagged_tumor_${i}" >> $file_name
		# echo "DATA_DIR = /data/Colin27_tagged_tumor_${i}" >> $sa_plan_file

		echo "DATA_NAME = Colin27_tagged_tumor_${i}" >> $file_name
		# echo "DATA_NAME = Colin27_tagged_tumor_${i}" >> $sa_plan_file
		
		# echo "SA_PLACEMENT_FILE = /build/sa_files/tumor_${i}.txt" >> $file_name
		# echo "SA_PLACEMENT_FILE = /build/sa_files/tumor_${i}.txt" >> $sa_plan_file

		echo $tumor

		echo "INIT_PLACEMENT_FILE = /data/Line_sources/fixed_length/${tumor}.xml" >> $file_name
		# echo "INIT_PLACEMENT_FILE = /data/Line_sources/fixed_length/${tumor}.csv" >> $sa_plan_file

		# echo "tumor_weight = ${tumor_weights[${t_idx}]}" >> $sa_plan_file

	done
done
