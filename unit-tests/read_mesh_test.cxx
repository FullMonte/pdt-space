/**
 * @file: read_mesh_test.cxx
 * @brief: unit tests to test mesh metrics read from .mesh and .vtk files
 * Each test reads 9 colin27 mesh files, reports and compares the:
 *   1. number of tetra
 *   2. number of points
 *   3. number of regions
 *   4. x_min x_max
 *   5. y_min y_max
 *   6. z_min z_max
 *   7. volumes of each tetra
 * 
 *      @author: Shuran Wang
 *      @date: August 1, 2020
 */

#include "gtest/gtest.h"
#include "pdt_plan.h"
#include "read_mesh.h"
#include "test_common.h"

#define PARAMS_FILE "%s/unit-tests/input_params/test_params/mesh_read_params/tumor%u_test_params.xml"
#define GOLDEN_FILE "%s/unit-tests/golden_results/mesh_metrics/Colin27_tagged_tumor_%u.txt"

#define NUM_TESTS 9
#define BUF_SIZE 255

GTEST_TEST(FileReadTests, MeshReadTest)
{
    for (unsigned int i = 1; i <= NUM_TESTS; i++) {
        // construct full path name
        char params_file[BUF_SIZE];
        snprintf(params_file, BUF_SIZE, PARAMS_FILE, PDT_SPACE_SRC_DIR, i);

        // read mesh file
        pdt_plan * plan = new pdt_plan(params_file);

        // set the parameters for plan to read the mesh
        plan->read_mesh();
        mesh * data = plan->get_mesh();

        // compute metrics
        vector <double> metrics = compute_mesh_metrics(data);
        
        // read pre-computed metrics
        char golden_file[BUF_SIZE];
        snprintf(golden_file, BUF_SIZE, GOLDEN_FILE, PDT_SPACE_SRC_DIR, i);
        vector <double> golden = readGoldenResult(golden_file);

        // compares and checks if error is less than 0.001%
        compare_results (golden, metrics, 0.00001, "mesh");

        // free memory
        delete plan;
    }
}

#define VTK_PARAMS_FILE "%s/unit-tests/input_params/test_params/mesh_read_params/tumor%u_vtk_test_params.xml"
GTEST_TEST(FileReadTests, VTKReadTest)
{
    for (unsigned int i = 1; i <= NUM_TESTS; i++) {
        // construct full path name
        char params_file[BUF_SIZE];
        snprintf(params_file, BUF_SIZE, VTK_PARAMS_FILE, PDT_SPACE_SRC_DIR, i);

        // read mesh file
        pdt_plan * plan = new pdt_plan(params_file);

        // set the parameters for plan to read the mesh
        plan->read_mesh();
        mesh * data = plan->get_mesh();

        // compute metrics
        vector <double> metrics = compute_mesh_metrics(data);
        
        // read pre-computed metrics
        char golden_file[BUF_SIZE];
        snprintf(golden_file, BUF_SIZE, GOLDEN_FILE, PDT_SPACE_SRC_DIR, i);
        vector <double> golden = readGoldenResult(golden_file);

        // compares and checks if error is less than 0.001%
        compare_results (golden, metrics, 0.00001, "vtk");

        // free memory
        delete plan;
    }
}
