#include "test_common.h"
#include "read_mesh.h"

#include "run_fullmonte.h"
#include "pdt_plan.h"

using namespace std;

//!
//! Runs PDT-SPACE with the pdt_plan.
//! @param plan: [in] the pdt_plan to run PDT-SPACE
//! @return: v100 values
//!
vector<double> run_pdt_space(pdt_plan* plan) {
    // Guardband the thresholds 
    // double guardband = 0.1; // 0.1 is for 90% guardband on white and grey matter
    vector<double> thresholds = plan->get_thresholds_with_guardband();

    // printing the dose thresholds 
    cout << endl << "Dose thresholds of the materials: " << endl;
    for (unsigned int i  = 0; i < thresholds.size(); i++)
        cout << "Material " << i << ": ====> " << thresholds[i] << endl;
    cout << endl;

    /*
    Material 0: ====> 2.25e-07
Material 1: ====> 2.25e-06
Material 2: ====> 3.6e-07
Material 3: ====> 2.925e-07
Material 4: ====> 2.5e-06

    */

    // compute original tetra data (before removing unnecessary tetra)
    plan->compute_original_tetra_data(thresholds); 

    // remove unnecessary tetra that do not contribute to the cost
    plan->remove_unnecessary_tetra(thresholds);

    // compute tetra data such as the volumes, weights and individual thresholds
    plan->compute_tetra_data(thresholds);

    // setting p_max on sources
    plan->set_p_max_vector(_INFINITY_);

	if (plan->get_placement_type() == "virtual" &&
		plan->get_source_type() == "point") // run source reduction algorithm
	{
		unsigned num_plans = 10; // number of different plans wanted (num sources goes from 1 to 10)

		vector<vector<Src_Abstract*>> reduced_plans = plan->run_virtual_source_reduction(num_plans, thresholds); 

		for (unsigned i = 0; i < num_plans; i++) {
			if (reduced_plans[i].size() == 10) // 10 is the number of point sources in the fixed placement for the tumor 4 test case
			{
				plan->run_fullmonte_multiple_sources(reduced_plans[i]); 

				plan->remove_unnecessary_tetra(thresholds); 
				plan->compute_tetra_data(thresholds);
				plan->set_p_max_vector(_INFINITY_); 

				cout << "Virtual plan used is: " << endl;
				for (unsigned j = 0; j < reduced_plans[i].size(); j++)
					cout << reduced_plans[i][j] << endl;
			}
		}
	}

    // Initialize the linear program (there is an option for a function to initialize the lp with a given number of sources)
    // The below function assumes that default number of sources read from the FullMonte output
    plan->initialize_lp();

    // Solving the lp
    plan->solve_lp(thresholds);
   
    // Compute the final fluene
    plan->compute_post_opt_fluence();

    // Compute v_alpha
    double alpha = 1.0; // 1 means v100
    vector<double> v_alpha = plan->compute_v_alpha_tissues(thresholds, alpha);

    return v_alpha;
}

//! Reads the golden results from the file into a vector of doubles
vector <double> readGoldenResult (string filename) {
    cout << "Reading from " << filename << endl;
    vector <double> result;

    ifstream fin(filename);

    EXPECT_FALSE (!fin) << "Cannot open file: " << filename;

    double read_value;
    while (fin >> read_value)
    {
        result.push_back(read_value);
    }

    fin.close();

    return result;
}

//!
//! Compares value with golden results.
//! @param golden: [in] Golden values.
//! @param value: [in] Values to be compared.
//! @param absolute_range: [in] Tolerance accepted for the error.
//! @param comp_type: [in] Name of value being compared.
//!
void compare_results (vector<double> golden, vector<double> value, double absolute_range, string comp_type) {
    ASSERT_EQ (value.size(), golden.size()) << "Size of " << comp_type << "files do not match.";
    double err = 0;
    for (unsigned int i = 0; i < value.size(); i++) {
        if (abs(golden[i]) >= DOUBLE_ACCURACY) {
            err += abs(value[i] - golden[i])/abs(golden[i]);
        } else if (abs(value[i]) >= DOUBLE_ACCURACY) {
            err += abs(value[i] - golden[i])/abs(value[i]);
        } // else error is 0
    }
    err /= value.size();

    EXPECT_NEAR(err, 0, absolute_range) << "Testing " << comp_type << " error to be less than " << absolute_range*100 << "%.";
}

vector <double> compute_mesh_metrics (mesh* data) {
    vector <double> result;

    // Compute number of tetra.
    result.push_back(data->get_mesh_size());

    // Compute number of points.
    result.push_back(data->get_number_of_points());

    // Computing number of regions.
    result.push_back(data->get_number_of_materials());

    // Compute min/max coordinates.
    float max_x = _NEG_INFINITY_;
    float max_y = _NEG_INFINITY_;
    float max_z = _NEG_INFINITY_;
    float min_x = _INFINITY_;
    float min_y = _INFINITY_;
    float min_z = _INFINITY_;
    for (unsigned int i = 0; i < data->get_list_of_points().size(); i++)
    {
        min_x = min(min_x, data->get_list_of_points()[i]->get_xcoord());
        min_y = min(min_y, data->get_list_of_points()[i]->get_ycoord());
        min_z = min(min_z, data->get_list_of_points()[i]->get_zcoord());
        max_x = max(max_x, data->get_list_of_points()[i]->get_xcoord());
        max_y = max(max_y, data->get_list_of_points()[i]->get_ycoord());
        max_z = max(max_z, data->get_list_of_points()[i]->get_zcoord());
    }
    result.push_back(min_x);
    result.push_back(max_x);
    result.push_back(min_y);
    result.push_back(max_y);
    result.push_back(min_z);
    result.push_back(max_z);

    // Compute volumes of each tetra
    for (unsigned int i = 0; i < data->get_list_of_tetrahedra().size(); i++)
    {
        result.push_back(data->get_list_of_tetrahedra()[i]->get_material_id());
        result.push_back(data->compute_tet_volume(data->get_list_of_tetrahedra()[i]));
    }

    return result;
}
