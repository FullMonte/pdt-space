mkdir -p Build/Release_Nightly
cd Build/Release_Nightly

cmake -DCMAKE_CXX_COMPILER=/usr/bin/g++-7 \
     -DCMAKE_C_COMPILER=/usr/bin/gcc-7 \
     -DMOSEK_PREFIX=/home/gitlab-runner/mosek/9.1/tools/platform/linux64x86 \
     -DVTK_DIR=/usr/local/lib/cmake/vtk-7.1 \
     -DFullMonteSW_DIR=/usr/local/FullMonteSW/include \
     -DCMAKE_BUILD_TYPE=Release \
     -DRUN_NIGHTLY=1 \
     -DUSE_CUDA=0 \
     ../.. \
     && make -j24 
