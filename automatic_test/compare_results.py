# This script is to compare the output of the test optimization case with golden output

from os import sys
import numpy as np

if len(sys.argv) < 3:
    print "Please supply the input data files to compare"
    sys.exit(1)


golden_result = np.loadtxt(sys.argv[1])
test_result = np.loadtxt(sys.argv[2])

if len(golden_result) != len(test_result):
    print "Lengths of arrays do not match"
    sys.exit(1)

err = np.absolute(golden_result - test_result)

for i in range(len(err)):
    if (golden_result[i] != 0):
        err[i] = err[i]/golden_result[i]
    elif (err[i] != 0):
        err[i] = err[i]/test_result[i]

avg_err = np.average(err)

avg_err = avg_err*100

if (avg_err < 5): # error is less than 5%
    print "Test passed"
else:
    print "Test failed between inputs {} and {}".format(sys.argv[1], sys.argv[2])
    print "Average error is: {:0.2f}%".format(avg_err)
    sys.exit(1)
