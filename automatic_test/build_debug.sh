mkdir -p Build/Debug
cd Build/Debug

cmake -DCMAKE_CXX_COMPILER=/usr/bin/g++-7 \
     -DCMAKE_C_COMPILER=/usr/bin/gcc-7 \
     -DMOSEK_PREFIX=/home/gitlab-runner/mosek/9.1/tools/platform/linux64x86 \
     -DVTK_DIR=/usr/local/lib/cmake/vtk-7.1 \
     -DFullMonteSW_DIR=/usr/local/FullMonteSW/include \
     -DCMAKE_BUILD_TYPE=Debug \
     -DUSE_CUDA=0 \
     ../.. \
     && make -j8 
