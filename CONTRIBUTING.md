Contributing to PDT-SPACE 
=========================

We love your input! When contributing to this repository, please first discuss the change you wish to make with the owners of this repository by submitting an issue on the Gitlab [issue tracker](https://gitlab.com/FullMonte/pdt-space/-/issues). When contributing, please make sure you satisfy the below guidelines in order to easily maintain the software. 

**Note:** Any contributions you make will be under the MIT Software License. This means that when you submit code changes, you submissions are understood to be under the same [MIT License](https://gitlab.com/FullMonte/pdt-space/-/blob/master/LICENSE) that covers the project. Feel free to contact the maintainers if that's a concern. 


# Documentation

## Source code

Documentation pertaining to the source code - what function/class does what, what a variable means - should be included in source
code comments. Use [Doxygen](http://www.stack.nl/~dimitri/doxygen/) where appropriate to document class members. Separate documentation for source code is to be avoided as it is difficult (read: unlikely) to be kept up to date.

More in-depth explanations such as design or testing rationale, or how to use tools, should be kept on the project 
[wiki](https://gitlab.com/FullMonte/pdt-space/-/wikis/home).




## CMake files

Important CMake variables that the user should provide should be made accessible as cache variables with appropriate documentation strings. Any host- or user-specific information (eg. paths to packages or files) should be held in variables. Never check in CMake files with system-specific paths! Paths relative to the source or build tree ``${CMAKE_SOURCE_DIR}`` or ``${CMAKE_BINARY_DIR}`` are acceptable.


## Separate text files

A few key items require relatively small separate text files (such as this one) which are tied to the software version.
Generally we want to avoid creating too many separate files so don't unless it's very important and has no other natural home (see
examples below).

[Markdown](https://docs.gitlab.com/ee/user/markdown/html) is preferred as it is compact, portable, searchable, and 
plays nice with Git revision control. Avoid screenshots or other images unless absolutely necessary to convey the point. In place of
screenshots, please copy-paste ``inline code and resulting text`` or

```
// multiple line
// inline code
```
and Gitlab will render it nicely.

In cases where images really make sense, please include them in your document using the Markdown syntax explained at the link above. 

If your document describes a series of commands to be run, it really should be recorded as a script.
In that case, consider creating a [snippet](https://gitlab.com/FullMonte/pdt-space/snippets) or
checking it in as a test or an example. Any host- or user-specific variables (usernames, paths, hostnames, etc) should be replaced
by variables for ease of maintanance and portability.



## Snippets

Self-contained scripts and information which are host- or application-specific are good candidates for a snippet.
If the functionality is of general importance (enough to want confidence that it works) and amenable to automatic testing, then it
should be made part of the test suite instead. For instance, if running the PDT-SPACE Docker container requires a particular command
on Windows, that would be a good snippet.



## Wiki

Longer discussions of what features are available, general descriptions of how to do things, and explanations of design/test rationale
belong on the wiki.


## Build system

The CMakeLists.txt file should be commented with any input variables that are required from the user.


# Data files

Data files (anything binary, or a text format more than a few lines of text) should be stored in the Git LFS (Large File Storage).
It keeps the repository smaller, makes for faster cleaner diffs (no 100,000+ line diffs), and permits more efficient cloning. 
If you have a new file type (extension) that is large that you'd like to add to the repository, use the `git lfs track` command described [here](https://github.com/git-lfs/git-lfs/wiki/Tutorial).
**Note:** The environment variable ``GIT_LFS_SKIP_SMUDGE`` will prevent downloading large data files during a git pull if it is set to ``1``.


# Testing

**Any new functionality or bug fix introduced should be tested automatically.** 

A test is an executable or script that is run, returning a status code zero for success or non-zero for failure.
It must be fully automatic, and should report a text error message on failure to indicate the reason.

We use the [GoogleTest](https://github.com/google/googletest) framework to add tests (See [Creating Tests Guidelines](https://gitlab.com/FullMonte/pdt-space/-/wikis/Creating-tests)). 

CMake will create a target `test` which runs all tests when invoked (eg. ``make test`` if using Make, ``ninja test`` if using Ninja).

**The CI pipeline should always run all tests, with the possibility of named exclusions**. This makes it harder for a test to be
missed accidentally since its simple existence in CMakeLists.txt means it will run unless specifically called out to be skipped


## Continuous Integration

Gitlab provides a Continuous Integration pipeline that automatically builds, tests, and (if successful) deploys a Docker image with
the software. 
It's controlled by ``.gitlab-ci.yml`` in the root of the source repo. 
Generally few changes should be needed since it runs all tests except for named exclusions. 
It would need to be changed primarily if the build options change.

You may direct Gitlab to skip running tests by including ``[ci skip]]`` in your commit message.
CI must run and pass before a merge request will be considered.


## Continuous Deployment via Docker

Working hand-in-hand with the CI pipeline, successful builds will be available to the users as a Docker image every day at midnight (EST).

# Style guidelines

## C++ code

 - One user-facing class per header file (helper classes not meant to be exposed may be defined inside this class)
 - No host-specific paths in the code, particularly for tests. Use ``#define`` variables provided by ``Config.h`` which is generated
 by the build system.


## CMakeLists.txt

 - Absolutely no host-specific paths in the CMakeLists.txt or related files.
 - Variables either documented at the top of the file, or for cache variables given an appropriate documentation string
 - Functions used where appropriate to eliminate repeated code
 - Ensure there is an install target for all programs, libraries, scripts, and data files meant for the end-user
 

# Git working practices

The ``master`` branch is protected so that only approved changes will be merged and appear.

## Start with an issue

[Always start with an issue](https://about.gitlab.com/2016/03/03/start-with-an-issue/) as this Gitlab blog post argues. 
It helps you keep focus on what should be fixed here and now vs. elsewhere and later.
It also means that a given merge to master will have a unifying theme.

## Work on a branch

Clone the repo, and start a branch off the current ``master``.

## Test your work locally

All tests should be added to GoogleTest working tree. 

## Push your branch and check CI status

As long as your changes don't require new build options or dependencies, the CI pipeline should be able to run unaltered.
Since it runs all tests except named exclusions, your tests will run automatically. 

## Rebase if needed

If the master branch has been updated since your work, you will need to rebase your branch onto the new master.
That may involve resolving conflicts and ensuring that all tests still pass.
The longer you wait before creating a merge request

## Create a merge request

Once your code meets the style guidelines above, the changes are tested, the CI pipeline passes, and the branch is based off the
current master you can [create a merge request](https://docs.gitlab.com/ee/gitlab-basics/add-merge-request.html) to have your code
integrated into master. It will be reviewed, and if the merge request is accepted your code will be merged.
In the message, please refer to the issue(s) that it addresses and note which test(s) validate that fact.
You should tag the appropriate issues in the MR so that they are automatically closed when the merge request is accepted (see Gitlab 
docs)

