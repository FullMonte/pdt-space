/**
 * \file main_pdt_plan.cxx \brief Main program Implementation after creating class pdt_plan
 *
 * This file shows an example of how to run the PDT-SPACE optimization tool
 * 
 *
 *
 *   @author: Abed Yassine
 *   @date: May 4th, 2018
 */

#include <unordered_set>
#include <unordered_map>

#include "read_mesh.h"
#include "src_placer.h"
#include "util.h"
#include "pdt_plan.h"
#include "profiler.h"
#include "run_fullmonte.h"
#include "light_source.h"

using namespace std;

// #define RUN_POWER_VARIATION 1

// Global variables
FILE* _matlab = NULL;
bool _print_matlab_file = false;
//! Helper function to print to matlab file
void print_sources_matlab_file (FILE* matlab, vector<Src_Abstract*> sources, string name) {
    // print sources
    vector<double> src_x_0 (sources.size());
    vector<double> src_y_0 (sources.size());
    vector<double> src_z_0 (sources.size());
    vector<double> src_x_1 (sources.size());
    vector<double> src_y_1 (sources.size());
    vector<double> src_z_1 (sources.size());
    vector<double> src_l (sources.size());
    bool is_cutend = false;
    for (unsigned i = 0; i < sources.size(); i++) {
        if (sources[i]->get_type() == Src_Abstract::Line) {
            src_x_0[i] = ((Src_Line*)sources[i])->end0.get_xcoord();
            src_y_0[i] = ((Src_Line*)sources[i])->end0.get_ycoord();
            src_z_0[i] = ((Src_Line*)sources[i])->end0.get_zcoord();
            src_x_1[i] = ((Src_Line*)sources[i])->end1.get_xcoord();
            src_y_1[i] = ((Src_Line*)sources[i])->end1.get_ycoord();
            src_z_1[i] = ((Src_Line*)sources[i])->end1.get_zcoord();
            src_l[i] = ((Src_Line*)sources[i])->get_src_length();
        } else if (sources[i]->get_type() == Src_Abstract::Point) {
            src_x_0[i] = ((Src_Line*)sources[i])->end0.get_xcoord();
            src_y_0[i] = ((Src_Line*)sources[i])->end0.get_ycoord();
            src_z_0[i] = ((Src_Line*)sources[i])->end0.get_zcoord();
            src_x_1[i] = ((Src_Line*)sources[i])->end0.get_xcoord();
            src_y_1[i] = ((Src_Line*)sources[i])->end0.get_ycoord();
            src_z_1[i] = ((Src_Line*)sources[i])->end0.get_zcoord();
            src_l[i] = 0;
        } else {
            is_cutend = true;
            cout << "Cut-end fibres not supported for position printing in matlab file.\n";
        }
    }
    if (!is_cutend) {
        Util::print_vector_matlab<double>(matlab, src_x_0, name + "_end0_x");
        Util::print_vector_matlab<double>(matlab, src_y_0, name + "_end0_y");
        Util::print_vector_matlab<double>(matlab, src_z_0, name + "_end0_z");
        Util::print_vector_matlab<double>(matlab, src_x_1, name + "_end1_x");
        Util::print_vector_matlab<double>(matlab, src_y_1, name + "_end1_y");
        Util::print_vector_matlab<double>(matlab, src_z_1, name + "_end1_z");
        Util::print_vector_matlab<double>(matlab, src_l, name + "_length");
        stringstream point_stream;
        point_stream << name << "_end0 = [" << name << "_end0_x " << name << "_end0_y " << name << "_end0_z];\n";
        fprintf(matlab, point_stream.str().c_str());
        point_stream.clear();
        point_stream << name << "_end1 = [" << name << "_end1_x " << name << "_end1_y " << name << "_end1_z];\n";
        fprintf(matlab, point_stream.str().c_str());
    }
}

// powers only
void print_matlab_file (FILE* matlab, pdt_plan* plan, vector<double> thresholds, vector<double> source_powers, 
                        vector<double> v_alpha, string name) {

    Util::print_vector_matlab<double>(matlab, source_powers, name + "_powers"); 
    Util::print_vector_matlab<double>(matlab, v_alpha, name + "_v100");
//         Util::print_vector_matlab<double>(matlab, plan->get_final_fluence(), "fluence");
//         Util::print_vector_matlab<unsigned int>(matlab, plan->get_original_tetra_material_ids(), "material_ids");
    stringstream dvh_declare;
    dvh_declare << name << "_dvh = zeros(500, " << v_alpha.size() << ");\n";
    fprintf(matlab, dvh_declare.str().c_str());
    int cnt = 1;
    for (double alpha_dvh = 0.01; alpha_dvh <= 5; alpha_dvh += 0.01)
    {
        vector<double> v_alphas = plan->compute_v_alpha_tissues(thresholds, alpha_dvh);

        stringstream arr_name;
        arr_name << name << "_dvh(" << cnt++ << ", :)"; 

        Util::print_vector_matlab<double>(matlab, v_alphas, arr_name.str());
    }
}

//! Helper function to run power optimization
void run_power_optimization (pdt_plan* plan, vector<double> thresholds) {
    // remove unnecessary tetra that do not contribute to the cost
    plan->remove_unnecessary_tetra(thresholds);

    // compute tetra data such as the volumes, weights and individual thresholds
    plan->compute_tetra_data(thresholds);

    // setting p_max on sources
    plan->set_p_max_vector(_INFINITY_);

    if (plan->get_placement_type() == "virtual" &&
        plan->get_source_type() == "point") // run source reduction algorithm
    {
        unsigned num_plans = 10; // number of different plans wanted (num sources goes from 1 to 10)

        vector<vector<Src_Abstract*>> reduced_plans = plan->run_virtual_source_reduction(num_plans, thresholds); 

        for (unsigned i = 1; i <= num_plans; i++) {
            if (reduced_plans[i].size() == 10) // 10 is the number of point sources in the fixed placement for the tumor 4 test case
            {
                plan->run_fullmonte_multiple_sources(reduced_plans[i]); 

                plan->remove_unnecessary_tetra(thresholds); 
                plan->compute_tetra_data(thresholds);
                plan->set_p_max_vector(_INFINITY_); 

                cout << "Virtual plan used is: " << endl;
                for (unsigned j = 0; j < reduced_plans[i].size(); j++)
                    cout << reduced_plans[i][j] << endl;
            }
        }
    }


    // Initialize the linear program (there is an option for a function to initialize the lp with a given number of sources)
    // The below function assumes that default number of sources read from the FullMonte output
    plan->initialize_lp();

    // Solving the lp
    plan->solve_lp(thresholds);

    // Compute the final fluene
    plan->compute_post_opt_fluence();
}

#ifdef RUN_POWER_VARIATION
void power_variation_problem (pdt_plan* plan, vector<double> thresholds) {
    double weight = 1.0;
    double alpha = 1.0;
    vector<double> non_guard_thresholds = plan->get_thresholds();

    // set beta to 5% of max source power
    double uncertainty = 0.05;

    // step 1: obtain v100_max and v100_min with setting power to +-2sigma
    vector<double> source_powers = plan->get_final_source_powers();
    vector<double> variation_source_powers = source_powers;
    vector<double> variation_fluence;
    cout << "Printing maximum source powers:\n";
    for (unsigned i = 0; i < variation_source_powers.size(); i++) {
        variation_source_powers[i] *= (1 + uncertainty);
        cout << "x[" << i << "] = " << variation_source_powers[i] << endl;
    }
    plan->set_final_source_powers(variation_source_powers);
    plan->compute_post_opt_fluence();

    vector<double> v_alpha = plan->compute_v_alpha_tissues(thresholds, alpha);
    
    std::cout << "V100s pre-op recovery" << std::endl;
    plan->print_v100s(v_alpha); 

    if (_print_matlab_file)
    {
        print_matlab_file(_matlab, plan, thresholds, variation_source_powers, v_alpha, "guardbanded_max");
    }

    // Compute non-guardbanded v_alpha
    v_alpha = plan->compute_v_alpha_tissues(non_guard_thresholds, alpha);
    
    std::cout << "\nV100s without guardband: " << std::endl;
    plan->print_v100s(v_alpha); 

    // output to vtk and/or MATLAB
    vector<float> source_powers_floats(variation_source_powers.size());
    for (unsigned i = 0; i < variation_source_powers.size(); i++)
        source_powers_floats[i] = static_cast<float>(variation_source_powers[i]);
    plan->run_fullmonte_composite_source(source_powers_floats);

    if (_print_matlab_file)
    {
        print_matlab_file(_matlab, plan, non_guard_thresholds, variation_source_powers, v_alpha, "no_guard_max");
    }

    cout << "Printing minimum source powers:\n";
    variation_source_powers = source_powers;
    for (unsigned i = 0; i < variation_source_powers.size(); i++) {
        variation_source_powers[i] *= (1 - uncertainty);
        cout << "x[" << i << "] = " << variation_source_powers[i] << endl;
    }
    plan->set_final_source_powers(variation_source_powers);
    plan->compute_post_opt_fluence();
    v_alpha = plan->compute_v_alpha_tissues(thresholds, alpha);
    
    std::cout << "V100s pre-op recovery" << std::endl;
    plan->print_v100s(v_alpha); 

    if (_print_matlab_file)
    {
        print_matlab_file(_matlab, plan, thresholds, variation_source_powers, v_alpha, "guardbanded_min");
    }

    // Compute non-guardbanded v_alpha
    v_alpha = plan->compute_v_alpha_tissues(non_guard_thresholds, alpha);
    
    std::cout << "\nV100s without guardband: " << std::endl;
    plan->print_v100s(v_alpha); 

    // output to vtk and/or MATLAB
    for (unsigned i = 0; i < variation_source_powers.size(); i++)
        source_powers_floats[i] = static_cast<float>(variation_source_powers[i]);
    plan->run_fullmonte_composite_source(source_powers_floats);

    if (_print_matlab_file)
    {
        print_matlab_file(_matlab, plan, non_guard_thresholds, variation_source_powers, v_alpha, "no_guard_min");
    }

    // step 2: 
    plan->power_variation_problem(weight, uncertainty, thresholds);
    // plan->solve_lp(thresholds);
    source_powers = plan->get_final_source_powers();

    cout << "Printing optimized source powers:\n";
    for (unsigned i = 0; i < source_powers.size(); i++) {
        cout << "x[" << i << "] = " << source_powers[i] << endl;
    }
    plan->compute_post_opt_fluence();

    v_alpha = plan->compute_v_alpha_tissues(thresholds, alpha);
    
    std::cout << "V100s pre-op recovery" << std::endl;
    plan->print_v100s(v_alpha); 

    if (_print_matlab_file)
    {
        print_matlab_file(_matlab, plan, thresholds, source_powers, v_alpha, "guardbanded_opt");
    }

    // Compute non-guardbanded v_alpha
    v_alpha = plan->compute_v_alpha_tissues(non_guard_thresholds, alpha);
    
    std::cout << "\nV100s without guardband: " << std::endl;
    plan->print_v100s(v_alpha); 

    // output to vtk and/or MATLAB
    for (unsigned i = 0; i < source_powers.size(); i++)
        source_powers_floats[i] = static_cast<float>(source_powers[i]);
    plan->run_fullmonte_composite_source(source_powers_floats);

    if (_print_matlab_file)
    {
        print_matlab_file(_matlab, plan, non_guard_thresholds, source_powers, v_alpha, "no_guard_opt");
    }

    cout << "Printing maximum source powers:\n";
    variation_source_powers = source_powers;
    for (unsigned i = 0; i < variation_source_powers.size(); i++) {
        variation_source_powers[i] *= (1 + uncertainty);
        cout << "x[" << i << "] = " << variation_source_powers[i] << endl;
    }
    plan->set_final_source_powers(variation_source_powers);
    plan->compute_post_opt_fluence();

    v_alpha = plan->compute_v_alpha_tissues(thresholds, alpha);
    
    std::cout << "V100s pre-op recovery" << std::endl;
    plan->print_v100s(v_alpha); 

    if (_print_matlab_file)
    {
        print_matlab_file(_matlab, plan, thresholds, variation_source_powers, v_alpha, "guardbanded_max_opt");
    }

    // Compute non-guardbanded v_alpha
    v_alpha = plan->compute_v_alpha_tissues(non_guard_thresholds, alpha);
    
    std::cout << "\nV100s without guardband: " << std::endl;
    plan->print_v100s(v_alpha); 

    // output to vtk and/or MATLAB
    for (unsigned i = 0; i < variation_source_powers.size(); i++)
        source_powers_floats[i] = static_cast<float>(variation_source_powers[i]);
    plan->run_fullmonte_composite_source(source_powers_floats);

    if (_print_matlab_file)
    {
        print_matlab_file(_matlab, plan, non_guard_thresholds, variation_source_powers, v_alpha, "no_guard_max_opt");
    }

    cout << "Printing minimum source powers:\n";
    variation_source_powers = source_powers;
    for (unsigned i = 0; i < variation_source_powers.size(); i++) {
        variation_source_powers[i] *= (1 - uncertainty);
        cout << "x[" << i << "] = " << variation_source_powers[i] << endl;
    }
    plan->set_final_source_powers(variation_source_powers);
    plan->compute_post_opt_fluence();

    v_alpha = plan->compute_v_alpha_tissues(thresholds, alpha);
    
    std::cout << "V100s pre-op recovery" << std::endl;
    plan->print_v100s(v_alpha); 

    if (_print_matlab_file)
    {
        print_matlab_file(_matlab, plan, thresholds, variation_source_powers, v_alpha, "guardbanded_min_opt");
    }

    // Compute non-guardbanded v_alpha
    v_alpha = plan->compute_v_alpha_tissues(non_guard_thresholds, alpha);
    
    std::cout << "\nV100s without guardband: " << std::endl;
    plan->print_v100s(v_alpha); 

    // output to vtk and/or MATLAB
    for (unsigned i = 0; i < variation_source_powers.size(); i++)
        source_powers_floats[i] = static_cast<float>(variation_source_powers[i]);
    plan->run_fullmonte_composite_source(source_powers_floats);

    if (_print_matlab_file)
    {
        print_matlab_file(_matlab, plan, non_guard_thresholds, variation_source_powers, v_alpha, "no_guard_min_opt");
    }
}
#endif

int main(int argc, char** argv)
{
	/******************************* Reading input parameters file *******************************/
    if (argc< 2)
    {
        fprintf(stderr, "\033[1;%dmNo parameters file supplied!\nExiting...\n\033[0m\n", 31);
		fprintf(stderr, "\033[1;%dmType %s <file_name> to run, or %s --list_params to see list of required parameters. \n\033[0m\n", 
					31, argv[0], argv[0]);
        exit(-1);
    }
    string params_file = argv[1];

    string output_file = "";
    if (argc > 2) {
        output_file = argv[2];
        _matlab = fopen(output_file.c_str(), "w");
        _print_matlab_file = true;
    }

    // creating an object of the pdt_plan class
    /*
     * This function reads the parameters file, read the mesh and the per tissue type death thresholds 
     * and then reads the output fluence matrix from FullMonte 
     */
    pdt_plan* plan = new pdt_plan(params_file);

    // Guardband the thresholds 
    // double guardband = 0.1; // 0.1 is for 90% guardband on white and grey matter
    vector<double> thresholds = plan->get_thresholds_with_guardband();
    vector<double> no_guard_thresholds = plan->get_thresholds();

    // printing the dose thresholds 
    cout << endl << "Dose thresholds of the materials: " << endl;
    for (unsigned int i  = 0; i < thresholds.size(); i++)
        cout << "Material " << i << ": ====> " << thresholds[i] << endl;
    cout << endl;

    // compute original tetra data (before removing unnecessary tetra)
    plan->compute_original_tetra_data(thresholds); 

	plan->get_diffuser_placement_and_dose_matrix(); 

    vector<vector<Src_Abstract*> > final_placement = plan->get_placement_results();

    // Run conservativae approach for OP variation
//        unsigned int num_op_cases = 1000;
//     plan->conservative_approach_op_variation(num_op_cases);
//     plan->percentile_approach_op_variation(plan->get_fullmonte_output_file(), num_op_cases, 1);
//        plan->cvar_optimization_op_problem(num_op_cases, 0.95);
    if (final_placement.size() == 1) {
        run_power_optimization(plan, thresholds);
        // Compute v_alpha
        double alpha = 1.0; // 1 means v100
        vector<double> v_alpha = plan->compute_v_alpha_tissues(thresholds, alpha);
        
        std::cout << "V100s pre-op recovery" << std::endl;
        plan->print_v100s(v_alpha); 

        // printing the final source powers 
        vector<double> source_powers = plan->get_final_source_powers();
        cout << "Printing source powers after optimization pre op recovery: " << endl;
        for (unsigned int i = 0; i < source_powers.size(); i ++)
            cout << "x[" << i << "] = " << fixed << setprecision(8) << source_powers[i] << endl;
        
        plan->run_rt_feedback();

        vector<float> source_powers_floats(source_powers.size());
        for (unsigned i = 0; i < source_powers.size(); i++)
            source_powers_floats[i] = static_cast<float>(source_powers[i]);
        plan->run_fullmonte_composite_source(source_powers_floats);

        if (_print_matlab_file)
        {
            print_sources_matlab_file(_matlab, final_placement[0], "guardbanded");
            print_matlab_file(_matlab, plan, thresholds, source_powers, v_alpha, "guardbanded");
        }

        // Compute non-guardbanded v_alpha
        v_alpha = plan->compute_v_alpha_tissues(no_guard_thresholds, alpha);
        
        std::cout << "\nV100s without guardband: " << std::endl;
        plan->print_v100s(v_alpha); 

        if (_print_matlab_file)
        {
            print_sources_matlab_file(_matlab, final_placement[0], "no_guardband");
            print_matlab_file(_matlab, plan, no_guard_thresholds, source_powers, v_alpha, "no_guardband");
        }

#ifdef RUN_POWER_VARIATION
        cout << "\nRun power variation.\n";
        power_variation_problem(plan, thresholds);
#endif
    } else if (final_placement.size() > 1) {
        // Ran SA placement with multiple injection points
        for (unsigned p = 0; p < final_placement.size(); p++) {
            cout << "Result for injection point " << p << endl;
            for(unsigned i = 0; i < final_placement[p].size(); i++) {
                cout << final_placement[p][i] << endl;
            }
            plan->set_initial_placement(final_placement[p]);
            plan->run_fullmonte_multiple_sources(final_placement[p]);

            // Compute v_alpha
            double alpha = 1.0; // 1 means v100
            vector<double> v_alpha = plan->compute_v_alpha_tissues(thresholds, alpha);
            
            std::cout << "V100s pre-op recovery" << std::endl;
            plan->print_v100s(v_alpha); 

            // printing the final source powers 
            vector<double> source_powers = plan->get_final_source_powers();
            cout << "Printing source powers after optimization pre op recovery: " << endl;
            for (unsigned int i = 0; i < source_powers.size(); i ++)
                cout << "x[" << i << "] = " << fixed << setprecision(8) << source_powers[i] << endl;
            
            plan->run_rt_feedback();

            vector<float> source_powers_floats(source_powers.size());
            for (unsigned i = 0; i < source_powers.size(); i++)
                source_powers_floats[i] = static_cast<float>(source_powers[i]);
            plan->run_fullmonte_composite_source(source_powers_floats);

            if (_print_matlab_file)
            {
                stringstream ss;
                ss << "guardbanded_inj_" << p;
                print_sources_matlab_file(_matlab, final_placement[p], ss.str());
                print_matlab_file(_matlab, plan, thresholds, source_powers, v_alpha, ss.str());
            }

            // Compute non-guardbanded v_alpha
            v_alpha = plan->compute_v_alpha_tissues(no_guard_thresholds, alpha);
            
            std::cout << "\nV100s without guardband: " << std::endl;
            plan->print_v100s(v_alpha); 

            if (_print_matlab_file)
            {
                stringstream ss;
                ss << "no_guardband_inj_" << p;
                print_sources_matlab_file(_matlab, final_placement[p], ss.str());
                print_matlab_file(_matlab, plan, no_guard_thresholds, source_powers, v_alpha, ss.str());
            }

#ifdef RUN_POWER_VARIATION
            cout << "\nRun power variation.\n";
            power_variation_problem(plan, thresholds);
#endif
        }
    } else {
        cout << "ERROR: No placement solution was obtained. Exiting...\n";
    }

    Util::print_profiler_stat();

    if (_matlab != NULL)
        fclose(_matlab);
    delete plan;
    return 0;
}
