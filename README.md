[![pipeline status](https://gitlab.com/fullmonte/pdt-space/badges/master/pipeline.svg)](https://gitlab.com/fullmonte/pdt-space/commits/master)
[![coverage report](https://gitlab.com/FullMonte/pdt-space/badges/master/coverage.svg)](https://gitlab.com/FullMonte/pdt-space/-/commits/master)


PDT-SPACE: A PDT Source Power Allocation with Convex optimization Engine
========================================================================

|[Overview](https://gitlab.com/FullMonte/pdt-space/-/wikis/home) |[Installation Guide](https://gitlab.com/FullMonte/pdt-space/-/wikis/Install) | [User Guide](https://gitlab.com/FullMonte/pdt-space/-/wikis/User-guide) |
|:---: |:---:|:---:|
---

Description:
------------
This repository contains a tool (written in C++) that builds an optimization engine for power allocation to a set of light sources
with fixed positions in interstitial Photodynamic Therapy (iPDT) planning. The tool finds the optimal power allocation that minimizes the damage to the 
organs-at-risk surrounding the tumor. The tool supports point light sources as well as line sources with both flat and custom 
emission profiles. The tool outputs the allocated powers to the set of sources, along with the volume of each tissue type at alpha% of its 
light dose threshold. The tool also has functionalities to look into the effect of optical property variation, and to automatically place the 
line sources using simulated annealing and reinforcement learning. 


Overall flow:
-------------
The overall flow of PDT-SPACE in its basic functionality is shown in the following flowchart:

```mermaid
flowchart TD
    
    subgraph Optimization
    F --> B((PDT-SPACE))
    B --> F((FullMonte))  
    end

    subgraph Inputs
    A(3D mesh of the<br>target tissues)  
    C(Tissue optical<br>properties)
    D(Tissue death PDT<br>dose thresholds) 
    E(Diffusers' initial<br>placement) 
    CC(Additional<br>constraints) 
    end

    subgraph Outputs
    G(Optimized<br>source powers)
    H(Optimized<br>diffuser placement)
    I(Final PDT<br>dose distribution)
    J(Dose-Volume<br>histograms)
    end
    Inputs --> Optimization
    Optimization-->Outputs
 ```

Installing, Building and Running:
--------------------------------
For instrunctions on how to install PDT-SPACE and run it, please refer to the [wiki](https://gitlab.com/FullMonte/pdt-space/-/wikis/Home). 
 
 


Citing the tool:
----------------
The following papers may be used as a general citation for PDT-SPACE software (bibtex format):
```
@article{yassine2018automatic,
         title={Automatic interstitial photodynamic therapy planning via convex optimization},
         author={Yassine, Abdul-Amir and Kingsford, William and Xu, Yiwen and Cassidy, Jeffrey and Lilge, Lothar and Betz, Vaughn},
         journal={Biomed. Opt. Express},
         volume={9},
         number={2},
         pages={898--920},
         year={2018},
         publisher={Optical Society of America}
}
```
```
@article{yassine2019optimizing,
  title={Optimizing interstitial photodynamic therapy with custom cylindrical diffusers},
  author={Yassine, Abdul-Amir and Lilge, Lothar and Betz, Vaughn},
  journal={Journal of Biophotonics},
  volume={12},
  number={1},
  pages={e201800153},
  year={2019},
  publisher={Wiley Online Library}
}
```
```
@inproceedings{yassine2019tolerating,
  title={Tolerating uncertainty: photodynamic therapy planning with optical property variation},
  author={Yassine, Abdul-Amir and Lilge, Lothar and Betz, Vaughn},
  booktitle={Optical Methods for Tumor Treatment and Detection: Mechanisms and Techniques in Photodynamic Therapy XXVIII},
  volume={10860},
  pages={108600B},
  year={2019},
  organization={International Society for Optics and Photonics}
}
```
Owners/Maintainers:
-------------------
- Name: Shuran Wang.
  Email: shuran.wang@mail.utoronto.ca
- Name: Abdul-Amir (Abed) Yassine.  
  Email: abed.yassine@mail.utoronto.ca

Active Contributors:
--------------------

Past Contributors:
------------------
- William Kingsford. 
