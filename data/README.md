Overview
--------
This directory contains all the data files (meshes, optical properties and source locations) used in all of PDT-SPACE papers (as of April 2021). All data files are stored using git large file storage (lfs), so you need to download them to view them. 

Directory Structure
-------------------
- **BonePDT_Case<case_number>**: Contain the .mesh and .vtk files along with the source locations and optical properties for the bone metastases case studies. Each case also contains tissue_properties_<wavelength>.txt files that store the dose thresholds (in J/mm^2) at different wavelengths for all tissues involved. The tissues are ordered as follows: 
    - Region ID 1: Spinal cord. 
    - Region ID 2: Metastases.
    - Region ID 3: Bone. 
    - Region ID 4: Muscle. 
    
     These meshes were constructed from MRI images for patients with spinal metastases taken from Sunnybrook Health Sciences Center in Ontario.

- **Colin27_tagged_tumor_<tumor_number>**: Contain the .mesh and .vtk files for all GBM models used in PDT papers. Each case also contains a TissueTypes.txt file that stores the region ID for each tetrahedron in the mesh, and tissue_properties_<wavelength>.txt files that stor the dose thresholds at differen wavelengths for all tissues involved. The tissues are ordered as follows:
    - Region ID 1: Skull. 
    - Region ID 2: CSF. 
    - Region ID 3: Grey Matter. 
    - Region ID 4: White Matter. 
    - Region ID 5: Tumor. 
    
     These meshes were constructed by taking GBM MRI images from [the cancer imaging archive](https://www.cancerimagingarchive.net/nbia-search/?CollectionCriteria=TCGA-GBM), segmenting them and then adding them to a brain atlas called Colin27 taken from [here](http://mcx.space/wiki/index.cgi?MMC/Colin27AtlasMesh). 

- **Line_sources**: Contains csv files for the parallel line source placements used in all Colin27_tagged_tumor_<tumor_number> models. 

- **Point_sources**: Contains csv files for the point source placements used in all Colin27_tagged_tumor_<tumor_number> models. 

- **<wavelength>nm.op**: Contains the optical properties (mu_a, mu_s, g and n) at different wavelengths for all tissues used in the Colin27_tagged_tumor_<tumor_number> models. 


How to use? 
-----------
Please refer to these wiki pages:
- [PDT-SPACE Input parameters and file formats](https://gitlab.com/FullMonte/pdt-space/-/wikis/User-guide). 
- [How to run PDT-SPACE](https://gitlab.com/FullMonte/pdt-space/-/wikis/Running-PDT-SPACE).
