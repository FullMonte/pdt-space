# This scripts reads the points from a csv file and creats an XML file init_sources section

import sys
from sys import argv
import csv

if (len(argv) < 3):
    print "Please, provide input csv file name and output xml filename"
    sys.exit(0)

file_name = argv[1]

xmlfile = argv[2]

xmlout = open(xmlfile, "w")


with open(file_name, 'rb') as csvfile:
    file_reader = csv.reader(csvfile, delimiter=',', quotechar='\"')
    next(file_reader, None) #skip header row

    row_count = sum(1 for row in file_reader) 
    #print ("num rows is: ", row_count)
    csvfile.seek(0)

    xmlout.write("<?xml version=\"1.0\"?>\n")
    xmlout.write("<sources>\n")
    xmlout.write("  <max_sources>" + str(row_count) + "</max_sources> <!--Required, max number of sources used-->\n")
    xmlout.write("  <sources_init> <!--Initial positions of sources, required for now-->\n")
    next(file_reader, None) #skip header row
    i = 1
    for row in file_reader:
        xmlout.write("    <source type=\"point\">\n")
        xmlout.write("      <endpoint>\n")
        xmlout.write("        <x>" + str(row[0]) + "</x> <y>" + str(row[1]) + "</y> <z>" + str(row[2]) + "</z>\n")
        xmlout.write("      </endpoint>\n")
        xmlout.write("    </source>\n")
        i += 1

xmlout.write("  </sources_init>\n")
xmlout.write("</sources>\n")

xmlout.close()
