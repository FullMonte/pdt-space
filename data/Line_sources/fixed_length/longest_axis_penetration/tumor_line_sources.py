# This scripts reads the points from a csv file and creats a VTK legacy file with all the lines connected

import sys
from sys import argv
import csv

if (len(argv) < 3):
    print "Please, provide input csv file name and output vtk filename"
    sys.exit(0)

file_name = argv[1]

vtkfile = argv[2]

vtkout = open(vtkfile, "w")

vtkout.write("""\
# vtk DataFile Version 4.0
vtk output
ASCII
DATASET POLYDATA
""")

with open(file_name, 'rb') as csvfile:
    file_reader = csv.reader(csvfile, delimiter=',', quotechar='\"')
    next(file_reader, None) #skip header row

    row_count = sum(1 for row in file_reader) 
    #print ("num rows is: ", row_count)
    csvfile.seek(0)

    vtkout.write("Points " + str(row_count) + " double\n")

    next(file_reader, None) #skip header row
    for row in file_reader:
        vtkout.write(str(row[0]) + " " + str(row[1]) + " " + str(row[2]) + "\n")


    num_lines = row_count/2

    vtkout.write("Lines " + str(num_lines) + " " + str(3*num_lines) + "\n")

    for i in range(num_lines):
        vtkout.write("2 " + str(2*i) + " " + str(2*i + 1) + "\n")

vtkout.close()
