/**
 * @author: Shuran Wang
 * @date: December 26, 2021
 *
 * \file file_parser.cxx \brief External file format wrapper implementation
 *
 * This file implements wrappers to external file format (XML) parsers
 */

#include <iostream>
#include <fstream>
#include "file_parser_old.h"
#include "converter_common.h"

using namespace std;

//! Default Constructor 
ParserOld::ParserOld() {
}

/**
 * Regular Constructor, reads data from data file
 * @param filename [in]: Path to the data file
 */
ParserOld::ParserOld(string filename) {
    if (read_from_xml(filename)) {
        fprintf(stderr, "\n\033[1;%dmSuccessfully read XML file.\033[0m\n", 35);
    }

    // TODO: add checks
}

/*
 * Regular Destructor, deletes sources
 */
ParserOld::~ParserOld() {}

//!
//! This method sets the XML file and builds data structures
//! @param filename [in]: Path to the data file
//! 
bool ParserOld::read_from_xml(string filename) {
    // use boost ptree to store xml information
    using boost::property_tree::ptree;
    ptree pt;

    // create istream to read from file
    ifstream f;
    f.open(filename);
    if (!f) {
        fprintf(stderr, "\033[1;%dmCannot open XML File to read. \033[0m\n", 37);
        return false;
    }

    // read from xml file into ptree
    read_xml(f, pt);
    f.close();

    // traverse ptree
    // sources
    // sources
    _sources.sources_init.clear();
    fprintf(stderr, "\033[1;%dmReading sources\033[0m\n", 36);
    BOOST_FOREACH(ptree::value_type const& v, pt.get_child("sources")) {
        if (v.first == "max_sources") {
            fprintf(stderr, "\033[1;%dmReading max_sources\033[0m\n", 36);
            _sources.max_sources = boost::lexical_cast<unsigned>(v.second.data());
        } else if (v.first == "sources_init") {
            fprintf(stderr, "\033[1;%dmReading sources_init\033[0m\n", 36);
            BOOST_FOREACH(ptree::value_type const& w, v.second) {
                if (w.first == "source") {
                    string type = w.second.get<string>("<xmlattr>.type", "");

                    if (type == "line") {
                        if (_sources.sources_init.size() == 0 || _sources.source_type == "point") 
                            _sources.source_type = "line";
                        else if (_sources.unified_type) {
                            if (_sources.source_type != "line")
                                _sources.unified_type = false;
                        }
                        read_line_source(w.second);
                    } else if (type == "point") {
                        if (_sources.sources_init.size() == 0) {
                            _sources.source_type = "point";
                        } else if (_sources.unified_type && _sources.source_type != "line") {
                            if (_sources.source_type != "point")
                                _sources.unified_type = false;
                        }
                        read_point_source(w.second);
                    } else if (type == "cut-end") {
                        if (_sources.sources_init.size() == 0) 
                            _sources.source_type = "cut-end";
                        else if (_sources.unified_type) {
                            if (_sources.source_type != "cut-end")
                                _sources.unified_type = false;
                        }
                        read_cutend_source(w.second);
                    } else {
                        fprintf(stderr, "\033[1;%dmNot supported source type. Exiting... \033[0m\n", 31);
                        exit(-1);
                    }
                } else if (w.first != "<xmlcomment>" && w.first != "<xmlattr>") {
                    fprintf(stderr, "\033[1;%dmInvalid tag in sources_init, should be source. \033[0m\n", 33);
                }
            }
        } else if (v.first != "<xmlcomment>" && v.first != "<xmlattr>") {
            fprintf(stderr, "\033[1;%dmInvalid tag in sources. \033[0m\n", 33);
        }
    }

    // injection_points
    // _placement_mode = PM_NONE;
    if (_run_sa) {
        BOOST_FOREACH(ptree::value_type const& u, pt.get_child("sa_options")) {
            // if (u.first == "placement_constraint") {
            //     if (u.second.data() == "none") _placement_mode = PM_NONE;
            //     else if (u.second.data() == "parallel") _placement_mode = PM_PARALLEL;
            //     else if (u.second.data() == "point") _placement_mode = PM_POINT;
            //     else {
            //         _placement_mode = PM_NONE;
            //         fprintf(stderr, "\033[1;%dmInvalid placement mode, defaulting to none. \033[0m\n", 37);
            //     }
            /*} else */if (u.first == "injection_points") {
                _constrain_injection_point = u.second.get<bool>("<xmlattr>.constraint", true);
                BOOST_FOREACH(ptree::value_type const& v, u.second) {
                    if (v.first == "region_id") {
                        _injection_points.region_id = boost::lexical_cast<int>(v.second.data());
                    } else if (v.first == "point") {
                        Parser_injection_point ip;
                        ip.id = -1;
                        ip.diameter = -1;
                        BOOST_FOREACH(ptree::value_type const& w, v.second) {
                            if (w.first == "id") {
                                ip.id = boost::lexical_cast<int>(w.second.data());
                            } else if (w.first == "diameter") {
                                ip.diameter = boost::lexical_cast<float>(w.second.data());
                            } else if (w.first == "normal") {
                                ip.normal.x = w.second.get<float>("x");
                                ip.normal.y = w.second.get<float>("y");
                                ip.normal.z = w.second.get<float>("z");
                            } else if (w.first == "center") {
                                ip.center.x = w.second.get<float>("x");
                                ip.center.y = w.second.get<float>("y");
                                ip.center.z = w.second.get<float>("z");
                            } else if (w.first != "<xmlcomment>") {
                                fprintf(stderr, "\033[1;%dmInvalid tag in point. \033[0m\n", 33);
                            }
                        }
                        if (ip.id < 0 || ip.diameter < 0) {
                            fprintf(stderr, "\033[1;%dmInvalid injection point. \033[0m\n", 31);
                            return false;
                        }
                        _injection_points.injection_points.push_back(ip);
                    } else if (v.first != "<xmlcomment>" && v.first != "<xmlattr>") {
                        fprintf(stderr, "\033[1;%dmInvalid tag in injection_points. \033[0m\n", 33);
                    }
                }

                if (_injection_points.region_id < 0) {
                    fprintf(stderr, "\033[1;%dmMissing injection point region id. \033[0m\n", 31);
                    return false;
                }
            } else if (u.first == "critical_tissues") {
                stringstream s (u.second.data());
                _critical_regions.clear();
                unsigned region;
                while (s >> region) {
                    _critical_regions.insert(region);
                }

                cout << "Read SA critical regions: ";
                for (auto const &elem: _critical_regions)
                    cout << elem << " ";
                cout << endl;
            } else if (u.first == "max_src_distance") {
                _max_src_dist = boost::lexical_cast<double>(u.second.data());
                cout << "Read maximum source spacing " << _max_src_dist << " mm.\n";
            } else if (u.first == "min_src_distance") {
                _min_src_dist = boost::lexical_cast<double>(u.second.data());
                cout << "Read minimum source spacing " << _min_src_dist << " mm.\n";
            } else if (u.first == "distance_to_boundary") {
                _dist_to_bound = boost::lexical_cast<double>(u.second.data());
                cout << "Read minimum source distance to tumor boundaries " << _dist_to_bound << " mm.\n";
            } else if (u.first != "<xmlcomment>") {
                fprintf(stderr, "\033[1;%dmInvalid tag in sa_options. \033[0m\n", 37);
            }
        }
    }

    return true;
}

// void Parser::write_to_init_placement_csv(string filename) {
//     ofstream f (filename);

//     cout << "Writing placement to csv file " << filename << endl;

//     f << "\"Points:0\",\"Points:1\",\"Points:2\"\n";
    
//     for (unsigned i = 0; i < _sources.sources_init.size(); i++) {
//         f << _sources.sources_init[i].endpoint[0].get_xcoord() << "," << _sources.sources_init[i].endpoint[0].get_ycoord() << "," << _sources.sources_init[i].endpoint[0].get_zcoord() << endl;
//         f << _sources.sources_init[i].endpoint[1].get_xcoord() << "," << _sources.sources_init[i].endpoint[1].get_ycoord() << "," << _sources.sources_init[i].endpoint[1].get_zcoord() << endl;
//     }

//     f << endl;
//     f.close();
// }

//! Getter functions for injection points
Parser_injection_points ParserOld::get_injection_points() {
    return _injection_points;
}

void ParserOld::get_injection_point_options(bool &constrain/*, PlacementMode &mode*/) {
    constrain = _constrain_injection_point;
    // mode = _placement_mode;
}

//! Getter function for sources called by pdt_plan
void ParserOld::get_sources(Parser_sources &sources) {
    sources = _sources;
}


void ParserOld::read_line_source(const boost::property_tree::ptree& token) {
    using boost::property_tree::ptree;
    string location; // location for endpoint0
    vector<Parser_point> endpoints;
    double length = _INFINITY_;
    BOOST_FOREACH(ptree::value_type const& e, token) {
        if (e.first == "endpoint") {
            // create endpoint
            Parser_point endpoint;
            endpoint.x = e.second.get<float>("x");
            endpoint.y = e.second.get<float>("y");
            endpoint.z = e.second.get<float>("z");

            if (endpoints.size() == 0) { // first endpoint
                location = e.second.get<string>("<xmlattr>.location", "proximal");
                endpoints.push_back(endpoint);

                // cout << "Pushing proximal endpoint " << endpoint << endl;
            } else {
                if (location == "proximal") {
                    endpoints.push_back(endpoint);
                    // cout << "Pushing distal endpoint " << endpoint << endl;
                } else if (location == "distal") {
                    endpoints.emplace(endpoints.begin(), endpoint);
                    // cout << "Pushing proximal endpoint " << endpoint << endl;
                } else {
                    fprintf(stderr, "\033[1;%dmInvalid attribute for endpoint, should be proximal or distal. \033[0m\n", 33);
                }
            }
        } else if (e.first == "length") {
            // set length
            length = boost::lexical_cast<double>(e.second.data());
        } else if (e.first != "<xmlcomment>" && e.first != "<xmlattr>") {
            fprintf(stderr, "\033[1;%dmInvalid tag in source, should be endpoint. Exiting... \033[0m\n", 31);
            exit(-1);
        }
    }

    if (endpoints.size() != 2) {
        fprintf(stderr, "\033[1;%dmLine sources should have 2 endpoints, entered %lu. Exiting... \033[0m\n", 31, endpoints.size());
        exit(-1);
    }

    // cout << "endpoints[0] " << endpoints[0] << " endpoints[1] " << endpoints[1] << endl;
    if (length == _INFINITY_) length = get_distance_from(endpoints[0], endpoints[1]);
    Parser_source s;
    s.end0 = endpoints[0];
    s.end1 = endpoints[1];
    s.length = length;
    s.type = "line";
    if (abs(length) < 1e-6) {
        s.type = "point";
    }

    _sources.sources_init.push_back(s);
}

void ParserOld::read_point_source(const boost::property_tree::ptree& token) {
    using boost::property_tree::ptree;
    vector<Parser_point> endpoints;
    BOOST_FOREACH(ptree::value_type const& e, token) {
        if (e.first == "endpoint") {
            // create endpoint
            Parser_point endpoint;
            endpoint.x = e.second.get<float>("x");
            endpoint.y = e.second.get<float>("y");
            endpoint.z = e.second.get<float>("z");

            if (endpoints.size() == 0) { // first endpoint
                endpoints.push_back(endpoint);
            } else {
                fprintf(stderr, "\033[1;%dmPoint source should only have 1 endpoint. Exiting... \033[0m\n", 31);
                exit(-1);
            }
        } else if (e.first != "<xmlcomment>" && e.first != "<xmlattr>") {
            fprintf(stderr, "\033[1;%dmInvalid tag in source. Exiting... \033[0m\n", 31);
            exit(-1);
        }
    }

    Parser_source s;
    s.end0 = endpoints[0];
    s.length = 0;
    s.type = "point";

    _sources.sources_init.push_back(s);
}

//!
//! This method reads the fullmonte parameters specified in xml file
//! @param token [in]: string value of the <source> tag
//! 
void ParserOld::read_cutend_source(const boost::property_tree::ptree& token) {
    vector<Parser_point> endpoints;
    float r = 0, na= 0;
    Parser_point dir;
    bool set_r = false;
    bool set_na = false;
    bool set_dir = false;
    bool set_endpoint = false;
    using boost::property_tree::ptree;
    BOOST_FOREACH(ptree::value_type const& e, token) {
        if (e.first == "endpoint") {
            // create endpoint
            Parser_point endpoint;
            endpoint.x = e.second.get<float>("x");
            endpoint.y = e.second.get<float>("y");
            endpoint.z = e.second.get<float>("z");

            if (endpoints.size() == 0) { // first endpoint
                endpoints.push_back(endpoint);
            } else {
                fprintf(stderr, "\033[1;%dmCut-end source should only have 1 endpoint. Exiting... \033[0m\n", 31);
                exit(-1);
            }
            set_endpoint = true;
        } else if (e.first == "radius") {
            r = boost::lexical_cast<float>(e.second.data());
            set_r = true;
        } else if (e.first == "na") {
            na = boost::lexical_cast<float>(e.second.data());
            set_na = true;
        } else if (e.first == "direction") {
            // create endpoint
            dir.x = e.second.get<float>("x");
            dir.y = e.second.get<float>("y");
            dir.z = e.second.get<float>("z");
            set_dir = true;
        } else if (e.first != "<xmlcomment>" && e.first != "<xmlattr>") {
            fprintf(stderr, "\033[1;%dmInvalid tag in source. Exiting... \033[0m\n", 31);
            exit(-1);
        }
    }

    if (!(set_dir && set_endpoint && set_na && set_r)) {
        fprintf(stderr, "\033[1;%dmCutEnd source not entered properly. Exiting... \033[0m\n", 31);
        exit(-1);
    }
    Parser_source s;
    s.end0 = endpoints[0];
    s.end1 = dir;
    s.length = 0;
    s.type = "cut-end";
    s.r = r;
    s.na = na;

    _sources.sources_init.push_back(s);
}

//! Method to compute the distance from another point 
double get_distance_from(const Parser_point& p1, const Parser_point& p2) 
{
    return sqrt(pow(p2.x - p1.x, 2) + pow(p2.y - p1.y, 2) + 
                pow(p2.z - p1.z, 2));
}