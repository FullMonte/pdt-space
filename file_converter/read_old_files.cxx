#include "converter_common.h"
#include "read_old_files.h"

#include "pdt_space_config.h"

unordered_map<string, string>  _params_to_values_map;

string _data_name;
string _data_dir_path;
string _optical_prop_file;
string _fullmonte_output_file;
string _final_distribution_output_file;
bool _file_path_override;
bool _read_data_from_vtk;
string _mesh_file;
bool _running_tests;

// FM parameters
double _num_packets;
double _total_energy;
string _wavelength;
string _wavelength_read;
bool _use_cuda;

// conv opt parameters
string _placement_type;
double _tumor_weight;
bool _vary_tumor_weight;
double _target_tumor_v100;

// sources
string _source_type;
bool _source_tailored;
Parser_sources _init_placement;

// materials
vector<material_opt*> _materials;
unsigned _number_of_materials;
double _index_of_refraction;

vector<double> _d_max_tissues;
vector<double> _d_min_tissues;
unordered_set<unsigned> _tumor_region_number; 
unordered_map<unsigned, string> _tissue_names_ids; 
vector<double> _guardband_tissues;

// sa parameters
bool		 _run_sa;
double    _sa_engine_beta;
double	 _sa_engine_lambda;
string	 _sa_engine_init_placement_file; 
string	 _sa_engine_progress_placement_file; 
double    _sa_engine_moves_per_temp;
Parser_injection_points _injection_points;
bool _constrain_injection_point;
unordered_set<unsigned> _critical_regions;

// rt feedback
bool _run_rt_feedback;
int       _rt_feedback_rand_seed;
float     _rt_feedback_detector_radius;
float     _rt_feedback_detector_na;
unsigned  _rt_feedback_lut_size;
string    _rt_feedback_lut_file_name;


void read_initial_placement() {
    ParserOld fparser((_file_path_override ? (string(PDT_SPACE_SRC_DIR) + _sa_engine_init_placement_file) : _sa_engine_init_placement_file));

    _critical_regions = fparser.get_critical_regions();
    _injection_points = fparser.get_injection_points();
    fparser.get_injection_point_options(_constrain_injection_point);
     
    fparser.get_sources(_init_placement);
}

//! This method reads the parameters file and initializes the corresponding parameters map
void read_params_file(string params_file)
{
    _params_to_values_map = read_parameters_file(params_file);

    // Reading if data is read from the vtk file
    string read_vtk = _params_to_values_map["READ_VTK"];
    transform(read_vtk.begin(), read_vtk.end(), read_vtk.begin(), ::tolower);
    if (read_vtk == "true") 
        _read_data_from_vtk = true;
    else 
        _read_data_from_vtk = false;

    string run_tests = _params_to_values_map["RUN_TESTS"];
    transform(run_tests.begin(), run_tests.end(), run_tests.begin(), ::tolower);
    if (run_tests == "true") 
        _running_tests = true;
    else 
        _running_tests = false;

    // Setting the wavelength
    _wavelength_read = _params_to_values_map["WAVELENGTH"];

    for (char const & c : _wavelength_read) {
        if (std::isdigit(c) == 0) {
            fprintf(stderr, "\033[1;%dmInvalid wavelength: %s. Exiting...\033[0m\n", 31, _wavelength_read.c_str());
            exit(-1);
        }
    }
    stringstream wavelength_ss;
    wavelength_ss << _wavelength_read << "nm";
    _wavelength = wavelength_ss.str();

    // Reading the source and placement types
    _source_type = _params_to_values_map["SOURCE_TYPE"];
    transform(_source_type.begin(), _source_type.end(), _source_type.begin(), ::tolower);
    if (_source_type != "point" && _source_type != "line" && _source_type != "tailored" && _source_type != "cut-end") {
        
        fprintf(stderr, "\033[1;%dmUnsupported source type: %s. Exiting...\033[0m\n", 31, _source_type.c_str());
        exit(-1);
    }

    _placement_type = _params_to_values_map["PLACEMENT_TYPE"]; // tells whether we are using fixed or virtual sources
    transform(_placement_type.begin(), _placement_type.end(), _placement_type.begin(), ::tolower);
    if (_placement_type != "fixed" && _placement_type != "virtual" && _placement_type != "sa") {
    
        fprintf(stderr, "\033[1;%dmUnsupported placement type: %s. Exiting...\033[0m\n", 31, _placement_type.c_str());
        exit(-1);
    }

    // check if tailored
    if (_source_type == "tailored") { 
        _source_type = "line"; 
        _source_tailored = true; 
    }
    else {
        _source_tailored = false;
    }

    // TODO: check if empty 
    _data_name = _params_to_values_map["DATA_NAME"];
    _data_dir_path = _params_to_values_map["DATA_DIR"];
	_optical_prop_file = _params_to_values_map["OPTICAL_FILE"]; 

    // TODO: check if empty 
    _fullmonte_output_file = _params_to_values_map.find("FULLMONTE_OUTPUT_FILE") != _params_to_values_map.end() ?
                        _params_to_values_map["FULLMONTE_OUTPUT_FILE"] : "";
    
    // TODO: check if empty 
    _final_distribution_output_file = _params_to_values_map.find("FINAL_DIST_FILE") != _params_to_values_map.end() ?
                        _params_to_values_map["FINAL_DIST_FILE"] : "";
   
    // TODO: check if these are not numbers 
    _num_packets = atof(_params_to_values_map["NUM_PACKETS"].c_str());
    if (_params_to_values_map.find("PNF") != _params_to_values_map.end()) {
        _total_energy = atof(_params_to_values_map["PNF"].c_str());
    } else {
        _total_energy = 4e11;
    }
    _tumor_weight = atof(_params_to_values_map["TUMOR_WEIGHT"].c_str());

    // TODO: check if these are numbers
    if (_params_to_values_map.find("SA_ENGINE_BETA") != _params_to_values_map.end()) {
        _sa_engine_beta = atof(_params_to_values_map["SA_ENGINE_BETA"].c_str()); 
    }
    else {
        _sa_engine_beta = 0.02; // default value
    }
	
	if (_params_to_values_map.find("SA_ENGINE_LAMBDA") != _params_to_values_map.end()) {
		_sa_engine_lambda = atof(_params_to_values_map["SA_ENGINE_LAMBDA"].c_str());
	}
	else {
		_sa_engine_lambda = 0.3; // default value
	}

    if (_params_to_values_map.find("SA_ENGINE_MPT") != _params_to_values_map.end()) {
        _sa_engine_moves_per_temp = atof(_params_to_values_map["SA_ENGINE_MPT"].c_str()); 
    }
    else {
        _sa_engine_moves_per_temp = 0.0; 
    }

	if (_params_to_values_map.find("USE_CUDA") != _params_to_values_map.end()) {
		string use_cuda = _params_to_values_map["USE_CUDA"];
        transform(use_cuda.begin(), use_cuda.end(), use_cuda.begin(), ::tolower);

		_use_cuda = use_cuda == "true" ? true : false;

        if (!USE_CUDA && _use_cuda) {
            fprintf(stderr, "\033[1;%dmCuda not supported, using CPU instead...\033[0m\n", 31);
            _use_cuda = false;
        }
	}
	else {
		_use_cuda = false;
	}

    if (_running_tests) _file_path_override = true;
    else _file_path_override = false;

    if (_params_to_values_map.find("OVERRIDE_FILE_PATH") != _params_to_values_map.end()) {
		string file_path_override = _params_to_values_map["OVERRIDE_FILE_PATH"];
        transform(file_path_override.begin(), file_path_override.end(), file_path_override.begin(), ::tolower);

		_file_path_override = file_path_override == "true" ? true : false;
	}

    if (_placement_type == "sa") {
		_run_sa = true;
	}
	else {
		_run_sa = false;
	}

    if (_params_to_values_map.find("RUN_RT_FEEDBACK") != _params_to_values_map.end()) {
		string run_rt_feedback_s = _params_to_values_map["RUN_RT_FEEDBACK"];
        transform(run_rt_feedback_s.begin(), run_rt_feedback_s.end(), run_rt_feedback_s.begin(), ::tolower);

		_run_rt_feedback = run_rt_feedback_s == "true" ? true : false;
	}
	else {
		_run_rt_feedback = false;
	}

    if (_params_to_values_map.find("INIT_PLACEMENT_FILE") != _params_to_values_map.end()) {
	    _sa_engine_init_placement_file = _params_to_values_map["INIT_PLACEMENT_FILE"];
    } else {
        fprintf(stderr, "\033[1;%dmMissing init placement file. Exiting...\033[0m\n", 31);
        exit(-1);
    }

	if (_params_to_values_map.find("SA_PLACEMENT_FILE") != _params_to_values_map.end()) {
		_sa_engine_progress_placement_file = _params_to_values_map["SA_PLACEMENT_FILE"];
	} else {
		_sa_engine_progress_placement_file = ""; // Default doesn't print sources
	}

    if (_params_to_values_map.find("VARY_TUMOR_WEIGHT") != _params_to_values_map.end()) {
        string vary = _params_to_values_map["VARY_TUMOR_WEIGHT"];
        transform(vary.begin(), vary.end(), vary.begin(), ::tolower);

        _vary_tumor_weight = (vary == "true") ? true : false;
    } else { // default
        _vary_tumor_weight = true;
    }

    // TODO: check if these are numbers
    if (_params_to_values_map.find("TARGET_TUMOR_COVERAGE") != _params_to_values_map.end()) {
        _target_tumor_v100 = std::abs(atof(_params_to_values_map["TARGET_TUMOR_COVERAGE"].c_str()));
    } else { // default 
        _target_tumor_v100 = 98.0;
    }

    if (_params_to_values_map.find("RT_FEEDBACK_SEED") != _params_to_values_map.end()) {
        _rt_feedback_rand_seed = atoi(_params_to_values_map["RT_FEEDBACK_SEED"].c_str());
    } else {
        _rt_feedback_rand_seed = 5; // Default value
    }

    if (_params_to_values_map.find("RT_FEEDBACK_DETECTOR_RADIUS") != _params_to_values_map.end()) {
        _rt_feedback_detector_radius = static_cast<float>(atof(_params_to_values_map["RT_FEEDBACK_DETECTOR_RADIUS"].c_str()));
    } else {
        _rt_feedback_detector_radius = 0.5f; // Default value
    }
    
    if (_params_to_values_map.find("RT_FEEDBACK_DETECTOR_NA") != _params_to_values_map.end()) {
        _rt_feedback_detector_na = static_cast<float>(atof(_params_to_values_map["RT_FEEDBACK_DETECTOR_NA"].c_str()));
    } else {
        _rt_feedback_detector_na = 0.5f; // Default value
    }

    if (_params_to_values_map.find("RT_FEEDBACK_LUT_FILE") != _params_to_values_map.end()) {
        _rt_feedback_lut_file_name = _params_to_values_map["RT_FEEDBACK_LUT_FILE"]; 
    } else {
        _rt_feedback_lut_file_name = "";
    }

    // check if a number
    if (_params_to_values_map.find("RT_FEEDBACK_LUT_SIZE") != _params_to_values_map.end()) {
        _rt_feedback_lut_size = atoi(_params_to_values_map["RT_FEEDBACK_LUT_SIZE"].c_str()); 
    } else {
        _rt_feedback_lut_size = 50; 
    }

    // get input mesh file before file path override
    stringstream mesh_file_ss;
    mesh_file_ss << _data_dir_path << "/" << _data_name << ((_read_data_from_vtk) ? ".vtk" : ".mesh");
    _mesh_file = mesh_file_ss.str();
}

//! This method reads a parameter file that contains all the required options for the program
unordered_map<string, string> read_parameters_file(string params_file)
{
    // initialize list of parameters expected
    static const string params_arr[] = {"PNF", "NUM_PACKETS", "WAVELENGTH", "DATA_DIR", "DATA_NAME", "OPTICAL_FILE", 
                    "READ_VTK", "SOURCE_TYPE", "TUMOR_WEIGHT", "PLACEMENT_TYPE", "FULLMONTE_OUTPUT_FILE", "FINAL_DIST_FILE", 
                    "SA_ENGINE_BETA", "INIT_PLACEMENT_FILE", "SA_ENGINE_MPT", "RUN_TESTS", "OVERRIDE_FILE_PATH", 
                    "SA_ENGINE_LAMBDA", "SA_PLACEMENT_FILE", "VARY_TUMOR_WEIGHT", "TARGET_TUMOR_COVERAGE", "RT_FEEDBACK_SEED", 
                    "USE_CUDA", "RT_FEEDBACK_DETECTOR_RADIUS", "RT_FEEDBACK_DETECTOR_NA", "RT_FEEDBACK_LUT_FILE", 
                    "RT_FEEDBACK_LUT_SIZE", "RUN_RT_FEEDBACK"};

    vector<string> params_vec(params_arr, params_arr+sizeof(params_arr)/sizeof(string));

    static const string optional_params_arr[] = {"PNF", "SA_ENGINE_BETA", "SA_ENGINE_MPT", "RUN_TESTS", "OVERRIDE_FILE_PATH", 
                    "FULLMONTE_OUTPUT_FILE", "FINAL_DIST_FILE", "SA_ENGINE_LAMBDA", "SA_PLACEMENT_FILE", "VARY_TUMOR_WEIGHT", 
                    "TARGET_TUMOR_COVERAGE", "RT_FEEDBACK_SEED", "USE_CUDA", "RT_FEEDBACK_DETECTOR_RADIUS", 
                    "RT_FEEDBACK_DETECTOR_NA", "RT_FEEDBACK_LUT_FILE", "RT_FEEDBACK_LUT_SIZE", "RUN_RT_FEEDBACK"};

    vector<string> optional_params_vec(optional_params_arr, optional_params_arr+sizeof(optional_params_arr)/sizeof(string));

    // Check if asking to list parameters 
    string list_params = params_file;
    std::transform(list_params.begin(), list_params.end(), list_params.begin(),
            [](unsigned char c) { return std::tolower(c); });
    
    if (list_params == "--list_params") {
        fprintf(stderr, "\033[1;%dmRequired parameters are: \033[0m\n", 33);

        unsigned idx = 1;
        for (unsigned i = 0; i < params_vec.size(); i++) {
            if (find(optional_params_vec.begin(), optional_params_vec.end(), params_vec[i]) == optional_params_vec.end()) {
                std::cerr << idx++ << "- " <<  params_vec[i] << std::endl;
            }
        }
        std::cerr << std::endl;
        fprintf(stderr, "\033[1;%dmOptional parameters are: \033[0m\n", 33);
        for (unsigned i = 0; i < optional_params_vec.size(); i++) {
            std::cerr << i << "- " << optional_params_vec[i] << std::endl;
        }
    
        std::exit(0); 
    }

    fprintf(stderr, "\033[1;%dmReading parameters file...\033[0m\n", 33);

    vector<bool> found_params(params_vec.size(), false); // to make sure we read all parameters

    unordered_map<string, string> params_to_values;

    ifstream params;
    params.open(params_file.c_str());
    if (!params)
    {
        fprintf(stderr, "\033[1;%dmCould not open parameters file: %s.\nExiting...\033[0m\n", 31, params_file.c_str());
        exit(-1);
    }
    
    while (!params.eof())
    {
        char str[511] = "";
        params.getline(str, 511);
        
        if (str[0] != '\0')
        {
            char* param = strtok(str, " ="); // delimiter
            char* str_option = strtok(NULL, " =");

            if (param[0] == '#' || isblank(param[0])) // commented by # or empty line
                continue;

            /*** Inputs ****/
            else
            {
                // make the parameter read upper case
                for (unsigned int i = 0; i < strlen(param); i++)
                {
                    param[i] = (char)toupper(param[i]);
                }

                // Check first that the read parameter is one of the parameters specified in the params_vec vector above
                bool found = false;
                for (unsigned int param_idx = 0; param_idx < params_vec.size(); param_idx++)
                {
                    if (strcmp(param, params_vec[param_idx].c_str()) == 0)
                    {
                        params_to_values[param] = str_option;
                        cout << param << " = " << str_option <<  endl;

                        found = true;

                        found_params[param_idx] = true;

                        break;
                    }
                }

                if (!found)
                {
                    fprintf(stderr, "\033[1;%dmInvalid parameter option: %s!\nExiting...\033[0m\n", 31, param);
                    exit(-1);
                }
            } // end else
        } // end if
    } // end while
    params.close();

    // Check that all required parameters have been read
    bool missing_param = false;
    for (unsigned int i = 0; i < found_params.size(); i++)
    {
        if (!found_params[i])
        {
            if (find(optional_params_vec.begin(), optional_params_vec.end(), params_vec[i]) == optional_params_vec.end())
            {
                if (missing_param)
                {
                    fprintf(stderr, "\033[1;%dm    %s\033[0m\n", 31, params_vec[i].c_str());
                }
                else 
                { 
                    missing_param = true;
                    fprintf(stderr, "\n\033[1;%dmThe following paramaters have no supplied values:\n    %s\033[0m\n", 
                                31, params_vec[i].c_str());
                }
            }
        }
    }

    if (missing_param)
    {
        fprintf(stderr, "\033[1;%dm\nExiting...\033[0m\n", 31);
        exit(-1);
    }

    return params_to_values;
}

//! This method reads the tissue properties file to set d_max and d_min vectors
//! Call after read_tissue_types
void read_tissue_properties()
{
    fprintf(stderr, "\n\033[1;%dmReading Tissue Properties.\033[0m\n", 33);

    stringstream tissue_properties_file; // specifies the thresholds
    tissue_properties_file << (_file_path_override ? (string(PDT_SPACE_SRC_DIR) + _data_dir_path) : _data_dir_path) << "/tissue_properties_" << _wavelength << ".txt"; 
    string tissue_props = tissue_properties_file.str();

    ifstream thresholds_in;
    thresholds_in.open(tissue_props);
    if (!thresholds_in)
    {
        fprintf(stderr, "\033[1;%dmCould not open %s\nExiting...\033[0m\n", 31, tissue_props.c_str());
        exit(-1);
    }

    /*
     * - read the tissue properties in an array of size num_materials.
     * - Do a conversion to fluence threshold when assigning d_max and d_min
     * - Add support of volume weighting in the optimizer.
     *
     */

    // array of thresholds based on the material type
    _d_max_tissues.resize(_number_of_materials);
    _d_min_tissues.resize(_number_of_materials);
    _guardband_tissues.resize(_number_of_materials);
    // _slopes.resize(_number_of_materials); // this is used in weighting the tetras 
    //                             // slopes are inversely proportional to the thresholds
    // Reading the tissue properties file
    string str;
    string token = "";
    // double first_threshold = -1; // used in setting the slopes
    for (unsigned int i = 0; i < _number_of_materials; i++)
    {
        getline(thresholds_in, str);
        istringstream line(str);
        if (_tumor_region_number.find(i+1) == _tumor_region_number.end())
        {
            _d_min_tissues[i] = 0.0;
            
            getline(line, token, ',');
            _tissue_names_ids[i+1] = token;
            line >> _d_max_tissues[i];
            
            // if (first_threshold == -1 && _d_max_tissues[i] > 1e-8)
            // {
            //     first_threshold = _d_max_tissues[i];
            //     _slopes[i] = 1.0;
            // }
            // else if (_d_max_tissues[i] > 1e-8)
            //     _slopes[i] = (first_threshold)/_d_max_tissues[i];
            // else 
            //     _slopes[i] = 0.0;

            // _d_max_tissues[i] *= 0.9; // This is to set a 10% guardband on healthy tissues for more safety
            getline(line, token, ',');
            getline(line, token, ',');
        }
        else
        {
            getline(line, token, ',');
            
            _tissue_names_ids[i+1] = token;
            
            getline(line, token, ',');
            _d_min_tissues[i] = atof(token.c_str());
            
            if (_d_min_tissues[i] < 1e-8)
            {
                fprintf(stderr, "\033[1;%dmMinimum dose threshold on Tumor CANNOT be ZERO! Exiting...\033[0m\n", 31);
                exit(-1);
            }

            // if (i == 0)
            // {
            //     first_threshold = _d_min_tissues[i];
            //     _slopes[i] = 1.0;
            // }
            // else
            //     _slopes[i] = (first_threshold)/_d_min_tissues[i];

            getline(line, token, ',');
            if (atof(token.c_str()) < 1e-8)
                _d_max_tissues[i] = 0;
            else
                _d_max_tissues[i] = atof(token.c_str());
        }
        getline(line, token, ',');
        _guardband_tissues[i] = atof(token.c_str());
    }
}

//! Call after reading opt properties
void read_tissue_types() {

    stringstream tissue_types_ss;
    tissue_types_ss << (_file_path_override ? (string(PDT_SPACE_SRC_DIR) + _data_dir_path) : _data_dir_path) << "/TissueTypes.txt"; // TODO: Add it to the params file
    ifstream read_tissue_types; 
    read_tissue_types.open(tissue_types_ss.str());


    string read_line = "";
    getline(read_tissue_types, read_line);
    // set first line of tissue types file to the tumor indices
    // if start with 0 assume to be max region index
    if (stoi(read_line) != 0) {
        stringstream tumor_ind_ss(read_line);
        int tumor_ind;
        while (tumor_ind_ss >> tumor_ind) {
            _tumor_region_number.insert(tumor_ind);
        }
    } else {
        _tumor_region_number.insert(_number_of_materials);
    }
}

//!
//! This method reads the optical properties file
//! @param opt_file [in]: Metrials' properties data (optical properties)
//!
void read_opt_file()
{
    // Reading the optical properties
    fprintf(stderr, "\033[1;%dmReading optical properties file....\033[0m\n", 33);

    ifstream input;
    input.open((_file_path_override ? (string(PDT_SPACE_SRC_DIR) + _optical_prop_file) : _optical_prop_file));
    if (!input)
    {
        fprintf(stderr, "\033[1;%dmCould not open %s\nExiting...\033[0m\n", 31, _optical_prop_file.c_str());
        exit(-1);
    }

    char str[255] = "";

    // Reading number of materials
    input.getline(str, 255);
    input.getline(str, 255);
    stringstream line(str);

    line >> _number_of_materials;

    // Reading the optical properties for each material
    material_opt * tmp_material;
    double mu_a, mu_s, g, n;
    for (unsigned int i = 0; i < _number_of_materials; i++)
    {
        input.getline(str, 255);
        stringstream line(str);

        line >> mu_a >> mu_s >> g >> n;

        tmp_material = new material_opt(i+1, mu_a, mu_s, g, n);
        _materials.push_back (tmp_material);
    }

    input.getline(str, 255);
    input.getline(str, 255);

    line.str(std::string());
    line.clear();

    line << str;

    line >> _index_of_refraction;

    input.close();
}