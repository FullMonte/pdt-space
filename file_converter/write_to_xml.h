#ifndef WRITE_TO_XML_H
#define WRITE_TO_XML_H

#include <iostream>
using namespace std;

// Prints to the materials section
void print_material_metrics (ostream& fout);

// Prints to the sources section
void print_sources (ostream& fout);

// Prints to the sa_options section
void print_sa_options (ostream& fout);

// Prints to the fm_params section
void print_fm_params (ostream& fout);

// Prints to the program_params section
void print_program_params (ostream& fout);

#endif // WRITE_TO_XML_H