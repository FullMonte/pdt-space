#include "converter_common.h"
#include "read_old_files.h"
#include "write_to_xml.h"

int main(int argc, char** argv)
{
	/******************************* Reading input parameters file *******************************/
    if (argc< 2)
    {
        fprintf(stderr, "\033[1;%dmNo parameters file supplied!\nExiting...\n\033[0m\n", 31);
		fprintf(stderr, "\033[1;%dmType %s <file_name> to run, or %s --list_params to see list of required parameters. \n\033[0m\n", 
					31, argv[0], argv[0]);
        exit(-1);
    }
    string params_file = argv[1];

    read_params_file(params_file);

    // set the parameters for plan to read the mesh
    // plan->set_parameters(MESH_FILE_PATH, DATA_NAME, OPTICAL_FILE[i], true, "");
    // plan->read_mesh();
    read_opt_file ();
    read_tissue_types ();
    read_tissue_properties ();
    read_initial_placement();

    // output file
    string output_file = params_file.substr(0, params_file.find(".")) + ".xml";
    cout << "Writing to " << output_file << endl;
    ofstream fout (output_file);
    fout << "<?xml version=\"1.0\"?>\n\n";

    // print to file
    print_material_metrics(fout);
    print_sources(fout);
    print_fm_params(fout);
    print_program_params(fout);

    if (_run_sa)
        print_sa_options (fout);

    fout.close();
}