/**
 * @author: Abdul-Amir Yassine
 * @date: January 11th, 2017
 *
 * \file read_mesh.h \brief Mesh Reader Header File
 *
 * This file defines all the classes related to the tetrahedral mesh
 */

#ifndef _MATERIAL_OPT_H_
#define _MATERIAL_OPT_H_

#include <vector>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <cstdlib>
#include <sstream>
#include <map>
#include <string>
#include <algorithm>
#include <limits>
#include <assert.h>

using namespace std;

class material_opt
{
public: 
    // Default Constructor
    material_opt();

    // Regular Constuctor
    material_opt(int id, double mu_a, double mu_s, double g, double n);

    // Destructor
    ~material_opt();

    double      get_mu_a();
    double      get_mu_s();
    double      get_g();
    double      get_n();

    int         get_id();

    // Setters
    void        set_mu_a(double mu_a);
    void        set_mu_s(double mu_s);
    void        set_g(double g);
    void        set_n(double n);

    void         set_id(int i);

protected:

    double      _mu_a; // absorption coefficient
    double      _mu_s; // scattering coefficient
    double      _g;    // anisotropy coefficient
    double      _n;    // deflection angle

    int         _id; 
};
#endif // _MATERIAL_OPT_H_
