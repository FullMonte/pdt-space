#ifndef READ_OLD_FILES_H
#define READ_OLD_FILES_H

#include <unordered_map>

using namespace std;

//! This method reads a parameter file that contains all the required options for the program
unordered_map<string, string> read_parameters_file(string params_file);
void read_params_file(string params_file);

//! This method reads the tissue properties file to set d_max and d_min vectors
//! Call after read_tissue_types
void read_tissue_properties();

//! Call after reading opt properties
void read_tissue_types();

void read_initial_placement();
void read_opt_file();

#endif // READ_OLD_FILES_H