#ifndef CONVERTER_COMMON_H
#define CONVERTER_COMMON_H

#include <vector>
#include <unordered_map>

#include "file_parser_old.h"
#include "material_opt.h"

#define _INFINITY_ numeric_limits<float>::max()
#define _NEG_INFINITY_ numeric_limits<float>::lowest()

using namespace std;

extern unordered_map<string, string>  _params_to_values_map;

extern string _data_name;
extern string _data_dir_path;
extern string _optical_prop_file;
extern string _fullmonte_output_file;
extern string _final_distribution_output_file;
extern bool _file_path_override;
extern string _mesh_file;
extern bool _read_data_from_vtk;
extern bool _running_tests;

// FM parameters
extern double _num_packets;
extern double _total_energy;
extern string _wavelength;
extern string _wavelength_read;
extern bool _use_cuda;

// conv opt parameters
extern string _placement_type;
extern double _tumor_weight;
extern bool _vary_tumor_weight;
extern double _target_tumor_v100;

// sources
extern Parser_sources _init_placement;
extern bool _source_tailored;
extern string _source_type;

// materials
extern vector<material_opt*> _materials;
extern unsigned _number_of_materials;
extern double _index_of_refraction;

extern vector<double> _d_max_tissues;
extern vector<double> _d_min_tissues;
extern unordered_set<unsigned> _tumor_region_number; 
extern unordered_map<unsigned, string> _tissue_names_ids; 
extern vector<double> _guardband_tissues;

// sa parameters
extern bool		 _run_sa;
extern double    _sa_engine_beta;
extern double	 _sa_engine_lambda;
extern string	 _sa_engine_init_placement_file; 
extern string    _sa_engine_init_placement_file_xml; // TODO: remove this
extern string	 _sa_engine_progress_placement_file; 
extern double    _sa_engine_moves_per_temp;

extern Parser_injection_points _injection_points;
extern bool _constrain_injection_point;
extern unordered_set<unsigned> _critical_regions;

// rt feedback
extern bool _run_rt_feedback;
extern int       _rt_feedback_rand_seed;
extern float     _rt_feedback_detector_radius;
extern float     _rt_feedback_detector_na;
extern unsigned  _rt_feedback_lut_size;
extern string    _rt_feedback_lut_file_name;

#endif // CONVERTER_COMMON_H