#include "write_to_xml.h"

#include "converter_common.h"

using namespace std;

void print_material_metrics (ostream& fout) {
    fout << "<materials>\n";
    fout << "  <matched_boundary>false</matched_boundary>\n";
    fout << "  <material name=\"Exterior\">\n";
    fout << "    <id>0</id>\n";
    fout << "    <n>" << _index_of_refraction << "</n>\n";
    fout << "    <mu_a>0</mu_a>\n";
    fout << "    <mu_s>0</mu_s>\n";
    fout << "    <g>0</g>\n";
    fout << "  </material>\n";

    for (unsigned int i = 0; i < _number_of_materials; i++) {
        fout << "  <material name=\"" << _tissue_names_ids[i+1] << "\">\n";
        fout << "    <id>" << i+1 << "</id>\n";
        fout << "    <mu_a>" << _materials[i]->get_mu_a() << "</mu_a>\n";
        fout << "    <mu_s>" << _materials[i]->get_mu_s() << "</mu_s>\n";
        fout << "    <g>" << _materials[i]->get_g() << "</g>\n";
        fout << "    <n>" << _materials[i]->get_n() << "</n>\n";
        fout << "    <d_min>" << _d_min_tissues[i] << "</d_min>\n";
        fout << "    <d_max>" << _d_max_tissues[i] << "</d_max>\n";
        fout << "    <safety_multiplier>" << _guardband_tissues[i] << "</safety_multiplier>\n";
        if (_tumor_region_number.find(i+1) != _tumor_region_number.end()) {
            fout << "    <is_tumor>true</is_tumor>\n";
        } else {
            fout << "    <is_tumor>false</is_tumor>\n";
        }
        fout << "  </material>\n";
    }

    fout << "</materials>\n\n";
}

void print_sources (ostream& fout) {
    // write_xml(fout, _init_placement);
    fout << "<sources>\n";
    fout << "  <max_sources>" << _init_placement.max_sources << "</max_sources>\n";
    fout << "  <line_tailored>" << (_source_tailored ? "true" : "false") << "</line_tailored>\n";
    fout << "  <sources_init>\n";
    for (unsigned i = 0; i < _init_placement.sources_init.size(); i++) {
        if (_init_placement.sources_init[i].type == "line") {
            fout << "    <source type=\"line\">\n";
            if (_init_placement.sources_init[i].length != _INFINITY_)
                fout << "      <length>" << _init_placement.sources_init[i].length << "</length>\n";
            fout << "      <endpoint location=\"proximal\">\n";
            fout << "        <x>" << _init_placement.sources_init[i].end0.x << "</x>\n";
            fout << "        <y>" << _init_placement.sources_init[i].end0.y << "</y>\n";
            fout << "        <z>" << _init_placement.sources_init[i].end0.z << "</z>\n";
            fout << "      </endpoint>\n";
            fout << "      <endpoint location=\"distal\">\n";
            fout << "        <x>" << _init_placement.sources_init[i].end1.x << "</x>\n";
            fout << "        <y>" << _init_placement.sources_init[i].end1.y << "</y>\n";
            fout << "        <z>" << _init_placement.sources_init[i].end1.z << "</z>\n";
            fout << "      </endpoint>\n";
            fout << "    </source>\n";
        } else if (_init_placement.sources_init[i].type == "point") {
            fout << "    <source type=\"point\">\n";
            fout << "      <endpoint>\n";
            fout << "        <x>" << _init_placement.sources_init[i].end0.x << "</x>\n";
            fout << "        <y>" << _init_placement.sources_init[i].end0.y << "</y>\n";
            fout << "        <z>" << _init_placement.sources_init[i].end0.z << "</z>\n";
            fout << "      </endpoint>\n";
            fout << "    </source>\n";
        } else if (_init_placement.sources_init[i].type == "cut-end") {
            fout << "    <source type=\"cut-end\">\n";
            fout << "      <radius>" << _init_placement.sources_init[i].r << "</radius>\n";
            fout << "      <na>" << _init_placement.sources_init[i].na << "</na>\n";
            fout << "      <endpoint>\n";
            fout << "        <x>" << _init_placement.sources_init[i].end0.x << "</x>\n";
            fout << "        <y>" << _init_placement.sources_init[i].end0.y << "</y>\n";
            fout << "        <z>" << _init_placement.sources_init[i].end0.z << "</z>\n";
            fout << "      </endpoint>\n";
            fout << "      <direction>\n";
            fout << "        <x>" << _init_placement.sources_init[i].end1.x << "</x>\n";
            fout << "        <y>" << _init_placement.sources_init[i].end1.y << "</y>\n";
            fout << "        <z>" << _init_placement.sources_init[i].end1.z << "</z>\n";
            fout << "      </direction>\n";
            fout << "    </source>\n";
        }
    }
    fout << "  </sources_init>\n";

    fout << "</sources>\n\n";
}

void print_sa_options (ostream& fout) {
    fout << "<sa_options>\n";

    fout << "  <max_src_distance>15</max_src_distance>\n";
    fout << "  <min_src_distance>10</min_src_distance>\n";
    fout << "  <distance_to_boundary>15</distance_to_boundary>\n";
    fout << "  <critical_tissues>";
    unsigned cnt = 0;
    for (auto const &elem: _critical_regions) {
        fout << elem;
        if (++cnt < _critical_regions.size()) fout << " ";
    }
    fout << "</critical_tissues>\n";

    // bool     _constrain_injection_point = plan->get_constrain_injection_point();
    unsigned _num_injection_points = _injection_points.injection_points.size();

    if (_constrain_injection_point || (_num_injection_points >= 1)) {
        fout << "  <injection_points constraint=\"" << (_constrain_injection_point ? "true" : "false") << "\">\n";
        fout << "    <region_id>" << _injection_points.region_id << "</region_id>\n";
        for (unsigned i = 0; i < _num_injection_points; i++) {
            fout << "    <point>\n";
            fout << "      <id>" << i << "</id>\n";
            fout << "      <normal>\n";
            fout << "        <x>" << _injection_points.injection_points[i].normal.x << "</x>\n";
            fout << "        <y>" << _injection_points.injection_points[i].normal.y << "</y>\n";
            fout << "        <z>" << _injection_points.injection_points[i].normal.z << "</z>\n";
            fout << "      </normal>\n";
            fout << "      <center>\n";
            fout << "        <x>" << _injection_points.injection_points[i].center.x << "</x>\n";
            fout << "        <y>" << _injection_points.injection_points[i].center.y << "</y>\n";
            fout << "        <z>" << _injection_points.injection_points[i].center.z << "</z>\n";
            fout << "      </center>\n";
            fout << "      <diameter>" << _injection_points.injection_points[i].diameter << "</diameter>\n";
            fout << "    </point>\n";
        }
        fout << "  </injection_points>\n";
    }

    fout << "</sa_options>\n\n";
}

// Prints to the fm_params section
void print_fm_params (ostream& fout) {
    fout << "<fm_params>\n";
    fout << "  <pnf>" << _total_energy << "</pnf>\n";
    fout << "  <num_packets>" << _num_packets << "</num_packets>\n";
    fout << "  <wavelength>" << _wavelength_read << "</wavelength>\n";
    fout << "  <use_cuda>" << (_use_cuda ? "true" : "false") << "</use_cuda>\n";

    // Old files don't have these, using default values
    fout << "  <rand_seed>" << 2 << "</rand_seed> <!--Default to 2, inform developer if need to make it more random-->\n";
    fout << "  <roulette_pr_win>" << 0.1 << "</roulette_pr_win> <!--Default value 0.1-->\n";
    fout << "  <roulette_w_min>" << 0.00001 << "</roulette_w_min> <!--Default value 0.00001-->\n";
    fout << "  <max_steps>" << 100000 << "</max_steps> <!--Default value 100000-->\n";
    fout << "  <max_hits>" << 1000 << "</max_hits> <!--Default value 1000-->\n";

    fout << "  <mesh_file>" << _mesh_file << "</mesh_file>\n";
    fout << "</fm_params>\n\n";
}

// Prints to the program_params section
void print_program_params (ostream& fout) {
    fout << "<program_params>\n";
    if (_fullmonte_output_file != "")
        fout << "  <fm_output_file>" << _fullmonte_output_file << "</fm_output_file>\n";
    fout << "  <read_vtk>" << (_read_data_from_vtk ? "true" : "false") << "</read_vtk>\n";
    fout << "  <tumor_weight>" << _tumor_weight << "</tumor_weight>\n";
    fout << "  <vary_tumor_weight>" << (_vary_tumor_weight ? "true" : "false") << "</vary_tumor_weight>\n";
    fout << "  <run_tests>" << (_running_tests ? "true" : "false") << "</run_tests>\n";
    fout << "  <override_file_path>" << (_file_path_override ? "true" : "false") << "</override_file_path>\n";
    fout << "  <run_rt_feedback>" << (_run_rt_feedback ? "true" : "false") << "</run_rt_feedback>\n";
    if (_run_rt_feedback) {
        fout << "  <rt_feedback_seed>" << _rt_feedback_rand_seed << "</rt_feedback_seed>\n";
        fout << "  <rt_feedback_det_rad>" << _rt_feedback_detector_radius << "</rt_feedback_det_rad>\n";
        fout << "  <rt_feedback_det_na>" << _rt_feedback_detector_na << "</rt_feedback_det_na>\n";
        fout << "  <rt_feedback_lut_file>" << _rt_feedback_lut_file_name << "</rt_feedback_lut_file>\n";
        fout << "  <rt_feedback_lut_size>" << _rt_feedback_lut_size << "</rt_feedback_lut_size>\n";
    }
    if (_final_distribution_output_file != "")
        fout << "  <final_dist_file>" << _final_distribution_output_file << "</final_dist_file>\n";
    fout << "  <target_tumor_cov>" << _target_tumor_v100 << "</target_tumor_cov>\n";
    fout << "  <placement_type>" << _placement_type << "</placement_type>\n";
    fout << "</program_params>\n\n";
}