/**
 * @author: Shuran Wang
 * @date: December 13, 2021
 *
 * \file file_parser_old.h \brief Old External file format wrapper Header File
 *
 * This file is the old xml parser file used by input file conversion only
 */

#ifndef FILE_PARSER_OLD_H_
#define FILE_PARSER_OLD_H_

#include <vector>
#include <unordered_set>

#include <boost/property_tree/xml_parser.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/foreach.hpp>
#include <boost/lexical_cast.hpp>

using namespace std;

struct Parser_point {
    float x;
    float y;
    float z;
};

struct Parser_source {
    string type;
    double length;
    Parser_point end0;
    Parser_point end1;
    float r;   // cut-end only
    float na;  // cut-end only
};

//! Method to compute the distance from another point 
double get_distance_from(const Parser_point& p1, const Parser_point& p2);

struct Parser_sources {
    int max_sources;
    string source_type;
    bool unified_type = true; 
    vector<Parser_source> sources_init;
    bool tailored = false;
};

struct Parser_injection_point {
    int id;
    float diameter;
    Parser_point normal;
    Parser_point center;
};

struct Parser_injection_points {
    int region_id;
    vector<Parser_injection_point> injection_points;
};

class ParserOld {
public:
    // Default constructor
    ParserOld();

    // Regular constructor
    ParserOld(string filename);

    // Destructor
    ~ParserOld();

    // Parse XML file
    bool read_from_xml(string filename);

    // Write init placement data to csv file (not applicable)
    // void write_to_init_placement_csv(string filename);

    // Getter functions for injection points
    Parser_injection_points get_injection_points();
    void get_injection_point_options(bool &constrain, /*PlacementMode &mode,*/ unsigned &max_num);
    void get_injection_point_options(bool &constrain/*, PlacementMode &mode*/);

    // Getter function for sources
    void get_sources(Parser_sources &sources); 

    unordered_set <unsigned> get_critical_regions() { return _critical_regions; }

    void get_init_src_params (double &min_dist, double &max_dist, double &bound_dist) {
        min_dist = _min_src_dist;
        max_dist = _max_src_dist;
        bound_dist = _dist_to_bound;
    }

private:

    // helper function to read fullmonte options
    void read_fm_params(boost::property_tree::ptree token);
    void read_line_source  (const boost::property_tree::ptree& token);
    void read_point_source (const boost::property_tree::ptree& token);
    void read_cutend_source(const boost::property_tree::ptree& token);

    // probe options
    Parser_sources _sources;
    unsigned _max_sources;
    bool _line_tailored;


    // other placement options
    bool _constrain_injection_point;
    Parser_injection_points _injection_points;
    // PlacementMode _placement_mode;
    unordered_set <unsigned> _critical_regions;

    double _min_src_dist = 10.0;
    double _max_src_dist = 15.0;
    double _dist_to_bound = 8.0;
};

#endif // FILE_PARSER_H_