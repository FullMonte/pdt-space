#ifndef _VTK_include_
#define _VTK_include_

#include <vtkSmartPointer.h>
#include <vtkPointData.h>
#include <vtkCellData.h>
#include <vtkSphereSource.h>
#include <vtkMatrixMathFilter.h>
#include <vtkPolyDataMapper.h>
#include <vtkActor.h>
#include <vtkRenderWindow.h>
#include <vtkRenderer.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkUnstructuredGridReader.h>
#include <vtkXMLUnstructuredGridReader.h>
#include <vtkDataSetSurfaceFilter.h>
#include <vtkUnstructuredGridWriter.h>
#include <vtkXMLUnstructuredGridWriter.h>
#include <vtkXMLPolyDataWriter.h>
#include <vtkPoints.h>
#include <vtkUnstructuredGrid.h>
#include <vtkDataArray.h>
#include <vtkAbstractArray.h>
#include <vtkCellLocator.h>
#include <vtkPointLocator.h>
#include <vtkMergeDataObjectFilter.h>

#endif
