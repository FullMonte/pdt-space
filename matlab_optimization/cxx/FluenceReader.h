#include <vtkSmartPointer.h>
#include <vtkPointData.h>
#include <vtkCellData.h>
#include <vtkSphereSource.h>
#include <vtkMatrixMathFilter.h>
#include <vtkPolyDataMapper.h>
#include <vtkActor.h>
#include <vtkRenderWindow.h>
#include <vtkRenderer.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkUnstructuredGridReader.h>
#include <vtkDataSetSurfaceFilter.h>
#include <vtkUnstructuredGridWriter.h>
#include <vtkPoints.h>
#include <vtkUnstructuredGrid.h>
#include <vtkDataArray.h>
#include <vtkAbstractArray.h>
#include <vtkTetra.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <stdlib.h>
#include <string>
#include <stdio.h>
#include <vector>
#include <sstream>

using namespace std;

// read_fluence reads a vtk file produced by FullMonte after a simulation, produces text files
// containing the fluences, tissue types, tetra volumes, source centers, and pairwise distances 
// between sources
// inputFileName: vtk file with fluences to read
// outputFilePfx: prefix of filename to write outputs to, e.g. will write fluences to ${outputFilePfx}_${meshNumber}_fluences.txt
// numSources: number of sources FullMonte was run with, i.e. number of fluence arrays in file
// numCellsOriginal: number of cells in original file, to make sure this wasn't changed by FullMonte (e.g. as a result of the 
//          VTKLegacyReader renumberZero method)
// meshNumber: used to tag output files from different meshes (e.g. tumor1 vs tumor2)
bool read_fluence(string inputFileName, string outputFilePfx, int numSources, int numCellsOriginal, int meshNumber);
