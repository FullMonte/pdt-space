#include "TumorTagger.h"

double eps = 1e-8;

// tag_tumor: given a vtk mesh file, tags an elliptical tumor with random semi-axis lengths within a reasonable range,
// centered at a random position such that it is entirely within the mesh, and writes the mesh with the tagged tumor 
// to another vtk file.
// inputFileName: full path to vtk mesh to read
// outputFileName: full path to vtk mesh to create, based off input mesh but with tumor cells tagged
// sourceCells: will be filled with vector of cell indices of cells that were labelled as sources
// meshScale: ratio of units used by mesh to cm (e.g. 10 for mesh in mm)
// sourceSpacing: separation between adjacent sources, in cm
// tumorIdx: will be set to the tissue type that was assigned to the tumor
// numCells: will be set to the total number of cells in the mesh
// numPoints: will be set to the total number of points in the mesh
// tagSources: if true, will tag tetras selected to be sources with tissue type tumorIdx + 1
bool tag_tumor(string inputFileName, string outputFileName, vector<int> &sourceCells, double meshScale, double sourceSpacing, int &tumorIdx, int &numCells, int &numPoints, bool tagSources) {
    srand(10);//time(NULL));

    int tissueArrayIdx = -1; // which abstract array in CellData is the tissue type, will be set below

    // want tumor to be approximately football shaped, with long axis approx. 3-6cm
    double tumorSemiAxes[3];
    tumorSemiAxes[0] = meshScale * (1.5 * rand() / double(RAND_MAX) + 1.5);
    tumorSemiAxes[1] = 0.6 * tumorSemiAxes[0] * (0.2 * rand() / double(RAND_MAX) + 0.9); // 60% of long axis, plus noise of +- 10%
    tumorSemiAxes[2] = 0.6 * tumorSemiAxes[0] * (0.2 * rand() / double(RAND_MAX) + 0.9); // 60% of long axis, plus noise of +- 10%

    cout << "Tumor semiaxis dimensions are (" << tumorSemiAxes[0] << ", " << tumorSemiAxes[1] << ", " << tumorSemiAxes[2] << ")" << endl;

    // error-checking input
    if (tumorSemiAxes[0] <= 0 || tumorSemiAxes[1] <= 0 || tumorSemiAxes[2] <= 0) {
        cout << "ERROR: tumorSemiAxes must all be positive!" << endl;
        return false;
    }

    //read all the data from the file
    vtkSmartPointer<vtkUnstructuredGridReader> reader = vtkSmartPointer<vtkUnstructuredGridReader>::New();
    reader->SetFileName(inputFileName.c_str());
    reader->Update();

    vtkSmartPointer<vtkUnstructuredGrid> unstructuredGrid = reader->GetOutput();

    vtkSmartPointer<vtkPoints> points = unstructuredGrid->GetPoints();
    vtkSmartPointer<vtkCellData> cells = unstructuredGrid->GetCellData();
    
    numPoints = points->GetNumberOfPoints();
    numCells = unstructuredGrid->GetNumberOfCells();

    // whether or not each point/cell is in the tumor. If a cell has at least one point in the tumor, it is counted as being inside.
    bool* pointInTumor = new bool[numPoints];
    for (int i=0; i<numPoints; i++) {
        pointInTumor[i] = false;
    }

    //
    // === Randomly choose tumor location ===
    //

    // the first step is to find a viable center for the tumor. The method here (valid for any approximately elliptical mesh) 
    // is to find the average of the upper and lower limits of the coordinates of the mesh and consider a sphere of diameter equal
    // to the width of the mesh in its smallest dimension. This sphere will be entirely within the mesh for meshes that are reasonably
    // approximated by an ellipse. Then the problem is reduced to the much simpler problem of choosing a center for an ellipse that is
    // entirely within this sphere.
    
    // find upper and lower bounds for mesh
    double lowerBounds[3] = {DBL_MAX, DBL_MAX, DBL_MAX}, upperBounds[3] = {-DBL_MAX, -DBL_MAX, -DBL_MAX};
    for (int i=1; i<numPoints; i++) {
        // array of x,y,z location
        double* point = points->GetPoint(i);
        for (int j=0; j<3; j++) {
            lowerBounds[j] = min(point[j], lowerBounds[j]);
            upperBounds[j] = max(point[j], upperBounds[j]);
        }
    }
    cout << "Lower bound is (" << lowerBounds[0] << ", " << lowerBounds[1] << ", " << lowerBounds[2] << "), upper bound is (" <<
        upperBounds[0] << ", " << upperBounds[1] << ", " << upperBounds[2] << ")." << endl;


    // find center sphere and largest semiaxis of the ellipse
    double inscribedSphereCenter[3];
    double inscribedRadius = DBL_MAX;
    double majorSemiAxis = 0;
    for (int i=0; i<3; i++) {
        inscribedSphereCenter[i] = 0.5 * (lowerBounds[i] + upperBounds[i]);
        inscribedRadius = min(0.5 * (upperBounds[i] - lowerBounds[i]), DBL_MAX);
        majorSemiAxis = max(tumorSemiAxes[i], majorSemiAxis);
    }

    // an ellipsoid with center within a sphere with the same center but radius of (inscribedRadius - the ellipse's largest semiaxis) 
    // should be fully contained in the mesh
    double viableRadius = inscribedRadius - 2.0 * majorSemiAxis; // factor of 2 to make tumor more likely to be fully inside mesh
    if (viableRadius <= 0) {
        cout << "inscribedRadius = " << inscribedRadius << ", viableRadius = " << viableRadius << endl << endl;
        cout << "ERROR: viableRadius is <=0, so there is no feasible place to put the tumor!" << endl;
        return false;
    }

    // randomly generate a tumor center given this sphere of viable placements
    double tumorCenter[3];
    double sinTheta = 2.0 * rand() / double(RAND_MAX) - 1.0;    // generate sinTheta randomly so that the probability is uniform on the unit sphere

    if (abs(sinTheta) > 1.0) {
        cout << "ERROR: sinTheta is out of bounds, with value of " << sinTheta << endl;
        return false;
    }
    // P(r<x) ~ x^3 since surface area ~ r^2, so generate uniform variable on [0,1] and take cube root to get appropriately distributed r
    double uniform = rand() / double(RAND_MAX); 
    double r = viableRadius * cbrt(uniform);                // radius
    double theta = asin(sinTheta);                          // polar
    double phi = 4.0 * atan(1) * rand() / double(RAND_MAX); // azimuthal
    tumorCenter[0] = inscribedSphereCenter[0] + r * sinTheta * cos(phi); // x
    tumorCenter[1] = inscribedSphereCenter[1] + r * sinTheta * sin(phi); // y
    tumorCenter[2] = inscribedSphereCenter[2] + r * cos(theta);          // z

    // randomly generate a tumor orientation
    double tumorAxisVectors[3][3];
    sinTheta = 2.0 * rand() / double(RAND_MAX) - 1.0;
    theta = asin(sinTheta);                          // polar
    phi = 4.0 * atan(1) * rand() / double(RAND_MAX);   // azimuthal
    double sinPhi = sin(phi);
    double cosTheta = cos(theta);
    double cosPhi = cos(phi);

    // first axis
    tumorAxisVectors[0][0] = sinTheta * cosPhi; // x
    tumorAxisVectors[0][1] = sinTheta * sinPhi; // y
    tumorAxisVectors[0][2] = cosTheta;          // z

    // check: should be unit length
    double length = 0;
    for (int i=0; i<3; i++) {
        length += tumorAxisVectors[0][i] * tumorAxisVectors[0][i];
    }
    if (length >= 1.0 + eps || length <= 1.0 - eps) {
        cout << "ERROR: Axis vector 0 is not of unit length! It has length " << length << endl;
        return false;
    }

    // second axis: generate a random point on the unit circle in the x-y plane then rotate to be normal to the first axis
    double theta2 = 4.0 * atan(1) * rand() / double(RAND_MAX);
    double tmp_x = cos(theta2);
    double tmp_y = sin(theta2);

    // rotating
    tumorAxisVectors[1][0] = cosTheta * cosPhi * tmp_x - sinPhi * tmp_y; // x
    tumorAxisVectors[1][1] = cosTheta * sinPhi * tmp_x + cosPhi * tmp_y; // y
    tumorAxisVectors[1][2] = -sinTheta * tmp_x;                          // z

#ifdef VERBOSE
    cout << "inscribedRadius = " << inscribedRadius << ", viableRadius = " << viableRadius << endl;
    cout << "Tumor centered at (" << tumorCenter[0] << ", " << tumorCenter[1] << ", " << tumorCenter[2] << ")" << endl;
    cout << "r = " << r << ", theta = " << theta << ", phi = " << phi << ", theta2 = " << theta2 << endl;
#endif

    // check: should be unit length
    length = 0;
    for (int i=0; i<3; i++) {
        length += tumorAxisVectors[1][i] * tumorAxisVectors[1][i];
    }
    if (length >= 1 + eps || length <= 1 - eps) {
        cout << "ERROR: Axis vector 1 is not of unit length! It has length " << length << endl;
        return false;
    }

    // check: first two axes should be orthogonal
    double tmp = 0;
    for (int i=0; i<3; i++) {
        tmp += tumorAxisVectors[0][i] * tumorAxisVectors[1][i];
    }
    if (tmp > eps || tmp < -eps) {
        cout << "ERROR: First two unit vectors are not orthogonal! Their dot product is " << tmp << endl;
        return false;
    }

    // third axis: from cross product of first two axes
    tumorAxisVectors[2][0] = (tumorAxisVectors[0][1] * tumorAxisVectors[1][2]) - (tumorAxisVectors[0][2] * tumorAxisVectors[1][1]);
    tumorAxisVectors[2][1] = (tumorAxisVectors[0][2] * tumorAxisVectors[1][0]) - (tumorAxisVectors[0][0] * tumorAxisVectors[1][2]);
    tumorAxisVectors[2][2] = (tumorAxisVectors[0][0] * tumorAxisVectors[1][1]) - (tumorAxisVectors[0][1] * tumorAxisVectors[1][0]);

    // check:  should be unit length
    length = 0;
    for (int j=0; j<3; j++) {
        length += tumorAxisVectors[2][j] * tumorAxisVectors[2][j];
    }
    if (length >= 1.0 + eps || length <= 1.0 - eps) {
        cout << "ERROR: Axis vector 2 is not of unit length! It has length " << length << endl;
        return false;
    }

    //
    // === Now tag tumor region ===
    //

    // find which points are within tumor region. Recall that an ellipse is defined by {x | x^T * A^(-1) * x <= 1}, where A is positive
    // semidefinite and its eigenvectors are the axes and its eigenvalues are the squares of the axis lengths 
    double eigenvalues[3];
    for (int i=0; i<3; i++) {
        eigenvalues[i] = tumorSemiAxes[i] * tumorSemiAxes[i];
    }

    int numTumorTetras = 0;
    for (int i=0; i<numPoints; i++) {
        // array of x,y,z location
        double* point = points->GetPoint(i);

        double dx = point[0] - tumorCenter[0];
        double dy = point[1] - tumorCenter[1];
        double dz = point[2] - tumorCenter[2];

        // project (dx,dy,dz) onto axes of A
        double projected[3] = {0,0,0};
        for (int j=0; j<3; j++) {
            projected[j] = tumorAxisVectors[j][0] * dx + tumorAxisVectors[j][1] * dy + tumorAxisVectors[j][2] * dz;
        }

        double condition = 0;
        for (int j=0; j<3; j++) {
            condition += projected[j] * projected[j] / eigenvalues[j];
        }

        if (condition <= 1) {
            pointInTumor[i] = true;
            numTumorTetras++;
        }

    #ifdef VERBOSE
        if (i % (points->GetNumberOfPoints() / 20) == 0) {
            cout << "Point at (" << point[0] << ", " << point[1] << ", " << point[2] << ")" << endl;
            cout << "Distance squared = " << dx*dx + dy*dy + dz*dz << endl;
            cout << "Point " << (pointInTumor[i] ? "" : "not ") << "in tumor." << endl;
        }
    #endif
    }

    cout << "Number of tumor tetras: " << numTumorTetras << endl << endl;
    // print names of all data arrays
    int idx = 0;
    vtkSmartPointer<vtkDataArray> array = cells->GetArray(idx);
    cout << "Listing array names:" << endl;
    while (array != NULL) {
        string arrayName = array->GetName();
        cout << "Array " << idx << ":" << arrayName << endl;
        if (arrayName.compare(string("Region")) == 0 || arrayName.compare(string("Tissue Type")) == 0) {
            if (tissueArrayIdx >= 0) {
                cout << "ERROR: Multiple arrays with name \"Region\" or \"Tissue Type\" found in the file!" << endl;
                return false;
            }
            cout << "tissueArrayIdx = " << idx << endl;
            tissueArrayIdx = idx;
        }
        idx++;
        array = cells->GetArray(idx);
    }
    cout << endl;

    if (tissueArrayIdx < 0) {
        cout << "ERROR: No array with name \"Region\" or \"Tissue Type\" was found in the file \"" << inputFileName << "\"!" << endl;
        return false;
    }

    vtkSmartPointer<vtkDataArray> tissueTypes = cells->GetArray(tissueArrayIdx);

    // find maximum tissue type, so that tumor type can be one higher
    double maxTissueType = -1;
    for (int i=0; i<numCells; i++) {
        maxTissueType = max(tissueTypes->GetTuple1(i), maxTissueType);
    }
    if (maxTissueType < 0) {
        cout << "ERROR: No positive tissue types are set! numCells = " << numCells << endl << endl;
        return false;
    }
    tumorIdx = (int)maxTissueType + 1;

    // file to write tissue types to
    string output_dir_path = outputFileName;
    output_dir_path.erase(output_dir_path.rfind('/'));
    stringstream tissue_out_ss;
    tissue_out_ss << output_dir_path << "/" << "TissueTypes.txt";
    std::ofstream outputFile(tissue_out_ss.str());

    // find which cells contain any of those points
    for (int i=0; i<numCells; i++) {
        vtkSmartPointer<vtkIdList> cellPointIds = vtkSmartPointer<vtkIdList>::New();
        unstructuredGrid->GetCellPoints(i, cellPointIds);

        for (int j=0; j<cellPointIds->GetNumberOfIds(); j++) {
            if (pointInTumor[cellPointIds->GetId(j)]) {
                // update cell tissue type to be a tumor
                tissueTypes->SetTuple1(i, tumorIdx);
                break;
            }
        }

        // write file with tissue types, to be read by matlab
        outputFile << tissueTypes->GetTuple1(i) << endl;
    }

    //
    // === identify tumor cells to be considered as light sources by FullMonte ===
    //

    vtkSmartPointer<vtkCellLocator> cellLocator = 
            vtkSmartPointer<vtkCellLocator>::New();
    cellLocator->SetDataSet(unstructuredGrid);
    cellLocator->BuildLocator();

    // update upper/lower bounds to be those of the tumor, not of the mesh
    lowerBounds[0] = tumorCenter[0] - tumorSemiAxes[0]; // tumorSemiAxes[0] is always the largest element
    lowerBounds[1] = tumorCenter[1] - tumorSemiAxes[0];
    lowerBounds[2] = tumorCenter[2] - tumorSemiAxes[0];
    upperBounds[0] = tumorCenter[0] + tumorSemiAxes[0];
    upperBounds[1] = tumorCenter[1] + tumorSemiAxes[0];
    upperBounds[2] = tumorCenter[2] + tumorSemiAxes[0];

    // divide the entire mesh into a grid of cubes of side length sourceSpacing and take all those tetras that are within the tumor region
    double testPoint[3];
    vtkIdType cellId;
    double closestPoint[3]; // needed by FindClosestPoint
    double closestPointDist2; // needed by FindClosestPoint
    int subId; // needed by FindClosestPoint

    cout << "About to tag sources" << endl;
    double stepSize = sourceSpacing * meshScale;
    for (testPoint[0] = lowerBounds[0]; testPoint[0] < upperBounds[0]; testPoint[0] += stepSize) {
        for (testPoint[1] = lowerBounds[1]; testPoint[1] < upperBounds[1]; testPoint[1] += stepSize) {
            for (testPoint[2] = lowerBounds[2]; testPoint[2] < upperBounds[2]; testPoint[2] += stepSize) {
                // find closest cell
                cellLocator->FindClosestPoint(testPoint, closestPoint, cellId, subId, closestPointDist2);

                // check if it's in the tumor region, if so then tag it
                if (tissueTypes->GetTuple1(cellId) == tumorIdx) {
                    // make sure it's unique
                    bool unique = true;
                    for (int i=0; i<sourceCells.size(); i++) {
                        if (sourceCells[i] == cellId) {
                            unique = false;
                            break;
                        }
                    }
                    if (unique) {
                        sourceCells.push_back(cellId);
                        cout << "tagged a source at cellId = " << cellId << endl;
                    }
                    else {
                        cout << "Note: Found repeated source id at cellId = " << cellId << endl;
                    }
                }
            }
        }
    }
    cout << "Finished tagging sources" << endl;

#ifdef VERBOSE
    if (sourceCells.size() != 0) {
        cout << "sourceCells (size " << sourceCells.size() << ") = " << sourceCells[0];
        for (int i=0; i<min(30, (int) sourceCells.size()); i++) {
            cout << ", " << sourceCells[i];
        }
        if (sourceCells.size() > 30) {
            cout << ", ...";
        }
        else {
            cout << ".";
        }
        cout << endl << endl;
    }
    else {
        cout << "ERROR: sourceCells is empty!" << endl;
        return false;
    }
#endif

    // write new VTK file with tumor tagged
    vtkSmartPointer<vtkUnstructuredGridWriter> writer = vtkSmartPointer<vtkUnstructuredGridWriter>::New();
    writer->SetFileName(outputFileName.c_str());
    writer->SetInputData(unstructuredGrid);
    writer->Write();

    // write another VTK file with sources also tagged
    if (tagSources) {
        cout << "Tagging sources..." << endl;
        for (int i=0; i<sourceCells.size(); i++) {
            tissueTypes->SetTuple1(sourceCells[i], tumorIdx+1);
        }

        outputFileName = outputFileName.substr(0, outputFileName.length() - 4) + "_sources.vtk";
        writer->SetFileName(outputFileName.c_str());
        writer->SetInputData(unstructuredGrid);
        writer->Write();
    }
    
    // file to write sourceIDs to
    stringstream source_id_ss;
    source_id_ss << output_dir_path << "/" << "sourceIndices.txt";
    std::ofstream outputFile_sources(source_id_ss.str());
    
    for (unsigned int i = 0; i < sourceCells.size(); i++)
    {
        outputFile_sources << sourceCells[i];
        if (i < sourceCells.size() - 1)
            outputFile_sources << endl;
    }
    outputFile_sources.close();

    delete[] pointInTumor;

    cout << endl << "numCells = " << numCells << endl;

    return true;
}

// takes a mesh (baseFile) and a second mesh that is a subset of the first (tumorFile), and writes a new mesh
// that is a copy of the first but where every cell of the mesh that is also in the tumor mesh is set to a new
// tissue type.
// pointSources: if true, sourceCells will contain strings of the form "x y z" representing point sources, else it will contain strings of the form "n" representing tetrahedron indices
// meshScale: ratio of units used by mesh to cm (e.g. 10 for mesh in mm)
// sourceSpacing: separation between adjacent sources, in cm
bool tag_tumor_from_file(string baseFile, string tumorFile, string outputFile, bool pointSources, vector<string> &sourceCells, double meshScale, double sourceSpacing) {
    vtkSmartPointer<vtkUnstructuredGridReader> baseReader = vtkSmartPointer<vtkUnstructuredGridReader>::New();
    baseReader->SetFileName(baseFile.c_str());
    baseReader->Update();

    vtkSmartPointer<vtkUnstructuredGrid> baseGrid = baseReader->GetOutput();

    vtkSmartPointer<vtkPoints> basePoints = baseGrid->GetPoints();
    vtkSmartPointer<vtkCellData> baseCells = baseGrid->GetCellData();
    
    int numBaseCells = baseGrid->GetNumberOfCells();
    int numBasePoints = basePoints->GetNumberOfPoints();

    vtkSmartPointer<vtkUnstructuredGridReader> tumorReader = vtkSmartPointer<vtkUnstructuredGridReader>::New();
    tumorReader->SetFileName(tumorFile.c_str());
    tumorReader->Update();

    vtkSmartPointer<vtkUnstructuredGrid> tumorGrid = tumorReader->GetOutput();

    vtkSmartPointer<vtkPoints> tumorPoints = tumorGrid->GetPoints();
    
    int numTumorPoints = tumorPoints->GetNumberOfPoints();

    // array of vectors: the index is a point id, the vector it gives contains a list of cell id's of all cells
    // that contain that point
    vector<vector<vtkIdType>*> pointsToCells(numBasePoints, NULL);
   
    for (int i=0; i<numBasePoints; i++) {
        pointsToCells[i] = new vector<vtkIdType>;
    }

    cout << "About to fill in pointsToCells... ";
    for (int i=0; i<numBaseCells; i++) {
        vtkSmartPointer<vtkCell> cell = baseGrid->GetCell(i);
        vtkSmartPointer<vtkIdList> cellPoints = cell->GetPointIds();
        int numCellPoints = cell->GetNumberOfPoints();
        if (numCellPoints != 4) {
            cout << "ERROR: Non-tetrahedral mesh element at index " << i << "!" << endl;
            return false;
        }

        for (int j=0; j<4; j++) {
            bool unique = true;
            vtkIdType pointId = cellPoints->GetId(j);
            if (pointId >= numBasePoints) {
                cout << "ERROR: pointId of " << pointId << " exceeds numBasePoints=" << numBasePoints << endl;
                return false;
            }
            if (pointId < 0) {
                cout << "ERROR: negative pointId at i=" << i << endl;
                return false;
            }
            for (int k=0; k<pointsToCells[pointId]->size(); k++) {
                if ((*pointsToCells[pointId])[k] == i) {
                    unique = false;
                    break;
                }
            }
            if (unique) {
                pointsToCells[pointId]->push_back(i);
            }
        }
    }
    cout << "Done." << endl;

    // number of points from each cell in the base mesh that are present in the tumor mesh: if this is 4 for a given
    // cell, the cell is part of the tumor and so its tissue type should be changed
    vector<int> numPointsOverlappingTumor(numBaseCells, 0);

    vtkSmartPointer<vtkPointLocator> pointLocator = vtkSmartPointer<vtkPointLocator>::New();
    pointLocator->SetDataSet(baseGrid);
    pointLocator->AutomaticOn();
    pointLocator->SetNumberOfPointsPerBucket(2);
    pointLocator->BuildLocator();

    double dist2 = DBL_MAX;

    // bounds on tumor points, used for tagging sources
    double upperBounds[3], lowerBounds[3];
    for (int i=0; i<3; i++) {
        upperBounds[i] = tumorPoints->GetPoint(0)[i];
        lowerBounds[i] = tumorPoints->GetPoint(0)[i];
    }

    // to avoid adding the same point twice
    vector<vtkIdType> matchingPointIds;

    for (int i=0; i<numTumorPoints; i++) {
        // find id of point in base mesh corresponding to point in tumor mesh
        vtkIdType closestPoint = pointLocator->FindClosestPointWithinRadius(1e-6, tumorPoints->GetPoint(i), dist2);
        // check that this point has not already been considered
        bool unique = true;
        for (int j=0; j<matchingPointIds.size(); j++) {
            if (closestPoint == matchingPointIds[j]) {
                cout << "Repeated point in tumor! Not an error, but not fully expected." << endl;
                unique = false;
            }
        }
        if (unique) { 
            matchingPointIds.push_back(closestPoint);
            if (dist2 < 1e-10) {
                // add 1 to the number of overlapping points for each cell containing this point
                for (int j=0; j<pointsToCells[closestPoint]->size(); j++) {
                    numPointsOverlappingTumor[(*pointsToCells[closestPoint])[j]]++;
                }
            }
            else {
                cout << "ERROR: Tumor point " << i << " does not match any base mesh point, which should never happen " << 
                    "because the tumor mesh is a subset of the base mesh!" << endl;
                return false;
            }
        }

        // update bounds
        for (int j=0; j<3; j++) {
            double x = tumorPoints->GetPoint(i)[j];
            if (upperBounds[j] < x) {
                upperBounds[j] = x;
            }
            if (lowerBounds[j] > x) {
                lowerBounds[j] = x;
            }
        }
    }

    cout << "Tumor bounds are:" << endl;
    for (int i=0; i<3; i++) {
        string coord;
        if (i==0)
            coord = "x";
        else if (i==1)
            coord = "y";
        else
            coord = "z";
        cout << lowerBounds[i] << " <= " << coord << " <= " << upperBounds[i] << endl;
    }
    cout << endl;

    // sanity check: all cells should be tetrahedrons so can't overlap at more than 4 points
    for (int i=0; i<numBaseCells; i++) {
        if (numPointsOverlappingTumor[i] > 4) {
            cout << "ERROR: Cell " << i << " has " << numPointsOverlappingTumor[i] << " points overlapping the tumor, " <<
                                "where it should never be more than 4!" << endl;
            return false;
        }
    }

    // those cells with 4 points overlapping the tumor are in the tumor, and so their tissue type should be changed

    // first, must find the tissue type array
    int idx = 0;
    int tissueArrayIdx = -1;
    vtkSmartPointer<vtkDataArray> array = baseCells->GetArray(idx);
    cout << "Listing array names:" << endl;
    while (array != NULL) {
        string arrayName = array->GetName();
        cout << "Array " << idx << ":" << arrayName << endl;
        if (arrayName.compare(string("Region")) == 0 || arrayName.compare(string("Tissue Type")) == 0) {
            if (tissueArrayIdx >= 0) {
                cout << "ERROR: Multiple arrays with name \"Region\" or \"Tissue Type\" found in the file!" << endl;
                return false;
            }
            cout << "tissueArrayIdx = " << idx << endl;
            tissueArrayIdx = idx;
        }
        idx++;
        array = baseCells->GetArray(idx);
    }

    if (tissueArrayIdx < 0) {
        cout << "ERROR: No array with name \"Region\" or \"Tissue Type\" was found in the file \"" << baseFile << "\"!" << endl;
        return false;
    }

    vtkSmartPointer<vtkDataArray> tissueTypes = baseCells->GetArray(tissueArrayIdx);

    // find maximum tissue type, so that tumor type can be one higher
    double maxTissueType = -1;
    for (int i=0; i<numBaseCells; i++) {
        maxTissueType = max(tissueTypes->GetTuple1(i), maxTissueType);
    }
    if (maxTissueType < 0) {
        cout << "ERROR: No positive tissue types are set!" << endl;
        return false;
    }
    int tumorIdx = (int)maxTissueType + 1;

    // set tissue type to tumorIdx for tumor cells
    int numTumorCells = 0;
    for (int i=0; i<numBaseCells; i++) {
        if (numPointsOverlappingTumor[i] == 4) {
            tissueTypes->SetTuple1(i, tumorIdx);
            numTumorCells++;
        }
    }

    cout << numTumorCells << " tumor cells were tagged!" << endl;

    // choose sources from the tumor cells

    // divide the entire mesh into a grid of cubes of side length sourceSpacing and take all those tetras that are within the tumor region
    vtkSmartPointer<vtkCellLocator> cellLocator = 
            vtkSmartPointer<vtkCellLocator>::New();
    cellLocator->SetDataSet(baseGrid);
    cellLocator->BuildLocator();

    double testPoint[3];
    vtkIdType cellId;
    double closestPoint[3]; // needed by FindClosestPoint
    double closestPointDist2; // needed by FindClosestPoint
    int subId; // needed by FindClosestPoint

    cout << "About to tag sources" << endl;
    double stepSize = sourceSpacing * meshScale;
    for (testPoint[0] = lowerBounds[0]; testPoint[0] < upperBounds[0]; testPoint[0] += stepSize) {
        for (testPoint[1] = lowerBounds[1]; testPoint[1] < upperBounds[1]; testPoint[1] += stepSize) {
            for (testPoint[2] = lowerBounds[2]; testPoint[2] < upperBounds[2]; testPoint[2] += stepSize) {
                // find closest cell
                cellLocator->FindClosestPoint(testPoint, closestPoint, cellId, subId, closestPointDist2);

                // check if it's in the tumor region, if so then tag it
                if (tissueTypes->GetTuple1(cellId) == tumorIdx) {
                    if (pointSources) {
                        stringstream ss;
                        ss << testPoint[0] << " " << testPoint[1] << " " << testPoint[2];
                        sourceCells.push_back(ss.str());
                    }
                    else { // volume source
                        stringstream ss;
                        ss << cellId;
                        
                        // make sure it's unique
                        bool unique = true;
                        for (int i=0; i<sourceCells.size(); i++) {
                            if (sourceCells[i] == ss.str()) {
                                unique = false;
                                break;
                            }
                        }
                        if (unique) {
                            sourceCells.push_back(ss.str());
                            cout << "tagged a source at cellId = " << ss.str() << endl;
                        }
                        else {
                            cout << "Note: Found repeated source id at cellId = " << cellId << endl;
                        } 
                    }
                }
            }
        }
    }
    cout << "Finished tagging sources" << endl;

    // write new VTK file with tumor tagged
    vtkSmartPointer<vtkUnstructuredGridWriter> writer = vtkSmartPointer<vtkUnstructuredGridWriter>::New();
    writer->SetFileName(outputFile.c_str());
    writer->SetInputData(baseGrid);
    writer->Write();

    for (int i=0; i<numBasePoints; i++) {
        delete pointsToCells[i];
    }

    return true;
}
