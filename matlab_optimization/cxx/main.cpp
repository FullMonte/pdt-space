#include <iostream>
#include <sstream>

#include "TumorTagger.h"
#include "generate_scripts.cpp"
#include "FluenceReader.h"

#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_RESET   "\x1b[0m"

int main(int argc, char* argv[]) {
    int stepIdx;
    int numRuns = 0;
    int numParams = 0;
    string meshName;
    bool validArgs = true;
    if (argc < 4 || argc > 5) {
        validArgs = false;
    }
    else {
        meshName = argv[1];

        stepIdx = argv[3][0] - 48;

        if (((stepIdx == 0 || stepIdx == 3) && argc == 4) || (stepIdx != 0 && stepIdx != 3 && argc == 5)) {
            validArgs = false; // need to specify numParams if running the optimization part
        }
        else {
            // convert ASCII to int
            string tmp = argv[2];
            for (int i=0; i<tmp.size(); i++) {
                numRuns *= 10;
                int tmp2 = tmp[i] - 48;
                numRuns += tmp2;
                if (tmp2 < 0 || tmp2 > 9) {
                    fprintf(stderr, ANSI_COLOR_RED "ERROR: Second argment must be a positive integer!" ANSI_COLOR_RESET "\n");
                    validArgs = false;
                    break;
                }
            }

            if (stepIdx == 0 || stepIdx == 3) {
                // convert ASCII to int
                tmp = argv[4];
                for (int i=0; i<tmp.size(); i++) {
                    numParams *= 10;
                    int tmp2 = tmp[i] - 48;
                    numParams += tmp2;
                    if (tmp2 < 0 || tmp2 > 9) {
                        fprintf(stderr, ANSI_COLOR_RED "ERROR: Second argment must be a positive integer!" ANSI_COLOR_RESET "\n");
                        validArgs = false;
                        break;
                    }
                }
            }
        }
    }

    if (!validArgs) {
        cout << "Usage:" << endl;
        cout << "   TumorTagger meshName numRuns stepIdx <numParams>     - runs some or all steps (depending on stepIdx), n times with different tumors," <<
                                                                        " for the mesh /var/fullmonte/<meshName>/meshes/<meshName>.vtk, where the steps are:" << endl;
        cout << "       0: run all steps." << endl;
        cout << "       1: tag an elliptical tumor at a random position and generate a corresponding FullMonte TCL script." << endl;
        cout << "       2: read fluences & tissue types from FullMonte output, write to txt files." << endl;
        cout << "       3: run optimization in matlab." << endl;

        return 1;
    }

    int totalPower = 400; // TODO

    cout << "numRuns = " << numRuns << endl << endl;

    string basePath = "/home/wkingsford/pdt_source_placement/";
    string dataPath = "/var/fullmonte/" + meshName + "/";

    // Whether or not to clean up temporary files during process. Useful for batch runs.
    // Deleted files can be recreated with command-line argument of 2
    bool deleteFiles = true;

    // if true, will tag tetras selected to be sources with tissue type tumorIdx + 1.
    // this does not affect simulation or optimization in any way, just for visualization.
    bool tagSources = true;

    // set to true if fixed sources are being used (only changes the name of the fluences files being read and written)
    bool fixedSources = false;

    string inputFileName = dataPath + "meshes/" + meshName + ".vtk";
    string outputFilePfx = dataPath + "extracted_data/" + meshName;
    if (fixedSources) {
        outputFilePfx += "_fixed";
    }

    for (int meshNumber = 1; meshNumber <= numRuns; meshNumber++) {
        string taggedFileName;
        if (stepIdx == 0 || stepIdx == 1) { // tagging a random tumor
            taggedFileName = outputFilePfx + "_tagged_" + to_string(meshNumber) + ".vtk";
        }
        else { // using a pre-tagged tumor from Yiwen
            taggedFileName = dataPath + "meshes/" + meshName + "_tagged_tumor" + to_string(meshNumber) + ".vtk";
        }

        string FullMonteScript = "/usr/local/fullmonte_rc1/bin/tclmonte.sh";
        string optFileName = dataPath + "/630nm_tumor.opt";
        string tclFileName = dataPath + "/TCL_scripts/sourceArray_tumor" + to_string(meshNumber) + ".tcl";

        vector<int> sourceCells; // will contain cellId's of tumor cells to be used as volume sources by FullMonte
        int tumorIdx = -1; // will contain tissue type that corresponds to the tumor region
        int numCells = -1; // will contain number of tetras in original mesh, used for basic error-checking 
        int numPoints = -1; // needed by tag_tumor
        bool success = false;

        if (stepIdx == 0 || stepIdx == 1) {
            //
            // tag tumor
            //

            success = tag_tumor(inputFileName, taggedFileName, sourceCells, 10.0, 0.5, tumorIdx, numCells, numPoints, tagSources);
            if (!success) {
                cout << "tag_tumor failed!" << endl;
                return 1;
            }
            if (sourceCells.size() == 0) {
                fprintf(stderr, ANSI_COLOR_RED "ERROR: tag_tumor did not label any cells as sources!" ANSI_COLOR_RESET "\n");
                return 1;
            }
            if (tumorIdx < 0) {
                fprintf(stderr, ANSI_COLOR_RED "ERROR: tumorIdx was not set!" ANSI_COLOR_RESET "\n");
                return 1;
            }

            //
            // create FullMonte script with given sources
            //

            std::vector<std::string> sourceCellsString(sourceCells.size());
            for (int i=0; i<sourceCells.size(); i++) {
                std::stringstream ss;
                ss << sourceCells[i];
                sourceCellsString[i] = ss.str();
            }

            make_tcl_script_absorption(tclFileName, taggedFileName.substr(0,taggedFileName.length() - 4), optFileName, sourceCellsString, false, true, false);
        }

        // run FullMonte script
        if (stepIdx == 0) {
            int command = system((FullMonteScript + " " + tclFileName).c_str());

            if (command == 0) {
                cout << "Script terminated with no error code." << endl;
            }
            else {
                fprintf(stderr, ANSI_COLOR_RED "ERROR: FullMonte tcl script terminated with nonzero exit code of %d!" ANSI_COLOR_RESET "\n", command);
                return 1;
            }
        }

        if (stepIdx == 0 || stepIdx == 2) {
            //
            // read fluences and tissue types from vtk file, write to matlab vectors and matrices
            //

            string fluencesFile;
            if (fixedSources) {
                fluencesFile = taggedFileName.substr(0,taggedFileName.length() - 4) + "_fixed_sources_fluences.vtk";                
            }
            else {
                fluencesFile = taggedFileName.substr(0,taggedFileName.length() - 4) + "_fluences.vtk";
            } 

            success = read_fluence(fluencesFile, outputFilePfx, sourceCells.size(), numCells, meshNumber);
            if (!success) {
                cout << "read_fluence failed!" << endl;
                return 1;
            }
        }

        if (stepIdx == 0 || stepIdx == 3) {
            //
            // run optimization
            //
            string matlabPath = basePath + "matlab_optimization/matlab/run_PowerOptimizer.sh /usr/local/MATLAB/MATLAB_Runtime/v901 " + 
                    dataPath + " " +              // base_path
                    meshName + " " +              // mesh_name
                    to_string(meshNumber) + " "    // mesh_number
                    "1000000 " +                  // num_packets
                    to_string(totalPower) + " " + // total_energy (in Joules)
                    "500 " +                      // dvh_percentage_range (maximum target-dose percentage to show on dose-volume histogram)
                    to_string(numParams);                          // num_params (range of slopes to try)
            int command = system(matlabPath.c_str());

            if (command == 0) {
                cout << "Matlab terminated with no error code." << endl;
            }
            else {
                fprintf(stderr, ANSI_COLOR_RED "ERROR: Matlab executable terminated with nonzero exit code of %d!" ANSI_COLOR_RESET "\n", command);
                return 1;
            }
        }

        /*
        if (stepIdx == 0 && deleteFiles) {
            // command to delete all unneeded text files when done (can be re-created by running this program with argument 2)
            int command = system(("rm " + basePath + "data/" + meshName + "/*.txt").c_str());

            if (command == 0) {
                cout << "Deleted unneeded files with no error code." << endl;
            }
            else {
                fprintf(stderr, ANSI_COLOR_RED "ERROR: Deleting unneeded files terminated with nonzero exit code of %d!" ANSI_COLOR_RESET "\n", command);
                return 1;
            }
        }
        */
    }

    return 0;
}
