#include "FluenceReader.h"

using namespace std;

// read_fluence reads a vtk file produced by FullMonte after a simulation, produces text files
// containing the fluences, tissue types, tetra volumes, source centers, and pairwise distances 
// between sources
// inputFileName: vtk file with fluences to read
// outputFilePfx: prefix of filename to write outputs to, e.g. will write fluences to ${outputFilePfx}_${meshNumber}_fluences.txt
// numSources: number of sources FullMonte was run with, i.e. number of fluence arrays in file
// numCellsOriginal: number of cells in original file, to make sure this wasn't changed by FullMonte (e.g. as a result of the 
//          VTKLegacyReader renumberZero method)
// meshNumber: used to tag output files from different meshes (e.g. tumor1 vs tumor2)
bool read_fluence(string inputFileName, string outputFilePfx, int numSources, int numCellsOriginal, int meshNumber) {
    string outputFilePfx2 = outputFilePfx + "_" + to_string(meshNumber);

    //read all the data from the file
    vtkSmartPointer<vtkUnstructuredGridReader> reader = vtkSmartPointer<vtkUnstructuredGridReader>::New();
    reader->SetFileName(inputFileName.c_str());
    reader->Update();

    vtkSmartPointer<vtkUnstructuredGrid> unstructuredGrid = reader->GetOutput();

    vtkSmartPointer<vtkCellData> cells = unstructuredGrid->GetCellData();
    int numCells = unstructuredGrid->GetNumberOfCells();

    // numCellsOriginal of -1 is when it wasn't counted by the previous process, e.g. read_fluence is being run on its own
    if (numCellsOriginal != -1 && numCells != numCellsOriginal) {
        cout << "ERROR: Number of cells has changed from " << numCellsOriginal << " to " << numCells << " after FullMonte was run! " <<
                        "Is this because \"R renumberZero false\" (where R is the VTKLegacyReader) was not done in the TCL script?" << endl;
        return false;
    }

    // check that names of all data arrays are correct (i.e. this is the data we're expecting)
    int idx = 0;

    vtkSmartPointer<vtkDataArray> array = cells->GetArray(idx);

    // if number of sources is specified, use it to double-check that there are the right number of fluence arrays. If not, count the number of sources.
    // either way, read off the list of sources
    vector<int> sourceIds;
    int tissueTypeIdx = -1;
    if (numSources > 0) {
        sourceIds.reserve(numSources);
        while (array != NULL) {
            string arrayName(array->GetName());
            cout << "Array " << idx << ":" << arrayName << endl;
            bool isRegionArray = arrayName.compare(string("Tissue Type")) == 0 || arrayName.compare(string("Region")) == 0;
            if (isRegionArray) {
                if (tissueTypeIdx == -1) {
                    cout << "Tissue type array at index " << idx << endl;
                    tissueTypeIdx = idx;
                }
                else {
                    cout << "ERROR: Multiple tissue type arrays present in vtk file!" << endl;
                    return false;
                }
            }
            else if (idx > numSources) {
                cout << "ERROR: There should be only " << (numSources+1) << " arrays in the file." << endl;
                return false;
            }
            else if (arrayName.compare(0,7,string("Fluence")) == 0) {
                // read off the id's of the sources, so that the distance matrix can be produced for those sources
                sourceIds.push_back(std::stoi(arrayName.substr(7, string::npos)));
            }
            else {
                cout << "ERROR: First " << numSources << " arrays in vtk file should have names beginning with \"Fluence\", or be \"Tissue type\" or \"Region\"!" << endl;
                return false;
            }
            idx++;
            array = cells->GetArray(idx);
        }
    }
    else {
        while (array != NULL) {
            string arrayName(array->GetName());
            cout << "Array " << idx << ":" << arrayName << endl;
            bool isRegionArray = arrayName.compare(string("Tissue Type")) == 0 || arrayName.compare(string("Region")) == 0;
            if (isRegionArray) {
                if (tissueTypeIdx == -1) {
                    cout << "Region array at index " << idx << endl;
                    tissueTypeIdx = idx;
                }
                else {
                    cout << "ERROR: Multiple region arrays present in vtk file!" << endl;
                    return false;
                }
            }
            else if (arrayName.compare(0,7,string("Fluence")) == 0) {
                // read off the id's of the sources, so that the distance matrix can be produced for those sources
                sourceIds.push_back(std::stoi(arrayName.substr(7, string::npos)));
                numSources++;
            }
            else {
                cout << "ERROR: Invalid array name at array idx " << idx << "." << endl;
                return false;
            }
            idx++;
            array = cells->GetArray(idx);
        }
    }
    cout << endl << endl;
    if (tissueTypeIdx == -1) {
        cout << "ERROR: No tissue type array found in the vtk file!" << endl;
        return false;
    }

    if (sourceIds.size() != numSources) {
        cout << "ERROR: sourceIds.size() = " << sourceIds.size() << " != numSources = " << numSources << endl;
        return false;
    }

    // write fluences to a file
    string fluenceFileName = outputFilePfx2 + "_fluences.txt";
    FILE* fluenceFile = fopen(fluenceFileName.c_str(), "w");
    if (fluenceFile == NULL) {
        cout << "ERROR: fluenceFile failed to open!" << endl;
        return false;
    }

    vtkSmartPointer<vtkDataArray> *fluences = new vtkSmartPointer<vtkDataArray>[numSources];

    for (int j=0; j<numSources; j++) {
        fluences[j] = cells->GetArray(j);
    }

    // print comma-delimited matrix for matlab to read
    for (int i=0; i<numCells; i++) {
        for (int j=0; j<(numSources-1); j++) {
            fprintf(fluenceFile, "%e,", fluences[j]->GetTuple1(i));
        }
        fprintf(fluenceFile, "%e\n", fluences[numSources-1]->GetTuple1(i));
    }
    delete[] fluences;
    fclose(fluenceFile);

    // write tissue types to a file
    string tissueFileName = outputFilePfx2 + "_tissues.txt";
    FILE* tissueFile = fopen(tissueFileName.c_str(), "w");
    if (tissueFile == NULL) {
        cout << "ERROR: tissueFile failed to open!" << endl;
        return false;
    }
    vtkSmartPointer<vtkDataArray> tissueTypes = cells->GetArray(tissueTypeIdx);
    for (int i=0; i<numCells; i++) {
        fprintf(tissueFile, "%d\n", (int) tissueTypes->GetTuple1(i));
    }
    fclose(tissueFile);

    // write tetrahedron volumes to a file
    string volumeFileName = outputFilePfx2 + "_volumes.txt";
    FILE* volumeFile = fopen(volumeFileName.c_str(), "w");
    if (volumeFile == NULL) {
        cout << "ERROR: volumeFile failed to open!" << endl;
        return false;
    }
    for (int i=0; i<numCells; i++) {
        vtkSmartPointer<vtkCell> cell = unstructuredGrid->GetCell(i);
        vtkSmartPointer<vtkPoints> points = cell->GetPoints();
        if (points->GetNumberOfPoints() != 4) {
            cout << "ERROR: Non-tetrahedral point encountered in mesh!" << endl;
            return false;
        }

        // volume = (1/6) * (AB x AC) dot AD
        double ab[3] = { points->GetPoint(1)[0] - points->GetPoint(0)[0],
                         points->GetPoint(1)[1] - points->GetPoint(0)[1],
                         points->GetPoint(1)[2] - points->GetPoint(0)[2] };
        double ac[3] = { points->GetPoint(2)[0] - points->GetPoint(0)[0],
                         points->GetPoint(2)[1] - points->GetPoint(0)[1],
                         points->GetPoint(2)[2] - points->GetPoint(0)[2] };
        double ad[3] = { points->GetPoint(3)[0] - points->GetPoint(0)[0],
                         points->GetPoint(3)[1] - points->GetPoint(0)[1],
                         points->GetPoint(3)[2] - points->GetPoint(0)[2] };
        double volume = abs((1.0 / 6.0) * (ad[0] * (ab[1] * ac[2] - ab[2] * ac[1]) + 
                                           ad[1] * (ab[2] * ac[0] - ab[0] * ac[2]) + 
                                           ad[2] * (ab[0] * ac[1] - ab[1] * ac[0])));


        fprintf(volumeFile, "%e\n", volume);
    }
    fclose(volumeFile);

    // write matrix of pairwise distances between sources to a file
    // TODO: could make use of the fact that this matrix is symmetric to use less storage/memory. Currently more concerned about runtime than storage/memory
    
    // compute centers
    vector<vector<double>> centers(numSources, vector<double>(3,0.0));
    for (int i=0; i<numSources; i++) {
        vtkSmartPointer<vtkCell> cell = unstructuredGrid->GetCell(sourceIds[i]);
        vtkSmartPointer<vtkPoints> points = cell->GetPoints();
        if (points->GetNumberOfPoints() != 4) {
            cout << "ERROR: Non-tetrahedral point encountered in mesh!" << endl;
            return false;
        }

        for (int j=0; j<3; j++) {
            for (int k=0; k<4; k++) {
                centers[i][j] += points->GetPoint(k)[j];
            }
            centers[i][j] /= 4.0;
        }
    }
    // compute pairwise distances
    vector<vector<double>> centerDistances(numSources, vector<double>(numSources, 0.0));
    for (int i=0; i<numSources; i++) {
        for (int j=0; j<i; j++) {
            for (int k=0; k<3; k++) {
                centerDistances[i][j] += (centers[i][k] - centers[j][k]) * (centers[i][k] - centers[j][k]);
            }
            centerDistances[i][j] = sqrt(centerDistances[i][j]);
            centerDistances[j][i] = centerDistances[i][j]; // symmetry
        }
    }

    cout << "Printing centerDistances for source 0 (cellId = " << sourceIds[0] << "):" << endl;
    for (int i=0; i<numSources; i++) {
        cout << "centerDistances[0][" << i << "] = " << centerDistances[0][i] << endl;
    }

    // write center positions to a file
    string centersFileName = outputFilePfx2 + "_source_centers.txt";
    FILE* centersFile = fopen(centersFileName.c_str(), "w");
    if (centersFile == NULL) {
        cout << "ERROR: centersFile failed to open!" << endl;
        return false;
    }
    for (int i=0; i<numSources; i++) {
        fprintf(centersFile, "%e,%e,%e\n", centers[i][0], centers[i][1], centers[i][2]);
    }
    fclose(centersFile);

    // write center distances to a file
    string distanceFileName = outputFilePfx2 + "_source_distances.txt";
    FILE* distanceFile = fopen(distanceFileName.c_str(), "w");
    if (distanceFile == NULL) {
        cout << "ERROR: distanceFile failed to open!" << endl;
        return false;
    }
    for (int i=0; i<numSources; i++) {
        for (int j=0; j<(numSources-1); j++) {
            fprintf(distanceFile, "%e,", centerDistances[i][j]);
        }
        fprintf(distanceFile, "%e\n", centerDistances[i][numSources-1]);
    }
    fclose(distanceFile);

    return true;
}
