cmake_minimum_required(VERSION 2.8)
 
PROJECT(TumorTester)

set (CMAKE_CXX_STANDARD 11)
 
find_package(VTK REQUIRED)
if (VTK_FOUND)
    include_directories(SYSTEM ${VTK_INCLUDE_DIR})
endif()

add_executable(TumorTester main.cpp TumorTagger.cpp FluenceReader.cpp)
 
if(VTK_LIBRARIES)
  target_link_libraries(TumorTester ${VTK_LIBRARIES})
else()
  target_link_libraries(TumorTester vtkHybrid vtkWidgets)
endif()
