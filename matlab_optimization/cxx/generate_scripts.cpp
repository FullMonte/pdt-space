// Mostly copied from Abed Yassine's generate_scripts.cxx from https://github.com/AbedYassine/pdt_source_placement

#include<string>

using std::string;

/* This function generates a tcl script to run FullMonte with the input source positions,
 * one source at a time. If point_sources = true, the strings in the sources vector
 * should be of the form "x y z" where x,y,z are the coordinates of the source. If
 * point_sources = false, the strings should be of the form "x" where x is the tetrahedron
 * id of each tetrahedron that is being used as a volume source.
 * The script outputs the amount of fluence that passes through each tetrahedron
 */
void make_tcl_script_absorption(string out_file_name, 
            string mesh_path, string opt_path, const vector<string> & sources,
            bool point_sources, bool read_mesh_from_vtk, bool window_manager_exists)
{
    fprintf(stderr, "\033[1;%dm\nCreating a TCL FullMonte Absorption Script: %s\033[0m\n", 33, out_file_name.c_str());

    if (sources.size() == 0)
    {
        fprintf(stderr, "\033[1;%dmNo sources provided! Exiting... \033[0m\n", 31);
        return;
    }

    // Creating output file
    stringstream out_file_ss; 
    FILE* outfile = fopen(out_file_name.c_str(), "w");
    
    // Importing VTK TCK Package
    fprintf(outfile, "##### Import VTK TCL Package \n");
    fprintf(outfile, "package require vtk \n \n");

    // Close the default TCL/TK window
    if (window_manager_exists) {
        fprintf(outfile, "##### Close the default tcl/tk window\n");
        fprintf(outfile, "wm withdraw .\n\n");
    }

    // Load required packages
    fprintf(outfile, "##### Loading the required packages\n\n");
    
    fprintf(outfile, "##### TIMOS format reader (for Digimouse) with TCL bindings\n");
    fprintf(outfile, "load libFullMonteTIMOSTCL.so\n\n");

    fprintf(outfile, "##### Geometry model with TCL bindings\n");
    fprintf(outfile, "load libFullMonteGeometryTCL.so\n\n");

    fprintf(outfile, "##### Software kernels with TCL bindings\n");
    fprintf(outfile, "load libFullMonteSWKernelTCL.so\n");
    fprintf(outfile, "load libFullMonteKernelsTCL.so\n\n");

    fprintf(outfile, "##### Data output manipulation\n");
    fprintf(outfile, "load libFullMonteDataTCL.so\n");
    fprintf(outfile, "load libFullMonteQueriesTCL.so\n\n");

    fprintf(outfile, "##### VTK interface\n");
    fprintf(outfile, "load libvtkFullMonteTCL-6.3.so\n");
    fprintf(outfile, "load libFullMonteVTKFileTCL.so\n\n");

    fprintf(outfile, "puts \"loaded libs\" \n\n");

    // File parameters
    fprintf(outfile, "##### Basic parameters: file name \n\n");

    fprintf(outfile, "#default file prefix\n");
    fprintf(outfile, "set pfx \"%s\"\n\n", mesh_path.c_str());

    fprintf(outfile, "##### override with 1st cmdline arg\n");
    fprintf(outfile, "if { $argc >= 1 } { set pfx [lindex $argv 0] }\n\n");

    // Setting optical properties and mesh files
    fprintf(outfile, "set optfn \"%s\"\n", opt_path.c_str());
    if (read_mesh_from_vtk)
        fprintf(outfile, "set meshfn \"$pfx.vtk\"\n");
    else
        fprintf(outfile, "set meshfn \"$pfx.mesh\"\n");

    // Reading problem definitions in TIMOS or VTK format
    fprintf(outfile, "VTKLegacyReader R\n");
    fprintf(outfile, "R setFileName $meshfn\n\n");

    fprintf(outfile, "R renumberZero false\n\n");

    fprintf(outfile, "TIMOSAntlrParser TR\n\n");

    fprintf(outfile, "TR setOpticalFileName $optfn\n\n");

    fprintf(outfile, "set mesh [R mesh]\n");
    fprintf(outfile, "set opt [TR materials_simple]\n\n");


    // Configuring simulation kernel
    fprintf(outfile, "##### Create and configure simulation kernel with surface scoring\n");

    fprintf(outfile, "set M [TetraMesh M $mesh]\n");
    fprintf(outfile, "TetraVolumeKernel k $M\n\n");
    fprintf(outfile, "k energy 1\n      # total energy\n"); // Candidate sources are all of unit power
    fprintf(outfile, "k materials $opt\n        # materials\n");
    fprintf(outfile, "k setUnitsToMM\n      # units for mesh dimensions "
                    "& optical properties (must match each other)\n");
    
    // Monte carlo kernel properties
    fprintf(outfile, "\n##### Monte Carlo kernel properties\n");
    fprintf(outfile, "k roulettePrWin 0.1\n        # probability of roulette win\n");
    fprintf(outfile, "k rouletteWMin 1e-5\n        # minimum weight "
                "\"wmin\" before roulette takes effect\n");
    fprintf(outfile, "k maxSteps 100000\n        # maximum"
                " number of steps to trace a packet\n");
    fprintf(outfile, "k maxHits 1000\n        # maximum number"
                "of boundaries a single step can take\n");
    fprintf(outfile, "k packetCount 1000000\n"       
                    "        # number of packets to simulate"
                    " (more -> better quality, longer run)\n");
    fprintf(outfile, "k threadCount 8\n        # number of threads "
                    "(set to number of cores, or 2x number of cores if hyperthreading\n\n");

    
    // Defining progress timer call back function for use during simulation run
    fprintf(outfile, "##### Define progress timer callback function for use during simulation run\n");

    fprintf(outfile, "\nproc progresstimer {} {\n"
                "   # loop while not finished\n"
                "   while { ![k done] } {\n"
                    "       # display %% completed to 2 decimal places\n"
                    "       puts -nonewline [format \"\\rProgress %%6.2f%%%%\" [expr 100.0*[k progressFraction]]]\n"
                    "       flush stdout\n\n"

                    "       # refresh interval: 200ms\n"
                    "       after 200\n"
                "   }\n"
                "   puts [format \"\\rProgress %%6.2f%%%%\" 100.0]\n"
            "}\n\n");

    // Setting up internal fluence counting 
    stringstream vtk_v_file_name;
    vtk_v_file_name << out_file_name.substr(0, out_file_name.length()-4) << "_volume.vtk";
    fprintf(outfile, "###### VTK output pipeline\n\n"
                     "# wrap FullMonte mesh and make available as vtkPolyData & vtkUnstructuredGrid\n"
                     "vtkFullMonteTetraMeshWrapper VTKM\n"
                     "  VTKM mesh $M\n\n"
                     "# Create fluence wrapper\n"
                     "vtkFullMonteArrayAdaptor vtkPhi\n"
                     
                     "vtkFieldData volumeFieldData\n"

                     "vtkDataObject volumeDataObject\n"
                     "  volumeDataObject SetFieldData volumeFieldData\n\n"

                     "# merge fluence wrapper onto the tetras of the mesh\n"
                     "vtkMergeDataObjectFilter mergeVolume\n"
                     "  mergeVolume SetDataObjectInputData volumeDataObject\n"
                     "  mergeVolume SetInputData [VTKM blankMesh]\n"
                     "  mergeVolume SetOutputFieldToCellDataField\n\n"

                     "vtkUnstructuredGridWriter VW\n"
                     "  VW SetInputConnection [mergeVolume GetOutputPort]\n\n"
                     
                     "EnergyToFluence EVF\n"
                     "  EVF mesh $M\n"
                     "  EVF materials $opt\n\n");
    
    // Looping over all the sources
    fprintf(outfile, "##### Initializing array of tetrahedral sources\n");
    int tetra_idx;
    for(unsigned int i = 0; i < sources.size(); i++)
    {
        if (point_sources) {
            fprintf(outfile, "set sources_array(%d) \"%s\"\n", i, sources[i].c_str()); 
        }
        else {
            fprintf(outfile, "set sources_array(%d) %s\n", i, sources[i].c_str());
        }
    }
    fprintf(outfile, "\n");

    
    // TCL commands to loop over the sources
    fprintf(outfile, "##### Looping over the sources array\n");
    fprintf(outfile, "for {set index 0} { $index < [array size sources_array] } { incr index} {\n");

    // Configuring sources
    fprintf(outfile, "  ##### Configuring Sources\n");
    if (point_sources) {
        fprintf(outfile, "  Point S\n");
        fprintf(outfile, "  S position $sources_array($index)\n\n");
    }
    else {
        // The sources are volume elements with unit power
        fprintf(outfile, "  Volume S 1.0 $sources_array($index)\n");
    }

    fprintf(outfile, "  # the source to launch from \n");
    fprintf(outfile, "  k source S \n");

    // Running the kernel
    fprintf(outfile, "  ##### Run it\n\n  ##### Run Kernel, display progress timer, and await finish\n");
    fprintf(outfile, "  k startAsync\n  progresstimer\n  k finishAsync\n\n");

    // Gathering data
    stringstream vtk_file_name;
    vtk_file_name << "../TCL_scripts/" << out_file_name.substr(0, out_file_name.length()-4) << "_volume.vtk";

    fprintf(outfile, "  EVF source [k getResultByIndex 2]\n"
                     "  EVF update\n\n"

                     "  vtkPhi source [EVF result]\n"
                     "  volumeFieldData AddArray [vtkPhi array]\n"
                     "  [vtkPhi array] SetName \"Fluence$index\"\n");
    fprintf(outfile, "}\n\n");

    fprintf(outfile, "set regions [VTKM regions]\n"
                     "$regions SetName \"Tissue Type\"\n"

                     "volumeFieldData AddArray $regions\n");

    fprintf(outfile, "VW SetFileName \"${pfx}_fluences.vtk\"\n"
                     "VW Update");

    // closing the file
    fclose(outfile);
    fprintf(stderr, "\033[1;%dmScript Generated!\033[0m\n\n", 36);
}
