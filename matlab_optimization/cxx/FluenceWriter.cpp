#include <vtkSmartPointer.h>
#include <vtkPointData.h>
#include <vtkCellData.h>
#include <vtkSphereSource.h>
#include <vtkMatrixMathFilter.h>
#include <vtkPolyDataMapper.h>
#include <vtkActor.h>
#include <vtkRenderWindow.h>
#include <vtkRenderer.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkUnstructuredGridReader.h>
#include <vtkDataSetSurfaceFilter.h>
#include <vtkUnstructuredGridWriter.h>
#include <vtkPoints.h>
#include <vtkUnstructuredGrid.h>
#include <vtkDataArray.h>
#include <vtkAbstractArray.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <vtkDoubleArray.h>

using namespace std;

int main(int, char *[]) {
    string basePath = "/home/wkingsford/pdt_source_placement/";

    int runNumber = 4;

    string inputFileName = basePath + "data/Colin27/Colin27_tagged_" + to_string(runNumber) + "_fluences.vtk";
    string outputFileName = basePath + "data/Colin27/Colin27_optimized_powers.vtk";

    //read all the data from the input file
    vtkSmartPointer<vtkUnstructuredGridReader> reader = vtkSmartPointer<vtkUnstructuredGridReader>::New();
    reader->SetFileName(inputFileName.c_str());
    reader->Update();

    vtkSmartPointer<vtkUnstructuredGrid> unstructuredGrid = reader->GetOutput();

    vtkSmartPointer<vtkCellData> cells = unstructuredGrid->GetCellData();
    int numCells = unstructuredGrid->GetNumberOfCells();

    // print names of all data arrays
    int idx = 0;
    vtkSmartPointer<vtkDataArray> array = cells->GetArray(idx);
    cout << "Listing array names:" << endl;
    vector<int> sourceIds;
    int numSources = 0;
    while (array != NULL) {
        string arrayName(array->GetName());
        cout << "Array " << idx << ":" << arrayName << endl;
        if (arrayName.compare(string("Tissue Type")) == 0 || arrayName.compare(string("Region")) == 0) {
            cout << idx << " fluence arrays are present in the file." << endl;
        }
        else if (arrayName.compare(0,7,string("Fluence")) == 0) {
            // read off the id's of the sources, so that the powers can be written to those tetras
            sourceIds.push_back(std::stoi(arrayName.substr(7, string::npos)));
            numSources++;
        }
        else if (idx > numSources) {
            cout << "ERROR: An array was found after the tissue types array, which should not occur." << endl;
            return false;
        }
        else {
            cout << "ERROR: Invalid array name at array idx " << idx << "." << endl;
            return false;
        }
        idx++;
        array = cells->GetArray(idx);
    }
    cout << endl;

    if (sourceIds.size() != numSources) {
        cout << "ERROR: sourceIds.size() = " << sourceIds.size() << " != numSources = " << numSources << endl;
        return false;
    }

    string fluencesFileName = basePath + "matlab_optimization/output/solutions/LPFluences_" + std::to_string(runNumber) + "_1.txt";
    std::ifstream fluencesInFile(fluencesFileName.c_str());

    string powersFileName = basePath + "matlab_optimization/output/solutions/LPPowers_" + std::to_string(runNumber) + "_1.txt";
    std::ifstream powersInFile(powersFileName.c_str());

    cout << "About to start populating arrays..." << endl;

    double *fluenceVals = new double[numCells];
    double *powerVals = new double[numCells];
    
    for (int i=0; i<numCells; i++) {
        powerVals[i] = 0.0;
    }

    for (int i=0; i<numCells; i++) {
        double fluence;
        fluencesInFile >> fluence;
        fluenceVals[i] = fluence;
    }
    
    for (int i=0; i<sourceIds.size(); i++) {
        double power;
        powersInFile >> power;
        powerVals[sourceIds[i]] = power;
    }
    cout << "finished populating c++ arrays" << endl;

    vtkSmartPointer<vtkDoubleArray> fluences = vtkSmartPointer<vtkDoubleArray>::New();
    fluences->SetName("Total Fluence");
    fluences->SetArray(fluenceVals, numCells, 0);

    vtkSmartPointer<vtkDoubleArray> powers = vtkSmartPointer<vtkDoubleArray>::New();    
    powers->SetName("Source Power");
    powers->SetArray(powerVals, numCells, 0);

    cells->AddArray(fluences);
    cells->AddArray(powers);

    // write new VTK file with fluences/powers tagged
    vtkSmartPointer<vtkUnstructuredGridWriter> writer = vtkSmartPointer<vtkUnstructuredGridWriter>::New();
    writer->SetFileName(outputFileName.c_str());
    writer->SetInputData(unstructuredGrid);
    writer->Write();
}
