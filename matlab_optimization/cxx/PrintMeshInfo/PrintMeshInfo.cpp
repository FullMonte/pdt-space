#include <vtkSmartPointer.h>
#include <vtkPointData.h>
#include <vtkCellData.h>
#include <vtkSphereSource.h>
#include <vtkMatrixMathFilter.h>
#include <vtkPolyDataMapper.h>
#include <vtkActor.h>
#include <vtkRenderWindow.h>
#include <vtkRenderer.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkUnstructuredGridReader.h>
#include <vtkDataSetSurfaceFilter.h>
#include <vtkUnstructuredGridWriter.h>
#include <vtkPoints.h>
#include <vtkUnstructuredGrid.h>
#include <vtkDataArray.h>
#include <vtkAbstractArray.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <vtkDoubleArray.h>

using namespace std;

int main(int, char *[]) {
    string basePath = "/usr/local/share/pdt_opt/Colin27/meshes/";

    for (int runNumber = 1; runNumber<=1; runNumber++) {
	    string inputFileName = basePath + "Colin27_tagged_tumor" + to_string(runNumber) + "_fluences.vtk";

        cout << "Reading file " << inputFileName << "." << endl;

	    //read all the data from the input file
	    vtkSmartPointer<vtkUnstructuredGridReader> reader = vtkSmartPointer<vtkUnstructuredGridReader>::New();
	    reader->SetFileName(inputFileName.c_str());
	    reader->Update();

	    vtkSmartPointer<vtkUnstructuredGrid> unstructuredGrid = reader->GetOutput();

	    vtkSmartPointer<vtkCellData> cells = unstructuredGrid->GetCellData();
	    int numCells = unstructuredGrid->GetNumberOfCells();

	    // print names of all data arrays
	    int idx = 0;
	    vtkSmartPointer<vtkDataArray> array = cells->GetArray(idx);
	    cout << "Listing array names:" << endl;
	    vector<int> sourceIds;
	    int numSources = 0;
	    int tissueTypeIdx = -1;
	    while (array != NULL) {
	        string arrayName(array->GetName());
	        cout << "Array " << idx << ":" << arrayName << endl;
	        bool isRegionArray = arrayName.compare(string("Tissue Type")) == 0 || arrayName.compare(string("Region")) == 0;
	        if (isRegionArray) {
	            if (tissueTypeIdx == -1) {
	                cout << "Region array at index " << idx << endl;
	                tissueTypeIdx = idx;
	            }
	            else {
	                cout << "ERROR: Multiple region arrays present in vtk file!" << endl;
	                return false;
	            }
	        }
	        else if (arrayName.compare(0,7,string("Fluence")) == 0) {
	            // read off the id's of the sources, so that the distance matrix can be produced for those sources
	            sourceIds.push_back(std::stoi(arrayName.substr(7, string::npos)));
	            numSources++;
	        }
	        else {
	            cout << "ERROR: Invalid array name at array idx " << idx << "." << endl;
	            return false;
	        }
	        idx++;
	        array = cells->GetArray(idx);
	    }
	    cout << endl;

	    if (sourceIds.size() != numSources) {
	        cout << "ERROR: sourceIds.size() = " << sourceIds.size() << " != numSources = " << numSources << endl;
	        return false;
	    }

	    string tissueFileName = basePath + "tissue_types_" + to_string(runNumber) + ".txt";
	    FILE* tissueFile = fopen(tissueFileName.c_str(), "w");
	    if (tissueFile == NULL) {
	        cout << "ERROR: tissueFile failed to open!" << endl;
	        return false;
	    }
	    vtkSmartPointer<vtkDataArray> tissueTypes = cells->GetArray(tissueTypeIdx);
	    for (int i=0; i<numCells; i++) {
	        fprintf(tissueFile, "%d\n", (int) tissueTypes->GetTuple1(i));
	    }
	    fclose(tissueFile);

	    string sourcesFileName = basePath + "sources_" + to_string(runNumber) + ".txt";
	    FILE* sourcesFile = fopen(sourcesFileName.c_str(), "w");
	    if (sourcesFile == NULL) {
	        cout << "ERROR: sourcesFile failed to open!" << endl;
	        return false;
	    }
	    for (int i=0; i<sourceIds.size(); i++) {
	    	fprintf(sourcesFile, "%d\n", sourceIds[i]);
	    }
	    fclose(sourcesFile);
	}
}
