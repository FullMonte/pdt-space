/*
 *  This file defins a tumor tagger method that randomly tags a tumor inside the
 *  given tissue in vtk format
 *
 *  @author: William Kinsford
 * 
 */

#ifndef _TUMOR_TAGGER_
#define _TUMOR_TAGGER_

#include "vtk_include.h"

#include <iostream>
#include <fstream>
#include <algorithm>
#include <cmath>
#include <cstdlib>
#include <stdlib.h>
#include <time.h>
#include <vector>
#include <float.h>
#include <string>
#include <sstream>

using namespace std;

/* #define VERBOSE */

// tag_tumor: given a vtk mesh file, tags an elliptical tumor with random semi-axis lengths within a reasonable range,
// centered at a random position such that it is entirely within the mesh, and writes the mesh with the tagged tumor 
// to another vtk file.
// inputFileName: full path to vtk mesh to read
// outputFileName: full path to vtk mesh to create, based off input mesh but with tumor cells tagged
// sourceCells: will be filled with vector of cell indices of cells that were labelled as sources
// meshScale: ratio of units used by mesh to cm (e.g. 10 for mesh in mm)
// sourceSpacing: separation between adjacent sources, in cm
// tumorIdx: will be set to the tissue type that was assigned to the tumor
// numCells: will be set to the total number of cells in the mesh
// numPoints: will be set to the total number of Points in the mesh
// tagSources: if true, will tag tetras selected to be sources with tissue type tumorIdx + 1
bool tag_tumor(string inputFileName, string outputFileName, vector<int> &sourceCells, double meshScale, double sourceSpacing, int &tumorIdx, int &numCells, int &numPoints, bool tagSources);

// takes a mesh (baseFile) and a second mesh that is a subset of the first (tumorFile), and writes a new mesh
// that is a copy of the first but where every cell of the mesh that is also in the tumor mesh is set to a new
// tissue type.
// pointSources: if true, sourceCells will contain strings of the form "x y z" representing point sources, else it will contain strings of the form "n" representing tetrahedron indices
// meshScale: ratio of units used by mesh to cm (e.g. 10 for mesh in mm)
// sourceSpacing: separation between adjacent sources, in cm
bool tag_tumor_from_file(string baseFile, string tumorFile, string outputFile, bool pointSources, vector<string> &sourceCells, double meshScale, double sourceSpacing);

#endif
