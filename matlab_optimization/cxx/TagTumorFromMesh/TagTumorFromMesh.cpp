#include "../TumorTagger.h"
#include "../generate_scripts.cpp"

#include <string>

using namespace std;

int main() {
	string basePath = "/var/fullmonte/Colin27/"; 

    string baseMeshPath = basePath + "meshes/Colin27.vtk";
	string tumorMeshPathPfx = basePath + "meshes/Colin27_tumor";
	string taggedMeshPathPfx = basePath + "meshes/Colin27_tagged_tumor";
	string tclFileNamePfx = basePath + "TCL_scripts/sourceArray_tumor";
//  string optFileName = basePath + "630nm_tumor_old.opt";
	string optFileName = basePath + "675nm.opt";

    for (int i=1; i<=9; i++) {
    	vector<string> sourceCells;

        string tumorMeshPath = tumorMeshPathPfx + to_string(i) + string(".vtk");
        string taggedMeshPath = taggedMeshPathPfx + to_string(i) + string(".vtk");
        string tclFileName = tclFileNamePfx + to_string(i) + string(".tcl");

        bool success = tag_tumor_from_file(baseMeshPath, tumorMeshPath, taggedMeshPath, true, sourceCells, 10, 0.5);
        if (!success) {
            cout << "tag_tumor_from_file failed!" << endl;
            return 1;
        }

        make_tcl_script_absorption(tclFileName, taggedMeshPath.substr(0,taggedMeshPath.length() - 4), optFileName, sourceCells, true, true, false);
    }

	return 0;
}
