function [f, A, b] = LPOptimizerSetup(fluences_matrix, slopes, tumor_overdose_slope, thresholds, tumor_overdose_threshold,...
    tissue_types, volumes, power_limit, regularization_constant, num_tumor_tetras)
% LPOptimizerSetup  Produces matrices used for LP optimization
%   fluences_matrix is num_tetras x num_sources matrix giving fluence deposited at each tetra per unit source intensity
%   slopes is the vector of slopes for violating the given dose thresholds for a given tissue type
%   tumor_overdose_slope is the slope for violating the upper dose threshold for the tumor
%   thresholds is the dose threshold: it is a minimum for tumors (with negative slope), maximum for healthy tissue (with positive slope)
%   tumor_overdose_threshold is the threshold for maximum dose for a tumor
%   tissue_types is the vector of tissue types for each tetra, which is used as the index for the slopes and thresholds vectors
%   volumes is the vector of volume of each tumor, used to weight the cost function
%   power_limit is the maximum total (dimensionless) power between all sources, typically 1. Set to 0 for no limit.
%   regularization_constant is a nonnegative scalar used to introduce sparsity in powers (can be set to 0 for no regularization)
%   num_tumor_tetras is the number of tumor tetrahedra in the mesh
%   sparse_matrix should be set to true for large numbers of tetrahedrons, in case there is not enough contiguous memory for the 
%          whole A matrix

[num_tetras, num_sources] = size(fluences_matrix);

% Sparse matrix for slopes for costs above/below threshold for healthy/tumor cells
% Of form: slope_i * (fluence_i - threshold) <= t_i, where t_i are dummy
% variables
% Fluence_i is the i_th element of Ax where the columns of i are fluences for unit intensity from each source and x is source intensities
num_matrix_elements = (num_sources+1)*num_tetras;
num_constraints = num_tetras;
if tumor_overdose_threshold ~= 0
    num_matrix_elements = num_matrix_elements + (num_sources+1)*num_tumor_tetras;
    num_constraints = num_constraints + num_tumor_tetras;
end
if power_limit ~= 0
    num_matrix_elements = num_matrix_elements + num_sources;
    num_constraints = num_constraints + 1;
end

%if sparse_matrix
    b =    zeros(num_constraints,1);
    rows = zeros(num_matrix_elements,1);
    cols = zeros(num_matrix_elements,1);
    v =    zeros(num_matrix_elements,1);

    tumor_idx = 0;
    for i=1:size(slopes)
        if slopes(i) < 0
            if tumor_idx ~= 0
                fprintf('ERROR: Multiple tissues with negative slopes! Tissue types %d and %d\n', tumor_idx, i);
                return;
            end
            tumor_idx = i;
        end
    end

    % costs for tumor underdose, healthy overdose
    for i=1:num_tetras
        if tissue_types(i) ~= 0 % TODO: tissue type of 0 seems to apply for only the first cell?
            for j=1:num_sources
                v(i+(j-1)*num_tetras) = volumes(i) * slopes(tissue_types(i)) * fluences_matrix(i, j);
                rows(i+(j-1)*num_tetras) = i;
                cols(i+(j-1)*num_tetras) = j;
            end

            b(i) = volumes(i) * slopes(tissue_types(i)) * thresholds(tissue_types(i));
        else
            for j=1:num_sources
                v(i+(j-1)*num_tetras) = 0;
                rows(i+(j-1)*num_tetras) = i;
                cols(i+(j-1)*num_tetras) = j;
            end
            b(i) = 0;
        end
    end

    % dummy variables for tumor underdose, healthy overdose
    for i=1:num_tetras
        v(i+num_sources*num_tetras) = -1;
        rows(i+num_sources*num_tetras) = i;
        cols(i+num_sources*num_tetras) = i+num_sources;
    end

    last_matrix_idx = (num_sources+1)*num_tetras;
    last_vector_idx = num_tetras;

    % cost for tumor overdose
    if tumor_overdose_threshold ~= 0
        fprintf('Applying an upper bound on tumor dose.\n');
        idx = 1;
        for i=1:num_tetras
            if tissue_types(i) == tumor_idx
                for j=1:num_sources
                    v(last_matrix_idx + idx + (j-1)*num_tumor_tetras) = volumes(i) * tumor_overdose_slope * fluences_matrix(i, j);
                    rows(last_matrix_idx + idx + (j-1)*num_tumor_tetras) = num_tetras + idx;
                    cols(last_matrix_idx + idx + (j-1)*num_tumor_tetras) = j;
                end
                b(last_vector_idx + idx) = volumes(i) * tumor_overdose_slope * tumor_overdose_threshold;
                idx = idx + 1;
            end
        end

        if (idx-1) ~= num_tumor_tetras
            fprintf('ERROR: Found %d tumor tetras, but num_tumor_tetras = %d!\n', idx, num_tumor_tetras);
            return;
        end

        last_matrix_idx = last_matrix_idx + num_tumor_tetras*num_sources;

        % dummy variables for tumor overdose
        for i=1:num_tumor_tetras
            v(last_matrix_idx + i) = -1;
            rows(last_matrix_idx + i) = i + num_tetras;
            cols(last_matrix_idx + i) = i + num_sources + num_tetras;
        end

        last_matrix_idx = last_matrix_idx + num_tumor_tetras;
        last_vector_idx = last_vector_idx + num_tumor_tetras;
    end

    % upper bound on total power
    if power_limit ~= 0
        fprintf('Applying a total power limit.\n');
        for i=1:num_sources
            v(last_matrix_idx + i) = 1;
            if tumor_overdose_threshold == 0
                rows(last_matrix_idx + i) = num_tetras + 1;
            else
                rows(last_matrix_idx + i) = num_tetras + num_tumor_tetras + 1;
            end
            cols(last_matrix_idx + i) = i;
        end
        b(last_vector_idx + 1) = power_limit;

        last_matrix_idx = last_matrix_idx + num_sources;
    end

    % sanity check: verify that rows and cols have no nonpositive entries
    for i=1:last_matrix_idx
        if rows(i) <= 0
            fprintf('ERROR: Nonpositive element in rows at idx %d with value %f!\n', i, rows(i));
            return;
        end
        if cols(i) <= 0
            fprintf('ERROR: Nonpositive element in cols at idx %d with value %f!\n', i, cols(i));
            return;
        end
    end

    A = sparse(rows,cols,v);
%{
else
    % TODO: assumes total power limit, doesn't allow for tumor dose upper bound
    % non-sparse matrix
    scaled_fluences_matrix = zeros(num_tetras, num_sources);
    b = zeros(num_tetras+1, 1);
    for i=1:num_tetras
        scaled_fluences_matrix(i, :) = volumes(i) * slopes(tissue_types(i)) *  fluences_matrix(i,:);
        b(i) = volumes(i) * slopes(tissue_types(i)) * thresholds(tissue_types(i));
    end
    b(num_tetras+1) = power_limit;
    A = [scaled_fluences_matrix, diag(-ones(num_tetras,1)); ones(1, num_sources), zeros(1, num_tetras)];
end
%}

% sanity check: verify that the size of A matches size of fluences_matrix, plus one row for total power constraint
% and num_tetras cols for dummy variables
size_offset = [0 num_tetras];
if tumor_overdose_threshold ~= 0
    size_offset = size_offset + [num_tumor_tetras 0];
end
if power_limit ~= 0
    size_offset = size_offset + [1 0];
end

if size(A) ~= (size(fluences_matrix) + size_offset)
    fprintf('ERROR: Size mismatch in LPOptimizerSetup!\n');
    return;
end
% cost function is sum of dummy variables
if tumor_overdose_threshold ~= 0
    f = ones(num_sources + num_tetras + num_tumor_tetras, 1);
else
    f = ones(num_sources + num_tetras, 1);
end
% regularization_constant is to introduce sparsity in powers (can be set to 0 to disable)
for i=1:num_sources
    f(i) = regularization_constant;
end