function RecordCostFunctionStats(base_path, tumor_name, power_algorithm, placement_algorithm, fval, ...
    v_90, d_90, healthy_guard_band, tumor_guard_band, tumor_weight, sum_of_powers, total_energy)
% RecordCostFunctionStats  Writes statistics about the optimization to a file. ONLY WORKS FOR COLIN27

stats_file = fopen(strcat(base_path, 'matlab_optimization_output/cost_function_stats.txt'), 'a');

% avoid the need to transpose or squeeze
v_90 = v_90(:);
d_90 = d_90(:);

if size(v_90, 1) ~= 5 || size(d_90, 1) ~= 5
    fprintf('ERROR in RecordCostFunctionStats: Size of both v_90 and d_90 should be 5! ');
    fprintf('Note that this function is only valid for Colin27.\n');
    quit force;
end

fprintf(stats_file, '%s\t%s\t%s\t%s\t%e\t%e\t%e\t%e\t%e\t%e\t%e\t%f\t%f\t%f\t%f\t%f\n', ...
    datestr(now), tumor_name, power_algorithm, placement_algorithm, fval, v_90(3), d_90(3), ...
    v_90(4), d_90(4), v_90(5), d_90(5), healthy_guard_band, tumor_guard_band, tumor_weight, sum_of_powers, total_energy);

fclose(stats_file);