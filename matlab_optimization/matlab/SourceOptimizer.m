function SourceOptimizer(varargin)

%%% Parameters and setup
valid_args = true;

if nargin ~= 12
    valid_args = false;
else
    base_path = varargin{1};
    mesh_name = varargin{2};
    
    for i=[3 5:12]
        if isnan(str2double(varargin{i}))
            fprintf('ERROR: argument %d is not a number!', i);
            valid_args = false;
        end
    end
    
    mesh_number = str2double(varargin{3});
    wavelength = str2num(varargin{4});
    num_packets = str2double(varargin{5});
    total_energy = str2double(varargin{6});
    dvh_percentage_range = str2double(varargin{7});
    num_real_sources = str2double(varargin{8});
    alg_code = str2double(varargin{9});
    regularization_coefficient = str2double(varargin{10});
    annealer_runtime_limit = str2double(varargin{11});
    lp_alg_idx = str2double(varargin{12});
    
    if mod(mesh_number, 1) ~= 0
        display('ERROR: mesh_number must be an integer!');
        valid_args = false;
    end
    if mod(num_packets, 1) ~= 0
        display('ERROR: num_packets must be an integer!');
        valid_args = false;
    end
    if mod(dvh_percentage_range, 1) ~= 0
        display('ERROR: dvh_percentage_range must be an integer!');
        valid_args = false;
    end
    if mod(num_real_sources, 1) ~= 0
        display('ERROR: num_real_sources must be an integer!');
        valid_args = false;
    end
    if mod(alg_code, 1) ~= 0
        display('ERROR: alg_code must be an integer!');
        valid_args = false;
    end
    if lp_alg_idx ~= 1 && lp_alg_idx ~= 2
        fprintf('ERROR: lp_alg_idx must be 1 or 2!\n');
        valid_args = false;
    elseif lp_alg_idx==1
        alg_name = 'dual-simplex';
    else
        alg_name = 'interior-point';
    end
    
    alg_code = int8(alg_code);
end

if ~valid_args
    display('SourceOptimizer requires 12 arguments. In order:');
    display('base_path: path to the directory containing the mesh data (e.g. /var/fullmonte/Colin27/675nm/)');
    display('mesh_name: name used for the mesh and associated files (e.g. Colin27 or Digimouse)');
    display('mesh_number: ID for this run (e.g. 3 for Colin27_tumor3)');
    fprintf('wavelength: e.g. 675nm, used for naming output files\n');
    display('num_packets: number of packets launched in FullMonte simulation (set in the tcl script)');
    display('total_energy: total energy delivered through all sources, in Joules');
    display('dvh_percentage_range: the maximum dose percentage to show on the x-axis of the dose-volume histograms');
    display('num_real_sources: how many real sources to choose from the set of virtual sources');
    display('alg_code: which algorithms to run (an integer, where the ith algorithm is run if the ith bit is 1)');
    display('regularization_coefficient: for regularizing the LP towards a sparse virtual power vector');
    display('annealer_runtime_limit: how many seconds to run the source-placement annealer for');
    fprintf('lp_alg_idx: the LP algorithm to use: 1 for dual-simplex, 2 for interior-point\n');
    
    return;
end

tumor_name = strcat(mesh_name, '_tumor', num2str(mesh_number), '_', wavelength);

fprintf(strcat('Received parameters: base_path = %s, tumor_name = %s, num_packets = %f, total_energy = %f, ', ...
    ' dvh_percentage_range = %d, alg_code = %d, regularization_coefficient = %f, annealer_runtime_limit = %d\n\n'), ...
    base_path, tumor_name, num_packets, total_energy, dvh_percentage_range, alg_code, ...
    regularization_coefficient, annealer_runtime_limit);

topsrc_results_file = fopen(strcat(base_path, 'matlab_optimization_output/results_top_powers.txt'), 'a');
annealer_results_file = fopen(strcat(base_path, 'matlab_optimization_output/results_annealer.txt'), 'a');
kmeans_results_file = fopen(strcat(base_path, 'matlab_optimization_output/results_kmeans.txt'), 'a');
iterative_results_file = fopen(strcat(base_path, 'matlab_optimization_output/results_iterative.txt'), 'a');

powers_file = fopen(strcat(base_path, 'matlab_optimization_output/powers_', ...
    num2str(num_real_sources), '_sources.txt'), 'w');

total_power_limit = 1; % energy per packet is normalized so that total power of 1 = total_energy delivered
max_source_power = Inf; % TODO: currently not imposing any upper bound on each source

% this is half of the distance between virtual sources in the mesh, so there's no use perturbing by less than this distance
% as it will just return the original point
min_perturbation_distance = 5; % TODO: update this if we use more or fewer sources!

% TODO: set these to be the best values, empirically chosen from fixed sources
healthy_guard_band = 1;%0.9;
tumor_guard_band = 1;%1.1;
tumor_weight = 60;

% read necessary data from files
tic
[slopes, thresholds, hard_thresholds, tumor_overdose_slope, tumor_overdose_threshold, ...
          fluences_matrix, fluences_matrix_full, ...
          tissue_types, tissue_types_full, ...
          volumes, volumes_full, ...
          tumor_idx, num_tumor_tetras, tissue_names, num_tissues ...
          ] = ParseInputData(base_path, mesh_name, mesh_number, num_packets / total_energy, ...
                healthy_guard_band, tumor_guard_band, tumor_weight);
read_time = toc;
fprintf('Initial setup time = %f\n', read_time);
      
num_tetras_full = size(fluences_matrix_full, 1);
num_tetras = size(fluences_matrix, 1);
num_sources = size(fluences_matrix_full, 2);
tumor_slope = slopes(tumor_idx);

tumor_tetra_indices = (tissue_types(:) == tumor_idx);
tumor_tetra_indicator = ones(num_tetras,1) - 2*tumor_tetra_indices; % +1 for non-tumor tetras, -1 for tumor tetras

if num_sources < num_real_sources
    fprintf('ERROR: num_real_sources = %d, but only %d virtual sources are available!\n', num_real_sources, num_sources);
    return;
end

num_algs = 4;
alg_enabled = zeros(num_algs);
for i=1:num_algs
    alg_enabled(i) = mod(alg_code,2);
    alg_code = bitsra(alg_code,1);
end
fprintf('\n');
fprintf('alg_enabled = [');
for i=1:(num_algs-1)
    fprintf('%d, ', alg_enabled(i));
end
fprintf('%d]\n\n', alg_enabled(num_algs));

%%% algorithms 1,2,3,4 use the optimal virtual source powers from Cimmino & LP, so compute that data here
if alg_enabled(1) || alg_enabled(2) || alg_enabled(3) || alg_enabled(4)
    %%% setup for LP optimization
    tic
    [f_lp, A_lp, b_lp] = LPOptimizerSetup(fluences_matrix, slopes, tumor_overdose_slope,...
        thresholds, tumor_overdose_threshold, tissue_types, volumes, total_power_limit, regularization_coefficient,...
        num_tumor_tetras);
    fprintf('LPOptimizerSetup took %f seconds.\n', toc);
    
    %%% run LP optimization
    if tumor_overdose_threshold ~= 0
        ub = Inf(num_sources+num_tetras+num_tumor_tetras, 1);
        lb = zeros(num_sources+num_tetras+num_tumor_tetras, 1);
    else
        ub = Inf(num_sources+num_tetras, 1);
        lb = zeros(num_sources+num_tetras, 1);
    end
    ub(1:num_sources) = max_source_power * ones(num_sources, 1);
    
    virtual_powers_lp = LPOptimizer(f_lp, A_lp, b_lp, lb, ub, num_sources, lp_alg_idx);
    
    fprintf('Optimal power distribution with LP optimization, using all sources:\n');
    for i=1:num_sources
        fprintf('Virtual source %d power = %f\n', i, virtual_powers_lp(i));
    end
    fprintf('Total power = %f\n', sum(virtual_powers_lp));
    
    % choose best sources from LP solution
    best_sources_lp = zeros(num_real_sources,1);
    virtual_powers_lp_copy = virtual_powers_lp; % copy because will need the unmodified version for algorithm 3
    for i=1:num_real_sources
        [~, idx] = max(virtual_powers_lp_copy);
        virtual_powers_lp_copy(idx(1)) = -Inf;
        best_sources_lp(i) = idx(1);
    end
    lp_virtual_source_time = toc;
    fprintf('LP virtual source optimization took %f seconds.\n', lp_virtual_source_time);
    
    %%% setup for Cimmino feasibility algorithm
    tic

    % first num_tetras rows are fluence constraints, last row is total power constraint on sources
    if total_power_limit ~= 0
        A_ls = [ fluences_matrix; ones(1, size(fluences_matrix, 2)) ];
        b_ls = zeros(num_tetras+1, 1);
        b_ls(num_tetras+1) = total_power_limit;
        % weights for different tissue types for Cimmino. Last element is for power constraint
        w = zeros(num_tetras+1, 1);
    else
        A_ls = fluences_matrix;
        b_ls = zeros(num_tetras, 1);
        w = zeros(num_tetras, 1);
    end
    
    tumor_tetra_indices = (tissue_types(:) == tumor_idx);
    tumor_tetra_indicator = ones(num_tetras,1) - 2*tumor_tetra_indices; % +1 for non-tumor tetras, -1 for tumor tetras
    w = slopes(tissue_types(:));
    w = w .* volumes;
    w = w .* tumor_tetra_indicator; % since tumor slope is negative, but weight should be positive
    b_ls(1:num_tetras) = thresholds(tissue_types(:)) .* tumor_tetra_indicator;
    for i=1:num_tetras
        A_ls(i,:) = tumor_tetra_indicator(i) * A_ls(i,:);
    end

    % power constraint must be enforced stronger than other constraints
    if total_power_limit ~= 0
        w(num_tetras+1) = 2 * max(w);
    end
    
    if ~all(w >= 0)
        fprintf('ERROR: Some weights for Cimmino are negative!\n');
        return
    end
    
    %%% run Cimmino optimization
    [virtual_powers_ls, ~] = CimminoOptimizer(A_ls, b_ls, zeros(num_sources,1), max_source_power * ones(num_sources,1),...
        w, zeros(num_sources,1), (total_power_limit~=0), lp_virtual_source_time, false);
    
    fprintf('Optimal power distribution with Cimmino optimization, using all sources:\n');
    for i=1:num_sources
        fprintf('Virtual source %d power = %f\n', i, virtual_powers_ls(i));
    end
    fprintf('Total power = %f\n', sum(virtual_powers_ls));
    
    % choose best sources from Cimmino solution
    best_sources_ls = zeros(num_real_sources,1);
    virtual_powers_ls_copy = virtual_powers_ls; % copy because will need the unmodified version for algorithm 3
    for i=1:num_real_sources
        [~, idx] = max(virtual_powers_ls_copy);
        virtual_powers_ls_copy(idx(1)) = -Inf;
        best_sources_ls(i) = idx(1);
    end
    ls_virtual_source_time = toc;
    fprintf('Cimmino virtual source optimization took %f seconds.\n', ls_virtual_source_time);
end

%%% algorithms 2 and 3 use the distances between sources, so compute that data here
if alg_enabled(2) || alg_enabled(3)
    tic
    % read num_sources*3 array of source positions (for use in k-means clustering)
    source_centers = importdata(strcat(base_path, 'extracted_data/', mesh_name, '_',...
        num2str(mesh_number), '_source_centers.txt'), ',');
    if size(source_centers, 1) ~= num_sources || size(source_centers,2) ~= 3
        fprintf('ERROR: size(source_centers) = %d x %d, when it should be %d by 3!\n',...
                size(source_centers,1), size(source_centers,2), num_sources);
        return;
    end
    % read matrix of distances between sources, use it to make a list of sources within 1,2,3mm of each source
    distance_matrix = importdata(strcat(base_path, 'extracted_data/', mesh_name, '_',...
        num2str(mesh_number), '_source_distances.txt'), ',');
    
%{
    % nearby_sources(i,:) is a list of source indices. The first n(i,j) elements are within distance_limit(j) mm of source i
    nearby_sources = zeros(num_sources, num_sources);
    distance_limit = [10; 20; 30];
    num_stages = size(distance_limit, 1);
    n = zeros(num_sources, num_stages);
    current_idx = 0;
    prev_max_distance = 0;
    for i=1:num_sources
        for distance_idx=1:3
            for j=1:num_sources
                if (distance_matrix(i,j) < distance_limit(distance_idx)) && (distance_matrix(i,j) >= prev_max_distance)
                    current_idx = current_idx + 1;
                    nearby_sources(i, current_idx) = j;
                    %fprintf('Set nearby_sources(%d,%d)=%d\n', i, current_idx, j);
                end
            end
            n(i,distance_idx) = current_idx;
            prev_max_distance = distance_limit(distance_idx);
        end
        current_idx = 0;
        prev_max_distance = 0;
        
        fprintf('Source %d has %d sources within %f mm, %d sources within %f mm, %d sources within %f mm.\n',...
                    i, n(i,1), distance_limit(1), n(i,2), distance_limit(2), n(i,3), distance_limit(3));
        
        % verify that no source was repeated in the list
        sorted = sort(nearby_sources(i,:));
        for j=1:(size(sorted) - 1)
            if (sorted(j) ~= 0) && (sorted(j) == sorted(j+1))
                fprintf('ERROR: nearby_sources list for source %d contains repeats!\n', i);
                fprintf('nearby_sources list is:');
                fprintf('%d, ', nearby_sources(i,:));
                return;
            end
        end
    end
    
    % verify that all slots are filled
    all_slots_filled = true;
    for i=1:num_sources
        for j=1:3
            for k=1:n(i,j)
                if nearby_sources(i,k) <= 0
                    fprintf('Invalid nearby_sources(%d,%d) = %f, where j=%d\n', i, k, nearby_sources(i,k), j);
                    all_slots_filled = false;
                end
            end
        end
    end
    if ~all_slots_filled
        return;
    end
    
    adjacency_time = toc;
    fprintf('Time to set up nearby_sources matrix = %f\n', adjacency_time);
%}
end

%%% Source placement algorithm 1:
% Optimize power with both Cimmino and LP, choose the num_real_sources virtual sources that get assigned the highest
% power, re-optimize power with just those sources.
if alg_enabled(1)
    fprintf(powers_file, '====Top %d source powers method:====\n', num_real_sources);
    
    %%% optimize power across reduced set of sources from Cimmino solution
    A_ls_reduced = A_ls(:, best_sources_ls);
    [real_powers_ls , ls_real_source_time] = CimminoOptimizer(A_ls_reduced, b_ls, zeros(num_real_sources,1), ...
            max_source_power * ones(num_real_sources,1), w, zeros(num_real_sources,1), ...
            (total_power_limit~=0), 0, false);
    
	ls_total_time = ls_virtual_source_time + ls_real_source_time;
        
    fprintf('Optimal power distribution with Cimmino optimization, choosing best sources:\n');
    for i=1:num_real_sources
        fprintf('Source %d (virtual source %d) power = %f\n', i, best_sources_ls(i), real_powers_ls(i));
    end
    fprintf('Total power = %f\n', sum(real_powers_ls));
    
    % produce cumulative dose-volume histogram
    dvh_filename_prefix_top_sources_ls = strcat(base_path, 'matlab_optimization_output/dvh/LS_', ...
        num2str(num_real_sources), '_sources_dvh');
    fluences_ls = fluences_matrix_full(:, best_sources_ls) * real_powers_ls;
    [dose_count_ls, d_90_ls, v_90_ls] = DoseVolumeHistogram(dvh_percentage_range, fluences_ls, hard_thresholds, ...
        tissue_types_full, num_tissues, volumes_full, dvh_filename_prefix_top_sources_ls);
    % evaluate total cost (according to LP cost function)
    % for calculating cost (ignores low-dose tetras)
    fluences_ls_reduced = fluences_matrix(:, best_sources_ls) * real_powers_ls;
    costs_ls = zeros(num_tetras,1);
    for i=1:num_tetras
        if tissue_types(i) ~= 0
            threshold_difference = fluences_ls_reduced(i) - thresholds(tissue_types(i));
            % only count under-dosed tumor cells or over-dosed healthy cells
            if (tissue_types(i) == tumor_idx && threshold_difference < 0) || ...
               (tissue_types(i) ~= tumor_idx && threshold_difference > 0 )
                costs_ls(i) = threshold_difference * slopes(tissue_types(i)) * volumes(i);
            end
        end
    end
    fval_ls = sum(costs_ls);
    RecordCostFunctionStats(base_path, tumor_name, 'Cimmino', strcat('top_power_', num2str(num_real_sources)), fval_ls, ...
        v_90_ls, d_90_ls, healthy_guard_band, tumor_guard_band, tumor_weight, sum(real_powers_ls), total_energy);

    figure;
    hold on;
    for i=1:num_tissues
        plot(0:499, dose_count_ls(:,i));
    end
    title(strcat('Dose-volume histogram using Cimmino algorithm, best ', num2str(num_real_sources), ' sources'));
    xlabel('Percentage of threshold dose');
    ylabel('Percentage of tissue volume receiving given dose');
    legend(tissue_names);
    print(strcat(dvh_filename_prefix_top_sources_ls, '.png'), '-dpng');
    
    %%% optimize power across reduced set of sources from LP solution
    tic
    [f_lp_reduced, A_lp_reduced, b_lp_reduced] = LPOptimizerSetup(fluences_matrix(:, best_sources_lp), slopes, ...
        tumor_overdose_slope, thresholds, tumor_overdose_threshold, tissue_types, volumes, total_power_limit, 0, ...
        num_tumor_tetras);
    
    if tumor_overdose_threshold ~= 0
        ub = Inf(num_real_sources+num_tetras+num_tumor_tetras, 1);
        lb = zeros(num_real_sources+num_tetras+num_tumor_tetras, 1);
    else
        ub = Inf(num_real_sources+num_tetras, 1);
        lb = zeros(num_real_sources+num_tetras, 1);
    end
    ub(1:num_real_sources) = max_source_power * ones(num_real_sources, 1);
    
    [real_powers_lp, fval_lp] = LPOptimizer(f_lp_reduced, A_lp_reduced, b_lp_reduced, lb, ub, num_real_sources, lp_alg_idx);
    
    fprintf('Optimal power distribution with LP optimization, choosing best sources:\n');
    for i=1:num_real_sources
        fprintf('Source %d (virtual source %d) power = %f\n', i, best_sources_lp(i), real_powers_lp(i));
    end
    fprintf('Total power = %f\n', sum(real_powers_lp));
    
    lp_real_source_time = toc;
    lp_total_time = lp_virtual_source_time + lp_real_source_time;
    
    % produce cumulative dose-volume histogram
    dvh_filename_prefix_top_sources_lp = strcat(base_path, 'matlab_optimization_output/dvh/LP_', ...
        num2str(num_real_sources), '_sources_dvh');
    fluences_lp = fluences_matrix_full(:, best_sources_lp) * real_powers_lp;
    [dose_count_lp, d_90_lp, v_90_lp] = DoseVolumeHistogram(dvh_percentage_range, fluences_lp, hard_thresholds, ...
        tissue_types_full, num_tissues, volumes_full, dvh_filename_prefix_top_sources_lp);
    RecordCostFunctionStats(base_path, tumor_name, 'LP', strcat('top_power_', num2str(num_real_sources)), fval_lp, ...
        v_90_lp, d_90_lp, healthy_guard_band, tumor_guard_band, tumor_weight, sum(real_powers_lp), total_energy);
    
    figure;
    hold on;
    for i=1:num_tissues
        plot(0:499, dose_count_lp(:,i));
    end
    title(strcat('Dose-volume histogram using LP formulation, best ', num2str(num_real_sources), ' sources'));
    xlabel('Percentage of threshold dose');
    ylabel('Percentage of tissue volume receiving given dose');
    legend(tissue_names);
    print(strcat(dvh_filename_prefix_top_sources_lp, '.png'), '-dpng');
    
    fprintf(topsrc_results_file, '%s\t%s\t%s\t%d\t%f\t%f\t%f\t%f\t%e\t%e\t%e\t%f\t%f\t%f\n', datestr(now), ...
        tumor_name, 'Cimmino', num_real_sources, healthy_guard_band, tumor_guard_band, tumor_weight, fval_ls, ...
        v_90_ls(3), v_90_ls(4), v_90_ls(5), ls_total_time, sum(real_powers_ls), total_energy);
    fprintf(topsrc_results_file, '%s\t%s\t%s\t%d\t%f\t%f\t%f\t%f\t%e\t%e\t%e\t%f\t%f\t%f\n', datestr(now), ...
        tumor_name, alg_name, num_real_sources, healthy_guard_band, tumor_guard_band, tumor_weight, fval_lp, ...
        v_90_lp(3), v_90_lp(4), v_90_lp(5), lp_total_time, sum(real_powers_lp), total_energy);
    
    fprintf(powers_file, 'Cimmino powers: (source id, source power)\n');
    for i=1:num_real_sources
        fprintf(powers_file, '%d,%e\n', best_sources_ls(i), real_powers_ls(i));
    end
    fprintf(powers_file, 'LP powers: (source id, source power)\n');
    for i=1:num_real_sources
        fprintf(powers_file, '%d,%e\n', best_sources_lp(i), real_powers_lp(i));
    end
    
    % write full fluence matrix (including tetras for which it was ignored due to always being below the threshold)
    fluences_file_ls = fopen(strcat(base_path, 'matlab_optimization_output/solutions/fluences_top_powers_ls_',...
        num2str(mesh_number), '_', num2str(num_real_sources), '_src.txt'), 'w');
    fluences_file_lp = fopen(strcat(base_path, 'matlab_optimization_output/solutions/fluences_top_powers_lp_',...
        num2str(mesh_number), '_', num2str(num_real_sources), '_src.txt'), 'w');
    fprintf(fluences_file_ls, '%20.10f\n', fluences_ls(:));
    fprintf(fluences_file_lp, '%20.10f\n', fluences_lp(:));
    fclose(fluences_file_ls);
    fclose(fluences_file_lp);
end

%%% Source placement algorithm 2:
% Simulated annealing method: initialize with random set of sources, do random perturbations of decreasing distance until
% a time limit has passed

% number of sources to vary position of in each cycle
num_sources_to_perturb = 1;

% track existing combinations of sources that have been tried, to prevent unnecessary repeats
previously_tried_sources = zeros(1, num_real_sources);
num_source_repeats = 0;

if alg_enabled(2)
    fprintf(powers_file, '====Simulated annealing with %d sources:====\n', num_real_sources);
    
    for cimmino_annealer=0:1
        if cimmino_annealer
            dvh_filename_prefix_annealer = strcat(base_path, 'matlab_optimization_output/dvh/LS_Annealer_', num2str(num_real_sources), ...
                    '_sources_', num2str(print_step));
        else
            dvh_filename_prefix_annealer = strcat(base_path, 'matlab_optimization_output/dvh/LP_Annealer_', num2str(num_real_sources), ...
                    '_sources_', num2str(print_step));
        end
        
        % choose a set of initial sources: use the highest-power sources
        if cimmino_annealer
            fprintf(powers_file, '==Cimmino:==\n');
            %new_sources = best_sources_ls;
            new_sources = best_sources_lp; % TODO: eventually compare to initializing from best Cimmino sources
        else
            fprintf(powers_file, '==LP:==\n');
            new_sources = best_sources_lp;
        end
        % TODO: this is what Johansson et. al. did, but we should compare with choosing sources evenly spaced in the tumor
        %new_sources = randsample(num_sources, num_real_sources);

        % for printing performance data at regular intervals
        performance_data_interval = 300; % 5 min
        next_print_time = performance_data_interval;
        print_step = 0;

        % approx. the radius of the tumor divided by a factor representing the spatial density of sources
        %TODO: tests to choose best perturbation radius
        %max_perturbation_radius = 0.5 * max(max(distance_matrix)) / nthroot(num_real_sources, 3);
        max_perturbation_radius = 0.2 * max(max(distance_matrix));
        cumulative_time = 0;
        cycle_count = -1;
        switches_made = -1; % so that it doesn't count the first set of sources as a switch
        prev_cost = Inf;
        sources = new_sources;
        powers = zeros(num_real_sources, 1);
        
        % to keep track of how many perturbations have succeeded, and thus to control how to vary the perturbation
        % radius and number of sources to perturb
        success_counter = 0;
        while cumulative_time < annealer_runtime_limit
            annealer_time = tic;
            cycle_count = cycle_count + 1;

            % optimize power between next set of sources
            if cimmino_annealer
                A_ls_reduced = A_ls(:, new_sources);
                [new_powers, cimmino_time] = CimminoOptimizer(A_ls_reduced, b_ls, zeros(num_real_sources,1), ...
                        max_source_power * ones(num_real_sources,1), w, powers, (total_power_limit~=0), 0, false);

                fprintf('CimminoOptimizer took %f seconds.\n', cimmino_time);

                fprintf('Optimal power distribution with Cimmino optimization, with new sources:\n');
                for i=1:num_real_sources
                    fprintf('Source %d (virtual source %d) power = %f\n', i, new_sources(i), new_powers(i));
                end
                fprintf('Total power = %f\n', sum(new_powers));
                
                % evaluate total cost (according to LP cost function)
                CostCountingTime = tic;
                % for calculating cost (ignores low-dose tetras)
                fluences_ls_reduced = fluences_matrix(:, new_sources) * new_powers;
                costs_ls = zeros(num_tetras,1);
                for i=1:num_tetras
                    if tissue_types(i) ~= 0
                        threshold_difference = fluences_ls_reduced(i) - thresholds(tissue_types(i));
                        % only count under-dosed tumor cells or over-dosed healthy cells
                        if (tissue_types(i) == tumor_idx && threshold_difference < 0) || ...
                           (tissue_types(i) ~= tumor_idx && threshold_difference > 0 )
                            costs_ls(i) = threshold_difference * slopes(tissue_types(i)) * volumes(i);
                        end
                    end
                end
                fval = sum(costs_ls);
                fprintf('Computing cost according to LP cost function took %f seconds.\n', toc(CostCountingTime));
            else
                LPTime = tic;
                [f_lp, A_lp, b_lp] = LPOptimizerSetup(fluences_matrix(:, new_sources), slopes, ...
                    tumor_overdose_slope, thresholds, tumor_overdose_threshold, tissue_types, volumes,...
                    total_power_limit, 0, num_tumor_tetras);
                fprintf('LPOptimizerSetup took %f seconds.\n', toc(LPTime));

                if tumor_overdose_threshold ~= 0
                    ub = Inf(num_tetras+num_real_sources+num_tumor_tetras, 1);
                    lb = zeros(num_tetras+num_real_sources+num_tumor_tetras, 1);
                else
                    ub = Inf(num_tetras+num_real_sources, 1);
                    lb = zeros(num_tetras+num_real_sources, 1);
                end
                ub(1:num_real_sources) = max_source_power * ones(num_real_sources, 1);

                LPTime = tic;
                [new_powers, fval] = LPOptimizer(f_lp, A_lp, b_lp, lb, ub, num_real_sources, lp_alg_idx);
                fprintf('LPOptimizer took %f seconds.\n', toc(LPTime));
            end
            
            % record statistics on how perturbations affect power allocation
            %{
            if cumulative_time ~= 0
                unchanged_source_indices = setdiff(1:num_real_sources, perturbed_sources);
                if cimmino_annealer
                    RecordSourcePerturbationStats(tumor_name, 'Cimmino', fval, num_real_sources, ...
                        size(perturbed_sources,1), perturbation_radius, sources(unchanged_source_indices), ...
                        powers(unchanged_source_indices), new_powers(unchanged_source_indices));
                else
                    RecordSourcePerturbationStats(tumor_name, 'LP', fval, num_real_sources, ...
                        size(perturbed_sources,1), perturbation_radius, sources(unchanged_source_indices), ...
                        powers(unchanged_source_indices), new_powers(unchanged_source_indices));
                end
            end
            %}
            
            % if new sources are better, make them the reference. Either way, the best set of sources is the one to
            % perturb from
            if fval < prev_cost
                sources = new_sources;
                powers = new_powers;
                prev_cost = fval;
                switches_made = switches_made + 1;
                fprintf('Switch was profitable! %d/%d have been profitable so far.\n', switches_made, cycle_count);
                success_counter = 0;
                num_sources_to_perturb = 1;
                
                previously_tried_sources(size(previously_tried_sources,1)+1,:) = sort(new_sources.');
            else
                new_sources = sources;
                fprintf('Switch was not profitable. %d/%d have been profitable so far.\n', switches_made, cycle_count);
                if success_counter < -100
                    if num_sources_to_perturb < num_real_sources
                        num_sources_to_perturb = num_sources_to_perturb + 1;
                        success_counter = 0;
                    end
                else
                    success_counter = success_counter - 1;
                end
            end
            
            new_sources_found = false;
            prev_num_source_repeats = num_source_repeats;
            while ~new_sources_found
                % if many attempts have failed, start doing fully random perturbations
                if num_source_repeats - prev_num_source_repeats < 5 %TODO: magic number
                    % choose which sources to perturb
                    perturbed_sources = randsample(num_real_sources, num_sources_to_perturb);
                    % for each perturbed source, choose a random source within a certain range of it
                    for i=1:size(perturbed_sources)
                        prev_source_idx = sources(perturbed_sources(i));

                        %perturbation_radius = max_perturbation_radius * (1 - cumulative_time / annealer_runtime_limit);
                        if success_counter > 0
                            perturbation_radius = max_perturbation_radius;
                        else
                            perturbation_radius = max_perturbation_radius * 1.01 ^ (-success_counter);
                        end
                        % generate a random point inside the sphere of this radius
                        tmp = 2*rand - 1;
                        polar_angle = asin(tmp);
                        azimuthal_angle = 2*pi*rand;
                        r = perturbation_radius*((rand)^(1/3)) + min_perturbation_distance;
                        [x,y,z] = sph2cart(azimuthal_angle, polar_angle, r);
                        fprintf('spherical coords: (%f,%f,%f), cartesian coords: (%f,%f,%f)\n',...
                            azimuthal_angle, polar_angle, r, x, y, z);
                        newpos = source_centers(prev_source_idx, :) + [x y z];
                        %new_sources(perturbed_sources(i)) = knnsearch(source_centers, newpos);

                        % match this point to the nearest candidate source
                        new_sources(perturbed_sources(i)) = knnsearch(source_centers, newpos, 'K', 1);

                        % if the matches source is far away from newpos, this means it was outside the tumor,
                        % so instead of using it, select a random source. Likewise, if it matches to an existing
                        % source then replace it with a random source
                        if any(sources == new_sources(perturbed_sources(i))) || ...
                                norm(newpos - source_centers(new_sources(perturbed_sources(i)),:)) > 2*min_perturbation_distance
                            fprintf('Randomizing source.\n');
                            new_sources(perturbed_sources(i)) = randsample(setdiff(1:num_sources, [perturbed_sources; sources]), 1);
                        end

                        %{
                        % this gives 1 when we are in the first 1/num_stages of runtime, 2 in the second 1/num_stages, etc.
                        perturbation_range = ceil(num_stages*(annealer_runtime_limit - cumulative_time)/annealer_runtime_limit);
                        num_sources_in_range = n(prev_source_idx, perturbation_range);
                        new_sources(perturbed_sources(i)) = nearby_sources(prev_source_idx, randsample(num_sources_in_range, 1));
                        %}
                    end
                elseif num_source_repeats - prev_num_source_repeats < 25 %TODO: magic number
                    % choose which sources to perturb
                    perturbed_sources = randsample(num_real_sources, num_sources_to_perturb);
                    new_sources(perturbed_sources) = randsample(setdiff(1:num_sources,sources), num_real_sources);
                else
                    % choose which sources to perturb
                    perturbed_sources = randsample(num_real_sources, num_sources_to_perturb+2);
                    new_sources(perturbed_sources) = randsample(setdiff(1:num_sources,sources), num_real_sources);
                end

                % check if this combination has been tried before; if so then generate a new combination
                new_sources_found = ~any(ismember(previously_tried_sources, sort(new_sources.'), 'rows'));
                if ~new_sources_found
                    fprintf('New source combination has been tried before, trying another...\n');
                    num_source_repeats = num_source_repeats + 1;
                end
            end

            cumulative_time = cumulative_time + toc(annealer_time);
            fprintf('cumulative_time = %f\n', cumulative_time);

            % produce cumulative dose-volume histogram
            fluences = fluences_matrix_full(:, sources) * powers;
            [dose_count_annealer, d_90_annealer, v_90_annealer] = DoseVolumeHistogram(dvh_percentage_range, ...
                fluences, hard_thresholds, tissue_types_full, num_tissues, volumes_full, dvh_filename_prefix_annealer);
            if cimmino_annealer
                RecordCostFunctionStats(base_path, tumor_name, 'Cimmino', strcat('annealer_', num2str(num_real_sources)), ...
                    prev_cost, v_90_annealer, d_90_annealer, healthy_guard_band, tumor_guard_band, tumor_weight,...
                    sum(powers), total_energy);
            else
                RecordCostFunctionStats(base_path, tumor_name, 'LP', strcat('annealer_', num2str(num_real_sources)), ...
                    prev_cost, v_90_annealer, d_90_annealer, healthy_guard_band, tumor_guard_band, tumor_weight,...
                    sum(powers), total_energy);
            end
            
            if cumulative_time > next_print_time
                fprintf('Writing results so far to file...\n');

                print_step = print_step + 1;
                next_print_time = next_print_time + performance_data_interval;

                % print results in a table
                if cimmino_annealer
                    fprintf(annealer_results_file, '%s\t%s\t%s\t%d\t%f\t%f\t%f\t%f\t%e\t%e\t%e\t%f\t%f\t%f\t%d\t%d\t%d\n', ...
                        datestr(now), tumor_name, 'Cimmino', num_real_sources, healthy_guard_band, tumor_guard_band, tumor_weight, ...
                        fval, v_90_annealer(3), v_90_annealer(4), v_90_annealer(5), cumulative_time, sum(powers), total_energy, ...
                        switches_made, cycle_count, num_source_repeats);
                else
                    fprintf(annealer_results_file, '%s\t%s\t%s\t%d\t%f\t%f\t%f\t%f\t%e\t%e\t%e\t%f\t%f\t%f\t%d\t%d\t%d\n', ...
                        datestr(now), tumor_name, alg_name, num_real_sources, healthy_guard_band, tumor_guard_band, tumor_weight, ...
                        fval, v_90_annealer(3), v_90_annealer(4), v_90_annealer(5), cumulative_time, sum(powers), total_energy, ...
                        switches_made, cycle_count, num_source_repeats);
                end
                    
                figure;
                hold on;
                for i=1:num_tissues
                    plot(0:499, dose_count_annealer(:,i));
                end
                xlabel('Percentage of threshold dose');
                ylabel('Percentage of tissue volume receiving given dose');
                legend(tissue_names);
                if cimmino_annealer
                    title(strcat('DVH using SA with Cimmino with ', num2str(num_real_sources), ' sources, time step=', ...
                        num2str(print_step)));
                    print(strcat(dvh_filename_prefix_annealer, '.png'), '-dpng');
                else
                    title(strcat('DVH using SA with LP with ', num2str(num_real_sources), ' sources, time step=', ...
                        num2str(print_step)));
                    print(strcat(dvh_filename_prefix_annealer, '.png'), '-dpng');
                end
            end
        end

        fprintf('Optimal power distribution with simulated annealing with LP power optimization:\n');
        for i=1:num_real_sources
            fprintf('Source %d (virtual source %d) power = %f\n', i, sources(i), powers(i));
        end
        fprintf('Total power = %f\n', sum(powers));

        % produce cumulative dose-volume histogram
        fluences_annealer = fluences_matrix_full(:, sources) * powers;
        [dose_count_annealer, d_90_annealer, v_90_annealer] = DoseVolumeHistogram(dvh_percentage_range, fluences_annealer,...
            hard_thresholds, tissue_types_full, num_tissues, volumes_full, dvh_filename_prefix_annealer);

        figure;
        hold on;
        for i=1:num_tissues
            plot(0:499, dose_count_annealer(:,i));
        end
        xlabel('Percentage of threshold dose');
        ylabel('Percentage of tissue volume receiving given dose');
        legend(tissue_names);
        if cimmino_annealer
            title(strcat('DVH using simulated annealing with Cimmino with ', num2str(num_real_sources), ' sources'));
            print(strcat(base_path, 'matlab_optimization_output/dvh/LS_Annealer_', num2str(num_real_sources), '_sources.png'), '-dpng');

            for i=1:num_tissues
                fprintf('Tissue type: %d\n', i);
                fprintf('Simulated annealing Cimmino v_90 = %f, d_90 = %f\n', v_90_annealer(i), d_90_annealer(i));
            end
        else
            title(strcat('DVH using simulated annealing with LP with ', num2str(num_real_sources), ' sources'));
            print(strcat(base_path, 'matlab_optimization_output/dvh/LP_Annealer_', num2str(num_real_sources), '_sources.png'), '-dpng');

            for i=1:num_tissues
                fprintf('Tissue type: %d\n', i);
                fprintf('Simulated annealing LP v_90 = %f, d_90 = %f\n', v_90_annealer(i), d_90_annealer(i));
            end
        end

        if cimmino_annealer
            fprintf(powers_file, 'Cimmino powers: (source id, source power)\n');
        else
            fprintf(powers_file, 'LP powers: (source id, source power)\n');
        end
        for i=1:num_real_sources
            fprintf(powers_file, '%d,%e\n', sources(i), powers(i));
        end
        
        % write full fluence matrix (including tetras for which it was ignored due to always being below the threshold)
        if cimmino_annealer
            fluences_file_annealer = fopen(strcat(base_path, 'matlab_optimization_output/solutions/fluences_annealer_ls_',...
                num2str(mesh_number), '_', num2str(num_real_sources), '_src.txt'), 'w');
        else
            fluences_file_annealer = fopen(strcat(base_path, 'matlab_optimization_output/solutions/fluences_annealer_lp_',...
                num2str(mesh_number), '_', num2str(num_real_sources), '_src.txt'), 'w');
        end
        fprintf(fluences_file_annealer, '%20.10f\n', fluences_annealer(:));
        fclose(fluences_file_annealer);
    end
end

%%% Source placement algorithm 3:
% optimize source powers across all virtual sources, then do k-means clustering on the sources, weighted by 
% source power. The cluster centers must be at source locations, since they represent the choice of sources.
if alg_enabled(3)
    fprintf(powers_file, '====k-means clustering with %d sources:====\n', num_real_sources);
    
    %%% k-means clustering on Cimmino virtual-sources solution
    cluster_sources_ls = kMeansSourceClustering(best_sources_ls, virtual_powers_ls, source_centers, distance_matrix);
    
    %%% optimize power across reduced set of sources from Cimmino solution
    A_ls_reduced = A_ls(:, cluster_sources_ls);
    [real_powers_ls, ls_real_source_time] = CimminoOptimizer(A_ls_reduced, b_ls, zeros(num_real_sources,1), ...
            max_source_power * ones(num_real_sources,1), w, zeros(num_real_sources,1), (total_power_limit~=0), 0, false);
    
	ls_total_time = ls_virtual_source_time + ls_real_source_time;
    fprintf('Total time for LS optimization was %f s.\n\n', ls_total_time);
    
    fprintf('Optimal power distribution with Cimmino optimization, k-means:\n');
    for i=1:num_real_sources
        fprintf('Source %d (virtual source %d) power = %f\n', i, cluster_sources_ls(i), real_powers_ls(i));
    end
    fprintf('Total power = %f\n', sum(real_powers_ls));
    
    % produce cumulative dose-volume histogram
    dvh_filename_prefix_clustering_ls = strcat(base_path, 'matlab_optimization_output/dvh/LS_clustering_', ...
        num2str(num_real_sources), '_sources_dvh');
    fluences_ls = fluences_matrix_full(:, cluster_sources_ls) * real_powers_ls;
    [dose_count_ls, d_90_ls_clustering, v_90_ls_clustering] = DoseVolumeHistogram(dvh_percentage_range, fluences_ls, hard_thresholds, ...
        tissue_types_full, num_tissues, volumes_full, dvh_filename_prefix_clustering_ls);
    % evaluate total cost (according to LP cost function)
    % for calculating cost (ignores low-dose tetras)
    fluences_ls_reduced = fluences_matrix(:, best_sources_ls) * real_powers_ls;
    costs_ls = zeros(num_tetras,1);
    for i=1:num_tetras
        if tissue_types(i) ~= 0
            threshold_difference = fluences_ls_reduced(i) - thresholds(tissue_types(i));
            % only count under-dosed tumor cells or over-dosed healthy cells
            if (tissue_types(i) == tumor_idx && threshold_difference < 0) || ...
               (tissue_types(i) ~= tumor_idx && threshold_difference > 0 )
                costs_ls(i) = threshold_difference * slopes(tissue_types(i)) * volumes(i);
            end
        end
    end
    fval_ls = sum(costs_ls);
    RecordCostFunctionStats(base_path, tumor_name, 'Cimmino', strcat('kmeans_', num2str(num_real_sources)), ...
                    fval_ls, v_90_ls_clustering, d_90_ls_clustering, healthy_guard_band, tumor_guard_band,...
                    tumor_weight, sum(real_powers_ls), total_energy);

    figure;
    hold on;
    for i=1:num_tissues
        plot(0:499, dose_count_ls(:,i));
    end
    title(strcat('DVH using clustering with Cimmino, ', num2str(num_real_sources), ' sources'));
    xlabel('Percentage of threshold dose');
    ylabel('Percentage of tissue volume receiving given dose');
    legend(tissue_names);
    print(strcat(dvh_filename_prefix_clustering_ls, '.png'), '-dpng');
    
    %%% k-means clustering on LP virtual-sources solution
    cluster_sources_lp = kMeansSourceClustering(best_sources_lp, virtual_powers_lp, source_centers, distance_matrix);
    
    %%% optimize power across reduced set of sources from LP solution
    tic
    [f_lp_cluster, A_lp_cluster, b_lp_cluster] = LPOptimizerSetup(fluences_matrix(:, cluster_sources_lp), slopes, ...
        tumor_overdose_slope, thresholds, tumor_overdose_threshold, tissue_types, volumes, total_power_limit, 0, ...
        num_tumor_tetras);
    
    if tumor_overdose_threshold ~= 0
        ub = Inf(num_real_sources+num_tetras+num_tumor_tetras, 1);
        lb = zeros(num_real_sources+num_tetras+num_tumor_tetras, 1);
    else
        ub = Inf(num_real_sources+num_tetras, 1);
        lb = zeros(num_real_sources+num_tetras, 1);
    end
    ub(1:num_real_sources) = max_source_power * ones(num_real_sources, 1);
    
    [real_powers_lp, fval_lp] = LPOptimizer(f_lp_cluster, A_lp_cluster, b_lp_cluster, lb, ub, num_real_sources, lp_alg_idx);
    
    fprintf('Optimal power distribution with LP optimization, choosing best sources:\n');
    for i=1:num_real_sources
        fprintf('Source %d (virtual source %d) power = %f\n', i, cluster_sources_lp(i), real_powers_lp(i));
    end
    fprintf('Total power = %f\n', sum(real_powers_lp));
    
    lp_real_source_time = toc;
    lp_total_time = lp_virtual_source_time + lp_real_source_time;
    fprintf('Total time for LP optimization was %f s.\n\n', lp_total_time);
    
    % produce cumulative dose-volume histogram
    dvh_filename_prefix_clustering_lp = strcat(base_path, 'matlab_optimization_output/dvh/LP_clustering_', ...
        num2str(num_real_sources), '_sources_dvh');
    fluences_lp = fluences_matrix_full(:, cluster_sources_lp) * real_powers_lp;
    [dose_count_lp, d_90_lp_clustering, v_90_lp_clustering] = DoseVolumeHistogram(dvh_percentage_range, fluences_lp, ...
        hard_thresholds, tissue_types_full, num_tissues, volumes_full, dvh_filename_prefix_clustering_lp);
    RecordCostFunctionStats(base_path, tumor_name, 'LP', strcat('kmeans_', num2str(num_real_sources)), ...
                    fval_lp, v_90_lp_clustering, d_90_lp_clustering, healthy_guard_band, tumor_guard_band, ...
                    tumor_weight, sum(real_powers_lp), total_energy);
    
    figure;
    hold on;
    for i=1:num_tissues
        plot(0:499, dose_count_lp(:,i));
    end
    title(strcat('DVH using clustering with LP, ', num2str(num_real_sources), ' sources'));
    xlabel('Percentage of threshold dose');
    ylabel('Percentage of tissue volume receiving given dose');
    legend(tissue_names);
    print(strcat(dvh_filename_prefix_clustering_lp, '.png'), '-dpng');
    
    fprintf(ls_kmeans_results_file, '%s\t%s\t%d\t%e\t%e\t%e\t%f\t%f\t%f\t%f\t%f\t%f\n', datestr(now), tumor_name,...
        num_real_sources, v_90_ls_clustering(3), v_90_ls_clustering(4), v_90_ls_clustering(5), ls_total_time, ...
        sum(real_powers_ls), total_energy, healthy_guard_band, tumor_guard_band, tumor_weight);
    fprintf(lp_kmeans_results_file, '%s\t%s\t%d\t%e\t%e\t%e\t%f\t%f\t%f\t%f\t%f\t%f\n', datestr(now), tumor_name,...
        num_real_sources, v_90_lp_clustering(3), v_90_lp_clustering(4), v_90_lp_clustering(5), lp_total_time, ...
        sum(real_powers_lp), total_energy, healthy_guard_band, tumor_guard_band, tumor_weight);
    
    fprintf(kmeans_results_file, '%s\t%s\t%s\t%d\t%f\t%f\t%f\t%f\t%e\t%e\t%e\t%f\t%f\t%f\n', datestr(now), ...
        tumor_name, 'Cimmino', num_real_sources, healthy_guard_band, tumor_guard_band, tumor_weight, fval_ls, ...
        v_90_ls_clustering(3), v_90_ls_clustering(4), v_90_ls_clustering(5), ls_total_time, sum(real_powers_ls), total_energy);
    fprintf(kmeans_results_file, '%s\t%s\t%s\t%d\t%f\t%f\t%f\t%f\t%e\t%e\t%e\t%f\t%f\t%f\n', datestr(now), ...
        tumor_name, alg_name, num_real_sources, healthy_guard_band, tumor_guard_band, tumor_weight, fval_lp, ...
        v_90_lp_clustering(3), v_90_lp_clustering(4), v_90_lp_clustering(5), lp_total_time, sum(real_powers_lp), total_energy);
    
    fprintf(powers_file, 'Cimmino powers: (source id, source power)\n');
    for i=1:num_real_sources
        fprintf(powers_file, '%d,%e\n', best_sources_ls(i), real_powers_ls(i));
    end
    fprintf(powers_file, 'LP powers: (source id, source power)\n');
    for i=1:num_real_sources
        fprintf(powers_file, '%d,%e\n', best_sources_lp(i), real_powers_lp(i));
    end
    
    % write full fluence matrix (including tetras for which it was ignored due to always being below the threshold)
    fluences_file_ls = fopen(strcat(base_path, 'matlab_optimization_output/solutions/fluences_kmeans_ls_',...
        num2str(mesh_number), '_', num2str(num_real_sources), '_src.txt'), 'w');
    fluences_file_lp = fopen(strcat(base_path, 'matlab_optimization_output/solutions/fluences_kmeans_lp_',...
        num2str(mesh_number), '_', num2str(num_real_sources), '_src.txt'), 'w');
    fprintf(fluences_file_ls, '%20.10f\n', fluences_ls(:));
    fprintf(fluences_file_lp, '%20.10f\n', fluences_lp(:));
    fclose(fluences_file_ls);
    fclose(fluences_file_lp);
end

%%% Source placement algorithm 4:
% optimize source powers across all virtual sources, then reject sources with negligible power. Then re-optimize powers
% between remaining sources, drop lowest-power source, and repeat until only n sources remain
if alg_enabled(4)
    %%% Cimmino
    tic
    %TODO: number of sources to take before dropping 1 at a time is arbitrary
    [~, top_power_sources_ls] = sort(virtual_powers_ls, 'descend');
    reduced_sources_ls = top_power_sources_ls(1:(2*num_real_sources));
    
    % main loop
    num_reduced_sources = 2*num_real_sources;
    reduced_powers_ls_prev = zeros(num_reduced_sources, 1);
    ls_iterative_total_time = ls_virtual_source_time + toc;
    while num_reduced_sources >= 1
        % optimize powers
        A_ls_reduced = A_ls(:, reduced_sources_ls);
        [reduced_powers_ls , ls_reduced_source_time] = CimminoOptimizer(A_ls_reduced, b_ls, zeros(num_reduced_sources,1), ...
                max_source_power * ones(num_reduced_sources,1), w, reduced_powers_ls_prev, ...
                (total_power_limit~=0), 0, false);

        ls_iterative_total_time = ls_iterative_total_time + ls_reduced_source_time;

        fprintf('Optimal power distribution with Cimmino optimization, choosing best sources:\n');
        for i=1:num_reduced_sources
            fprintf('Source %d (virtual source %d) power = %f\n', i, reduced_sources_ls(i), reduced_powers_ls(i));
        end
        fprintf('Total power = %f\n', sum(reduced_powers_ls));
        
        % print output for low enough number of sources
        if num_reduced_sources <= 2 * num_real_sources
            % produce cumulative dose-volume histogram
            dvh_filename_prefix_reduced_ls = strcat(base_path, 'matlab_optimization_output/dvh/LS_iterative_', ...
                tumor_name, '_', num2str(num_reduced_sources), 'sources');
            fluences_ls = fluences_matrix_full(:, reduced_sources_ls) * reduced_powers_ls;
            [dose_count_ls, d_90_ls, v_90_ls] = DoseVolumeHistogram(dvh_percentage_range, fluences_ls, hard_thresholds, ...
                tissue_types_full, num_tissues, volumes_full, dvh_filename_prefix_reduced_ls);
            
            % evaluate total cost (according to LP cost function)
            % for calculating cost (ignores low-dose tetras)
            fluences_ls_reduced = fluences_matrix(:, reduced_sources_ls) * reduced_powers_ls;
            costs_ls = zeros(num_tetras,1);
            for i=1:num_tetras
                if tissue_types(i) ~= 0
                    threshold_difference = fluences_ls_reduced(i) - thresholds(tissue_types(i));
                    % only count under-dosed tumor cells or over-dosed healthy cells
                    if (tissue_types(i) == tumor_idx && threshold_difference < 0) || ...
                       (tissue_types(i) ~= tumor_idx && threshold_difference > 0 )
                        costs_ls(i) = threshold_difference * slopes(tissue_types(i)) * volumes(i);
                    end
                end
            end
            fval_ls = sum(costs_ls);
            
            RecordCostFunctionStats(base_path, tumor_name, 'LS', strcat('iterative_removal_', num2str(num_reduced_sources)), ...
                    fval_ls, v_90_ls, d_90_ls, healthy_guard_band, tumor_guard_band, tumor_weight, ...
                    sum(reduced_powers_ls), total_energy);

            figure;
            hold on;
            for i=1:num_tissues
                plot(0:499, dose_count_ls(:,i));
            end
            title(strcat('DVH using LS iterative source-dropping, best ', num2str(num_reduced_sources), ' sources'));
            xlabel('Percentage of threshold dose');
            ylabel('Percentage of tissue volume receiving given dose');
            legend(tissue_names);
            print(strcat(dvh_filename_prefix_reduced_ls, '.png'), '-dpng');

            fprintf(iterative_results_file, '%s\t%s\t%s\t%d\t%f\t%f\t%f\t%f\t%e\t%e\t%e\t%f\t%f\t%f\n', datestr(now), ...
                tumor_name, 'Cimmino', num_reduced_sources, healthy_guard_band, tumor_guard_band, tumor_weight, fval_ls, ...
                v_90_ls(3), v_90_ls(4), v_90_ls(5), ls_iterative_total_time, sum(reduced_powers_ls), total_energy);
            
            powers_file_ls_reduced = fopen(strcat(base_path,'matlab_optimization_output/solutions/powers_ls_iterative_', ...
                tumor_name, '_', num2str(num_reduced_sources), 'sources.txt'), 'w');
            for i=1:num_reduced_sources
                fprintf(powers_file_ls_reduced, '%d\t%e\n', reduced_sources_ls(i), reduced_powers_ls(i));
            end
            fclose(powers_file_ls_reduced);
        end
        
        % choose next set of sources
        num_reduced_sources = num_reduced_sources - 1;
        [~, worst_source] = min(reduced_powers_ls);
        % set of powers to use as initial point for next step
        reduced_powers_ls_prev = zeros(num_reduced_sources, 1);
        reduced_powers_ls_prev(1:(worst_source-1)) = reduced_powers_ls(1:(worst_source-1));
        reduced_powers_ls_prev(worst_source:end) = reduced_powers_ls((worst_source+1):end);
        % set of sources without the lowest-power one
        reduced_sources_ls = setdiff(reduced_sources_ls, reduced_sources_ls(worst_source(1)));
        
        if size(reduced_sources_ls,1) < num_reduced_sources
            fprintf('Warning: size(reduced_sources_ls,1) < num_reduced_sources. Setting num_reduced_sources to match.\n');
            num_reduced_sources = size(reduced_sources_ls,1);
        elseif size(reduced_sources_ls,1) > num_reduced_sources
            fprintf('ERROR: size(reduced_sources_ls,1) > num_reduced_sources!\n');
            return;
        end
    end
    
    %%% LP
    %find virtual sources with non-negligible power
    tic
    %TODO: number of sources to take before dropping 1 at a time is arbitrary
    [~, top_power_sources_lp] = sort(virtual_powers_lp, 'descend');
    reduced_sources_lp = top_power_sources_lp(1:(2*num_real_sources));
    
    % main loop
    num_reduced_sources = 2*num_real_sources;
    lp_iterative_total_time = lp_virtual_source_time + toc;
    while num_reduced_sources >= 1
        iterative_step = tic;
        % optimize powers
        [f_lp_reduced, A_lp_reduced, b_lp_reduced] = LPOptimizerSetup(fluences_matrix(:, reduced_sources_lp), slopes, ...
            tumor_overdose_slope, thresholds, tumor_overdose_threshold, tissue_types, volumes, total_power_limit, 0, ...
            num_tumor_tetras);

        if tumor_overdose_threshold ~= 0
            ub = Inf(num_reduced_sources+num_tetras+num_tumor_tetras, 1);
            lb = zeros(num_reduced_sources+num_tetras+num_tumor_tetras, 1);
        else
            ub = Inf(num_reduced_sources+num_tetras, 1);
            lb = zeros(num_reduced_sources+num_tetras, 1);
        end
        ub(1:num_reduced_sources) = max_source_power * ones(num_reduced_sources, 1);

        [reduced_powers_lp, fval_lp] = LPOptimizer(f_lp_reduced, A_lp_reduced, b_lp_reduced, lb, ub, num_reduced_sources, lp_alg_idx);

        fprintf('Optimal power distribution with LP optimization, choosing best %d sources:\n', num_reduced_sources);
        for i=1:num_reduced_sources
            fprintf('Source %d (virtual source %d) power = %f\n', i, reduced_sources_lp(i), reduced_powers_lp(i));
        end
        fprintf('Total power = %f\n', sum(reduced_powers_lp));

        lp_reduced_source_time = toc(iterative_step);
        lp_iterative_total_time = lp_iterative_total_time + lp_reduced_source_time;
        fprintf('This step took %f seconds.\n', lp_reduced_source_time);
        
        % print output for low enough number of sources
        if num_reduced_sources <= 2 * num_real_sources
            % produce cumulative dose-volume histogram
            dvh_filename_prefix_reduced_lp = strcat(base_path, 'matlab_optimization_output/dvh/LP_iterative_', ...
                tumor_name, '_', num2str(num_reduced_sources), '_sources');
            fluences_lp = fluences_matrix_full(:, reduced_sources_lp) * reduced_powers_lp;
            [dose_count_lp, d_90_lp, v_90_lp] = DoseVolumeHistogram(dvh_percentage_range, fluences_lp, hard_thresholds, ...
                tissue_types_full, num_tissues, volumes_full, dvh_filename_prefix_reduced_lp);
            RecordCostFunctionStats(base_path, tumor_name, 'LP', strcat('iterative_removal_', num2str(num_reduced_sources)), ...
                    fval_lp, v_90_lp, d_90_lp, healthy_guard_band, tumor_guard_band, tumor_weight, ...
                    sum(reduced_powers_lp), total_energy);

            figure;
            hold on;
            for i=1:num_tissues
                plot(0:499, dose_count_lp(:,i));
            end
            title(strcat('DVH using LP iterative source-dropping, best ', num2str(num_reduced_sources), ' sources'));
            xlabel('Percentage of threshold dose');
            ylabel('Percentage of tissue volume receiving given dose');
            legend(tissue_names);
            print(strcat(dvh_filename_prefix_reduced_lp, '.png'), '-dpng');

            fprintf(iterative_results_file, '%s\t%s\t%s\t%d\t%f\t%f\t%f\t%f\t%e\t%e\t%e\t%f\t%f\t%f\n', datestr(now), ...
                tumor_name, alg_name, num_reduced_sources, healthy_guard_band, tumor_guard_band, tumor_weight, fval_lp, ...
                v_90_lp(3), v_90_lp(4), v_90_lp(5), lp_iterative_total_time, sum(reduced_powers_lp), total_energy);
            
            powers_file_lp_reduced = fopen(strcat(base_path,'matlab_optimization_output/solutions/powers_lp_iterative_', ...
                tumor_name, '_', num2str(num_reduced_sources), 'sources.txt'), 'w');
            for i=1:num_reduced_sources
                fprintf(powers_file_lp_reduced, '%d\t%e\n', reduced_sources_lp(i), reduced_powers_lp(i));
            end
            fclose(powers_file_lp_reduced);
        end
        
        % choose next set of sources
        num_reduced_sources = num_reduced_sources - 1;
        [~, worst_source] = min(reduced_powers_lp);
        reduced_sources_lp = setdiff(reduced_sources_lp, reduced_sources_lp(worst_source(1)));
        
        if size(reduced_sources_lp,1) < num_reduced_sources
            fprintf('Warning: size(reduced_sources_lp,1) < num_reduced_sources. Setting num_reduced_sources to match.\n');
            num_reduced_sources = size(reduced_sources_lp,1);
        elseif size(reduced_sources_lp,1) > num_reduced_sources
            fprintf('ERROR: size(reduced_sources_lp,1) < num_reduced_sources!\n');
            return;
        end
    end
end

fclose('all');

return;