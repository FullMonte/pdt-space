function [source_indices] = kMeansSourceClustering(initial_sources, virtual_powers, source_centers, distance_matrix)
% kMeansSourceClustering   Takes a set of virtual source powers and an initial guess at the best set
% of sources, and applies k-means clustering to try to find a better set of sources (uses Lloyd's
% algorithm

total_time = tic;

cluster_center_indices = initial_sources;
num_real_sources = size(initial_sources, 1);
num_virtual_sources = size(virtual_powers, 1);
prev_cluster_center_indices = zeros(num_real_sources, 1);

if size(distance_matrix,1) ~= num_virtual_sources || size(distance_matrix,2) ~= num_virtual_sources
    fprintf('ERROR: distance_matrix in kMeansSourceClustering is %d x %d elements, while num_virtual_sources = %d\n',...
        size(distance_matrix,1), size(distance_matrix,2), num_virtual_sources);
    quit force;
end

if size(source_centers,1) ~= num_virtual_sources || size(source_centers,2) ~= 3
    fprintf('ERROR: source_centers in kMeansSourceClustering is %d x %d elements, while num_virtual_sources = %d\n',...
        size(source_centers,1), size(source_centers,2), num_virtual_sources);
    quit force;
end

% the first cluster_size(i) elements of clusters(i,:) are the virtual sources in cluster i, the other values
% are don't-cares
clusters = zeros(num_real_sources, num_virtual_sources);

distance_to_each_cluster = zeros(num_real_sources);
% to detect repeatedly taking small steps, a sign of oscillation due to quantization
small_step_threshold = 20; % TODO: this depends on the spacing of the sources in the mesh!
num_small_steps = 0;

stepNumber = 1;
while any(prev_cluster_center_indices ~= cluster_center_indices) && num_small_steps < 20
    step_time = tic;
    
    %%% Assignment step: assign each source to a cluster
    assignment_timer = tic;
    
    % the number of virtual sources in each cluster
    cluster_size = zeros(num_real_sources, 1);
    
    for i=1:num_virtual_sources
        % calculate distance to each cluster center
        for j=1:num_real_sources
            distance_to_each_cluster(j) = distance_matrix(i,cluster_center_indices(j));
        end
        % find nearest cluster
        [~, nearest_cluster] = min(distance_to_each_cluster);
        % add this source to that cluster
        cluster_size(nearest_cluster) = cluster_size(nearest_cluster) + 1;
        clusters(nearest_cluster, cluster_size(nearest_cluster)) = i;
    end
    assignment_time = toc(assignment_timer);
    
    %%% Update step: calculate the centroid of each cluster and set it as the new center
    centroid_timer = tic;
    % the coordinates of the center of each cluster
    cluster_center_positions = zeros(num_real_sources, 3);
    for i=1:num_real_sources
        total_cluster_power = 0;
        for j=1:cluster_size(i)
            cluster_center_positions(i,1:3) = cluster_center_positions(i,1:3) + ...
                virtual_powers(clusters(i,j)) * source_centers(clusters(i,j),1:3);
            total_cluster_power = total_cluster_power + virtual_powers(clusters(i,j));
            %cluster_center_positions(i,1:3) = cluster_center_positions(i,1:3) + source_centers(clusters(i,j),1:3);
        end
        % TODO: how to handle total_cluster_power = 0?
        if total_cluster_power == 0
            fprintf('WARNING: total_cluster_power = 0 for cluster %d!\n', i);
        end
        cluster_center_positions(i,1:3) = cluster_center_positions(i,1:3) / total_cluster_power;
        %cluster_center_positions(i,1:3) = cluster_center_positions(i,1:3) / cluster_size(i);
    end
    centroid_time = toc(centroid_timer);
    
    % find sources that most closely match these positions
    matching_timer = tic;
    prev_cluster_center_indices = cluster_center_indices;
    for i=1:num_real_sources
        neighbors = knnsearch(source_centers, cluster_center_positions, 'K', i);
        idx = 1;
        cluster_center_indices(i) = neighbors(1);
        % enforce uniqueness
        while any(cluster_center_indices(i) == cluster_center_indices(1:(i-1)))
            %fprintf('Repeated cluster center %d at i=%d\n', cluster_center_indices(i), i);
            idx = idx+1;
            cluster_center_indices(i) = neighbors(idx);
        end
    end
    matching_time = toc(matching_timer);
    
    distance_moved = 0;
    for i=1:num_real_sources
        distance_moved = distance_moved + distance_matrix(prev_cluster_center_indices(i),cluster_center_indices(i));
    end
    % to detect repeatedly taking small steps, a sign of oscillation due to quantization
    if distance_moved < small_step_threshold
        num_small_steps = num_small_steps + 1;
    else
        num_small_steps = 0;
    end
    
    if mod(stepNumber,100) == 0 || num_small_steps > 0 % TODO: remove
        fprintf('Clusters moved a total distance of %f\n', distance_moved);
        fprintf('Assignment runtime was %f seconds\n', assignment_time);
        fprintf('Time to calculate centroids was %f seconds\n', centroid_time);
        fprintf('Time to match centroids to indices was %f seconds\n', matching_time);
        fprintf('Finished k-means step %d in %f seconds, previous and new centers:\n', stepNumber, toc(step_time));
        for i=1:num_real_sources
            fprintf('Previous: %d, New: %d\n', prev_cluster_center_indices(i), cluster_center_indices(i));
        end
        fprintf('\n');
    end
    stepNumber = stepNumber + 1;
end

source_indices = cluster_center_indices;
fprintf('k-means clustering finished in %f seconds\n', toc(total_time));