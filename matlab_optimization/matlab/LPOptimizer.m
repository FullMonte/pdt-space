function [source_powers, fval, exitflag] = LPOptimizer(f, A, b, lb, ub, num_sources, alg_idx)
% LPOptimizer  Runs linprog to solve the LP: min f^T x s.t. Ax <= b, x >= 0, using the algorithm specified by alg_idx.
% 	alg_idx of 1 uses dual-simplex, 2 uses interior-point.

tic
if alg_idx==1
    % dual-simplex algorithm
    options = optimoptions(@linprog, 'Algorithm', 'dual-simplex', 'MaxIterations', 1000000000, 'Display', 'iter', 'OptimalityTolerance', 1e-3);
    [x, fval, exitflag] = linprog(f, A, b, [], [], lb, ub, [], options);
elseif alg_idx==2
    % interior-point algorithm
    options = optimoptions(@linprog, 'Algorithm', 'interior-point', 'MaxIterations', 100000, 'Display', 'iter');
    [x, fval, exitflag] = linprog(f, A, b, [], [], lb, ub, [], options);
elseif alg_idx==3
    % minimization with square root regularization
    [x, fval] = fmincon(@quasi_regularizer, zeros(size(A, 2),1), A, b, [], [], lb, ub);
else
	fprintf('Invalid algorithm index!');
	pause
end

if (exitflag ~= 1)
    %fprintf('Optimizer did not converge \n');
    source_powers = zeros(num_sources,1);
else
    source_powers = x(1:num_sources);
end