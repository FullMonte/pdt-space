function [x, runtime] = CimminoOptimizer(A, b, lb, ub, w, initial_x, total_power_limit, runtime_lp, make_plot,...
    thresholds, tissue_types, fluences_matrix, volumes, slopes, tumor_idx, costs_filename, fval_lp, power_limit)
% CimminoOptimizer  Finds a vector x satisfying Ax <= b, lb <= x <= ub, if none exists then finds a least-squares solution, weighted by w
% 	A is an mxn matrix, b is an mx1 vector, x, lb, ub are nx1 vectors, w is an mx1 vector
%	If total_power_limit = true, the last row of A and last entry of b are taken to represent the power constraint
%   runtime_lp is the runtime of the LP algorithm on the same problem, which can be used to set a time limit for Cimmino.
%        Set to 0 to ignore this and use a stopping criterion based on step size
%   make_plot should be set to true if you want a plot of the cost (with respect to LP cost function) against time. In this
%       case, the other arguments must be provided also, else they can be safely ignored.
setup_runtime = tic;

% num_rows is num_tetras + 1 (the last row being the total power constraint)
[num_rows, num_sources] = size(A);

if total_power_limit
    num_tetras = num_rows - 1; % omitting power-constraint row
else
    num_tetras = num_rows;
end

x = initial_x;
step_magnitude = Inf;

% whether or not to print status updates while running
verbose = true;

fprintf('Beginning Cimmino optimization. num_rows = %d, num_sources = %d\n\n', num_rows, num_sources);

%%% Uses Cimmino algorithm to find weighted least-squares solution to Ax<=b, while strictly enforcing lb <= x <= ub

% pre-compute squared magnitude of each row
row_magnitudes = sum(A.^2,2);
% ignore rows with negligible magnitude
for i=1:num_rows
    if row_magnitudes(i) < 1e-10
        %fprintf('Found row of magnitude %e at i=%d, removing it.\n', row_magnitudes(i), i);
        row_magnitudes(i) = Inf; % this means w(i) / row_magnitudes(i) = 0, making the row irrelevant
    end
end

%scale_matrix = sparse(1:num_rows, 1:num_rows, w ./ row_magnitudes) * A;
scale_matrix = zeros(num_rows, num_sources);
for i=1:num_rows
    scale_matrix(i,:) = (w(i) / row_magnitudes(i)) * A(i,:);
end
if any(isnan(scale_matrix))
    fprintf('ERROR: In CimminoOptimizer: scale_matrix contains NaN elements!\n');
    return;
end

power_constraint_active = false;

% for tracking performance of Cimmino against time
time_step = 0.03 * runtime_lp;
time_idx = 1;

runtime = toc(setup_runtime);

% main loop of Cimmino algorithm
count = 1;
% for stopping criterion, either use LP runtime as a reference, or use a threshold on step size
%TODO: see if this is a reasonable stopping criterion. Runtime is strongly dependent on this threshold
while runtime_lp == 0 && (count < 50 || step_magnitude > 1e-16 * (x.' * x)) || ...
    (runtime < 3 * runtime_lp && (count < 50 || step_magnitude > 1e-28 * (x.' * x)))
    
    loop_runtime_clock = tic;

    % vectorized formulation
    violation = b - A*x;
    active_constraints = (violation < 0);
    diag_violation = sparse(1:num_rows, 1:num_rows, violation);
    if any(isnan(diag_violation))
        fprintf('ERROR: In CimminoOptimizer: diag_violation contains NaN elements!\n');
        return;
    end
    sum_of_weights = active_constraints.' * w;

    % used for status output
    if active_constraints(end) && total_power_limit
        power_constraint_active = true;
    end

    delta_x = ((2/sum_of_weights) * active_constraints.' * diag_violation * scale_matrix).';
    
    if size(delta_x,1) ~= num_sources || size(delta_x,2) ~= 1
        fprintf('ERROR: size(delta_x) = [%d %d] instead of [%d 1]!\n', ...
            size(delta_x,1), size(delta_x,2), num_sources);
        return;
    end

    if any(isnan(delta_x))
        fprintf('ERROR: A component of delta_x is NaN! delta_x = [\n');
        for i=1:num_sources
            fprintf('%e,\n', delta_x(i));
        end
        fprintf('].\n');

        fprintf('violation(1:100)=\n');
        fprintf('%f\n', violation(1:100));
        fprintf('active_constraints(1:100)=\n');
        fprintf('%f\n', active_constraints(1:100));
        fprintf('sum_of_weights=%f\n', sum_of_weights);
        return;
    end

    % forbid the step from taking x out of bounds
    upper_clipped = 0;
    lower_clipped = 0;
    for i=1:num_sources
        if x(i) + delta_x(i) < lb(i)
            lower_clipped = lower_clipped + 1;
            delta_x(i) = lb(i) - x(i);
        end
        if x(i) + delta_x(i) > ub(i)
            upper_clipped = upper_clipped + 1;
            delta_x(i) = ub(i) - x(i);
        end
    end

    step_magnitude = delta_x.' * delta_x;
    if verbose
        if count <= 10
            print_status = true;
        elseif count <= 100 && mod(count,10)==0
            print_status = true;
        elseif count > 100 && mod(count,25)==0
            print_status = true;
        end
    elseif mod(count,100) == 0
        print_status = true;
    end

    if print_status
        %fprintf('Cimmino step %d, ||delta_x||^2 / ||x^2|| = %e\n', count, step_magnitude/(x.' * x));
        if upper_clipped > 0
         %   fprintf('Clipped %d coordinates to upper bound\n', upper_clipped);
        end
        if lower_clipped > 0
          %  fprintf('Clipped %d coordinates to lower bound\n', lower_clipped);
        end

        if power_constraint_active
            %fprintf('Power constraint was active since last status output.\n');
            power_constraint_active = false;
        end

        print_status = false;
    end
     
    count = count + 1;

    x = x + delta_x;
    
    if runtime_lp ~= 0
        runtime = runtime + toc(loop_runtime_clock);
    end
    if make_plot
        if runtime >= time_step * time_idx
            % evaluate total cost (according to LP cost function)
            fluences_ls_reduced = fluences_matrix * x;

            costs_ls = zeros(num_tetras,1);
            overdose_cost = 0;
            underdose_cost = 0;
            for i=1:num_tetras
                if tissue_types(i) ~= 0
                    if tissue_types(i) == tumor_idx % cost due to tumor cells
                        threshold_difference = fluences_ls_reduced(i) - thresholds(tissue_types(i));
                        if threshold_difference < 0 % under-dosed tumor cell
                            costs_ls(i) = threshold_difference * slopes(tissue_types(i)) * volumes(i);
                            underdose_cost  = underdose_cost + costs_ls(i);
                        end
                    else
                        threshold_difference = fluences_ls_reduced(i) - thresholds(tissue_types(i));
                        if threshold_difference > 0 % over-dosed healthy cell
                            costs_ls(i) = threshold_difference * slopes(tissue_types(i)) * volumes(i);
                            overdose_cost = overdose_cost + costs_ls(i);
                        end
                    end
                end
            end

            cost_value(time_idx) = sum(costs_ls);
            time_value(time_idx) = runtime;
            
            overdose_cost_value(time_idx) = overdose_cost;
            underdose_cost_value(time_idx) = underdose_cost;

            time_idx = time_idx + 1;
        end 
    end
end

% print cost against time
if runtime_lp ~= 0 && make_plot == true
    % if completed before the LP time was reached
    if time_idx < 33
        % evaluate total cost (according to LP cost function)
        fluences_ls_reduced = fluences_matrix * x;
        
        costs_ls = zeros(num_tetras,1);
        overdose_cost = 0;
            underdose_cost = 0;
        for i=1:num_tetras
            if tissue_types(i) ~= 0
                if tissue_types(i) == tumor_idx % cost due to tumor cells
                    threshold_difference = fluences_ls_reduced(i) - thresholds(tissue_types(i));
                    if threshold_difference < 0 % under-dosed tumor cell
                        costs_ls(i) = threshold_difference * slopes(tissue_types(i)) * volumes(i);
                        underdose_cost  = underdose_cost + costs_ls(i);

                    end
                else
                    threshold_difference = fluences_ls_reduced(i) - thresholds(tissue_types(i));
                    if threshold_difference > 0 % over-dosed healthy cell
                        costs_ls(i) = threshold_difference * slopes(tissue_types(i)) * volumes(i);
                        overdose_cost = overdose_cost + costs_ls(i);
                    end
                end
            end
        end
        
        for i=time_idx:33
            cost_value(i) = sum(costs_ls);
            time_value(i) = time_idx * time_step;
            overdose_cost_value(i) = overdose_cost;
            underdose_cost_value(i) = underdose_cost;
        end
    end
    
    % plot
    figure;
    hold on;
    
   
    plot(time_value, overdose_cost_value);
    plot(time_value, cost_value);
    plot([runtime_lp runtime_lp], [cost_value(1) overdose_cost_value(end)], '--'); % vertical dashed line marking LP time
 
    title(strcat('LP cost function vs time, using Cimmino algorithm (compared to LP fval=', num2str(fval_lp), ')'));
    xlabel('Time (s)');
    ylabel('fval');
    
    legend('Cimmino Healthy Overdose Cost', 'Cimmino Total Cost', 'LP Time of Convergence');
    %print(strcat(costs_filename, '.png'), '-dpng');
    
    % table
    %{
    costs_file = fopen(strcat(costs_filename, '.txt'), 'w');
    for i=1:size(cost_value)
        fprintf(costs_file, '%f %f\n', time_value(i), cost_value(i));
    end
    fclose(costs_file);
    %}
end

if ~(all(x <= ub) && all(x >= lb))
    fprintf('ERROR: Violated bound constraints! delta_x = [\n');
    for i=1:num_sources
        fprintf('%e,\n', delta_x(i));
    end
    fprintf('].\n');

    fprintf('violation(1:100)=\n');
    fprintf('%f\n', violation(1:100));
    fprintf('active_constraints(1:100)=\n');
    fprintf('%f\n', active_constraints(1:100));
    fprintf('sum_of_weights=%f\n', sum_of_weights);
    return;
end

total_power = sum(x);
if total_power_limit && total_power > power_limit
    fprintf('Total power of %f exceeds limit of %g, normalizing it to a power of 1!\n', total_power, power_limit);
    x = power_limit*(x / total_power);
end