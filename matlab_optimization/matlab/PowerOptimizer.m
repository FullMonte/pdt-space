function [cimmino_dvh_percentage, cimmino_dvh_volume] = PowerOptimizer(varargin)

clc 
close all

%
%%% Parameters and setup
%
valid_args = true;

if nargin < 12
    valid_args = false;
else
    base_path = varargin{1};
    mesh_name = varargin{2};
    
    for i=[3 5:10]
        if isnan(str2double(varargin{i}))
            fprintf('ERROR: argument %d is not a number!', i);
            valid_args = false;
        end
    end
    
    mesh_number = str2double(varargin{3});
    wavelength = str2num(varargin{4});
    num_packets = str2double(varargin{5});
    total_energy = str2double(varargin{6});
    dvh_percentage_range = str2double(varargin{7});
    param_to_sweep = str2double(varargin{8});
    num_params = str2double(varargin{9});
    alg_idx = str2double(varargin{10});
    lp_tumor_weight = str2double(varargin{11});
    cimmino_tumor_weight = str2double(varargin{12});
    
    if mod(mesh_number, 1) ~= 0
        fprintf('ERROR: mesh_number must be an integer!\n');
        valid_args = false;
    end
    if mod(num_packets, 1) ~= 0
        fprintf('ERROR: num_packets must be an integer!\n');
        valid_args = false;
    end
    if mod(dvh_percentage_range, 1) ~= 0
        fprintf('ERROR: dvh_percentage_range must be an integer!\n');
        valid_args = false;
    end
    if mod(param_to_sweep, 1) ~= 0 || param_to_sweep < 0 || param_to_sweep > 4
        fprintf('ERROR: param_to_sweep must be an integer from 0 to 4 (inclusive)!\n');
        valid_args = false;
    end
    if mod(num_params, 1) ~= 0
        fprintf('ERROR: num_params must be an integer!\n');
        valid_args = false;
    end
    if alg_idx ~= 1 && alg_idx ~= 2
        fprintf('ERROR: alg_idx must be 1 or 2!\n');
        valid_args = false;
    end
    
    % if no parameter is being sweeped, make sure it doesn't just repeat the same optimization num_params times
    if param_to_sweep == 0
        num_params = 1;
    end
    
    if nargin == 13
        tumor_number = str2num(varargin{13});
    end
end

if ~valid_args
    fprintf('\n');
    fprintf('PowerOptimizer requires 10 arguments. In order:\n');
    fprintf('base_path: path to the directory, e.g. /var/fullmonte/Colin27/675nm/\n');
    fprintf('mesh_name: name used for the mesh and associated files (e.g. Colin27 or Digimouse)\n');
    fprintf('mesh_number: ID for this run (typically starting at 1, and set when optimizing a range of meshes in a batch)\n');
    fprintf('wavelength: e.g. 675nm, used for naming output files\n');
    fprintf('num_packets: number of packets launched in FullMonte simulation (set in the tcl script)\n');
    fprintf('total_energy: total energy delivered through all sources, in Joules\n');
    fprintf('dvh_percentage_range: the maximum dose percentage to show on the x-axis of the dose-volume histograms\n');
    fprintf('param_to_sweep: 0 for no parameter varying, 1 to vary both guard bands, 2 to vary only the healthy guard band, ');
    fprintf('3 to vary only the total_energy, 4 to vary only tumor_weight\n');
    fprintf('num_params: the range of parameter values to test\n');
    fprintf('alg_idx: the LP algorithm to use: 1 for dual-simplex, 2 for interior-point\n');
    fprintf('lp_tumor_weight: Tumor weight in the LP if not being swept\n');
    fprintf('cimmino_tumor_weight: Tumor weight in the Cimmino algorithm if not being swept.\n');
    
    return;
end

error_log = fopen(strcat(base_path, 'MATLAB_ERROR_LOG.txt'), 'a');

fprintf('Received parameters: base_path = %s, mesh_name = %s, mesh_number = %d, wavelength = %s, num_packets = %f, total_energy = %f, dvh_percentage_range = %d, num_params = %d\n\n', ...
    base_path, mesh_name, mesh_number, wavelength, num_packets, total_energy, dvh_percentage_range, num_params);

tumor_name = strcat(mesh_name, '_tumor', num2str(mesh_number), '_', num2str(wavelength));

results_file = fopen(strcat('Cimmino_results/line_sources/fixed_length/tumor_v100/', num2str(wavelength), 'nm.txt'), 'a');

% whether or not to normalize dose after optimization so that every tumor
% cell achieves the minimum dose
cimmino_normalization = false;

fprintf('Using guard bands for Cimmino.\n\n');

% whether or not to write fluences and cost function values to a file
% after optimization
write_solutions = false;

max_source_power = Inf;
total_power_limit = 1;
fprintf('Setting upper bound of %f on each source intensity, total power limit of %f for mesh %s.\n\n',...
        max_source_power, total_power_limit, tumor_name);

given_total_energy = total_energy; % need to save this since it will be modified if we're varying total_energy

sweep_healthy_guard_band = false;
sweep_tumor_guard_band = false;
vary_total_energy = false;
vary_tumor_slope = false;

if param_to_sweep == 1
    sweep_healthy_guard_band = true;
    sweep_tumor_guard_band = true;
elseif param_to_sweep == 2
    sweep_healthy_guard_band = true;
elseif param_to_sweep == 3
    vary_total_energy = true;
elseif param_to_sweep == 4
    vary_tumor_slope = true;
end

for param_idx=1:num_params
    % sweeping values for guard band
    if sweep_tumor_guard_band
        tumor_guard_band = 1 + 0.05*(param_idx - 1);
    else
        tumor_guard_band = 1;%.1;
    end
    if sweep_healthy_guard_band
        healthy_guard_band = 1 - 0.05*(param_idx - 1);
    else
        healthy_guard_band = 0.9;
    end

    if vary_total_energy
        % chosen so that 20 data points give energies from 1 to 1024 times the given total_energy
        total_energy = given_total_energy * 2^((param_idx-1)/1.9);
    else
        total_energy = given_total_energy;
    end
    
    if vary_tumor_slope
        % chosen so that 20 data points give weights from ~1 to ~100
        tumor_weight = 10 * 2^(((param_idx-0.5) - num_params/2)/3);
    else
        tumor_weight = lp_tumor_weight;% 150;
    end
    
    %for tumor_weight = 200:50:1000
        fprintf('Doing this run with tumor_guard_band=%f, healthy_guard_band=%f, total_energy=%f, tumor_weight=%f\n',...
            tumor_guard_band, healthy_guard_band, total_energy, tumor_weight);

        % read necessary data from files, threshold by power limit, apply guard bands
        tic
        [slopes, thresholds, hard_thresholds, tumor_overdose_slope, tumor_overdose_threshold, ...
                  fluences_matrix, fluences_matrix_full, ...
                  tissue_types, tissue_types_full, ...
                  volumes, volumes_full, ...
                  tumor_idx, num_tumor_tetras, tissue_names, num_tissues ...
                  ] = ParseInputData(base_path, mesh_name, mesh_number, num_packets / total_energy, ...
                    healthy_guard_band, tumor_guard_band, tumor_weight, wavelength);
        read_time = toc;
        fprintf('\nParsing time = %f\n\n', read_time);

        if ~all(fluences_matrix_full >= 0)
            fprintf('ERROR: At least one fluence matrix entry from ParseInputData is negative!\n');
            fprintf(error_log, '%s: %s, parameter %d.%d: At least one fluence matrix entry from ParseInputData is negative!\n',...
                datestr(now), tumor_name, param_to_sweep, param_idx);
            return;
        end

        % on the first run, allocate memory for all the arrays
        if param_idx == 1
            num_tetras_full = size(fluences_matrix_full, 1);
            num_sources = size(fluences_matrix_full, 2);

            % variables for recording performance
            runtime_ls = zeros(num_params,1);
            runtime_lp = zeros(num_params,1);
            d_90_ls = -ones(num_params,num_tissues);
            v_90_ls = -ones(num_params,num_tissues);
            v_90_volume_ls = -ones(num_params, num_tissues);
            v_100_ls = -ones(num_params,num_tissues);
            v_100_volume_ls = -ones(num_params, num_tissues);
            
            d_90_lp = -ones(num_params,num_tissues,3); % last index is for non-renormalized, renormalized to match v90, renormalized to match d90, respectively
            v_90_lp = -ones(num_params,num_tissues,3);
            v_90_volume_lp = -ones(num_params, num_tissues, 3);
            
            dose_count_lp = zeros(num_params,dvh_percentage_range,num_tissues,3);
            dose_count_lp_volume = zeros(num_params,dvh_percentage_range,num_tissues,3);

            total_cost_ls = zeros(num_params,1);
            total_cost_lp = zeros(num_params,1);

            x_ls = zeros(num_sources,num_params);
            x_lp = zeros(num_sources,num_params);
            fluences_ls = zeros(num_tetras_full,num_params);
            fluences_lp = zeros(num_tetras_full,num_params);
            
            integral_overdose_lp = zeros(num_params,1);
            integral_overdose_ls = zeros(num_params,1);
        end

        num_tetras_full = size(fluences_matrix_full, 1);
        num_tetras = size(fluences_matrix, 1);
        num_sources = size(fluences_matrix_full, 2);

        %%% run LP optimization
        run_lp = false;
        %tumor_number = 1;
        
        if (run_lp)
            tic
            [f_lp, A_lp, b_lp] = LPOptimizerSetup(fluences_matrix, slopes, tumor_overdose_slope,...
                thresholds, tumor_overdose_threshold, tissue_types, volumes, total_power_limit, 0, num_tumor_tetras);
            setup_time_lp = toc;
            fprintf('\nLP setup time = %f\n\n', setup_time_lp);

            tic

            if tumor_overdose_threshold ~= 0
                ub = Inf(num_tetras+num_sources+num_tumor_tetras, 1);
                lb = zeros(num_tetras+num_sources+num_tumor_tetras, 1);
            else
                ub = Inf(num_tetras+num_sources, 1);
                lb = zeros(num_tetras+num_sources, 1);
            end
            ub(1:num_sources) = max_source_power * ones(num_sources, 1);

            [x_lp(:,param_idx), fval, exitflag] = LPOptimizer(f_lp, A_lp, b_lp, lb, ub, num_sources, alg_idx);

            if (exitflag ~= 1)
                fprintf('Optimizer did not converge\n');
                return;
            end

            fluences_lp(:,param_idx) = fluences_matrix_full * x_lp(:,param_idx); % for producing DVH (counts low-dose tetras)
            fluences_lp_reduced = fluences_matrix*x_lp(:,param_idx);


            runtime_lp(param_idx) = toc + setup_time_lp;

            total_cost_lp(param_idx) = fval;

            overdose_cost = 0;
            underdose_cost = 0; 
            for i=1:num_tetras
                if tissue_types(i) ~= 0
                    if tissue_types(i) == tumor_idx % cost due to tumor cells
                        threshold_difference = fluences_lp_reduced(i) - thresholds(tissue_types(i));
                        if threshold_difference < 0 % under-dosed tumor cell
                            costs_lp = threshold_difference * slopes(tissue_types(i)) * volumes(i);
                            %underdose_cost = underdose_cost + costs_lp;
                            underdose_cost = underdose_cost + (-1*threshold_difference * volumes(i));
                        end
                    else
                        threshold_difference = fluences_lp_reduced(i) - thresholds(tissue_types(i));
                        if threshold_difference > 0 % over-dosed healthy cell
                            costs_lp = threshold_difference * slopes(tissue_types(i)) * volumes(i);
                            %overdose_cost = overdose_cost + costs_lp;
                            overdose_cost = overdose_cost + (threshold_difference * volumes(i));
                        end
                    end
                end
            end

            fprintf('Optimal power distribution with LP optimization, ');
            if alg_idx == 1
                fprintf('dual-simplex');
            elseif alg_idx == 2
                fprintf('interior-point');
            end
            fprintf(' algorithm, with parameter of %d: (last 10 powers)\n', param_idx);
            if size(x_lp) > 10
                fprintf('%f\n', x_lp((end-10):end,param_idx));
            else
                fprintf('%f\n', x_lp(:, param_idx));
            end
            fprintf('||x||_1 = %f, total energy used = %f, total cost = %e, time taken = %f\n', sum(x_lp(:,param_idx)), ...
                sum(x_lp(:,param_idx)) * total_energy, total_cost_lp(param_idx), runtime_lp(param_idx));

            fprintf('Overdose cost = %g\n', overdose_cost);
            fprintf('Underdose cost = %g\n', underdose_cost);


            % produce cumulative dose-volume histogram for LP
            dvh_filename_prefix_lp = strcat(base_path, 'matlab_optimization_output/dvh/LP_dvh_', tumor_name, '_', ...
                        num2str(num_sources), 'sources_param', num2str(param_to_sweep), '.', num2str(param_idx));
            [dose_count_lp(param_idx, :, :, 1), dose_count_lp_volume(param_idx, :, :, 1), d_90_lp(param_idx, :, 1), v_90_lp(param_idx, :, 1), v_90_volume_lp(param_idx, :, 1)] = ...
                DoseVolumeHistogram(dvh_percentage_range, fluences_lp(:,param_idx), hard_thresholds, tissue_types_full, ...
                num_tissues, volumes_full, dvh_filename_prefix_lp);
        else
            %addpath /home/yassineab/PhD_Research/src_placement/build/build_cmake/Matlab_tests/fixed_sources_with_ignored_tetra/point_sources/%line_sources/fixed_length/
            %../../build/build_cmake/Matlab_tests/fixed_sources_with_ignored_tetras
            
            %sim_res_path = '/home/yassineab/PhD_Research/src_placement/build/build_cmake/Matlab_tests/fixed_sources_with_ignored_tetra/line_sources/fixed_length/'
            sim_res_path = '/media/yassineab/DATAPART1/PhD_Research_temp_data/Matlab_tests/fixed_sources/line_sources/fixed_length/'

            file_name = strcat(sim_res_path, 'Colin27_tagged_tumor_', num2str(tumor_number), '_', num2str(wavelength), 'nm.m')% '_fixed_sources_', num2str(wavelength), 'nm.m')
            run(file_name);
            
            overdose_cost = 0;
            underdose_cost = 0;
            skull_overdose_cost = 0;
            for i=1:size(fluences_final,1)
                if material_ids(i) ~= 0
                    if material_ids(i) == tumor_idx % cost due to tumor cells
                        threshold_difference = fluences_final(i) - thresholds_cpp(material_ids(i));
                        if threshold_difference < 0 % under-dosed tumor cell
                            costs_lp = threshold_difference * slopes(material_ids(i)) * tetra_volumes(i);
                            %underdose_cost = underdose_cost + costs_lp;
                            underdose_cost = underdose_cost + (-1*threshold_difference * tetra_volumes(i));
                        end
                    else
                        threshold_difference = fluences_final(i) - thresholds_cpp(material_ids(i));
                        if threshold_difference > 0 % over-dosed healthy cell
                            costs_lp = threshold_difference * slopes(material_ids(i)) * tetra_volumes(i);
                            %overdose_cost = overdose_cost + costs_lp;
                            overdose_cost = overdose_cost + (threshold_difference * tetra_volumes(i));
                            if material_ids(i) == 1
                                skull_overdose_cost = skull_overdose_cost + (threshold_difference*tetra_volumes(i));
                            end
                        end
                    end
                end
            end
            
            integral_overdose_lp(param_idx) = overdose_cost;

            fprintf('Optimal power distribution with LP optimization ');
            
            fprintf('(last 10 powers)\n');
            if size(source_powers,1) > 10
                fprintf('%f\n', source_powers((end-10):end));%,param_idx));
            else
                fprintf('%f\n', source_powers);
            end
            
            fprintf('||x||_1 = %f, total energy used = %f, total cost = %e, time taken = %f\n', sum(source_powers), ...
                sum(source_powers) * total_energy, total_cost_lp_cpp, runtime_lp_cpp);

            fprintf('Overdose cost = %g\n', overdose_cost);
            fprintf('Skull Overdose cost = %g\n', skull_overdose_cost);
            fprintf('Percentage of skull overdose cost = %g%%\n', (skull_overdose_cost/overdose_cost)*100);
            fprintf('Underdose cost = %g\n', underdose_cost);

            x_lp(:,param_idx) = source_powers;
            total_cost_lp(param_idx) = total_cost_lp_cpp;
            runtime_lp(param_idx) = runtime_lp_cpp;
            fluences_lp(:, param_idx) = fluences_final;
            fval = total_cost_lp_cpp;

            % produce cumulative dose-volume histogram for LP
            dvh_filename_prefix_lp = strcat(base_path, 'matlab_optimization_output/dvh/LP_dvh_', tumor_name, '_', ...
                        num2str(num_sources), 'sources_param', num2str(param_to_sweep), '.', num2str(param_idx));
            [dose_count_lp(param_idx, :, :, 1), dose_count_lp_volume(param_idx, :, :, 1), d_90_lp(param_idx, :, 1), v_90_lp(param_idx, :, 1), v_90_volume_lp(param_idx, :, 1)] = ...
                DoseVolumeHistogram(dvh_percentage_range, fluences_final, hard_thresholds, material_ids, ...
                num_tissues, volumes_full, dvh_filename_prefix_lp);
        end

         fprintf('\nTumor v_90 LP: %g%%\n', v_90_lp(param_idx, 5, 1))

        fprintf('White Matter v_90 LP: %gcm^3, %g%%\n', v_90_volume_lp(param_idx, 4, 1), v_90_lp(param_idx, 4, 1))
        fprintf('Grey Matter v_90 LP: %gcm^3, %g%%\n', v_90_volume_lp(param_idx, 3, 1), v_90_lp(param_idx, 3, 1))
        fprintf('Skull and Scalp v_90 LP: %gcm^3, %g%%\n', v_90_volume_lp(param_idx, 1, 1), v_90_lp(param_idx, 1, 1))
        fprintf('CSF v_90 LP: %gcm^3, %g%%\n', v_90_volume_lp(param_idx, 2, 1), v_90_lp(param_idx, 2, 1))
%         return;
    %end
    %%% setup for Cimmino feasibility algorithm

    % first num_tetras rows are fluence constraints, last row is total power constraint on sources
    % TODO: Cimmino solution doesn't apply an upper bound to tumor dose!
    
   dose_count_ls = zeros(500, num_tissues);
   dose_count_ls_volume = zeros(500, num_tissues);
    
   % looping to achieve a 98% v_100 ------- Added by Abed
   prev_cimmino_weight = 1;
   first_weight_trial = true;
   while(true)
       
       fprintf('\n\nCurrent Cimmino Tumor weight is: %g\n\n', cimmino_tumor_weight)
        %cimmino_tumor_weight = 15;

        tic
        % first num_tetras rows are fluence constraints, last row is total power constraint on sources
        if total_power_limit ~= 0
            A_ls = [ fluences_matrix; ones(1, size(fluences_matrix, 2)) ];
            b_ls = zeros(num_tetras+1, 1);
            b_ls(num_tetras+1) = total_power_limit; %sum(x_lp(:,param_idx)); %
            % weights for different tissue types for Cimmino. Last element is for power constraint
            w = zeros(num_tetras+1, 1);
        else
            A_ls = fluences_matrix;
            b_ls = zeros(num_tetras, 1);
            w = zeros(num_tetras, 1);
        end

        tumor_tetra_indices = (tissue_types(:) == tumor_idx);
        tumor_tetra_indicator = ones(num_tetras,1) - 2*tumor_tetra_indices; % +1 for non-tumor tetras, -1 for tumor tetras
        w = slopes(tissue_types(:));

        % Added by Abed
        w(tissue_types == tumor_idx) = (w(tissue_types == tumor_idx)/tumor_weight)*cimmino_tumor_weight;

        w = w .* volumes;
        w = w .* tumor_tetra_indicator; % since tumor slope is negative, but weight should be positive
        b_ls(1:num_tetras) = thresholds(tissue_types(:)) .* tumor_tetra_indicator;
        for i=1:num_tetras
            A_ls(i,:) = tumor_tetra_indicator(i) * A_ls(i,:);
        end

        % power constraint must be enforced stronger than other constraints
        if total_power_limit ~= 0
            w(num_tetras+1) = 2 * max(w);
        end

        if ~all(w >= 0)
            error_text = 'Some weights for Cimmino are negative!';
            fprintf('ERROR: %s\n', error_text);
            fprintf(error_log, '%s: %s, parameter %d.%d: %s\n', datestr(now), tumor_name, param_to_sweep, param_idx, error_text);
            return
        end

        setup_time_ls = toc;
        fprintf('\nCimmino setup time = %f\n\n', setup_time_ls);

        %%% run Cimmino optimization

        costs_filename = strcat(base_path, 'matlab_optimization_output/cimmino_cost/cost_vs_time_', tumor_name, ...
            '_', num2str(num_sources), 'sources_param', num2str(param_to_sweep), '.', num2str(param_idx));
    
            
        % Added by abed temporarily
        lp_runtime = runtime_lp(param_idx);
        if (lp_runtime > 10)
            lp_runtime = lp_runtime/10;
        end
        [x_ls(:,param_idx), runtime_ls(param_idx)] = CimminoOptimizer(A_ls, b_ls, zeros(num_sources,1), max_source_power * ones(num_sources,1),...
            w, zeros(num_sources,1), (total_power_limit~=0), lp_runtime, false, thresholds, tissue_types, ...
            fluences_matrix, volumes, slopes, tumor_idx, costs_filename, fval, total_power_limit);
        runtime_ls(param_idx) = runtime_ls(param_idx) + setup_time_ls;

        fluences_ls_reduced = fluences_matrix * x_ls(:,param_idx); % for calculating cost (ignores low-dose tetras)
        fluences_ls(:, param_idx) = fluences_matrix_full * x_ls(:,param_idx);  % for producing DVH (counts low-dose tetras)

        % normalizing so that all tumor cells achieve minimum fluence levels
        if cimmino_normalization
            min_fluence = Inf;
            for i=1:num_tetras
                if tissue_types(i) == tumor_idx
                    if fluences_ls_reduced(i) < min_fluence
                        min_fluence = fluences_ls_reduced(i);
                    end
                end
            end
            if min_fluence == 0
                error_text = 'Impossible to normalize: some tetras received a fluence of 0!';
                fprintf('ERROR: %s\n', error_text);
                fprintf(error_log, '%s: %s, parameter %d.%d: %s\n', datestr(now), tumor_name, param_to_sweep, param_idx, error_text);
                return;
            elseif scale_factor > 100
                fprintf('WARNING: Scaling Cimmino source power by %f times!', scale_factor');
            end
            scale_factor = hard_thresholds(tumor_idx) / min_fluence

            fluences_ls(:, param_idx) = fluences_ls(:, param_idx) * scale_factor;
            x_ls(:,param_idx) = x_ls(:,param_idx) * scale_factor;
        end



        % produce cumulative dose-volume histogram for Cimmino
        dvh_filename_prefix_ls = strcat(base_path, 'matlab_optimization_output/dvh/LS_dvh_', tumor_name, '_', ...
            num2str(num_sources), 'sources_param', num2str(param_to_sweep), '.', num2str(param_idx));
        [dose_count_ls, dose_count_ls_volume, d_90_ls(param_idx, :), v_90_ls(param_idx, :), v_90_volume_ls(param_idx, :), v_100_ls(param_idx, :), v_100_volume_ls(param_idx, :)] = DoseVolumeHistogram(dvh_percentage_range, fluences_ls(:, param_idx), hard_thresholds, ...
            tissue_types_full, num_tissues, volumes_full, dvh_filename_prefix_ls);
        
        
        cimmino_dvh_percentage = dose_count_ls;
        cimmino_dvh_volume = dose_count_ls_volume;

        % evaluate total cost (according to LP cost function)
        costs_ls = zeros(num_tetras,1);

        overdose_cost = 0;
        underdose_cost = 0; 
        for i=1:num_tetras
            if tissue_types(i) ~= 0
                if tissue_types(i) == tumor_idx % cost due to tumor cells
                    threshold_difference = fluences_ls_reduced(i) - thresholds(tissue_types(i));
                    if threshold_difference < 0 % under-dosed tumor cell
                        costs_ls(i) = threshold_difference * slopes(tissue_types(i)) * volumes(i);
                        %underdose_cost = underdose_cost + costs_ls(i);
                        underdose_cost = underdose_cost + (-1*threshold_difference * volumes(i));
                    end
                else
                    threshold_difference = fluences_ls_reduced(i) - thresholds(tissue_types(i));
                    if threshold_difference > 0 % over-dosed healthy cell
                        costs_ls(i) = threshold_difference * slopes(tissue_types(i)) * volumes(i);
                        %overdose_cost = overdose_cost + costs_ls(i);
                        overdose_cost = overdose_cost + (threshold_difference * volumes(i));
                    end
                end
            end
        end
        total_cost_ls(param_idx) = sum(costs_ls);
        integral_overdose_ls(param_idx) = overdose_cost;
        % sanity check: all costs should be nonnegative
        if ~all(costs_ls >= 0)
            error_text = 'At least one cost is negative! (Cimmino method)';
            fprintf('ERROR: %s\n', error_text);
            fprintf(error_log, '%s: %s, parameter %d.%d: %s\n', datestr(now), tumor_name, param_to_sweep, param_idx, error_text);
            return;
        end

        fprintf('Optimal power distribution with Cimmino optimization (least squares solution), with parameter of %d: (last 10 powers)\n', param_idx)
        if size(x_ls) > 10
            fprintf('%f\n', x_ls((end-10):end,param_idx));
        else
            fprintf('%f\n', x_ls(:, param_idx));
        end
        fprintf('||x||_1 = %f, total energy used = %f, total cost = %e, time taken = %f\n', sum(x_ls(:,param_idx)), ...
            sum(x_ls(:,param_idx)) * total_energy, total_cost_ls(param_idx), runtime_ls(param_idx));
        fprintf('Overdose cost = %g\n', overdose_cost);
        fprintf('Underdose cost = %g\n', underdose_cost);
        
        
        temp_prev = prev_cimmino_weight;
        prev_cimmino_weight = cimmino_tumor_weight;
        multiplier = 2;
        target_tumor_v100 = 98;
        lower_v100_bound = target_tumor_v100 - 1e-1;
        upper_v100_bound = target_tumor_v100 + 1e-1;
        
        current_v100 = v_100_ls(param_idx, 5);
        
        if (true) %current_v100 > lower_v100_bound && current_v100 < upper_v100_bound)
            break;
        end
        if (first_weight_trial)
            first_weight_trial = false;
            
            if (current_v100 > upper_v100_bound)
                cimmino_tumor_weight = cimmino_tumor_weight/multiplier;
            else
                cimmino_tumor_weight = cimmino_tumor_weight*multiplier;
            end
        else
            if (current_v100 > upper_v100_bound)
                if (temp_prev > cimmino_tumor_weight)
                    cimmino_tumor_weight = cimmino_tumor_weight/multiplier;
                else
                    cimmino_tumor_weight = cimmino_tumor_weight - ((cimmino_tumor_weight - temp_prev)/2);
                end
            else
                if (temp_prev < cimmino_tumor_weight)
                    cimmino_tumor_weight = cimmino_tumor_weight*multiplier;
                else
                    cimmino_tumor_weight = cimmino_tumor_weight + ((temp_prev - cimmino_tumor_weight)/2);
                end
            end
        end
        
        fprintf('\nTumor v_100 Cimmino: %g%%\n', v_100_ls(param_idx, 5))
       
        fprintf('White Matter v_100 Cimmino: %gcm^3, %g%%\n', v_100_volume_ls(param_idx, 4), v_100_ls(param_idx, 4))
       
        fprintf('Grey Matter v_100 Cimmino: %gcm^3, %g%%\n', v_100_volume_ls(param_idx, 3), v_100_ls(param_idx, 3))     
   end


    %{
    figure;
    hold on;
    for i=1:num_tissues
        plot(0:499, dose_count_ls(:,i));
    end
    title('Dose-volume histogram using Cimmino algorithm');
    xlabel('Percentage of threshold dose');
    ylabel('Percentage of tissue volume receiving given dose');
    legend(tissue_names);
%}
%    print(strcat(dvh_filename_prefix_ls, '.png'), '-dpng');
        %savefig(strcat(base_path, 'dvh/LS_dvh_', num2str(mesh_number), '.fig'));
 
    
    %%% dose-volume histograms for LP, normalized to Cimmino result

    % produce cumulative dvh after normalizing so that the tumor v_90 (percentage of the tumor volume with a dose equal to 
    % 90% or more of the threshold) is equal to that from the Cimmino algorithm

    if v_90_ls(param_idx, tumor_idx) == 100
        fprintf('Cimmino v_90 is 100 percent, so cannot renormalize LP solution to it!\n');
        scale_factor = 1;
    else
        % find dose for LP solution at Cimmino solution's v_90 - run through doses in order from largest to smallest 
        % until a volume equal to Cimmino's v_90 has been accounted for, then the average of the dose received by the 
        % two tetras on each side of the threshold is the dose at Cimmino's v_90
        tumor_doses_lp = zeros(num_tumor_tetras, 1);
        tumor_volumes = zeros(num_tumor_tetras, 1);
        idx = 1;
        for i=1:num_tetras_full
            if tissue_types_full(i) == tumor_idx
                tumor_doses_lp(idx) = 100 * (fluences_lp(i, param_idx) / hard_thresholds(tumor_idx));
                tumor_volumes(idx) = volumes_full(i);
                idx = idx + 1;
            end
        end

        idx = 1;
        [sorted_doses, sorted_indices] = sort(tumor_doses_lp, 'descend');
        cumulative_volume = tumor_volumes(sorted_indices(1));
        while cumulative_volume < sum(tumor_volumes) * 0.01 * v_90_ls(param_idx, tumor_idx) && (idx <= num_tumor_tetras)
            cumulative_volume = cumulative_volume + tumor_volumes(sorted_indices(idx));
            idx = idx + 1;
        end
        % check for error-case of very low v_90 from Cimmino solution
        if idx > 1
            %TODO: linear interpolation
            %dose_at_cimmino_v90 = sorted_doses(idx) + (sorted_doses(idx-1) - sorted_doses(idx)) * ...
            %        (cumulative_volume - 0.01 * v_90_ls(param_idx, tumor_idx) * sum(tumor_volumes)) / tumor_volumes(sorted_indices(idx));
            dose_at_cimmino_v90 = sorted_doses(idx);
            
            scale_factor = 90 / dose_at_cimmino_v90;
            if scale_factor < 0
                scale_factor = 0;
            end
        else
            % make the error obvious by making the fluences zero
            scale_factor = 0;
            error_text = 'Cimmino v90 extremely low, setting scale_factor = 0.';
            fprintf('ERROR: %s\n', error_text);
            fprintf(error_log, '%s: %s, parameter %d.%d: %s\n', datestr(now), tumor_name, param_to_sweep, param_idx, error_text);
        end
    end
    fprintf('scale_factor for matching LP v_90 to Cimmino v_90 is %f\n\n', scale_factor);
    
    dvh_filename_prefix_lp_v90 = strcat(dvh_filename_prefix_lp, '_Cimmino_v90');
    x_lp_normalized_v90 = x_lp(:,param_idx) * scale_factor;
    fluences_lp_normalized_v90 = fluences_lp(:,param_idx) * scale_factor;
    fluences_lp_reduced_normalized_v90 = fluences_matrix * x_lp_normalized_v90;
    [dose_count_lp(param_idx, :, :, 2), dose_count_lp_volume(param_idx, :, :, 2), d_90_lp(param_idx, :, 2), v_90_lp(param_idx, :, 2), v_90_volume_lp(param_idx, :, 2)] = ...
        DoseVolumeHistogram(dvh_percentage_range, fluences_lp_normalized_v90, ...
            hard_thresholds, tissue_types_full, num_tissues, volumes_full, dvh_filename_prefix_lp_v90);
    
    % evaluate total cost (according to LP cost function)
    costs_lp_normalized_v90 = zeros(num_tetras,1);
    for i=1:num_tetras
        if tissue_types(i) ~= 0
            if tissue_types(i) == tumor_idx % cost due to tumor cells
                threshold_difference = fluences_lp_reduced_normalized_v90(i) - thresholds(tissue_types(i));
                if threshold_difference < 0 % under-dosed tumor cell
                    costs_lp_normalized_v90(i) = threshold_difference * slopes(tissue_types(i)) * volumes(i);
                end
            else
                threshold_difference = fluences_lp_reduced_normalized_v90(i) - thresholds(tissue_types(i));
                if threshold_difference > 0 % over-dosed healthy cell
                    costs_lp_normalized_v90(i) = threshold_difference * slopes(tissue_types(i)) * volumes(i);
                end
            end
        end
    end
    total_cost_lp_normalized_v90 = sum(costs_lp_normalized_v90);
        
    % produce dvh after normalizing so that d_90 (dose received by 90% of the tumor volume) is equal to that 
    % from the cimmino algorithm
    scale_factor = d_90_ls(param_idx, tumor_idx) / d_90_lp(param_idx, tumor_idx, 1);
    % check for error-cases of d_90_lp = 0 or d_90_ls = -1, respectively
    if scale_factor == Inf || ~(scale_factor >= 0)
        % make the error obvious by making the fluences zero
        error_text = 'scale_factor for matching LP d_90 to Cimmino d_90 is invalid (value of %f), setting it to zero.';
        fprintf('Warning: %s\n', error_text);
        fprintf(error_log, '%s: %s, parameter %d.%d: %s\n', datestr(now), tumor_name, param_to_sweep, param_idx, error_text);
        scale_factor = 0;
    end
    fprintf('scale_factor for matching LP d_90 to Cimmino d_90 is %f\n\n', scale_factor);
    
    dvh_filename_prefix_lp_d90 = strcat(dvh_filename_prefix_lp, '_Cimmino_d90');
    x_lp_normalized_d90 = x_lp(:,param_idx) * scale_factor;
    fluences_lp_normalized_d90 = fluences_lp(:,param_idx) * scale_factor;
    fluences_lp_reduced_normalized_d90 = fluences_matrix * x_lp_normalized_d90;
    [dose_count_lp(param_idx, :, :, 3), dose_count_lp_volume(param_idx, :, :, 3), d_90_lp(param_idx, :, 3), v_90_lp(param_idx, :, 3), v_90_volume_lp(param_idx, :, 3)] = ...
        DoseVolumeHistogram(dvh_percentage_range, fluences_lp_normalized_d90, ...
        hard_thresholds, tissue_types_full, num_tissues, volumes_full, dvh_filename_prefix_lp_d90);
    
    % evaluate total cost (according to LP cost function)
    costs_lp_normalized_d90 = zeros(num_tetras,1);
    for i=1:num_tetras
        if tissue_types(i) ~= 0
            if tissue_types(i) == tumor_idx % cost due to tumor cells
                threshold_difference = fluences_lp_reduced_normalized_d90(i) - thresholds(tissue_types(i));
                if threshold_difference < 0 % under-dosed tumor cell
                    costs_lp_normalized_d90(i) = threshold_difference * slopes(tissue_types(i)) * volumes(i);
                end
            else
                threshold_difference = fluences_lp_reduced_normalized_d90(i) - thresholds(tissue_types(i));
                if threshold_difference > 0 % over-dosed healthy cell
                    costs_lp_normalized_d90(i) = threshold_difference * slopes(tissue_types(i)) * volumes(i);
                end
            end
        end
    end
    total_cost_lp_normalized_d90 = sum(costs_lp_normalized_d90);
    %{
    figure()
    yyaxis left
    plot(0:499, dose_count_lp(param_idx, :, 5, 1), 'b')
    hold on 
    plot(0:499, dose_count_ls( :, 5), '--b')
    ylabel('Tumor Volume Receiving given Dose (\%)');
    xlabel('Percentage of Optimization Threshold (\%)');
    
    yyaxis right
    plot(1:499, dose_count_lp_volume(param_idx, 2:500, 4, 1), 'r')
    hold on 
    plot(1:499, dose_count_ls_volume( 2:500, 4), '--r')
    
    plot(1:499, dose_count_lp_volume(param_idx, 2:500, 3, 1), 'g')
    hold on 
    plot(1:499, dose_count_ls_volume( 2:500, 3), '--g')
    
    ylabel('Healthy Tissue Volume Receiving given Dose ($cm^3$)')
    % 
    %}

    fprintf('\nTumor v_100 Cimmino: %g%%\n', v_100_ls(param_idx, 5))
    %fprintf('Tumor v_90 LP: %g%%\n\n', v_90_lp(param_idx, 5, 1))
    
    
    fprintf('White Matter v_100 Cimmino: %gcm^3, %g%%\n', v_100_volume_ls(param_idx, 4), v_100_ls(param_idx, 4))
    %fprintf('White Matter v_90 LP: %gcm^3, %g%%\n', v_90_volume_lp(param_idx, 4, 1), v_90_lp(param_idx, 4, 1))
    %fprintf('Percent Change = %g%%\n\n', ((v_90_volume_lp(param_idx, 4, 1) - v_90_volume_ls(param_idx, 4))/v_90_volume_ls(param_idx, 4))*100);
    
    
    fprintf('Grey Matter v_100 Cimmino: %gcm^3, %g%%\n', v_100_volume_ls(param_idx, 3), v_100_ls(param_idx, 3))
    %fprintf('Grey Matter v_90 LP: %gcm^3, %g%%\n', v_90_volume_lp(param_idx, 3, 1), v_90_lp(param_idx, 3,1))
    %fprintf('Percent Change = %g%%\n\n', ((v_90_volume_lp(param_idx, 3, 1) - v_90_volume_ls(param_idx, 3))/v_90_volume_ls(param_idx, 3))*100);

    
    fprintf(results_file, '%s\t%s\t%s\t%s\t%s\t%d\t%f\t%g\t%g\t%g\t%g\t%g\t%g\t%f\n', datestr(now), tumor_name, 'line', 'fixed', 'Cimmino',...
        sum(x_ls(:,param_idx) > 0), cimmino_tumor_weight, total_cost_ls(param_idx), v_100_ls(param_idx, 5), ...
        v_100_volume_ls(param_idx, 3), v_100_volume_ls(param_idx,4), runtime_ls(param_idx), sum(x_ls(:,param_idx)), total_energy);
   return;
    
    
    % print all 3 dvh's for LP solution
    %{
    for i=1:3
        figure;
        hold on;
        for j=1:num_tissues
            plot(0:499, squeeze(dose_count_lp(param_idx,:,j,i)));
        end
        xlabel('Percentage of threshold dose');
        ylabel('Percentage of tissue volume receiving given dose');
        legend(tissue_names);
        if i==1
            figure_title = strcat('Fixed-src LP with ', {' '}, num2str(healthy_guard_band), ':', num2str(tumor_guard_band),...
                ' guard bands, non-renormalized');
            title(figure_title);
            %print(strcat(dvh_filename_prefix_lp, '.png'), '-dpng');
        elseif i==2
            figure_title = strcat('Fixed-src LP with ', {' '}, num2str(healthy_guard_band), ':', num2str(tumor_guard_band),...
                ' guard bands, renormalized to match Cimmino v_{90}');
            title(figure_title);
            %print(strcat(dvh_filename_prefix_lp_v90, '.png'), '-dpng');
        elseif i==3
            figure_title = strcat('Fixed-src LP with ', {' '}, num2str(healthy_guard_band), ':', num2str(tumor_guard_band),...
                ' guard bands, renormalized to match Cimmino d_{90}');
            title(figure_title);
            %print(strcat(dvh_filename_prefix_lp_d90, '.png'), '-dpng');
        end
    end
    %}
    
%     return;
    %%% output results
    % Cimmino
    %
    fprintf(results_file, '%s\t%s\t%s\t%d\t%f\t%f\t%f\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%f\n', datestr(now), tumor_name, 'Cimmino',...
        sum(x_ls(:,param_idx) > 0), healthy_guard_band, tumor_guard_band, cimmino_tumor_weight, total_cost_ls(param_idx), integral_overdose_ls(param_idx), v_90_ls(param_idx, 3), v_90_ls(param_idx, 4), ...
        v_90_ls(param_idx, 5), v_90_volume_ls(param_idx, 3), v_90_volume_ls(param_idx,4), runtime_ls(param_idx), sum(x_ls(:,param_idx)), total_energy);
%     return;

    if alg_idx == 1
        algorithm_name = 'dual-simplex';
    else
        algorithm_name = 'interior-point';
    end
    % LP
    fprintf(results_file, '%s\t%s\t%s\t%d\t%f\t%f\t%f\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%f\n', datestr(now), tumor_name, algorithm_name,...
        sum(x_lp(:,param_idx) > 0), healthy_guard_band, tumor_guard_band, tumor_weight, total_cost_lp(param_idx), integral_overdose_lp(param_idx), ...
        v_90_lp(param_idx, 3, 1), v_90_lp(param_idx, 4, 1), v_90_lp(param_idx, 5, 1), v_90_volume_lp(param_idx, 3, 1), v_90_volume_lp(param_idx, 4, 1), runtime_lp(param_idx, 1), ...
        sum(x_lp(:,param_idx)), total_energy);
    
    fclose(results_file);
    
    return;
    %{
    % LP, renormalized to match Cimmino v90
    fprintf(results_file, '%s\t%s\t%s\t%d\t%f\t%f\t%f\t%e\t%e\t%e\t%e\t%e\t%e\t%e\t%e\t%f\n', datestr(now), tumor_name, ...
        strcat(algorithm_name,'_v90'), sum(x_lp(:,param_idx) > 0), healthy_guard_band, tumor_guard_band, tumor_weight, ...
        total_cost_lp_normalized_v90, v_90_lp(param_idx, 3, 2), v_90_lp(param_idx, 4, 2), v_90_lp(param_idx, 5, 2), ...
        v_90_volume_lp(param_idx, 3, 2), v_90_volume_lp(param_idx, 4, 2), runtime_lp(param_idx, 1), sum(x_lp_normalized_v90), total_energy);
    % LP, renormalized to match Cimmino d90
    fprintf(results_file, '%s\t%s\t%s\t%d\t%f\t%f\t%f\t%e\t%e\t%e\t%e\t%e\t%e\t%e\t%e\t%f\n', datestr(now), tumor_name, ...
        strcat(algorithm_name, '_d90'), sum(x_lp(:,param_idx) > 0), healthy_guard_band, tumor_guard_band, tumor_weight, ...
        total_cost_lp_normalized_d90, v_90_lp(param_idx, 3, 3), v_90_lp(param_idx, 4, 3), v_90_lp(param_idx, 5, 3), ...
        v_90_volume_lp(param_idx, 3, 3), v_90_volume_lp(param_idx, 4, 3), runtime_lp(param_idx, 1), sum(x_lp_normalized_d90), total_energy);
    %}
    %RecordCostFunctionStats(base_path, tumor_name, 'Cimmino', strcat('fixed_', num2str(num_sources)), total_cost_ls(param_idx), ...
     %   v_90_ls(param_idx, :), d_90_ls(param_idx, :), healthy_guard_band, tumor_guard_band, tumor_weight,...
      %  sum(x_ls(:,param_idx)), total_energy);
    %RecordCostFunctionStats(base_path, tumor_name, 'LP', strcat('fixed_', num2str(num_sources)), total_cost_lp(param_idx), ...
     %   v_90_lp(param_idx, :, 1), d_90_lp(param_idx, :, 1), healthy_guard_band, tumor_guard_band, tumor_weight,...
      %  sum(x_lp(:,param_idx)), total_energy);

    if write_solutions
        format_spec = '%e\n';

        % write out optimal powers for both approaches
        powers_file_ls = fopen(strcat('LSPowers_', num2str(wavelength), ...
            '_', num2str(param_idx), '_', num2str(param_to_sweep), '.', num2str(param_idx), '.txt'), 'w');
        fprintf(powers_file_ls, format_spec, x_ls(:,param_idx));
        fclose(powers_file_ls);
        
        powers_file_lp = fopen(strcat('LPPowers_', num2str(wavelength), ...
            '_', num2str(param_idx), '_', num2str(param_to_sweep), '.', num2str(param_idx), '.txt'), 'w');
        fprintf(powers_file_lp, format_spec, x_lp(:, param_idx));
        fclose(powers_file_lp);
        
        % calculate full fluence matrix (including tetras for which it was ignored due to always being below the threshold)
        fluences_file_ls = fopen(strcat('LSFluences_', num2str(wavelength), ...
            '_', num2str(param_idx), '_', num2str(param_to_sweep), '.', num2str(param_idx), '.txt'), 'w');
        fprintf(fluences_file_ls, format_spec, fluences_ls(:, param_idx));
        fclose(fluences_file_ls);
        
        fluences_file_lp = fopen(strcat('LPFluences_', num2str(wavelength), ...
            '_', num2str(param_idx), '_', num2str(param_to_sweep), '.', num2str(param_idx), '.txt'), 'w');
        fprintf(fluences_file_lp, format_spec, fluences_lp(:, param_idx));
        fclose(fluences_file_lp);
    end
end

fclose('all');

return