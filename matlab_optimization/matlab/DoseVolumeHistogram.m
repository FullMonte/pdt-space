function [volume_total_percent, volume_total, d_90, v_90, v_90_volume, v_100, v_100_volume] = DoseVolumeHistogram(max_percentage, fluences, target_dose, tissue_types, num_tissues, ...
    volumes, filename_prefix)
% DoseVolumeHistogram  Produces cumulative dose-volume percentages (does not plot them)
% returns:
%   volume_total: max_percentage*num_tissues matrix where the (i,j)th element is the percentage of the volume of the 
%       j'th tissue receiving i% or more of the dose target
%   d_90: a num_tissues element vector, containing the percentage of the threshold dose received by 90% of the volume 
%       of the i'th tissue type
%   v_90: a num_tissues element vector, containing the percentage of the volume of the i'th tissue receiving 90% of 
%       the threshold dose
% arguments:
%	max_percentage: calculate number of tetras with doses from 0:max_percentage. 
%           dose_count will have max_percentage elements
%	fluences: vector of fluence absorbed by each tetra
% 	target_dose: vector of target dose for each tissue type
%	tissue_types: vector of tissue type of each tetra
%   num_tissues: number of distinct tissue types
%   volumes: volume of each tetrahedron
%   filename_prefix: append '.txt' to this to get the filename to write the results to

if ~all(fluences >= 0)
    fprintf('ERROR: At least one negative fluence in fluences_matrix in DoseVolumeHistogram function!\n');
    %quit force;
    return;
end

num_tetras = size(fluences, 1);

% if there's no dose received by <90% of tissue, 90% must be at max_percentage or higher
d_90 = max_percentage * ones(num_tissues, 1);   

% first, discretize into buckets from 0-499% of threshold dose, and total how much volume is at that percentage
volume_total_percent = zeros(max_percentage,num_tissues);
volume_total_clipped = zeros(num_tissues,1);
tissue_volume_counts = zeros(num_tissues,1);
for i=1:num_tetras
    if tissue_types(i) ~= 0
        if target_dose(tissue_types(i)) ~= 0
            dose_percentage = floor(100 * (fluences(i) / target_dose(tissue_types(i)))) + 1; % +1 since indexing starts at 1
            if dose_percentage < max_percentage
                volume_total_percent(dose_percentage, tissue_types(i)) = volume_total_percent(dose_percentage, tissue_types(i)) + volumes(i);
            else
                volume_total_clipped(tissue_types(i)) = volume_total_clipped(tissue_types(i)) + volumes(i);
            end
        end
        tissue_volume_counts(tissue_types(i)) = tissue_volume_counts(tissue_types(i)) + volumes(i);
    end
end
% make it into a cumulative dose count
volume_total_percent(max_percentage,:) = volume_total_percent(max_percentage,:) + volume_total_clipped.';
for i=1:num_tissues
    percent_clipped = 100 * volume_total_clipped(i) / tissue_volume_counts(i);
    if percent_clipped > 0.1
        fprintf('%f percent of tissue %d received over %d percent dose\n', percent_clipped, i, max_percentage);
    end
end

for i=(max_percentage-1):(-1):1
    volume_total_percent(i,:) = volume_total_percent(i,:) + volume_total_percent(i+1,:);
end

volume_total = volume_total_percent/1000; % divide by 1000 to convert to cm^3

% make it into a percentage0
for i=1:num_tissues
    volume_total_percent(:,i) = 100 * volume_total_percent(:,i) / tissue_volume_counts(i);
end

% count v_90
v_90 = volume_total_percent(91, :).';
v_100 = volume_total_percent(101, :).';
v_90_volume = (volume_total_percent(91,:)'/100).*tissue_volume_counts;
v_100_volume = (volume_total_percent(101,:)'/100).*tissue_volume_counts;

v_90_volume = v_90_volume/1000; % to convert to cm^3 from mm^3
v_100_volume = v_100_volume/1000; % to convert to cm^3 from mm^3


% count d_90 - run through doses in order from largest to smallest until 90% of the volume has been accounted
% for, then the average of the dose received by the two tetras on each side of the threshold is d_90
tissue_dose_lists = zeros(num_tetras, num_tissues);
for i=1:num_tetras
    current_tissue = tissue_types(i);
    if current_tissue ~= 0
        tissue_dose_lists(i, current_tissue) = 100 * (fluences(i) / target_dose(current_tissue));
    else
        fprintf('In DVH function, ignoring tetra with tissue type of 0 at i=%d\n', i);
    end
end

for i=1:num_tissues
    idx = 1;
    [sorted_doses, sorted_indices] = sort(tissue_dose_lists(:,i),'descend');
    cumulative_volume = volumes(sorted_indices(1));
    while cumulative_volume < 0.9 * tissue_volume_counts(i)
        idx = idx + 1;
        cumulative_volume = cumulative_volume + volumes(sorted_indices(idx));
    end
    % linear interpolation
    d_90(i) = sorted_doses(idx) + (sorted_doses(idx-1) - sorted_doses(idx)) * ...
            (cumulative_volume - 0.9 * tissue_volume_counts(i)) / volumes(sorted_indices(idx));
end

% write DVH data to a file
%{
output_file = fopen(strcat(filename_prefix, '.txt'), 'w');
fprintf(output_file, 'v90 by tissue type:\n');
for i=1:num_tissues
    fprintf(output_file, '%e\t', v_90(i));
end
fprintf(output_file, '\n');
fprintf(output_file, 'd90 by tissue type:\n');
for i=1:num_tissues
    fprintf(output_file, '%e\t', d_90(i));
end
fprintf(output_file, '\n');
fprintf(output_file, '\n');
fprintf(output_file, 'Doses: ith row and jth column is the percentage of tissue receiving at least i percent of the threshold dose\n');
for i=1:max_percentage
    for j=1:num_tissues
        fprintf(output_file, '%e\t', volume_total(i,j));
    end
    fprintf(output_file, '\n');
end
fclose(output_file);
%}
