function [slopes, thresholds, hard_thresholds, tumor_overdose_slope, tumor_overdose_threshold, ...
          fluences_matrix, fluences_matrix_full, ...
          tissue_types, tissue_types_full, ...
          volumes, volumes_full, ...
          tumor_idx, num_tumor_tetras, tissue_names, num_tissues ...
          ] = ParseInputData(base_path, mesh_name, mesh_number, packets_per_joule, healthy_guard_band,...
            tumor_guard_band, tumor_weight, wavelength)
% ParseProblemData   Reads files and parse data to be used for optimization

% read tissue names and thresholds from file
base_path
tissue_properties = importdata(strcat(base_path, 'tissue_properties_', num2str(wavelength), 'nm.txt'), ',');
tissue_names = tissue_properties.rowheaders;
tumor_idx = -1;
% these are the actual thresholds, provided by medical team
hard_thresholds = tissue_properties.data(:,1);
num_tissues = size(hard_thresholds, 1);
% these are the thresholds and slopes used in the LP cost function, which are healthy_guard_band times
% the hard thresholds for healthy tissue and tumor_guard_band times it for the tumor.
thresholds = zeros(num_tissues, 1);
slopes = zeros(num_tissues, 1);
first_threshold = -1;
for i=1:num_tissues
    % slopes are inversely proportional to hard_thresholds, so costs are proportional to % over/underdose
    if first_threshold == -1
        if hard_thresholds(i) > 1e-8
            first_threshold = hard_thresholds(i);
            slopes(i) = 1;
        end
    else
        if hard_thresholds(i) ~= 0
            slopes(i) = first_threshold / hard_thresholds(i);
        else
            slopes(i) = 0;
        end
    end
    
    if strcmp(tissue_names{i}, 'Tumor')
        if tumor_idx ~= -1
            fprintf('ERROR: Multiple rows with name ''Tumor'' in the tissue_properties.txt file!\n');
            return;
        end
        tumor_idx = i; % which tissue type is the tumor
        thresholds(i) = tumor_guard_band * hard_thresholds(i);
        slopes(i) = slopes(i) * -1;
    else
        thresholds(i) = healthy_guard_band * hard_thresholds(i);
    end
end

if tumor_idx == -1
    fprintf('ERROR: No row with name ''Tumor'' in the tissue_properties.txt file!\n');
    return;
end

if first_threshold == -1
    fprintf('ERROR: No lower dose threshold was above 1e-8 in the tissue_properties.txt file!\n');
    return;
end

% scale thresholds and slopes by packets per Joule, so they are now in units of packets/(distance^2)
thresholds = thresholds * packets_per_joule;
tumor_overdose_threshold = tissue_properties.data(tumor_idx, 2) * packets_per_joule;
hard_thresholds = hard_thresholds * packets_per_joule;
slopes = slopes * packets_per_joule;

tumor_overdose_slope = first_threshold / tumor_overdose_threshold;

% read tissue types and fluences and tetra volumes: these files contain:
%   fluences_matrix_full: a (num_tetras_full) x (num_sources) matrix, containing the fluence to each tetra
%   tissue_types_full: a (num_tetras_full) x 1 vector, containing the tissue type of each tetra
%   volumes_full: a (num_tetras_full) x 1 vector, containing the volume of each tetra
%%% Added by Abed
fprintf('Reading data......\n');
run(strcat(base_path, mesh_name, '_data.m'));


fluences_matrix_full = fluence_mat';%importdata(strcat(base_path, 'extracted_data/', mesh_name, '_', num2str(mesh_number), '_fluences.txt'));
tissue_types_full = material_ids;%importdata(strcat(base_path, 'extracted_data/', mesh_name, '_', num2str(mesh_number), '_tissues.txt'));
volumes_full = tetra_volumes;%importdata(strcat(base_path, 'extracted_data/', mesh_name, '_', num2str(mesh_number), '_volumes.txt'));

clear fluence_mat;
clear material_ids;
clear tetra_volumes;

%TODO: This is a temporary workaround for a FullMonte bug, remove this later!
%{
for j=size(fluences_matrix_full, 2):(-1):2
    fluences_matrix_full(:, j) = fluences_matrix_full(:, j) - fluences_matrix_full(:, j-1);
end
if ~all(fluences_matrix_full >= 0)
    display('ERROR: Negative entry in fluences_matrix_full! Could mean the fluence accumulation bug is gone!');
    return;
end
%}
fprintf('Largest fluence matrix element is %f\n', max(max(fluences_matrix_full)));

num_tetras_full = size(fluences_matrix_full, 1);
num_sources = size(fluences_matrix_full, 2);

% ignore rows corresponding to tetras with negligible fluence from all sources
nonzero_rows = zeros(size(fluences_matrix_full,1),1);
num_nonzero_rows = 0;
num_low_dose_tumor_tetras = 0; % number of tumor tetras that get ignored

% tissue_type of num_tissues + 1 means it was tagged as a source (which is always inside a tumor): 
% this only matters for visualization purposes, and we should undo it if it has occurred
if ~all(tissue_types_full <= num_tissues)
    for i=1:size(fluences_matrix_full,1)
        if tissue_types_full(i) > num_tissues
            tissue_types_full(i) = tumor_idx;
        end
    end
end

for i=1:size(fluences_matrix_full,1)
    % keep all tumor tetras, reject all tetras with no dose threshold (e.g. CSF), otherwise keep if 
    % it's possible to overdose it given the max total power limit
    if tissue_types_full(i) == tumor_idx
        num_nonzero_rows = num_nonzero_rows + 1;
        nonzero_rows(num_nonzero_rows) = i;

        % check if it passes the threshold without guard bands (shouldn't terminate run because of high guard band)
        if max(fluences_matrix_full(i,:)) < hard_thresholds(tumor_idx)
            num_low_dose_tumor_tetras = num_low_dose_tumor_tetras + 1;
        end
    elseif tissue_types_full(i) ~= 0
        if thresholds(tissue_types_full(i)) > 0.0 && ...
                    (max(fluences_matrix_full(i,:)) > thresholds(tissue_types_full(i)))
            num_nonzero_rows = num_nonzero_rows + 1;
            nonzero_rows(num_nonzero_rows) = i;
            if sum(fluences_matrix_full(i,:).^2) < 1e-10
                %fprintf('Warning: Including a small row at i=%d\n', i);
            end
        end
    else
        fprintf('Ignoring tetra with tissue type of 0 at index %d\n', i);
    end
end

% this indicates the energy limit is way too low, since each tumor tetra should be relatively near to some source
if num_low_dose_tumor_tetras > 0
    fprintf('WARNING: %d tumor tetras with too low fluence to ever pass the threshold with this dose!\n', ...
        num_low_dose_tumor_tetras);
end

fprintf('Thresholding...\n');
fluences_matrix = fluences_matrix_full(nonzero_rows(1:num_nonzero_rows), :);
tissue_types = tissue_types_full(nonzero_rows(1:num_nonzero_rows));
volumes = volumes_full(nonzero_rows(1:num_nonzero_rows));

if ~all(volumes >= 0)
    display('ERROR: Some tetrahedron volumes are negative!');
    return;
end
num_tetras = size(fluences_matrix, 1); % NOTE: not the full number of tetras; this is the trimmed-down number
total_volume = sum(volumes);
tumor_volume = 0;
num_tumor_tetras = 0;
for i=1:num_tetras
    if tissue_types(i) == tumor_idx
        tumor_volume = tumor_volume + volumes(i);
        num_tumor_tetras = num_tumor_tetras + 1;
    end
end
% TODO: No longer using tumor volume weighting ratio
tumor_volume_ratio = (total_volume - tumor_volume) / tumor_volume;

% scale tumor slope by tumor volume ratio and weight
%slopes(tumor_idx) = slopes(tumor_idx) * tumor_volume_ratio * tumor_weight;

slopes(tumor_idx) = slopes(tumor_idx) * tumor_weight;

% verify that the correct number of tissue types are present
if min(tissue_types) < 0
    display('ERROR: A negative tissue type is present in the tissue_types vector!');
    return;
end

fprintf('num_tetras = %d, num_tetras_full = %d, num_sources = %d\n\n', num_tetras, num_tetras_full, num_sources);
fprintf('total_volume  = %f, tumor_volume = %f, ratio = %f\n', total_volume, tumor_volume, tumor_volume_ratio);
for i=1:num_tissues
    if i==tumor_idx
        fprintf('slopes(%d) = %f <-- Tumor\n', i, slopes(i));
        fprintf('thresholds(%d) = %f <-- Tumor\n', i, thresholds(i));
        fprintf('hard_thresholds(%d) = %f <-- Tumor\n', i, hard_thresholds(i));
        fprintf('tumor_overdose_threshold = %f\n', tumor_overdose_threshold);
        fprintf('tumor_overdose_slope = %f\n', tumor_overdose_slope);
    else
        fprintf('slopes(%d) = %f\n', i, slopes(i));
        fprintf('thresholds(%d) = %f\n', i, thresholds(i));
        fprintf('hard_thresholds(%d) = %f\n', i, hard_thresholds(i));
    end
end