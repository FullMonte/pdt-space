/** 
 * \file mesh.cxx \brief Mesh Implementation
 *
 * This source files implements the different methods for the mesh class in order to be able to read
 * the mesh data from the files
 */

#include "read_mesh.h"
#include "file_parser.h"

// mesh Class

//! Default Constructor 
mesh::mesh()
{
    _number_of_points = -1;
    _number_of_tetrahedra = -1;
    _number_of_materials = -1;

    _index_of_refraction = -1;
}

/**
 * Regular Constructor 
 * @param mesh_file [in]: Path to the mesh file
 * @param fparser [in]: Pointer to the file parser to read the optical properties
 * @param read_from_vtk [in]: Selects whether read from .mesh file or .vtk file
 */
mesh::mesh(string mesh_file, Parser* fparser, bool read_from_vtk)
{
    // Reading data 
    read_opt_props(fparser);
    if (read_from_vtk) {
        read_vtk(mesh_file);
    }
    else { 
        read_mesh(mesh_file); //opt_file_ss.str());
    }
}

//! compute the volume of a teterahedron
double mesh::compute_tet_volume(tetrahedron* t)
{
    SP_Point *p0, *p1, *p2, *p3;

    p0 = _list_of_points[t->get_p1_id() - 1];
    p1 = _list_of_points[t->get_p2_id() - 1];
    p2 = _list_of_points[t->get_p3_id() - 1];
    p3 = _list_of_points[t->get_p4_id() - 1];
        

    // volume = (1/6) * (AB x AC) dot AD
    double ab[3] = { p1->get_xcoord() - p0->get_xcoord(),
                     p1->get_ycoord() - p0->get_ycoord(),
                     p1->get_zcoord() - p0->get_zcoord() };
    double ac[3] = { p2->get_xcoord() - p0->get_xcoord(),
                     p2->get_ycoord() - p0->get_ycoord(),
                     p2->get_zcoord() - p0->get_zcoord() };
    double ad[3] = { p3->get_xcoord() - p0->get_xcoord(),
                     p3->get_ycoord() - p0->get_ycoord(),
                     p3->get_zcoord() - p0->get_zcoord() };
    double volume = abs((1.0 / 6.0) * (ad[0] * (ab[1] * ac[2] - ab[2] * ac[1]) + 
                                       ad[1] * (ab[2] * ac[0] - ab[0] * ac[2]) + 
                                       ad[2] * (ab[0] * ac[1] - ab[1] * ac[0])));

    return volume;
}

//! Destructor
mesh::~mesh()
{
    for (unsigned int i = 0; i < _list_of_points.size(); i++)
        delete _list_of_points[i];

    for (unsigned int i = 0; i < _list_of_tetrahedra.size(); i++)
        delete _list_of_tetrahedra[i];

    for (unsigned int i = 0; i < _list_of_materials.size(); i++)
        delete _list_of_materials[i];
}

//  Getters

//! This method returns a list of pointers to all points in the mesh
vector<SP_Point*> & mesh::get_list_of_points()
{
    return _list_of_points;
}

//! This method returns a list of pointers to all tetrahedra in the mesh
vector<tetrahedron*> & mesh::get_list_of_tetrahedra()
{
    return _list_of_tetrahedra;
}

//! This method returns a list of pointers to all materials in the mesh
vector<material_opt*> & mesh::get_list_of_materials()
{
    return _list_of_materials;
}

//! This method returns the number of points in the mesh
unsigned int mesh::get_number_of_points()
{
    return _number_of_points;
}

//! This method returns the mesh size in terms of number of tetrahedra in the mesh
unsigned int mesh::get_mesh_size()
{
    return _number_of_tetrahedra;
}

//! This method returns the number of materials in the mesh
unsigned int mesh::get_number_of_materials()
{
    return _number_of_materials;
}

//! This method returns the refraction index in the current mesh
double mesh::get_index_of_refraction()
{
    return _index_of_refraction;
}

//! Setters

//!
//! This method adds a point to the list of points
//! @param p [in]: pointer to the point to add
//!
void mesh::add_point(SP_Point *p)
{
    _list_of_points.push_back(p);
}

//!
//! This method adds a tetrahedron to the list of tetrahedra
//! @param t [in]: pointer to the tetrahedron to add
//!
void mesh::add_tetrahedron(tetrahedron *t)
{
    _list_of_tetrahedra.push_back(t);
}

//!
//! This method adds a new material to the list of materials
//! @param m [in]: pointer to the new material to add
//!
void mesh::add_material(material_opt *m)
{
    _list_of_materials.push_back(m);
}

//!
//! This method sets the number of points in the mesh
//! @param np [in]: number of points
//! 
void mesh::set_number_of_points(unsigned int np)
{
    _number_of_points = np;
}

//!
//! This method sets the number of tetrahedra in the mesh
//! @param ms [in]: mesh size in terms of number of tetrahedra
//!
void mesh::set_mesh_size(unsigned int ms)
{
    _number_of_tetrahedra = ms;
}

//!
//! This method sets the number of materials in the mesh
//! @param nm [in]: number of materials
//!
void mesh::set_number_of_materials(unsigned int nm)
{
    _number_of_materials = nm;
}

//! Helper methods

//!
//! This method converts the optical properties read by file parser to pdt-space optical properties
//! @param fparser [in]: Pointer to the file parser
//!
void mesh::read_opt_props(Parser* fparser)
{
    // Reading the optical properties
    fprintf(stderr, "\033[1;%dmSetting optical properties....\033[0m\n", 33);

    const vector<Parser_material> materials = fparser->get_materials();

    if (materials.size() == 0)
    {
        fprintf(stderr, "\033[1;%dmNo materials read.\nExiting...\033[0m\n", 31);
        exit(-1);
    }

    _number_of_materials = (unsigned) (materials.size()) - 1; // exterior does not count

    // Reading the optical properties for each material
    material_opt * tmp_material;
    for (unsigned int i = 1; i <= _number_of_materials; i++)
    {
        tmp_material = new material_opt(materials[i].id, materials[i].mu_a, 
                materials[i].mu_s, materials[i].g, materials[i].n, this);
        add_material(tmp_material);
    }

    // PDT-SPACE assumes exterior to be air
    _index_of_refraction = materials[0].n;
}

//!
//! This method reads the data files and constructs the mesh
//! @param mesh_file [in]: file name of the mesh data (points and tetrahedra)
//! @param opt_file [in]: Metrials' properties data (optical properties)
//!
void mesh::read_mesh(string mesh_file)
{
    ifstream input;
    char str[255] = "";

    // Reading mesh data
    fprintf(stderr, "\033[1;%dmReading mesh file....\033[0m\n", 33);
    input.open(mesh_file);
    if (!input)
    {
        fprintf(stderr, "\033[1;%dmCould not open %s\nExiting...\033[0m\n", 31, mesh_file.c_str());
        exit(-1);
    }

    for (int i = 0; i < 2; i++)
    {
        input.getline(str, 255);
        stringstream line(str);
        
        if (i == 0)
            line >> _number_of_points;
        else 
            line >> _number_of_tetrahedra;
    }

    SP_Point * tmp_p;
    float xcoord, ycoord, zcoord;
    for (unsigned int i = 0; i < _number_of_points; i++)
    {
        input.getline(str, 255);
        stringstream line(str);

        line >> xcoord >> ycoord >> zcoord;

        tmp_p = new SP_Point(i+1, xcoord, ycoord, zcoord, this);
        add_point(tmp_p);
    }

    tetrahedron* tmp_tet;
    int id1, id2, id3, id4, mat_id;
    for(unsigned int i = 0; i < _number_of_tetrahedra; i++)
    {
        input.getline(str, 255);
        stringstream line(str);

        line >> id1 >> id2 >> id3 >> id4 >> mat_id;

        tmp_tet = new tetrahedron(i+1, id1, id2, id3, id4, mat_id, this);
        add_tetrahedron(tmp_tet);
    }

    fprintf(stderr, "\033[1;%dmDone Reading files....\033[0m\n", 33);
}

//!
//! This method reads the vtk file and constructs the mesh
//! @param vtk_file [in]: file name of the mesh data (points and tetrahedra)
//!
void mesh::read_vtk(string vtk_file)
{
    // Reading from vtk file
    fprintf(stderr, "\033[1;%dmReading vtk file....\033[0m\n", 33);

    vtkUnstructuredGridReader* reader = vtkUnstructuredGridReader::New();
    reader->vtkDataReader::SetFileName(vtk_file.c_str());
    reader->vtkDataReader::Update();

    vtkUnstructuredGrid * vtk_input = reader->GetOutput();

    _number_of_tetrahedra = (unsigned int) vtk_input->GetNumberOfCells();
    _number_of_points     = (unsigned int) vtk_input->GetNumberOfPoints();  // vtkPoints

    // Converting VTK points into PDT-SPACE points
    SP_Point * tmp_p;
    double p_coord[3];
    for (unsigned int i = 0; i < _number_of_points; i++) {
        vtk_input->GetPoint(i, p_coord);

        tmp_p = new SP_Point(i+1, (float)(p_coord[0]), (float)(p_coord[1]), (float)(p_coord[2]), this);
        add_point(tmp_p);
    }

    // Converting VTK cells into PDT-SPACE tetrahedra
    tetrahedron * tmp_tet;
    vtkIdList* pt_ids = vtkIdList::New();

    // set region
    vtkCellData* cd = vtk_input->GetCellData();
    vtkDataArray* regions = nullptr;
    bool has_region_data = false;

    if (cd && (regions = cd->GetScalars("Region"))) {
        has_region_data = true;
    } else if (cd && (regions = cd->GetScalars())) {
		const char *name = regions->GetName();
        fprintf(stderr, "\033[1;%dmRegion label not found, using data label %s instead.\033[0m\n", 33, name);
        has_region_data = true;
    }
    int id[4];
    for (unsigned int i = 0; i < _number_of_tetrahedra; i++) {
        vtk_input->GetCellPoints(i, pt_ids);
        for (unsigned int j = 0; j < 4; j++) {
            id[j] = (int) pt_ids->GetId(j);
        }

        int region = 0;
        if (has_region_data) {
            region = (int) regions->GetTuple1(i);
        }

        tmp_tet = new tetrahedron(i+1, id[0]+1, id[1]+1, id[2]+1, id[3]+1, region, this);
        add_tetrahedron(tmp_tet);
    }

    fprintf(stderr, "\033[1;%dmDone Reading files....\033[0m\n", 33);

}

void mesh::print_vector_matlab(FILE* matlab, 
                               vector<double> &v, 
                               string v_name)
{
    fprintf(matlab, "%s = [", v_name.c_str());
    for (unsigned int i = 0; i < v.size() - 1; i++)
    {
        fprintf(matlab, "%-15g;", v[i]);
        if ((i + 1) % 10 == 0)
            fprintf(matlab, "\n");
    }
    fprintf(matlab, "%-15g", v[v.size()-1]);
    fprintf(matlab, "];\n\n");
}

void mesh::print_vector_matlab(FILE* matlab, 
                               vector<int> &v, 
                               string v_name)
{
    fprintf(matlab, "%s = [", v_name.c_str());
    for (unsigned int i = 0; i < v.size() - 1; i++)
    {
        fprintf(matlab, "%-15d;", v[i]);
        if ((i + 1) % 10 == 0)
            fprintf(matlab, "\n");
    }
    fprintf(matlab, "%-15d", v[v.size()-1]);
    fprintf(matlab, "];\n\n");
}
