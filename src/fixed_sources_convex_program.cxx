#include "fixed_sources_convex_program.h"

#include <cassert>
#include <iostream>
#include <cmath>
#include <fstream>

#include "placer_defs.h"
// #include "util.h"

using namespace mosek::fusion;
using namespace monty;

//! Default constructor
fixed_sources_cp::fixed_sources_cp()
{
    _mesh_size = 0;
    _num_sources = 0;
}

//! Regular Constructor
fixed_sources_cp::fixed_sources_cp(unsigned int mesh_size, unsigned int num_sources,
               unsigned int num_tumor_elements,
               const vector<double> & d_max, const vector<double> & d_min,
               const vector<double> & p_max, 
               const vector<vector<float>>& source_element_fluence_matrix,
               const vector<double>& tetra_weights,
               const vector<bool>& tetra_to_consider)
{
    assert(num_sources == p_max.size());
    assert(mesh_size == d_max.size());
    assert(num_sources == source_element_fluence_matrix.size());
    assert(num_tumor_elements != 0);

    // Initializing optimizer private members
    _mesh_size = mesh_size;
    _num_sources = num_sources;
    _num_tumor_elements = num_tumor_elements;
    
    _d_max.resize(d_max.size());
    _d_min.resize(d_min.size());

    for (unsigned int i = 0; i < d_max.size(); i++)
    {
        _d_max[i] = d_max[i];
        _d_min[i] = d_min[i];
    }

    _p_max.resize(_num_sources);
    for (unsigned int i = 0; i < _num_sources; i++)
        _p_max[i] = p_max[i];

    _sources_elements_fluence_matrix.resize(_num_sources);
    for (unsigned int i = 0; i < _num_sources; i++)
    {
        _sources_elements_fluence_matrix[i].resize(_mesh_size);

        // checking if there is a dummy variable or not
        unsigned int index_to_consider = 0;
        if (source_element_fluence_matrix[i].size() == tetra_to_consider.size() + 1) // TODO: This is useless with tetra_to_consider
        {
            for (unsigned int j = 1; j < source_element_fluence_matrix[i].size(); j++)
            {
                if (tetra_to_consider[j])
                {
                    _sources_elements_fluence_matrix[i][index_to_consider-1] = source_element_fluence_matrix[i][j];
                    index_to_consider++;
                }
            }
        }
        else
        {
            for (unsigned int j = 0; j < source_element_fluence_matrix[i].size(); j++)
            {
                if (tetra_to_consider[j])
                   _sources_elements_fluence_matrix[i][index_to_consider++] = source_element_fluence_matrix[i][j];
            }
        }

        if (index_to_consider != mesh_size)
        {
            cout << source_element_fluence_matrix[i].size() << " " <<  index_to_consider << " " << mesh_size << endl;
            cerr << "fixed_sources_cp::ERROR!!! number of tetra not matching" << endl;
            exit(-1);
        }
    }

    _tetra_weights.resize(tetra_weights.size());
    for (unsigned int i = 0; i < tetra_weights.size(); i++)
    {
        _tetra_weights[i] = tetra_weights[i];
    }
}

//! destructor
fixed_sources_cp::~fixed_sources_cp()
{
    _lp_model->dispose(); 
    _lp_x->destroy(); 
    _lp_t->destroy(); 
}


/** This method multiples the matrix of the fluence with the current source power 
 * To find the current fluence at each element (Note that the matrix is transposed)
 * current_fluence_out [out] stores the result
 */
void fixed_sources_cp::compute_current_fluence(const vector<double>& current_source_powers,
                                               vector<double>& current_fluence_out)
{
    current_fluence_out.clear();
    current_fluence_out.resize(_mesh_size);


    for (unsigned int i = 0; i < _mesh_size; i++)
    {
        current_fluence_out[i] = 0; 
        for (unsigned int j = 0; j < _num_sources; j++)
        {
            current_fluence_out[i] += _sources_elements_fluence_matrix[j][i]*current_source_powers[j];
        }
    }
}

/**
 * This method performs gradient descent on the given initial solution in order to manually 
 * minimize the cost
 * Currently, the derivative is based on the linear formulation of the optimization problem we 
 * defined
 */
vector<double> fixed_sources_cp::gradient_descent(double learning_rate, const vector<double> init_solution)
{
    fprintf(stderr, "\033[1;%dmRunning Gradient Descent!\033[0m\n", 33);

    if (init_solution.size() != _num_sources)
    {
        cerr << "fixed_source_cp::gradient_descent::Size of initial solution does not equal number of sources! Exiting" << endl;
        exit(-1);
    }

    vector<double> curr_solution = init_solution;

    vector<double> next_solution(curr_solution.size());

    vector<double> error(curr_solution.size(), 0.0);
    
    vector<double> current_fluence;


    // TODO: Make the learning rate depend on the previous result
    vector<double> current_grad_f(curr_solution.size()), 
                    prev_grad_f(curr_solution.size());
    

    int iter_index = 0;
    srand(time(NULL));
    while (true)//iter_index <= 100)//true)
    {
        iter_index++;

        cout << "Learning rate at iteration " << iter_index << " is: " << learning_rate << endl;

        // Computing the current fluence with the current solution
        compute_current_fluence(curr_solution, current_fluence);

        // Looping over all elements
        for (unsigned int i = 0; i < curr_solution.size(); i++)
        {
            // computing the gradient at element i
            double grad_f = 0.0;
            for (unsigned int j = 0; j < _mesh_size; j++)
            {
                if (fabs(_d_min[j]) < 1e-8) // means that this is an organ at risk
                {
                    if (current_fluence[j] - _d_max[j] > DOUBLE_ACCURACY) // means that it violated
                        grad_f += _sources_elements_fluence_matrix[i][j];
                
                    // non-linear formulation
//                     grad_f += _sources_elements_fluence_matrix[i][j]/_d_max[j];
                    
                }
                else
                {
                    if (_d_min[j] - current_fluence[j] > DOUBLE_ACCURACY) // means that it violated
                        grad_f += -1*_sources_elements_fluence_matrix[i][j];
                
                    // non-linear formulation
//                     grad_f += -1*(_sources_elements_fluence_matrix[i][j]*_d_min[j])/(current_fluence[j] + 1e-8);
                }
            }

            current_grad_f[i] = grad_f;
            
            next_solution[i] = curr_solution[i] - learning_rate*grad_f; 

            if (next_solution[i] < 0 || next_solution[i] > _p_max[i])
                next_solution[i] = _p_max[i]*((double)rand()/RAND_MAX);
        }

        // computing the error
        for (unsigned int i = 0; i < curr_solution.size(); i++)
            error[i] = abs(next_solution[i] - curr_solution[i]);
        
        double norm_err = 0; //compute_p_norm_of(error, 1);
        
        for (unsigned int i = 0; i < error.size(); i++)
            norm_err = max(norm_err, error[i]);

        cout << "Error at iteration " << iter_index << " is: " << norm_err << endl;

        if (norm_err <= 1e-6) // convergence
            break;

        if (iter_index != 1)
        {
            vector<double> sol_diff(curr_solution.size()),
                    grad_diff(curr_solution.size());

            for (unsigned int m = 0; m < sol_diff.size(); m++)
            {
                sol_diff[m] = next_solution[m] - curr_solution[m];

//                 cout << curr_solution[m] << " " << next_solution[m] << " " <<  sol_diff[m] << endl;

                grad_diff[m] = current_grad_f[m] - prev_grad_f[m];
//                 cout << prev_grad_f[m] << " " << current_grad_f[m] << " " <<  grad_diff[m] << endl << endl;
            }

            double dp = dot_product(sol_diff, grad_diff);
            double grad_norm = pow(compute_p_norm_of(grad_diff, 2), 2);

            //cout << "dot product: " << dp << "          grad_norm   " << grad_norm << endl;

            learning_rate = abs(dp)/grad_norm;
//             exit(-1);
        }
        
        // updating solution
        for (unsigned int i = 0; i < curr_solution.size(); i++)
        {
            curr_solution[i] = next_solution[i];
            prev_grad_f[i] = current_grad_f[i];
        }
    }

    cout << "fixed_source_cp::gradient_descent::Number of iterations for convergence is: " << iter_index << endl;


   // printing results to a file

   ofstream out_grad; 
   out_grad.open("gradient_descent_results.txt");

   for (unsigned int i  = 0; i < next_solution.size(); i++)
       out_grad << "x[" << i << "] = " << next_solution[i] << endl;
    out_grad.close(); 

    return next_solution;
}

//! This method initializes the basic LP model and defines its constraints
bool fixed_sources_cp::define_mosek_constraints()
{
    _scaling_factor = 1e6;

    // Defining the objective parameters
    cout << "Setting the objective parameters" << endl;
    vector<double> weights(_mesh_size), d(_mesh_size), fw(_num_sources);

    for (unsigned int i = 0; i < _d_min.size(); i++)
    {
        if (_d_min[i] < 1e-8) // organ at risk
        {
            weights[i] = _tetra_weights[i]*_scaling_factor;
            d[i] = _d_max[i];
        }
        else // tumor element
        {
            weights[i] = -1*_tetra_weights[i]*_scaling_factor;
            d[i] = _d_min[i];

            _tumor_indices.push_back(i);
        }
    }

    cout << "Defining the optimization problem" << endl;
    // Defining the optimization model
    _lp_model = new Model("Mosek Linear Optimization");
    //auto _M = finally([&]() { _lp_model->dispose(); });
    
    _lp_model->setSolverParam("optimizer", "intpnt");//"dualSimplex");//intpnt"); // INTPNT
    _lp_model->setSolverParam("log", 1);
    _lp_model->setSolverParam("numThreads", 0);//24);
    _lp_model->setSolverParam("intpntTolPath", 0.5); 
//  _lp_model->setSolverParam("intpntBasis", "never");
//   _lp_model->setSolverParam("intpntTolPsafe", 100);

    //_lp_model->setLogHandler([=](const std::string & msg) { cout << msg << endl; } );

    // Defining the optimization variables 
    _lp_x = _lp_model->variable("source_powers", _num_sources, Domain::inRange(0.0, _p_max[0]));
    _lp_t = _lp_model->variable("dummy_var", _mesh_size, Domain::greaterThan(0.0));

    // Defining the constraints
    cout << "Defining the constraints" << endl;
       
    try {
            //auto wd = new_array_ptr<double, 1>(_mesh_size);
            double wd = 0.0;
            for (unsigned int i = 0; i < _mesh_size; i++)
            {   
                auto fluence_mat_i = new_array_ptr<double, 1>(_num_sources);
                for (unsigned int j = 0; j < _num_sources; j++)
                {
                    (*fluence_mat_i)[j] = weights[i]*_sources_elements_fluence_matrix[j][i];
                }

                //(*wd)[i] = weights[i]*d[i];
                wd = weights[i]*d[i];

                if (std::abs(weights[i]*d[i]) > 100*_scaling_factor)//1e20) 
                {
                    std::cerr << "run_mosek_optimization::very large value in constraints" << std::endl;
                    std::cerr << weights[i]*d[i] << std::endl;
                    _lp_model->dispose(); 
                    return false; 
                }

                auto const_expr = Expr::sub(Expr::sub(Expr::dot(fluence_mat_i, _lp_x),
                        wd), _lp_t->index(i));//), _scaling_factor); 
                try {
                    auto curr_const = _lp_model->constraint(const_expr, Domain::lessThan(0.0));
                } catch (std::bad_alloc) {
                    std::cerr << "run_mosek_optimization::bad alloc during defining constraint" << std::endl;
                    _lp_model->dispose();
                    return false; 
                } catch (const mosek::MosekException & e) {
                    std::cerr << "run_mosek_optimization::Error during defining constraint" << std::endl;
                    _lp_model->dispose(); 
                    return false; 
                }

            }
    } catch (std::bad_alloc) {
            std::cerr << "run_mosek_optimization::bad alloc during optimization " << _mesh_size << std::endl;
            _lp_model->dispose();
            return false; 
       } 
    // constraint for total power limit
    _lp_model->constraint("total_p_limit", Expr::sum(_lp_x), Domain::lessThan(1.0));

    // Adding binary constraint to minimize the number of sources 
//     Variable::t beta = model->variable("interger_variable", _num_sources, Domain::integral(Domain::inRange(0, 1)));
//     model->constraint("beta_c", Expr::sub(Expr::mul(1.0/5, x), beta), Domain::lessThan(0.0));
//     model->constraint("sum_beta", Expr::sum(beta), Domain::lessThan(84));


    return true;
}
    
//! This method defines the objective of the basic LP formulation 
bool fixed_sources_cp::define_mosek_objective()
{
    // L1-regularization
//     model->objective("obj", ObjectiveSense::Minimize, Expr::add(Expr::mul(-1000, Expr::sum(x)), Expr::sum(t)));

    // No regularization

    std::cout << "Defining the objective" << std::endl;
    try{
        _lp_model->objective("obj", ObjectiveSense::Minimize, Expr::sum(_lp_t));
    }
    catch (const mosek::MosekException & e) {
        std::cerr << "run_mosek_optimization::Error during setting objective" << std::endl;

        cout << e.what() << endl;

        _lp_model->dispose(); 

        return false; 
    }

    return true;
}

//! This method updates the constraints of the tumor elements when the tumor weight changes
bool fixed_sources_cp::update_tumor_mosek_constraints(double curr_tumor_weight)
{
    // loop over all tumor indices 
    for (auto tumor_idx : _tumor_indices)
    {
        auto fluence_mat_i = new_array_ptr<double, 1>(_num_sources);
        
        double weight = -1*_tetra_weights[tumor_idx]*_scaling_factor;
        for (unsigned int j = 0; j < _num_sources; j++)
        {
            (*fluence_mat_i)[j] = weight*_sources_elements_fluence_matrix[j][tumor_idx];
        }

        double wd = weight*_d_min[tumor_idx];

        if (std::abs(wd) > 100*_scaling_factor)//1e20) 
        {
            std::cerr << "update_mosek_constraint::very large value in constraints" << std::endl;
            std::cerr << wd << std::endl;
            return false; 
        }

        auto const_expr = Expr::sub(Expr::sub(Expr::dot(fluence_mat_i, _lp_x),
                wd), _lp_t->index(tumor_idx));//), _scaling_factor); 

        auto curr_const = _lp_model->getConstraint(tumor_idx); 
        try {
            curr_const->update(const_expr);
        } catch (std::bad_alloc) {
            std::cerr << "update_mosek_constraint::bad alloc during defining constraint" << std::endl;
            return false; 
        } catch (const mosek::MosekException & e) {
            std::cerr << "update_mosek_constraint::Error during defining constraint" << std::endl;
            std::cerr << e.what() << std::endl;
            return false; 
        }
    }
    return true;
}

/** 
 * This method performs the optimization using the Mosek Interface, 
 * and returns the final source powers after choosing the sources
 * It also returns the run-time of the optimizer, the number of non-zero power sourcrs
 * and the total source power
 */
vector<double> fixed_sources_cp::run_mosek_optimization(double & optimizer_time, 
                                                        unsigned int & num_sources_non_zero, 
                                                        double & total_source_power)
{
    fprintf(stderr, "\033[1;%dmRunning Mosek Optimization!\033[0m\n", 33);
    /*
     * The problem is formulated in the form:
     *          min weights^T *(Fx - d) = min weights^T*F x - weight^T*d
     */
          
    // Solving
    try {
        cout << "Solving for objective" << endl;
        _lp_model->solve();
        cout << "Optimization finished" << endl;
    } catch (const OptimizeError& e) {
        std::cerr << "run_mosek_optimization::mosek optimize error: " << e.what() << std::endl;
        _lp_model->dispose(); 

        return vector<double>(); 
    }

    optimizer_time = _lp_model->getSolverDoubleInfo("optimizerTime");
    cout << "Optimizer Time: " << optimizer_time << " sec" << endl; 

    auto sol = new_array_ptr<double, 1> (_num_sources); 
    // Extracting the solution
    try {
        sol = _lp_x->level(); 

        // Output the cost solution
           cout << std::fixed << std::setprecision(8) << "The solution is: " << _lp_model->primalObjValue()/_scaling_factor << endl; 
        _final_cost = _lp_model->primalObjValue()/_scaling_factor;
    } catch (const SolutionError & e) {
        std::cerr << "run_mosek_optimization::mosek solution error optimization " << std::endl;
        
        auto prosta = _lp_model->getProblemStatus(SolutionType::Interior);
        switch(prosta)
        {
            case ProblemStatus::DualInfeasible:
                std::cout << "Dual infeasibility certificate found.\n";
                break;

            case ProblemStatus::PrimalInfeasible:
                std::cout << "Primal infeasibility certificate found.\n";
                break;

            case ProblemStatus::Unknown:
            // The solutions status is unknown. The termination code
            // indicates why the optimizer terminated prematurely.
                std::cout << "The solution status is unknown.\n";
                char symname[MSK_MAX_STR_LEN];
                char desc[MSK_MAX_STR_LEN];
                MSK_getcodedesc((MSKrescodee)(_lp_model->getSolverIntInfo("optimizeResponse")), symname, desc);
                std::cout << "  Termination code: " << symname << " " << desc << "\n";
                break;

            default:
                std::cout << "Another unexpected problem status: " << prosta << "\n";            
        }
        return vector<double>(); 
    }
    
    // printing results to a file
    ofstream out_grad; 
    out_grad.open("mosek_opt_results.txt");

    for (unsigned int i  = 0; i < _num_sources; i++)
       out_grad << "x[" << i << "] = " << (*sol)[i] << endl;
    out_grad.close(); 

    // Computing fluence limits
    // computing total fluence at each element
    vector<double> total_fluence(_mesh_size, 0.0);
    
    vector<double> final_source_powers(_num_sources);
    num_sources_non_zero = 0;
    total_source_power = 0.0;
    for (unsigned int i = 0; i < _num_sources; i++)
    {
        total_source_power += (*sol)[i];

        if ((*sol)[i] > 0)
            num_sources_non_zero++;

        if ((*sol)[i] > SOURCE_POWER_MIN)
            final_source_powers[i] = (*sol)[i];
        else
            final_source_powers[i] = 0;
    }
    cout << endl << "Number of non-zero sources: " << num_sources_non_zero << endl;
    cout << "Source powers:\n";
    for (unsigned i = 0; i < _num_sources; i++) {
        cout << "Source [" << i << "]: " << std::fixed << std::setprecision(8) << final_source_powers[i] << endl;
    }
    cout << "Total source power achieved is: " << total_source_power << endl << endl;

    //compute_current_fluence(final_source_powers, total_fluence);
    
    //double maxf = _NEG_INFINITY_; 
    //double minf = _INFINITY_;
    //int num_oar_violating = 0; 
    //int num_tumor_violating = 0;
    //int num_oar = 0;
    //int num_tumor = 0;
    //double violation_accuracy = 1e-8; 
    //for (unsigned int i = 0; i < _mesh_size; i++)
    //{
        //if (_d_min[i] < DOUBLE_ACCURACY)
        //{
            //num_oar++;
            //if (total_fluence[i] - (0.9/*0.75*/)*_d_max[i] > violation_accuracy
                //&& _d_max[i] > DOUBLE_ACCURACY) // making sure that the threshold is not zero, otherwise, we do not care (CSF in Colin27)
                                        // 0.9/0.75 is to compare to V90
            //{
                //num_oar_violating++;
//                 cout << "OAR Violation: " << i << " " << total_fluence[i] << " " << _d_max[i] << endl;
            //}
            //maxf = max(maxf, total_fluence[i]);
        //}
        //else 
        //{
            //num_tumor++;
            //if (0.9*_d_min[i] - total_fluence[i]  > violation_accuracy) //< _d_min[i])
            //{
                //num_tumor_violating++;
//                 cout << "Tumor Violation: " << total_fluence[i] << " " << _d_min[i] << endl;
            //}
            //minf = min(minf, total_fluence[i]);
        //}
    //}
//     cout << "Max fluence on OAR = " << maxf << endl;
//     cout << "Min fluence on Tumor cells = " << minf << endl;

//     cout << "Num OAR: " << num_oar << "     Num Violating: " << num_oar_violating << " ===> " 
//             << ((double)num_oar_violating/num_oar)*100.0 << "%" << endl;
//     cout << "Num Tumor: " << num_tumor << "     Num Violating: " << num_tumor_violating << " ===> " 
//                 << ((double)num_tumor_violating/num_tumor)*100.0 << "%" << endl;

    return final_source_powers;
}

/**
 * This method performs the optimization using the Mosek Interface with tailored emission profiles, 
 *  and returns the final source powers after choosing the sources
 * It also returns the run-time of the optimizer, the number of non-zero power sourcrs
 * and the total source power
 */
vector<double> fixed_sources_cp::run_mosek_optimization_with_tailored_profiles(const vector<unsigned int> & fiber_elements,
                                                        double & optimizer_time, 
                                                        unsigned int & num_sources_non_zero, 
                                                        double & total_source_power)
{
    fprintf(stderr, "\033[1;%dmRunning Mosek Optimization with Tailored Emission Profiles!\033[0m\n", 33);
    /*
     * The problem is formulated in the form:
     *          min weights^T *(Fx - d) = min weights^T*F x - weight^T*d
     */


     double scaling_factor = 1;//e8;

    // Defining the objective parameters
    cout << "Setting the objective parameters" << endl;
    vector<double> weights(_mesh_size), d(_mesh_size), fw(_num_sources);
    //double wt_dot_d;

    for (unsigned int i = 0; i < _d_min.size(); i++)
    {
        if (_d_min[i] < 1e-8) // organ at rist
        {
            weights[i] = _tetra_weights[i]/scaling_factor;
            d[i] = _d_max[i];
        }
        else // tumor element
        {
            weights[i] = -1*_tetra_weights[i]/scaling_factor;
            d[i] = _d_min[i];
        }
    }

    cout << "Defining the optimization problem" << endl;
    // Defining the optimization model
    Model::t model = new Model("Mosek Linear Optimization");
    auto _M = finally([&]() { model->dispose(); });
    
//     model->setLogHandler([=](const std::string & msg) { cout << msg << endl; } );

    // Defining the optimization variables 
    Variable::t x = model->variable("source_powers", _num_sources, Domain::inRange(0.0, _p_max[0]));
    Variable::t t = model->variable("dummy_var", _mesh_size, Domain::greaterThan(0.0));

    // Defining the constraints
    cout << "Defining the constraints" << endl;
    
    auto wd = new_array_ptr<double, 1>(_mesh_size);
    for (unsigned int i = 0; i < _mesh_size; i++)
    {   
        auto fluence_mat_i = new_array_ptr<double, 1>(_num_sources);
        for (unsigned int j = 0; j < _num_sources; j++)
        {
            (*fluence_mat_i)[j] = weights[i]*_sources_elements_fluence_matrix[j][i];
        }

        (*wd)[i] = weights[i]*d[i];


        stringstream const_name;
        const_name << "c" << i;

        model->constraint(const_name.str(), Expr::sub(Expr::sub(Expr::dot(fluence_mat_i, x),
                        (*wd)[i]), t->index(i)), Domain::lessThan(0.0));
    }
    
    // constraint for total power limit
    model->constraint("total_p_limit", Expr::sum(x), Domain::lessThan(1.0));


    // Defining constraints on fiber elements (difference between neighboring elements' power emission should not exceed 10% of the max power emission per fiber) 
    unsigned int curr_src_num = 0; 
    unsigned int curr_num_elems = 0;
    vector<unsigned int> num_elems_in_sources; // this vector stores the number of elements in each source
    vector<unsigned int> src_start_idx(1,0); // the starting index of the source in the x vector
    vector<unsigned int> src_end_idx; // the ending index of the source in the x vector
    for (unsigned int i = 0; i < _num_sources; i++) // _num_sources in this case is the total number of elements
    {
        if (fiber_elements[i] == curr_src_num)
        {
            curr_num_elems++;
        }
        else
        {
            num_elems_in_sources.push_back(curr_num_elems);
            curr_num_elems = 1;
            curr_src_num++;

            src_end_idx.push_back(i-1); // current source

            src_start_idx.push_back(i);
        }
    } 
    src_end_idx.push_back(_num_sources - 1);
    num_elems_in_sources.push_back(curr_num_elems);

    if (src_start_idx.size() != num_elems_in_sources.size())
    {
        cerr << "tailored_constraints::vector sizes are not equal. Exiting!" << endl;
        exit(-1);
    }
    if (src_end_idx.size() != num_elems_in_sources.size())
    {
        cerr << "tailored_constraints::vector sizes are not equal. Exiting!" << endl;
        exit(-1);
    }

    unsigned int actual_num_srcs = num_elems_in_sources.size();
    for (unsigned int i = 0; i < actual_num_srcs; i++)
    {

//         cout << src_start_idx[i] << "       ====>       " << src_end_idx[i] << endl;
//         continue;
        // We assume a sinusoidal shape by forcing the middle element to be the maximum 
        // TODO: Another approach is to force a constance constraint

        if (num_elems_in_sources[i] == 1) // this is a point source
            continue;

        //unsigned int middle_idx = (src_end_idx[i] + src_start_idx[i])/2;
    
        // constraint on the first idx
        stringstream temp_const;
        temp_const << "tailored" << src_start_idx[i] << "_" << 1;
        //model->constraint(temp_const.str(), Expr::sub(x->index(src_start_idx[i]), x->index(middle_idx)),  Domain::lessThan(0.0));
        model->constraint(temp_const.str(), Expr::mul(1, x->index(src_start_idx[i])),  Domain::greaterThan(0.0));
        
        for (unsigned int j = src_start_idx[i] + 1; j <= src_end_idx[i]; j++)
        {
            stringstream const_name;
            const_name << "tailored" << j << "_" << 1;
            model->constraint(const_name.str(), Expr::sub(x->index(j), Expr::mul(1.1, x->index(j-1))), Domain::lessThan(0.0));
            
            //model->constraint(const_name.str(), Expr::sub(Expr::sub(x->index(j), x->index(j-1)), 
                                                        //0.1*x->index(middle_idx)),  Domain::lessThan(0.0));

//             stringstream const_name1;
//             const_name1 << "tailored" << j << "_" << 2;
//             model->constraint(const_name1.str(), Expr::mul(1, x->index(j)), Domain::greaterThan(1e-5));
            //model->constraint(const_name1.str(), Expr::sub(Expr::sub(x->index(j-1), x->index(j)), 
                                                        //0.1*x->index(middle_idx)),  Domain::lessThan(0.0));

            //stringstream const_name2;
            //const_name2 << "tailored" << j << "_" << 3;
            //model->constraint(const_name2.str(), Expr::sub(x->index(j), x->index(middle_idx)),  Domain::lessThan(0.0));
        }
    }
//     exit(-1);


    // Defining the objective
    model->setSolverParam("optimizer", "intpnt");//"dualSimplex");//intpnt"); // INTPNT
    model->setSolverParam("log", 5);
    model->setSolverParam("numThreads", 8);
    //model->setSolverParam("anaSolInfeasTol", 1e-8);
    model->setSolverParam("intpntCoTolRelGap", 1e-8);
    model->setSolverParam("intpntCoTolDfeas", 1e-11);
    model->setSolverParam("intpntCoTolPfeas", 1e-11);
    model->setSolverParam("intpntCoTolInfeas", 1e-11);
    model->setSolverParam("intpntCoTolMuRed", 1e-10);
    model->setSolverParam("intpntTolRelGap", 1e-8);
    model->setSolverParam("intpntTolDfeas", 1e-11);
    model->setSolverParam("intpntTolPfeas", 2e-11);
    model->setSolverParam("intpntTolInfeas", 1e-11);
    model->setSolverParam("intpntTolMuRed", 1e-10);
//   
//     model->setSolverParam("intpntTolPsafe", 100);
       
    // No regularization
    model->objective("obj", ObjectiveSense::Minimize, Expr::sum(t));

    // Solving
    cout << "Solving for objective" << endl;
    model->solve();
    cout << "Optimization finished" << endl;


    optimizer_time = model->getSolverDoubleInfo("optimizerTime");
    cout << "Optimizer Time: " << optimizer_time << " sec" << endl; 

    // Extracting the solution
    auto sol = x->level(); 

    // Output the cost solution
    cout << "The solution is: " << model->primalObjValue()*scaling_factor << endl; 
    _final_cost = model->primalObjValue()*scaling_factor;

    
    // printing results to a file
    ofstream out_grad; 
    out_grad.open("mosek_opt_results.txt");

    for (unsigned int i  = 0; i < _num_sources; i++)
       out_grad << "x[" << i << "] = " << (*sol)[i] << endl;
    out_grad.close(); 

    // Computing fluence limits
    // computing total fluence at each element
    vector<double> total_fluence(_mesh_size, 0.0);
    double maxf = _NEG_INFINITY_; 
    double minf = _INFINITY_;
    
    vector<double> final_source_powers(_num_sources);
    num_sources_non_zero = 0;
    total_source_power = 0.0;
    for (unsigned int i = 0; i < _num_sources; i++)
    {
        total_source_power += (*sol)[i];

        if ((*sol)[i] > 0)
            num_sources_non_zero++;

        if ((*sol)[i] > SOURCE_POWER_MIN)
            final_source_powers[i] = (*sol)[i];
        else
            final_source_powers[i] = 0;

          if (final_source_powers[i] < 0)
          {
              cerr << "ERRRRRRRRRRRRRRR: power less than 0: " << i << "       " << fiber_elements[i] << "       " << final_source_powers[i] << endl;
          }
    }
    cout << endl << "Number of non-zero sources: " << num_sources_non_zero << endl;
    cout << "Total source power achieved is: " << total_source_power << endl << endl;

    compute_current_fluence(final_source_powers, total_fluence);
    
    int num_oar_violating = 0; 
    int num_tumor_violating = 0;
    int num_oar = 0;
    int num_tumor = 0;
    double violation_accuracy = 1e-8; // TODO: 1e-8
    for (unsigned int i = 0; i < _mesh_size; i++)
    {
        if (_d_min[i] < DOUBLE_ACCURACY)
        {
            num_oar++;
            if (total_fluence[i] - (0.9/*0.75*/)*_d_max[i] > violation_accuracy
                && _d_max[i] > DOUBLE_ACCURACY) // making sure that the threshold is not zero, otherwise, we do not care (CSF in Colin27)
                                        // 0.9/0.75 is to compare to V90
            {
                num_oar_violating++;
            }
            maxf = max(maxf, total_fluence[i]);
        }
        else 
        {
            num_tumor++;
            if (0.9*_d_min[i] - total_fluence[i]  > violation_accuracy) //< _d_min[i])
            {
                num_tumor_violating++;
            }
            minf = min(minf, total_fluence[i]);
        }
    }
//     cout << "Max fluence on OAR = " << maxf << endl;
//     cout << "Min fluence on Tumor cells = " << minf << endl;

//     cout << "Num OAR: " << num_oar << "     Num Violating: " << num_oar_violating << " ===> " << ((double)num_oar_violating/num_oar)*100.0 << "%" << endl;
//     cout << "Num Tumor: " << num_tumor << "     Num Violating: " << num_tumor_violating << " ===> " << ((double)num_tumor_violating/num_tumor)*100.0 << "%" << endl;

    return final_source_powers;//total_fluence;
}

// ! This method is used to initialize some parameters used in the CVaR constrained optimization approach that takes into account OP variation
void fixed_sources_cp::initialize_cvar_parameters(double cvar_beta, const vector<unsigned int>& mat_ids)
{
    _beta_cvar = cvar_beta;

    _material_ids.resize(mat_ids.size());
    for (unsigned int i = 0; i < mat_ids.size(); i++)
    {
        _material_ids[i] = mat_ids[i];

        if (_materials_num_elements.find(mat_ids[i]) == _materials_num_elements.end())
            _materials_num_elements[mat_ids[i]] = 1;
        else
            _materials_num_elements[mat_ids[i]]++;
    }
}

/** 
 * This method performs the optimization with CVarR constraints for OP variation using the Mosek Interface, 
 * and returns the final source powers after choosing the sources
 * It also returns the run-time of the optimizer, the number of non-zero power sourcrs
 * and the total source power
 */
vector<double> fixed_sources_cp::run_mosek_optimization_with_cvar_constraints(double & optimizer_time, 
                                                        unsigned int & num_sources_non_zero, 
                                                        double & total_source_power, 
                                                        const unordered_set<unsigned>&  tumor_region_number,
                                                        unsigned int num_tissues, 
                                                        const vector<vector<float>> & expected_dose_matrix)
{
    fprintf(stderr, "\033[1;%dmRunning Mosek Optimization with CVaR Constraints!\033[0m\n", 33);
    /*
     * The problem is formulated in the form:
     *          min weights^T *(Fx - d) = min weights^T*F x - weight^T*d
     */

    double scaling_factor = 1;

    // Defining the objective parameters
    cout << "Setting the objective parameters" << endl;
    vector<double> weights(_mesh_size), d(_mesh_size), fw(_num_sources);
    unordered_map<unsigned int, double> materials_thresholds; // used in the CVaR constraints

    for (unsigned int i = 0; i < _d_min.size(); i++)
    {
        if (_material_ids[i] == 0) 
            continue;
        if (tumor_region_number.find(_material_ids[i]) == tumor_region_number.end())//_d_min[i] < 1e-8) // organ at rist
        {
            weights[i] = _tetra_weights[i]/scaling_factor;
            d[i] = _d_max[i];
            
            materials_thresholds[_material_ids[i]] =  8*d[i]; // TODO
        }
        else // tumor element
        {
            weights[i] = -1*_tetra_weights[i]/scaling_factor;
            d[i] = _d_min[i];

            materials_thresholds[_material_ids[i]] = 0.1*d[i]; // TODO
        }
    }

    cout << "Defining the optimization problem" << endl;
    // Defining the optimization model
    Model::t model = new Model("Mosek Linear Optimization");
    auto _M = finally([&]() { model->dispose(); });
    
    model->setLogHandler([=](const std::string & msg) { cout << msg << endl; } );

    // Defining the optimization variables 
    Variable::t x = model->variable("source_powers", _num_sources, Domain::inRange(0.0, _p_max[0]));
    Variable::t t = model->variable("dummy_var", _mesh_size, Domain::greaterThan(0.0));
    Variable::t dose = model->variable("element_dose", _mesh_size, Domain::greaterThan(0.0));
    Variable::t zeta = model->variable("intermediate_VaR_variable", num_tissues, Domain::greaterThan(0.0));

    // Defining the constraints
    cout << "Defining the constraints" << endl;
    
    auto wd = new_array_ptr<double, 1>(_mesh_size);
    for (unsigned int i = 0; i < _mesh_size; i++)
    {   
        auto fluence_mat_i = new_array_ptr<double, 1>(_num_sources);

        auto expected_fluence_mat_i = new_array_ptr<double, 1>(_num_sources);

        for (unsigned int j = 0; j < _num_sources; j++)
        {
            //(*fluence_mat)[i][j] = weights[i]*_sources_elements_fluence_matrix[j][i];
            (*fluence_mat_i)[j] = weights[i]*_sources_elements_fluence_matrix[j][i];  //expected_dose_matrix[j][i];//_sources_elements_fluence_matrix[j][i];
            (*expected_fluence_mat_i)[j] = _sources_elements_fluence_matrix[j][i];//expected_dose_matrix[j][i];//_sources_elements_fluence_matrix[j][i];
        }

        (*wd)[i] = weights[i]*d[i];


        stringstream const_name;
        const_name << "c" << i;

        model->constraint(const_name.str(), Expr::sub(Expr::sub(Expr::dot(fluence_mat_i, x),
                        (*wd)[i]), t->index(i)), Domain::lessThan(0.0));

        // constrainst to capture CVaR (average dose in the highest (1-beta)% of the tissue
        if (_material_ids[i] != 0)
        {
            stringstream cvar_const_i;
            cvar_const_i << "cvar" << i;

            if (tumor_region_number.find(_material_ids[i]) != tumor_region_number.end())
            {
                model->constraint(cvar_const_i.str(), Expr::sub(Expr::sub(zeta->index((*(tumor_region_number.begin()))-1), // TODO does not work with multiple tumor regions
                        Expr::dot(expected_fluence_mat_i, x)), dose->index(i)), Domain::lessThan(0.0));
            }
            else
            {
                model->constraint(cvar_const_i.str(), Expr::sub(Expr::sub(Expr::dot(expected_fluence_mat_i, x), 
                            zeta->index(_material_ids[i]-1)), dose->index(i)), Domain::lessThan(0.0));
            }
        }
    }

    // building the CVaR constraints
    auto cvar_thresholds = new_array_ptr<double, 1> (num_tissues);
    for (unsigned int i = 0; i < num_tissues; i++)
    {
        (*cvar_thresholds)[i] = materials_thresholds[i+1];
        stringstream const_name;
        const_name << "cc" << i;

        // defining the average scale to capture the average
        double tissue_average_scale = 1/((1-_beta_cvar)*_materials_num_elements[i+1]);

        cout << i+1 << " " << _materials_num_elements[i+1] << " " << tissue_average_scale << " " << materials_thresholds[i+1] << endl;

        auto avg_scale = new_array_ptr<double, 1>(_mesh_size);

        for (unsigned int j = 0; j < _mesh_size; j++)
        {
//             if (i+1 == tumor_region_number)
//                 (*avg_scale)[j] = ((i+1) == _material_ids[j]) ? 1/((1-0.99)*_materials_num_elements[i+1]) : 0.0;
//             else
                (*avg_scale)[j] = ((i+1) == _material_ids[j]) ? tissue_average_scale : 0.0;
        }

        if (tumor_region_number.find(i+1) != tumor_region_number.end())
        {
            model->constraint(const_name.str(), Expr::sub(Expr::sub(zeta->index(i), Expr::dot(avg_scale, dose)),
                                                (*cvar_thresholds)[i]), Domain::greaterThan(0.0));
        }
        else
        {
            model->constraint(const_name.str(), Expr::sub(Expr::add(zeta->index(i), Expr::dot(avg_scale, dose)),
                                                (*cvar_thresholds)[i]), Domain::lessThan(0.0));
        }
    }
    
    // constraint for total power limit
    model->constraint("total_p_limit", Expr::sum(x), Domain::lessThan(1.0));

    // Defining the objective
    model->setSolverParam("optimizer", "dualSimplex");//intpnt"); // INTPNT
    model->setSolverParam("log", 100);
    model->setSolverParam("numThreads", 0);
//     model->setSolverParam("intpntBasis", "never");
    
    try {
        model->setSolverParam("logInfeasAna", 100);
//         model->setSolverParam("infeasReportLevel", 10);
    } catch (mosek::fusion::ParameterError &e) {
        cout << "Error: " << e.toString() << endl;
        exit(-1);
    }
//     model->setSolverParam("intpntTolPsafe", 100);
       

    // No regularization
    model->objective("obj", ObjectiveSense::Minimize, Expr::sum(t));

    // Solving
    cout << "Solving for objective" << endl;
    try {
        model->solve();
    } catch (mosek::fusion::SolutionError& e) {
        std::cout << "CVAR approach mosek solution error:  " << e.toString() << endl;
        exit(-1);
    }
    
    cout << "Optimization finished" << endl;


    optimizer_time = model->getSolverDoubleInfo("optimizerTime");
    cout << "Optimizer Time: " << optimizer_time << " sec" << endl; 

    // Extracting the solution
    auto sol = x->level(); 
    auto zeta_sol = zeta->level();

    cout << "Zeta solution is: " << endl;
    for (unsigned int i = 0; i < num_tissues; i++)
        cout << "zeta[" << i << "] = " << (*zeta_sol)[i] << endl;

    // Output the cost solution
    cout << "The solution is: " << model->primalObjValue()*scaling_factor << endl; 
    _final_cost = model->primalObjValue()*scaling_factor;

    
    // printing results to a file
    ofstream out_grad, out_cvar; 
    out_grad.open("mosek_opt_results.txt");
    out_cvar.open("mosek_opt_cvar_results.txt");

    for (unsigned int i  = 0; i < _num_sources; i++)
       out_grad << "x[" << i << "] = " << (*sol)[i] << endl;
    out_grad.close(); 


    auto dose_sol = dose->level();
    cout << endl << "printing cvar solution" << endl;
    for (unsigned int i = 0; i < _mesh_size; i++)
        out_cvar << "material[" << i << "] = "  << _material_ids[i] << " d[" << i << "] = " << (*dose_sol)[i] << endl;
    out_cvar.close();

    vector<double> sum_expected_dose(num_tissues, 0);
    for (unsigned int i = 0; i < _mesh_size; i++)
    {
        if (_material_ids[i] != 0)
            sum_expected_dose[_material_ids[i]-1] += (*dose_sol)[i];
    }
    for (unsigned int i = 0; i < num_tissues; i++)
        cout << "Tissue " << i << ": " << sum_expected_dose[i]*(1/((1-_beta_cvar)*_materials_num_elements[i+1])) << endl;

    // Computing fluence limits
    // computing total fluence at each element
    vector<double> total_fluence(_mesh_size, 0.0);
    double maxf = _NEG_INFINITY_; 
    double minf = _INFINITY_;
    
    vector<double> final_source_powers(_num_sources);
    num_sources_non_zero = 0;
    total_source_power = 0.0;
    for (unsigned int i = 0; i < _num_sources; i++)
    {
        total_source_power += (*sol)[i];

        if ((*sol)[i] > 0)
            num_sources_non_zero++;

        if ((*sol)[i] > SOURCE_POWER_MIN)
            final_source_powers[i] = (*sol)[i];
        else
            final_source_powers[i] = 0;
    }
    cout << endl << "Number of non-zero sources: " << num_sources_non_zero << endl;
    cout << "Total source power achieved is: " << total_source_power << endl << endl;

    compute_current_fluence(final_source_powers, total_fluence);
    
    int num_oar_violating = 0; 
    int num_tumor_violating = 0;
    int num_oar = 0;
    int num_tumor = 0;
    double violation_accuracy = 1e-8;
    for (unsigned int i = 0; i < _mesh_size; i++)
    {
        if (_d_min[i] < DOUBLE_ACCURACY)
        {
            num_oar++;
            if (total_fluence[i] - (0.9/*0.75*/)*_d_max[i] > violation_accuracy
                && _d_max[i] > DOUBLE_ACCURACY) // making sure that the threshold is not zero, otherwise, we do not care (CSF in Colin27)
            {
                num_oar_violating++;
            }
            maxf = max(maxf, total_fluence[i]);
        }
        else 
        {
            num_tumor++;
            if (0.9*_d_min[i] - total_fluence[i]  > violation_accuracy) //< _d_min[i])
            {
                num_tumor_violating++;
            }
            minf = min(minf, total_fluence[i]);
        }
    }

    return final_source_powers;//total_fluence;
}

/**
 * This method performs the optimization with variance(risk) constraints for OP variation using the Mosek Interface, 
 * and returns the final source powers after choosing the sources
 * It also returns the run-time of the optimizer, the number of non-zero power sourcrs
 * and the total source power
 */
vector<double> fixed_sources_cp::run_mosek_optimization_with_variance_constraints(double & optimizer_time, 
                                                        unsigned int & num_sources_non_zero, 
                                                        double & total_source_power, 
                                                        const unordered_set<unsigned>& tumor_region_number,
                                                        unsigned int num_tissues, 
                                                        const vector<vector<double>> & dose_variance_matrix)
{
    fprintf(stderr, "\033[1;%dmRunning Mosek Optimization with variance Constraints!\033[0m\n", 33);
    /*
     * The problem is formulated in the form:
     *          min weights^T *(Fx - d) = min weights^T*F x - weight^T*d
     */

    double scaling_factor = 1;

    // Defining the objective parameters
    cout << "Setting the objective parameters" << endl;
    vector<double> weights(_mesh_size), d(_mesh_size), fw(_num_sources);

    for (unsigned int i = 0; i < _d_min.size(); i++)
    {
        if (_material_ids[i] == 0) 
            continue;
        if (tumor_region_number.find(_material_ids[i]) == tumor_region_number.end())//_d_min[i] < 1e-8) // organ at rist
        {
            weights[i] = _tetra_weights[i]/scaling_factor;
            d[i] = _d_max[i];
        }
        else // tumor element
        {
            weights[i] = -1*_tetra_weights[i]/scaling_factor;
            d[i] = _d_min[i];
        }
    }

    // Setting variance thresholds TODO: Automate these
    auto var_thresholds = new_array_ptr<double, 1>(num_tissues);
    (*var_thresholds)[0] = 10000000;
    (*var_thresholds)[1] = 10000000;
    (*var_thresholds)[2] = 0.01;
    (*var_thresholds)[3] = 0.01;
    (*var_thresholds)[4] = 0.1;//10000000;


    cout << "Defining the optimization problem" << endl;
    // Defining the optimization model
    Model::t model = new Model("Mosek Linear Optimization");
    auto _M = finally([&]() { model->dispose(); });
    
    model->setLogHandler([=](const std::string & msg) { cout << msg << endl; } );

    // Defining the optimization variables 
    Variable::t x = model->variable("source_powers", _num_sources, Domain::inRange(0.0, _p_max[0]));
    Variable::t t = model->variable("dummy_var", _mesh_size, Domain::greaterThan(0.0));

    // Defining the constraints
    cout << "Defining the constraints" << endl;
    
    auto wd = new_array_ptr<double, 1>(_mesh_size);
    for (unsigned int i = 0; i < _mesh_size; i++)
    {   
        auto fluence_mat_i = new_array_ptr<double, 1>(_num_sources);


        for (unsigned int j = 0; j < _num_sources; j++)
        {
            //(*fluence_mat)[i][j] = weights[i]*_sources_elements_fluence_matrix[j][i];
            (*fluence_mat_i)[j] = weights[i]*_sources_elements_fluence_matrix[j][i];  //expected_dose_matrix[j][i];//_sources_elements_fluence_matrix[j][i];
        }

        (*wd)[i] = weights[i]*d[i];


        stringstream const_name;
        const_name << "c" << i;

        model->constraint(const_name.str(), Expr::sub(Expr::sub(Expr::dot(fluence_mat_i, x),
                        (*wd)[i]), t->index(i)), Domain::lessThan(0.0));

        // Building variance constraints for white are grey matters
        if (_material_ids[i] != 0i)// && _material_ids[i] != tumor_region_number)   ///TODO Check what is 0i (maybe a typo)
        {
            // The constraint is of the form x^T*Sigma_i*x <= gamma, where Sigma_i is the covariance matrix of the dose at each element i
            // Since the sources are independent, the covariance matrix is a diagonal matrix of the variance from each source, 
            // This matrix can be written as G_i*G_i, where G_i is the diagonal matrix of the standard deviation
            // Hence the constraint can be written as norm(G*x) <= gamma
            
            
            vector<double> std_dev_vec(_num_sources*_num_sources, 0.0);
            for (unsigned j = 0; j < _num_sources; j++)
                std_dev_vec[j*_num_sources + j] = sqrt(dose_variance_matrix[j][i]);

            auto std_dev_matrix  = new ndarray<double, 2>(shape(_num_sources, _num_sources), std_dev_vec.begin(), std_dev_vec.end());

            stringstream cone_name;
            cone_name << "cone" << i;

            model->constraint(cone_name.str(), Expr::vstack((*var_thresholds)[_material_ids[i]-1], Expr::mul((shared_ptr<ndarray<double, 2>>)std_dev_matrix, x)), Domain::inQCone());
        }
    }

    
    // constraint for total power limit
    model->constraint("total_p_limit", Expr::sum(x), Domain::lessThan(1.0));

    // Defining the objective
//     model->setSolverParam("optimizer", "intpnt");//"dualSimplex");//intpnt"); // INTPNT
    model->setSolverParam("log", 100);
    model->setSolverParam("logOptimizer", 100);
    model->setSolverParam("numThreads", 0);
//     model->setSolverParam("intpntBasis", "never");
    
    try {
        model->setSolverParam("logInfeasAna", 100);
    } catch (mosek::fusion::ParameterError &e) {
        cout << "Error: " << e.toString() << endl;
        exit(-1);
    }
       

    // No regularization
    model->objective("obj", ObjectiveSense::Minimize, Expr::sum(t));

    // Solving
    cout << "Solving for objective" << endl;
    try {
        model->solve();
    } catch (mosek::fusion::SolutionError& e) {
        std::cout << "Variance Constraints approach mosek solution error:  " << e.toString() << endl;
        exit(-1);
    }
    
    cout << "Optimization finished" << endl;


    optimizer_time = model->getSolverDoubleInfo("optimizerTime");
    cout << "Optimizer Time: " << optimizer_time << " sec" << endl; 

    // Extracting the solution
    auto sol = x->level(); 

    // Output the cost solution
    cout << "The solution is: " << model->primalObjValue()*scaling_factor << endl; 
    _final_cost = model->primalObjValue()*scaling_factor;

    
    // printing results to a file
    ofstream out_grad, out_cvar; 
    out_grad.open("mosek_opt_results.txt");

    for (unsigned int i  = 0; i < _num_sources; i++)
       out_grad << "x[" << i << "] = " << (*sol)[i] << endl;
    out_grad.close(); 

    // finding maximum standard deviation for each tissue healthy tissue
    vector<double> max_variance(5, 0);
    for (unsigned int i = 0; i < _mesh_size; i++)
    {
        if (_material_ids[i] != 0)// && _material_ids[i] != tumor_region_number)
        {
            double tetra_variance = 0;
            for (unsigned int j = 0; j < _num_sources; j++)
            {
                tetra_variance += dose_variance_matrix[j][i]*((*sol)[j])*((*sol)[j]);
            }
            max_variance[_material_ids[i] - 1] = max(max_variance[_material_ids[i]-1], tetra_variance);
        }
    }

    // Printing the max variance
    cout << "The maximum variance per tissue is: " << endl;
    for (unsigned int j = 0; j < max_variance.size(); j++)
        cout << "Tissue " << j + 1 << ": " << max_variance[j] << endl; 

    vector<double> final_source_powers(_num_sources);
    num_sources_non_zero = 0;
    total_source_power = 0.0;
    for (unsigned int i = 0; i < _num_sources; i++)
    {
        total_source_power += (*sol)[i];

        if ((*sol)[i] > 0)
            num_sources_non_zero++;

        if ((*sol)[i] > SOURCE_POWER_MIN)
            final_source_powers[i] = (*sol)[i];
        else
            final_source_powers[i] = 0;
    }
    cout << endl << "Number of non-zero sources: " << num_sources_non_zero << endl;
    cout << "Total source power achieved is: " << total_source_power << endl << endl;

    return final_source_powers;//total_fluence;
}

/** 
 * This method performs the optimization using the Mosek Interface with power variation, 
 * and returns the final source powers after choosing the sources
 * It also returns the run-time of the optimizer, the number of non-zero power sourcrs
 * and the total source power
 * @param [in] sigma: uncertainty containing 95% of possible data
 * @param [in] weight: weight factor adjustable for power variation optimization
 */
vector<double> fixed_sources_cp::run_mosek_optimization_with_power_variation(double & optimizer_time, 
                                                        unsigned int & num_sources_non_zero, 
                                                        double & total_source_power,
                                                        double & sigma, double & weight)
{

    /*
     * The problem is formulated in the form:
     *          min weights^T *(F(x + 1*(2*sigma)) - d) = min weights^T*F x - (weight^T*d - weight^T*(2*sigma))
     */

    _scaling_factor = 1e6;

    // Defining the objective parameters
    cout << "Setting the objective parameters" << endl;
    vector<double> weights(_mesh_size), d(_mesh_size), fw(_num_sources);

    for (unsigned int i = 0; i < _d_min.size(); i++)
    {
        if (_d_min[i] < 1e-8) // organ at risk
        {
            weights[i] = _tetra_weights[i]*_scaling_factor;
            d[i] = _d_max[i];
        }
        else // tumor element
        {
            weights[i] = -1*_tetra_weights[i]*_scaling_factor;
            d[i] = _d_min[i];

            _tumor_indices.push_back(i);
        }
    }

    cout << "Defining the optimization problem" << endl;
    // Defining the optimization model
    _lp_model = new Model("Mosek Linear Optimization");
    //auto _M = finally([&]() { _lp_model->dispose(); });
    
    _lp_model->setSolverParam("optimizer", "intpnt");//"dualSimplex");//intpnt"); // INTPNT
    _lp_model->setSolverParam("log", 1);
    _lp_model->setSolverParam("numThreads", 0);//24);
    _lp_model->setSolverParam("intpntTolPath", 0.5); 
//  _lp_model->setSolverParam("intpntBasis", "never");
//   _lp_model->setSolverParam("intpntTolPsafe", 100);

    //_lp_model->setLogHandler([=](const std::string & msg) { cout << msg << endl; } );

    // Defining the optimization variables 
    _lp_x = _lp_model->variable("source_powers", _num_sources, Domain::inRange(0.0, _p_max[0]));
    _lp_t = _lp_model->variable("dummy_var", _mesh_size*2, Domain::greaterThan(0.0));

    // Defining the constraints
    cout << "Defining the constraints" << endl;
       
    try {
            //auto wd = new_array_ptr<double, 1>(_mesh_size);
            double wd = 0.0;
                double sig = 2*weight*sigma;
            for (unsigned int i = 0; i < _mesh_size*2; i++)
            {   
                auto fluence_mat_i = new_array_ptr<double, 1>(_num_sources);
                if (i >= _mesh_size) sig = -sig;
                for (unsigned int j = 0; j < _num_sources; j++)
                {
                    (*fluence_mat_i)[j] = weights[i%_mesh_size]*_sources_elements_fluence_matrix[j][i%_mesh_size]*(1+sig);
                }

                //(*wd)[i] = weights[i]*d[i];
                wd = weights[i%_mesh_size]*d[i%_mesh_size];

                if (std::abs(weights[i%_mesh_size]*d[i%_mesh_size]) > 100*_scaling_factor)//1e20) 
                {
                    std::cerr << "run_mosek_optimization_with_power_variation::very large value in constraints (wd)" << std::endl;
                    std::cerr << weights[i%_mesh_size]*d[i%_mesh_size] << std::endl;
                    _lp_model->dispose(); 
                    return vector<double>(); 
                }
                //if (std::abs(g_i) > 100*_scaling_factor)//1e20) 
                //{
                //    std::cerr << "run_mosek_optimization_with_power_variation::very large value in constraints (g_i)" << std::endl;
                    //std::cerr << g_i << std::endl;
                //    _lp_model->dispose(); 
                //    return vector<double>(); 
                    //}

                auto const_expr = Expr::sub(Expr::sub(Expr::dot(fluence_mat_i, _lp_x),
                        wd), _lp_t->index(i));//), _scaling_factor); 
                try {
                    auto curr_const = _lp_model->constraint(const_expr, Domain::lessThan(0.0));
                } catch (std::bad_alloc) {
                    std::cerr << "run_mosek_optimization_with_power_variation::bad alloc during defining constraint" << std::endl;
                    _lp_model->dispose();
                    return vector<double>(); 
                } catch (const mosek::MosekException & e) {
                    std::cerr << "run_mosek_optimization_with_power_variation::Error during defining constraint" << std::endl;
                    _lp_model->dispose(); 
                    return vector<double>(); 
                }

            }
    } catch (std::bad_alloc) {
            std::cerr << "run_mosek_optimization_with_power_variation::bad alloc during optimization " << _mesh_size << std::endl;
            _lp_model->dispose();
            return vector<double>(); 
       } 
    // constraint for total power limit
    _lp_model->constraint("total_p_limit", Expr::sum(_lp_x), Domain::lessThan(1.0));

    std::cout << "Defining the objective" << std::endl;
    try{
        _lp_model->objective("obj", ObjectiveSense::Minimize, Expr::sum(_lp_t));
    }
    catch (const mosek::MosekException & e) {
        std::cerr << "run_mosek_optimization_with_power_variation::Error during setting objective" << std::endl;

        cout << e.what() << endl;

        _lp_model->dispose(); 

        return vector<double>(); 
    }

    fprintf(stderr, "\033[1;%dmRunning Mosek Optimization!\033[0m\n", 33);
          
    // Solving
    try {
        cout << "Solving for objective" << endl;
        _lp_model->solve();
        cout << "Optimization finished" << endl;
    } catch (const OptimizeError& /*e*/) {
        std::cerr << "run_mosek_optimization_with_power_variation::mosek optimize error" << std::endl;
        _lp_model->dispose(); 

        return vector<double>(); 
    }

    optimizer_time = _lp_model->getSolverDoubleInfo("optimizerTime");
    cout << "Optimizer Time: " << optimizer_time << " sec" << endl; 

    auto sol = new_array_ptr<double, 1> (_num_sources); 
    // Extracting the solution
    try {
        sol = _lp_x->level(); 

        // Output the cost solution
           cout << std::fixed << std::setprecision(8) << "The solution is: " << _lp_model->primalObjValue()/_scaling_factor << endl; 
        _final_cost = _lp_model->primalObjValue()/_scaling_factor;
    } catch (const SolutionError & e) {
        std::cerr << "run_mosek_optimization_with_power_variation::mosek solution error optimization " << std::endl;
        
        auto prosta = _lp_model->getProblemStatus(SolutionType::Interior);
        switch(prosta)
        {
            case ProblemStatus::DualInfeasible:
                std::cout << "Dual infeasibility certificate found.\n";
                break;

            case ProblemStatus::PrimalInfeasible:
                std::cout << "Primal infeasibility certificate found.\n";
                break;

            case ProblemStatus::Unknown:
            // The solutions status is unknown. The termination code
            // indicates why the optimizer terminated prematurely.
                std::cout << "The solution status is unknown.\n";
                char symname[MSK_MAX_STR_LEN];
                char desc[MSK_MAX_STR_LEN];
                MSK_getcodedesc((MSKrescodee)(_lp_model->getSolverIntInfo("optimizeResponse")), symname, desc);
                std::cout << "  Termination code: " << symname << " " << desc << "\n";
                break;

            default:
                std::cout << "Another unexpected problem status: " << prosta << "\n";            
        }
        return vector<double>(); 
    }
    
    // printing results to a file
    ofstream out_grad; 
    out_grad.open("mosek_opt_results.txt");

    for (unsigned int i  = 0; i < _num_sources; i++)
       out_grad << "x[" << i << "] = " << (*sol)[i] << endl;
    out_grad.close(); 

    // Computing fluence limits
    // computing total fluence at each element
    vector<double> total_fluence(_mesh_size, 0.0);
    
    vector<double> final_source_powers(_num_sources);
    num_sources_non_zero = 0;
    total_source_power = 0.0;
    for (unsigned int i = 0; i < _num_sources; i++)
    {
        total_source_power += (*sol)[i];

        if ((*sol)[i] > 0)
            num_sources_non_zero++;

        if ((*sol)[i] > SOURCE_POWER_MIN)
            final_source_powers[i] = (*sol)[i];
        else
            final_source_powers[i] = 0;
    }
    cout << endl << "Number of non-zero sources: " << num_sources_non_zero << endl;
    cout << "Total source power achieved is: " << total_source_power << endl << endl;

    return final_source_powers;
}

//! This method computes the p-norm of a vector
double fixed_sources_cp::compute_p_norm_of(const vector<double> & v, int p)
{
    double sum = 0; 
    for (unsigned int i = 0; i < v.size(); i++)
    {
        sum += pow(abs(v[i]), p);
    }

    return pow(sum, 1.0/p);
}

//! This method computes the dot product of two vectors
double fixed_sources_cp::dot_product(const vector<double>& v1, const vector<double>& v2)
{
    double sum = 0; 
    for (unsigned int i = 0; i < v1.size(); i++)
        sum += v1[i]*v2[i];

    return sum;
}

//! This method returns the cost after optimization
double fixed_sources_cp::get_cost()
{
    return _final_cost;
}

//! This method updates the tetra weights
void fixed_sources_cp::set_tetra_weights(const vector<double>& tet_weights)
{
    _tetra_weights = tet_weights;
}

/*
// ! This method is to print results to a matlab file
void fixed_sources_cp::print_results_matlab(string file_name, 
                                vector<double> results, string vector_name)
{
    fprintf(stderr, "\033[1;%dmWriting results to a matlab file.\033[0m\n", 33);

    FILE* matlab = fopen(("Matlab_tests/" + file_name).c_str(), "w");
    Util::print_vector_matlab<double>(matlab, results, vector_name);
    fclose(matlab);

    fprintf(stderr, "\033[1;%dmDone Writing.\033[0m\n", 36);
}
*/
