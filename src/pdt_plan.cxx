/** 
 * \file pdt_plan.cxx \brief PDT Plan Implementation
 *
 * This source files implements the different methods for the pdt_plan class
 *
 *  @author: Abdul-Amir Yassine
 *  @date: March 6th, 2018
 */

#include "pdt_plan.h"
#include "read_mesh.h"
#include "src_placer.h"
#include "fixed_sources_convex_program.h"
#include "run_fullmonte.h"
#include "rt_feedback.h"
#include "sa_engine.h"
#include "file_parser.h"
#include "light_source.h"

//! Default Constructor
pdt_plan::pdt_plan()
{
    _placer = nullptr;
    _data = nullptr;
    _my_lp = nullptr;

    _rt_feedback_system = nullptr;
    
    _wavelength = "";
    _source_type = "";
    _placement_type = "";
    _tumor_weight = 0; 

    _source_tailored = false;

    _num_packets = 0;
    _total_energy = 0;

    _tumor_region_number.clear();
    _mesh_size = 0;
    _num_sources = 0;

    _tumor_volume = 0;
    _total_mesh_volume = 0;
    _max_region_index = 0;

    _ignore_unnecessary_tetra = false;
    _actual_mesh_size = 0; 

    _params_file = "";
    _read_data_from_vtk = false;

    _guardband_thresholds_called = false;
    _is_p_max_set = false;
    _is_unnecessary_removed = false;
    _is_tetra_data_called = false; 
    _is_lp_solved = false;

    _num_non_zero_sources = 0;
    _total_power = 0;

    _vary_tumor_weight = false;

    _beta_cvar = 0.0;
    _op_cvar_approach = false;
    _op_variance_constraints = false;

    _fullmonte_engine = nullptr;

    _sa_engine_progress_placement_file = "";
    _sa_engine_beta = 0.02; // Default value 
    _sa_engine_lambda = 0.3; // Default value
    _sa_engine_moves_per_temp = 0.0;
    _run_sa = false;
    _sa_engine_early_terminate = false;

    _final_distribution_output_file = "";

    _run_rt_feedback = false; 

    _final_distribution_output_file = "";

    _run_rt_feedback = false; 

    _target_tumor_v100 = 0.0;
    _upper_tumor_v100_bound = 0.0;
    _lower_tumor_v100_bound = 0.0; 

    _max_num_lp_iterations = std::numeric_limits<int>::max(); 

    _lp_constraints_defined = false;
    _lp_objective_defined = false; 
    
    _rt_feedback_rand_seed = 1;
    _rt_feedback_detector_radius = 0.5f; 
    _rt_feedback_detector_na = 0.5f; 
    _rt_feedback_lut_size = 50;
    _rt_feedback_lut_file_name = "";

    _power_variance_optimization = false;
    _sigma_power = 0.0;
}

//!
//! Regular Constructor
//! @param params_file: [in] the path to the parameters file
//!
pdt_plan::pdt_plan(string params_file)
{
    _sa_engine_beta = 0.02; // default value in case it is not supplied
    _sa_engine_lambda = 0.3; // default value in case it is not supplied
    _sa_engine_moves_per_temp = 0.0;
    _run_sa = false;

    _run_rt_feedback = false; 

    _tumor_volume = 0;
    _total_mesh_volume = 0;
    _max_region_index = 0;

    _placer = new src_placer();
    _my_lp = nullptr;

    _rt_feedback_system = nullptr;

    _source_tailored = false;
    _sa_engine_early_terminate = false;

    // Reading the parameters file
    _params_file = params_file;

    string file_type = _params_file.substr(_params_file.size() - 3);
    if (file_type == "xml") {
        // read xml file
        _fparser = new Parser (_params_file);
    } else {
        fprintf(stderr, "\033[1;%dmUnsupported file type: %s. Exiting...\033[0m\n", 31, file_type);
        exit(-1);
    }

    read_params_file();

    // in read_params_file
    _placer->set_num_packets(_num_packets);
    _placer->set_total_energy(_total_energy);

    _fparser->get_injection_point_options(_constrain_injection_point/*, _placement_mode*/);
    _num_injection_points = _fparser->get_num_injection_points();

    // Append project directory path to files specified in input parameters for testing
    if (_file_path_override) {
        override_file_paths();
    }

    // Read the mesh 
    read_mesh(); 
    
    // Read tissue properties to set _d_max and _d_min vectors
    read_tissue_properties();

    // Read the fluence matrix from FullMonte output vtk
    if (_running_tests && !_run_sa) {
        read_fullmonte_output_matrix(); 
    }

    _fullmonte_engine = new WrapperFullMonteSW(_mesh_file, _fparser, _read_data_from_vtk, _use_cuda); 

    // ignoring unnecessary tetra
    _ignore_unnecessary_tetra = true;
    _actual_mesh_size = _mesh_size;
    _tetra_to_consider.resize(_mesh_size);
    fill(_tetra_to_consider.begin(), _tetra_to_consider.end(), true);

    _guardband_thresholds_called = false;
    _is_p_max_set = false;
    _is_unnecessary_removed = false;
    _is_tetra_data_called = false; 
    _is_lp_solved = false;

    _num_non_zero_sources = 0;
    _total_power = 0;

    _lower_tumor_v100_bound = _target_tumor_v100 - 1e-1; // Default value
    _upper_tumor_v100_bound = _target_tumor_v100 + 5e-2; // Default value

    _max_num_lp_iterations = std::numeric_limits<int>::max(); 
    _num_lp_iterations = 0; 
    
    _beta_cvar = 0.0;
    _op_cvar_approach = false;

    _op_variance_constraints = false;
    
    _lp_constraints_defined = false;
    _lp_objective_defined = false; 
    
    _power_variance_optimization = false;
    _sigma_power = 0.0;
}

//! Destructor
pdt_plan::~pdt_plan()
{
    if (_placer) 
        delete _placer;
    if (_data)
        delete _data;
    if (_my_lp)
        delete _my_lp;
    if (_fullmonte_engine)
        delete _fullmonte_engine;
    if (_rt_feedback_system)
        delete _rt_feedback_system; 
    if (_fparser)
        delete _fparser;

    for (unsigned int i = 0; i < _placers.size(); i++)
        if (_placers[i])
            delete _placers[i];
    _placers.clear();
}

// General methods
//! This method reads the mesh and stores it inside the _data pointer
void pdt_plan::read_mesh()
{
    fprintf(stderr, "\n\033[1;%dmReading Mesh %s.\033[0m\n", 35, _mesh_file.c_str());

    _data = new mesh(_mesh_file, _fparser, _read_data_from_vtk);

    _fparser->get_tumor_regions(_tumor_region_number);

    cout << "Number of Different Materials is: " << _data->get_number_of_materials() << endl;

    cout << "Number of points is: " << _data->get_number_of_points() << endl;

    cout << "Number of Tetrahedra is: " << _data->get_mesh_size() << endl;

    // Finding max and min coords
    float max_x = _NEG_INFINITY_;
    float max_y = _NEG_INFINITY_;
    float max_z = _NEG_INFINITY_;
    float min_x = _INFINITY_;
    float min_y = _INFINITY_;
    float min_z = _INFINITY_;
    for (unsigned int i = 0; i < _data->get_list_of_points().size(); i++)
    {
        min_x = min(min_x, _data->get_list_of_points()[i]->get_xcoord());
        min_y = min(min_y, _data->get_list_of_points()[i]->get_ycoord());
        min_z = min(min_z, _data->get_list_of_points()[i]->get_zcoord());
        max_x = max(max_x, _data->get_list_of_points()[i]->get_xcoord());
        max_y = max(max_y, _data->get_list_of_points()[i]->get_ycoord());
        max_z = max(max_z, _data->get_list_of_points()[i]->get_zcoord());
    }
    cout << "max_x: " << max_x << " min_x: " << min_x << endl;
    cout << "max_y: " << max_y << " min_y: " << min_y << endl;
    cout << "max_z: " << max_z << " min_z: " << min_z << endl;


    /**************************************************************************************/
    
    _mesh_size = _data->get_mesh_size();
  
    //cout << "Tumor Region index is: " << _tumor_region_number << endl;
}

//! This method reads the tissue properties file to set d_max and d_min vectors
void pdt_plan::read_tissue_properties()
{
    fprintf(stderr, "\n\033[1;%dmReading Tissue Properties.\033[0m\n", 33);

    _fparser->get_tissue_properties(_d_min_tissues, _d_max_tissues, _guardband_tissues, _tissue_names_ids);

    double scale_factor_packets = _num_packets/_total_energy;
    for (unsigned int i = 0; i < _data->get_number_of_materials(); i++)
    {
        if (_d_max_tissues[i] != _INFINITY_)
            _d_max_tissues[i] *= scale_factor_packets;
        _d_min_tissues[i] *= scale_factor_packets;
        cout << i << " dmax " << _d_max_tissues[i] << " dmin " << _d_min_tissues[i] << " guardband " << _guardband_tissues[i] << endl;
    } 

}

//! This method reads the Fluence matrix G from FullMonte output VTK file
void pdt_plan::read_fullmonte_output_matrix()
{
    profiler fullmonte_read("Reading Fluence Matrix");

    stringstream tcl_path;
    tcl_path << _fullmonte_output_file; 

    string vtk_file_path = tcl_path.str();

    Util::BEGIN_TIME = chrono::steady_clock::now();
    _placer->read_vtk_file(vtk_file_path, _mesh_size);
    Util::END_TIME = chrono::steady_clock::now();

    double vtk_read_time = (double)chrono::duration_cast<chrono::seconds>
                    (Util::END_TIME - Util::BEGIN_TIME).count(); 

    cout << "Time to read VTK file is: " << vtk_read_time << " secs" << endl;
    
    _num_sources = (unsigned int)_placer->get_source_element_fluence_matrix().size();
    _final_source_powers.resize(_num_sources, 0.0);

    cout << "Number of candidate sources is: " << _num_sources << endl;
}

//! Helper method: generates the initial solution
void pdt_plan::read_initial_placement()
{
    // read file
    _fparser->get_sources(_initial_placement);

    for (unsigned i = 0; i < _placement_results.size(); i++) {
        for (unsigned j = 0; j < _placement_results[i].size(); j++) {
            if (_placement_results[i][j]) delete _placement_results[i][j];
        }
    }
    _placement_results.clear();
    _placement_results.push_back(_initial_placement);
}

//! This method returns the thresholds of the tissues
vector<double> pdt_plan::get_thresholds() {
    vector<double> thresholds(_d_max_tissues.size());

    for (unsigned i = 0; i < thresholds.size(); i++) {
        if (_tumor_region_number.find(i+1) == _tumor_region_number.end()) {
            thresholds[i] = _d_max_tissues[i];
        } else {
            thresholds[i] = _d_min_tissues[i];
        }
    }

    return thresholds;
}

//! This method returns the thresholds of the tissues multiplied by the specified guardband
vector<double> pdt_plan::get_thresholds_with_guardband()
{
    vector<double> thresholds(_d_max_tissues.size());

    _slopes.resize(_data->get_number_of_materials()); // this is used in weighting the tetras 
                                // slopes are inversely proportional to the thresholds

    double first_threshold = -1; // used in setting the slopes

    for (unsigned i = 0; i < thresholds.size(); i++) {
        if (_tumor_region_number.find(i+1) == _tumor_region_number.end()) {
            thresholds[i] = _guardband_tissues[i]*_d_max_tissues[i];
        } else {
            thresholds[i] = _guardband_tissues[i]*_d_min_tissues[i];
        }
        if (i == 0) {
            first_threshold = thresholds[i];
            _slopes[i] = 1.0;
        } else {
            _slopes[i] = (first_threshold)/thresholds[i];
        }
        _slopes[i] *= _num_packets/_total_energy;
    }

    _guardband_thresholds_called = true;
    
    return thresholds;
}

//! This method removes the unnecessary tetra that do not affect the optimizations based on the thresholds given
void pdt_plan::remove_unnecessary_tetra(const vector<double> & guardbanded_thresholds)
{
    _d_max.clear(); 
    _d_min.clear(); 
    _tetra_volumes.clear(); 
    _tetra_weights.clear(); 
    _tet_material_ids.clear(); 
    _tumor_indices.clear(); 
    _tumor_indices.resize(0); 

    if (_ignore_unnecessary_tetra)
        _placer->get_ignored_tetras(_tumor_region_number, _data, guardbanded_thresholds, _actual_mesh_size,
             _tetra_to_consider, _final_source_powers);
             
    ofstream fout ("/sims/pdt-space/build/tetras_blurred.csv");
    for (unsigned i = 0; i < _tetra_to_consider.size(); i++) {
        fout << (_tetra_to_consider[i] ? 1 : 0) << endl;
    }
    fout.close();
    
    cout << "Number of Tetra to consider: " << _actual_mesh_size << endl;

    _d_max.resize(_actual_mesh_size);
    _d_min.resize(_actual_mesh_size);
    _tetra_volumes.resize(_actual_mesh_size);
    _tetra_weights.resize(_actual_mesh_size);
    _tet_material_ids.resize(_actual_mesh_size);

    _is_unnecessary_removed = true;

    if (_op_variance_constraints)
        compute_tetra_dose_variance();
}

//!
//! This method computes the data of all tetra. It updates the following vectors: _original_tet_material_ids, 
//! _original_tetra_volumes, _original_tumor_indices, _d_max_orig, _d_min_orig
//!
void pdt_plan::compute_original_tetra_data(const vector<double> & thresholds)
{

    tetrahedron *t;
    int tumor_count = 0; 
    _total_mesh_volume = 0.0; _tumor_volume = 0.0;
    unordered_map<unsigned int, unsigned int> mat_ids; // stores the material ids

    // Initialize the size of all important vectors 
    _original_tetra_volumes.clear();
    _original_tetra_volumes.resize(_mesh_size);

    _original_tet_material_ids.clear();
    _original_tet_material_ids.resize(_mesh_size);
    
    _d_max_orig.clear(); _d_min_orig.clear(); 
    _d_max_orig.resize(_mesh_size);
    _d_min_orig.resize(_mesh_size);

    // Initialize coordinates to compute center of tumor
    float max_x = _NEG_INFINITY_, max_y = _NEG_INFINITY_, max_z = _NEG_INFINITY_, 
            min_x = _INFINITY_, min_y = _INFINITY_, min_z = _INFINITY_;

    for (unsigned int i = 0; i < _mesh_size; i++)
    {
        t = _data->get_list_of_tetrahedra()[i];

        _original_tetra_volumes[i] = _data->compute_tet_volume(t);
        _total_mesh_volume += _original_tetra_volumes[i]; 

        _original_tet_material_ids[i] = t->get_material_id();

        if (_material_volumes.find(t->get_material_id()) == _material_volumes.end())
            _material_volumes[t->get_material_id()] = _original_tetra_volumes[i];
        else
            _material_volumes[t->get_material_id()] += _original_tetra_volumes[i];
        
        if (mat_ids.find(t->get_material_id()) == mat_ids.end())
            mat_ids[t->get_material_id()] = 1;
        else
            mat_ids[t->get_material_id()] += 1;

        if (_tumor_region_number.find((unsigned) t->get_material_id()) != _tumor_region_number.end()) 
        {
            tumor_count++;
            _original_tumor_indices.push_back(t->get_id());

            max_x = max(max_x, _data->get_list_of_points()[t->get_p1_id()-1]->get_xcoord());
            max_x = max(max_x, _data->get_list_of_points()[t->get_p2_id()-1]->get_xcoord());
            max_x = max(max_x, _data->get_list_of_points()[t->get_p3_id()-1]->get_xcoord());
            max_x = max(max_x, _data->get_list_of_points()[t->get_p4_id()-1]->get_xcoord());
            min_x = min(min_x, _data->get_list_of_points()[t->get_p1_id()-1]->get_xcoord());
            min_x = min(min_x, _data->get_list_of_points()[t->get_p2_id()-1]->get_xcoord());
            min_x = min(min_x, _data->get_list_of_points()[t->get_p3_id()-1]->get_xcoord());
            min_x = min(min_x, _data->get_list_of_points()[t->get_p4_id()-1]->get_xcoord());

            max_y = max(max_y, _data->get_list_of_points()[t->get_p1_id()-1]->get_ycoord());
            max_y = max(max_y, _data->get_list_of_points()[t->get_p2_id()-1]->get_ycoord());
            max_y = max(max_y, _data->get_list_of_points()[t->get_p3_id()-1]->get_ycoord());
            max_y = max(max_y, _data->get_list_of_points()[t->get_p4_id()-1]->get_ycoord());
            min_y = min(min_y, _data->get_list_of_points()[t->get_p1_id()-1]->get_ycoord());
            min_y = min(min_y, _data->get_list_of_points()[t->get_p2_id()-1]->get_ycoord());
            min_y = min(min_y, _data->get_list_of_points()[t->get_p3_id()-1]->get_ycoord());
            min_y = min(min_y, _data->get_list_of_points()[t->get_p4_id()-1]->get_ycoord());

            max_z = max(max_z, _data->get_list_of_points()[t->get_p1_id()-1]->get_zcoord());
            max_z = max(max_z, _data->get_list_of_points()[t->get_p2_id()-1]->get_zcoord());
            max_z = max(max_z, _data->get_list_of_points()[t->get_p3_id()-1]->get_zcoord());
            max_z = max(max_z, _data->get_list_of_points()[t->get_p4_id()-1]->get_zcoord());
            min_z = min(min_z, _data->get_list_of_points()[t->get_p1_id()-1]->get_zcoord());
            min_z = min(min_z, _data->get_list_of_points()[t->get_p2_id()-1]->get_zcoord());
            min_z = min(min_z, _data->get_list_of_points()[t->get_p3_id()-1]->get_zcoord());
            min_z = min(min_z, _data->get_list_of_points()[t->get_p4_id()-1]->get_zcoord());
        }

        if (t->get_material_id() == 0) // only one element has material id of 0
        {
            _d_max_orig[i] = 0; _d_min_orig[i] = 0; 
        }
        else
        {
            if (_tumor_region_number.find((unsigned) t->get_material_id()) == _tumor_region_number.end()) 
            {
                _d_max_orig[i] = thresholds[t->get_material_id() - 1];
                _d_min_orig[i] = 0;

            }
            else
            {
                _tumor_volume += _original_tetra_volumes[i];
                _d_max_orig[i] = _INFINITY_; 
                _d_min_orig[i] = thresholds[t->get_material_id() - 1];
            }
        }
    }

    // Compute center of tumor
    _tumor_center = SP_Point((max_x + min_x)/2, (max_y + min_y)/2, (max_z + min_z)/2);

    cout << endl << "Tumor size = " << tumor_count << " tetrahedra, with volume = " 
                            << _tumor_volume << " unit_length^3" << endl;
    cout << "NON-Tumor size = " << _mesh_size - tumor_count << " tetrahedra, with volume = " 
                        << _total_mesh_volume - _tumor_volume << " unit_length^3" << endl;
    cout << "Tumor center = " << _tumor_center << endl << endl;

    for (unsigned int current_idx = 1; current_idx <= _data->get_number_of_materials(); current_idx++)
        cout << "Number of elements in Region " << current_idx << " is " << mat_ids[current_idx]
            << "     with volume " << _material_volumes[current_idx]/1000 << " cm^3" << endl; 
    cout << endl;
}

//!
//! This method computes the data of all tetra. It updates the following vectors: _tetra_weights, _tetra_volumes, 
//! _tet_material_ids, _d_max, _d_min
//!
void pdt_plan::compute_tetra_data(const vector<double> & thresholds)
{

    if (!_is_unnecessary_removed)
    {
        fprintf(stderr, "\n\033[1;%dmpdt_plan::compute_tetra_data(): remove_unnecessary_tetra() was not called. Exiting!\033[0m\n", 31);
        exit(-1);

    }

    tetrahedron *t;
    int tumor_count = 0; 
    double total_volume = 0.0, tumor_volume = 0.0;
    int ignored_tumor_count = 0;
    double ignored_tumor_volume = 0.0;

    unsigned int index_to_consider = 0;
    for (unsigned int i = 0; i < _mesh_size; i++)
    {
        t = _data->get_list_of_tetrahedra()[i];

        if (!_tetra_to_consider[i] && _tumor_region_number.find((unsigned) t->get_material_id()) == _tumor_region_number.end()) // ignored tetra
            continue;

        _tetra_volumes[index_to_consider] = _data->compute_tet_volume(t);
        total_volume += _tetra_volumes[index_to_consider]; 
        
        if (_tumor_region_number.find((unsigned) t->get_material_id()) != _tumor_region_number.end()) 
        {
            // computing volume of the tumor
            tumor_volume += _tetra_volumes[index_to_consider];

            tumor_count++; 

            if (!_tetra_to_consider[i]) {
                ignored_tumor_count++;
                ignored_tumor_volume += _tetra_volumes[index_to_consider];
                continue;
            }

            _tumor_indices.push_back(index_to_consider+1);
        }

        if (t->get_material_id() == 0) // only one element has material id of 0
        {
            _d_max[index_to_consider] = 0; _d_min[index_to_consider] = 0; _tetra_weights[index_to_consider] = 0;
        }
        else
        {
            if (_tumor_region_number.find((unsigned) t->get_material_id()) == _tumor_region_number.end()) 
            {
                _d_max[index_to_consider] = thresholds[t->get_material_id() - 1];
                _d_min[index_to_consider] = 0;
            }
            else
            {
                _d_max[index_to_consider] = _INFINITY_; 
                _d_min[index_to_consider] = thresholds[t->get_material_id() - 1];
            }
            _tetra_weights[index_to_consider] = _slopes[t->get_material_id() - 1]*_tetra_volumes[index_to_consider];

            if (_tumor_region_number.find((unsigned) t->get_material_id()) != _tumor_region_number.end()) 
                _tetra_weights[index_to_consider] *= _tumor_weight;
        }

        _tet_material_ids[index_to_consider] = t->get_material_id();

        index_to_consider++;
    }

    cout << endl << "After removing unnecessary tetra: " << endl;
    cout << "   Tumor size = " << tumor_count << " tetrahedra, with volume = " 
                            << tumor_volume << " unit_length^3" << endl;
    cout << "   Blurred Tumor size = " << ignored_tumor_count << " tetrahedra, with volume = " 
                            << ignored_tumor_volume << " unit_length^3" << endl;
    cout << "   NON-Tumor size = " << _actual_mesh_size - tumor_count << " tetrahedra, with volume = " 
                        << total_volume - tumor_volume << " unit_length^3" << endl << endl;

    _is_tetra_data_called = true;

    // if (ignored_tumor_count > 0) exit(-1); // exit at the first time
}

//! This method initializes the LP with its required parameters. The instance initialized assumes the default number of fixed sources.
void pdt_plan::initialize_lp()
{
    // delete previous instance of LP
    if (_my_lp != nullptr)
        delete _my_lp;

    // checking if _p_max is set
    if (_p_max.size() != _num_sources)
    {
        fprintf(stderr, "\n\033[1;%dmpdt_plan::initialize_lp(): _p_max vector size does not match number of sources. Exiting!\033[0m\n", 31);
        exit(-1);
    }

    if (!(_guardband_thresholds_called && _is_unnecessary_removed && _is_tetra_data_called))
    {
        fprintf(stderr, "\n\033[1;%dmpdt_plan::initialize_lp(): Dependent functions were not called. Exiting!\033[0m\n", 31);
        exit(-1);
    }
    
    // Creating a new instance of the fixed source optimization lp
    // TODO: placer->get_num_tumor_elements() returns a 0 but the fixed_sources_cp program does not need it 
    _my_lp = new fixed_sources_cp(_actual_mesh_size, _num_sources, _placer->get_num_tumor_elements(),
                    _d_max, _d_min, 
                    _p_max, _placer->get_source_element_fluence_matrix(),
                    _tetra_weights,
                    _tetra_to_consider);

    if (_op_cvar_approach || _op_variance_constraints)
        _my_lp->initialize_cvar_parameters(_beta_cvar, _tet_material_ids); //_original_tet_material_ids);

    // Defining constraints and objective
    _lp_constraints_defined = _my_lp->define_mosek_constraints();
    if (_lp_constraints_defined)
        _lp_objective_defined = _my_lp->define_mosek_objective(); 
}


//! This method initializes the LP with its required parameters. The instance initialized assumes the given number of sources.
void pdt_plan::initialize_lp(unsigned int num_sources, double p_max_value)
{
    // delete previous instance of LP
    if (_my_lp != nullptr)
        delete _my_lp;

    // checking if the specified number of sources is greater than the maximum number of sources found in FullMonte output
    if (num_sources > _num_sources)
    {
        fprintf(stderr, "\n\033[1;%dmpdt_plan::initialize_lp(num_sources, p_max_value): number of sources specified "
                            "is greater than the maximum number of sources from the light simulater output. Exiting!\033[0m\n", 31);
        exit(-1);
    }

    // checking if _p_max is set
    if (_p_max.size() != num_sources)
    {
        set_p_max_vector(p_max_value); // TODO this won't work
    }

    if (!(_guardband_thresholds_called && _is_unnecessary_removed && _is_tetra_data_called))
    {
        fprintf(stderr, "\n\033[1;%dmpdt_plan::initialize_lp(num_sources): Dependent functions were not called. Exiting!\033[0m\n", 31);
        exit(-1);
    }
    
    // Creating a new instance of the fixed source optimization lp
    _my_lp = new fixed_sources_cp(_actual_mesh_size, num_sources, _placer->get_num_tumor_elements(),
                    _d_max, _d_min, 
                    _p_max, _placer->get_source_element_fluence_matrix(),
                    _tetra_weights,
                    _tetra_to_consider);

    if (_op_cvar_approach || _op_variance_constraints)
        _my_lp->initialize_cvar_parameters(_beta_cvar, _tet_material_ids);//_original_tet_material_ids);

    // Defining constraints and objective
    _lp_constraints_defined = _my_lp->define_mosek_constraints();
    if (_lp_constraints_defined)
        _lp_objective_defined = _my_lp->define_mosek_objective(); 
}

//!
//! This method initializes the LP with its required parameters. The instance initialized assumes the given number of sources
//! and the given fluence matrix. This method is used in the virtual source reduction algorithm 
//!
void pdt_plan::initialize_lp(unsigned int num_sources, double p_max_value, const vector<vector<float>> & fluence_matrix)
{
    // delete previous instance of LP
    if (_my_lp != nullptr)
        delete _my_lp;

    // checking if _p_max is set
    if (_p_max.size() != num_sources)
    {
        _p_max.clear(); 
        _p_max.resize(num_sources);
    }
    fill(_p_max.begin(), _p_max.end(), p_max_value); 

    if (!(_guardband_thresholds_called && _is_unnecessary_removed && _is_tetra_data_called))
    {
        fprintf(stderr, "\n\033[1;%dmpdt_plan::initialize_lp(num_sources): Dependent functions were not called. Exiting!\033[0m\n", 31);
        exit(-1);
    }
    
    // Creating a new instance of the fixed source optimization lp
    _my_lp = new fixed_sources_cp(_actual_mesh_size, num_sources, _placer->get_num_tumor_elements(),
                    _d_max, _d_min, 
                    _p_max, fluence_matrix,
                    _tetra_weights,
                    _tetra_to_consider);

    if (_op_cvar_approach || _op_variance_constraints)
        _my_lp->initialize_cvar_parameters(_beta_cvar, _tet_material_ids);

    // Defining constraints and objective
    _lp_constraints_defined = _my_lp->define_mosek_constraints();
    if (_lp_constraints_defined)
        _lp_objective_defined = _my_lp->define_mosek_objective(); 
}

//! This method solves the LP by calling the suitable MOSEK function. source_tailored is true if we are optimizing over line sources with tailored profiles.
void pdt_plan::solve_lp(const vector<double> & guardbanded_thresholds)
{
    profiler opt_time("Optimization time"); 

    fprintf(stderr, "\n\033[1;%dmSolving the linear program...\033[0m\n", 35);
    
    double optimizer_time = 0;
    double max_tumor_weight = -1; // initialize to -1 (indicates INF)
    double min_tumor_weight = 0;  // initialize to 0
    
    if (_vary_tumor_weight) // keep varying the weight of the tumor to get a v100 of 98%
    {
        double prev_weight = _tumor_weight; // initial previous weight
        double prev_tumor_v100 = 0.0; // used to terminate solve if tumor v100 dearch when it should have increased (mosek error) 
        double alpha = 1.0;
        _num_lp_iterations = 0; 

        bool first_trial = true;
            
        bool lp_constraints_updated = true;
        while (true) // keep looping if it didn't converge
        { 
            _num_lp_iterations++;
            cout << endl << "Current Tumor weight is: " << _tumor_weight << endl;

            // change the tetra weights accordingly 

            for (unsigned int i = 0; i < _tumor_indices.size(); i++)
            {
                _tetra_weights[_tumor_indices[i]-1] *= (_tumor_weight/prev_weight);
            }

            // create a new lp object
            if (_my_lp == nullptr) {
                initialize_lp();  
            } else if (!first_trial) { 
                _my_lp->set_tetra_weights(_tetra_weights); 
                lp_constraints_updated = _my_lp->update_tumor_mosek_constraints(_tumor_weight);
            }

            if (!_lp_constraints_defined || !_lp_objective_defined || !lp_constraints_updated)
            {
                fprintf(stderr, "\n\033[1;%dmpdt_plan::solve_lp::constraints and/or objectives are not defined. Exiting..."
                        "\033[0m\n", 31); 
                _num_lp_iterations = _max_num_lp_iterations + 1; 
                return;
                //std::exit(-1);
            }

            // solve the lp
            if (_source_tailored) {
                _final_source_powers = _my_lp->run_mosek_optimization_with_tailored_profiles(_placer->
                        get_fiber_elements(), optimizer_time, _num_non_zero_sources, _total_power);
            }
            else if (_op_cvar_approach) {
                _final_source_powers = _my_lp->run_mosek_optimization_with_cvar_constraints(optimizer_time, _num_non_zero_sources, 
                                    _total_power, _tumor_region_number, (unsigned int)_d_max_tissues.size(), 
                                    _cvar_expected_dose_matrix);
            }
            else if (_op_variance_constraints) {
                _final_source_powers = _my_lp->run_mosek_optimization_with_variance_constraints(optimizer_time, _num_non_zero_sources, 
                                    _total_power, _tumor_region_number, (unsigned int)_d_max_tissues.size(), 
                                    _source_element_dose_variance);
            }
            else if (_power_variance_optimization) {
                _final_source_powers = _my_lp->run_mosek_optimization_with_power_variation(optimizer_time, _num_non_zero_sources, _total_power, _sigma_power, _power_variance_weight);
            }
            else {
                _final_source_powers = _my_lp->run_mosek_optimization(optimizer_time, _num_non_zero_sources, _total_power);
            }
            if (_final_source_powers.empty()) {
                _num_lp_iterations = _max_num_lp_iterations + 1; 
                return;
            }

            _is_lp_solved = true; 

            // clear old fluence values
            _final_fluence.clear();

            vector<double> v100_tissues = compute_v_alpha_tissues(guardbanded_thresholds, alpha);
        
            print_v100s(v100_tissues);

            double temp_prev = first_trial ? 1.0 : prev_weight; // store previous weight
            prev_weight = _tumor_weight;

            double lower_v100_bound = _lower_tumor_v100_bound;
            double upper_v100_bound = _upper_tumor_v100_bound;

            // Compute v100 of tumor using all tumor regions 
            double tumor_v100 = 0.0; 
            double total_tumor_volume = 0.0; 
            for (auto tumor_region : _tumor_region_number) {
                total_tumor_volume += _material_volumes[tumor_region];

                tumor_v100 += ((v100_tissues[tumor_region - 1]*_material_volumes[tumor_region])/100);
            }
            tumor_v100 = (tumor_v100/total_tumor_volume)*100;
           
            if ((tumor_v100/*v100_tissues[*(_tumor_region_number.begin()) - 1]*/ > lower_v100_bound && 
                tumor_v100/*v100_tissues[*(_tumor_region_number.begin()) - 1]*/ < upper_v100_bound) || 
                _num_lp_iterations > _max_num_lp_iterations/* || _power_variance_optimization*/) // within target
            {
                break;
            }
            else if (tumor_v100/*v100_tissues[*(_tumor_region_number.begin()) - 1]*/ < prev_tumor_v100 && 
                    temp_prev < _tumor_weight) 
            {
                std::cerr << "Error during optimization" << std::endl;
                _num_lp_iterations = _max_num_lp_iterations + 1; 
                //return; 
                break;
            }
            else if (first_trial)
            {
                first_trial = false;
                
                if (tumor_v100/*v100_tissues[*(_tumor_region_number.begin()) - 1]*/ > upper_v100_bound)
                {
                    max_tumor_weight = _tumor_weight;
                    _tumor_weight /= 2;
                }
                else 
                {
                    min_tumor_weight = _tumor_weight;
                    _tumor_weight *= 2;
                }

            }
            else
            {
                if (tumor_v100/*v100_tissues[*(_tumor_region_number.begin()) - 1]*/ > upper_v100_bound)
                {
                    max_tumor_weight = _tumor_weight;
                    if (min_tumor_weight == 0) {
                        _tumor_weight /= 10; // set to 10 to quickly restore tumor weight
                    } else {
                        _tumor_weight = (min_tumor_weight + max_tumor_weight) / 2;
                    }
                    
                    if (_tumor_weight >= prev_weight) {
                        std::cerr << "Tumor coverage cannot be lowered further." << std::endl;
                        break;
                    } 
                }
                else 
                {
                    min_tumor_weight = _tumor_weight;
                    if (max_tumor_weight == -1) {
                        _tumor_weight *= 10; // set to 10 to achieve larger tumor weight faster
                        if (_tumor_weight >= 1e11) {
                            std::cerr << "Not able to achieve tumor coverage." << std::endl;
                            _num_lp_iterations = _max_num_lp_iterations + 1; 
                            //return; 
                            break;
                        }
                    } else {
                        _tumor_weight = (min_tumor_weight + max_tumor_weight) / 2;
                    }
                }
            }

            prev_tumor_v100 = tumor_v100;//v100_tissues[*(_tumor_region_number.begin()) - 1]; 

            //delete _my_lp;
            //_my_lp = nullptr;
        }
    }   
    else
    {

        if (_my_lp == nullptr) // LP is not initialized
        {
            fprintf(stderr, "\n\033[1;%dmpdt_plan::solve_lp(): Linear program is not initialized. Exiting!\033[0m\n", 31);
            exit(-1);
        }
        if (!_lp_constraints_defined || !_lp_objective_defined)
        {
            fprintf(stderr, "\n\033[1;%dmpdt_plan::solve_lp::constraints and or objectives are not defined. Exiting..."
                    "\033[0m\n", 31); 
            std::exit(-1);
        }
   
        // running the optimization
        if (_source_tailored)
            _final_source_powers = _my_lp->run_mosek_optimization_with_tailored_profiles(_placer->get_fiber_elements(), optimizer_time,
                                    _num_non_zero_sources, _total_power);
        else if (_op_cvar_approach)
            _final_source_powers = _my_lp->run_mosek_optimization_with_cvar_constraints(optimizer_time, _num_non_zero_sources, 
                                _total_power, _tumor_region_number, (unsigned int)_d_max_tissues.size(), _cvar_expected_dose_matrix);
        else if (_op_variance_constraints)
            _final_source_powers = _my_lp->run_mosek_optimization_with_variance_constraints(optimizer_time, _num_non_zero_sources, 
                                _total_power, _tumor_region_number, (unsigned int)_d_max_tissues.size(), _source_element_dose_variance);
        else if (_power_variance_optimization) {
            _final_source_powers = _my_lp->run_mosek_optimization_with_power_variation(optimizer_time, _num_non_zero_sources, _total_power, _sigma_power, _power_variance_weight);
        }
        else
            _final_source_powers = _my_lp->run_mosek_optimization(optimizer_time, _num_non_zero_sources, _total_power);

        // setiing source powers to original source powers from population average of OP
        {
//             // tumor 4
//             _final_source_powers.resize(3);
//             _final_source_powers[0] = 0.00215877;//1.94776e-5;
//             _final_source_powers[1] = 0.00983784;//8.78593e-5;
//             _final_source_powers[2] = 0.00506754;//9.70509e-5;
        }

        {
            // tumor 1
//             _final_source_powers.resize(19);
//             _final_source_powers[0] = 7.67176e-10;
//             _final_source_powers[1] = 8.45444e-08;
//             _final_source_powers[2] = 0;
//             _final_source_powers[3] = 4.94303e-06;
//             _final_source_powers[4] = 5.95508e-07;
//             _final_source_powers[5] = 1.27716e-05;
//             _final_source_powers[6] = 2.52222e-07;
//             _final_source_powers[7] = 3.76555e-08;
//             _final_source_powers[8] = 1.26749e-06;
//             _final_source_powers[9] = 3.65716e-07;
//             _final_source_powers[10] = 1.3793e-06;
//             _final_source_powers[11] = 5.57241e-07;
//             _final_source_powers[12] = 1.57735e-06;
//             _final_source_powers[13] = 3.22146e-08;
//             _final_source_powers[14] = 6.72433e-07;
//             _final_source_powers[15] = 1.3181e-06;
//             _final_source_powers[16] = 1.61248e-06;
//             _final_source_powers[17] = 5.04861e-08;
//             _final_source_powers[18] = 1.30405e-09;
        }
        {
            // tumor 6
//             _final_source_powers.resize(14);
//             _final_source_powers[0] = 8.32786e-08;
//             _final_source_powers[1] = 3.95238e-05;
//             _final_source_powers[2] = 3.3434e-05;
//             _final_source_powers[3] = 8.56056e-07;
//             _final_source_powers[4] = 1.42737e-06;
//             _final_source_powers[5] = 0.000127729;
//             _final_source_powers[6] = 9.52769e-07;
//             _final_source_powers[7] = 5.8649e-06;
//             _final_source_powers[8] = 5.52204e-07;
//             _final_source_powers[9] = 2.03246e-07;
//             _final_source_powers[10] = 0.000304194;
//             _final_source_powers[11] = 8.242e-05;
//             _final_source_powers[12] = 1.07207e-05;
//             _final_source_powers[13] = 4.97289e-07;
        }
    
    }
    
//             _final_source_powers.resize(3);
//             _final_source_powers[0] = 3.15699e-5;//0.00215877;//1.94776e-5;
//             _final_source_powers[1] = 0.000410393;//0.00983784;//8.78593e-5;
//             _final_source_powers[2] = 0.00010072;//0.00506754;//9.70509e-5;
    // setting the optimizer time vector
    _lp_runtime.push_back(optimizer_time);

    // setting the cost vector
    _lp_cost.push_back(_my_lp->get_cost());

    _is_lp_solved = true; 
}

//! This method computes the cost of the last optimization
double pdt_plan::get_last_cost()
{
    return _my_lp->get_cost();
}

//! This method computes the fluence distribution after optimization and stores the result in _final_fluence
void pdt_plan::compute_post_opt_fluence()
{
    if (!_is_lp_solved)
    {
        fprintf(stderr, "\n\033[1;%dmpdt_plan::compute_post_opt_fluence(): LP is not solved yet. Exiting!\033[0m\n", 31);
        exit(-1);
    }

    _placer->compute_current_fluence(_mesh_size, _final_source_powers, _final_fluence);
}

//! This method computes the v_alpha of all tissues after optimization at the given thresholds. alpha = 1 ==> v_100, alpha = 0.9 ==>v90, ...etc.
vector<double> pdt_plan::compute_v_alpha_tissues(const vector<double> & guardbanded_thresholds, double alpha)
{
    if (_final_fluence.size() == 0) // did not compute the final fluence distribution
        compute_post_opt_fluence();


    vector<double> v_alpha = _placer->compute_v_alpha_tissues(_final_fluence, _original_tet_material_ids,
                    _original_tetra_volumes, guardbanded_thresholds, _tumor_region_number, alpha);

    return v_alpha;
}

//!
//! This method evaluates the placer with a different number of packets
//! @param vtk_matrix_file [in]: this specifies the file and name of the vtk file from FullMonte that has the fluence distribution of the
//!                       sources according to the new number of packets
//! @param guardband [in]: guardband on the healthy tissue thresholds. Range: (0,1]
//!
// void pdt_plan::evaluate_placer(double new_num_packets, string vtk_matrix_file, double guardband)
// {

//     // TODO Depricate this 

//     if (!_is_lp_solved)
//     {
//         fprintf(stderr, "\n\033[1;%dmpdt_plan::evaluate_placer(): LP is not solved yet. Exiting!\033[0m\n", 31);
//         exit(-1);
//     }

//     // Creating a new src_placer object
//     src_placer * evaluation_placer = new src_placer();
//     evaluation_placer->set_num_packets(new_num_packets);
//     evaluation_placer->set_total_energy(_total_energy);

//     // Reading VTK file
//     evaluation_placer->read_vtk_file(vtk_matrix_file, _mesh_size);
    
//     // computing the fluence using the placer function since it takes into account the ignored tetra
//     vector<double> final_fluence;
//     evaluation_placer->compute_current_fluence(_mesh_size, _final_source_powers, final_fluence);

//     double original_scale_factor_packets = _num_packets/_total_energy;
//     double new_scale_factor_packets = new_num_packets/_total_energy;
//     for (unsigned int i = 0; i < _d_max_tissues.size(); i++)
//     {
//         if (_d_max_tissues[i] != _INFINITY_)
//             _d_max_tissues[i] *= (new_scale_factor_packets/original_scale_factor_packets);
//         _d_min_tissues[i] *= (new_scale_factor_packets/original_scale_factor_packets);
//     }
   
//     vector<double> new_thresholds(5);
//     new_thresholds[0] = _d_min_tissues[4];
//     new_thresholds[4] = _d_min_tissues[4];
//     new_thresholds[2] = guardband*(_d_max_tissues[2]/0.1);
//     new_thresholds[3] = guardband*(_d_max_tissues[3]/0.1); // dividing by 0.1 since given data is already guardbanded by 10%
//     new_thresholds[1] = 10*max(_d_min_tissues[4], max(new_thresholds[2], new_thresholds[3]));


//     // computing v90 of the different tissues
//     double alpha = 0.9; // TODO get as input
//     vector<double> v_alpha_tissues_evaluation = evaluation_placer->compute_v_alpha_tissues(final_fluence, _tet_material_ids,
//                                                                 _tetra_volumes, new_thresholds,
//                                                                 _tumor_region_number, alpha);

//     cout << endl << "Evaluation Grey Matter V90 is:  " << v_alpha_tissues_evaluation[2] << " cm^3" << endl;
//     cout <<         "Evaluation White Matter V90 is: " << v_alpha_tissues_evaluation[3] << " cm^3" << endl;
//     cout <<         "Evaluation Tumor V90 is:        " << v_alpha_tissues_evaluation[4] << " %" << endl << endl;

//     //*
//     bpt::ptime local_time = Util::get_local_time();
//     ofstream results_out;
//     stringstream results_out_ss;
//     results_out_ss << "Results/line_sources/fixed_length/photons10e5/post_opt/" << _wavelength << ".txt";  // TODO: specify as input
//     results_out.open(results_out_ss.str().c_str(), ofstream::app);
//     results_out << local_time << "\t" << _data_name << "\t" << _source_type << "\t" << _placement_type << "\t" << 
//             "interior point\t" << _num_non_zero_sources << "\t" << _tumor_weight << "\t" << _my_lp->get_cost() << 
//             "\t" << v_alpha_tissues_evaluation[4] << "\t" << v_alpha_tissues_evaluation[2] << "\t" << v_alpha_tissues_evaluation[3] << "\t" << _lp_runtime[0] 
//             << "\t" << _total_power << "\t" << _total_energy << endl;
//     results_out.close();
//     // */

//     delete evaluation_placer;
// }

//!
//! This method runs the conservative approach of the OP variation by reading all num_op_cases fullmonte output files, and 
//! builds the matrix by assigning each healthy tissue element its maximum fluence, and each tumor element its minimum element
//!
void pdt_plan::conservative_approach_op_variation(unsigned int num_op_cases)
{
    profiler opt_time("Conservative OP Variation"); 

    fprintf(stderr, "\n\033[1;%dmGenerating dose matrix for conservative OP variation approach...\033[0m\n", 35);

    // get the file path and partial name of the fullmonte output files (convention is <file_name>_<file_number>.vtk where 0 < file_number < num_op_cases
    string vtk_file_path = _fullmonte_output_file.substr(0, _fullmonte_output_file.size()-5);

//     // create a vector of placers of size num_op_cases
//     _placers.resize(num_op_cases);
// 
//     // initialize the placers 
//     for (unsigned int i = 0; i < num_op_cases; i++)
//         _placers[i] = new src_placer(); 
// 
//     // read the num_op_cases fullmonte outputs
//     Util::BEGIN_TIME = chrono::steady_clock::now();
//     for (unsigned int i = 0; i < num_op_cases; i++)
//     {
//         stringstream vtk_file_ss; 
//         vtk_file_ss << vtk_file_path << i << ".vtk";
// 
//         _placers[i]->read_vtk_file(vtk_file_ss.str(), _mesh_size);
//     }
//     Util::END_TIME = chrono::steady_clock::now();

    double vtk_read_time = (double)chrono::duration_cast<chrono::seconds>
                    (Util::END_TIME - Util::BEGIN_TIME).count(); 

    cout << "Time to read all VTK files is: " << vtk_read_time << " secs" << endl;
    
    read_fullmonte_output_diff_op(vtk_file_path, num_op_cases);

    // safety check
    if (_data->get_list_of_tetrahedra().size() != _mesh_size)
    {
        fprintf(stderr, "\033[1;%dmconservative_approach_op_variation::Size doesn't match. Exiting...\033[0m\n", 31);
        exit(-1);
    }
    
    // checking over all matrices to find max fluence for each healthy element and min fluence for each tumor element
    for (unsigned int i = 0; i < num_op_cases; i++)
    {
        for (unsigned int j = 0; j < _num_sources; j++)
        {
            for (unsigned int k = 0; k < _mesh_size; k++)
            {
                if (_tumor_region_number.find((unsigned)_data->get_list_of_tetrahedra()[k]->get_material_id()) 
                        != _tumor_region_number.end())
                {
                    _placer->get_source_element_fluence_matrix()[j][k] = min(_placer->get_source_element_fluence_matrix()[j][k],
                                                                        _placers[i]->get_source_element_fluence_matrix()[j][k]);
                }
                else
                {
                    _placer->get_source_element_fluence_matrix()[j][k] = max(_placer->get_source_element_fluence_matrix()[j][k],
                                                                        _placers[i]->get_source_element_fluence_matrix()[j][k]);
                }
            }
        }
    }
    fprintf(stderr, "\n\033[1;%dmDone Generating dose matrix for conservative OP variation approach...\033[0m\n", 35);

}

//!
//! method that reads all fullmonte output cases for tumor op variation, 
//! assigns each healthy tissue element fluence to the <percentile> of that element 
//! (value that is largest than <percentile> of the elements),
//! and each tumor element fluence to the 1-percentile of that element    
//! 0 < percentile < 1
//!
void pdt_plan::percentile_approach_op_variation(string vtk_files_path, unsigned int num_op_cases, double percentile)
{
    profiler opt_time("Percentile OP Variation"); 

    fprintf(stderr, "\n\033[1;%dmGenerating dose matrix for Percentile OP variation approach...\033[0m\n", 35);

    
    if (percentile < 0 || percentile > 1)
    {
        fprintf(stderr, "\033[1;%dmpercentile_approach_op_variation::percentile should be between 0 and 1. Exiting...\033[0m\n", 31);
        exit(-1);
    }
    
    // get the file path and partial name of the fullmonte output files (convention is <file_name>_<file_number>.vtk where 0 < file_number < num_op_cases
//     string vtk_file_path = _fullmonte_output_file.substr(0, _fullmonte_output_file.size()-5);
    string vtk_file_path = vtk_files_path.substr(0, vtk_files_path.size()-5);

//     // create a vector of placers of size num_op_cases
//     _placers.resize(num_op_cases);
// 
//     // initialize the placers 
//     for (unsigned int i = 0; i < num_op_cases; i++)
//         _placers[i] = new src_placer(); 
// 
//     // read the num_op_cases fullmonte outputs
//     Util::BEGIN_TIME = chrono::steady_clock::now();
//     for (unsigned int i = 0; i < num_op_cases; i++)
//     {
//         stringstream vtk_file_ss; 
//         vtk_file_ss << vtk_file_path << i << ".vtk";
// 
//         _placers[i]->read_vtk_file(vtk_file_ss.str(), _mesh_size);
//     }
//     Util::END_TIME = chrono::steady_clock::now();
// 
//     double vtk_read_time = (double)chrono::duration_cast<chrono::seconds>
//                     (Util::END_TIME - Util::BEGIN_TIME).count(); 
// 
//     cout << "Time to read all VTK files is: " << vtk_read_time << " secs" << endl;

    read_fullmonte_output_diff_op(vtk_file_path, num_op_cases);
    
    // safety check
    if (_data->get_list_of_tetrahedra().size() != _mesh_size)
    {
        fprintf(stderr, "\033[1;%dmpercentile_approach_op_variation::Size doesn't match. Exiting...\033[0m\n", 31);
        exit(-1);
    }
    
    // checking over all matrices to find <percentile> fluence for each healthy element and <1-percentile> fluence for each tumor element
    
    // creating an array for each element of size ceil(percentile*mesh_size) to store the max values for healthy tissue elements (and taking the minimum) 
    // or the minimum values for tumor elements (and taking the maximum)
    // The temporary arrays will be filled in ascending order for OAR and descending order for tumor elements
    
    unsigned int perc_array_size = 1 + (unsigned int)((1-percentile)*num_op_cases);
    vector<vector<vector<float>>> percentile_array(_num_sources);
    vector<vector<vector<unsigned int>>> percentile_placer_index(_num_sources); // stores the index of the case where the percentile was taken from

    for (unsigned int i = 0; i < _num_sources; i++)
    {
        percentile_array[i].resize(_mesh_size);
        percentile_placer_index[i].resize(_mesh_size);
        for (unsigned int j = 0; j < _mesh_size; j++)
        {
            percentile_array[i][j].resize(perc_array_size);
            percentile_placer_index[i][j].resize(perc_array_size);

            if (_tumor_region_number.find((unsigned)_data->get_list_of_tetrahedra()[j]->get_material_id())
                    != _tumor_region_number.end())
                fill(percentile_array[i][j].begin(), percentile_array[i][j].end(), _INFINITY_);
            else
                fill(percentile_array[i][j].begin(), percentile_array[i][j].end(), 0.0);

            fill(percentile_placer_index[i][j].begin(), percentile_placer_index[i][j].end(), 0);
        }
    }

    // looping over all num_op_cases
    for (unsigned int i = 0; i < num_op_cases; i++)
    {
        for (unsigned int j = 0; j < _num_sources; j++)
        {
            for (unsigned int k = 0; k < _mesh_size; k++)
            {
                float fluence_value = _placers[i]->get_source_element_fluence_matrix()[j][k];
                if (_tumor_region_number.find((unsigned)_data->get_list_of_tetrahedra()[k]->get_material_id())
                            != _tumor_region_number.end())
                {
                    for (unsigned int p = 0; p < perc_array_size; p++)
                    {
                        if (fluence_value < percentile_array[j][k][p]) // if the new value is less than one of the elements then shift up one and replace
                        {
                            for (unsigned int z = perc_array_size - 1; z > p; z--)
                            {
                                percentile_array[j][k][z] = percentile_array[j][k][z-1];
                                percentile_placer_index[j][k][z] = percentile_placer_index[j][k][z-1];
                            }
                            percentile_array[j][k][p] = fluence_value;
                            percentile_placer_index[j][k][p] = i;
                            break;
                        }
                    }
                }
                else
                {
                    for (int p = perc_array_size - 1; p >= 0; p--)
                    {
                        if (fluence_value > percentile_array[j][k][p]) // if the new value is greater than one of the elements then shift down one and replace
                        {
                            for (int z = 0; z < p; z++)
                            {
                                percentile_array[j][k][z] = percentile_array[j][k][z+1];
                                percentile_placer_index[j][k][z] = percentile_placer_index[j][k][z+1];
                            }
                            percentile_array[j][k][p] = fluence_value;
                            percentile_placer_index[j][k][p] = i;
                            break;
                        }
                    }
                }
            }
        }
    }
    
    // getting the smallest value in the percentile array for the OAR elements and the largest for the tumor elements
    for (unsigned int j = 0; j < _num_sources; j++)
    {
        float max_oar_percentile = 0.0;
        //int index_max = -1;
        for (unsigned int k = 0; k < _mesh_size; k++)
        {
            // for each source check the highest percentile value for each OAR tetra and get the fluence matrix of 
            // the placer that gives the highest percentile among all tetra (can make it for a specific tissue type)
            if (_tumor_region_number.find((unsigned)_data->get_list_of_tetrahedra()[k]->get_material_id())
                    == _tumor_region_number.end())
            {
                if (percentile_array[j][k][perc_array_size - 1] > max_oar_percentile)
                {
                    //index_max = percentile_placer_index[j][k][perc_array_size - 1];
                    max_oar_percentile = percentile_array[j][k][perc_array_size - 1];
                }
            }
        }

        for (unsigned int k = 0; k < _mesh_size; k++)
        {
            _placer->get_source_element_fluence_matrix()[j][k] =  percentile_array[j][k][perc_array_size - 1];
//              _placer->get_source_element_fluence_matrix()[j][k] =  _placers[index_max]->get_source_element_fluence_matrix()[j][k];
        }
    }


    for (unsigned int i = 0; i < percentile_array.size(); i++)
    {
        for (unsigned int j = 0; j < percentile_array[i].size(); j++)
        {
            percentile_array[i][j].clear();
            percentile_placer_index[i][j].clear();
        }
        percentile_array[i].clear();
        percentile_placer_index[i].clear();
    }
    percentile_array.clear();
    percentile_placer_index.clear();

    fprintf(stderr, "\n\033[1;%dmDone Generating dose matrix for conservative OP variation approach...\033[0m\n", 35);
}

//! This method returns a set of v100 for different tissues with different sets of OP
vector<vector<double>> pdt_plan::compute_v_alpha_tissues_percentile_approach(const vector<double> & guardbanded_thresholds, double alpha, unsigned int num_op_cases)
{
    if (_placers.size() == 0)
    {
        fprintf(stderr, "\033[1;%dmcompute_v_alpha_percentile_approach::FullMonte output for different cases was not read before. Exiting...\033[0m\n", 31);
        exit(-1);
    }

    vector<vector<double>> v_alphas(num_op_cases);

    for (unsigned int i = 0; i < num_op_cases; i++)
    {
        vector<double> post_opt_fluence;
        _placers[i]->compute_current_fluence(_mesh_size, _final_source_powers, post_opt_fluence);

        v_alphas[i] = _placers[i]->compute_v_alpha_tissues(post_opt_fluence, _original_tet_material_ids,
                    _original_tetra_volumes, guardbanded_thresholds, _tumor_region_number, alpha);
    }
    
    return v_alphas;
}

//!
//! This method creates a feasibility problem that guarantees that no tetra exceeds its threshold dose 
//! with a probability of 1-delta when the optical properties are varying
//!
void pdt_plan::op_variation_feasibility_problem(const vector<double> & /*guardbanded_thresholds*/, 
                                        unsigned int num_op_cases, double delta)
{
    profiler feasibility("OP Variation Feasibility Study"); 
    /*
     *  The method follows the following steps:
     *  1- Set the mean value for each tetrahedron to the dose it gets from the average optical properties
     *  2- Read all Fullmonte outputs for different sets of optical properties.
     *  3- Fit a normal distribution for the fluence at each tetrahedron due to each source by assigning a certain standard deviation
     *  4- Compute the covariance matrix for each tetrahedron between the different sources 
     *  5- build the feasibility problem and solve it
     *  6- If a solution is found compute the probablity of tissues v100s and output the dose-expected volume histogram
     */

    fprintf(stderr, "\n\033[1;%dmStarting the feasibility study of the OP variation...\033[0m\n", 35);

    vector<vector<float>> mean_tetra_fluence = _placer->get_source_element_fluence_matrix();

    
    if (delta < 0 || delta > 1)
    {
        fprintf(stderr, "\033[1;%dmop_variation_feasibility_problem::delta should be between 0 and 1. Exiting...\033[0m\n", 31);
        exit(-1);
    }
    
    // get the file path and partial name of the fullmonte output files (convention is <file_name>_<file_number>.vtk where 0 < file_number < num_op_cases
    string vtk_file_path = "/media/yassineab/DATAPART1/PhD_Research_temp_data/variation_optical_props/line_sources/flat_profiles/"
                            "normal_distribution/vary_mu_a_s/Colin27_tagged_tumor_4_line_sources_tet_fluence_v1_635nm_10p_volume_tumor_";

    read_fullmonte_output_diff_op(vtk_file_path, num_op_cases);


    // TODO: Continue this approach if time permits
}

//!
//! This method minimizes the expected cost while constraining the expected dose
//! received by the highest 100*(1-beta)% of OAR tissues and lowest 100*(1-beta)% of the tumore 
//!
void pdt_plan::cvar_optimization_op_problem(unsigned int num_op_cases, double beta)
{
    profiler cvar_opt("CVAR OPT problem");
    
    fprintf(stderr, "\n\033[1;%dmStarting the CVAR optimization study of the OP variation...\033[0m\n", 35);

    if (beta < 0 || beta > 1)
    {
        fprintf(stderr, "\033[1;%dmcvar_optimization_op_problem::beta should be between 0 and 1. Exiting...\033[0m\n", 31);
        exit(-1);
    }

    // get the file path and partial name of the fullmonte output files (convention is <file_name>_<file_number>.vtk where 0 < file_number < num_op_cases
    string vtk_file_path = "/media/yassineab/DATAPART1/PhD_Research_temp_data/variation_optical_props/line_sources/flat_profiles/"
                            "normal_distribution/vary_mu_a_s/Colin27_tagged_tumor_6_line_sources_tet_fluence_v1_635nm_10p_volume_tumor_1.vtk";
//     string vtk_file_path = "/media/yassineab/DATAPART1/PhD_Research_temp_data/variation_optical_props/line_sources/flat_profiles/"
//                             "normal_distribution/vary_mu_a_s/with_sampling_probabilities/Colin27_tagged_tumor_4_line_sources_tet_fluence_v1_635nm_10p_volume_tumor_1.vtk";

//     read_fullmonte_output_diff_op(vtk_file_path, num_op_cases);

    // TODO: build the dose response function from the average fluence (assign probabilities to bins)
//     percentile_approach_op_variation(vtk_file_path, num_op_cases, 0.5);


    // reading discretized probabilities of the rand OPs
    string probs_file = "/home/yassineab/PhD_Research/src_placement/TCL_scripts/fixed_sources_vtk/line_sources/fixed_length/variation_optical_props/635nm/"
                            "normal_distribution/vary_mu_a_s/with_sampling_probabilities/mu_eff_probs.txt";

    ifstream probs_in(probs_file);
    if (!probs_in)
    {
        fprintf(stderr, "\033[1;%dmpdt_plan::cvar_optimization_op_problem: Could not read "
                        "Probabilities file %s\033[0m\n", 31, probs_file.c_str());
        exit(-1);
    }

    vector<double> mu_eff_probs(num_op_cases, 0);
    for (unsigned int i = 0; i < num_op_cases; i++)
    {
        string line;
        getline(probs_in, line);
        stringstream str(line);
        str >> mu_eff_probs[i];
    }

    double sum_probs = 0; 
    for (unsigned int i = 0; i < num_op_cases; i++)
        sum_probs += mu_eff_probs[i];

    // normalizing the probabilities just in case we used a different number of cases
    for (unsigned int i = 0; i < num_op_cases; i++)
    {
        mu_eff_probs[i] = mu_eff_probs[i]/sum_probs;
    }

    // Computing the expected dose matrix from the probabilities and the _placers vector
    _cvar_expected_dose_matrix.resize(_num_sources);
    for (unsigned int i = 0; i < _num_sources; i++)
    {
        _cvar_expected_dose_matrix[i].resize(_mesh_size);
        fill(_cvar_expected_dose_matrix[i].begin(), _cvar_expected_dose_matrix[i].end(), 0);
        
        for (unsigned int j = 0; j < _mesh_size; j++)
        {
            for (unsigned int k = 0; k < num_op_cases; k++)
            {
       //         _cvar_expected_dose_matrix[i][j] += (float)mu_eff_probs[k]*_placers[k]->get_source_element_fluence_matrix()[i][j];
            }
        }
    }
     

    _op_cvar_approach = true;
    _beta_cvar = beta; 

//     _op_variance_constraints = true;
    //compute_tetra_dose_variance(); // TODO: doesn't work if we are calling ignore_unnecessary_tetra

    //_ignore_unnecessary_tetra = false; // TODO: doesn't work if we are using cvar_expected_dose_matrix
}

//! This method reads different outputs of FullMonte corresponding to different sets of optical properties, and places the results in _placers vector
void pdt_plan::read_fullmonte_output_diff_op(string vtk_files_path, unsigned int num_op_cases)
{
    profiler op_cases("Reading Fullmonte output diff OP");

    // create a vector of placers of size num_op_cases
    for (unsigned int i = 0; i < _placers.size(); i++)
        if (_placers[i] != nullptr)
            delete _placers[i];

    _placers.clear();
    _placers.resize(num_op_cases);

    // initialize the placers 
    for (unsigned int i = 0; i < num_op_cases; i++)
        _placers[i] = new src_placer(); 

    // read the num_op_cases fullmonte outputs
    Util::BEGIN_TIME = chrono::steady_clock::now();
    for (unsigned int i = 0; i < num_op_cases; i++)
    {
        stringstream vtk_file_ss; 
        vtk_file_ss << vtk_files_path << i << ".vtk";

        _placers[i]->read_vtk_file(vtk_file_ss.str(), _mesh_size);
    }
    Util::END_TIME = chrono::steady_clock::now();

    double vtk_read_time = (double)chrono::duration_cast<chrono::seconds>
                    (Util::END_TIME - Util::BEGIN_TIME).count(); 

    cout << "Time to read all VTK files is: " << vtk_read_time << " secs" << endl;
}

//! This method computes the variance of the dose at each tetrahedron from each source with unit power due to variation in OP
void pdt_plan::compute_tetra_dose_variance()
{
    profiler variance_compu("Dose variance computation");

    fprintf(stderr, "\n\033[1;%dmComputing the variance of the dose at each tetrahedron...\033[0m\n", 35);

    if (_placers.size() == 0)
    {
         fprintf(stderr, "\n\033[1;%dmpdt_plan::compute_tetra_dose_variance()::No variation cases have been read! Exiting...\033[0m\n", 31);
        exit(-1);    
    }

    unsigned int num_op_cases = (unsigned int)_placers.size();

    // Clearing previously created variance matrix
    for (unsigned int i = 0; i < _source_element_dose_variance.size(); i++)
    {
        if (_source_element_dose_variance[i].size() != 0)
        {
            _source_element_dose_variance[i].clear();
            _source_element_dose_variance[i].resize(0);
        }
    }
    _source_element_dose_variance.clear();
    
    // Creating the variance matrix
    _source_element_dose_variance.resize(_num_sources);
    for (unsigned int i = 0; i < _num_sources; i++)
    {
        _source_element_dose_variance[i].resize(_actual_mesh_size);
        
        int actual_tet_idx = -1;
        for (unsigned int j = 0; j < _mesh_size; j++)
        {
            if (!_tetra_to_consider[j]) // ignored tetra
                continue; 
            
            actual_tet_idx++; 

            // computing the expected value of the dose first
            double tetra_expected_value = 0; 
            for (unsigned int k = 0; k < num_op_cases; k++)
            {
                tetra_expected_value += _placers[k]->get_source_element_fluence_matrix()[i][j];
            }
            tetra_expected_value = tetra_expected_value/num_op_cases; 

            // Computing the variance
            double tetra_variance = 0.0;
            for (unsigned int k = 0; k < num_op_cases; k++)
            {
                tetra_variance += pow(_placers[k]->get_source_element_fluence_matrix()[i][j] - tetra_expected_value, 2);
            }
            tetra_variance = tetra_variance/(num_op_cases - 1); // N-1 degrees of freedom 

            _source_element_dose_variance[i][actual_tet_idx] = tetra_variance;
        }
        if ((int)_actual_mesh_size != actual_tet_idx+1)
        {
            cerr << endl << "ERROR in computing dose variance matrix. Exiting...." << endl;
            exit (-1);
        }
    }
}

vector<vector<Src_Abstract*> > & pdt_plan::get_placement_results() {
    return _placement_results;
}

//! This method creates and runs an SA engine for source placement
void pdt_plan::run_sa_engine()
{
    profiler SA("SA optimization"); 
    for (unsigned i = 0; i < _placement_results.size(); i++) {
        for (unsigned j = 0; j < _placement_results[i].size(); j++) {
            if (_placement_results[i][j]) delete _placement_results[i][j];
        }
    }
    _placement_results.clear();
    
    sa_engine* annealer = new sa_engine(_fparser, _fullmonte_engine, _data, 
                        _d_min_orig, _d_max_orig, _original_tumor_indices, _tumor_volume, 
                        _file_path_override, _sa_engine_early_terminate, this);
    if (!_constrain_injection_point) {
        annealer->run_sa_engine(-1);

        // TODO: update to save up to 5 best placements
        cout << "Finished running SA." << endl;

        _initial_placement = annealer->get_placement_solution(); 
        run_fullmonte_multiple_sources(_initial_placement); 

        _placement_results.push_back(_initial_placement);
    } else {
        for (unsigned j = 0; j < _num_injection_points; j++) {
            annealer->run_sa_engine(j);

            // TODO: update this for multiple injection points
            cout << "Finished running SA." << endl;

            _initial_placement = annealer->get_placement_solution(); 
            run_fullmonte_multiple_sources(_initial_placement); 

            _placement_results.push_back(_initial_placement);
        }
    } 

    delete annealer;
}

//! This method returns an annealer for unit testing purposes
sa_engine* pdt_plan::get_annealer_for_testing() {
    sa_engine* annealer = new sa_engine(_fparser, _fullmonte_engine, _data, 
                        _d_min_orig, _d_max_orig, _original_tumor_indices, _tumor_volume, 
                        _file_path_override, _sa_engine_early_terminate, this);
    
    return annealer;
}


bool pdt_plan::get_constrain_injection_point(){
    return _constrain_injection_point;
}
unsigned pdt_plan::get_num_injection_points(){
    return _num_injection_points;
}

//! This method either runs SA or reads the initial placement and then runs FM to generate dose matrix
void pdt_plan::get_diffuser_placement_and_dose_matrix() 
{
    if (_running_tests) {
        return; 
    }

    if (_run_sa) {
        run_sa_engine();
    }
    else {
        read_initial_placement(); 
        
//         run_rt_feedback(); 

        if (_fullmonte_output_file != "") {
            read_fullmonte_output_matrix(); 
        }
        else {
            run_fullmonte_multiple_sources(get_initial_placement()); 
        }
    }

    _final_source_powers.resize(_initial_placement.size(), 0.0);
}

//!
//! This method runs a large number of virtual sources, allocates the power across them
//!     then it starts eliminating least power sources and re-optimizes.
//! The algorithm generates multiple plans the trades-off number of sources to plan quality
//! @param num_plans [in]: number of desired plans
//! @param thresholds [in]: vector of dose thresholds
//! @return Returns a vector of vectors of size equal to num_plans. Each vector is the placement of sources of the corresponding plan.
//!
std::vector<std::vector<Src_Abstract*>> pdt_plan::run_virtual_source_reduction(unsigned num_plans, 
                    const std::vector<double> & thresholds)
{
    profiler virtual_profile("Running virtual source reduction"); 

    if (_placement_type != "virtual") {
        fprintf(stderr, "\033[1;%dmpdt_plan::run_virtual_source_reduction::Running light source reduction algorithm while "
                   "placement_type is not virtual. Exiting...\033[0m\n", 31);
        exit(-1);
    }

    /* The method assumes that you already run Fullmonte with the virtual sources */

    unsigned num_sources_chosen = 51; // We first choose the highest 51 power sources and then eliminate one at a time. 

    if (num_sources_chosen > _num_sources || num_plans > num_sources_chosen)
    {
        fprintf(stderr, "\033[1;%dmpdt_plan::run_virtual_source_reduction::Number of chosen sources is greater than "
                    "the number of virtual sources. Exiting.....\033[0m\n", 31);
        exit(-1);
    }
    
    // We don't need to reallocate
    bool old_vary_tumor_weight = _vary_tumor_weight;
    _vary_tumor_weight = false;

    // 1- Run Power allocation on virtual sources
    initialize_lp();
    solve_lp(thresholds); 

    
    // 2- Copy the original fluence matrix and sources
    vector<vector<float>> old_source_fluence_matrix(_num_sources); 
    for (unsigned i = 0; i < _num_sources; i++) {
        old_source_fluence_matrix[i].resize(_placer->get_source_element_fluence_matrix()[i].size());
        for (unsigned j = 0; j < _placer->get_source_element_fluence_matrix()[i].size(); j++) {
            old_source_fluence_matrix[i][j] = _placer->get_source_element_fluence_matrix()[i][j]; 
        }
    }
    vector<Src_Abstract*> old_source_placement = _initial_placement;

    // 3- Start elimination loop
    vector<vector<Src_Abstract*>> new_plans(num_plans+1);
    new_plans[0] = old_source_placement;
    unsigned curr_plan_idx = 1;
    for (unsigned new_num_sources = num_sources_chosen; new_num_sources >= 1; new_num_sources--)
    {
        vector<double> curr_source_powers = get_final_source_powers(); 

        // sort the source powers 
        std::vector<std::pair<unsigned, double>> power_idx_pairs(curr_source_powers.size()); 
        for (unsigned i = 0; i < curr_source_powers.size(); i++) {
            power_idx_pairs[i].first = i;
            power_idx_pairs[i].second = curr_source_powers[i];
        }
        std::sort(power_idx_pairs.begin(), power_idx_pairs.end(), [] (const std::pair<unsigned, double> & p1, 
                    const std::pair<unsigned, double> & p2) -> bool { return p1.second > p2.second; });

        
        // building the new matrix based on the new sources chosen
        vector<vector<float>> new_source_fluence_matrix; 
        vector<Src_Abstract*> new_source_placement;
        for (unsigned i = 0; i < new_num_sources; i++) {
            new_source_fluence_matrix.push_back(old_source_fluence_matrix[power_idx_pairs[i].first]);

            new_source_placement.push_back(old_source_placement[power_idx_pairs[i].first]);
        }
            
        if (new_num_sources <= num_plans)
        {
            new_plans[curr_plan_idx] = new_source_placement;
            curr_plan_idx++; 
        }

        cout << "Number of sources for re-optimization is: " << new_num_sources << endl;

        // initialize new lp
        initialize_lp(new_num_sources, _INFINITY_, new_source_fluence_matrix); 

        // Solve lp 
        solve_lp(thresholds); 

        // Re-initialize the source fluence matrix and sources vector
        old_source_placement.clear(); 
        old_source_placement = new_source_placement;

        for (unsigned i = 0; i < old_source_fluence_matrix.size(); i++) {
            old_source_fluence_matrix[i].clear();
        }
        old_source_fluence_matrix.clear(); 

        old_source_fluence_matrix.resize(new_num_sources); 
        for (unsigned i = 0; i < new_num_sources; i++) 
        {
            old_source_fluence_matrix[i] = new_source_fluence_matrix[i]; 
        }
    } // end elimination loop

    _vary_tumor_weight = old_vary_tumor_weight;

    return new_plans;
}

//! This method runs the problem with power variation
void pdt_plan::power_variation_problem(double weight, double uncertainty, vector<double>& thresholds)
{
    printf("\nRunning power variation...\n");
    _num_packets *= 2;
    _power_variance_optimization = true;
    _power_variance_weight = weight;
    _sigma_power = uncertainty/2.0;

    // approach 2: run lp
    //initialize_lp();
    solve_lp(thresholds);

    _power_variance_optimization = false;
    _num_packets /= 2;
}

// Getters

//! This method returns the final source powers after optimizations
vector<double> & pdt_plan::get_final_source_powers()
{
    return _final_source_powers;
}

//! This method returns the final fluence distribution of the mesh
vector<double> & pdt_plan::get_final_fluence()
{
    return _final_fluence;
}

//! This method returns the vector that stores the lp costs after optimization (usually it is of size 1 unless we are running the LSR algorithm)
vector<double> & pdt_plan::get_lp_cost()
{
    return _lp_cost;
}

//! This method returns the vector that stores the lp runtimes after optimization (usually it is of size 1 unless we are running the LSR algorithm)
vector<double> & pdt_plan::get_lp_runtime()
{
    return _lp_runtime;
}

//! This method returns the vector of max thresholds for all elements
vector<double> & pdt_plan::get_d_max_vec()
{
    return _d_max;
}

//! This method returns the vector of min thresholds for all elements
vector<double> & pdt_plan::get_d_min_vec()
{
    return _d_min;
}

//! This method returns the slopes vector
vector<double> & pdt_plan::get_slopes()
{
    return _slopes;
}

//! This method returns a vector of max thresholds per tissue type (size is the same as number of materials)
vector<double> & pdt_plan::get_d_max_tissues()
{
    return _d_max_tissues;
}

//! This method returns a vector of min thresholds per tissue type (size is the same as number of materials)
vector<double> & pdt_plan::get_d_min_tissues()
{
    return _d_min_tissues;
}

//! This method returns a vector of safety multipliers per tissue type (size is the same as number of materials)
vector<double> & pdt_plan::get_guardband_tissues()
{
    return _guardband_tissues;
}

//! This method returns the volume of each tetra before ignoring the unnecessary tetra
vector<double> & pdt_plan::get_original_tetra_volumes()
{
    return _original_tetra_volumes;
}

//! This method returns the volume of each tetra after ignoring the unnecessary tetra
vector<double> & pdt_plan::get_tetra_volumes()
{
    return _tetra_volumes;
}

//! This method returns the weight of each tetra considered in the optimization
vector<double> & pdt_plan::get_tetra_weights()
{
    return _tetra_weights;
}

//! This method returns a vector of the material ids of all tetras in the mesh
vector<unsigned int> & pdt_plan::get_original_tetra_material_ids()
{
    return _original_tet_material_ids;
}    

//! This method returns a vector of the material ids of the tetras considered in the optimization
vector<unsigned int> & pdt_plan::get_tetra_material_ids()
{
    return _tet_material_ids;
}

//! This method returns a vector of size _mesh_size that indicates if a tetra is considered in the optimization or not
vector<bool> & pdt_plan::get_tetra_to_consider()
{
    return _tetra_to_consider;
}

//! This method returns a vector of the initial placement read from file 
vector<Src_Abstract*> & pdt_plan::get_initial_placement()
{
    return _initial_placement;
}

// //! This method returns a vector of the cut-end fibers initial directions read from a file
// vector<SP_Point> & pdt_plan::get_cut_end_dirs()
// {
//     return _cut_end_dirs;
// }

// //! This method returns a vector of the cut-end fibers radii read from file 
// vector<double> & pdt_plan::get_cut_end_radii()
// {
//     return _cut_end_radii;
// }

// //! This method returns a vector of the cut-end fibers NA read from file 
// vector<double> & pdt_plan::get_cut_end_na()
// {
//     return _cut_end_na;
// }

//! This method returns a vector of indices of tumor elements in the mesh after ignoring unnecessary tetra
vector<unsigned int> & pdt_plan::get_tumor_indices()
{
    return _tumor_indices;
}

//! This method returns the number of photon packets used in FullMonte
double pdt_plan::get_num_packets()
{
    return _num_packets;
}

//! This method returns the total energy used which is read from the params file
double pdt_plan::get_total_energy()
{
    return _total_energy;
}

//! This method returns the index of the tumor region (usually it is the highest index of the material indices in the mesh)
unordered_set<unsigned> pdt_plan::get_tumor_region_number()
{
    return _tumor_region_number;
}

//! This method returns the mesh size in terms of number of tetrahedra 
unsigned int pdt_plan::get_mesh_size()
{
    return _mesh_size;
}

//! This method returns the mesh size after removing the unnecessary tetra
unsigned int pdt_plan::get_actual_mesh_size()
{
    return _actual_mesh_size;
}

//! This method returns the number of sources to place
unsigned int pdt_plan::get_num_sources()
{
    return _num_sources;
}

//! This method returns a pointer to the placer object
src_placer* pdt_plan::get_placer()
{
    return _placer;
}

//! This method returns a pointer to the mesh 
mesh* pdt_plan::get_mesh()
{
    return _data;
}

//! This method returns the tumor volume. It should be called after the compute_original_tetra_data function
double pdt_plan::get_tumor_volume()
{
    return _tumor_volume;
}

//! This method returns the tumor weight. 
double pdt_plan::get_tumor_weight()
{
    return _tumor_weight; 
}

//! This method returns the mesh volume. It should be called after the compute_original_tetra_data function
double pdt_plan::get_total_mesh_volume()
{
    return _total_mesh_volume;
}

//! This method returns the center of the rectangular volume containing the tumor volume (NOT the centroid)
SP_Point pdt_plan::get_tumor_center() 
{
    return _tumor_center;
}

//! This method returns the total power from the optimizer
double pdt_plan::get_total_power()
{
    return _total_power;
}

//! This method returns the a map from tissue names to their IDs (1-based index)
unordered_map<unsigned, string>& pdt_plan::get_tissue_names() 
{
    return _tissue_names_ids;
}

//! This method returns the number of sources with non-zero power after optimization
unsigned int pdt_plan::get_num_non_zero_sources()
{
    return _num_non_zero_sources;
}

//! This method returns the source type inputted by the user
string pdt_plan::get_source_type()
{
    return _source_type;
}

//! This method returns the placement type inputted by the user
string pdt_plan::get_placement_type()
{
    return _placement_type;
}

//! This method returns the file path and name of the fullmonte vtk output that is read from the input parameters to pdt-space
string pdt_plan::get_fullmonte_output_file()
{
    return _fullmonte_output_file;
}

//! This method returns the wavelength inputted by the user 
string pdt_plan::get_wavelength()
{
    return _wavelength;
}

//! This method returns a boolean variable indicating whether the sources emission profiles are tailored or not depending on the user input
bool pdt_plan::is_source_tailored()
{
    return _source_tailored;
}

//! This method returns the number of iterations needed to converge to target tumor v100
int pdt_plan::get_num_iterations()
{
    return _num_lp_iterations; 
}

//! This method indicates if we are running regression tests or not
bool pdt_plan::is_running_tests() 
{
    return _running_tests;
}

//! This method returns the parameters file
string pdt_plan::get_params_file()
{
    return _params_file;
}

// Setters
//! This method resizes the _p_max vector to _num_sources and initializes it with p_value
void pdt_plan::set_p_max_vector(double p_value)
{
    _p_max.clear();
    _p_max.resize(_num_sources);
    fill(_p_max.begin(), _p_max.end(), p_value);

    _is_p_max_set = true;
}

//! This method sets the number of packets used to run FM
void pdt_plan::set_num_packets(double num_packets)
{
    _num_packets = num_packets;
}

//! This method sets the tumor weight to a new weight 
void pdt_plan::set_tumor_weight(double weight)
{
    _tumor_weight = weight; 
}

//! This method enables or disables varying the tumor weight to converge to 98%
void pdt_plan::vary_tumor_weight(bool vary)
{
    _vary_tumor_weight = vary;
}

//! This method sets the target tumor v100
void pdt_plan::set_target_tumor_v100(double target)
{
    _target_tumor_v100 = target;
}

//! This method sets the upper and lower bounds on the target tumor v100 desired 
void pdt_plan::set_tumor_v100_bounds(double lower, double upper)
{
    _lower_tumor_v100_bound = lower;
    _upper_tumor_v100_bound = upper;
}

//! This method sets the maximum number of lp iterations to terminate early
void pdt_plan::set_max_num_lp_iterations(int num)
{
    _max_num_lp_iterations = num; 
}

//! This method sets whether early termination of SA engine is required
void pdt_plan::set_sa_early_termination()
{
    _sa_engine_early_terminate = true;
}


// Helper Methods

//! This method reads the parameters file and initializes the corresponding parameters map
void pdt_plan::read_params_file()
{
    _params_to_values_map = _fparser->get_params();

    // Reading if data is read from the vtk file
    string read_vtk = _params_to_values_map["read_vtk"];
    transform(read_vtk.begin(), read_vtk.end(), read_vtk.begin(), ::tolower);
    if (read_vtk == "true") 
        _read_data_from_vtk = true;
    else 
        _read_data_from_vtk = false;

    // Setting the wavelength
    string wavelength = _params_to_values_map["wavelength"];

    for (char const & c : wavelength) {
        if (std::isdigit(c) == 0) {
            fprintf(stderr, "\033[1;%dmInvalid wavelength: %s. Exiting...\033[0m\n", 31, wavelength);
            exit(-1);
        }
    }
    stringstream wavelength_ss;
    wavelength_ss << wavelength << "nm";
    _wavelength = wavelength_ss.str();

    // Reading the source and placement types
    _source_type = _fparser->get_source_type();
    transform(_source_type.begin(), _source_type.end(), _source_type.begin(), ::tolower);
    if (_source_type == "mixed") {
        fprintf(stderr, "\033[1;%dmWarning: Mixed source types.\033[0m\n", 33);
    } else if (_source_type != "point" && _source_type != "line" && _source_type != "tailored" && _source_type != "cut-end") {
        fprintf(stderr, "\033[1;%dmUnsupported source type: %s. Exiting...\033[0m\n", 31, _source_type);
        exit(-1);
    }

    _placement_type = _params_to_values_map["placement_type"]; // tells whether we are using fixed or virtual sources
    transform(_placement_type.begin(), _placement_type.end(), _placement_type.begin(), ::tolower);
    if (_placement_type != "fixed" && _placement_type != "virtual" && _placement_type != "sa") {
    
        fprintf(stderr, "\033[1;%dmUnsupported placement type: %s. Exiting...\033[0m\n", 31, _placement_type);
        exit(-1);
    }

    // set if tailored
    _source_tailored = _fparser->get_source_tailored();

    // TODO: check if empty 
    // _data_name = _params_to_values_map["DATA_NAME"];
    // _data_dir_path = _params_to_values_map["DATA_DIR"];
    // _optical_prop_file = _params_to_values_map["OPTICAL_FILE"]; 
    _mesh_file = _params_to_values_map["mesh_file"];

    // TODO: check if empty 
    _fullmonte_output_file = _params_to_values_map.find("fm_output_file") != _params_to_values_map.end() ?
                        _params_to_values_map["fm_output_file"] : "";
    
    // TODO: check if empty 
    _final_distribution_output_file = _params_to_values_map.find("final_dist_file") != _params_to_values_map.end() ?
                        _params_to_values_map["final_dist_file"] : "";
   
    // TODO: check if these are not numbers 
    _num_packets = atof(_params_to_values_map["num_packets"].c_str());
    if (_params_to_values_map.find("pnf") != _params_to_values_map.end()) {
        _total_energy = atof(_params_to_values_map["pnf"].c_str());
    } else {
        _total_energy = 4e11;
    }
    _tumor_weight = atof(_params_to_values_map["tumor_weight"].c_str());

    _placer->set_num_packets(_num_packets);
    _placer->set_total_energy(_total_energy);

    if (_params_to_values_map.find("run_tests") != _params_to_values_map.end()) {
        string run_tests = _params_to_values_map["run_tests"];
        transform(run_tests.begin(), run_tests.end(), run_tests.begin(), ::tolower);

        _running_tests = run_tests == "true" ? true : false;
    }
    else {
        _running_tests = false;
    }

    if (_params_to_values_map.find("USE_CUDA") != _params_to_values_map.end()) {
        string use_cuda = _params_to_values_map["USE_CUDA"];
        transform(use_cuda.begin(), use_cuda.end(), use_cuda.begin(), ::tolower);

        _use_cuda = use_cuda == "true" ? true : false;

        if (!USE_CUDA && _use_cuda) {
            fprintf(stderr, "\033[1;%dmCuda not supported, using CPU instead...\033[0m\n", 31);
            _use_cuda = false;
        }
    }
    else {
        _use_cuda = false;
    }

    if (_running_tests) _file_path_override = true;
    else _file_path_override = false;

    if (_params_to_values_map.find("override_file_path") != _params_to_values_map.end()) {
        string file_path_override = _params_to_values_map["override_file_path"];
        transform(file_path_override.begin(), file_path_override.end(), file_path_override.begin(), ::tolower);

        _file_path_override = file_path_override == "true" ? true : false;
    }

    _run_sa = _fparser->get_run_sa();

    if (_params_to_values_map.find("run_rt_feedback") != _params_to_values_map.end()) {
        string run_rt_feedback_s = _params_to_values_map["run_rt_feedback"];
        transform(run_rt_feedback_s.begin(), run_rt_feedback_s.end(), run_rt_feedback_s.begin(), ::tolower);

        _run_rt_feedback = run_rt_feedback_s == "true" ? true : false;
    }
    else {
        _run_rt_feedback = false;
    }

    if (_params_to_values_map.find("vary_tumor_weight") != _params_to_values_map.end()) {
        string vary = _params_to_values_map["vary_tumor_weight"];
        transform(vary.begin(), vary.end(), vary.begin(), ::tolower);

        _vary_tumor_weight = (vary == "true") ? true : false;
    } else { // default
        _vary_tumor_weight = true;
    }

    // TODO: check if these are numbers
    if (_params_to_values_map.find("target_tumor_cov") != _params_to_values_map.end()) {
        _target_tumor_v100 = std::abs(atof(_params_to_values_map["target_tumor_cov"].c_str()));
    } else { // default 
        _target_tumor_v100 = 98.0;
    }

    if (_params_to_values_map.find("rt_feedback_seed") != _params_to_values_map.end()) {
        _rt_feedback_rand_seed = atoi(_params_to_values_map["rt_feedback_seed"].c_str());
    } else {
        _rt_feedback_rand_seed = 5; // Default value
    }

    if (_params_to_values_map.find("rt_feedback_det_rad") != _params_to_values_map.end()) {
        _rt_feedback_detector_radius = static_cast<float>(atof(_params_to_values_map["rt_feedback_det_rad"].c_str()));
    } else {
        _rt_feedback_detector_radius = 0.5f; // Default value
    }
    
    if (_params_to_values_map.find("rt_feedback_det_na") != _params_to_values_map.end()) {
        _rt_feedback_detector_na = static_cast<float>(atof(_params_to_values_map["rt_feedback_det_na"].c_str()));
    } else {
        _rt_feedback_detector_na = 0.5f; // Default value
    }

    if (_params_to_values_map.find("rt_feedback_lut_file") != _params_to_values_map.end()) {
        _rt_feedback_lut_file_name = _params_to_values_map["rt_feedback_lut_file"]; 
    } else {
        _rt_feedback_lut_file_name = "";
    }

    // check if a number
    if (_params_to_values_map.find("rt_feedback_lut_size") != _params_to_values_map.end()) {
        _rt_feedback_lut_size = atoi(_params_to_values_map["rt_feedback_lut_size"].c_str()); 
    } else {
        _rt_feedback_lut_size = 50; 
    }
}

/**
 * This method runs FullMonteSW with one source at a time with unit power to generate the dose matrix
 * @param source [in]: The sources to run FullMonte with 
 */
void pdt_plan::run_fullmonte_multiple_sources(const vector<Src_Abstract*> & sources)
{
    profiler fm_engine("Running FM one source at a time"); 
    // create a vector of sources 
    std::vector<Source::Abstract*> srcs(sources.size()); 
    for (unsigned i = 0; i < sources.size(); i++) {
        srcs[i] = sources[i]->get_fm_source();
    }

    for (unsigned i = 0; i < _placer->get_source_element_fluence_matrix().size(); i++)
        _placer->get_source_element_fluence_matrix()[i].clear(); 
    _placer->get_source_element_fluence_matrix().clear(); 

    // default 10^6 packets
    _fullmonte_engine->run_multiple_fullmonte(srcs, _placer->get_source_element_fluence_matrix(), false, "", 
            static_cast<unsigned>(_num_packets)); 
    _num_sources = static_cast<unsigned>(_placer->get_source_element_fluence_matrix().size()); 

    // memory cleanup
    for (unsigned i = 0; i < srcs.size(); i++) {
        if (srcs[i]) delete srcs[i];
    }
}

//! This method runs a composite source of all sources in _initial_placement vector
void pdt_plan::run_fullmonte_composite_source(const vector<float> & src_powers)
{
    profiler fm_engine("Running FM composite source"); 

    if (_final_distribution_output_file == "") {
        return;
    }

    // create a vector of sources 
    std::vector<Source::Abstract*> srcs(_initial_placement.size()); 
    for (unsigned i = 0; i < _initial_placement.size(); i++) {
        srcs[i] = _initial_placement[i]->get_fm_source();
        srcs[i]->power(src_powers[i]);
    }

    // default 10^6 packets
    vector<float> fluence; 
    _fullmonte_engine->run_composite_fullmonte(srcs, fluence, true, _final_distribution_output_file, 
            static_cast<unsigned>(_num_packets*static_cast<double>(_initial_placement.size()))); 

    // memory cleanup
    for (unsigned i = 0; i < srcs.size(); i++) {
        if (srcs[i]) delete srcs[i];
    }
}


/**
 * This method runs FullMonteSW with one single source with unit power to generate one column of the dose matrix
 * @param sources [in]: The full list of sources to run FullMonte with 
 * @param idx [in]: The index of the specific source to be run 
 */
void pdt_plan::run_fullmonte_single_source(const vector<Src_Abstract*> & sources, unsigned idx)
{
    profiler fm_engine("Running FM for one source"); 
    // create a vector of sources 
    Source::Abstract* src = sources[idx]->get_fm_source(); 

    // for (unsigned i = 0; i < _placer->get_source_element_fluence_matrix().size(); i++)
    //     _placer->get_source_element_fluence_matrix()[i].clear(); 
    // _placer->get_source_element_fluence_matrix().clear(); 

    // default 10^6 packets
    _fullmonte_engine->run_single_fullmonte(idx, src, _placer->get_source_element_fluence_matrix(), false, "", 
            static_cast<unsigned>(_num_packets)); 
    _num_sources = static_cast<unsigned>(_placer->get_source_element_fluence_matrix().size()); 

    // memory cleanup
    if (src) delete src;
}

/**
 * This method backups one column of the fluence matrix before computing single source fluence, should be called before 
 * running run_fullmonte_single_source
 * @param idx [in]: The index of the specific source changed
 */
void pdt_plan::backup_matrix_single_source(unsigned idx)
{
    profiler matrix_backup("Backing up fluence matrix (1 column)");
    _fluence_matrix_backup_called = true; 

    _fluence_matrix_column_backup.resize(_placer->get_source_element_fluence_matrix()[idx].size());

    for (unsigned i = 0; i < _placer->get_source_element_fluence_matrix()[idx].size(); i++) {
        _fluence_matrix_column_backup[i] = _placer->get_source_element_fluence_matrix()[idx][i];
    }
    // _fluence_matrix_column_backup = _placer->get_source_element_fluence_matrix()[idx];
}

/**
 * This method restores one column of the fluence matrix if single source move is not accepted
 * @param idx [in]: The index of the specific source changed
 */
void pdt_plan::restore_matrix_single_source(unsigned idx)
{
    if (!_fluence_matrix_backup_called) {
        fprintf(stderr, "\n\033[1;%dmWarning: did not call backup_matrix_single_source before restore_matrix_single_source. Not restoring.\033[0m\n", 35);
    }
    profiler matrix_backup("Restoring fluence matrix (1 column)");
    for (unsigned i = 0; i < _fluence_matrix_column_backup.size(); i++) {
        _placer->get_source_element_fluence_matrix()[idx][i] = _fluence_matrix_column_backup[i];
    }
    _fluence_matrix_backup_called = false;
}

/**
 * This method backups the fluence matrix before computing multi source fluence, should be called before 
 * running run_fullmonte_multiple_source
 */
void pdt_plan::backup_matrix()
{
    profiler matrix_backup("Backing up fluence matrix (all)");
    _fluence_matrix_backup_called = true; 

    vector<vector<float> >& fluence_matrix = _placer->get_source_element_fluence_matrix();
    _fluence_matrix_backup.resize(fluence_matrix.size(), vector<float>());
    for (unsigned i = 0; i < fluence_matrix.size(); i++) {
        _fluence_matrix_backup[i].resize(fluence_matrix[i].size(), 0.0f);
        for (unsigned j = 0; j < fluence_matrix[i].size(); j++) {
            _fluence_matrix_backup[i][j] = fluence_matrix[i][j];
        }
    }
}

/**
 * This method restores the fluence matrix if multi source move is not accepted
 */
void pdt_plan::restore_matrix()
{
    if (!_fluence_matrix_backup_called) {
        fprintf(stderr, "\n\033[1;%dmWarning: did not call backup_matrix before restore_matrix. Not restoring.\033[0m\n", 35);
    }
    profiler matrix_backup("Restoring fluence matrix (all)");
    // _placer->get_source_element_fluence_matrix() = _fluence_matrix_backup;
    vector<vector<float> >& fluence_matrix = _placer->get_source_element_fluence_matrix();
    fluence_matrix.resize(_fluence_matrix_backup.size(), vector<float>());
    for (unsigned i = 0; i < _fluence_matrix_backup.size(); i++) {
        fluence_matrix[i].resize(_fluence_matrix_backup[i].size(), 0.0f);
        for (unsigned j = 0; j < _fluence_matrix_backup[i].size(); j++) {
            fluence_matrix[i][j] = _fluence_matrix_backup[i][j];
        }
    }
    _fluence_matrix_backup_called = false;
}

/**
 * This method should be used to restore the _fluence_matrix_backup_called flag in case restore_matrix is not required
 */
void pdt_plan::reset_matrix_backup_flag()
{
    _fluence_matrix_backup_called = false;
}

//!
//! This method generates a vector of vectors of tetra IDs that enclose the points through which each source passes through,
//! Then it tags the tetra of each source with a new material and generates a new vtk mesh
//!
void pdt_plan::mesh_sources() 
{
 // TODO This is not used anymore
    /* 
     * Step 1: Discretize source
     *         Each source is discretized into 0.2mm elements, and the enclosing tetra is found for each point
     *
     */
    std::vector<std::vector<unsigned>> enclosing_tetra_ids;
    for (auto src : _initial_placement) 
    {
        std::vector<unsigned> src_enclosing_tetra;

        if (src->get_type() == Src_Abstract::Point) // point or cut-end source
        {
            Src_Point* src_p = (Src_Point*)src;
            src_enclosing_tetra.push_back(_fullmonte_engine->get_tetra_enclosing_point(src_p->get_xcoord(), 
                    src_p->get_ycoord(), src_p->get_zcoord()));
        }
        else if (src->get_type() == Src_Abstract::CutEnd) // point or cut-end source
        {
            SP_Point endp = ((Src_CutEnd*)src)->get_endpoint();
            src_enclosing_tetra.push_back(_fullmonte_engine->get_tetra_enclosing_point(endp.get_xcoord(), 
                    endp.get_ycoord(), endp.get_zcoord()));
        }
        else // line source
        {
            Src_Line* src_l = (Src_Line*)src;
            double src_length = src_l->get_src_length();

            // Get direction vector
            SP_Point src_dir = (src_l->end1 - src_l->end0)/src_length;

            double elem_length = 0.2; // mm
            
            unsigned num_pts = static_cast<unsigned>(floor(src_length/elem_length) + 1);
            
            
            // Get num_rand_perp random perpendicular vectors and store them
            unsigned num_rand_perp = 10; 
            std::vector<SP_Point> rand_perp(num_rand_perp); 
            for (unsigned i = 0; i < num_rand_perp; i++) 
            {
                rand_perp[i] = src_l->get_rand_perpendicular_vector(); 
            }

            // Get number of points in perpendicular direction
            double cylinder_radius = 2.5; // assuming 5mm cylindrical diffuser diameter
            unsigned num_pts_diameter = static_cast<unsigned>(floor(cylinder_radius/elem_length) + 1);

            // find enclosing tetra
            for (unsigned i = 0; i < num_pts; i++) 
            {
                SP_Point curr_pt = src_l->end0 + (elem_length*i*src_dir); 
               
                src_enclosing_tetra.push_back(_fullmonte_engine->get_tetra_enclosing_point(curr_pt.get_xcoord(), 
                       curr_pt.get_ycoord(), curr_pt.get_zcoord()));

                // Get num_rand_perp random perpendicular vectors and find enclosing tetra
                for (unsigned j = 0; j < num_rand_perp; j++) 
                {
                    for (unsigned k = 0; k < num_pts_diameter; k++)
                    {
                        SP_Point curr_pt_polar = curr_pt + (elem_length*k*rand_perp[j]); 
                       
                        src_enclosing_tetra.push_back(_fullmonte_engine->get_tetra_enclosing_point(curr_pt_polar.get_xcoord(), 
                               curr_pt_polar.get_ycoord(), curr_pt_polar.get_zcoord()));
                    }
                }
            }
        }
        enclosing_tetra_ids.push_back(src_enclosing_tetra); 
    }


    /*
     * 
     * Step 2: Tag each tetra in enclosing_tetra_ids with new region IDs (only for FullMonte, as it does not affect PDT-SPACE) 
     */
    // Get FullMonte Regions 
    WrapperFullMonteSW* fullmonte_engine_clone = _fullmonte_engine->clone();
    Partition* fm_regions = fullmonte_engine_clone->get_tetra_mesh()->regions(); 

    cout << "Number of regions is: " << fm_regions->count() << endl;

    cout << "Number of sources: " << _initial_placement.size() << endl;
    for (unsigned i = 0; i < enclosing_tetra_ids.size(); i++)
    {
        for (unsigned j = 0; j < enclosing_tetra_ids[i].size(); j++) 
        {
            fm_regions->assign(enclosing_tetra_ids[i][j], _max_region_index + 1);//+ i);
        }
        fm_regions->recountRegions();   
    }
    cout << "Number of regions after: " << fm_regions->count() << endl;
    cout << "Number of materials: " << fullmonte_engine_clone->get_material_set()->size() << endl;

    fullmonte_engine_clone->write_mesh("logs/rt_feedback_results/test_src_mesh.vtk");
    std::exit(0);
}

//! This method prints the tissue names with their v100
void pdt_plan::print_v100s(const std::vector<double>& v100s)
{
    std::cout << std::endl;
    double tumor_volume = 0;
    double coverage_volume = 0;
    for (unsigned i = 1; i <= _d_max_tissues.size(); i++) 
    {
        if (_tumor_region_number.find(i) != _tumor_region_number.end()) {
            stringstream tumor;
            tumor << _tissue_names_ids[i];
            std::cout << std::left << std::setw(20) << std::setfill(' ') << tumor.str();
            tumor_volume += _material_volumes[i];
            coverage_volume += v100s[i - 1] * _material_volumes[i];
            std::cout << std::fixed << std::setprecision(5) << " V100 is: " << v100s[i - 1] << " ";
            std::cout << "%";
        } else {
            std::cout << std::left << std::setw(20) << std::setfill(' ') << _tissue_names_ids[i];
            std::cout << std::fixed << std::setprecision(5) << " V100 is: " << v100s[i - 1] << " ";
            std::cout << "unit_length^3";
        }

        std::cout << std::endl;
    }

    std::cout << std::left << std::setw(20) << std::setfill(' ') << "Total Tumor" 
                << std::fixed << std::setprecision(5) << " V100 is: " << coverage_volume/tumor_volume << " %\n";

    std::cout << std::endl;
}

//! This method appends all directory paths with PDT_SPACE_SRC_DIR
void pdt_plan::override_file_paths () {
    if (_mesh_file != "")
        _mesh_file = string(PDT_SPACE_SRC_DIR) + _mesh_file;
    if (_fullmonte_output_file != "")
        _fullmonte_output_file = string(PDT_SPACE_SRC_DIR) + _fullmonte_output_file;
    if (_final_distribution_output_file != "")
        _final_distribution_output_file = string(PDT_SPACE_SRC_DIR) + _final_distribution_output_file; 
    if (_sa_engine_progress_placement_file != "")
        _sa_engine_progress_placement_file = string(PDT_SPACE_SRC_DIR) + _sa_engine_progress_placement_file;
}

// //!
// //! This method sets the parameters used to read mesh files
// //! @param data_dir_path: [in] Value for _data_dir_path
// //! @param data_name: [in] Name of the mesh file with no extension
// //! @param optical_prop_file: [in] Value for _optical_prop_file
// //! @param read_data_from_vtk: [in] Value for _read_data_from_vtk
// //! @param wavelength: [in] Value for _wavelength
// //!
// void pdt_plan::set_parameters (string data_dir_path, string data_name, 
//         string optical_prop_file, bool read_data_from_vtk, string wavelength) {
//     _data_dir_path = data_dir_path;
//     _data_name = data_name;
//     _optical_prop_file = optical_prop_file;
//     _read_data_from_vtk = read_data_from_vtk;
//     _wavelength = wavelength;
// }

//! 
//! This method acts as a setter for the _params_file and reads it
//! @param params_file: [in] Value for _params_file
//!
void pdt_plan::set_params_file (string params_file) {
    _params_file = params_file;
    if (_placer) delete _placer;
    _placer = new src_placer();
    if (_fparser) delete _fparser;
    _fparser = new Parser(params_file);
    read_params_file();
}

//!
//! This method sets the initial placement vector. This setter is not managing any memory operations on sources
//! @param placement: [in] vector of Src_Abstract* of the source placement to be set to
//!
void pdt_plan::set_initial_placement(vector<Src_Abstract*> placement) {
    _initial_placement = placement;
    _final_source_powers.resize(_initial_placement.size(), 0.0);
}

//!
//! This method sets the final source power vector. Use with caution as it sets the result vector from conv opt
//! @param source_powers: [in] vector of double of the source powers to be set to
//!
void pdt_plan::set_final_source_powers(vector<double> source_powers) {
    _final_source_powers = source_powers;
    _is_lp_solved = true;
}

//!
//! This method resets all source powers to 0. This is used by accurate recompute
//!
void pdt_plan::reset_source_powers() {
    std::fill(_final_source_powers.begin(), _final_source_powers.end(), 0.0);
}

/**
 * This method initializes an RtFeedback object and runs a realtime feedback system to recover optical properties 
 */
void pdt_plan::run_rt_feedback()
{
    if (!_run_rt_feedback) {
        return;
    }

    if (_tumor_region_number.size() > 1) {
        fprintf(stderr, "\033[1;%dmpdt_plan::RtFeedback does not support more than one tumor region ID. Exiting...\033[0m\n", 31); 
        std::exit(-1); 
    }

    // 1- Initialize RtFeedback object
    {
        profiler init_rt("Initializing RtFeedback");
        _rt_feedback_system = new RtFeedback(_fullmonte_engine, _initial_placement, static_cast<unsigned long long>(_num_packets), 
                            *(_tumor_region_number.begin()), _tumor_volume, _rt_feedback_rand_seed, _rt_feedback_detector_radius, 
                                    _rt_feedback_detector_na, _rt_feedback_lut_size); 
    }
    
    std::vector<float> powers(_initial_placement.size(), 1.0f); 
    { 
        profiler LUT_rt("Building mu_eff lookup table"); 
        if (_rt_feedback_lut_file_name == "") {
            _rt_feedback_system->build_lut(_initial_placement, powers); // TODO: make it an option
        } else {
            _rt_feedback_system->build_lut_from_file(_rt_feedback_lut_file_name, _initial_placement.size()); 
        }
    }

//         _rt_feedback_system->prepare_training_data(_initial_placement); // TODO: make it an option 
    {
        profiler prof_run_rt("Runnning RT Feedback System"); 
        unsigned num_simulations = 25; // 25 recoveries per tumor 

        // 2- Define loop criterion 
        vector<double> thresholds = get_thresholds_with_guardband(); 
        for (unsigned i = 0; i < num_simulations; i++) {

            // 3- While criterion not met, call recover_tumor_optical_properties, and re-optimize powers

            // 3- While criterion not met, call recover_tumor_optical_properties, and re-optimize powers


        
            float new_mu_a, new_mu_s; 
            _rt_feedback_system->recover_tumor_optical_properties(_initial_placement, powers, new_mu_a, new_mu_s); 

            run_fullmonte_multiple_sources(_initial_placement);
            remove_unnecessary_tetra(thresholds); 
            compute_tetra_data(thresholds); 
            initialize_lp(); 
            solve_lp(thresholds); 
            compute_post_opt_fluence();
            vector<double> v_alpha = compute_v_alpha_tissues(thresholds, 1.0); 

            std::cout << "Simulation: " << i << " V100s post op recovery:" << std::endl;
            print_v100s(v_alpha); 
            std::cout << "Simulation: "  << i << " Printing source powers after optimization post op recovery" << std::endl;
            for (unsigned int j = 0; j < _final_source_powers.size(); j++) {
                std::cout << "x[" << j << "] = " << std::fixed << std::setprecision(8) << _final_source_powers[j] << std::endl;
            }
        }
    }
}
