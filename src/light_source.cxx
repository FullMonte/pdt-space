#include "light_source.h"

std::ostream& operator<<(std::ostream& os, const Src_Abstract * src) { 
    if (src->get_type() == Src_Abstract::Line) {
        os << *((const SP_Line*) (const Src_Line*) src);
        return os; 
    }
    if (src->get_type() == Src_Abstract::Point) {
        os << *((const SP_Point*) (const Src_Point*) src);
        return os; 
    }
    if (src->get_type() == Src_Abstract::CutEnd) {
        const Src_CutEnd* ce = (const Src_CutEnd*) src;
        os << "Cut-end r " << ce->get_radius() << ", na " << ce->get_na() << std::endl;
        os << "  endpoint " << ce->get_endpoint() << ", direction " << ce->get_dir() << std::endl;
        return os;
    }
    return os; 
}

std::ostream& print_to_xml(std::ostream& os, const vector<Src_Abstract*> &srcs) { 
    if (srcs[0]->get_type() == Src_Abstract::Line) {
        for (unsigned i = 0; i < srcs.size(); i++) {
            Src_Line* l = (Src_Line*) srcs[i];
            os << "    <source type=\"line\">\n";
            os << "      <endpoint location=\"proximal\">\n";
            os << "        <x>" << l->end0.get_xcoord() << "</x>\n";
            os << "        <y>" << l->end0.get_ycoord() << "</y>\n";
            os << "        <z>" << l->end0.get_zcoord() << "</z>\n";
            os << "      </endpoint>\n";
            os << "      <endpoint location=\"distal\">\n";
            os << "        <x>" << l->end1.get_xcoord() << "</x>\n";
            os << "        <y>" << l->end1.get_ycoord() << "</y>\n";
            os << "        <z>" << l->end1.get_zcoord() << "</z>\n";
            os << "      </endpoint>\n";
            os << "    </source>\n";
        }
        return os; 
    }
    if (srcs[0]->get_type() == Src_Abstract::Point) {
        for (unsigned i = 0; i < srcs.size(); i++) {
            Src_Point* pt = (Src_Point*) srcs[i];
            os << "    <source type=\"point\">\n";
            os << "      <endpoint>\n";
            os << "        <x>" << pt->get_xcoord() << "</x>\n";
            os << "        <y>" << pt->get_ycoord() << "</y>\n";
            os << "        <z>" << pt->get_zcoord() << "</z>\n";
            os << "      </endpoint>\n";
            os << "    </source>\n";
        }
        return os; 
    }
    if (srcs[0]->get_type() == Src_Abstract::CutEnd) {
        for (unsigned i = 0; i < srcs.size(); i++) {
            Src_CutEnd* ce = (Src_CutEnd*) srcs[i];
            os << "    <source type=\"cut-end\">\n";
            os << "      <radius>" << ce->get_radius() << "</radius>\n";
            os << "      <na>" << ce->get_na() << "</na>\n";
            os << "      <endpoint>\n";
            os << "        <x>" << ce->get_endpoint().get_xcoord() << "</x>\n";
            os << "        <y>" << ce->get_endpoint().get_ycoord() << "</y>\n";
            os << "        <z>" << ce->get_endpoint().get_zcoord() << "</z>\n";
            os << "      </endpoint>\n";
            os << "      <direction>\n";
            os << "        <x>" << ce->get_dir().get_xcoord() << "</x>\n";
            os << "        <y>" << ce->get_dir().get_ycoord() << "</y>\n";
            os << "        <z>" << ce->get_dir().get_zcoord() << "</z>\n";
            os << "      </direction>\n";
            os << "    </source>\n";
        }
        return os; 
    }
    return os;
} 

// return FullMonte sources from PDT-SPACE sources
Source::Line* Src_Line::get_fm_source() {
    Source::Line * line = new Source::Line(); 
    line->endpoint(0, {end0.get_xcoord(), end0.get_ycoord(), end0.get_zcoord()}); 
    line->endpoint(1, {end1.get_xcoord(), end1.get_ycoord(), end1.get_zcoord()}); 
    return line;
}

Source::Point* Src_Point::get_fm_source() {
    Source::Point * p = new Source::Point(); 
    p->position({_xcoord, _ycoord, _zcoord}); 
    return p; 
}

Source::Fiber* Src_CutEnd::get_fm_source() {
    Source::Fiber * f = new Source::Fiber(); 

    f->fiberPos({_endpoint.get_xcoord(), _endpoint.get_ycoord(), _endpoint.get_zcoord()});
    f->fiberDir({_dir.get_xcoord(), _dir.get_ycoord(), _dir.get_zcoord()}); 
    f->radius(_r);
    f->numericalAperture(_na);
    f->power(1.0); 

    return f; 
}
