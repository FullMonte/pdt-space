/**
 * \deprecarted
 * \file generate_scripts.cxx \brief Generating scripts implementations
 *
 * This source file implements the different methods to generate FullMonte TCL scripts in the src_placer class
 *
 *   @author: Abed Yassine
 *   @date: April 4th, 2017
 *
 *   // TODO Deprecate this
 */

#include "src_placer.h"

/* This function generates a tcl script to run FullMonte with the input source positions given as tetrahedra
 * The script generates a path tracing of the light inside the mesh
 */
void src_placer::make_tcl_script_traces(string out_file_name, string mesh_path, const vector<tetrahedron* > & sources)
                                                    //const vector<Point*> & sources)
{
    fprintf(stderr, "\033[1;%dmCreating a TCL FullMonte Path Tracing Script: %s\033[0m\n", 33, out_file_name.c_str());

    if (sources.size() == 0)
    {
        fprintf(stderr, "\033[1;%dmNo sources provided! Exiting... \033[0m\n", 31);
        exit(-1);
    }

    // Creating output file
    stringstream out_file_ss; 
    out_file_ss << "TCL_scripts/" << out_file_name;
    FILE* outfile = fopen(out_file_ss.str().c_str(), "w");
    
    // Importing VTK TCK Package
    fprintf(outfile, "##### Import VTK TCL Package \n");
    fprintf(outfile, "package require vtk \n \n");

    // Close the default TCL/TK window
    fprintf(outfile, "##### Close the default tcl/tk window\n");
    fprintf(outfile, "wm withdraw .\n\n");

    // Load required packages
    fprintf(outfile, "##### Loading the required packages\n\n");
    
    fprintf(outfile, "##### TIMOS format reader (for Digimouse) with TCL bindings\n");
    fprintf(outfile, "load libFullMonteTIMOSTCL.so\n\n");
    
    fprintf(outfile, "##### Geometry model with TCL bindings\n");
    fprintf(outfile, "load libFullMonteGeometryTCL.so\n\n");

    fprintf(outfile, "##### Software kernels with TCL bindings\n");
    fprintf(outfile, "load libFullMonteSWKernelTCL.so\n");
    fprintf(outfile, "load libFullMonteKernelsTCL.so\n\n");

    fprintf(outfile, "##### Data output manipulation\n");
    fprintf(outfile, "load libFullMonteDataTCL.so\n");
    fprintf(outfile, "load libFullMonteQueriesTCL.so\n\n");

    fprintf(outfile, "##### VTK interface\n");
    fprintf(outfile, "load libvtkFullMonteTCL-6.3.so\n");
    fprintf(outfile, "load libFullMonteVTKFileTCL.so\n\n");

    fprintf(outfile, "puts \"loaded libs\" \n\n");

    // File parameters
    fprintf(outfile, "##### Basic parameters: file name \n\n");

    fprintf(outfile, "#default file prefix\n");
    fprintf(outfile, "set pfx \"%s\"\n\n", mesh_path.c_str());

    fprintf(outfile, "##### override with 1st cmdline arg\n");
    fprintf(outfile, "if { $argc >= 1 } { set pfx [lindex $argv 0] }\n\n");

    // Setting optical properties and mesh files
    fprintf(outfile, "set optfn \"$pfx.opt\"\n");
    fprintf(outfile, "set meshfn \"$pfx.mesh\"\n");
    fprintf(outfile, "set legendfn \"$pfx.legend\"\n");
    fprintf(outfile, "set ofn \"fluence.out\"\n\n");

    // Reading problem definitions in TIMOS format
    fprintf(outfile, "###### Read problem definition in TIMOS format\n");
    fprintf(outfile, "TIMOSAntlrParser R\n\n");

    fprintf(outfile, "R setMeshFileName $meshfn\n");
    fprintf(outfile, "R setOpticalFileName $optfn\n\n");
//     fprintf(outfile, "R setLegendFileName $legendfn\n\n");

    fprintf(outfile, "set mesh [R mesh]\n");
    fprintf(outfile, "set opt [R materials_simple]\n\n");

    
    // Configuring sources
    fprintf(outfile, "##### Configuring Sources\n");
        // The sources are volume elements
//     int tetra_idx; 
    for (unsigned int i = 0; i < sources.size(); i++)
    {
//         tetra_idx = sources[i]->get_id();

        stringstream source_name;
        source_name << "V" << i; 
        
        SP_Point centroid = sources[i]->compute_centroid_of();
        // sources are generated with unit weighy
        //fprintf(outfile, "Volume %s 1.0 %d\n", source_name.str().c_str(), tetra_idx);
        fprintf(outfile, "Ball %s\n", source_name.str().c_str());
        fprintf(outfile, "%s centre \"%g %g %g\"\n", source_name.str().c_str(), 
                centroid.get_xcoord(), centroid.get_ycoord(), centroid.get_zcoord());
        fprintf(outfile, "%s radius 0.5\n", source_name.str().c_str());
    }
    fprintf(outfile, "\n");

    // if number of sources is more than 1, we have to create a composite source
    if (sources.size() > 1)
    {
        fprintf(outfile, "Composite C");
        for (unsigned int i = 0; i < sources.size(); i++)
        {
            stringstream source_name;
            source_name << "V" << i;

            fprintf(outfile, "C add %s\n", source_name.str().c_str());
        }
        fprintf(outfile, "\n");
    }

    // Configuring simulation kernel
    fprintf(outfile, "##### Create and configure simulation kernel with surface scoring\n");

    fprintf(outfile, "TetraTraceKernel k $mesh\n\n");

    string source_name;
    if (sources.size() == 1)
    {
        source_name = "V0";
    }
    else
    {
        source_name = "C";
    }

    fprintf(outfile, "k source %s\n     # the source to launch from \n", source_name.c_str());
    fprintf(outfile, "k energy 1\n      # total energy\n"); // Candidate sources are all of unit power
    fprintf(outfile, "k materials $opt\n        # materials\n");
    fprintf(outfile, "k setUnitsToMM\n      # units for mesh dimensions "
                    "& optical properties (must match each other)\n");
    
    // Monte carlo kernel properties
    fprintf(outfile, "\n##### Monte Carlo kernel properties\n");
    fprintf(outfile, "k roulettePrWin 0.1\n        # probability of roulette win\n");
    fprintf(outfile, "k rouletteWMin 1e-5\n        # minimum weight "
                "\"wmin\" before roulette takes effect\n");
    fprintf(outfile, "k maxSteps 10000\n        # maximum"
                " number of steps to trace a packet\n");
    fprintf(outfile, "k maxHits 100\n        # maximum number"
                "of boundaries a single step can take\n");
    fprintf(outfile, "k packetCount 1000000\n"       
                    "        # number of packets to simulate"
                    " (more -> better quality, longer run)\n");
    fprintf(outfile, "k threadCount 8\n        # number of threads "
                    "(set to number of cores, or 2x number of cores if hyperthreading\n\n");

    
    // Defining progress timer call back function for use during simulation run
    fprintf(outfile, "##### Define progress timer callback function for use during simulation run\n");

    fprintf(outfile, "\nproc progresstimer {} {\n"
                "   # loop while not finished\n"
                "   while { ![k done] } {\n"
                    "       # display %% completed to 2 decimal places\n"
                    "       puts -nonewline [format \"\\rProgress %%6.2f%%%%\" [expr 100.0*[k progressFraction]]]\n"
                    "       flush stdout\n\n"

                    "       # refresh interval: 200ms\n"
                    "       after 200\n"
                "   }\n"
                "   puts [format \"\\rProgress %%6.2f%%%%\" 100.0]\n"
            "}\n\n");

    // Running the kernel
    fprintf(outfile, "##### Run it\n\n  ##### Run Kernel, display progress timer, and awaut finish\n");
    fprintf(outfile, "  k startAsync\n    progresstimer\n    k finishAsync\n\n");

    stringstream vtk_file_name;
    vtk_file_name << out_file_name.substr(0, out_file_name.length()-4) << "_traces.vtk";
    fprintf(outfile, "for { set i 0 } { $i < [k getResultCount] } { incr i } { puts \""
                            "[[k getResultByIndex $i] typeString]\" }\n\n"
                      "set paths [k getResultByTypeString \"PacketPositionTraceSet\"]\n\n"

                      "puts \"paths=$paths\"\n"
//                       "puts \"Returned a path with [$paths nTraces] traces and a total"
//                         " of [$paths nPoints] points\"\n\n"

                      "vtkFullMontePacketPositionTraceSetToPolyData O\n"
                      "    O source $paths\n"
                      "    O update\n\n"

                      "vtkPolyDataWriter W\n"
                      "    W SetInputData [O getPolyData]\n"
                      "    W SetFileName \"%s\"\n"
                      "    W Update\n"
                      "    W Delete\n"
                      "exit", vtk_file_name.str().c_str());

    // closing the file
    fclose(outfile);
}

/* This function generates a tcl script to run FullMonte with the input source positions given as tetrahedra
 * The script outputs the amount of energy absorbed inside each tetrahedron
 */
void src_placer::make_tcl_script_absorption_multiple_sources(string out_file_name, 
            string mesh_path, const vector<tetrahedron* > & sources,
            const vector<float>& source_powers_to_run, 
            bool read_mesh_from_vtk) //const vector<Point*> & sources)
{
    fprintf(stderr, "\033[1;%dm\nCreating a TCL FullMonte Absorption Script: %s\033[0m\n", 33, out_file_name.c_str());

    if (sources.size() == 0)
    {
        fprintf(stderr, "\033[1;%dmNo sources provided! Exiting... \033[0m\n", 31);
        exit(-1);
    }

    // Creating output file
    stringstream out_file_ss; 
    out_file_ss << "TCL_scripts/" << out_file_name;
    FILE* outfile = fopen(out_file_ss.str().c_str(), "w");
    
    // Importing VTK TCK Package
    fprintf(outfile, "##### Import VTK TCL Package \n");
    fprintf(outfile, "package require vtk \n \n");

    // Close the default TCL/TK window
    fprintf(outfile, "##### Close the default tcl/tk window\n");
    fprintf(outfile, "wm withdraw .\n\n");

    // Load required packages
    fprintf(outfile, "##### Loading the required packages\n\n");
    
    fprintf(outfile, "##### TIMOS format reader (for Digimouse) with TCL bindings\n");
    fprintf(outfile, "load libFullMonteTIMOSTCL.so\n\n");
    
    fprintf(outfile, "##### Geometry model with TCL bindings\n");
    fprintf(outfile, "load libFullMonteGeometryTCL.so\n\n");

    fprintf(outfile, "##### Software kernels with TCL bindings\n");
    fprintf(outfile, "load libFullMonteSWKernelTCL.so\n");
    fprintf(outfile, "load libFullMonteKernelsTCL.so\n\n");

    fprintf(outfile, "##### Data output manipulation\n");
    fprintf(outfile, "load libFullMonteDataTCL.so\n");
    fprintf(outfile, "load libFullMonteQueriesTCL.so\n\n");

    fprintf(outfile, "##### VTK interface\n");
    fprintf(outfile, "load libvtkFullMonteTCL-6.3.so\n");
    fprintf(outfile, "load libFullMonteVTKFileTCL.so\n\n");

    fprintf(outfile, "puts \"loaded libs\" \n\n");

    // File parameters
    fprintf(outfile, "##### Basic parameters: file name \n\n");

    fprintf(outfile, "#default file prefix\n");
    fprintf(outfile, "set pfx \"%s\"\n\n", mesh_path.c_str());

    fprintf(outfile, "##### override with 1st cmdline arg\n");
    fprintf(outfile, "if { $argc >= 1 } { set pfx [lindex $argv 0] }\n\n");

    // Setting optical properties and mesh files
    fprintf(outfile, "set optfn \"$pfx.opt\"\n");
    if (read_mesh_from_vtk)
        fprintf(outfile, "set meshfn \"$pfx.vtk\"\n");
    else
        fprintf(outfile, "set meshfn \"$pfx.mesh\"\n");
    fprintf(outfile, "set legendfn \"$pfx.legend\"\n");
    fprintf(outfile, "set ofn \"fluence.out\"\n\n");


    // Reading problem definitions in TIMOS or VTK format
    if (read_mesh_from_vtk)
    {
        fprintf(outfile, "#### Read Problem Mesh in VTK fromat\n");
        fprintf(outfile, "VTKLegacyReader VTKR\n"
                         "      VTKR setFileName $meshfn\n"
                         "      VTKR addZeroPoint 1\n"
                         "      VTKR addZeroCell 1\n");

        fprintf(outfile, "set MB [VTKR mesh]\n"
                         "set mesh [TetraMesh foo $MB]\n");
    }
    fprintf(outfile, "###### Read problem definition in TIMOS format\n");
    fprintf(outfile, "TIMOSAntlrParser R\n\n");

    if (!read_mesh_from_vtk)
        fprintf(outfile, "R setMeshFileName $meshfn\n");
    fprintf(outfile, "R setOpticalFileName $optfn\n\n");
//     fprintf(outfile, "R setLegendFileName $legendfn\n\n");

    if (!read_mesh_from_vtk)
        fprintf(outfile, "set mesh [R mesh]\n");
    fprintf(outfile, "set opt [R materials_simple]\n\n");

    
    // Configuring sources
    fprintf(outfile, "##### Configuring Sources\n");
        // The sources are volume elements
//     int tetra_idx; 
    assert(sources.size() == source_powers_to_run.size());
    for (unsigned int i = 0; i < sources.size(); i++)
    {
//         tetra_idx = sources[i]->get_id();

        stringstream source_name;
        source_name << "V" << i; 
        
        SP_Point centroid = sources[i]->compute_centroid_of();
        // sources are generated with unit weight
//         fprintf(outfile, "Volume %s %f %d\n", source_name.str().c_str(), source_powers_to_run[i], tetra_idx);
//         fprintf(outfile, "Volume %s %d\n", source_name.str().c_str(), tetra_idx);

        // Making point sources
        fprintf(outfile, "Point %s\n", source_name.str().c_str());
        fprintf(outfile, "%s position \"%g %g %g\"\n", source_name.str().c_str(), centroid.get_xcoord(),
                centroid.get_ycoord(), centroid.get_zcoord());



//         fprintf(outfile, "Ball %s\n", source_name.str().c_str());
//         fprintf(outfile, "%s centre \"%g %g %g\"\n", source_name.str().c_str(), 
//                 centroid->get_xcoord(), centroid->get_ycoord(), centroid->get_zcoord());
//         fprintf(outfile, "%s radius 0.5\n", source_name.str().c_str());
    }
    fprintf(outfile, "\n");

    // if number of sources is more than 1, we have to create a composite source
    if (sources.size() > 1)
    {
        fprintf(outfile, "Composite C\n");
        for (unsigned int i = 0; i < sources.size(); i++)
        {
            stringstream source_name;
            source_name << "V" << i;

            fprintf(outfile, "C add %f %s\n", source_powers_to_run[i], source_name.str().c_str());
        }
        fprintf(outfile, "\n");
    }

    // Configuring simulation kernel
    fprintf(outfile, "##### Create and configure simulation kernel with surface scoring\n");

    fprintf(outfile, "TetraSVKernel k $mesh\n\n");

    string source_name;
    if (sources.size() == 1)
    {
        source_name = "V0";
    }
    else
    {
        source_name = "C";
    }

    fprintf(outfile, "k source %s\n     # the source to launch from \n", source_name.c_str());
    fprintf(outfile, "k energy 1\n      # total energy\n"); // Candidate sources are all of unit power
    fprintf(outfile, "k materials $opt\n        # materials\n");
    fprintf(outfile, "k setUnitsToMM\n      # units for mesh dimensions "
                    "& optical properties (must match each other)\n");
    
    // Monte carlo kernel properties
    fprintf(outfile, "\n##### Monte Carlo kernel properties\n");
    fprintf(outfile, "k roulettePrWin 0.1\n        # probability of roulette win\n");
    fprintf(outfile, "k rouletteWMin 1e-5\n        # minimum weight "
                "\"wmin\" before roulette takes effect\n");
    fprintf(outfile, "k maxSteps 10000\n        # maximum"
                " number of steps to trace a packet\n");
    fprintf(outfile, "k maxHits 100\n        # maximum number"
                "of boundaries a single step can take\n");
    fprintf(outfile, "k packetCount 1000000\n"       
                    "        # number of packets to simulate"
                    " (more -> better quality, longer run)\n");
    fprintf(outfile, "k threadCount 8\n        # number of threads "
                    "(set to number of cores, or 2x number of cores if hyperthreading\n\n");

    
    // Defining progress timer call back function for use during simulation run
    fprintf(outfile, "##### Define progress timer callback function for use during simulation run\n");

    fprintf(outfile, "\nproc progresstimer {} {\n"
                "   # loop while not finished\n"
                "   while { ![k done] } {\n"
                    "       # display %% completed to 2 decimal places\n"
                    "       puts -nonewline [format \"\\rProgress %%6.2f%%%%\" [expr 100.0*[k progressFraction]]]\n"
                    "       flush stdout\n\n"

                    "       # refresh interval: 200ms\n"
                    "       after 200\n"
                "   }\n"
                "   puts [format \"\\rProgress %%6.2f%%%%\" 100.0]\n"
            "}\n\n");

    // Setting up internal fluence counting 
    stringstream vtk_v_file_name;
    vtk_v_file_name << out_file_name.substr(0, out_file_name.length()-4) << "_volume.vtk";
    fprintf(outfile, "##### Set up internal fluence counting\n"
                     "TriFilterRegionBounds TF\n"
                     "  TF mesh $mesh\n"
                     "  TF bidirectional 1\n\n"

                     "##### designate regions whose boundaries should be monitored\n"
                     "  TF includeRegion 4 1\n"
                     "  TF includeRegion 3  1\n\n"

                     "vtkFullMonteArrayAdaptor vtkPhiV\n\n"

                     "TetraFilterByRegion MF\n"
                     "  MF mesh $mesh\n"
                     "  MF include 4 1\n\n"

                     "$mesh setFacesForFluenceCounting TF\n\n"
                     "vtkFullMonteTetraMeshWrapper VTKM\n"
                     "  VTKM mesh $mesh\n\n"

                     "##### Writer pipeline for volume field data (regions & vol fluence)\n"
                     "vtkFieldData volumeFieldData\n"
                     "  volumeFieldData AddArray [VTKM regions]\n\n"

                     "vtkDataObject volumeDataObject\n"
                     "  volumeDataObject SetFieldData volumeFieldData\n\n"

                     "vtkMergeDataObjectFilter mergeVolume\n"
                     "  mergeVolume SetDataObjectInputData volumeDataObject\n"
                     "  mergeVolume SetInputData [VTKM blankMesh]\n"
                     "  mergeVolume SetOutputFieldToCellDataField\n\n"

                     "vtkUnstructuredGridWriter VW\n"
                     "  VW SetInputConnection [mergeVolume GetOutputPort]\n"
                     "  VW SetFileName \"%s\"\n\n"
                     
                     "EnergyToFluence EVF\n"
                     "  EVF mesh $mesh\n"
                     "  EVF materials $opt\n\n", vtk_v_file_name.str().c_str());


   /*fprintf(outfile, "## Writer pipeline for surface field data (organ-surface fluence)\n"
                   "vtkFullMonteSpatialMapWrapperFU vtkPhi\n\n"

                   "set surfaceFluenceArray [vtkPhi array]\n"
                   "    $surfaceFluenceArray SetName \"Surface Fluence (au)\"\n\n"

                   "vtkFieldData surfaceFieldData\n"
                   "    surfaceFieldData AddArray $surfaceFluenceArray\n\n"

                   "puts \"surfaceFieldData size: [surfaceFieldData GetNumberOfTuples]\"\n"
                   "puts \"number of faces:       [[VTKM faces] GetNumberOfCells]\"\n\n"

                   "vtkDataObject surfaceData\n"
                   "    surfaceData SetFieldData surfaceFieldData\n\n"
                    
                   "vtkMergeDataObjectFilter mergeFluence\n"
                   "    mergeFluence SetDataObjectInputData surfaceData\n"
                   "    mergeFluence SetInputData [VTKM faces]\n"
                   "    mergeFluence SetOutputFieldToCellDataField\n\n"

                   "vtkFullMonteFilterTovtkIdList surfaceTriIDs\n"
                   "    surfaceTriIDs mesh $mesh\n"
                   "    surfaceTriIDs filter [TF self]\n\n"


                   "vtkExtractCells extractSurface\n"
                   "    extractSurface SetInputConnection [mergeFluence GetOutputPort]\n"
                   "    extractSurface SetCellList [surfaceTriIDs idList]\n\n"

                   "vtkGeometryFilter geom\n"
                   "    geom SetInputConnection [extractSurface GetOutputPort]\n\n"

                   "vtkPolyDataWriter VTKW\n"
                   "    VTKW SetInputConnection [geom GetOutputPort]\n\n"

                   "DoseSurfaceHistogramGenerator DSHG\n"
                   "    DSHG mesh $mesh\n"
                   "    DSHG filter TF\n\n"

                   "DoseVolumeHistogramGenerator DVHG\n"
                   "    DVHG mesh $mesh\n"
                   "    DVHG filter MF\n\n"

                   "BidirectionalFluence BF\n\n"

                   "FluenceConverter FC\n"
                   "    FC mesh $mesh\n"
                   "    FC materials $opt\n\n");
    // */


    // Running the kernel
    fprintf(outfile, "##### Run it\n\n  ##### Run Kernel, display progress timer, and awaut finish\n");
    fprintf(outfile, "  k startAsync\n    progresstimer\n    k finishAsync\n\n");


    // Gathering data
    stringstream vtk_file_name;
    vtk_file_name << "TCL_scripts/" << out_file_name.substr(0, out_file_name.length()-4) << "_volume1.vtk";
    
    /*
    fprintf(outfile, "BF source [k getInternalSurfaceFluenceMap]\n\n"

                   "set Emap [FC convertToEnergyDensity [k getVolumeAbsorbedEnergyMap]]\n\n"

                   "set phiV [FC convertToFluence [k getVolumeAbsorbedEnergyMap]]\n\n"

                   "vtkFullMonteArrayAdaptor EmapAdaptor\n"
                   "    EmapAdaptor source $Emap\n\n"

                   "vtkFullMonteArrayAdaptor PhiAdaptor\n"
                   "    PhiAdaptor source [k getVolumeFluenceMap]\n\n"

                   "puts \"Emap = $Emap\"\n\n"

                   "set phi [BF result]\n\n"

                   "vtkPhi source $phi\n"
                   "vtkPhi update\n\n"


                   "DSHG fluence $phi\n"
                   "set dsh [DSHG result]\n\n"

                   "puts \"DSH generated\"\n\n"

                   "volumeFieldData AddArray [EmapAdaptor result]\n"
                   "volumeFieldData AddArray [PhiAdaptor result]\n\n"

                   "$dsh print\n\n"

                   "DVHG fluence $phiV\n"
                   "set dvh [DVHG result]\n\n"

                   "puts \"DVH generated\"\n\n"

                   "$dvh print\n\n"

                   "VTKW SetFileName \"%s\"\n"
                   "VTKW Update\n"

                   "VW Update", vtk_file_name.str().c_str());
    // */

   fprintf(outfile, "EVF source [k getResultByIndex 2]\n"
                   "EVF update\n\n"

                   "vtkPhiV source [EVF result]\n"
                   "volumeFieldData AddArray [vtkPhiV array]\n"
                   "    [vtkPhiV array] SetName \"Fluence\"\n\n"

                   "VW SetFileName \"%s\"\n"
                   "VW Update\n"
                   "exit", vtk_file_name.str().c_str());

    // closing the file
    fclose(outfile);
    fprintf(stderr, "\033[1;%dmScript Generated!\033[0m\n\n", 36);
}

/* This function generates a tcl script to run FullMonte with the input source positions given as tetrahedra
 * (one source at a time)
 * The script outputs the amount of energy absorbed inside each tetrahedron
 */
void src_placer::make_tcl_script_absorption(string out_file_name, 
            string mesh_path, const vector<tetrahedron* > & sources,
            bool read_mesh_from_vtk) //const vector<Point*> & sources)
{
    fprintf(stderr, "\033[1;%dm\nCreating a TCL FullMonte Absorption Script: %s\033[0m\n", 33, out_file_name.c_str());

    if (sources.size() == 0)
    {
        fprintf(stderr, "\033[1;%dmNo sources provided! Exiting... \033[0m\n", 31);
        exit(-1);
    }

    // Creating output file
    stringstream out_file_ss; 
    out_file_ss << "TCL_scripts/" << out_file_name;
    FILE* outfile = fopen(out_file_ss.str().c_str(), "w");
    
    // Importing VTK TCK Package
    fprintf(outfile, "##### Import VTK TCL Package \n");
    fprintf(outfile, "package require vtk \n \n");

    // Close the default TCL/TK window
    fprintf(outfile, "##### Close the default tcl/tk window\n");
    fprintf(outfile, "wm withdraw .\n\n");

    // Load required packages
    fprintf(outfile, "##### Loading the required packages\n\n");
    
    fprintf(outfile, "##### TIMOS format reader (for Digimouse) with TCL bindings\n");
    fprintf(outfile, "load libFullMonteTIMOSTCL.so\n\n");
    
    fprintf(outfile, "##### Geometry model with TCL bindings\n");
    fprintf(outfile, "load libFullMonteGeometryTCL.so\n\n");

    fprintf(outfile, "##### Software kernels with TCL bindings\n");
    fprintf(outfile, "load libFullMonteSWKernelTCL.so\n");
    fprintf(outfile, "load libFullMonteKernelsTCL.so\n\n");

    fprintf(outfile, "##### Data output manipulation\n");
    fprintf(outfile, "load libFullMonteDataTCL.so\n");
    fprintf(outfile, "load libFullMonteQueriesTCL.so\n\n");

    fprintf(outfile, "##### VTK interface\n");
    fprintf(outfile, "load libvtkFullMonteTCL-6.3.so\n");
    fprintf(outfile, "load libFullMonteVTKFileTCL.so\n\n");

    fprintf(outfile, "puts \"loaded libs\" \n\n");

    // File parameters
    fprintf(outfile, "##### Basic parameters: file name \n\n");

    fprintf(outfile, "#default file prefix\n");
    fprintf(outfile, "set pfx \"%s\"\n\n", mesh_path.c_str());

    fprintf(outfile, "##### override with 1st cmdline arg\n");
    fprintf(outfile, "if { $argc >= 1 } { set pfx [lindex $argv 0] }\n\n");

    // Setting optical properties and mesh files
    fprintf(outfile, "set optfn \"$pfx.opt\"\n");
    if (read_mesh_from_vtk)
        fprintf(outfile, "set meshfn \"$pfx.vtk\"\n");
    else
        fprintf(outfile, "set meshfn \"$pfx.mesh\"\n");
    fprintf(outfile, "set legendfn \"$pfx.legend\"\n");
    fprintf(outfile, "set ofn \"fluence.out\"\n\n");


    // Reading problem definitions in TIMOS or VTK format
    if (read_mesh_from_vtk)
    {
        fprintf(outfile, "#### Read Problem Mesh in VTK fromat\n");
        fprintf(outfile, "VTKLegacyReader VTKR\n"
                         "      VTKR setFileName $meshfn\n"
                         "      VTKR addZeroPoint 1\n"
                         "      VTKR addZeroCell 1\n");

        fprintf(outfile, "set MB [VTKR mesh]\n"
                         "set mesh [TetraMesh foo $MB]\n");
    }
    fprintf(outfile, "###### Read problem definition in TIMOS format\n");
    fprintf(outfile, "TIMOSAntlrParser R\n\n");

    if (!read_mesh_from_vtk)
        fprintf(outfile, "R setMeshFileName $meshfn\n");
    fprintf(outfile, "R setOpticalFileName $optfn\n\n");
//     fprintf(outfile, "R setLegendFileName $legendfn\n\n");

    if (!read_mesh_from_vtk)
        fprintf(outfile, "set mesh [R mesh]\n");
    fprintf(outfile, "set opt [R materials_simple]\n\n");


    // Configuring simulation kernel
    fprintf(outfile, "##### Create and configure simulation kernel with surface scoring\n");

    fprintf(outfile, "TetraSVKernel k $mesh\n\n");
    fprintf(outfile, "k energy 1\n      # total energy\n"); // Candidate sources are all of unit power
    fprintf(outfile, "k materials $opt\n        # materials\n");
    fprintf(outfile, "k setUnitsToMM\n      # units for mesh dimensions "
                    "& optical properties (must match each other)\n");
    
    // Monte carlo kernel properties
    fprintf(outfile, "\n##### Monte Carlo kernel properties\n");
    fprintf(outfile, "k roulettePrWin 0.1\n        # probability of roulette win\n");
    fprintf(outfile, "k rouletteWMin 1e-5\n        # minimum weight "
                "\"wmin\" before roulette takes effect\n");
    fprintf(outfile, "k maxSteps 10000\n        # maximum"
                " number of steps to trace a packet\n");
    fprintf(outfile, "k maxHits 100\n        # maximum number"
                "of boundaries a single step can take\n");
    fprintf(outfile, "k packetCount 1000000\n"       
                    "        # number of packets to simulate"
                    " (more -> better quality, longer run)\n");
    fprintf(outfile, "k threadCount 8\n        # number of threads "
                    "(set to number of cores, or 2x number of cores if hyperthreading\n\n");

    
    // Defining progress timer call back function for use during simulation run
    fprintf(outfile, "##### Define progress timer callback function for use during simulation run\n");

    fprintf(outfile, "\nproc progresstimer {} {\n"
                "   # loop while not finished\n"
                "   while { ![k done] } {\n"
                    "       # display %% completed to 2 decimal places\n"
                    "       puts -nonewline [format \"\\rProgress %%6.2f%%%%\" [expr 100.0*[k progressFraction]]]\n"
                    "       flush stdout\n\n"

                    "       # refresh interval: 200ms\n"
                    "       after 200\n"
                "   }\n"
                "   puts [format \"\\rProgress %%6.2f%%%%\" 100.0]\n"
            "}\n\n");

    // Setting up internal fluence counting 
    stringstream vtk_v_file_name;
    vtk_v_file_name << out_file_name.substr(0, out_file_name.length()-4) << "_volume.vtk";
    fprintf(outfile, "##### Set up internal fluence counting\n"
                     "TriFilterRegionBounds TF\n"
                     "  TF mesh $mesh\n"
                     "  TF bidirectional 1\n\n"

                     "##### designate regions whose boundaries should be monitored\n"
                     "  TF includeRegion 4 1\n"
                     "  TF includeRegion 3  1\n\n"

                     "vtkFullMonteArrayAdaptor vtkPhiV\n\n"

                     "TetraFilterByRegion MF\n"
                     "  MF mesh $mesh\n"
                     "  MF include 4 1\n\n"

                     "$mesh setFacesForFluenceCounting TF\n\n"
                     "vtkFullMonteTetraMeshWrapper VTKM\n"
                     "  VTKM mesh $mesh\n\n"

                     "##### Writer pipeline for volume field data (regions & vol fluence)\n"
                     "vtkFieldData volumeFieldData\n"
                     "  volumeFieldData AddArray [VTKM regions]\n\n"

                     "vtkDataObject volumeDataObject\n"
                     "  volumeDataObject SetFieldData volumeFieldData\n\n"

                     "vtkMergeDataObjectFilter mergeVolume\n"
                     "  mergeVolume SetDataObjectInputData volumeDataObject\n"
                     "  mergeVolume SetInputData [VTKM blankMesh]\n"
                     "  mergeVolume SetOutputFieldToCellDataField\n\n"

                     "vtkUnstructuredGridWriter VW\n"
                     "  VW SetInputConnection [mergeVolume GetOutputPort]\n"
                     "  VW SetFileName \"%s\"\n\n"
                     
                     "EnergyToFluence EVF\n"
                     "  EVF mesh $mesh\n"
                     "  EVF materials $opt\n\n", vtk_v_file_name.str().c_str());
    
    // Looping over all the sources
    fprintf(outfile, "##### Initializing array of tetrahedral sources\n");
    int tetra_idx;
    for(unsigned int i = 0; i < sources.size(); i++)
    {
        tetra_idx = sources[i]->get_id();

        fprintf(outfile, "set sources_array(%d) %d\n", i, tetra_idx); 
    }
    fprintf(outfile, "\n");

    
    // TCL commands to loop over the sources
    fprintf(outfile, "##### Looping over the sources array\n");
    fprintf(outfile, "for {set index 0} { $index < [array size sources_array] } { incr index} {\n   puts $index\n");

    // Configuring sources
    fprintf(outfile, "  ##### Configuring Sources\n");
    fprintf(outfile, "  Volume V0 $sources_array($index)\n\n");
        // The sources are volume elements with unit power

    fprintf(outfile, "  k source V0\n     # the source to launch from \n");

    // Running the kernel
    fprintf(outfile, "  ##### Run it\n\n    ##### Run Kernel, display progress timer, and awaut finish\n");
    fprintf(outfile, "      k startAsync\n      progresstimer\n     k finishAsync\n\n");

    // Gathering data
    stringstream vtk_file_name;
    vtk_file_name << "TCL_scripts/" << out_file_name.substr(0, out_file_name.length()-4) << "_volume.vtk";

    fprintf(outfile, "  EVF source [k getResultByIndex 2]\n"
                   "    EVF update\n\n"

                   "    vtkPhiV source [EVF result]\n"
                   "    volumeFieldData AddArray [vtkPhiV array]\n"
                   "        [vtkPhiV array] SetName \"Fluence$sources_array($index)\"\n");
    fprintf(outfile, "}\n\n");

    fprintf(outfile, "VW SetFileName \"%s\"\n"
                     "VW Update\n"
                     "exit", vtk_file_name.str().c_str());

    // closing the file
    fclose(outfile);
    fprintf(stderr, "\033[1;%dmScript Generated!\033[0m\n\n", 36);
}

