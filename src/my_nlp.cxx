// #include "my_nlp.h"
// 
// #include <cassert>
// #include <iostream>
// #include <cmath>
// #include <fstream>
// 
// #define _WITH_EQUALITY_CONSTRAINTS_
// #define _WO_INEQUALITY_CONSTRAINTS_
// 
// using namespace Ipopt;
// 
// // constructor
// my_nlp::my_nlp()
// {
//     _mesh_size = 0;
//     _num_sources = 0;
// }
// 
// // Regular Constructor
// my_nlp::my_nlp(unsigned int mesh_size, unsigned int num_sources,
//                const vector<double> & d_max, const vector<double> & d_min,
//                const vector<double> & mu_a, const vector<double> & p_max, vector<double> delta, 
//                const vector<SP_Point*> & mesh_points)
// {
//     _mesh_size = mesh_size;
//     _num_sources = num_sources;
// 
//     _d_max.resize(d_max.size());
//     _d_min.resize(d_min.size());
// 
//     _mu_a.resize(mu_a.size());
//     _delta.resize(delta.size());
// 
//     for (unsigned int i = 0; i < d_max.size(); i++)
//     {
//         _d_max[i] = d_max[i];
//         _d_min[i] = d_min[i];
//     }
// 
//     for (unsigned int i = 0; i < mu_a.size(); i++)
//     {
//         _mu_a[i] = mu_a[i];
//         _delta[i] = delta[i];
//     }
// 
//     _mesh_positions.resize(mesh_points.size());
//     
//     for (unsigned int i = 0; i < mesh_points.size(); i++)
//         _mesh_positions[i] = mesh_points[i];
// 
// 
//     _source_power.resize(_num_sources);
// 
//     for (unsigned int i = 0; i < _num_sources; i++)
//     {
//         _source_power[i] = p_max[i]*((1.0*rand())/RAND_MAX);
//     }
// 
//     // TODO: Maybe we do not want to use for loops to save memory (no need to copy)
// }
// 
// //destructor
// my_nlp::~my_nlp()
// {}
// 
// 
// // returns the size of the problem
// bool my_nlp::get_nlp_info(Index& n, Index& m, Index& nnz_jac_g,
//                              Index& nnz_h_lag, IndexStyleEnum& index_style)
// {
// #ifdef _WO_INEQUALITY_CONSTRAINTS_
// //    cout << "Hello get_nlp_info" << endl;
//   // The problem described in my_nlp.hpp has 4 variables, x[0] through x[3]
//   assert(_mesh_size != 0 && _num_sources != 0);
//   
//   // For the r's: n = _mesh_size*_num_sources
//   // For the positions: n = 3*_num_sources
//   n = _mesh_size*_num_sources;// + 3*_num_sources; //4;
// 
//   // one equality constraint and one inequality constraint
//   // For the equality of r to distance: m = _mesh_size*_num_sources
//   m = 0;
// 
//   // For Phi(r)*1: nnz = _mesh_size*(_num_sources) (w.r.t r's) + 0 (w.r.t x's)
//   // Fot r = distance: nnz = (_mesh_size*_num_sources (w.r.t r's) + _mesh_size*3*_num_sources (w.r.t x's))
//   nnz_jac_g = 0; 
// 
// #ifdef _WITH_EQUALITY_CONSTRAINTS_
//     n += 3*_num_sources;
//     m += _mesh_size*_num_sources;
//     nnz_jac_g += 4*_mesh_size*_num_sources;
// #endif
// 
//   // the hessian is also dense and has 16 total nonzeros, but we
//   // only need the lower left corner (since it is symmetric)
//   nnz_h_lag = (pow(n, 2) - n)/2 + n; //10;
// 
//   // use the C style indexing (0-based)
//   index_style = TNLP::C_STYLE;
// 
//   return true;
// #endif
// 
// //    cout << "Hello get_nlp_info" << endl;
//   // The problem described in my_nlp.hpp has 4 variables, x[0] through x[3]
//   assert(_mesh_size != 0 && _num_sources != 0);
//   
//   // For the r's: n = _mesh_size*_num_sources
//   // For the positions: n = 3*_num_sources
//   n = _mesh_size*_num_sources;// + 3*_num_sources; //4;
// 
//   // one equality constraint and one inequality constraint
//   // For the equality of r to distance: m = _mesh_size*_num_sources
//   // For the inequality of Phi: m = _mesh_size
//   m = _mesh_size;// + _mesh_size*_num_sources; // + _mesh_size; //2;
// 
//   // For Phi(r)*1: nnz = _mesh_size*(_num_sources) (w.r.t r's) + 0 (w.r.t x's)
//   // Fot r = distance: nnz = (_mesh_size*_num_sources (w.r.t r's) + _mesh_size*3*_num_sources (w.r.t x's))
//   nnz_jac_g = _mesh_size*(_num_sources);// + _mesh_size*_num_sources*4;//_mesh_size*_num_sources; //8;
// 
// 
// #ifdef _WITH_EQUALITY_CONSTRAINTS_
//     n += 3*_num_sources;
//     m += _mesh_size*_num_sources;
//     nnz_jac_g += 4*_mesh_size*_num_sources;
// #endif
// 
//   // the hessian is also dense and has 16 total nonzeros, but we
//   // only need the lower left corner (since it is symmetric)
//   nnz_h_lag = (pow(n, 2) - n)/2 + n; //10;
// 
//   // use the C style indexing (0-based)
//   index_style = TNLP::C_STYLE;
// 
//   return true;
// }
// 
// // returns the variable bounds
// bool my_nlp::get_bounds_info(Index n, Number* x_l, Number* x_u,
//                                 Index m, Number* g_l, Number* g_u)
// {
// #ifdef _WO_INEQUALITY_CONSTRAINTS_
//     //    cout << "Hello get_bounds_info" << endl;
//     // here, the n and m we gave IPOPT in get_nlp_info are passed back to us.
//     // If desired, we could assert to make sure they are what we think they are.
// #ifdef _WITH_EQUALITY_CONSTRAINTS_
//     assert(n == _mesh_size*_num_sources + 3*_num_sources);//4);
//     assert(m == _mesh_size*_num_sources);//2);
// #else
//     assert(n == _mesh_size*_num_sources);
//     assert(m == 0);
// #endif
//     
//     // Equality constraints second: r_ij - norm(x_i - x_j) = 0     SIZE: _mesh_size*_num_sources
// #ifdef _WITH_EQUALITY_CONSTRAINTS_
//     for (Index i = 0; i < m; i++)
//       g_l[i] = 0.0;//-1e-6; 
// 
//     for (Index i = 0; i < m; i++)
//       g_u[i] = 0.0;//1e-6;
// 
//     for (Index i = 0; i < 3*_num_sources; i++)
//     {
//         // TODO: make it depend on the input file
//         x_l[i] = -1;
//         x_u[i] = 1;
//     }
//     for (Index i = 3*_num_sources; i < n; i++)
//     {
//         x_l[i] = 0.0;
//         x_u[i] = 10;//1e19;
//     }
// #else
//     for (Index i = 0; i < n; i++)
//     {
//         x_l[i] = 0.0;
//         x_u[i] = 10;//1e19;
//     }
// #endif
//         return true;
// #endif // without inequality constraints
// 
// //    cout << "Hello get_bounds_info" << endl;
//     // here, the n and m we gave IPOPT in get_nlp_info are passed back to us.
//     // If desired, we could assert to make sure they are what we think they are.
// #ifdef _WITH_EQUALITY_CONSTRAINTS_
//     assert(n == _mesh_size*_num_sources + 3*_num_sources);//4);
//     assert(m == _mesh_size*_num_sources + _mesh_size);//2);
// #else
//     assert(n == _mesh_size*_num_sources);
//     assert(m == _mesh_size);
// #endif
//     // the variables have lower bounds of 1
//     //   for (Index i=0; i<4; i++) {
//     //     x_l[i] = 1.0;
//     //   }
//     
//     // Inequality constraints first: d_min <= Phi(r)*1 <= d_max    SIZE: _mesh_size
//     // Equality constraints second: r_ij - norm(x_i - x_j) = 0     SIZE: _mesh_size*_num_sources
// 
//     for (Index i = 0; i < _mesh_size; i++)
//       g_l[i] = _d_min[i];
// 
//     // the variables have upper bounds of 5
//     //   for (Index i=0; i<4; i++) {
//     //     x_u[i] = 5.0;
//     //   }
//     for (Index i = 0; i < _mesh_size; i++)
//       g_u[i] = _d_max[i];
// 
// #ifdef _WITH_EQUALITY_CONSTRAINTS_
//     for (Index i = _mesh_size; i < m; i++)
//       g_l[i] = 0.0;//-1e-6; 
// 
//     for (Index i = _mesh_size; i < m; i++)
//       g_u[i] = 0.0;//1e-6;
// 
//     for (Index i = 0; i < 3*_num_sources; i++)
//     {
//         // TODO: make it depend on the input file
//         x_l[i] = -1;
//         x_u[i] = 1;
//     }
//     for (Index i = 3*_num_sources; i < n; i++)
//     {
//         x_l[i] = 0.0;
//         x_u[i] = 10;//1e19;
//     }
// #else
//     for (Index i = 0; i < n; i++)
//     {
//         x_l[i] = 0.0;
//         x_u[i] = 10;//1e19;
//     }
// #endif
//     // the first constraint g1 has a lower bound of 25
//     //   g_l[0] = 25;
//     // the first constraint g1 has NO upper bound, here we set it to 2e19.
//     // Ipopt interprets any number greater than nlp_upper_bound_inf as
//     // infinity. The default value of nlp_upper_bound_inf and nlp_lower_bound_inf
//     // is 1e19 and can be changed through ipopt options.
//     //   g_u[0] = 2e19;
// 
//     // the second constraint g2 is an equality constraint, so we set the
//     // upper and lower bound to the same value
//     //   g_l[1] = g_u[1] = 40.0;
// 
//     return true;
// }
// 
// // returns the initial point for the problem
// bool my_nlp::get_starting_point(Index n, bool init_x, Number* x,
//                                    bool init_z, Number* z_L, Number* z_U,
//                                    Index m, bool init_lambda,
//                                    Number* lambda)
// {
// //    cout << "Hello get_starting_point" << endl;
//     // Here, we assume we only have starting values for x, if you code
//     // your own NLP, you can provide starting values for the dual variables
//     // if you wish
//     assert(init_x == true);
//     assert(init_z == false);
//     assert(init_lambda == false);
// 
//     // initialize to the given starting point
//     //   x[0] = 1.0;
//     //   x[1] = 5.0;
//     //   x[2] = 20.0;
//     //   x[3] = 1.0;
// 
// //     x[0] = x[1] = x[2] = 0.5;
// //     x[3] = x[4] = x[5] = -0.5;
// 
//     for (Index i = 0; i < n; i++)
//         x[i] = 1e-8; 
// 
//     return true;
// }
// 
// // returns the value of the objective function
// bool my_nlp::eval_f(Index n, const Number* x, bool new_x, Number& obj_value)
// {
// //    cout << "Hello eval_f" << endl;
// #ifdef _WITH_EQUALITY_CONSTRAINTS_
//     assert(n == _mesh_size*_num_sources + 3*_num_sources);
// #else
//     assert(n == _mesh_size*_num_sources);
// #endif
//     //obj_value = x[0] * x[3] * (x[0] + x[1] + x[2]) + x[2];
//     //obj_value = exp(-fabs(x[0]) - fabs(x[1]) - fabs(x[2]) - fabs(x[3]));
//     
//     vector<double> cost_before_norm_max(_mesh_size, 0.0);
//     vector<double> cost_before_norm_min(_mesh_size, 0.0);
// 
//     // computing total fluence at each element
//     vector<double> total_fluence(_mesh_size, 0.0);
//     for (unsigned int i = 0; i < _mesh_size; i++)
//     {
//         for (unsigned int j = 0; j < _num_sources; j++)
//         {
// #ifdef _WITH_EQUALITY_CONSTRAINTS_
//             total_fluence[i] += compute_phi_r(x[3*_num_sources + j*_mesh_size + i], _mu_a[i], _delta[i], _source_power[j]);
//                                 // adding 3*_num_sources to the index to get the r variables
// #else       
//             total_fluence[i] += compute_phi_r(x[j*_mesh_size + i], _mu_a[i], _delta[i],  _source_power[j]);
// #endif
//         }
//     }
// 
//     // computing cost before norm
//     for (unsigned int i = 0; i < _mesh_size; i++)
//     {
//         if (fabs(_d_min[i]) < 1e-8) // means that this is an organ at risk
//         {
//             if (total_fluence[i] - _d_max[i] > 1e-7) // means that it violated
//                 cost_before_norm_max[i] = _d_max[i] - total_fluence[i];
//         }
//         else
//         {
//             if (_d_min[i] - total_fluence[i] > 1e-7) // means that it violated
//                 cost_before_norm_min[i] = total_fluence[i] - _d_min[i];
//         }
//     }
// 
//     obj_value = compute_p_norm_of(cost_before_norm_max, 2) + compute_p_norm_of(cost_before_norm_min, 2);
// 
//     return true;
// }
// 
// // return the gradient of the objective function grad_{x} f(x)
// bool my_nlp::eval_grad_f(Index n, const Number* x, bool new_x, Number* grad_f)
// {
// //    cout << "Hello eval_grad_f" << endl;
// #ifdef _WITH_EQUALITY_CONSTRAINTS_
//     assert(n == _mesh_size*_num_sources + 3*_num_sources);
// #else
//     assert(n == _mesh_size*_num_sources);
// #endif
// 
//   //grad_f[0] = x[0] * x[3] + x[3] * (x[0] + x[1] + x[2]);
//   //grad_f[1] = x[0] * x[3];
//   //grad_f[2] = x[0] * x[3] + 1;
//   //grad_f[3] = x[0] * (x[0] + x[1] + x[2]);
// 
// //   Number obj = 0;
// //   eval_f(n, x, new_x, obj);
// //   for (unsigned int i = 0; i < 4; i++)
// //       grad_f[i] =  -1.0*obj;
// 
//     // computing total fluence at each element
//     vector<double> total_fluence(_mesh_size, 0.0);
//     for (unsigned int i = 0; i < _mesh_size; i++)
//     {
//         for (unsigned int j = 0; j < _num_sources; j++)
//         {
// #ifdef _WITH_EQUALITY_CONSTRAINTS_
//             total_fluence[i] += compute_phi_r(x[3*_num_sources + j*_mesh_size + i], _mu_a[i], _delta[i],  _source_power[j]);
//                                 // adding 3*_num_sources to the index to get the r variables
// #else       
//             total_fluence[i] += compute_phi_r(x[j*_mesh_size + i], _mu_a[i], _delta[i], _source_power[j]);
// #endif
//         }
//     }
// 
// 
// #ifdef _WITH_EQUALITY_CONSTRAINTS_
//     // Derivated with respect to the point positions (TODO: for now, I am setting them to 0)
//     for (Index i = 0; i < 3*_num_sources; i++)
//         grad_f[i] = 0.0; 
// #endif
// 
//     for (unsigned int i = 0; i < _mesh_size; i++)
//     {
//         for (unsigned int j = 0; j < _num_sources; j++)
//         {
//             Number x_in;
// #ifdef _WITH_EQUALITY_CONSTRAINTS_
//             x_in = x[3*_num_sources + j*_mesh_size + i] + 1e-8;
// #else
//             x_in = x[j*_mesh_size + i] + 1e-8;
// #endif
// 
//             double del_phi_ij = (-1.0*_source_power[j]/(4*M_PI*_mu_a[i]*pow(_delta[i],2)))*((x_in/_delta[i]+1)*exp(-1.0*x_in/_delta[i])/pow(x_in,2));
//             
// #ifdef _WITH_EQUALITY_CONSTRAINTS_
//             if (fabs(_d_min[i]) < 1e-8) // means that this is an organ at risk
//             {
//                 if (total_fluence[i] - _d_max[i] > 1e-7) // means that it violated
//                     grad_f[3*_num_sources + j*_mesh_size + i] = -2*_d_max[i]*del_phi_ij + 2*total_fluence[i]*del_phi_ij;
//                 else
//                     grad_f[3*_num_sources + j*_mesh_size + i] = 0.0;
//             }
//             else
//             {
//                 if (_d_min[i] - total_fluence[i] > 1e-7) // means that it violated
//                     grad_f[3*_num_sources + j*_mesh_size + i] = -2*_d_min[i]*del_phi_ij + 2*total_fluence[i]*del_phi_ij;
//                 else
//                     grad_f[3*_num_sources + j*_mesh_size + i] = 0.0;
//             }
// #else
//             if (fabs(_d_min[i]) < 1e-8) // means that this is an organ at risk
//             {
//                 if (total_fluence[i] - _d_max[i] > 1e-7) // means that it violated
//                     grad_f[j*_mesh_size + i] = -2*_d_max[i]*del_phi_ij + 2*total_fluence[i]*del_phi_ij;
//                 else
//                     grad_f[j*_mesh_size + i] = 0.0;
//             }
//             else
//             {
//                 if (_d_min[i] - total_fluence[i] > 1e-7) // means that it violated
//                     grad_f[j*_mesh_size + i] = -2*_d_min[i]*del_phi_ij + 2*total_fluence[i]*del_phi_ij;
//                 else
//                     grad_f[j*_mesh_size + i] = 0.0;
//             }
// #endif
//         }
//     }
//     return true;
// }
// 
// // return the value of the constraints: g(x)
// bool my_nlp::eval_g(Index n, const Number* x, bool new_x, Index m, Number* g)
// {
// #ifdef _WO_INEQUALITY_CONSTRAINTS_
//     //    cout << "Hello eval_g" << endl;
// #ifdef _WITH_EQUALITY_CONSTRAINTS_
//     assert(n == _mesh_size*_num_sources + 3*_num_sources);//4);
//     assert(m == _mesh_size*_num_sources);//2);
// #else
//     assert(n == _mesh_size*_num_sources);
//     assert(m == 0);
// #endif
// 
// #ifdef _WITH_EQUALITY_CONSTRAINTS_
//     for (unsigned int i = 0; i < _mesh_size; i++)
//     {
//         for (unsigned int j = 0; j < _num_sources; j++)
//         {
//             g[j*_mesh_size + i] = x[3*_num_sources + j*_mesh_size + i] - 
//                         compute_distance(x[3*j], x[3*j + 1], x[3*j + 2], _mesh_positions[i]);
//         }
//     }
// #endif
//     return true;
// #endif // without inequality constraints
// 
// //    cout << "Hello eval_g" << endl;
// #ifdef _WITH_EQUALITY_CONSTRAINTS_
//     assert(n == _mesh_size*_num_sources + 3*_num_sources);//4);
//     assert(m == _mesh_size*_num_sources + _mesh_size);//2);
// #else
//     assert(n == _mesh_size*_num_sources);
//     assert(m == _mesh_size);
// #endif
// 
// //   g[0] = x[0] * x[1] * x[2] * x[3];
// //   g[1] = x[0]*x[0] + x[1]*x[1] + x[2]*x[2] + x[3]*x[3];
//     for (unsigned int i = 0; i < _mesh_size; i++)
//     {
//         for (unsigned int j = 0; j < _num_sources; j++)
//         {
// #ifdef _WITH_EQUALITY_CONSTRAINTS_
//             g[i] += compute_phi_r(x[3*_num_sources + j*_mesh_size + i], _mu_a[i], _delta[i], _source_power[j]);
//                                 // adding 3*_num_sources to the index to get the r variables
// #else
//             g[i] += compute_phi_r(x[j*_mesh_size + i], _mu_a[i], delta[i], _source_power[j]);
// #endif
//         }
//     }
// 
// #ifdef _WITH_EQUALITY_CONSTRAINTS_
//     for (unsigned int i = 0; i < _mesh_size; i++)
//     {
//         for (unsigned int j = 0; j < _num_sources; j++)
//         {
//             g[_mesh_size + j*_mesh_size + i] = x[3*_num_sources + j*_mesh_size + i] - 
//                         compute_distance(x[3*j], x[3*j + 1], x[3*j + 2], _mesh_positions[i]);
//         }
//     }
// #endif
// 
//     return true;
// }
// 
// // return the structure or values of the jacobian
// bool my_nlp::eval_jac_g(Index n, const Number* x, bool new_x,
//                            Index m, Index nele_jac, Index* iRow, Index *jCol,
//                            Number* values)
// {
// #ifdef _WO_INEQUALITY_CONSTRAINTS_
//     //    cout << "Hello eval_jac_g" << endl;
//   if (values == NULL) {
//     // return the structure of the jacobian
// 
//     Index triplet_index = 0; 
// 
// #ifdef _WITH_EQUALITY_CONSTRAINTS_
//     // r_ij constraints
//     for (Index i = 0; i < _mesh_size; i++)
//     {
//         for (Index j = 0; j < _num_sources; j++)
//         {
//             iRow[triplet_index] = j*_mesh_size + i; 
//             jCol[triplet_index] = 3*j; // xcoordinate of the source
//             triplet_index++; 
// 
//             iRow[triplet_index] = j*_mesh_size + i; 
//             jCol[triplet_index] = 3*j + 1; // ycoordinate of the source
//             triplet_index++; 
// 
//             iRow[triplet_index] = j*_mesh_size + i; 
//             jCol[triplet_index] = 3*j + 2; // zcoordinate of the source
//             triplet_index++; 
// 
//             iRow[triplet_index] = j*_mesh_size + i; 
//             jCol[triplet_index] = 3*_num_sources + j*_mesh_size + i; // r_ij
//             triplet_index++; 
//         }
//     }
// 
//     assert(triplet_index == 4*_mesh_size*_num_sources);
// #endif
//   }
//   else 
//   {
//     // return the values of the jacobian of the constraints
//     Index triplet_index = 0; 
// 
// #ifdef _WITH_EQUALITY_CONSTRAINTS_
//     // r_ij constraints 
//     for (Index i = 0; i < _mesh_size; i++)
//     {
//         for (Index j = 0; j < _num_sources; j++)
//         {
//             double distance = compute_distance(x[3*j], x[3*j + 1], x[3*j + 2], _mesh_positions[i]);
// 
//             values[triplet_index] = (x[3*j] - _mesh_positions[i]->get_xcoord())/distance; // TODO: distance may be zero
//             values[triplet_index+1] = (x[3*j+1] - _mesh_positions[i]->get_ycoord())/distance;  
//             values[triplet_index+2] = (x[3*j+2] - _mesh_positions[i]->get_zcoord())/distance;
// 
//             values[triplet_index+3] = 1;
// 
//             triplet_index += 4;
//         }
//     }
// #endif
//   }
//   return true;
// #endif // without inequality constraints
// 
// //    cout << "Hello eval_jac_g" << endl;
//   if (values == NULL) {
//     // return the structure of the jacobian
// 
//     // this particular jacobian is dense
//     /*
//     iRow[0] = 0;
//     jCol[0] = 0;
//     iRow[1] = 0;
//     jCol[1] = 1;
//     iRow[2] = 0;
//     jCol[2] = 2;
//     iRow[3] = 0;
//     jCol[3] = 3;
//     iRow[4] = 1;
//     jCol[4] = 0;
//     iRow[5] = 1;
//     jCol[5] = 1;
//     iRow[6] = 1;
//     jCol[6] = 2;
//     iRow[7] = 1;
//     jCol[7] = 3;
//     */
// 
//     Index triplet_index = 0; 
// 
//     // Phi(r) constraints 
//     for (Index i = 0; i < _mesh_size; i++)
//     {
//         for (Index j = 0; j < _num_sources; j++)
//         {
//             iRow[triplet_index] = i;
// #ifdef _WITH_EQUALITY_CONSTRAINTS_
//             jCol[triplet_index] = 3*_num_sources + j*_mesh_size + i; // This is the index of the specific r wanted (whole row)
// #else
//             jCol[triplet_index] = j*_mesh_size + i; // This is the index of the specific r wanted (whole row)
// #endif
//             triplet_index++;
//         }
//     }
// 
//     assert(triplet_index == _num_sources*_mesh_size);
// 
// #ifdef _WITH_EQUALITY_CONSTRAINTS_
//     // r_ij constraints
//     for (Index i = 0; i < _mesh_size; i++)
//     {
//         for (Index j = 0; j < _num_sources; j++)
//         {
//             iRow[triplet_index] = _mesh_size + j*_mesh_size + i; // Added mesh size to account for the first _mesh_size Phi(r) constraints
//             jCol[triplet_index] = 3*j; // xcoordinate of the source
//             triplet_index++; 
// 
//             iRow[triplet_index] = _mesh_size + j*_mesh_size + i; // Added mesh size to account for the first _mesh_size Phi(r) constraints
//             jCol[triplet_index] = 3*j + 1; // ycoordinate of the source
//             triplet_index++; 
// 
//             iRow[triplet_index] = _mesh_size + j*_mesh_size + i; // Added mesh size to account for the first _mesh_size Phi(r) constraints
//             jCol[triplet_index] = 3*j + 2; // zcoordinate of the source
//             triplet_index++; 
// 
//             iRow[triplet_index] = _mesh_size + j*_mesh_size + i; // Added mesh size to account for the first _mesh_size Phi(r) constraints
//             jCol[triplet_index] = 3*_num_sources + j*_mesh_size + i; // r_ij
//             triplet_index++; 
//         }
//     }
// 
//     assert(triplet_index == _mesh_size*_num_sources + 4*_mesh_size*_num_sources);
// #endif
//   }
//   else 
//   {
//     // return the values of the jacobian of the constraints
// 
// //     values[0] = x[1]*x[2]*x[3]; // 0,0
// //     values[1] = x[0]*x[2]*x[3]; // 0,1
// //     values[2] = x[0]*x[1]*x[3]; // 0,2
// //     values[3] = x[0]*x[1]*x[2]; // 0,3
// // 
// //     values[4] = 2*x[0]; // 1,0
// //     values[5] = 2*x[1]; // 1,1
// //     values[6] = 2*x[2]; // 1,2
// //     values[7] = 2*x[3]; // 1,3
// 
//     Index triplet_index = 0; 
//     // Phi Constraints
//     for (Index i = 0; i < _mesh_size; i++)
//     {
//         for (Index j = 0; j < _num_sources; j++)
//         {
// #ifdef _WITH_EQUALITY_CONSTRAINTS_
//             Number x_in = x[3*_num_sources + j*_mesh_size + i] + 1e-8; 
// #else
//             Number x_in = x[j*_mesh_size + i] + 1e-8; 
// #endif
//             values[triplet_index] = (-1.0*_source_power[j]/(4*M_PI*_mu_a[i]*_delta[i]*_delta[i]))*
//                                     ((x_in/_delta[i] + 1)*exp(-1.0*x_in/_delta[i])/pow(x_in,2));
// 
//             triplet_index++; 
//         }
//     }
// 
// #ifdef _WITH_EQUALITY_CONSTRAINTS_
//     // r_ij constraints 
//     for (Index i = 0; i < _mesh_size; i++)
//     {
//         for (Index j = 0; j < _num_sources; j++)
//         {
//             double distance = compute_distance(x[3*j], x[3*j + 1], x[3*j + 2], _mesh_positions[i]);
// 
//             values[triplet_index] = (x[3*j] - _mesh_positions[i]->get_xcoord())/distance; // TODO: distance may be zero
//             values[triplet_index+1] = (x[3*j+1] - _mesh_positions[i]->get_ycoord())/distance;  
//             values[triplet_index+2] = (x[3*j+2] - _mesh_positions[i]->get_zcoord())/distance;
// 
//             values[triplet_index+3] = 1;
// 
//             triplet_index += 4;
//         }
//     }
// #endif
//   }
//   return true;
// }
// 
// //return the structure or values of the hessian
// /*
// bool my_nlp::eval_h(Index n, const Number* x, bool new_x,
//                        Number obj_factor, Index m, const Number* lambda,
//                        bool new_lambda, Index nele_hess, Index* iRow,
//                        Index* jCol, Number* values)
// {
//   if (values == NULL) {
//     // return the structure. This is a symmetric matrix, fill the lower left
//     // triangle only.
// 
//     // the hessian for this problem is actually dense
//     Index idx=0;
//     for (Index row = 0; row < 4; row++) {
//       for (Index col = 0; col <= row; col++) {
//         iRow[idx] = row;
//         jCol[idx] = col;
//         idx++;
//       }
//     }
// 
//     assert(idx == nele_hess);
//   }
//   else {
//     // return the values. This is a symmetric matrix, fill the lower left
//     // triangle only
// 
//     Number obj = 0.0;
//     bool ff = eval_f(n, x, new_x, obj);
//     // fill the objective portion
//     values[0] = obj_factor * obj;//(2*x[3]); // 0,0
// 
//     values[1] = obj_factor * obj;//(x[3]);   // 1,0
//     values[2] = 0.;                    // 1,1
// 
//     values[3] = obj_factor * obj;//(x[3]);   // 2,0
//     values[4] = obj;//0.;                    // 2,1
//     values[5] = obj;//0.;                    // 2,2
// 
//     values[6] = obj_factor * obj;//(2*x[0] + x[1] + x[2]); // 3,0
//     values[7] = obj_factor * obj;//(x[0]);                 // 3,1
//     values[8] = obj_factor * obj;//(x[0]);                 // 3,2
//     values[9] = obj;//0.;                                  // 3,3
// 
// 
//     // add the portion for the first constraint
//     values[1] += lambda[0] * (x[2] * x[3]); // 1,0
// 
//     values[3] += lambda[0] * (x[1] * x[3]); // 2,0
//     values[4] += lambda[0] * (x[0] * x[3]); // 2,1
// 
//     values[6] += lambda[0] * (x[1] * x[2]); // 3,0
//     values[7] += lambda[0] * (x[0] * x[2]); // 3,1
//     values[8] += lambda[0] * (x[0] * x[1]); // 3,2
// 
//     // add the portion for the second constraint
//     values[0] += lambda[1] * 2; // 0,0
// 
//     values[2] += lambda[1] * 2; // 1,1
// 
//     values[5] += lambda[1] * 2; // 2,2
// 
//     values[9] += lambda[1] * 2; // 3,3
//   }
// 
//   return true;
// }
// // */
// 
// void my_nlp::finalize_solution(SolverReturn status,
//                                   Index n, const Number* x, const Number* z_L, const Number* z_U,
//                                   Index m, const Number* g, const Number* lambda,
//                                   Number obj_value,
// 				  const IpoptData* ip_data,
// 				  IpoptCalculatedQuantities* ip_cq)
// {
//     //    cout << "Hello finalize_solution" << endl;
//     // here is where we would store the solution to variables, or write to a file, etc
//     // so we could use the solution.
// 
//     // For this example, we write the solution to the console
//     ofstream output;
//     output.open("opt_results.txt");//, "w");
// 
//     output << std::endl << std::endl << "Solution of the primal variables, x" << std::endl;
//     for (Index i=0; i<n; i++) {
//      output << "x[" << i << "] = " << x[i] << std::endl;
//     }
// 
//     output << std::endl << std::endl << "Solution of the bound multipliers, z_L and z_U" << std::endl;
//     for (Index i=0; i<n; i++) {
//     output << "z_L[" << i << "] = " << z_L[i] << std::endl;
//     }
//     //for (Index i=0; i<n; i++) {
//     //ouput << "z_U[" << i << "] = " << z_U[i] << std::endl;
//     //}
// 
//     output << std::endl << std::endl << "Objective value" << std::endl;
//     output << "f(x*) = " << obj_value << std::endl;
// 
//     output << std::endl << "Final value of the constraints:" << std::endl;
//     for (Index i=0; i<m ;i++) {
//     output << "g(" << i << ") = " << g[i] << std::endl;
//     }
// 
// 
//     // Computing fluence limits
//     // computing total fluence at each element
//     vector<double> total_fluence(_mesh_size, 0.0);
//     double maxf = _NEG_INFINITY_; 
//     double minf = _INFINITY_;
//     for (unsigned int i = 0; i < _mesh_size; i++)
//     {
//         for (unsigned int j = 0; j < _num_sources; j++)
//         {
//             total_fluence[i] += compute_phi_r(x[3*_num_sources + j*_mesh_size + i], _mu_a[i], _delta[i], _source_power[j]);
//         }
// 
//         if (_d_min[i] < 1e-7)
//             maxf = max(maxf, total_fluence[i]);
//         else 
//             minf = min(minf, total_fluence[i]);
//     }
//     cout << "Max fluence on OAR = " << maxf << endl;
//     cout << "Min fluence on Unhealthy cells = " << minf << endl;
// 
//   output.close();
// }
// 
// // Implementing the private functions
// double my_nlp::compute_distance(Number x, Number y, Number z, SP_Point* p)
// {
//     return sqrt(pow(x - p->get_xcoord(), 2) + pow(y - p->get_ycoord(), 2) + pow(z - p->get_zcoord(), 2));
// }
// 
// double my_nlp::compute_phi_r(double r, double mu_a_i, double _delta, double p_j)
// {
//     r += 1e-8;
//     return ((p_j*exp(-r/_delta))/(4*M_PI*mu_a_i*(r)*_delta*_delta));
// }
// 
// // ! This method computes the p-norm of a vector
// double my_nlp::compute_p_norm_of(const vector<double> & v, int p)
// {
//     double sum = 0; 
//     for (unsigned int i = 0; i < v.size(); i++)
//     {
//         sum += pow(fabs(v[i]), p);
//     }
// 
//     return pow(sum, 1.0/p);
// }
