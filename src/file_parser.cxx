/**
 * @author: Shuran Wang
 * @date: December 26, 2021
 *
 * \file file_parser.cxx \brief External file format wrapper implementation
 *
 * This file implements wrappers to external file format (XML) parsers
 */

#include <iostream>
#include <fstream>
#include "file_parser.h"
#include <algorithm>

//! Default Constructor 
Parser::Parser() {
    _injection_points.region_id = -1; // not defined
    _sources.max_sources = -1; // not defined
}

/**
 * Regular Constructor, reads data from data file
 * @param filename [in]: Path to the data file
 */
Parser::Parser(string filename) {
    if (read_from_xml(filename)) {
        fprintf(stderr, "\n\033[1;%dmSuccessfully read XML file.\033[0m\n", 35);
    }

    // TODO: add checks
}

/*
 * Regular Destructor, deletes sources
 */
Parser::~Parser() {
    for (unsigned i = 0; i < _sources.sources_init.size(); i++) {
        if (_sources.sources_init[i]) delete _sources.sources_init[i];
    }
}

void Parser::read_line_source(const boost::property_tree::ptree& token) {
    using boost::property_tree::ptree;
    string location; // location for endpoint0
    vector<SP_Point> endpoints;
    double length = _INFINITY_;
    BOOST_FOREACH(ptree::value_type const& e, token) {
        if (e.first == "endpoint") {
            // create endpoint
            SP_Point endpoint;
            endpoint.set_xcoord (e.second.get<float>("x"));
            endpoint.set_ycoord (e.second.get<float>("y"));
            endpoint.set_zcoord (e.second.get<float>("z"));

            if (endpoints.size() == 0) { // first endpoint
                location = e.second.get<string>("<xmlattr>.location", "proximal");
                endpoints.push_back(endpoint);

                // cout << "Pushing proximal endpoint " << endpoint << endl;
            } else {
                if (location == "proximal") {
                    endpoints.push_back(endpoint);
                    // cout << "Pushing distal endpoint " << endpoint << endl;
                } else if (location == "distal") {
                    endpoints.emplace(endpoints.begin(), endpoint);
                    // cout << "Pushing proximal endpoint " << endpoint << endl;
                } else {
                    fprintf(stderr, "\033[1;%dmInvalid attribute for endpoint, should be proximal or distal. \033[0m\n", 33);
                }
            }
        } else if (e.first == "length") {
            // set length
            length = boost::lexical_cast<double>(e.second.data());
        } else if (e.first != "<xmlcomment>" && e.first != "<xmlattr>") {
            fprintf(stderr, "\033[1;%dmInvalid tag in source, should be endpoint. Exiting... \033[0m\n", 31);
            exit(-1);
        }
    }

    // if endpoints are not entered, use entered length to generate initial solution
    if (endpoints.size() == 0) {
        if (_sources.sources_init.size() != 0) {
            fprintf(stderr, "\033[1;%dmAt least 1 line source not entered, ignoring entered sources and will generate initial solution. Exiting... \033[0m\n", 31);
            _sources.sources_init.clear();
        }
        _sources.lengths.push_back(length);
        return;
    }

    if (endpoints.size() != 2) {
        fprintf(stderr, "\033[1;%dmLine sources should have 2 endpoints, entered %lu. Exiting... \033[0m\n", 31, endpoints.size());
        exit(-1);
    }

    // cout << "endpoints[0] " << endpoints[0] << " endpoints[1] " << endpoints[1] << endl;
    if (length == _INFINITY_) length = endpoints[0].get_distance_from(endpoints[1]);
    _sources.lengths.push_back(length);

    Src_Line* s = new Src_Line(endpoints[0], endpoints[1]);
    // cout << "Reading source " << s << endl;
    _sources.sources_init.push_back(s);
}

void Parser::read_point_source(const boost::property_tree::ptree& token) {
    using boost::property_tree::ptree;
    vector<SP_Point> endpoints;
    BOOST_FOREACH(ptree::value_type const& e, token) {
        if (e.first == "endpoint") {
            // create endpoint
            SP_Point endpoint;
            endpoint.set_xcoord (e.second.get<float>("x"));
            endpoint.set_ycoord (e.second.get<float>("y"));
            endpoint.set_zcoord (e.second.get<float>("z"));

            if (endpoints.size() == 0) { // first endpoint
                endpoints.push_back(endpoint);
            } else {
                fprintf(stderr, "\033[1;%dmPoint source should only have 1 endpoint. Exiting... \033[0m\n", 31);
                exit(-1);
            }
        } else if (e.first != "<xmlcomment>" && e.first != "<xmlattr>") {
            fprintf(stderr, "\033[1;%dmInvalid tag in source. Exiting... \033[0m\n", 31);
            exit(-1);
        }
    }

    // check for incomplete point source
    if (endpoints.size() == 0) {
        if (_sources.sources_init.size() != 0) {
            fprintf(stderr, "\033[1;%dmAt least 1 point source not complete, ignoring entered sources and will generate initial solution. Exiting... \033[0m\n", 31);
            _sources.sources_init.clear();
        }
        _sources.lengths.push_back(0);
        return;
    }

    _sources.lengths.push_back(0);

    Src_Point* s = new Src_Point(endpoints[0].get_xcoord(), endpoints[0].get_ycoord(), endpoints[0].get_zcoord());
    _sources.sources_init.push_back(s);
}

//!
//! This method reads the fullmonte parameters specified in xml file
//! @param token [in]: string value of the <source> tag
//! 
void Parser::read_cutend_source(const boost::property_tree::ptree& token) {
    vector<SP_Point> endpoints;
    float r = 0, na= 0;
    SP_Point dir;
    bool set_r = false;
    bool set_na = false;
    bool set_dir = false;
    bool set_endpoint = false;
    using boost::property_tree::ptree;
    BOOST_FOREACH(ptree::value_type const& e, token) {
        if (e.first == "endpoint") {
            // create endpoint
            SP_Point endpoint;
            endpoint.set_xcoord (e.second.get<float>("x"));
            endpoint.set_ycoord (e.second.get<float>("y"));
            endpoint.set_zcoord (e.second.get<float>("z"));

            if (endpoints.size() == 0) { // first endpoint
                endpoints.push_back(endpoint);
            } else {
                fprintf(stderr, "\033[1;%dmCut-end source should only have 1 endpoint. Exiting... \033[0m\n", 31);
                exit(-1);
            }
            set_endpoint = true;
        } else if (e.first == "radius") {
            r = boost::lexical_cast<float>(e.second.data());
            set_r = true;
        } else if (e.first == "na") {
            na = boost::lexical_cast<float>(e.second.data());
            set_na = true;
        } else if (e.first == "direction") {
            // create endpoint
            dir.set_xcoord (e.second.get<float>("x"));
            dir.set_ycoord (e.second.get<float>("y"));
            dir.set_zcoord (e.second.get<float>("z"));
            set_dir = true;
        } else if (e.first != "<xmlcomment>" && e.first != "<xmlattr>") {
            fprintf(stderr, "\033[1;%dmInvalid tag in source. Exiting... \033[0m\n", 31);
            exit(-1);
        }
    }

    if (!(set_dir && set_endpoint && set_na && set_r)) {
        fprintf(stderr, "\033[1;%dmCutEnd source not entered properly. Exiting... \033[0m\n", 31);
        exit(-1);
    }
    _sources.lengths.push_back(0);

    Src_CutEnd* s = new Src_CutEnd(r, na, endpoints[0], dir);
    _sources.sources_init.push_back(s);
}

//!
//! This method sets the XML file and builds data structures
//! @param filename [in]: Path to the data file
//! 
bool Parser::read_from_xml(string filename) {
    // use boost ptree to store xml information
    using boost::property_tree::ptree;
    ptree pt;
    
    fprintf(stderr, "\033[1;%dmReading XML file\033[0m\n", 33);

    // create istream to read from file
    ifstream f;
    f.open(filename);
    if (!f) {
        fprintf(stderr, "\033[1;%dmCannot open XML File to read. \033[0m\n", 31);
        return false;
    }

    // read from xml file into ptree
    read_xml(f, pt);
    f.close();

    // traverse ptree
    // fm_params
    fprintf(stderr, "\033[1;%dmReading fm_params\033[0m\n", 36);
    _fm_params_map.clear();
    _params_map.clear();
    BOOST_FOREACH(ptree::value_type const& v, pt.get_child("fm_params")) {
        // check if tag is valid
        if (v.first == "use_cuda" || v.first == "pnf" || v.first == "num_packets" || v.first == "rand_seed" ||
                v.first == "wavelength" ||  v.first == "mesh_file" || v.first == "max_hits" ||
                v.first == "roulette_pr_win" || v.first == "roulette_w_min" || v.first == "max_steps") {
            fprintf(stderr, "\033[1;%dmReading %s\033[0m\n", 36, v.first.c_str());
            _fm_params_map[v.first] = v.second.data();
            _params_map[v.first] = v.second.data(); // some params will be used by pdt_plan
        } else if (v.first != "<xmlcomment>" && v.first != "<xmlattr>") {
            fprintf(stderr, "\033[1;%dmInvalid tag in fm_params. \033[0m\n", 33);
        }
    }

    // program_params
    fprintf(stderr, "\033[1;%dmReading program_params\033[0m\n", 36);
    BOOST_FOREACH(ptree::value_type const& v, pt.get_child("program_params")) {
        // check if tag is valid
        if (v.first == "read_vtk" || v.first == "tumor_weight" || v.first == "run_tests" || v.first == "fm_output_file" ||
                v.first == "override_file_path" || v.first == "vary_tumor_weight" || v.first == "target_tumor_cov" ||
                v.first == "final_dist_file" || v.first == "run_rt_feedback" || v.first == "rt_feedback_seed" ||
                v.first == "rt_feedback_det_rad" || v.first == "rt_feedback_det_na" || v.first == "rt_feedback_lut_file" ||
                v.first == "rt_feedback_lut_size" || v.first == "placement_type") {
            fprintf(stderr, "\033[1;%dmReading %s\033[0m\n", 36, v.first.c_str());
            _params_map[v.first] = v.second.data();
        } else if (v.first != "<xmlcomment>" && v.first != "<xmlattr>") {
            fprintf(stderr, "\033[1;%dmInvalid tag in program_params. \033[0m\n", 33);
        }
    }
    
    if (_params_map["placement_type"] == "sa") {
        _params_map["placement_type"] = "fixed";
		_run_sa = true;
	}
	else {
		_run_sa = false;
	}

    // sources
    _sources.sources_init.clear();
    fprintf(stderr, "\033[1;%dmReading sources\033[0m\n", 36);
    BOOST_FOREACH(ptree::value_type const& v, pt.get_child("sources")) {
        if (v.first == "max_sources") {
            fprintf(stderr, "\033[1;%dmReading max_sources\033[0m\n", 36);
            _sources.max_sources = boost::lexical_cast<int>(v.second.data());
        } else if (v.first == "line_tailored") {
            fprintf(stderr, "\033[1;%dmReading line_tailored\033[0m\n", 36);
            _sources.tailored = (v.second.data() == "true") ? true : false;
        } else if (v.first == "sources_init") {
            fprintf(stderr, "\033[1;%dmReading sources_init\033[0m\n", 36);
            BOOST_FOREACH(ptree::value_type const& w, v.second) {
                if (w.first == "source") {
                    string type = w.second.get<string>("<xmlattr>.type", "");

                    if (type == "line") {
                        if (_sources.sources_init.size() == 0 || _sources.source_type == Src_Abstract::Point) 
                            _sources.source_type = Src_Abstract::Line;
                        else if (_sources.unified_type) {
                            if (_sources.source_type != Src_Abstract::Line)
                                _sources.unified_type = false;
                        }
                        read_line_source(w.second);
                    } else if (type == "point") {
                        if (_sources.sources_init.size() == 0) {
                            _sources.source_type = Src_Abstract::Point;
                        } else if (_sources.unified_type && _sources.source_type != Src_Abstract::Line) {
                            if (_sources.source_type != Src_Abstract::Point)
                                _sources.unified_type = false;
                        }
                        read_point_source(w.second);
                    } else if (type == "cut-end") {
                        if (_sources.sources_init.size() == 0) 
                            _sources.source_type = Src_Abstract::CutEnd;
                        else if (_sources.unified_type) {
                            if (_sources.source_type != Src_Abstract::CutEnd)
                                _sources.unified_type = false;
                        }
                        read_cutend_source(w.second);
                    } else {
                        fprintf(stderr, "\033[1;%dmNot supported source type. Exiting... \033[0m\n", 31);
                        exit(-1);
                    }
                } else if (w.first != "<xmlcomment>" && w.first != "<xmlattr>") {
                    fprintf(stderr, "\033[1;%dmInvalid tag in sources_init, should be source. \033[0m\n", 33);
                }
            }
        } else if (v.first != "<xmlcomment>" && v.first != "<xmlattr>") {
            fprintf(stderr, "\033[1;%dmInvalid tag in sources. \033[0m\n", 33);
        }
    }

    if (_sources.max_sources == -1) {
        fprintf(stderr, "\033[1;%dmRequired field max_sources missing in sources. \033[0m\n", 31);
        return false;
    }

    if (_sources.sources_init.size() == 0) {
        fprintf(stderr, "\033[1;%dmInitial solution not provided. \033[0m\n", 31);
        if (!_run_sa) {
            fprintf(stderr, "\033[1;%dmNot running SA, exiting... \033[0m\n", 31);
            return false;
        }

        fprintf(stderr, "\033[1;%dmPDT-SPACE will generate an initial solution.\033[0m\n", 31);
        _sources.source_type = Src_Abstract::Line;
        while (_sources.lengths.size() < (unsigned)_sources.max_sources) {
            _sources.lengths.push_back(_INFINITY_);
        }
    }
    fprintf(stderr, "\033[1;%dmDone reading sources\033[0m\n", 36);

    // sa_options. Only read if running SA
    _constrain_injection_point = false;
    if (_run_sa) {
        // injection_points
        _injection_points.injection_points.clear();
        _sa_params_map.clear();
        // _placement_mode = PM_NONE;
        fprintf(stderr, "\033[1;%dmReading sa_options\033[0m\n", 36);
        BOOST_FOREACH(ptree::value_type const& u, pt.get_child("sa_options")) {
            // if (u.first == "placement_constraint") {
            //     if (u.second.data() == "none") _placement_mode = PM_NONE;
            //     else if (u.second.data() == "parallel") _placement_mode = PM_PARALLEL;
            //     else if (u.second.data() == "point") _placement_mode = PM_POINT;
            //     else {
            //         _placement_mode = PM_NONE;
            //         fprintf(stderr, "\033[1;%dmInvalid placement mode, defaulting to none. \033[0m\n", 33);
            //     }
            if (u.first == "beta" || u.first == "lambda" || u.first == "mpt" || u.first == "sa_placement_file" || u.first == "sa_num_packets" ||
                    u.first == "max_src_distance" || u.first == "min_src_distance" || u.first == "distance_to_boundary" || 
                    u.first == "output_top_placements") {
                fprintf(stderr, "\033[1;%dmReading %s\033[0m\n", 36, u.first.c_str());
                _sa_params_map[u.first] = u.second.data();
            } else if (u.first == "injection_points") {
                _constrain_injection_point = u.second.get<bool>("<xmlattr>.constraint", true);
                BOOST_FOREACH(ptree::value_type const& v, u.second) {
                    if (v.first == "region_id") {
                        _injection_points.region_id = boost::lexical_cast<int>(v.second.data());
                    } else if (v.first == "point") {
                        Parser_injection_point ip;
                        ip.id = -1;
                        ip.diameter = -1;
                        BOOST_FOREACH(ptree::value_type const& w, v.second) {
                            if (w.first == "id") {
                                ip.id = boost::lexical_cast<int>(w.second.data());
                            } else if (w.first == "diameter") {
                                ip.diameter = boost::lexical_cast<float>(w.second.data());
                            } else if (w.first == "normal") {
                                float x = w.second.get<float>("x");
                                float y = w.second.get<float>("y");
                                float z = w.second.get<float>("z");
                                ip.normal = SP_Point(x, y, z);
                            } else if (w.first == "center") {
                                float x = w.second.get<float>("x");
                                float y = w.second.get<float>("y");
                                float z = w.second.get<float>("z");
                                ip.center = SP_Point(x, y, z);
                            } else if (w.first != "<xmlcomment>") {
                                fprintf(stderr, "\033[1;%dmInvalid tag in point. \033[0m\n", 33);
                            }
                        }
                        if (ip.id < 0 || ip.diameter < 0) {
                            fprintf(stderr, "\033[1;%dmInvalid injection point. \033[0m\n", 31);
                            return false;
                        }
                        _injection_points.injection_points.push_back(ip);
                    } else if (v.first != "<xmlcomment>" && v.first != "<xmlattr>") {
                        fprintf(stderr, "\033[1;%dmInvalid tag in injection_points. \033[0m\n", 33);
                    }
                }

                if (_injection_points.region_id < 0) {
                    fprintf(stderr, "\033[1;%dmMissing injection point region id. \033[0m\n", 31);
                    return false;
                }
            } else if (u.first == "critical_tissues") {
                stringstream s (u.second.data());
                _critical_regions.clear();
                unsigned region;
                while (s >> region) {
                    _critical_regions.insert(region);
                }

                cout << "Read SA critical regions: ";
                for (auto const &elem: _critical_regions)
                    cout << elem << " ";
                cout << endl;
            } else if (u.first != "<xmlcomment>") {
                fprintf(stderr, "\033[1;%dmInvalid tag in sa_options. \033[0m\n", 33);
            }
        }
        fprintf(stderr, "\033[1;%dmDone reading sa_options\033[0m\n", 36);
    }

    // materials
    fprintf(stderr, "\033[1;%dmReading materials\033[0m\n", 36);
    _materials.clear();
    BOOST_FOREACH(ptree::value_type const& u, pt.get_child("materials")) {
        if (u.first == "material") {
            // do material read
            struct Parser_material m;
            m.name = u.second.get<string>("<xmlattr>.name", "Unnamed");
            m.id = u.second.get<int>("id");
            fprintf(stderr, "\033[1;%dm  Reading %s, material %d\033[0m\n", 36, m.name.c_str(), m.id);
            m.n = u.second.get<double>("n", 1);
            m.g = u.second.get<double>("g", 0);
            m.mu_a = u.second.get<double>("mu_a", 0);
            m.mu_s = u.second.get<double>("mu_s", 0);
            m.is_tumor = u.second.get<bool>("is_tumor", false);
            fprintf(stderr, "\033[1;%dm  n %f g %f mu_a %f mu_s %f %s\033[0m\n", 36, m.n, m.g, m.mu_a, m.mu_s, (m.is_tumor ? "tumour" : "non-tumour"));
            if (m.id != 0) {
                // only set if not exterior
                m.d_min = u.second.get<double>("d_min");
                m.d_max = u.second.get<double>("d_max");
                if (m.is_tumor && m.d_max < 1e-8) m.d_max = _INFINITY_;
                m.guardband = u.second.get<double>("safety_multiplier", 1);
                fprintf(stderr, "\033[1;%dm  d_min %f d_max %f guardband %f\033[0m\n", 36, m.d_min, m.d_max, m.guardband);
            }

            _materials.push_back(m);
        } else if (u.first == "matched_boundary") {
            _matched_boundary = (u.second.data() == "true") ? true : false;
        } else if (u.first != "<xmlcomment>") {
            fprintf(stderr, "\033[1;%dmInvalid tag in materials. \033[0m\n", 33);
        }
    }

    // error checks for materials
    if (_materials.size() == 0) {
        fprintf(stderr, "\033[1;%dmMaterials not read.\033[0m\n", 31);
        return false;
    }

    // sort materials by id
    sort(_materials.begin(), _materials.end(), [](const Parser_material & p1, 
					const Parser_material & p2) { return p1.id < p2.id; });
    
    // extensive error checks to avoid seg faults
    int last_id = _materials[0].id;
    // check for exterior, id should start from 0
    if (_materials[0].id != 0) {
        fprintf(stderr, "\033[1;%dmExterior (ID 0) not specified. Smallest ID is %u. Exiting... \033[0m\n", 31, _materials[0].id);
        exit(-1);
    }
    for (unsigned i = 1; i < _materials.size(); i++) {
        // check for overlapping ids
        if (_materials[i].id == last_id) {
            fprintf(stderr, "\033[1;%dmMaterial ID should be unique. Exiting... \033[0m\n", 31);
            exit(-1);
        }

        // check for discontinuity in indices. TODO: may want to support discontinued material id?
        if (_materials[i].id != last_id + 1) {
            fprintf(stderr, "\033[1;%dmMaterial ID should be continuous. Missing ID %d. Exiting... \033[0m\n", 31, last_id + 1);
            exit(-1);
        }

        last_id = _materials[i].id;
    }
    fprintf(stderr, "\033[1;%dmDone reading materials\033[0m\n", 36);

    return true;
}

//! Getter functions for injection points
Parser_injection_points Parser::get_injection_points() {
    return _injection_points;
}

void Parser::get_injection_point_options(bool &constrain/*, PlacementMode &mode*/, unsigned &max_num) {
    constrain = _constrain_injection_point;
    // mode = _placement_mode;
    max_num = (unsigned) (_sources.max_sources);
}

void Parser::get_injection_point_options(bool &constrain/*, PlacementMode &mode*/) {
    constrain = _constrain_injection_point;
    // mode = _placement_mode;
}

//! Getter function for sources called by sa_engine
void Parser::get_sources(vector<SP_Line> &sources, vector<double> &lengths) {
    if (!((_sources.source_type == Src_Abstract::Line || _sources.source_type == Src_Abstract::Point) && _sources.unified_type)) {
		fprintf(stderr, "\033[1;%dmCurrently only line and point sources supported in SA.\nExiting...\033[0m\n", 31);
		std::exit(-1);
    }
    sources.clear();
    sources.resize(_sources.sources_init.size());
    for (unsigned i = 0; i < _sources.sources_init.size(); i++) {
        if (_sources.sources_init[i]->get_type() == Src_Abstract::Line) {
            sources[i] = *((SP_Line*) (Src_Line*) (_sources.sources_init[i]));
        } else {
            sources[i] = SP_Line(*((SP_Point*) (Src_Point*) _sources.sources_init[i]), *((SP_Point*) (Src_Point*) _sources.sources_init[i]));
        }
        cout << "Reading source " << sources[i] << endl;
    }
    lengths = _sources.lengths;
}

//! Getter function for sources called by pdt_plan
void Parser::get_sources(vector<Src_Abstract*> &sources) {
    sources = _sources.sources_init;
}

//! Getter function for source types. If mixed type return mixed
string Parser::get_source_type() {
    if (_sources.unified_type) {
        switch (_sources.source_type) {
            case Src_Abstract::Line:
                if (_sources.tailored) return "tailored";
                return "line";
            case Src_Abstract::Point:
                return "point";
            case Src_Abstract::CutEnd:
                return "cut-end";
            default:
                return "undefined";
        }
    } else return "mixed";
}

//! Get the number of valid injection points
unsigned Parser::get_num_injection_points() {
    return (unsigned) (_injection_points.injection_points.size());
}

//! Gets the PDT-SPACE tissue properties
void Parser::get_tissue_properties (vector<double>& dmin, vector<double>& dmax, vector<double>& guard, 
        unordered_map<unsigned, string>& names) {
    
    dmin.resize(_materials.size() - 1);
    dmax.resize(_materials.size() - 1);
    guard.resize(_materials.size() - 1);
    names.clear();

    names[0] = _materials[0].name;

    for (unsigned i = 1; i < _materials.size(); i++) {
        names[i] = _materials[i].name;
        guard[i - 1] = _materials[i].guardband;

        if (_materials[i].is_tumor) {
            if (_materials[i].d_min < 1e-8) {
                fprintf(stderr, "\033[1;%dmMinimum dose threshold on Tumor CANNOT be ZERO! Exiting...\033[0m\n", 31);
                exit(-1);
            }
            dmin[i - 1] = _materials[i].d_min;

            if (_materials[i].d_max < 1e-8) {
                dmax[i - 1] = _INFINITY_;
            } else {
                dmax[i - 1] = _materials[i].d_max;
            }

            if (guard[i - 1] < 1) {
                fprintf(stderr, "\033[1;%dmWarning: guardband on tumor < 1!\033[0m\n", 33);
            }
        } else {
            dmin[i - 1] = _materials[i].d_min; // Previously hardcoded to 0, keep or not?
            dmax[i - 1] = _materials[i].d_max;

            if (guard[i - 1] > 1) {
                fprintf(stderr, "\033[1;%dmWarning: guardband on OAR > 1!\033[0m\n", 33);
            }
        }
    }
}

//! Gets the tumor region ids in an unordered_set
void Parser::get_tumor_regions (unordered_set<unsigned>& tumor_regions) {
    tumor_regions.clear();

    for (unsigned i = 1; i < _materials.size(); i++) {
        if (_materials[i].is_tumor) tumor_regions.insert(_materials[i].id);
    }
}

//! Gets the map for FullMonte parameters
unordered_map <string, string>& Parser::get_fm_params() {
    return _fm_params_map;
}

//! Gets the map for PDT-SPACE parameters
unordered_map <string, string>& Parser::get_params() {
    return _params_map;
}

//! Gets the map for SA engine parameters
unordered_map <string, string>& Parser::get_sa_params() {
    return _sa_params_map;
}
