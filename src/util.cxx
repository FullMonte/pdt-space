/**
 * @author: Abdul-Amir Yassine
 * @date_created: March 6th, 2018
 *
 * \file util.cxx \brief Helper methods implementation
 * This file implements all the methods inside the namespace Util in util.h
 */

#include "util.h"
#include <cctype>
#include <algorithm>

namespace Util
{

    chrono::steady_clock::time_point BEGIN_TIME;
    chrono::steady_clock::time_point END_TIME;


    //! Print a matrix to a matlab file
    void print_matrix_matlab(FILE* matlab, vector<vector<float>>& mat, string M_name)
    {   
    /*     stringstream mfile_name; */
    /*     mfile_name << "Matlab_tests/" << M_name << ".m"; */
    /*     FILE* matlab = fopen(mfile_name.str().c_str(), "w"); */

        stringstream Ti, Tj, Tx;
        Ti << M_name << "Ti";
        Tj << M_name << "Tj";
        Tx << M_name << "Tx";
        string sTi, sTj, sTx;
        sTi = Ti.str();
        sTj = Tj.str();
        sTx = Tx.str();

        vector<unsigned int> Ti_v, Tj_v;
        vector<float> Tx_v;
        for (unsigned int i = 0; i < mat.size(); i++)
        {
            for (unsigned int j = 0; j < mat[i].size(); j++)
            {
                if (abs(mat[i][j]) > 1e-5)
                {
                    Ti_v.push_back(i);
                    Tj_v.push_back(j);
                    Tx_v.push_back(mat[i][j]);
                }
            }
        }

        print_vector_matlab<unsigned int>(matlab, Ti_v, sTi);
        print_vector_matlab<unsigned int>(matlab, Tj_v, sTj);
        print_vector_matlab<float>(matlab, Tx_v, sTx);
        fprintf(matlab, "%s = triplet_format_to_matrix(%s, %s, %s, %ld, %ld);\n", 
                M_name.c_str(), sTi.c_str(), sTj.c_str(), sTx.c_str(),
                mat.size(), mat[0].size());
        fprintf(matlab, "clear %s %s %s \n", sTi.c_str(), sTj.c_str(),
                        sTx.c_str());

        fclose(matlab);
    }

    // Print a dense matrix to a matlab file
    void print_matrix_matlab_dense(FILE* matlab, vector<vector<float>>& mat, string M_name)
    {   
        fprintf(matlab, "%s = [", M_name.c_str());

        for (unsigned int i = 0; i < mat.size(); i++)
        {
            for (unsigned int j = 0; j < mat[0].size(); j++)
            {
                fprintf(matlab, "%-15f ", mat[i][j]);
            }
            if (i < mat.size() - 1)
                fprintf(matlab, "\n");
        }
        fprintf(matlab, "];\n\n");
    }

    //! This function prints the runtime and usage statistics from the profiler class
    void print_profiler_stat()
    {
        FILE* out = stdout;
        profiler::print_all_task_timer_records( out );
        profiler::clear_all_task_timer_records();

        // get and print resource usage
        char buffer[2000];
        profiler::get_resource_usage( buffer, RUSAGE_SELF );
        fprintf(out, "\n%s", buffer);
        fprintf(out,
        "===========================================================================\n");
        fprintf(out,
        "===============================END OF RUN==================================\n");
        fprintf(out,
        "===========================================================================\n\n");
        fclose(out);
    }

    //! This method returns the current local system time
    bpt::ptime get_local_time()
    {
        // Get current system time
        return bpt::second_clock::local_time();
    }


    //! This method sorts the given vector in descending order and returns the correspoding permutation
    vector<double> get_sorted_vector_idx(const vector<double>& vec_in, vector<int>& permutation_vec_out) 
    {
        vector<element_idx> vec_s(vec_in.size());

        for (unsigned int i = 0; i < vec_in.size(); i++)
        {
            vec_s[i].element = vec_in[i];
            vec_s[i].index = i;
        }

        sort(vec_s.begin(), vec_s.end(), sort_by_element());

        // Copying the new permutations and the sorted vector
        vector<double> sorted_elements(vec_in.size()); 
        permutation_vec_out.clear();
        permutation_vec_out.resize(vec_in.size());
        for (unsigned int i = 0; i < vec_in.size(); i++)
        {
            permutation_vec_out[i] = vec_s[i].index;
            sorted_elements[i] = vec_s[i].element;
        }

        return sorted_elements;
    }

    double compute_slope_regression(const std::vector<double>& x, const std::vector<double> & y) 
    {
        auto x_n = x.size(), y_n = y.size(); 
        if (x_n != y_n) {
            fprintf(stderr, "\033[1;%dmcompute_slope_regression::input vectors sizes do not match! Exiting....\n\033[0m\n", 31);
            std::exit(-1);
        }

        if (x_n <= 1) {
            fprintf(stderr, "\033[1;%dmcompute_slope_regression::input vector sizes are less than 2! Exiting....\n\033[0m\n", 31);
            std::exit(-1);
        }
        
        double sum_x = 0.0, sum_xx = 0.0, sum_y = 0.0, sum_xy = 0.0; 
        for (unsigned i = 0; i < x_n; i++) {
            double x_i = x[i], y_i = y[i];

            sum_x += x_i;
            sum_xx += x_i*x_i;
            sum_y += y_i;
            sum_xy += x_i*y_i;
        }


        double num = static_cast<double>(x_n)*sum_xy - sum_x*sum_y; 
        double denom = static_cast<double>(x_n)*sum_xx - sum_x*sum_x; 

        if (denom < 1e-8) {
            fprintf(stderr, "\033[1;%dmcompute_slope_regression::[Warning]: denominator is 0 ==> No" 
                    " solution ==> returning 0\n\033[0m\n", 33);
            return 0.0;
        }
        
        return num/denom; 
    }

    //! This function disables stdout and stderr by redirecting outputs to /dev/null
    void redirect_standard_streams_to_DEVNULL(int *_piOriginalSTDIN_FILENO, int *_piOriginalSTDOUT_FILENO, int *_piOriginalSTDERR_FILENO) {
        fflush(stdout);
        fflush(stderr);

        *_piOriginalSTDIN_FILENO = dup(STDIN_FILENO);
        *_piOriginalSTDOUT_FILENO = dup(STDOUT_FILENO);
        *_piOriginalSTDERR_FILENO = dup(STDERR_FILENO);

        int devnull = open("/dev/null", O_RDWR);
        dup2(devnull, STDIN_FILENO);
        dup2(devnull, STDOUT_FILENO);
        dup2(devnull, STDERR_FILENO);
        close(devnull);
    }

    //! This function restores stdout and stderr disabled by redirect_standard_streams_to_DEVNULL()
    void restore_standard_streams(int *_piOriginalSTDIN_FILENO, int *_piOriginalSTDOUT_FILENO, int *_piOriginalSTDERR_FILENO) {
        fflush(stdout);
        fflush(stderr);

        dup2(*_piOriginalSTDIN_FILENO, STDIN_FILENO);
        dup2(*_piOriginalSTDOUT_FILENO, STDOUT_FILENO);
        dup2(*_piOriginalSTDERR_FILENO, STDERR_FILENO);
    }
}
