/** 
 * \file Point.cxx \brief Point Implementation
 *
 * This source files implements the different methods for the SP_Point class. Class SP_Point defines a point or a vector in 3D space
 */

#include "read_mesh.h"

#include <cmath>

// Point Class

//! Default Constructor 
SP_Point::SP_Point()
{
    _parent_mesh = NULL;

    _xcoord = _NEG_INFINITY_;
    _ycoord = _NEG_INFINITY_;
    _zcoord = _NEG_INFINITY_;

    _id = -1;

    _tetra = -1;
}

//! Regular Constructor
SP_Point::SP_Point(int id, float xcoord, float ycoord, float zcoord, mesh* parent_mesh)
{
    _parent_mesh = parent_mesh;

    _xcoord = xcoord;
    _ycoord = ycoord;
    _zcoord = zcoord;

    _id = id; 
    _tetra = -1;
}

//! Regular Constructor
SP_Point::SP_Point(float xcoord, float ycoord, float zcoord)
{
    _parent_mesh = nullptr; 
    
    _xcoord = xcoord;
    _ycoord = ycoord; 
    _zcoord = zcoord; 

    _id = -1;
    _tetra = -1;
}

//! Destructor
SP_Point::~SP_Point()
{
}

// Getters

//! This method returns the parent mesh
mesh* SP_Point::get_parent_mesh()
{
    return _parent_mesh;
}

//! This method returns the xcoordinate of the point
float SP_Point::get_xcoord() const
{
    return _xcoord;
}

//! This method returns the ycoordinate of the point
float SP_Point::get_ycoord() const
{
    return _ycoord;
}

//! This method returns the zcoordinate of the point
float SP_Point::get_zcoord() const
{
    return _zcoord;
}

//! This method returns the id of the point
int SP_Point::get_id() const
{
    return _id;
}

//!
//! This method sets the xcoord of the point
//! @param xcoord [in]: the xcoordinate of the point
//!
void SP_Point::set_xcoord(float xcoord)
{
    _xcoord = xcoord;
    _tetra = -1;
}

//!
//! This method sets the ycoord of the point
//! @param ycoord [in]: the ycoordinate of the point
//!
void SP_Point::set_ycoord(float ycoord)
{
    _ycoord = ycoord;
    _tetra = -1;
}

//!
//! This method sets the zcoord of the point
//! @param zcoord [in]: the zcoordinate of the point
//!
void SP_Point::set_zcoord(float zcoord)
{
    _zcoord = zcoord;
    _tetra = -1;
}

//!
//! This method sets the id of the point
//! @param id [in]: the id of the point
//!
void SP_Point::set_id(int id)
{
    _id = id;
}

//! Method to compute the distance from another point 
double SP_Point::get_distance_from(const SP_Point& p2) const 
{
    return sqrt(pow(p2.get_xcoord() - _xcoord, 2) + pow(p2.get_ycoord() - _ycoord, 2) + 
                pow(p2.get_zcoord() - _zcoord, 2));
}

//! Overloads ostream to print the point
std::ostream& operator<<(std::ostream& os, const SP_Point & pt)
{
    os << "(" << pt.get_xcoord() << ", " << pt.get_ycoord() << ", " << pt.get_zcoord() << ")";
    return os;
}

//! Overload the plus operator
SP_Point operator+(const SP_Point & p1, const SP_Point & p2)
{
	SP_Point res(p1._xcoord + p2._xcoord, p1._ycoord + p2._ycoord, p1._zcoord + p2._zcoord);
	return res;
}

//! Overload the minus operator
SP_Point operator-(const SP_Point & p1, const SP_Point & p2)
{
	SP_Point res(p1._xcoord - p2._xcoord, p1._ycoord - p2._ycoord, p1._zcoord - p2._zcoord);
	return res;
}

//! Returns the dot product of two 3D vectors.
double sp_dot(const SP_Point& p1, const SP_Point & p2)
{
	return (p1._xcoord*p2._xcoord) + (p1._ycoord * p2._ycoord) + (p1._zcoord*p2._zcoord); 
}

//! Returns the l2 norm of the given vector 
double sp_norm(const SP_Point& vec) 
{
    return vec.get_distance_from(SP_Point(0.0f, 0.0f, 0.0f)); 
}

//! Divides a vector by a scalar
SP_Point operator/(const SP_Point & vec1, double c)
{
	if (c < DOUBLE_ACCURACY)
	{
		fprintf(stderr, "\033[1;%dmSP_Point constant division::dividing by zero.\nExiting...\033[0m\n", 31); 
		std::exit(-1); 
	}
	
	float a = static_cast<float>(c); 

	SP_Point res(vec1._xcoord/a, vec1._ycoord/a, vec1._zcoord/a); 
	return res; 
}

//! Multiplies a vector by a scalar
SP_Point operator*(double c, SP_Point& vec)
{
	float a = static_cast<float>(c); 
	SP_Point res(a*vec._xcoord, a*vec._ycoord, a*vec._zcoord); 
	return res;
}

//! Cross product of two vectors
SP_Point sp_cross(const SP_Point & vec1, const SP_Point & vec2)
{
    SP_Point res(vec1._ycoord*vec2._zcoord - vec1._zcoord*vec2._ycoord,
                vec2._xcoord*vec1._zcoord - vec1._xcoord*vec2._zcoord, 
                vec1._xcoord*vec2._ycoord - vec1._ycoord*vec2._xcoord);
    return res;
}

//! Normalize a vector
SP_Point sp_normalize(const SP_Point & vec)
{
    double l2_norm = vec.get_distance_from(SP_Point(0.f, 0.f, 0.f)); 

    if (l2_norm < DOUBLE_ACCURACY) {
        return vec;
    }

    return vec/l2_norm;
}

//! Compute determinante of three vectors. The matrix is assumed to be of this shape [v1';v2';v3'] 
double sp_determinant(const SP_Point & v1, const SP_Point & v2, const SP_Point & v3) 
{
    return v1._xcoord*(v2._ycoord*v3._zcoord - v2._zcoord*v3._ycoord) -
           v1._ycoord*(v2._xcoord*v3._zcoord - v2._zcoord*v3._xcoord) + 
           v1._zcoord*(v2._xcoord*v3._ycoord - v2._ycoord*v3._xcoord);
}
