/** 
* \file SP_Line.cxx \brief SP_Line Implementation
*
* This source files implements the different methods for the SP_Line class. Class SP_Line defines a line in 3D space
*/

#include "read_mesh.h"

#include <cmath>
#include <iostream>
#include <random>

// SP_Line Class
std::ostream& operator<<(std::ostream& os, const SP_Line & line)
{
    os << line.end0 << "--------" << line.end1;
    return os;
}

std::ostream& print_to_xml(std::ostream& os, const std::vector<SP_Line> &lines) {
    for (unsigned i = 0; i < lines.size(); i++) {
        os << "    <source type=\"line\">\n";
        os << "      <endpoint location=\"proximal\">\n";
        os << "        <x>" << lines[i].end0.get_xcoord() << "</x>\n";
        os << "        <y>" << lines[i].end0.get_ycoord() << "</y>\n";
        os << "        <z>" << lines[i].end0.get_zcoord() << "</z>\n";
        os << "      </endpoint>\n";
        os << "      <endpoint location=\"distal\">\n";
        os << "        <x>" << lines[i].end1.get_xcoord() << "</x>\n";
        os << "        <y>" << lines[i].end1.get_ycoord() << "</y>\n";
        os << "        <z>" << lines[i].end1.get_zcoord() << "</z>\n";
        os << "      </endpoint>\n";
        os << "    </source>\n";
    }
    return os;
}

//! This method returns the perpendicular distance from point pt to the current line 
double SP_Line::get_distance_pt_line(const SP_Point & pt) const
{
	/*
	 * d = |(end1 - end0)x(end0 - pt)|/|end1 - end0|
	 */

	 // find length of source
	 if (length < 1e-8) // pt source
	 {
	 	return end1.get_distance_from(pt);
	 }

	 // create vector end1 - end0
	 SP_Point end_1_0(end1.get_xcoord() - end0.get_xcoord(), 
	 				  end1.get_ycoord() - end0.get_ycoord(), 
					  end1.get_zcoord() - end0.get_zcoord()); 
	
	// create vector end0 - pt
	SP_Point end_0_pt(end0.get_xcoord() - pt.get_xcoord(), 
					  end0.get_ycoord() - pt.get_ycoord(), 
					  end0.get_zcoord() - pt.get_zcoord()); 

	// find L2 norm of end_1_0
	double norm_1_0 = length;

	// find cross product of end_1_0 and end_0_pt
	SP_Point cross_product(end_1_0.get_ycoord()*end_0_pt.get_zcoord() - end_0_pt.get_ycoord()*end_1_0.get_zcoord(), 
						   -1*(end_1_0.get_xcoord()*end_0_pt.get_zcoord() - end_0_pt.get_xcoord()*end_1_0.get_zcoord()), 
						   end_1_0.get_xcoord()*end_0_pt.get_ycoord() - end_0_pt.get_xcoord()*end_1_0.get_ycoord());

	// find L2 norm of cross_product
	double norm_cross_product = sqrt(pow(cross_product.get_xcoord(), 2) +
									 pow(cross_product.get_ycoord(), 2) +
									 pow(cross_product.get_zcoord(), 2));
	
	return norm_cross_product/norm_1_0; 
}

//! This method generates a unit vector that is perpendicular to the current line in any random direction
SP_Point SP_Line::get_rand_perpendicular_vector()
{
	// https://math.stackexchange.com/questions/2347215/how-to-find-a-random-unit-vector-orthogonal-to-a-random-unit-vector-in-3d/2347293
    //    a- get the vector components of the line source 
    SP_Point l_vec = end1 - end0;
    double l1_x = l_vec.get_xcoord(); 
    double l1_y = l_vec.get_ycoord(); 
    double l1_z = l_vec.get_zcoord();

    //    b- generate vector, p, perpendicular to that vector and normalize it
    double p_x = l1_y - l1_z, p_y = l1_z - l1_x, p_z = l1_x - l1_y; 
    if (abs(p_x) < DOUBLE_ACCURACY && abs(p_y) < DOUBLE_ACCURACY && abs(p_z) < DOUBLE_ACCURACY) // regenarate another vector
    {
        if (abs(l1_x) > DOUBLE_ACCURACY)
        {
            p_x = l1_y; 
            p_y = -l1_x; 
            p_z = 0.0;
        }
        else if (abs(l1_y) > DOUBLE_ACCURACY)
        {
            p_x = 0.0;
            p_y = l1_z; 
            p_z = -l1_y;
        }
        else // they can't all be 0, otherwise it is a point source 
        {
            p_x = l1_z; 
            p_y = 0; 
            p_z = -l1_x;
        }
    }
    SP_Point p(static_cast<float>(p_x), static_cast<float>(p_y), static_cast<float>(p_z));

    SP_Point p_n = sp_normalize(p);  

    //     c- take the cross product of l1xp and generate vector v
    SP_Point v = sp_cross(l_vec, p); 

    // get the normal components of v
    SP_Point v_n = sp_normalize(v);

    //    d- generate a random angle between 0 and 2*PI
    double theta = (2*M_PI*rand())/RAND_MAX; 
    
    //    e- random perpendicular vector = pn*cos(theta) + vn*sin(theta)
    SP_Point rand_dir = (cos(theta)*p_n) + (sin(theta)*v_n);

    //    f- normalize it and return
    return sp_normalize(rand_dir); 
}

/**
 * This method returns the perpendicular (shortest) distance between two lines 
 * @param line [in]: The line to find the distance of the current line from
 * @return double
 */
double SP_Line::get_distance_from(const SP_Line& line) const 
{
    /* 
     * Inspired from stackoverflow.com/questions/2824478/shortest-distance-between-two-line-segments
     */

    if (length < DOUBLE_ACCURACY && line.length < DOUBLE_ACCURACY) { // both point sources
        return end0.get_distance_from(line.end0);
    } else if (length < DOUBLE_ACCURACY) {
        return line.get_distance_pt_line(end0); 
    } else if (line.length < DOUBLE_ACCURACY) {
        return get_distance_pt_line(line.end0); 
    }

    SP_Point l_cross_line = sp_cross(dir, line.dir); 

    double norm = sp_norm(l_cross_line); 

    if (norm < DOUBLE_ACCURACY) {  // parallel
        // check if overlapping 
        double d0 = sp_dot(dir, line.end0 - end0); 
        double d1 = sp_dot(dir, line.end1 - end0); 
        
        if (d0 <= DOUBLE_ACCURACY && d1 <= DOUBLE_ACCURACY) { // line is before this
            if (std::abs(d0) < std::abs(d1)) {
                return end0.get_distance_from(line.end0);
            } else {
                return end0.get_distance_from(line.end1); 
            }
        } else if (d0 >= length - DOUBLE_ACCURACY && d1 >= length - DOUBLE_ACCURACY) { // line is after this 
            if (std::abs(d0) < std::abs(d1)) {
                return end1.get_distance_from(line.end0); 
            } else {
                return end1.get_distance_from(line.end1);
            }
        }
     // overlapping   
        return get_distance_pt_line(line.end0); 
    }
    
    SP_Point t = line.end0 -  end0;

    double det1 = sp_determinant(t, line.dir, l_cross_line);
    double det2 = sp_determinant(t, dir, l_cross_line);
    
    double t0 = det1/(norm*norm), t1 = det2/(norm*norm);

    SP_Point curr_dir = dir, l_dir = line.dir;

    SP_Point p1 = end0 + t0*curr_dir; // projected closest point on current segment 
    SP_Point p2 = line.end0 + t1*l_dir; // projected closes point on given line


    if (t0 < DOUBLE_ACCURACY) {
        p1 = end0; 
    } else if (t0 > length - DOUBLE_ACCURACY) {
        p1 = end1;
    }

    if (t1 < DOUBLE_ACCURACY) {
        p2 = line.end0; 
    } else if (t1 > line.length - DOUBLE_ACCURACY) {
        p2 = line.end1;
    }
    
    double temp = 0.0;
    if (t0 < DOUBLE_ACCURACY || t0 > length - DOUBLE_ACCURACY) {
        temp = sp_dot(l_dir, p1-line.end0);
        if (temp < DOUBLE_ACCURACY) {
            temp = 0.0;
        } else if (temp > line.length - DOUBLE_ACCURACY) {
            temp = line.length;
        }
        p2 = line.end0 + (temp*l_dir); 
    }

    if (t1 < DOUBLE_ACCURACY || t1 > line.length - DOUBLE_ACCURACY) {
        temp = sp_dot(curr_dir, p2-end0);
        if (temp < DOUBLE_ACCURACY) {
            temp = 0.0;
        } else if (temp > length - DOUBLE_ACCURACY) {
            temp = length;
        }
        p1 = end0 + (temp*curr_dir); 
    }

//     return (sp_dot(l_cross_line, end0 - line.end0)/norm); 
    return sp_norm(p1 - p2); 
}

/** 
 * This method rotates the source randomly in space. It changes the contents of the object
 */
void SP_Line::rotate_randomly()
{
    std::random_device rd;  // used to generate a seed for the rng
    std::mt19937 gen(rd()); // standard merseinne-twister engine seeded with rd() 

    std::uniform_real_distribution<float> rand_angle(0, 2*static_cast<float>(M_PI));

    // Get random polar and azimuthal angles and update the location of end1
    float phi = rand_angle(gen); 
    float theta = rand_angle(gen)/2.0f; // [0, M_PI]

    SP_Point new_dir(sin(phi)*cos(theta), sin(phi)*sin(theta), cos(phi)); 

    end1 = end0 + length*new_dir; 

    if (length > DOUBLE_ACCURACY) {
        dir = new_dir; 
    }
}

/**
 * This method returns the intersection between a plane to this line
 * @param n [in]: The normal vector of the plane
 * @param p [in]: A point on the plane
 * @return SP_Point
 */
bool SP_Line::get_intersection_with_plane(const SP_Point & n, const SP_Point & p, SP_Point & res) const {
    SP_Point l = end0 - end1;
    double l_dot_n = sp_dot(l, n);
    if (l_dot_n > 1e-4 || l_dot_n < -1e-4) { // a single injection point exists
        double d = sp_dot(p - end0, n) / l_dot_n;
        res = end0 + d * l;
        return true;
    } else { // line and plane are parallel
        return false;
    }
}
