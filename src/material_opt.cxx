/** 
 * \file material_opt.cxx \brief Material_Properties Implementation
 *
 * This source files implements the different methods for the material optical properties class
 */

#include "read_mesh.h"

// material_opt Class

//! Default Constructor 
material_opt::material_opt()
{
    _parent_mesh = NULL;

    _mu_a = 0.0;
    _mu_s = 0.0;
    _g = 0.0;
    _n = 0.0;

    _id = -1;
}

//! Regular Constructor
material_opt::material_opt(int id, double mu_a, double mu_s, double g, double n, mesh* parent_mesh)
{
    _parent_mesh = parent_mesh;

    _mu_a = mu_a;
    _mu_s = mu_s;
    _g = g;
    _n = n;

    _id = id;
}

//! Destructor
material_opt::~material_opt()
{
}

// Getters

//! This method returns the parent mesh
mesh* material_opt::get_parent_mesh()
{
    return _parent_mesh;
}

//! This method returns the absorption coefficient of the material
double material_opt::get_mu_a()
{
    return _mu_a;
}

//! This method returns the scattering coefficient of the material
double material_opt::get_mu_s()
{
    return _mu_s;
}

//! This method returns the anisotropy coefficient of the material
double material_opt::get_g()
{
    return _g;
}

//! This method returns the deflection angle  of the material
double material_opt::get_n()
{
    return _n;
}

//! This method returns the id of the material
int material_opt::get_id()
{
    return _id;
}


// Setters

//! 
//! This method sets the absorption coefficient of the material
//! @param mu_a [in]: absorption coefficient to be added
//! 
void material_opt::set_mu_a(double mu_a)
{
    _mu_a = mu_a;
}

//! 
//! This method sets the scattering coefficient of the material
//! @param mu_s [in]: scattering coefficient to be added
//! 
void material_opt::set_mu_s(double mu_s)
{
    _mu_s = mu_s;
}

//! 
//! This method sets the anisotropy coefficient of the material
//! @param g [in]: anisotropy coefficient to be added
//! 
void material_opt::set_g(double g)
{
    _g = g;
}

//! 
//! This method sets the deflection of the material
//! @param n [in]: deflection to be added
//! 
void material_opt::set_n(double n)
{
    _n = n;
}

//! 
//! This method sets the id of the material
//! @param id [in]: id to be added
//! 
void material_opt::set_id(int id)
{
    _id = id;
}
