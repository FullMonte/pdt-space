/** 
 * \file src_placer.cxx \brief Source Placement Utilities Implementation
 *
 * This source files implements the different methods for the src_placer class
 */

#include "src_placer.h"
#include "pdt_space_config.h"

//! Default Constructor 
src_placer::src_placer()
{
    _power_uncertainty = 0;
}

//! Destructor
src_placer::~src_placer()
{
}

//! This method reads the output vtk file of Fullmonte and stores the fluence at each element
void src_placer::read_vtk_file(string file_path, unsigned int mesh_size)
{
    fprintf(stderr, "\033[1;%dm\nReading Output VTK file\033[0m\n", 33);


    cout << file_path << endl;
    _list_of_elements_fluence.clear();

    ifstream fin;
    fin.open(file_path);

    if (!fin)
    {
        fprintf(stderr, "\033[1;%dmsrc_placer::read_vtk_file: Could not read "
                        "VTK file %s\033[0m\n", 31, file_path.c_str());
        
        exit(-1);
    }

    string line;
    bool found_fluence = false;
    vector<float> previous_fluence(mesh_size, 0); // adding this vector to account for accumulation error in Fullmonte
    int fluence_num = 0;
    int index = 0; 
    while(getline(fin, line))
    {
        if (line.substr(0, string("Tissue").size()) == "Tissue" || 
            line.substr(0, string("Region").size()) == "Region")
            found_fluence = false;
        
        if (found_fluence && line.substr(0, string("Fluence").size()) == "Fluence")
        {
            // Appending the fiber element number
            _fiber_elements.push_back(atoi(line.substr(string("Fluence").size(), line.find("_") - string("Fluence").size()).c_str()));

            //_source_element_fluence_matrix.push_back(_list_of_elements_fluence);
            append_fluence_vector_to_matrix();
            _list_of_elements_fluence.clear();

            index = 0; 
            
            continue;
        }

        if (found_fluence)
        {
            stringstream str(line);
            float fluence;

            while (str >> fluence)
            {
                _list_of_elements_fluence.push_back(fluence - previous_fluence[index]); // TODO: remove this
                if (fluence-previous_fluence[index] < 0)
                {
                    cerr << "Negative " << fluence << " " << previous_fluence[index] << " " << index << " " << fluence_num << endl;
                    exit(-1);
                }
                else if (index >= static_cast<int>(mesh_size)) 
                {
                    cerr << "src_placer::read_vtk_file: Index " << index << " is out of bounds. Exiting! " << endl;
                    exit(-1);
                }
                previous_fluence[index] = fluence;
                index++;
            }

            fluence_num++;
        }

        if (line.substr(0, string("Fluence").size()) == "Fluence")
        {
            // Appending the fiber element number
            _fiber_elements.push_back(atoi(line.substr(string("Fluence").size(), line.find("_") - string("Fluence").size()).c_str()));

            found_fluence = true;
        }
    }
    append_fluence_vector_to_matrix();
    fprintf(stderr, "\033[1;%dmDone Reading Output!\033[0m\n\n", 36);
}

//! This method returns a vector of boolean indicating ignored rows corresponding to tetras not affected by the optimizer
void src_placer::get_ignored_tetras(const unordered_set<unsigned> & tumor_region_number, 
                mesh* data, const vector<double> & d_max_tissues,
                unsigned int & actual_mesh_size, vector<bool>& tetras_to_consider,
                const vector<double> & last_source_powers)
{
    /** 
     * @details
     * The way we ignore tetra is by looking at the maximum fluence at that tetra from all 
     * sources, and if it is less than D_max of that tetra then ignore (since we assume that
     * the total power limit is one, so the worst case would have all power at that source
     */

    actual_mesh_size = 0;

	tetras_to_consider.clear(); 
    tetras_to_consider.resize(_source_element_fluence_matrix[0].size()); // false if ignored, true if not

    for (unsigned int i = 0; i < _source_element_fluence_matrix[0].size(); i++)
    {

        int mat_id = data->get_list_of_tetrahedra()[i]->get_material_id();
        if (mat_id != 0 && 
            tumor_region_number.find(mat_id) == tumor_region_number.end())
        {
            double max_fluence = 0;
            for (unsigned int j = 0; j < _source_element_fluence_matrix.size(); j++)
            {
                max_fluence = max(max_fluence, (double)_source_element_fluence_matrix[j][i]);
            }

            if (max_fluence + DOUBLE_ACCURACY >= d_max_tissues[mat_id - 1])
            {
                actual_mesh_size++;

                tetras_to_consider[i] = true;
            }
            else
                tetras_to_consider[i] = false;
        }
        else if (tumor_region_number.find(mat_id) != tumor_region_number.end())
        {
#ifdef SIZE_REDUCTION
            double fluence = 0;
            assert (_source_element_fluence_matrix.size() < 6);
            for (unsigned int j = 0; j < _source_element_fluence_matrix.size(); j++)
            {
                fluence += last_source_powers[j] * _source_element_fluence_matrix[j][i];
            }

            if (fluence > d_max_tissues[mat_id - 1] * 1000) {
                tetras_to_consider[i] = false;
            } else {
#endif
                tetras_to_consider[i] = true;

                actual_mesh_size++;
#ifdef SIZE_REDUCTION
            }
#endif
        }
        else
            tetras_to_consider[i] = false;
    }
}

//!
//! \deprecated
//! This method runs the tcl script specified
//!
void src_placer::run_tcl_script(string tcl_file_path)
{
    fprintf(stderr, "\033[1;%dmRunning TCL script: %s\033[0m\n", 33, tcl_file_path.c_str());

    int command = system(("tclmonte.sh " + tcl_file_path).c_str());

    if (command == 0)
        fprintf(stderr, "\033[1;%dmDone running script!\033[0m\n\n", 36);
}

//!
//! \deprecated
//! This method reads the output of the IPOPT optimization (as a vector of source powers)
//! It then generates a script of the composite sources to run FullMonte
//!
void src_placer::read_optimization_output(string filename, const vector<tetrahedron*>& sources, 
                                         string mesh_path, string script_name, mesh* /*data*/,
                                         const unordered_set<unsigned> & tumor_region_number, 
                                         bool read_data_from_vtk)
{
// TODO: Deprecate this 
    fprintf(stderr, "\033[1;%dmReading Optimization Output File: %s\033[0m\n", 33, filename.c_str());
    
    ifstream fin;
    fin.open(filename);

    if (!fin)
    {
        fprintf(stderr, "\033[1;%dmsrc_placer::read_optimization_output: Could not read"
                        "Opt. file %s\033[0m\n", 31, filename.c_str());
    
        exit(-1);
    }

    vector<float> source_powers(sources.size());
    unsigned int index = 0;
    string line;
    while(getline(fin, line))
    {
        if (line.substr(0, string("x[").size()) == "x[")
        {
            stringstream str(line);
            string word;
            while(str >> word);

            float source_power = stof(word);

            source_powers[index] = source_power;

            index++;
        }
    }

    assert(sources.size() == index);

    fprintf(stderr, "\033[1;%dmDone Reading Optimization Output!\033[0m\n\n", 36);

    // Generating script
    vector<tetrahedron*> sources_to_run; 
    vector<float> source_powers_to_run;
    int count_sources_to_run = 0; 
    for (unsigned int i = 0; i < sources.size(); i++)
    {
        if (tumor_region_number.find((unsigned) sources[i]->get_material_id()) != tumor_region_number.end() && 
			source_powers[i] > SOURCE_POWER_MIN)
        {
            sources_to_run.push_back(sources[i]);
            source_powers_to_run.push_back(source_powers[i]);

#ifdef VERBOSE_PLACER 
            cout << "Source " << count_sources_to_run << " Coordinates: " << endl;
            
            cout << "   P1: " << data->get_list_of_points()[sources[i]->get_p1_id() - 1]->get_xcoord() << " "
                        << data->get_list_of_points()[sources[i]->get_p1_id() - 1]->get_ycoord() << " "
                        << data->get_list_of_points()[sources[i]->get_p1_id() - 1]->get_zcoord() << endl;
            cout << "   P2: " << data->get_list_of_points()[sources[i]->get_p2_id() - 1]->get_xcoord() << " "
                        << data->get_list_of_points()[sources[i]->get_p2_id() - 1]->get_ycoord() << " "
                        << data->get_list_of_points()[sources[i]->get_p2_id() - 1]->get_zcoord() << endl;
            cout << "   P3: " << data->get_list_of_points()[sources[i]->get_p3_id() - 1]->get_xcoord() << " "
                        << data->get_list_of_points()[sources[i]->get_p3_id() - 1]->get_ycoord() << " "
                        << data->get_list_of_points()[sources[i]->get_p3_id() - 1]->get_zcoord() << endl;
            cout << "   P4: " << data->get_list_of_points()[sources[i]->get_p4_id() - 1]->get_xcoord() << " "
                        << data->get_list_of_points()[sources[i]->get_p4_id() - 1]->get_ycoord() << " "
                        << data->get_list_of_points()[sources[i]->get_p4_id() - 1]->get_zcoord() << endl;

            cout << "   Region: " << sources[i]->get_material_id() << endl;
#endif

            count_sources_to_run++;
        }
    }

    cerr << "Number of sources to use is: " << count_sources_to_run << endl;

    // TODO;
    return;

    if (read_data_from_vtk)
        make_tcl_script_absorption_multiple_sources(script_name, 
                                            mesh_path, sources_to_run, source_powers_to_run,
                                            true);
    else
        make_tcl_script_absorption_multiple_sources(script_name, 
                                            mesh_path, sources_to_run, source_powers_to_run,
                                            false);
    
    run_tcl_script("TCL_scripts/"+script_name);
}

//!
//! \deprecated
//! This function is modified to specify the candidate sources we want
//!
vector<tetrahedron*> src_placer::get_candidate_sources(mesh* data, const unordered_set<unsigned> & tumor_region_number,
                                                unsigned int number_of_tetra_to_skip)
{

    /*
     * Right now, the tumor is a cube from -1 to 1 in all directions
     * The candidate sources are chosen to be every other 10th element 
     * in the tumor region with an additional 0.5 units in all directions
     */

    TRegion* reg = new TRegion;

    // Defining the tumor region with additional boundary
    reg->x_min = (float) 13; //-2.7;
    reg->y_min = (float) 64; //-2.7;
    reg->z_min = (float) 3;  //-2.7;
    reg->x_max = (float) 20; //-1.3;
    reg->y_max = (float) 73; //-1.3;
    reg->z_max = (float) 9;  //-1.3;
//    13; //
//    64; //
//    3;  // 
//    20; //
//    73; //
//    9;  //
    tetrahedron * candidate;
//     SP_Point *c_p1, *c_p2, *c_p3, *c_p4;
    vector<tetrahedron*> sources;

    int current_num = 0;
    for (unsigned int i = 0; i < data->get_mesh_size(); i++)
    {
        candidate = data->get_list_of_tetrahedra()[i];

//         c_p1 = data->get_list_of_points()[candidate->get_p1_id() - 1];
//         c_p2 = data->get_list_of_points()[candidate->get_p2_id() - 1];
//         c_p3 = data->get_list_of_points()[candidate->get_p3_id() - 1];
//         c_p4 = data->get_list_of_points()[candidate->get_p4_id() - 1];

//         if (check_if_point_in_tumor_plus_region(c_p1, reg) || 
//             check_if_point_in_tumor_plus_region(c_p2, reg) ||
//             check_if_point_in_tumor_plus_region(c_p3, reg) || 
//             check_if_point_in_tumor_plus_region(c_p4, reg))
        if (tumor_region_number.find((unsigned)candidate->get_material_id()) != tumor_region_number.end())
        {
            if (current_num % number_of_tetra_to_skip == 0)
            {
                sources.push_back(candidate);
            }
            current_num++;
        }
    }

    _num_tumor_elements = current_num;

    return sources;
}


// Getters

//! This method returns the fluence at each element in the mesh
vector<float>& src_placer::get_list_of_elements_fluence()
{
    return _list_of_elements_fluence;
}

//! This method returns the matrix of the fluence due to each light source at each element
vector<vector<float>> & src_placer::get_source_element_fluence_matrix()
{
    return _source_element_fluence_matrix;
}

//! This method returns the vector fiber elements that specifies the fiber each element belongs to
vector<unsigned int> & src_placer::get_fiber_elements()
{
    return _fiber_elements;
}

//! This method returns the number of tumor elements in the mesh
unsigned int src_placer::get_num_tumor_elements()
{
    return _num_tumor_elements;
}

//! This method returns the total energy specified by the user
double src_placer::get_total_energy()
{
    return _total_energy;
}

//! This method returns the number of photon packets used in FullMonte
double src_placer::get_num_packets()
{
    return _num_packets;
}

// Setters
//! This method appends the computed vector of fluence at each element due to the last source added
void src_placer::append_fluence_vector_to_matrix()
{
    long unsigned int current_num_rows = _source_element_fluence_matrix.size();

    _source_element_fluence_matrix.resize(current_num_rows + 1);

    _source_element_fluence_matrix[current_num_rows] = _list_of_elements_fluence;
}

//! This method sets the total energy
void src_placer::set_total_energy(double total_energy)
{
    _total_energy = total_energy;
}

//! This method sets the number of photons used in FullMonte
void src_placer::set_num_packets(double num_packets)
{
    _num_packets = num_packets;
}


//! This method sets the power uncertainty factor (variance)
void src_placer::set_power_uncertainty(double sigma) 
{
    _power_uncertainty = sigma;
}

// Helper private methods
//!
//! This method checks if the given point is in the given tumor region in addition to some boundary
//! This method is used in defining the candidate sources
//!
bool src_placer::check_if_point_in_tumor_plus_region(SP_Point* p, TRegion* reg)
{
    float upper_xlim = reg->x_max,
          lower_xlim = reg->x_min,
          upper_ylim = reg->y_max,
          lower_ylim = reg->y_min,
          upper_zlim = reg->z_max,
          lower_zlim = reg->z_min;

    if (p->get_xcoord() < upper_xlim && p->get_xcoord() > lower_xlim &&
        p->get_ycoord() < upper_ylim && p->get_ycoord() > lower_ylim &&
        p->get_zcoord() < upper_zlim && p->get_zcoord() > lower_zlim
       )
        return true;

    return false;
}

/**
 * This method multiples the matrix of the fluence with the current source power  
 * To find the current fluence at each element (Note that the matrix is transposed)
 * @param mesh_size             [in] number of tetrahedra in the mesh
 * @param current_source_powers [in] current power allocation of the sources 
 * @param current_fluence_out   [out] stores the result
 */
void src_placer::compute_current_fluence(unsigned int mesh_size, 
                                         const vector<double>& current_source_powers,
                                               vector<double>& current_fluence_out)
{
    current_fluence_out.clear();
    current_fluence_out.resize(mesh_size);


    for (unsigned int i = 0; i < mesh_size; i++)
    {
        current_fluence_out[i] = 0; 
        for (unsigned int j = 0; j < current_source_powers.size(); j++)
        {
            if (_source_element_fluence_matrix[0].size() == mesh_size + 1)
                current_fluence_out[i] += _source_element_fluence_matrix[j][i+1]*current_source_powers[j];
            else
                current_fluence_out[i] += _source_element_fluence_matrix[j][i]*current_source_powers[j];
        }
    }
}


/** 
 * This method computes the v_alpha of the different tissue types
 * @param current_fluences     [in] A vector of the current fluence distribution at each tetrahedron
 * @param tetra_types          [in] Region ID at each tetrahedron
 * @param tetra_volumes        [in] volume of each tetrahedron
 * @param dose_thresholds      [in] PDT dose threshold value at each tetrahedron
 * @param tumor_region_index   [in] Region ID of the tumor tissue(s)
 * @param tumor_V_alpha_target [in] The percentage of the dose at which to compute the volume receiving that dose for each tissue
 */
vector<double> src_placer::compute_v_alpha_tissues(const vector<double>& current_fluences, 
                                   const vector<unsigned int>& tetra_types,
                                   const vector<double>& tetra_volumes,
                                   const vector<double>& dose_thresholds,
                                   const unordered_set<unsigned>& tumor_region_index, 
                                   double tumor_V_alpha_target)
{

//     double tumor_V_alpha_target = 0.9; // (0, 1) mapping to (0%, 100%)

    unsigned int num_regions = (unsigned int)dose_thresholds.size(); 

    vector<double> v90_tissues(num_regions);

    vector<double> region_volumes(num_regions ,0.0); 
    for (unsigned int i = 0; i < tetra_types.size(); i++)
    {
        if (tetra_types[i] != 0)
        {
            region_volumes[tetra_types[i] - 1] += tetra_volumes[i];
        }
    }
    

    // Checking which tetra exceeded its dose
    vector<double> violated_volume_by_region(num_regions, 0.0);
    for (unsigned int i = 0; i < tetra_types.size(); i++)
    {
        if (tetra_types[i] != 0)
        {
            if (tumor_region_index.find(tetra_types[i]) != tumor_region_index.end())
            {
                if (current_fluences[i] < tumor_V_alpha_target*dose_thresholds[tetra_types[i] - 1])
                    violated_volume_by_region[tetra_types[i] - 1] += tetra_volumes[i];
            }
            else
            {
                if (current_fluences[i] > tumor_V_alpha_target*dose_thresholds[tetra_types[i] - 1]) // didnt multiply by 0.9 cuz an additional guardband of 10% on the healthy
                                                                                    // tissues was taken
                {
                    violated_volume_by_region[tetra_types[i] - 1] += tetra_volumes[i];
                }
            }
        }
    }

    for (unsigned int i = 0; i < num_regions; i++)
    {
        // show tumor as a coverage percentage and healthy tissues as damage volume
        if (tumor_region_index.find(i+1) != tumor_region_index.end())
            v90_tissues[i] = 100*(1 - (violated_volume_by_region[i]/region_volumes[i]));
        else
            v90_tissues[i] = violated_volume_by_region[i]*1e-3; // to convert to cm^3
    }

    return v90_tissues;
}


