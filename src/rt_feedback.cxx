/**
 * \file rt_feedback.cxx 
 * \brief RTFeedback class implementation 
 *
 * \author: Abed Yassine
 * \date: July 20th, 2020
 */


#include "pdt_space_config.h"
#include "rt_feedback.h"
#include "run_fullmonte.h"
#include "profiler.h" 
#include "util.h"

#include <iostream>
#include <fstream>
#include <sstream>
#include <cstdlib>

//! Regular constructor
RtFeedback::RtFeedback(WrapperFullMonteSW* fullmonte_wrapper, 
                    const std::vector<Src_Abstract*>& curr_placement,
                    long long unsigned num_packets, 
                    unsigned tumor_material_id, double tumor_volume, unsigned rand_seed,
                    float detector_radius, float detector_na, 
                    unsigned num_lut_sims) :
            _fullmonte_wrapper(fullmonte_wrapper),
            _num_packets(num_packets), 
            _tumor_material_id(tumor_material_id),
            _tumor_volume(tumor_volume), 
            _detector_radius(detector_radius),
            _detector_na(detector_na),
            _num_lut_sims(num_lut_sims)
{
    if (fullmonte_wrapper == nullptr) {
        fprintf(stderr, "\033[1;%dm[ERROR] RtFeedback::FullMonte wrapper object is pointing to NULL! Exiting....\033[0m\n", 31); 
        std::exit(-1); 
    }

    _curr_placement = curr_placement;

    /** 
     * Define the normal random generator for the  scattering and absorption coefficients, 
     * where the mean is the initial optical properties of the tumor and the standard deviation is 10% of those
     */
    _original_tumor_properties = _fullmonte_wrapper->get_material_set()->material(_tumor_material_id); 
    float mean_absorption = _original_tumor_properties->absorptionCoeff(); 
    float mean_scattering = _original_tumor_properties->scatteringCoeff(); 

    std::cout << "Original mu_a: " << mean_absorption << " original mu_s: " << mean_scattering << std::endl;
    
    _mt_random_generator.seed(rand_seed); 
    _scattering_normal_dist = get_distribution(mean_scattering, 0.1f*mean_scattering);
    _absorption_normal_dist = get_distribution(mean_absorption, 0.1f*mean_absorption);
}

/**
 * This method emulates light dosimetry of the current source configuration by using one source at a time and the rest as detectors
 * @param sources a vector of the current source configuration 
 * @param source_powers a vector that contains the current source powers 
 * @param detected_weights [output] a vector of vectors that stores the detected weights at each detector after running each source 
                                    separatrely
 */
void RtFeedback::emulate_dosimetry(const std::vector<Src_Abstract*>& sources, const std::vector<float> & source_powers, 
                std::vector<std::vector<float>>& detected_weights)
{
    profiler fm_engine("Running FM with Detectors"); 
 
    // TODO: Add a check for the type of sources, as we do not support cut-end fibers or point sources for this 
    
    // create a vector of sources 
    std::vector<Source::Abstract*> srcs(sources.size()); 
    for (unsigned i = 0; i < sources.size(); i++)
    {
        auto src = sources[i]; 

        srcs[i] = src->get_fm_source();
    }

    // normalize the powers
    std::vector<float> powers = source_powers; 
    float sum_powers = 0.0f; 
    for (unsigned i = 0; i < powers.size(); i++) {
        sum_powers += powers[i];
    }
    for (unsigned i = 0; i < powers.size(); i++) {
        powers[i] = powers[i]/sum_powers;
    }

    _fullmonte_wrapper->run_multiple_fullmonte_with_detectors(srcs, detected_weights, _detector_radius, _detector_na, _num_packets); 

    // multiply detected weights by actual power of the source 
    for (unsigned i = 0; i < detected_weights.size(); i++) {
        for (unsigned j = 0; j < detected_weights[i].size(); j++) {
            detected_weights[i][j] *= powers[i]; 
        }
    }

    // clean up memory
    for (unsigned i = 0; i < srcs.size(); i++) {
        if (srcs[i]) delete srcs[i];
    }
}

/** 
 * This method generates random material optical properites for the tumor and returns a new materialSet
 * @return a new material set containing the new optical properties of the tumor along with the old ones for the other tissues 
 */ 
MaterialSet* RtFeedback::generate_random_op() 
{
    MaterialSet* curr_materials = _fullmonte_wrapper->get_material_set()->clone();

    // Call normal distribution 
    float mu_a_new = _absorption_normal_dist(_mt_random_generator); 
    float mu_s_new = _scattering_normal_dist(_mt_random_generator);

    std::cout << mu_a_new << " " << mu_s_new << std::endl;
    std::cout << "Actual mu_eff = " << std::sqrt(3*mu_a_new*(mu_a_new +
        mu_s_new*(1-_original_tumor_properties->anisotropy()))) << std::endl;

    Material* new_tumor_ops = new Material(mu_a_new, mu_s_new, _original_tumor_properties->anisotropy(), 
                    _original_tumor_properties->refractiveIndex());

    curr_materials->set(_tumor_material_id, new_tumor_ops); 

    return curr_materials; 
}

/** 
 * This method generates random material optical properites for all tissues and returns a new materialSet
 * @return a new material set containing the new optical properties of all the tissues 
 */ 
MaterialSet* RtFeedback::generate_random_op_all() 
{
    MaterialSet* curr_materials = _fullmonte_wrapper->get_material_set()->clone();

    for (unsigned i = 1; i < curr_materials->size(); i++)
    {
        float old_mu_a = curr_materials->material(i)->absorptionCoeff(); 
        float old_mu_s = curr_materials->material(i)->scatteringCoeff(); 

        RNG sca_normal_dist = get_distribution(old_mu_s, 0.1f*old_mu_s);
        RNG abs_normal_dist = get_distribution(old_mu_a, 0.1f*old_mu_a);
        
        // Call normal distribution 
        float mu_a_new = abs_normal_dist(_mt_random_generator); 
        float mu_s_new = sca_normal_dist(_mt_random_generator);

        std::cout << mu_a_new << " " << mu_s_new << std::endl;
        
        if (i == _tumor_material_id) {
            std::cout << "Actual mu_eff = " << std::sqrt(3*mu_a_new*(mu_a_new +
               mu_s_new*(1-_original_tumor_properties->anisotropy()))) << std::endl;
        }

        Material* new_ops = new Material(mu_a_new, mu_s_new, curr_materials->material(i)->anisotropy(), 
                        curr_materials->material(i)->refractiveIndex());

        curr_materials->set(i, new_ops); 
    }

    return curr_materials; 
}

/**
 * This method performs the overall process of online monitoring and realtime feedback. 
 * It starts with emulating dosimetry with random optical properties for the tumor. 
 * Then it uses the detected values to do spectroscopy to recover mu_eff 
 * From the mu eff, it calls an ML inference model to recover optical properties. 
 *
 * @param sources:                       current configuration of the source placement 
 * @param source_powers:                 current power allocation to the sources
 * @param new_absorption_coeff [output]: recovered value of absorption coefficient
 * @param new_scattering_coeff [output]: recovered value of scattering coefficient
 */
void RtFeedback::recover_tumor_optical_properties(const std::vector<Src_Abstract*>& sources, 
                        const std::vector<float>& source_powers, 
                        float & new_absorption_coeff, float & new_scattering_coeff)
{
    // 1- Generate random optical properties 
    // TODO: This should be done only once
    MaterialSet* new_ms = generate_random_op_all(); 
//     MaterialSet* new_ms = generate_random_op(); 

    // 2- Update the FullMonte kernel materials
    _fullmonte_wrapper->update_kernel_materials(new_ms); 

    for (unsigned i = 0; i < new_ms->size(); i++) {
        auto mat = new_ms->material(i); 
        std::cout << mat->absorptionCoeff() << " " << mat->scatteringCoeff() << " " << mat->anisotropy() << " " << mat->refractiveIndex() << std::endl;
    }

    // 3- Emulate dosimetry
    std::vector<std::vector<float>> detected_weights;
    emulate_dosimetry(sources, source_powers, detected_weights); 

	// Multiplier to transform detected weights from 0.5mm radius to 1mm radius
	float slope_to_1mm = 2.314641f; // Was found experimentally based on a linear regression

    std::cout << "Printing detected weights: " << std::endl;
    for (unsigned i = 0; i < detected_weights.size(); i++) {
        for (unsigned j = 0; j < detected_weights[i].size(); j++) {
            std::cout << std::fixed << std::setprecision(8) << detected_weights[i][j] << " ";

			detected_weights[i][j] *= slope_to_1mm; 
        }
        std::cout << std::endl;
    }

    // Choose highest m detectors 
    unsigned num_highest = static_cast<unsigned>(detected_weights.size()); // TODO: make input 
    std::vector<std::vector<float>> highest_detected_weights;
    get_highest_detected_weights(detected_weights, highest_detected_weights, num_highest); 
    
    std::cout << "Printing highest detected weights: " << std::endl;
    for (unsigned i = 0; i < highest_detected_weights.size(); i++) {
        for (unsigned j = 0; j < highest_detected_weights[i].size(); j++) {
            std::cout << std::fixed << std::setprecision(8) << highest_detected_weights[i][j] << " ";
        }
        std::cout << std::endl;
    }

    // 4- Call spectroscopy: 
    std::vector<double> mu_effs;
    if (_mu_effs_lut.empty()) {
        mu_effs = recover_mu_effs(sources, highest_detected_weights); 
    } else {
        mu_effs = recover_mu_effs_lut(sources, detected_weights); 
    }

    std::cout << "Recovered mu_effs: ";
    for (auto mu_eff : mu_effs) {
        std::cout << mu_eff << " ";
    }
    std::cout << std::endl;
    
    // 5- Dump features
    vector<double> features = get_features_vector(detected_weights, mu_effs, 
                            new_ms->material(_tumor_material_id)->absorptionCoeff(), 
                            new_ms->material(_tumor_material_id)->scatteringCoeff());

    // 6- Call python ML model inference 
    string py_command = "python " + 
            string(PDT_SPACE_SRC_DIR) + "/python/pdt_space_rt_feedback_ml.py -m ann -d test_data.csv -o test_output.csv";
    int sys_call = system(py_command.c_str());
    if (sys_call == -1) {
        fprintf(stderr, "\033[1;%dmrecover_tumor_optical_properties::[ERROR] system call for inference return error..\n\033[0m", 33);
        std::exit(-1); 
    }

    // 7- Read inference results
    ifstream in_file;
    in_file.open("test_output.csv"); 
    string line; 
    in_file >> line;
    stringstream ss(line); 
    string token; 
    std::getline(ss, token, ',');
    new_absorption_coeff = static_cast<float>(atof(token.c_str()));
    std::getline(ss, token, ',');
    new_scattering_coeff = static_cast<float>(atof(token.c_str()));


    // 7- Update FM kernel properties TODO
    MaterialSet* curr_materials = _fullmonte_wrapper->get_material_set()->clone();

    Material* new_tumor_ops = new Material(new_absorption_coeff, new_scattering_coeff, _original_tumor_properties->anisotropy(), 
                    _original_tumor_properties->refractiveIndex());

    curr_materials->set(_tumor_material_id, new_tumor_ops); 
    _fullmonte_wrapper->update_kernel_materials(curr_materials); 

    std::cout << "Recovered optical properties are: " << new_absorption_coeff << " " << new_scattering_coeff << std::endl;
}

/**
 * This method performs a linear fit to recover the effective attenuation coefficient from a set of detected weights at 
 * certain detectors in the mesh. 
 * @param sources:          current configuration of the source placement 
 * @param detected_weights: a vector of vectors that stores the detected weights at each detector after running each source 
 *                          separately
 * @return                  a vector of size equal to number of sources, each entry of which is the recovered mu_eff at that 
                            source. If regression is not possible, we insert a zero for the attenuation coefficient 
 */
std::vector<double> RtFeedback::recover_mu_effs(const std::vector<Src_Abstract*>& sources, 
                                    const std::vector<std::vector<float>>& detected_weights)
{
    profiler prof_rec_mu_eff ("Recovering mu_eff from DA"); 

    // First normalize detected weights 
    std::vector<std::vector<double>> normalized_detected_weights(detected_weights.size());
    for (unsigned i = 0; i < detected_weights.size(); i++) 
    {
        float max_detected_weights = 0.0; 
        for (unsigned j = 0; j < detected_weights[i].size(); j++) {
            max_detected_weights = std::max(max_detected_weights, detected_weights[i][j]);
        }

        normalized_detected_weights[i].resize(detected_weights[i].size());
        for (unsigned j = 0; j < detected_weights[i].size(); j++) {
            if (max_detected_weights > DOUBLE_ACCURACY) {
                normalized_detected_weights[i][j] = static_cast<double>(detected_weights[i][j]/max_detected_weights);
            } else {
                normalized_detected_weights[i][j] = 0.0;
            }
        }
        
    }

    // Regression
    std::vector<double> mu_effs;
    for (unsigned i = 0; i < sources.size(); i++) 
    {
        std::vector<double> dists; // x values of linear fit
        std::vector<double> log_phis; // y values of linear fit
        for (unsigned j = 0; j < sources.size(); j++) {
            
            if (i == j) {
                continue;
            }

            double dist = ((Src_Line*)(sources[i]))->get_distance_from(*((Src_Line*)(sources[j]))); // TODO: if we store the sources as a member variable (which makes
                                                             // sense for realtime dosimetry), we can calculate the distances in advance
            
            if (normalized_detected_weights[i][j] > DOUBLE_ACCURACY) {
                dists.push_back(dist); 
                log_phis.push_back(std::log(normalized_detected_weights[i][j]*dist)); // TODO: This might be dist^2 for line sources
            }
            if (normalized_detected_weights[j][i] > DOUBLE_ACCURACY) {
                dists.push_back(dist); 
                log_phis.push_back(std::log(normalized_detected_weights[j][i]*dist));
            }
        }
        if (dists.size() > 1) {
            double mu_eff = Util::compute_slope_regression(dists, log_phis);
            if (mu_eff < 0) {
                fprintf(stderr, "\033[1;%dmrecover_mu_effs::[WARNING] mu_eff is negative. Negating...\n\033[0m", 33);
//                 std::exit(-1); 
                mu_eff *= -1;
            } else if (mu_eff < DOUBLE_ACCURACY) {
                fprintf(stderr, "\033[1;%dmrecover_mu_effs::[WARNING] mu_eff is zero.\n\033[0m", 33);
            }
            mu_effs.push_back(mu_eff); 
        }
        else {
            fprintf(stderr, "\033[1;%dmrecover_mu_effs::[WARNING] size zero for regression. Ignoring this source.\n\033[0m", 33);
            mu_effs.push_back(0.0); 
        }
    }
    return mu_effs;
}

/** 
 * This method returns a matrix of detected weights with only the num_highest filled and the rest are zeros
 */ 
void RtFeedback::get_highest_detected_weights(const std::vector<std::vector<float>> & detected_weights, 
                                        std::vector<std::vector<float>> & highest_detected_weights, 
                                        unsigned num_highest)
{
    profiler prof_highest_det ("Extracting the higest detecte energies");
    highest_detected_weights = detected_weights;
    if (detected_weights.size() <= num_highest) {
        return;
    }

    for (unsigned i = 0; i < highest_detected_weights.size(); i++) {
        std::vector<std::pair<unsigned, float>> temp_dw_idx(highest_detected_weights[i].size());
        for (unsigned j = 0; j < highest_detected_weights[i].size(); j++) {
            temp_dw_idx[j].first = j;
            temp_dw_idx[j].second = highest_detected_weights[i][j];
        }

        std::sort(temp_dw_idx.begin(), temp_dw_idx.end(), [] (const std::pair<unsigned, float> & d1, 
                    const std::pair<unsigned, float> & d2) -> bool { return d1.second > d2.second;});
    
        // zero out any thing after num_highest 
        for (unsigned j = num_highest; j < highest_detected_weights[i].size(); j++) {
            highest_detected_weights[i][temp_dw_idx[j].first] = 0.0f;
        }
    }
}

/** 
 * This method runs Fullmonte _num_lut_sims times each with a different mu_eff and buils a lookup table for the detected weights
 * for each source. This lut is used in recovering the effective attenuation coefficient. 
 * @param sources a vector of the current source configuration 
 * @param source_powers a vector that contains the current source powers 
 */
void RtFeedback::build_lut(const std::vector<Src_Abstract*>& sources, const std::vector<float>& source_powers)
{
    _mu_effs_lut.clear(); 
    _mu_effs_lut.resize(sources.size()); 
    for (unsigned i = 0; i < _num_lut_sims; i++) 
    {
        // 1- Generate random optical properties 
        MaterialSet* new_ms = generate_random_op(); 

        // 2- Update the FullMonte kernel materials
        _fullmonte_wrapper->update_kernel_materials(new_ms); 

        for (unsigned j = 0; j < new_ms->size(); j++) {
            auto mat = new_ms->material(j); 
            std::cout << mat->absorptionCoeff() << " " << mat->scatteringCoeff() << " " << mat->anisotropy() << " " << mat->refractiveIndex() << std::endl;
        }

        // 3- Emulate dosimetry
        std::vector<std::vector<float>> detected_weights;
        emulate_dosimetry(sources, source_powers, detected_weights); 

        // 4- update lookup table // TODO ignore zeros 
        auto mat = new_ms->material(_tumor_material_id);
        double new_mu_eff = std::sqrt(3*mat->absorptionCoeff()*(mat->absorptionCoeff() + mat->reducedScatteringCoeff())); 
        std::cout << "New mu_eff: " << new_mu_eff << std::endl;
        std::cout << "Printing detected weights: " << std::endl;
        for (unsigned k = 0; k < detected_weights.size(); k++) {
            for (unsigned j = 0; j < detected_weights[k].size(); j++) {
                std::cout << std::fixed << std::setprecision(8) << detected_weights[k][j] << " ";
            }
            std::cout << std::endl;
            
            key_detected_weights new_kdw(detected_weights[k]); 

            if (_mu_effs_lut[k].find(new_kdw) == _mu_effs_lut[k].end()) {
                _mu_effs_lut[k].insert(std::make_pair(new_kdw, new_mu_eff));
            }
        }
    }

    std::cout << "Printing maps: " << std::endl;
    for (unsigned i = 0; i < _mu_effs_lut.size(); i++) {
        std::cout << "Map " << i << std::endl;
        for (auto it = _mu_effs_lut[i].begin(); it != _mu_effs_lut[i].end(); it++) {
            key_detected_weights kdw = it->first; 
            double mu_eff = it->second; 

            for (auto dw : kdw.detected_weights) 
                std::cout << dw << " " ; 
            std::cout << "      |     " << mu_eff << std::endl;
        }
    }
}

/**
 * This method recovers the effective attenuation coefficient from a set of detected weights at 
 * certain detectors in the mesh based on a look up table. If the set of detected weights is in the table,
 * it returns the corresponding mu_eff, otherwise it takes the average of the lower-bound and upper-bound keys
 * @param sources:          current configuration of the source placement 
 * @param detected_weights: a vector of vectors that stores the detected weights at each detector after running each source 
 *                          separately
 * @return                  a vector of size equal to number of sources, each entry of which is the recovered mu_eff at that 
                            source. If regression is not possible, we insert a zero for the attenuation coefficient 
 */
std::vector<double> RtFeedback::recover_mu_effs_lut(const std::vector<Src_Abstract*>& sources, 
                                    const std::vector<std::vector<float>>& detected_weights)
{
    vector<double> mu_effs;
    if (_mu_effs_lut.empty()) {
        fprintf(stderr, "\033[1;%dm[ERROR]RtFeedback::recover_mu_effs_lut::"
                "Look-up table size is 0. Exiting....\033[0m\n", 31); 
        std::exit(-1); 
    }

    if (_mu_effs_lut.size() != sources.size()) {
        fprintf(stderr, "\033[1;%dm[ERROR]RtFeedback::recover_mu_effs_lut::"
                "Look-up table size does not match number of sources/ Exiting... \033[0m\n", 31); 
        std::exit(-1); 
    }

    if (sources.size() != detected_weights.size()) {
        fprintf(stderr, "\033[1;%dm[ERROR]RtFeedback::recover_mu_effs_lut::"
                "Detected weight matrix row dimension does not match number of sources. Exiting....\033[0m\n", 31); 
        std::exit(-1); 
    }
    
    for (unsigned i = 0; i < sources.size(); i++) {
        if (_mu_effs_lut[i].empty()) {
            mu_effs.push_back(0.0); 
            continue;
        }

        key_detected_weights new_kdw(detected_weights[i]); 
        
        auto it = _mu_effs_lut[i].find(new_kdw);
        if (it != _mu_effs_lut[i].end()) { // found a similar enrty
            mu_effs.push_back(it->second); 
        } else {
            // find upper (it is map::lower_bound) and lower (it is map::lower_bound - 1) bounds
            auto upper_bound = _mu_effs_lut[i].lower_bound(new_kdw); 
            auto lower_bound = upper_bound;

            if (upper_bound == _mu_effs_lut[i].begin()) { // there is no lower bound. return this mu_eff
                mu_effs.push_back(upper_bound->second); 
            } else if (upper_bound == _mu_effs_lut[i].end()) { // there is no upper bound. return last mu_eff
                lower_bound--; 
                mu_effs.push_back(lower_bound->second);
            } else { // return average 
                lower_bound--; 
                mu_effs.push_back((lower_bound->second + upper_bound->second)/2);
            }
        }
    }

    return mu_effs;
}

/**
 * This method generates training data for the ML model. It writes to a file the following:
 * 1- Tumor volume
 * 2- Number of sources 
 * 3- Length of each source
 * 4- Source detection separation between every two sources
 * 5- The cross and dot products between each two sources
 * 6- Detected weight at each detector from each source with unit power
 * 7- The recovered mu_effs 
 * 8- The original randomly generated mu_a and mu_s
 *
 * @param sources [in]: a vector of source placements
 */
void RtFeedback::prepare_training_data(const std::vector<Src_Abstract*>& sources)
{
	std::vector<Src_Line> t_sources (sources.size());
    for (unsigned i = 0; i < sources.size(); i++) {
        t_sources[i] = *((Src_Line*) sources[i]);
    } 

    // 1- Open a csv file to append training data to
    ofstream out_file;
    out_file.open("logs/rt_feedback_results/training_data/rt_feedback_training_data_1e7_packets_sa_rl_placement_changing_g.csv", std::ofstream::app); 

    // Number of training example per tumor 
    unsigned num_examples = 100; 
	double percent_non_rotated = 1.0; // 50% of the training data is not rotated (sources are parallel), then randomly n rotate two sources

	std::uniform_int_distribution<int> rand_src_idx(0, static_cast<int>(t_sources.size()) - 1);
 
    // 2- Loop over number of training examples 
    for (unsigned i = 0; i < num_examples; i++) 
    {
		std::cout << "Example: " << i << std::endl;
		// 3- rotate sources randomly
		if (static_cast<double>(i)/static_cast<double>(num_examples) > percent_non_rotated) {
			// Rotate m t_sources randomly
			int num_to_rotate = rand_src_idx(_mt_random_generator) + 1;

			for (int j = 0; j < num_to_rotate; j++) {
				int src_idx_to_rotate = rand_src_idx(_mt_random_generator); 

				t_sources[src_idx_to_rotate].rotate_randomly(); 
			}	
		}

		// 4- Calculate all source-detector separations, cross product between sources, and dot products between sources
		std::vector<double> source_detector_seps;
		std::vector<SP_Point> source_detector_cross_product; 
		std::vector<double> source_detector_dot_products;
		for (unsigned s = 0; s < t_sources.size(); s++) {
			for (unsigned j = s+1; j < t_sources.size(); j++) {
				source_detector_seps.push_back(t_sources[s].get_distance_from(t_sources[j]));
				source_detector_cross_product.push_back(sp_normalize(sp_cross(t_sources[s].dir, t_sources[j].dir)));
				source_detector_dot_products.push_back(sp_dot(t_sources[s].dir, t_sources[j].dir)); 
			}
		}


        stringstream t_data; 
        
        // 5- append volume 
        t_data << _tumor_volume << ","; 

        // 6- append number of t_sources 
        t_data << t_sources.size() << ","; 

		// 7- append source lengths 
        for (auto src : t_sources) {
            t_data << src.length << ",";
        }

        // 8- append source-detector separations 
        for (auto dist : source_detector_seps) {
            t_data << dist << ",";
        }

		// 9- append source-detector cross product
		for (auto cross : source_detector_cross_product) {
			t_data << cross.get_xcoord() << "," << cross.get_ycoord() << "," << cross.get_zcoord() << ","; 
		}

		// 10- append source-detector dot product
		for (auto dot : source_detector_dot_products) {
			t_data << dot << ","; 
		}

        // 11- generate new optical properties for tumor  
        MaterialSet* new_ms = generate_random_op(); 

        // 12- Update the FullMonte kernel materials
        _fullmonte_wrapper->update_kernel_materials(new_ms);
	
        // 13- Emulate dosimetry
        std::vector<std::vector<float>> detected_weights;
        std::vector<float> powers(t_sources.size(), 1.0f); 
        std::vector<Src_Abstract*> srcs (t_sources.size());
        for (unsigned i = 0; i < t_sources.size(); i++) {
            srcs[i] = (Src_Abstract*) (&t_sources[i]);
        }
        emulate_dosimetry(srcs, powers, detected_weights); 

        // 14 - recover mu_effs
        std::vector<double> mu_effs;
		// Multiplier to transform detected weights from 0.5mm radius to 1mm radius
		float slope_to_1mm = 2.314641f; // Was found experimentally based on a linear regression

		for (unsigned i = 0; i < detected_weights.size(); i++) {
			for (unsigned j = 0; j < detected_weights[i].size(); j++) {
				detected_weights[i][j] *= slope_to_1mm; // TODO 
			}
		}

        mu_effs = recover_mu_effs_lut(srcs, detected_weights); 

        // 15- multiply detected_weights by number of t_sources since they are normalized by sum of powers, then append to line
        for (unsigned i = 0; i < detected_weights.size(); i++) {
            for (unsigned j = 0; j < detected_weights.size(); j++) {
                detected_weights[i][j] *= static_cast<float>(t_sources.size()); 

                t_data << detected_weights[i][j] << ",";
            }
        }

        // 16- Append the mu_effs 
        for (auto mu_eff : mu_effs) {
            t_data << mu_eff << ",";
        }

        // 17- Append the training labels: mu_a and mu_s
        auto mat = new_ms->material(_tumor_material_id);
        t_data << mat->absorptionCoeff() << "," << mat->scatteringCoeff(); 

        // 18- Print the new data to the file 
        out_file << t_data.str() << std::endl;
    }

    out_file.close(); 
	std::exit(-1); 
}

/** 
 * This method runs builds a lookup table for the detected weights for each source by reading it from a file. 
 * This lut is used in recovering the effective attenuation coefficient. 
 * @param filename [in] the file that contains the lut to read
 * @param num_sources [in] number of sources. (Used to set lut size)
 */
void RtFeedback::build_lut_from_file(string filename, size_t num_sources)
{
    ifstream in_f;
    in_f.open(filename); 

    if (!in_f) {
        fprintf(stderr, "\033[1;%dmRtFeedback::build_lut_from_file::Couldn't open file %s to build lut. Exiting...\033[0m\n", 31,
                    filename.c_str()); 
        std::exit(-1); 
    }

    _mu_effs_lut.clear(); 
    _mu_effs_lut.resize(num_sources);

    string line; 
    int curr_src = -1;
    while(getline(in_f, line)) {
        if (line.substr(0, string("Map").size()) == "Map") {
            curr_src++;
            continue;
        }

        std::vector<float> curr_detected_weights(num_sources);
        stringstream lss(line); 

        for (unsigned i = 0; i < num_sources; i++) {
            lss >> curr_detected_weights[i]; 
        }

        string temp = "";
        lss >> temp; 
        assert(temp == "|"); 

        double temp_mu_eff = 0.0; 
        lss >> temp_mu_eff; 

        key_detected_weights new_kdw(curr_detected_weights); 

        if (_mu_effs_lut[curr_src].find(new_kdw) == _mu_effs_lut[curr_src].end()) {
            _mu_effs_lut[curr_src].insert(std::make_pair(new_kdw, temp_mu_eff));
        }
    } // end while 
   
	std::cout << std::fixed << std::setprecision(8);
    std::cout << "Printing maps: " << std::endl;
    for (unsigned i = 0; i < _mu_effs_lut.size(); i++) {
        std::cout << "Map " << i << std::endl;
        for (auto it = _mu_effs_lut[i].begin(); it != _mu_effs_lut[i].end(); it++) {
            key_detected_weights kdw = it->first; 
            double mu_eff = it->second; 

            for (auto dw : kdw.detected_weights) 
                std::cout << dw << " " ; 
            std::cout << "      |     " << mu_eff << std::endl;
        }
    }
}

/** 
 * This method computes all the features of the current run, and writes them to a file called test_data.csv
 */
vector<double> RtFeedback::get_features_vector(const std::vector<std::vector<float>> & detected_weights, 
                            const std::vector<double> & recovered_mu_effs, 
                            double absorption_coeff, double scattering_coeff) {
    vector<double> features; 
    // Calculate all source-detector separations, cross product between sources, and dot products between sources
    std::vector<double> source_detector_seps;
    std::vector<SP_Point> source_detector_cross_product; 
    std::vector<double> source_detector_dot_products;
    std::vector<SP_Line> source_placement (_curr_placement.size());
    for (unsigned i = 0; i < _curr_placement.size(); i++) {
        source_placement[i] = *((SP_Line*) _curr_placement[i]);
    }
    for (unsigned s = 0; s < _curr_placement.size(); s++) {
        for (unsigned j = s+1; j < _curr_placement.size(); j++) {
            source_detector_seps.push_back(source_placement[s].get_distance_from(source_placement[j]));
            source_detector_cross_product.push_back(sp_normalize(sp_cross(source_placement[s].dir, source_placement[j].dir)));
            source_detector_dot_products.push_back(sp_dot(source_placement[s].dir, source_placement[j].dir)); 
        }
    }

    stringstream t_data; 
    
    // append volume 
    t_data << _tumor_volume << ","; 
    features.push_back(_tumor_volume); 

    // append number of _curr_placement 
    t_data << _curr_placement.size() << ","; 
    features.push_back(static_cast<double>(_curr_placement.size())); 

    // append source lengths 
    for (auto src : source_placement) {
        t_data << src.length << ",";
        features.push_back(src.length); 
    }

    // append source-detector separations 
    for (auto dist : source_detector_seps) {
        t_data << dist << ",";
        features.push_back(dist);
    }

    // append source-detector cross product
    for (auto cross : source_detector_cross_product) {
        t_data << cross.get_xcoord() << "," << cross.get_ycoord() << "," << cross.get_zcoord() << ","; 
    
        features.push_back(cross.get_xcoord());
        features.push_back(cross.get_ycoord());
        features.push_back(cross.get_zcoord());
    }

    // append source-detector dot product
    for (auto dot : source_detector_dot_products) {
        t_data << dot << ","; 
        features.push_back(dot); 
    }

    // multiply detected_weights by number of _curr_placement since they are normalized by sum of powers, then append to line
    float curr_size = static_cast<float>(_curr_placement.size());
    for (unsigned i = 0; i < detected_weights.size(); i++) {
        for (unsigned j = 0; j < detected_weights.size(); j++) {
            t_data << detected_weights[i][j]*curr_size << ",";
            features.push_back(static_cast<double>(detected_weights[i][j]*curr_size));
        }
    }

    // Append the mu_effs 
    for (auto mu_eff : recovered_mu_effs) {
        t_data << mu_eff << ",";
        features.push_back(mu_eff); 
    }

    // Append the training labels: mu_a and mu_s
    t_data << absorption_coeff << "," << scattering_coeff; 

    // Print the new data to the file 
    ofstream out_file;
    out_file.open("test_data.csv"); 
    out_file << t_data.str() << std::endl;


    return features;
}
