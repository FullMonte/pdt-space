#include "oriented_bounding_box.h"

#include <unordered_map>

/**
 * Functions for the OBB class.
 */
//! Regular constructor with provided region id
OBB::OBB(mesh * data, int region) {
    unordered_map<unsigned, bool>  indices;
    vector<SP_Point*> points = data->get_list_of_points();
    vector<tetrahedron*> tetra = data->get_list_of_tetrahedra();

    // loop through the mesh to find the indices
    for (unsigned i = 0; i < tetra.size(); i++) {
        tetrahedron* t = tetra[i];
        if(t->get_material_id() == region) {
            indices[t->get_p1_id()-1] = 1;
            indices[t->get_p2_id()-1] = 1;
            indices[t->get_p3_id()-1] = 1;
            indices[t->get_p4_id()-1] = 1;
        }
    }

    // insert valid points into data points
    for (auto& it: indices) {
        _data_points.push_back(points[it.first]);
    }

    compute_obb();
}

//! Regular constructor with provided tetra indices
OBB::OBB(mesh * data, vector<unsigned> tetra_indices) {

    unordered_map<unsigned, bool>  indices;
    vector<SP_Point*> points = data->get_list_of_points();
    vector<tetrahedron*> tetra = data->get_list_of_tetrahedra();
    
    // loop through provided tetra indices and add to points
    for (unsigned i = 0; i < tetra_indices.size(); i++) {
        tetrahedron* t = tetra[tetra_indices[i]];
        indices[t->get_p1_id()-1] = 1;
        indices[t->get_p2_id()-1] = 1;
        indices[t->get_p3_id()-1] = 1;
        indices[t->get_p4_id()-1] = 1;
    }

    // insert valid points into data points
    for (auto& it: indices) {
        _data_points.push_back(points[it.first]);
    }

    compute_obb();
}

//! helper function to do the computation
void OBB::compute_obb() {
    // get mean
    SP_Point mean = sp_mean();
    
    // get convariance matrix
    Matrix_3x3 cov_matrix = covariance_matrix(mean);

    // diagonalize matrix
    Quaternion q = cov_matrix.Diagonalize();

    // obtain matrix containing Eigen basis
    Matrix_3x3 res_matrix = q.get_matrix();

    // set Eigen vectors
    _final_eigen_vectors[0] = SP_Point(res_matrix.m00, res_matrix.m10, res_matrix.m20);
    _final_eigen_vectors[1] = SP_Point(res_matrix.m01, res_matrix.m11, res_matrix.m21);
    _final_eigen_vectors[2] = SP_Point(res_matrix.m02, res_matrix.m12, res_matrix.m22);
    
    // handle special cases
    int num_zeros = 0;
    int zero_ind = 0;
    for (unsigned i = 0; i < 3; i++) {
        if (sp_norm(_final_eigen_vectors[i]) <= DOUBLE_ACCURACY) {
            num_zeros++;
            zero_ind = i;
        } else _final_eigen_vectors[i] = sp_normalize(_final_eigen_vectors[i]);
    }

    if (num_zeros == 1) {
        // if one vector has zero length, compute it by cross product
        _final_eigen_vectors[zero_ind] = sp_normalize(sp_cross(_final_eigen_vectors[(zero_ind+1)%3], _final_eigen_vectors[(zero_ind+2)%3]));
    } else if (num_zeros > 1) {
        // if more than one has zero length, approximate using identity basis
        _final_eigen_vectors[0] = SP_Point(1.0f, 0.0f, 0.0f);
        _final_eigen_vectors[1] = SP_Point(0.0f, 1.0f, 0.0f);
        _final_eigen_vectors[2] = SP_Point(0.0f, 0.0f, 1.0f);
    }

    // orthonormalize vectors
    int orthonormalize_iterations = 24;
    bool done = false;
    for (int i = 0; i < orthonormalize_iterations && !done; i++) {
        done = true;
        if (sp_dot(_final_eigen_vectors[0], _final_eigen_vectors[1]) > DOUBLE_ACCURACY) {
            _final_eigen_vectors[1] = sp_normalize(sp_cross(_final_eigen_vectors[0], _final_eigen_vectors[2]));
            done = false;
        }
        if (sp_dot(_final_eigen_vectors[0], _final_eigen_vectors[2]) > DOUBLE_ACCURACY) {
            _final_eigen_vectors[2] = sp_normalize(sp_cross(_final_eigen_vectors[0], _final_eigen_vectors[1]));
            done = false;
        }
        if (sp_dot(_final_eigen_vectors[1], _final_eigen_vectors[2]) > DOUBLE_ACCURACY) {
            _final_eigen_vectors[2] = sp_normalize(sp_cross(_final_eigen_vectors[0], _final_eigen_vectors[1]));
            done = false;
        }
    }

    if (!done) cout << "Warning: final Eigen basis not orthonormal." << endl;

    // compute bounds
    SP_Point min_projected, max_projected;
    for (unsigned i = 0; i < _data_points.size(); i++) {
        // project onto basis
        SP_Point projected;
        SP_Point data_point = *(_data_points[i]);
        projected.set_xcoord((float)sp_dot(data_point, _final_eigen_vectors[0]));
        projected.set_ycoord((float)sp_dot(data_point, _final_eigen_vectors[1]));
        projected.set_zcoord((float)sp_dot(data_point, _final_eigen_vectors[2]));

        if (i == 0) {
            // set min and max to the same point
            min_projected = projected;
            max_projected = projected;
        } else {
            // update projected min and max points
            min_projected.set_xcoord(min(min_projected.get_xcoord(), projected.get_xcoord()));
            max_projected.set_xcoord(max(max_projected.get_xcoord(), projected.get_xcoord()));

            min_projected.set_ycoord(min(min_projected.get_ycoord(), projected.get_ycoord()));
            max_projected.set_ycoord(max(max_projected.get_ycoord(), projected.get_ycoord()));

            min_projected.set_zcoord(min(min_projected.get_zcoord(), projected.get_zcoord()));
            max_projected.set_zcoord(max(max_projected.get_zcoord(), projected.get_zcoord()));
        }
    }

    // convert projection into coordinates
    SP_Point min_x = min_projected.get_xcoord()*_final_eigen_vectors[0];
    SP_Point min_y = min_projected.get_ycoord()*_final_eigen_vectors[1];
    SP_Point min_z = min_projected.get_zcoord()*_final_eigen_vectors[2];

    SP_Point max_x = max_projected.get_xcoord()*_final_eigen_vectors[0];
    SP_Point max_y = max_projected.get_ycoord()*_final_eigen_vectors[1];
    SP_Point max_z = max_projected.get_zcoord()*_final_eigen_vectors[2];

    _final_bounding_box[0] = min_x + min_y + min_z;
    _final_bounding_box[1] = max_x + min_y + min_z;
    _final_bounding_box[2] = min_x + max_y + min_z;
    _final_bounding_box[3] = max_x + max_y + min_z;
    _final_bounding_box[4] = min_x + min_y + max_z;
    _final_bounding_box[5] = max_x + min_y + max_z;
    _final_bounding_box[6] = min_x + max_y + max_z;
    _final_bounding_box[7] = max_x + max_y + max_z;
}

//! helper function to compute the mean from point data
SP_Point OBB::sp_mean() const {
    SP_Point result (0.0f, 0.0f, 0.0f);

    // add all points
    for (unsigned i = 0; i < _data_points.size(); i++) {
        result = result + *(_data_points[i]);
    }

    // divide by size
    if (_data_points.size() > 0) {
        result = result / (float)_data_points.size();
    }

    return result;
}

//! helper function to compute the covariance matrix
Matrix_3x3 OBB::covariance_matrix(SP_Point mean) const {
    Matrix_3x3 m {0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f};

    // covariance matrix is defined as:
    //   --                             --
    //   | var(x)    cov(x,y)  cov(x,z)  |
    //   | cov(y,x)  var(y)    cov(y,z)  |
    //   | cov(z,x)  cov(z,y)  var(z)    |
    //   --                             --
    // where:
    //   var(X) = 1/N * SUM((x_i - mean(x))^2)
    //   cov(X,Y) = 1/N * SUM((x_i - mean(X))(y_i - mean(Y)))

    for (unsigned i = 0; i < _data_points.size(); i++) {
        float x = _data_points[i]->get_xcoord();
        float y = _data_points[i]->get_ycoord();
        float z = _data_points[i]->get_zcoord();

        m.m00 += (x - mean.get_xcoord()) * (x - mean.get_xcoord());
        m.m01 += (x - mean.get_xcoord()) * (y - mean.get_ycoord());
        m.m02 += (x - mean.get_xcoord()) * (z - mean.get_zcoord());

        m.m10 += (y - mean.get_ycoord()) * (x - mean.get_xcoord());
        m.m11 += (y - mean.get_ycoord()) * (y - mean.get_ycoord());
        m.m12 += (y - mean.get_ycoord()) * (z - mean.get_zcoord());

        m.m20 += (z - mean.get_zcoord()) * (x - mean.get_xcoord());
        m.m21 += (z - mean.get_zcoord()) * (y - mean.get_ycoord());
        m.m22 += (z - mean.get_zcoord()) * (z - mean.get_zcoord());
    }

    m.m00 /= (float)_data_points.size();
    m.m01 /= (float)_data_points.size();
    m.m02 /= (float)_data_points.size();
    m.m10 /= (float)_data_points.size();
    m.m11 /= (float)_data_points.size();
    m.m12 /= (float)_data_points.size();
    m.m20 /= (float)_data_points.size();
    m.m21 /= (float)_data_points.size();
    m.m22 /= (float)_data_points.size();

    return m;
}

//! Overloads ostream to print the output
std::ostream& operator<<(std::ostream& os, const OBB& obb) {
    os << "\nFinal Eigen basis is: \n";
    os << obb._final_eigen_vectors[0] << "\n";
    os << obb._final_eigen_vectors[1] << "\n";
    os << obb._final_eigen_vectors[2] << "\n";
    os << "\nFinal Bounding Box is: \n";
    os << obb._final_bounding_box[0] << "\n";
    os << obb._final_bounding_box[1] << "\n";
    os << obb._final_bounding_box[2] << "\n";
    os << obb._final_bounding_box[3] << "\n";
    os << obb._final_bounding_box[4] << "\n";
    os << obb._final_bounding_box[5] << "\n";
    os << obb._final_bounding_box[6] << "\n";
    os << obb._final_bounding_box[7] << "\n";

    return os;
}

//! Returns rotated bounding box in a given direction in result
void OBB::get_bounding_box(int z_dir, SP_Point (&result)[8]) {

    if (z_dir == 0) {
        // z points in the original x direction
        result[0] = _final_bounding_box[0];
        result[1] = _final_bounding_box[2];
        result[2] = _final_bounding_box[4];
        result[3] = _final_bounding_box[6];
        result[4] = _final_bounding_box[1];
        result[5] = _final_bounding_box[3];
        result[6] = _final_bounding_box[5];
        result[7] = _final_bounding_box[7];
    } else if (z_dir == 1) {
        // z points in the original y direction
        result[0] = _final_bounding_box[0];
        result[1] = _final_bounding_box[4];
        result[2] = _final_bounding_box[1];
        result[3] = _final_bounding_box[5];
        result[4] = _final_bounding_box[2];
        result[5] = _final_bounding_box[6];
        result[6] = _final_bounding_box[3];
        result[7] = _final_bounding_box[7];
    } else {
        // return unchanged bounding box
        result[0] = _final_bounding_box[0];
        result[1] = _final_bounding_box[1];
        result[2] = _final_bounding_box[2];
        result[3] = _final_bounding_box[3];
        result[4] = _final_bounding_box[4];
        result[5] = _final_bounding_box[5];
        result[6] = _final_bounding_box[6];
        result[7] = _final_bounding_box[7];
    }
}

/**
 * Functions for the Matrix_3x3 struct.
 */

//! Overloads operator +
Matrix_3x3 Matrix_3x3::operator+(const Matrix_3x3& m) const {
    Matrix_3x3 result;

    result.m00 = m00 + m.m00;
    result.m01 = m10 + m.m01;
    result.m02 = m20 + m.m02;
    result.m10 = m01 + m.m10;
    result.m11 = m11 + m.m11;
    result.m12 = m21 + m.m12;
    result.m20 = m02 + m.m20;
    result.m21 = m12 + m.m21;
    result.m22 = m22 + m.m22;

    return result;
}

//! Overloads operator *
Matrix_3x3 Matrix_3x3::operator*(const Matrix_3x3& m) const {
    Matrix_3x3 result;

    result.m00 = m00*m.m00 + m01*m.m10 + m02*m.m20;
    result.m01 = m00*m.m01 + m01*m.m11 + m02*m.m21;
    result.m02 = m00*m.m02 + m01*m.m12 + m02*m.m22;

    result.m10 = m10*m.m00 + m11*m.m10 + m12*m.m20;
    result.m11 = m10*m.m01 + m11*m.m11 + m12*m.m21;
    result.m12 = m10*m.m02 + m11*m.m12 + m12*m.m22;

    result.m20 = m20*m.m00 + m21*m.m10 + m22*m.m20;
    result.m21 = m20*m.m01 + m21*m.m11 + m22*m.m21;
    result.m22 = m20*m.m02 + m21*m.m12 + m22*m.m22;

    return result;
}

//! helper function to transpose a 3x3 matrix
Matrix_3x3 Matrix_3x3::transpose_matrix() const {
    Matrix_3x3 result;

    result.m00 = m00;
    result.m01 = m10;
    result.m02 = m20;
    result.m10 = m01;
    result.m11 = m11;
    result.m12 = m21;
    result.m20 = m02;
    result.m21 = m12;
    result.m22 = m22;

    return result;
}

/** 
 * Helper function to do diagonalization.
 * 
 * Uses Jacobi Transformations of a Symmetric Matrix.
 * 
 * this will store the Eigen values on the diagonal.
 */
Quaternion Matrix_3x3::Diagonalize() {
    int num_iterations = 24;

    Quaternion q(0.0f, 0.0f, 0.0f, 1.0f);

    // this matrix is D. Start iterating.
    for (int i = 0; i < num_iterations; i++) {
        // get the matrix from q
        Matrix_3x3 Q = q.get_matrix();

        // A = transpose(Q) * D * Q
        Matrix_3x3 A = Q * (*(this)) * Q.transpose_matrix();

        // get non-diagonal elements
        float theta;
        int k;
        if (abs(A.m12) >= abs(A.m02) && abs(A.m12) >= abs(A.m01)) {
            // exit if already diagonal
            if (abs(A.m12) <= DOUBLE_ACCURACY) return q;

            theta = (A.m11 - A.m22) / (2.0f * A.m12);
            k = 0;
        } else if (abs(A.m02) >= abs(A.m01)) {
            // exit if already diagonal
            if (abs(A.m02) <= DOUBLE_ACCURACY) return q;

            theta = (A.m22 - A.m00) / (2.0f * A.m02);
            k = 1;
        } else {
            // exit if already diagonal
            if (abs(A.m01) <= DOUBLE_ACCURACY) return q;

            theta = (A.m00 - A.m11) / (2.0f * A.m01);
            k = 2;
        }

        // make theta positive
        float sgn = (theta < 0.0f) ? -1.0f : 1.0f;
        theta *= sgn;

        // t = sign(theta)/(|theta| + sqrt(theta^2 + 1))
        float t = sgn / (theta + ((theta < DOUBLE_ACCURACY) ? sqrtf(theta * theta + 1.0f) : theta));
        // c = 1 / sqrt(t^2 + 1)
        float c = 1.0f / sqrtf(t * t + 1.0f);
        // s = tc

        // terminate if no room for improvement
        if (c == 1.0f) return q;

        // Jacobi rotation
        Quaternion jr;

        // Half angle identity
        float jr_k = -1 * sgn * sqrtf(1.0f - c) / 2.0f;
        float jr_w = sqrtf(1.0f - jr_k * jr_k);
        jr.set_value(k, jr_k);
        jr.set_value(3, jr_w);

        // terminate if reached floating point precision
        if (abs(jr_w - 1.0f) <= DOUBLE_ACCURACY) return q;
        
        // update q
        q = q * jr;
        q.normalize();
    }

    return q;
}

/**
 * Functions for the Quaternion class.
 */
//! Helper function to get the rotation matrix represented
Matrix_3x3 Quaternion::get_matrix() const {
    // Matrix is defined as:
    //   --                                            --
    //   | 1-2*y^2-2*z^2  2*x*y-2*z*w    2*x*z+2*y*w    |
    //   | 2*x*y+2*z*w    1-2*x^2-2*z^2  2*y*z-2*x*w    |
    //   | 2*x*z-2*y*w    2*y*z+2*x*w    1-2*x^2-2*y^2  |
    //   --                                            --
    Matrix_3x3 m;
    m.m00 = 1 - 2*_y*_y - 2*_z*_z;
    m.m01 = 2*_x*_y - 2*_z*_w;
    m.m02 = 2*_x*_z + 2*_y*_w;
    m.m10 = 2*_x*_y + 2*_z*_w;
    m.m11 = 1 - 2*_x*_x - 2*_z*_z;
    m.m12 = 2*_y*_z - 2*_x*_w;
    m.m20 = 2*_x*_z - 2*_y*_w;
    m.m21 = 2*_y*_z + 2*_x*_w;
    m.m22 = 1 - 2*_x*_x - 2*_y*_y;

    return m;
}

//! Setter for one field by index
void Quaternion::set_value(int i, float v) {
    switch(i) {
    case 0:
        _x = v;
        break;
    case 1:
        _y = v;
        break;
    case 2:
        _z = v;
        break;
    case 3:
        _w = v;
        break;
    // ignore if default
    }
}

//! Overloads operator *
Quaternion Quaternion::operator*(const Quaternion& q) const {
    Quaternion c;

    c._w = _w*q._w - _x*q._x - _y*q._y - _z*q._z;
    c._x = _w*q._x + _x*q._w + _y*q._z - _z*q._y;
    c._y = _w*q._y - _x*q._z + _y*q._w + _z*q._x;
    c._z = _w*q._z + _x*q._y - _y*q._x + _z*q._w;

    return c;
}

//! Helper function for normalization
void Quaternion::normalize() {
    float l2_norm = sqrtf(_x*_x + _y*_y + _z*_z + _w*_w);

    if (l2_norm < DOUBLE_ACCURACY) return;

    _x /= l2_norm;
    _y /= l2_norm;
    _z /= l2_norm;
    _w /= l2_norm;
}