/** 
 * \file tetrahedron.cxx \brief tetrahedron Implementation
 *
 * This source files implements the different methods for the tetrahedron class
 */

#include "read_mesh.h"

// tetrahedron Class

//! Default Constructor 
tetrahedron::tetrahedron()
{
    _parent_mesh = NULL;

    _p1_id = -1;
    _p2_id = -1;
    _p3_id = -1;
    _p4_id = -1;

    _material_id = -1;
    
    _id = -1;
}

//! Regular Constructor
tetrahedron::tetrahedron(int id, int p1_id, int p2_id, int p3_id, int p4_id,
                            int material_id, mesh* parent_mesh)
{
    _parent_mesh = parent_mesh;


    _p1_id = p1_id;
    _p2_id = p2_id;
    _p3_id = p3_id;
    _p4_id = p4_id;

    _material_id = material_id;

    _id = id;
}

//! Destructor
tetrahedron::~tetrahedron()
{
}

// Getters

//! This method returns the parent mesh
mesh* tetrahedron::get_parent_mesh()
{
    return _parent_mesh;
}

//! This method returns the id of the first boundary point
int tetrahedron::get_p1_id()
{
    return _p1_id;
}

//! This method returns the id of the second boundary point
int tetrahedron::get_p2_id()
{
    return _p2_id;
}

//! This method returns the id of the third boundary point
int tetrahedron::get_p3_id()
{
    return _p3_id;
}

//! This method returns the id of the forth boundary point
int tetrahedron::get_p4_id()
{
    return _p4_id;
}

//! This method returns the id of the tetrahedron's material
int tetrahedron::get_material_id()
{
    return _material_id;
}

//! This method returns the id of the tetrahedron
int tetrahedron::get_id()
{
    return _id;
}

//!
//! This method sets the id of the first boundary point
//! @param id [in]: id to be added
//!
void tetrahedron::set_p1_id(int id)
{
    _p1_id = id;
}

//!
//! This method sets the id of the second boundary point
//! @param id [in]: id to be added
//!
void tetrahedron::set_p2_id(int id)
{
    _p2_id = id;
}

//!
//! This method sets the id of the third boundary point
//! @param id [in]: id to be added
//!
void tetrahedron::set_p3_id(int id)
{
    _p3_id = id;
}

//!
//! This method sets the id of the forth boundary point
//! @param id [in]: id to be added
//!
void tetrahedron::set_p4_id(int id)
{
    _p4_id = id;
}

//!
//! This method sets the id of the material of the tetrahedron
//! @param id [in]: id to be added
//!
void tetrahedron::set_material_id(int id)
{
    _material_id = id;
}

//!
//! This method sets the id of the tetrahedron
//! @param id [in]: id to be added
//!
void tetrahedron::set_id(int id)
{
    _id = id;
}

// Helper functions

//!
//! This method returns the centroid of a tetrahedron
//!
SP_Point tetrahedron::compute_centroid_of()
{
    /**
     * The method finds all the 4 points of the tetrahedron 
     * and then computes the centroid by summing the x-coordinates together and dividing
     * by 4. We do the same thing for y and z coordinates.
     */

    vector<float> xcoords(4, 0.0);
    vector<float> ycoords(4, 0.0);
    vector<float> zcoords(4, 0.0);

    xcoords[0] = _parent_mesh->get_list_of_points()[_p1_id - 1]->get_xcoord();
    xcoords[1] = _parent_mesh->get_list_of_points()[_p2_id - 1]->get_xcoord();
    xcoords[2] = _parent_mesh->get_list_of_points()[_p3_id - 1]->get_xcoord();
    xcoords[3] = _parent_mesh->get_list_of_points()[_p4_id - 1]->get_xcoord();

    ycoords[0] = _parent_mesh->get_list_of_points()[_p1_id - 1]->get_ycoord();
    ycoords[1] = _parent_mesh->get_list_of_points()[_p2_id - 1]->get_ycoord();
    ycoords[2] = _parent_mesh->get_list_of_points()[_p3_id - 1]->get_ycoord();
    ycoords[3] = _parent_mesh->get_list_of_points()[_p4_id - 1]->get_ycoord();

    zcoords[0] = _parent_mesh->get_list_of_points()[_p1_id - 1]->get_zcoord();
    zcoords[1] = _parent_mesh->get_list_of_points()[_p2_id - 1]->get_zcoord();
    zcoords[2] = _parent_mesh->get_list_of_points()[_p3_id - 1]->get_zcoord();
    zcoords[3] = _parent_mesh->get_list_of_points()[_p4_id - 1]->get_zcoord();

    float centroid_x = 0.0; 
    float centroid_y = 0.0;
    float centroid_z = 0.0;
    for (int i = 0; i < 4; i++)
    {
        centroid_x += xcoords[i];
        centroid_y += ycoords[i];
        centroid_z += zcoords[i];
    }

    centroid_x = centroid_x/4;
    centroid_y = centroid_y/4;
    centroid_z = centroid_z/4;

    return SP_Point(centroid_x, centroid_y, centroid_z);

}