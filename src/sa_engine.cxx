/**
 * @author: Abed Yassine
 * @date: September 30th, 2019
 *
 * @file sa_engine.cxx
 * @brief: contains implementations for the class sa_engine methods
 */

#include "sa_engine.h"
#include "profiler.h"
#include "file_parser.h"

#include <cmath>
#include <fstream>
#include <iostream>
#include <sstream>
#include <ctime>

// #define SINGLE_FULLMONTE

//! Regular constructor
sa_engine::sa_engine(Parser* fparser,
			WrapperFullMonteSW * fm_engine,
			mesh* data,
            const std::vector<double>& min_dose,
            const std::vector<double>& max_dose,
            const std::vector<unsigned>& tumor_indices,
			double tumor_volume,
			bool override_file_path,
			bool early_termination,
			pdt_plan* parent_plan ) :
	_fparser(fparser),
    _min_dose(min_dose), _max_dose(max_dose),
    _tumor_indices(tumor_indices)
{
	_parent_plan = parent_plan;
    _init_temp = 1e4;
    _current_temp = 0;
	_tumor_volume = tumor_volume;
    _max_tumor_radius = std::cbrt(tumor_volume)*1.2;
    _old_max_move = 1.25*_max_tumor_radius; // max tumor radius we have is around 5 cm
    _curr_max_move = _max_tumor_radius;
	_file_path_override = override_file_path;

	// get parameters from fparser
	read_parameters();

	_early_termination = early_termination;
	if (early_termination) {
		_max_num_moves = 3;
		_max_unaccepted_since_last_acceptance = 0;
	}

	_generate_learning_only = false;
	_chose_force_based = false;

    _fullmonte_engine = fm_engine;
	_data = data;

    _num_tetra = static_cast<unsigned>(_min_dose.size());

	_num_rejected_inj_point = 0;
    _num_rejected_tumor = 0;
    _num_rejected_overlaps = 0;
	_num_rejected_removals = 0;
    _num_moves = 0;
    _num_legal_moves = 0;
    _num_accepted_moves = 0;
    _num_accepted_climbs = 0;
	_num_accepted_force_based = 0;
	_num_reset_to_best_moves = 0;

	_num_unaccepted_since_last_acceptance = 0;

	// Initialize moves probabilities
	_move_type_probability.insert(std::make_pair(MoveType::RANDOM, 0.7)); // at the beginning 70% of the moves are random
	_move_type_probability.insert(std::make_pair(MoveType::FORCE_MULTISOURCE, 0.12));
	_move_type_probability.insert(std::make_pair(MoveType::FORCE_CLOSEST, 0.12));
	_move_type_probability.insert(std::make_pair(MoveType::DISPLACE, 0.06));
	_move_type_probability.insert(std::make_pair(MoveType::RESET_BEST, 0.0));
	_move_type_probability.insert(std::make_pair(MoveType::LENGTH, 0.0));
	_move_type_probability.insert(std::make_pair(MoveType::REMOVE_SOURCE, 0.0));
	_move_type_probability.insert(std::make_pair(MoveType::RELOCATE, 0.0));

	// Initialize moves effectiveness
	_move_type_effectiveness.insert(std::make_pair(MoveType::RANDOM, 0.0));
	_move_type_effectiveness.insert(std::make_pair(MoveType::DISPLACE, 0.0));
	_move_type_effectiveness.insert(std::make_pair(MoveType::FORCE_CLOSEST, 0.0));
	_move_type_effectiveness.insert(std::make_pair(MoveType::FORCE_MULTISOURCE, 0.0));
	_move_type_effectiveness.insert(std::make_pair(MoveType::RESET_BEST, 0.0));
	_move_type_effectiveness.insert(std::make_pair(MoveType::LENGTH, 0.0));
	_move_type_effectiveness.insert(std::make_pair(MoveType::REMOVE_SOURCE, 0.0));
	_move_type_effectiveness.insert(std::make_pair(MoveType::RELOCATE, 0.0));

	_terminate_based_on_effectiveness = false;

	_curr_move_type = MoveType::NONE;

#ifdef USE_INTERPOLATION
    _delta_x = 0.086; // TODO: calculate from optical properties.
    _grid_min.resize(3); // TODO: compute from tumor location
    _grid_min[0] = 70.0;
    _grid_min[1] = 97.0;
    _grid_min[2] = 110.5;

    // Defining the source cache for interpolation
    vector<double> u(3, 0.0), v(3, 0.0), w(3, 0.0);
    u[0] = _delta_x;
    v[1] = _delta_x;
    w[2] = _delta_x;
    _grid_sources = new SP_IE::source_cache(3, _num_tetra, _grid_min, u, v, w, _fullmonte_engine);

    init_grid_sources();
#endif

	// compute oriented bounding box
	_obb_engine = OBB(_data, _tumor_indices);
	cout << _obb_engine << endl;

	// create a pdt plan object to allocate power
	string params_file = _parent_plan->get_params_file();
	_power_allocator = new pdt_plan(params_file);
	_power_allocator->set_num_packets(_num_packets);
	if (_early_termination) _power_allocator->vary_tumor_weight(false);
	_guardbanded_thresholds = _power_allocator->get_thresholds_with_guardband();
	_power_allocator->compute_original_tetra_data(_guardbanded_thresholds);
	_curr_tumor_weight = _power_allocator->get_tumor_weight();
	_orig_tumor_weight = _curr_tumor_weight;
	_power_allocator->set_target_tumor_v100(98.0);
	_power_allocator->set_tumor_v100_bounds(97.0, 99.0);
	_power_allocator->set_max_num_lp_iterations(11);

	_ignore_move = false;

	_print_sources = (_sa_placement_file != "");
	_print_top_placements = (_final_sa_placement_file != "");

	// Parser fparser (_init_placement_file);

	// Build vector of important tissues indices
	unordered_set <unsigned> critical_regions = _fparser->get_critical_regions();
	unordered_set <unsigned> tumor_regions = _parent_plan->get_tumor_region_number();
	for (auto const &elem: tumor_regions) {
		if (critical_regions.find(elem) == critical_regions.end()) {
			cout << "tumor index " << elem << endl;
			fprintf(stderr, "\033[1;%dmAt least 1 tumor index not in critical tissues.\nExiting...\033[0m\n", 31);
			std::exit(-1);
		}
	}
	for (unsigned i = 0; i < _num_tetra; i++)
	{
		// check if specified as critical region. Otherwise ignore.
		if (critical_regions.find(_data->get_list_of_tetrahedra()[i]->get_material_id()) != critical_regions.end()) // ignore if not
		{
			_oar_and_tumor_indices.push_back(i);
			_oar_and_tumor_volumes.push_back(_data->compute_tet_volume(_data->get_list_of_tetrahedra()[i]));
		}
	}
	// compute overall volume of interest
	_sum_oar_tumor_volumes = 0.0;
	for (auto v : _oar_and_tumor_volumes) {
		_sum_oar_tumor_volumes += v;
	}

	_fparser->get_sources(_current_state, _init_source_lengths);
    printf ("Successfully read %lu sources.\n", _current_state.size());

	if (_current_state.size() > 0) sort(_current_state.begin(), _current_state.end(), greater<SP_Line>());
	sort(_init_source_lengths.begin(), _init_source_lengths.end(), greater<double>());

	cout << "Maximum source lengths in descending order: " << endl;
	for (unsigned i = 0; i < _init_source_lengths.size(); i++) {
		cout << "Line " << i << ": " << _init_source_lengths[i] << endl;
	}

	_fparser->get_injection_point_options(_constrain_injection_point/*, _placement_mode*/, _max_num_sources);
	if (_constrain_injection_point)
		_injection_points = _fparser->get_injection_points();
	
	if (_max_num_sources < _init_source_lengths.size()) {
		fprintf(stderr, "\033[1;%dmsa_engine::Warning: max number of probes less than number of probe lengths specified, ignoring shortest ones.\033[0;m\n", 31);
	} else if (_max_num_sources > _init_source_lengths.size()) {
		_init_source_lengths.resize(_max_num_sources, _INFINITY_);
	}
}

#ifdef USE_INTERPOLATION
//! Helper method: build the grid cache of point sources
void sa_engine::init_grid_sources()
{
    // TODO make these dependent on the tumor size
    profiler grid_init("Initializing sources grid");

    unsigned num_src_x = 10;
    unsigned num_src_y = 10;
    unsigned num_src_z = 10;
    for (unsigned i = 0; i < num_src_x; i++) {
        for (unsigned j = 0; j < num_src_y; j++) {
            for (unsigned k = 0; k < num_src_z; k++) {
                std::cerr << "Running source: (" << i << ", " << j << ", " << k << ")" << std::endl;
                _grid_sources->get_source(10*i, 10*j, 10*k); // coarse grid of 0.86 spacing
            }
        }
    }

    // TODO: parameters of sources may be negative
}
#endif

//! Helper method: call is_valid_injection for all sources in current state
bool sa_engine::is_valid_injection() {
	for (unsigned i = 0; i < _current_state.size(); i++) {
		if (!is_valid_injection(_current_state[i])) return false;
	}

	return true;
}

//! Helper method to place a source, returns false if no solution
bool sa_engine::place_source_from_line(SP_Line &cur_line) {
#ifdef SA_DBG
	cout << endl << "sa_engine::inside place_source_from_line\n";
#endif

	float step_size = 3.0f; // fixing search step size to be 3mm.
	float max_dist_to_mesh = 20.0f; // assuming every point on injection circle is not apart from the mesh by this distance
	// double dist_to_boundary = 8.0f; // fixing distance of endpoints to tumor boundary.

	SP_Point ref_z = sp_normalize(cur_line.end1 - cur_line.end0);

	// search along ref_z to find the closest point in mesh
	int num_steps = 0;
	bool in_mesh = true;
	while (!is_in_mesh(cur_line.end0) && in_mesh) {
		num_steps++;
		cur_line.end0 = cur_line.end0 + step_size * ref_z;
		in_mesh = ((float) num_steps*step_size <= max_dist_to_mesh);
	}

	if (!in_mesh) return false;

	// search to find endpoints
	cur_line.end1 = cur_line.end0;
	bool done = false;
	while (is_in_mesh(cur_line.end1) && !done) {
		if (!is_pt_in_tumor(cur_line.end1) && is_pt_in_tumor(cur_line.end0)) {
			// binary search to find final end1
			SP_Point max_pt = cur_line.end1;
			SP_Point min_pt = cur_line.end1 - (double) step_size * ref_z;

			while (max_pt.get_distance_from(min_pt) > 1e-4) {
				SP_Point mid_pt = max_pt + min_pt;
				mid_pt = 0.5 * mid_pt;
				if (is_pt_in_tumor(mid_pt)) min_pt = mid_pt;
				else max_pt = mid_pt;
			}

			cur_line.end1 = min_pt - _dist_to_boundary * ref_z;
			done = true;
		} else if (is_pt_in_tumor(cur_line.end1) && !is_pt_in_tumor(cur_line.end0)) {
			// binary search to find final end0
			SP_Point max_pt = cur_line.end1;
			SP_Point min_pt = cur_line.end1 - (double) step_size * ref_z;

			while (max_pt.get_distance_from(min_pt) > 1e-4) {
				SP_Point mid_pt = max_pt + min_pt;
				mid_pt = 0.5 * mid_pt;
				if (is_pt_in_tumor(mid_pt)) max_pt = mid_pt;
				else min_pt = mid_pt;
			}

			cur_line.end0 = max_pt + _dist_to_boundary * ref_z;
		}

		// update end1
		if (!done) cur_line.end1 = cur_line.end1 + ref_z;
	}

	// check orientation
	cur_line.update_length_and_dir();
	SP_Point orientation = cur_line.end1 - cur_line.end0;

	return (done && sp_dot(orientation, ref_z) > 0);
}

//! Helper method to constrain source lengths
void sa_engine::constrain_source_length(SP_Line & source, double length) {
	if (source.length <= length) return;

	// shorten current source to match max allowed length
	SP_Point ref_z = sp_normalize(source.end1 - source.end0);
	double diff = 0.5 * (source.length - length);
	source.end0 = source.end0 + diff * ref_z;
	source.end1 = source.end1 - diff * ref_z;
	source.update_length_and_dir();
}

//! Helper method: generates a random initial solution aligning to the longest tumor axis
void sa_engine::generate_random_initial_solution() {
#ifdef SA_DBG
	std::cout << std::endl << "sa_engine::inside generate_random_initial_solution" << std::endl;
#endif

	double probe_spacing = 12.0; // fixing to be 1.2cm (12mm)
	double min_src_length = 0.0f; // setting minimum effective length to be 0mm

#ifdef ADJUST_SPACING
    int adj_iterations = 0;
    int ADJ_MAX_ITERATIONS = 20;
#endif

	_current_state.clear();

	// sort maximum constraint lengths
	sort(_init_source_lengths.begin(), _init_source_lengths.end(), greater<double>());
	//if (_init_source_lengths[_init_source_lengths.size()-1] != _INFINITY_) min_src_length = _init_source_lengths[0];

	// Precomputed center of tumor volume
	// choose center over centroid because want to cover entire tumor volume.
	SP_Point tumor_center = _power_allocator->get_tumor_center();

	// Get coordinate system with oriented bounding box of tumor
	SP_Point ref_x, ref_y, ref_z;
	SP_Point bounding_box[8];
	double box_dist_x = _obb_engine._final_bounding_box[0].get_distance_from(_obb_engine._final_bounding_box[1]);
	double box_dist_y = _obb_engine._final_bounding_box[0].get_distance_from(_obb_engine._final_bounding_box[2]);
	double box_dist_z = _obb_engine._final_bounding_box[0].get_distance_from(_obb_engine._final_bounding_box[4]);
	if (box_dist_x > box_dist_y && box_dist_x > box_dist_z) {
		// align sources in the x direction
		ref_x = _obb_engine._final_eigen_vectors[1];
		ref_y = _obb_engine._final_eigen_vectors[2];
		ref_z = _obb_engine._final_eigen_vectors[0];
		_obb_engine.get_bounding_box(0, bounding_box);
	} else if (box_dist_y > box_dist_z) {
		// align sources in the y direction
		ref_x = _obb_engine._final_eigen_vectors[2];
		ref_y = _obb_engine._final_eigen_vectors[0];
		ref_z = _obb_engine._final_eigen_vectors[1];
		_obb_engine.get_bounding_box(1, bounding_box);
	} else {
		// keep current direction
		ref_x = _obb_engine._final_eigen_vectors[0];
		ref_y = _obb_engine._final_eigen_vectors[1];
		ref_z = _obb_engine._final_eigen_vectors[2];
		_obb_engine.get_bounding_box(2, bounding_box);
	}

	// Draw a line at center of bounding box
	SP_Point min_pt = bounding_box[0] + bounding_box[1] + bounding_box[2] + bounding_box[3];
	min_pt = min_pt / 4.0;
	SP_Point max_pt = bounding_box[4] + bounding_box[5] + bounding_box[6] + bounding_box[7];
	max_pt = max_pt / 4.0;

	SP_Line ref_line = SP_Line(min_pt, max_pt);
	_current_inj_center = min_pt;
	_current_inj_normal = ref_z;

#ifdef ADJUST_SPACING
	// binary search to adjust spacing
	double bs_min = _min_src_spacing*2 - probe_spacing, bs_max = _max_src_spacing;
	unsigned max_num_sources = _max_num_sources;
	unsigned cur_max_num_src = 0;
	while (_current_state.size() == 0 || _current_state.size() != _max_num_sources) {
		if (_current_state.size() == _max_num_sources + 1) {
			// remove source with the shortest length to terminate
			sort (_current_state.begin(), _current_state.end(), greater<SP_Line>());
			_current_state.erase(_current_state.end() - 1);
            continue;
		} else if (_current_state.size() > _max_num_sources) {
			if (bs_max - DOUBLE_ACCURACY <= probe_spacing) {
				fprintf(stderr, "\033[1;%dmsa_engine::generate_random_initial_solution::not enough probes, exiting...\033[0;m\n", 31);
        		std::exit(-1);
			} else {
				bs_min = probe_spacing;
				double bs_mid = 0.5f * (bs_min + bs_max);
				probe_spacing = bs_mid;
			}
		} else if (_current_state.size() > 0) {
			if (_min_src_spacing + DOUBLE_ACCURACY >= probe_spacing) {
				cur_max_num_src = max(cur_max_num_src, (unsigned) _current_state.size());
				_max_num_sources = max(_max_num_sources - 1, cur_max_num_src);
				ref_x = sp_normalize(ref_line.get_rand_perpendicular_vector());
				ref_y = sp_normalize(sp_cross(ref_z, ref_x));

				if (adj_iterations >= ADJ_MAX_ITERATIONS) {
					_max_num_sources = cur_max_num_src;
					adj_iterations = 0;
				}
				bs_min = _min_src_spacing;
			} else if (bs_min + DOUBLE_ACCURACY >= probe_spacing) {
				bs_min = _min_src_spacing;
			} else {
				bs_max = probe_spacing;
				double bs_mid = 0.5f * (bs_min + bs_max);
				probe_spacing = bs_mid;
			}
		}

		// to prevent infinite loop, change coordinate system if cannot arrive at _max_num_sources
		if (_current_state.size() != 0 && (bs_min + DOUBLE_ACCURACY >= bs_max || adj_iterations > ADJ_MAX_ITERATIONS)) {
			ref_x = sp_normalize(ref_line.get_rand_perpendicular_vector());
			ref_y = sp_normalize(sp_cross(ref_z, ref_x));
			bs_min = _min_src_spacing;
			bs_max = _max_src_spacing;
            adj_iterations = 0;
			_max_num_sources = max_num_sources;
		}

		cout << "Currently " << _current_state.size() << " sources, step size is " << probe_spacing << ", target " << _max_num_sources << " sources." << endl;
		_current_state.clear();
#endif

	// compute size of grid to check
	double dist_x = 0.5 * (bounding_box[0].get_distance_from(bounding_box[1]));
	double dist_y = 0.5 * (bounding_box[0].get_distance_from(bounding_box[2]));
	int step_x = (int) (dist_x / probe_spacing);
	int step_y = (int) (dist_y / probe_spacing);

	// loop through grid
	for (int i = -step_x; i <= step_x; i++) {
		for (int j = -step_y; j <= step_y; j++) {
			// displace line
			SP_Point displacement = (float)(i * probe_spacing) * ref_x + (float)(j * probe_spacing) * ref_y;
			SP_Line cur_line (ref_line.end0 + displacement, ref_line.end1 + displacement);

			// continue if no valid source can be placed
			if (place_source_from_line(cur_line)) {
				// check if effective and push into current placement
				if (cur_line.length >= min_src_length) {
					_current_state.push_back(cur_line);
				}
			}
		}
	}
#ifdef ADJUST_SPACING
	}
#endif

	// sort and constrain lengths
	sort (_current_state.begin(), _current_state.end(), greater<SP_Line>());
	for (unsigned i = 0; i < _current_state.size(); i++) {
		if (_current_state[i].length > _init_source_lengths[i]) {
			// shorten current source to match max allowed length
			constrain_source_length(_current_state[i], _init_source_lengths[i]);
		} else if (_init_source_lengths[i] == _INFINITY_) {
			// heuristically add length constraint to save runtime
			_init_source_lengths[i] = _current_state[i].length;
		}
	}

	// print generated initial placement
	cout << "Generated initial placement with " << _current_state.size() << " sources.\n";
    for (auto line : _current_state)
        std::cout << line << "          length " << line.length << std::endl;
}

//! Helper method: generates a valid initial solution based on current injection point constraint
// Slow.
void sa_engine::generate_valid_initial_solution() {
#ifdef SA_DBG
    std::cout << std::endl << "sa_engine::inside generate_valid_initial_solution" << std::endl;
#endif

	double probe_spacing = 12.0f; // fixing to be 1cm (10mm)
	double min_src_length = 0.0f; // setting minimum effective length to be 0mm

#ifdef ADJUST_SPACING
    int adj_iterations = 0;
    int ADJ_MAX_ITERATIONS = 20;
#endif

	_current_state.clear();

	// sort maximum constraint lengths
	sort(_init_source_lengths.begin(), _init_source_lengths.end(), greater<double>());
	//if (_init_source_lengths[_init_source_lengths.size()-1] != _INFINITY_) min_src_length = _init_source_lengths[0];

	// Precomputed center of tumor volume
	// choose center over centroid because want to cover entire tumor volume.
	SP_Point tumor_center = _power_allocator->get_tumor_center();

	// Draw a line from center of circle to tumor_center
	SP_Line ref_line = SP_Line(_current_inj_center, tumor_center);

	// Expand to a grid of parallel lines
	// define coordinate system
	SP_Point ref_z = sp_normalize(ref_line.end1 - ref_line.end0);
	SP_Point ref_x = sp_normalize(ref_line.get_rand_perpendicular_vector());
	SP_Point ref_y = sp_normalize(sp_cross(ref_z, ref_x));

#ifdef ADJUST_SPACING
	// binary search to adjust spacing
	double bs_min = _min_src_spacing*2 - probe_spacing, bs_max = _max_src_spacing;
	unsigned max_num_sources = _max_num_sources;
	unsigned cur_max_num_src = 0;
	while (_current_state.size() == 0 || _current_state.size() != _max_num_sources) {
		if (_current_state.size() == _max_num_sources + 1) {
			// remove source with the shortest length to terminate
			sort (_current_state.begin(), _current_state.end(), greater<SP_Line>());
			_current_state.erase(_current_state.end() - 1);
            continue;
		} else if (_current_state.size() > _max_num_sources) {
			if (bs_max - DOUBLE_ACCURACY <= probe_spacing) {
				fprintf(stderr, "\033[1;%dmsa_engine::generate_valid_initial_solution::not enough probes, exiting...\033[0;m\n", 31);
        		std::exit(-1);
			} else {
				bs_min = probe_spacing;
				double bs_mid = 0.5f * (bs_min + bs_max);
				probe_spacing = bs_mid;
			}
		} else if (_current_state.size() > 0) {
			if (_min_src_spacing + DOUBLE_ACCURACY >= probe_spacing) {
				cur_max_num_src = max(cur_max_num_src, (unsigned) _current_state.size());
				_max_num_sources = max(_max_num_sources - 1, cur_max_num_src);
				ref_x = sp_normalize(ref_line.get_rand_perpendicular_vector());
				ref_y = sp_normalize(sp_cross(ref_z, ref_x));

				if (adj_iterations >= ADJ_MAX_ITERATIONS) {
					_max_num_sources = cur_max_num_src;
					adj_iterations = 0;
				}
				bs_min = _min_src_spacing;
			} else if (bs_min + DOUBLE_ACCURACY >= probe_spacing) {
				bs_min = _min_src_spacing;
			} else {
				bs_max = probe_spacing;
				double bs_mid = 0.5f * (bs_min + bs_max);
				probe_spacing = bs_mid;
			}
		}

		// to prevent infinite loop, change coordinate system if cannot arrive at _max_num_sources
		if (_current_state.size() != 0 && (bs_min + DOUBLE_ACCURACY >= bs_max || adj_iterations > ADJ_MAX_ITERATIONS)) {
			ref_x = sp_normalize(ref_line.get_rand_perpendicular_vector());
			ref_y = sp_normalize(sp_cross(ref_z, ref_x));
			bs_min = _min_src_spacing;
			bs_max = _max_src_spacing;
            adj_iterations = 0;
			_max_num_sources = max_num_sources;
		}

		cout << "Currently " << _current_state.size() << " sources, step size is " << probe_spacing << ", target " << _max_num_sources << " sources." << endl;
		_current_state.clear();
#endif
	// compute size of grid to check
	double dist_x = _INFINITY_, dist_y = _INFINITY_;
	get_rand_dist(ref_line, ref_x, dist_x);
	get_rand_dist(ref_line, ref_y, dist_y);
	int step_x = (int) (dist_x / probe_spacing);
	int step_y = (int) (dist_y / probe_spacing);

	// loop through grid
	for (int i = -step_x; i <= step_x; i++) {
		for (int j = -step_y; j <= step_y; j++) {
			// displace line
			SP_Point displacement = (float)i * probe_spacing * ref_x + (float)j * probe_spacing * ref_y;
			SP_Line cur_line = SP_Line(ref_line.end0 + displacement, ref_line.end1 + displacement);

			// continue if line doesn't pass through injection circle
			if (is_valid_injection(cur_line)) {
				// continue if no valid source can be placed
				if (place_source_from_line(cur_line)) {
					// check if effective and push into current placement
					if (cur_line.length >= min_src_length) {
						_current_state.push_back(cur_line);
					}
				}
			}
		}
	}
#ifdef ADJUST_SPACING
	}
#endif

	// sort and constrain lengths
	sort (_current_state.begin(), _current_state.end(), greater<SP_Line>());
	for (unsigned i = 0; i < _current_state.size(); i++) {
		if (_current_state[i].length > _init_source_lengths[i]) {
			// shorten current source to match max allowed length
			constrain_source_length(_current_state[i], _init_source_lengths[i]);
		} else if (_init_source_lengths[i] == _INFINITY_) {
			// heuristically add length constraint to save runtime
			_init_source_lengths[i] = _current_state[i].length;
		}
	}

	// print generated initial placement
	cout << "Generated initial placement with " << _current_state.size() << " sources.\n";
    for (auto line : _current_state)
        std::cout << line << "          length " << line.length << std::endl;
}

//! Helper method: generates the initial solution
void sa_engine::create_initial_solution()
{
	// _current_state set moved to constructor

	// Run initial placement generation 3 times and choose the best one
    unsigned N = (_early_termination) ? 1 : 3;
	// if no initial solution or current initial solution not satisfying injection constraint
    if (_current_state.size() > 0 && (!_constrain_injection_point || is_valid_injection())) N = 0;

	vector<double> costs(N, 0.0);
	vector<vector<SP_Line> > states;
	unsigned bad_placement = 0;
	unsigned bad_placement_tolerance = 2;
	unsigned max_num_sources = _max_num_sources;
	for (unsigned i = 0; i < N; i++) {
		_current_state.clear();
		_max_num_sources = max_num_sources;

		if (_constrain_injection_point) {
			profiler gen_init_sol("Initial solution generation with injection constraint.");
			generate_valid_initial_solution();
		} else {
			profiler gen_init_sol("initial solution generation without injection constraint.");
			generate_random_initial_solution();
		}
		
		_single_change = false;
		costs[i] = compute_cost_v100(_current_state);//, false, "");

		// Regenerate if not good enough initial solution
		unsigned max_iterations = 10;
		for (unsigned j = 0; j < max_iterations && _ignore_move; j++) {
			_current_powers = _power_allocator->get_final_source_powers();
			_current_fluence = _power_allocator->get_final_fluence();
			cout << "Current source powers: \n";
			for (unsigned i = 0; i < _current_powers.size(); i++) {
				cout << _current_powers[i] << endl;
			}

			//_curr_move_type = MoveType::FORCE_MULTISOURCE;
			//generate_force_move();
			_current_state.clear();
			_max_num_sources = max_num_sources;

			if (_constrain_injection_point) {
				profiler gen_init_sol("Initial solution generation with injection constraint.");
				generate_valid_initial_solution();
			} else {
				profiler gen_init_sol("initial solution generation without injection constraint.");
				generate_random_initial_solution();
			}

			_single_change = false;
			costs[i] = compute_cost_v100(_current_state);//, false, "");
		}

		if (_ignore_move && bad_placement < bad_placement_tolerance) {
			fprintf(stderr, "\033[1;%dmsa_engine::create_initial_solution::bad initial placement warning...\033[0;m\n", 31);
			i--;
			bad_placement++;
		} else if (_ignore_move) {
			fprintf(stderr, "\033[1;%dmsa_engine::create_initial_solution::not able to create initial placement with current constraints...\033[0;m\n", 31);
			exit(-1);
		} else {
			states.push_back(_current_state);
		}
	}

	// compare initial solution costs and choose the best one to use
	if (N > 0) {
		double min_cost = costs[0];
		unsigned index = 0;
		for (unsigned i = 1; i < N; i++) {
			if (costs[i] < min_cost) {
				index = i;
				min_cost = costs[i];
			}
		}
		_current_state = states[index];
		_max_num_sources = (unsigned)_current_state.size();
	}

	// should now have an initial placement
	if (_current_state.size() == 0) {
        fprintf(stderr, "\033[1;%dmsa_engine::create_initial_solution::error generating initial placement...\033[0;m\n", 31);
        std::exit(-1);
	}

	// print final initial placement
	cout << "Final initial placement with " << _current_state.size() << " sources.\n";
    for (auto line : _current_state)
        std::cout << line << "          length " << line.length << std::endl;

    // get the sources' lengths
    _source_lengths.resize(_current_state.size());

    for (unsigned i = 0; i < _current_state.size(); i++)
    {
        double length = get_source_length(_current_state[i]);

        _source_lengths[i] = length;
    }

	// _init_source_lengths = _source_lengths;

#ifdef SA_LOG
    for (unsigned i = 0; i < _current_state.size(); i++)
    {
        std::cout << "Line " << i << ": " << _current_state[i] << "           length: " << _source_lengths[i] << endl;
    }
#endif

    _init_temp = get_initial_temperature();
	//_power_allocator->set_tumor_weight(_orig_tumor_weight);
	_curr_tumor_weight = _orig_tumor_weight;

	/*
	_current_state[0] = SP_Line(SP_Point(124.77177429f, 163.39497375f, 86.58021545f),
								SP_Point(123.22262573f, 161.69728088f, 96.31253052f));
    _current_state[1] = SP_Line(SP_Point(114.0f, 165.21618652f, 91.43785858f),
							    SP_Point(134.0f, 165.21618652f, 91.43785858f));
	_current_state[2] = SP_Line(SP_Point(114.0f, 157.36363226f, 97.54965210f),
								SP_Point(134.0f, 159.36363226f, 97.54965210f));
    _current_state[3] = SP_Line(SP_Point(114.0f, 157.15512085f, 89.81100464f),
							    SP_Point(134.0f, 159.15512085f, 89.81100464f));
	*/
}

//! Helper method: generates a new move to replace the zero-powered sources
void sa_engine::generate_zero_powered_source_move()
{
#ifdef SA_DBG
	cout << "Inside sa_engine::generate_zero_powered_source_move.\n";
#endif
	// Get ref line used in source generation
	SP_Point tumor_center = _power_allocator->get_tumor_center();
	SP_Line ref_line;
	if (_constrain_injection_point) ref_line = SP_Line(_current_inj_center, tumor_center);
	else ref_line = SP_Line(_current_inj_center, _current_inj_center + _current_inj_normal);
	SP_Point ref_z = sp_normalize(ref_line.end1 - ref_line.end0);

	// Find underdosed tumor tetra
	double num_packets = _power_allocator->get_num_packets();

	vector<SP_Point> partitions; // partitions on plane that are underdosed
	vector<int> weight;
	for (unsigned i = 0; i < _tumor_indices.size(); i++) {
		bool underdosed = (_current_fluence[_tumor_indices[i]-1] < ((num_packets/1000000)*_min_dose[_tumor_indices[i]-1] + DOUBLE_ACCURACY));

		if (underdosed) {
			// compute centroid of tetra
			tetrahedron *tet = _data->get_list_of_tetrahedra()[_tumor_indices[i]-1];
			SP_Point centroid = tet->compute_centroid_of();

			// find projected point on injection circle
			SP_Line cur_line(centroid, centroid + ref_z);
			if (!cur_line.get_intersection_with_plane(_current_inj_normal, _current_inj_center, centroid)) {
				cout << "Error in generate_zero_powered_source_move.\n";
				break;
			}

			// loop through all partitions and see if one is within 10mm
			bool found = false;
			for (unsigned j = 0; j < partitions.size() && !found; j++) {
				if (centroid.get_distance_from(partitions[j]) <= 10.0f) {
					// add current centroid into partition
					partitions[j] = weight[j]*partitions[j] + centroid;
					weight[j]++;
					partitions[j] = partitions[j] / weight[j];
					found = true;
				}
			}

			// add new partition
			if (!found) {
				partitions.push_back(centroid);
#ifdef SA_DBG
                cout << "Add new partition at " << centroid << endl;
#endif
				weight.push_back(1);
			}

			// // clean up partitions by combining close ones
			// for (unsigned j = 0; j < partitions.size() - 1; j++) {
			// 	for (unsigned k = j + 1; k < partitions.size(); k++) {
			// 		if (partitions[j].get_distance_from(partitions[k]) <= 5.0f) {
			// 			// combine the partitions
			// 			partitions[j] = weight[j]*partitions[j] + weight[k]*partitions[k];
			// 			weight[j] += weight[k];
			// 			partitions[j] = partitions[j] / weight[j];

			// 			// erase the second partition
			// 			partitions.erase(partitions.begin()+k);
			// 			weight.erase(weight.begin()+k);
			// 			k--;
			// 		}
			// 	}
			// }
		}
	}

	// Loop through zero-powered sources and insert into partitions
	vector<unsigned> source_ind;
	for (unsigned i = 0; i < _current_state.size(); i++) {
		if (_current_powers[i] <= DOUBLE_ACCURACY) source_ind.push_back(i);
	}
	for (unsigned i = (unsigned)(_current_state.size()); i < _max_num_sources; i++) {
		source_ind.push_back(i);
	}

	// limit the number of partitions by weight
	//double partition_size = (double) partitions.size();
	for (unsigned i = 0; i < partitions.size(); i++) {
#ifdef SA_DBG
		cout << "partition " << i << " weight " << weight[i] << endl;
#endif
		if (/*((double)_tumor_indices.size() / (double)weight[i]) > ((double)_current_powers.size() * partition_size)
                           || */_constrain_injection_point && partitions[i].get_distance_from(_current_inj_center) > _current_inj_diameter) {
			// Not significant enough or not within injection constraint
			partitions.erase(partitions.begin() + i);
			weight.erase(weight.begin() + i);
			i--;
		}
	}

	// find target positions for partitions
	vector<SP_Line> target_position;
	for (unsigned i = 0; i < partitions.size(); i++) {
		SP_Line cur_line(partitions[i], partitions[i]+ref_z);
		if (place_source_from_line(cur_line)) {
			int pos = 0;
			for (unsigned j = 0; j < target_position.size(); j++) { 
				if (weight[j] > weight[i]) pos++;
			}
			target_position.insert(target_position.begin() + pos, cur_line);
		} else {
			partitions.erase(partitions.begin() + i);
			weight.erase(weight.begin() + i);
			i--;
		}
	}
	sort(target_position.begin(), target_position.end(), greater<SP_Line>());

	_next_state = _current_state;
	if (target_position.size() == 0 || source_ind.size() == 0) {
		// nothing to be done
		cout << "sa_engine::generate_zero_powered_source_move: target position or number of sources is 0.\n";
		return;
	}

	// randomly choose number of sources to place
	unsigned count = rand() % static_cast<unsigned>(min(target_position.size(), source_ind.size())) + 1;

#ifdef SA_DBG
	cout << "Final number of partitions: " << target_position.size() << "\nNumber of zero-powered sources: " << source_ind.size() << "\n";
#endif
	cout << "Relocating " << count << " sources.\n";

	for (unsigned i = 0; i < count; i++) {
		// insert into partitions
		constrain_source_length(target_position[i], _init_source_lengths[source_ind[i]]);
		
		// check if no overlaps
		bool overlap = false;
		for (unsigned j = 0; j < _next_state.size() && !overlap; j++)
		{
			if (j != source_ind[i])
			{
				if (do_intersect(target_position[i], _next_state[j]))
				{
#ifdef SA_DBG
		std::cout << "line intersects with source " << _next_state[j] << std::endl;
#endif
					overlap = true;
				}
			}
		}
		if (!overlap) {
#ifdef SA_LOG
			cout << "Source " << source_ind[i] << " relocated.\n";
#endif
			if (_next_state.size() > source_ind[i]) {
				_next_state[source_ind[i]] = target_position[i];
				_source_lengths[source_ind[i]] = target_position[i].length;
			} else {
				_next_state.push_back(target_position[i]);
				_source_lengths.push_back(target_position[i].length);
			}
		} else {
			// disregard target_position[i]
			target_position.erase(target_position.begin() + i);
			i--;
		}
	}

	// print source positions
#ifdef SA_DBG
	std::cout << std::endl << "generate_zero_powered_source_move." << std::endl << "Next state is:" << std::endl;
	for(unsigned i = 0; i < _next_state.size(); i++)
		std::cout << _next_state[i] << std::endl;
	std::cout << std::endl;
#endif
}

//! Helper method: an adaptive method to compute an initial temperature
//! for the annealer no matter what the number of sources or the mesh
//! size are. The idea is to compute a temperature that will virtually
//! lead to accepting any move at the beginning.
double sa_engine::get_initial_temperature()
{
    double init_temp = 1e4;

    /*
     * Idea is similar to VPR:
     * create an initial solution, then randomly generate N other moves.
     * Then compute the std dev of the cost of these N moves, then set
     * the temp to M times that std dev.
     */
    unsigned N = (_early_termination) ? 1 : 11;//*_current_state.size(); // random moves
    vector<double> costs(N, 0.0);
	_single_change = false;
    costs[0] = compute_cost_v100(_current_state);//, false, "");
	unsigned regenerate_tolerance = 3;
	unsigned regenerate_iteration = 0;

	// Regenerate if not good enough initial solution
	while (_ignore_move && regenerate_iteration < regenerate_tolerance) {
		regenerate_iteration++;
		_current_powers = _power_allocator->get_final_source_powers();
		_current_fluence = _power_allocator->get_final_fluence();
		cout << "Current source powers: \n";
		for (unsigned i = 0; i < _current_powers.size(); i++) {
			cout << _current_powers[i] << endl;
		}

		//_curr_move_type = MoveType::FORCE_MULTISOURCE;
		//generate_force_move();
		generate_zero_powered_source_move();
		_current_state = _next_state;

		_single_change = false;
		costs[0] = compute_cost_v100(_current_state);//, false, "");
	}

	if (_ignore_move) {
		fprintf(stderr, "\033[1;%dmsa_engine::get_initial_temperature::bad initial placement with current constraints...\033[0;m\n", 31);
		exit(-1);
	}

    double mean = costs[0];

	_curr_move_type = MoveType::RANDOM;
    for (unsigned i = 1; i < N; i++)
    {
        unsigned change_index = generate_move(true);
		if (is_in_mesh(_next_state[change_index]))
		{
			_single_change = false;
        	costs[i] = compute_cost_v100(_next_state);//, false, "");
        	if (_ignore_move)
			{
				i--;
			}
			else
			{
				mean += costs[i];
#ifdef LEARNING_BASED_MOVE
				update_weight(change_index, costs[0], costs[i]);
#endif
			}
		}
		else {
			i--; // repeat
		}
    }
    _next_state.clear();
    _next_state.resize(0);

    mean /= N;

    double std_dev = 0;
    for (double cost : costs) {
        std_dev += (cost - mean)*(cost - mean);
    }
    std_dev = sqrt(std_dev/N);
    init_temp = std_dev*20; // TODO: 20 is just a number to change

#ifdef SA_LOG
    fprintf(stderr, "\033[1;%dmInitial temperature is: %g\033[0;m\n", 31, init_temp);
    fprintf(stdout, "Initial temperature is: %g \n", init_temp);
#endif

    return init_temp;
}

//! Helper method: computes the cost of a state of the solution
double sa_engine::compute_cost(const std::vector<SP_Line>& state, bool write_to_file, std::string file_name) const
{
    vector<float> fluence;

	bool use_interpolation = false;
#ifdef USE_INTERPOLATION
    use_interpolation = true;
#endif
	unsigned num_packets = 100000;

	vector<float> src_powers;//= { 0.00032413f, 0.00001963f, 0.00000054f, 0.00003117f};

    // create a vector of sources
    if (!use_interpolation)
    {
        std::vector<Source::Abstract*> sources(state.size());
        for (unsigned i = 0; i < state.size(); i++)
        {
            auto src = state[i];

            if (_source_lengths[i] < 1e-6) // point source
            {
                Source::Point *p = new Source::Point();
                p->position({src.end0.get_xcoord(), src.end0.get_ycoord(), src.end0.get_zcoord()});
                sources[i] = p;
            }
            else
            {
                Source::Line * line = new Source::Line();
                line->endpoint(0, {src.end0.get_xcoord(), src.end0.get_ycoord(), src.end0.get_zcoord()});
                line->endpoint(1, {src.end1.get_xcoord(), src.end1.get_ycoord(), src.end1.get_zcoord()});
                sources[i] = line;
            }
			if (!src_powers.empty()) {
				sources[i]->power(src_powers[i]);
			}
        }
        {
            profiler FM ("Running FM in SA");
            _fullmonte_engine->run_composite_fullmonte(sources, fluence, write_to_file, file_name, num_packets);
        }

        for (auto s : sources) {
            delete s;
        }
    }
    else
    {
#ifdef USE_INTERPOLATION
        profiler INTERPOLATION("Running interpolation engine");
        fluence.resize(_num_tetra);
        std::fill(fluence.begin(), fluence.end(), 0.0);
        for (unsigned i = 0; i < state.size(); i++)
        {
            // TODO: 700 is so much!
            vector<float> fluence_temp = SP_IE::interpolate_line_source(state[i], _grid_min, *_grid_sources, 700, _delta_x);
            for (unsigned j = 0; j < _num_tetra; j++)
            {
                fluence[j] += fluence_temp[j];
            }
        }
#endif
    }

    if (fluence.size() != _min_dose.size() || fluence.size() != _max_dose.size())
    {
        fprintf(stderr, "\033[1;%dmsa_engine::compute_cost::flunece size doesn't match threshold dose size...\033[0;m\n", 31);
        std::exit(-1);
    }

    double cost = 0;
    for (unsigned i = 0; i < fluence.size(); i++)
    {
        if (_min_dose[i] < DOUBLE_ACCURACY) // healthy tissue element
        {
            cost += max(0.0, static_cast<double>(fluence[i]) - ((num_packets/1000000)*_max_dose[i]));
        }
        else
        {
            cost += max(0.0, ((num_packets/1000000)*_min_dose[i]) - static_cast<double>(fluence[i]));
        }
    }
#ifdef SA_DBG
    std::cout << std::endl << "Cost is: " << cost << std::endl;
#endif


    return cost;
}

//! Helper method: computes cost based on v100s
double sa_engine::compute_cost_v100(const std::vector<SP_Line>& state)
{
	/*
	 * This method runs the current state of sources (one source at a time), generates a dose matrix
	 * and then runs the power allocation engine. Then we compute the cost based on the
	 * overall damage to the white and grey matters.
	 */

	profiler cost_prof("Computing move cost");

	_ignore_move = false;
	_power_allocator->set_tumor_weight(_curr_tumor_weight);

	{
		profiler fm_cost_prof("Running simulator inside compute cost");
		std::vector<Src_Abstract*> srcs (state.size());
		for (unsigned i = 0; i < state.size(); i++) {
			if (state[i].length > 1e-6) srcs[i] = new Src_Line (state[i]);
			else srcs[i] = new Src_Point (state[i].end1);
		}
		_power_allocator->set_initial_placement(srcs);

		if (_single_change) {
			_power_allocator->run_fullmonte_single_source(srcs, _change_idx);
		} else {
			_power_allocator->run_fullmonte_multiple_sources(srcs);
		}

		for (unsigned i = 0; i < state.size(); i++)
			delete srcs[i];
	}

	{
		profiler tetra_data_prof("Computing tetra data in compute cost");
		_power_allocator->remove_unnecessary_tetra(_guardbanded_thresholds);
		_power_allocator->compute_tetra_data(_guardbanded_thresholds);
		_power_allocator->set_p_max_vector(_INFINITY_);
	}

	{
		profiler init_lp_prof("Initializing LP in compute cost");
		_power_allocator->initialize_lp();
	}

	{
		profiler solve_lp_prof("Solving LP in compute cost");
		_power_allocator->solve_lp(_guardbanded_thresholds);
	}

	double cost = 0.0;
	if (_power_allocator->get_num_iterations() <= 11)
	{
		profiler v100_prof("Computing V100 in compute cost");

		_curr_tumor_weight = _power_allocator->get_tumor_weight();

		_power_allocator->compute_post_opt_fluence();

		vector<double> v100s = _power_allocator->compute_v_alpha_tissues(_guardbanded_thresholds, 1.0);

		// TODO: these should be generalized
		unsigned white_matter_idx = 3, grey_matter_idx = 2;

    	cost =  2*1000*v100s[white_matter_idx] + 1000*v100s[grey_matter_idx]; // computing cost in terms of volume
																// damaged in cubed millimeter
																// and giving higher priority to white matter

		cout << "Power allocation cost = " << _power_allocator->get_last_cost()*_power_allocator->get_total_energy() << "; SA cost = " << cost << std::endl;

//		cost = _power_allocator->get_last_cost()*_power_allocator->get_total_energy();///get_average_distance_sources(state);
	}
	else
	{
		cost = _INFINITY_;
		_ignore_move = true;

		_power_allocator->compute_post_opt_fluence();
	}
#ifdef SA_DBG
    std::cout << std::endl << "Cost is: " << cost << std::endl;
#endif
	return cost;
}

#ifdef RECOMPUTE
//! Helper method: computes cost based on v100s using previous FM results (use with caution)
double sa_engine::compute_cost_v100_no_fm()
{
	/*
	 * This method runs the current state of sources (one source at a time), generates a dose matrix
	 * and then runs the power allocation engine. Then we compute the cost based on the
	 * overall damage to the white and grey matters.
	 */

	profiler cost_prof("Re-Computing move cost");

	_ignore_move = false;
	
	{
		profiler tetra_data_prof("Computing tetra data in compute cost");
		_power_allocator->reset_source_powers();
		_power_allocator->remove_unnecessary_tetra(_guardbanded_thresholds);
		_power_allocator->compute_tetra_data(_guardbanded_thresholds);
		_power_allocator->set_p_max_vector(_INFINITY_);
	}

	{
		profiler init_lp_prof("Initializing LP in compute cost");
		_power_allocator->initialize_lp();
        _power_allocator->set_final_source_powers(vector<double>());
	}

	{
		profiler solve_lp_prof("Solving LP in compute cost");
		_power_allocator->solve_lp(_guardbanded_thresholds);
	}

	double cost = 0.0;
	if (_power_allocator->get_num_iterations() <= 11)
	{
		profiler v100_prof("Computing V100 in compute cost");

		_curr_tumor_weight = _power_allocator->get_tumor_weight();

		_power_allocator->compute_post_opt_fluence();

		vector<double> v100s = _power_allocator->compute_v_alpha_tissues(_guardbanded_thresholds, 1.0);

		// TODO: these should be generalized
		unsigned white_matter_idx = 3, grey_matter_idx = 2;

    	cost =  2*1000*v100s[white_matter_idx] + 1000*v100s[grey_matter_idx]; // computing cost in terms of volume
																// damaged in cubed millimeter
																// and giving higher priority to white matter

		cout << "Power allocation cost = " << _power_allocator->get_last_cost()*_power_allocator->get_total_energy() << "; SA cost = " 	<< cost << std::endl;

		// cost = _power_allocator->get_last_cost()*_power_allocator->get_total_energy();///get_average_distance_sources(state);
	}
	else
	{
		cost = _INFINITY_;
		_ignore_move = true;

		_power_allocator->compute_post_opt_fluence();
	}
#ifdef SA_DBG
    std::cout << std::endl << "Cost is: " << cost << std::endl;
#endif
	return cost;
}
#endif


//! Helper method: generates the next move
unsigned sa_engine::generate_move(bool only_random)
{
#ifdef SA_DBG
    std::cout << std::endl << "sa_engine::Inside generate_move" << std::endl;
#endif
    unsigned change_idx = rand() % static_cast<unsigned>(_current_state.size());
	_single_change = false; // initialize

    _next_state = _current_state;
    double prev_length = _source_lengths[change_idx];

    // generate a random integer length (with max equal to init length)
    double length = 10*ceil((0.1*rand()/RAND_MAX)*_init_source_lengths[change_idx]);

#ifdef SA_DBG
    std::cout << "change_idx = " << change_idx << " current length = " << prev_length << std::endl;
//     _current_temp = 0.45*_init_temp;
#endif

    // Change the current position by a max of rand_dist depending on the accpetance rate
    // largest tumor we have has a radius of around 5 cm
    double acceptance_rate = get_acceptance_rate();
	cout << "Acceptance rate is: " << acceptance_rate*100 << "%" << endl;

	double rand_dist = _curr_max_move;
	//rand_dist = (rand_dist*rand())/RAND_MAX;


	// get the move type based on probabilites
	if (!only_random)
		_curr_move_type= get_next_move_type();

	//double rand_n = (1.0*rand())/RAND_MAX;
	if (_generate_learning_only)
	{
#ifdef LEARNING_BASED_MOVE
		generate_learned_move(change_idx, length);
#ifdef SINGLE_FULLMONTE
		_single_change = true;
#endif
		_change_idx = change_idx;
#endif
	}
	else
	{
		if (only_random || _curr_move_type == MoveType::RANDOM) //_current_temp >= acceptance_rate*_init_temp) // change randomly
		{
#ifdef SA_DBG
    		std::cout << "move type = RANDOM" << std::endl;
#endif
			SP_Point end0 = _current_state[change_idx].end0;

			end0.set_xcoord(static_cast<float>((end0.get_xcoord() - rand_dist) + ((1.0*rand())/RAND_MAX)*2*rand_dist));
			end0.set_ycoord(static_cast<float>((end0.get_ycoord() - rand_dist) + ((1.0*rand())/RAND_MAX)*2*rand_dist));
			end0.set_zcoord(static_cast<float>((end0.get_zcoord() - rand_dist) + ((1.0*rand())/RAND_MAX)*2*rand_dist));

			_curr_move_range = end0.get_distance_from(_current_state[change_idx].end0);

			SP_Line new_src(end0, _current_state[change_idx].end1);
			rand_rotate_source(new_src, length);

			_next_state[change_idx] = new_src;
			_source_lengths[change_idx] = length;

#ifdef SINGLE_FULLMONTE
			_single_change = true;
#endif
			_change_idx = change_idx;

#ifdef SA_DBG
			std::cout << "Random New source is: " << _next_state[change_idx] << " with length " << length/10 << " cm" << std::endl;
#endif
		} else if (_curr_move_type == MoveType::FORCE_MULTISOURCE || _curr_move_type == MoveType::FORCE_CLOSEST) {
#ifdef SA_DBG
    		std::cout << "move type = FORCE" << std::endl;
#endif
			generate_force_move();
			_chose_force_based = true;
			change_idx = 0;
			
			_single_change = false;
#ifdef SA_DBG
			std::cout << "Force based new source is: " << _next_state[change_idx] << std::endl;
#endif
		} else if (_curr_move_type == MoveType::DISPLACE) {
#ifdef SA_DBG
    		std::cout << "move type = DISPLACE" << std::endl;
#endif
			// figure out the source orientation and displace in an orthogonal direction
			if (abs(prev_length) < 1e-6) // point source
			{
				SP_Point end0 = _current_state[change_idx].end0;

				end0.set_xcoord(static_cast<float>((end0.get_xcoord() - rand_dist) + ((1.0*rand())/RAND_MAX)*2*rand_dist));
				end0.set_ycoord(static_cast<float>((end0.get_ycoord() - rand_dist) + ((1.0*rand())/RAND_MAX)*2*rand_dist));
				end0.set_zcoord(static_cast<float>((end0.get_zcoord() - rand_dist) + ((1.0*rand())/RAND_MAX)*2*rand_dist));

				_curr_move_range = end0.get_distance_from(_current_state[change_idx].end0);

				SP_Line new_src(end0, end0);
				_next_state[change_idx] = new_src;

#ifdef SINGLE_FULLMONTE
				_single_change = true;
#endif
				_change_idx = change_idx;
			}
			else
			{
				SP_Line src = _current_state[change_idx];

				// 1- generate a random unit vector orthogonal to the line source
				double dir_x = 0.0, dir_y = 0.0, dir_z = 0.0;
				get_rand_perpendicular(src, dir_x, dir_y, dir_z);

				// 2- generate a random distance between 0 and x-cm and multiply the found vector with that distance
				if (_constrain_injection_point) get_rand_dist(src, SP_Point((float)dir_x, (float)dir_y, (float)dir_z), rand_dist);
				double rand_distance = (rand_dist*rand())/RAND_MAX;

				_curr_move_range = rand_distance;

#ifdef SA_DBG
				std::cout << "direction vector to displace: <" << dir_x
						<< ", " << dir_y << ", " << dir_z << "> with distance "
						<< rand_distance << std::endl;
#endif
				dir_x *= rand_distance;
				dir_y *= rand_distance;
				dir_z *= rand_distance;

				// 3- move the current source in the direction found
				// with rand_dist units
				SP_Point end0 = src.end0, end1 = src.end1;
				src.end0.set_xcoord(static_cast<float>(end0.get_xcoord() + dir_x));
				src.end0.set_ycoord(static_cast<float>(end0.get_ycoord() + dir_y));
				src.end0.set_zcoord(static_cast<float>(end0.get_zcoord() + dir_z));

				src.end1.set_xcoord(static_cast<float>(end1.get_xcoord() + dir_x));
				src.end1.set_ycoord(static_cast<float>(end1.get_ycoord() + dir_y));
				src.end1.set_zcoord(static_cast<float>(end1.get_zcoord() + dir_z));

				_next_state[change_idx] = src;
			
#ifdef SINGLE_FULLMONTE
				_single_change = true;
#endif
				_change_idx = change_idx;
			}
#ifdef SA_DBG
			std::cout << "Displaced New source is: " << _next_state[change_idx] << std::endl;
#endif
		}
		else if (_curr_move_type == MoveType::RESET_BEST)
		{
#ifdef SA_DBG
    		std::cout << "move type = RESET_BEST" << std::endl;
#endif
			_next_state = _best_placement_so_far;
			_num_reset_to_best_moves++;
#ifdef SA_DBG
			std::cout << "Reset to best solution" << std::endl;
			
			_single_change = false;
#endif
		}
		else if (_curr_move_type == MoveType::LENGTH) {
#ifdef SA_DBG
    		std::cout << "move type = LENGTH" << std::endl;
#endif
			generate_rand_length_move(change_idx);
		}
		else if (_curr_move_type == MoveType::REMOVE_SOURCE) {
#ifdef SA_DBG
    		std::cout << "move type = REMOVE_SOURCE" << std::endl;
#endif
			// remove the source with the lease power
			generate_removal_move();
			
			_single_change = false;
		}
		else if (_curr_move_type == MoveType::RELOCATE) {
#ifdef SA_DBG
    		std::cout << "move type = RELOCATE" << std::endl;
#endif
			// relocate all zero-powered sources to partitioned locations
			generate_zero_powered_source_move();
			_chose_force_based = true;
			
			_single_change = false;
		}
		else // Rotate with a random angle and change length
		{
#ifdef SA_DBG
    		std::cout << "move type = ROTATE" << std::endl;
#endif
			rand_rotate_source(_next_state[change_idx], length);
			_source_lengths[change_idx] = length;
			
#ifdef SINGLE_FULLMONTE
			_single_change = true;
#endif
			_change_idx = change_idx;
#ifdef SA_DBG
			std::cout << "Rotated new source is: " << _next_state[change_idx] << std::endl;
#endif
		}
    }

#ifdef SA_DBG
    std::cout << "sa_engine::End of generate_move" << std::endl << std::endl;
#endif
    return change_idx;
}

//! Helper method: computes whether next solution can be accepted without updating states
bool sa_engine::is_move_accepted(double cost_diff) {
#ifdef SA_LOG
    std::cout << "Cost diff is: " << cost_diff << std::endl;
#endif

    if (cost_diff < 0) // better solution
    {
#ifdef SA_LOG
            std::cout << "Move Accepted" << std::endl;
#endif

        return true;
    }
    else
    {
		//return false;
        // generate a random number between 0 and 1
        double rand_num = rand()/(1.0*RAND_MAX);
        double acceptance_prob = std::exp((-1.0*cost_diff)/_current_temp);
#ifdef SA_LOG
        std::cout << "Acceptance probability is: " << acceptance_prob << std::endl;
#endif

        if (rand_num < acceptance_prob) //std::exp((-1.0*cost_diff)/_current_temp))
        {
#ifdef SA_LOG
            std::cout << "Move Accepted" << std::endl;
#endif
            return true;
        }
    }

    return false;
}

//! Helper method: accepts the next solution with a certain probability
bool sa_engine::accept_move(double cost_diff)
{
#ifdef SA_LOG
    std::cout << "Cost diff is: " << cost_diff << std::endl;
#endif

    if (cost_diff < 0) // better solution
    {
#ifdef SA_LOG
            std::cout << "Move Accepted" << std::endl;
#endif

		// update effectiveness
		_move_type_effectiveness[_curr_move_type] += abs(cost_diff);

        return true;
    }
    else
    {
		//return false;
        // generate a random number between 0 and 1
        double rand_num = rand()/(1.0*RAND_MAX);
        double acceptance_prob = std::exp((-1.0*cost_diff)/_current_temp);
#ifdef SA_LOG
        std::cout << "Acceptance probability is: " << acceptance_prob << std::endl;
#endif

		// update effectiveness
		if (!_ignore_move)
			_move_type_effectiveness[_curr_move_type] += (cost_diff*acceptance_prob);

        if (rand_num < acceptance_prob) //std::exp((-1.0*cost_diff)/_current_temp))
        {
            _num_accepted_climbs++;
#ifdef SA_LOG
            std::cout << "Move Accepted" << std::endl;
#endif
            return true;
        }
    }

    return false;
}

//! Helper method: computes the acceptance rate
double sa_engine::get_acceptance_rate()
{
    if (_num_moves == 0) {
        return 0;
    }

    // TODO: check whether to take num_moves or num_legal_moves
    return (_num_accepted_moves*1.0)/_num_moves;//legal_moves;
}

//! Helper method: updates the temperature
void sa_engine::update_temperature()
{
    double acceptance_rate = get_acceptance_rate();

    _curr_max_move = std::min(_max_tumor_radius,
                _old_max_move*(1 - 0.2 + acceptance_rate)); // trying to constrain the acceptance rate to 20%
    _old_max_move = _curr_max_move;

	//*
    //double beta = 0.01; // cooling parameter TODO: make it dependent on acceptance probability
    _current_temp = _current_temp/(1.0 + _beta*_current_temp);

    // */


	/*
	// TODO: These numbers may need to be changed. Experiment with
    double alpha = 1.0;
    if (acceptance_rate > 0.96) {
        alpha = 0.5; // cool too much
    }
    else if (acceptance_rate > 0.8) {
        alpha = 0.2;//9;
    }
    else if (acceptance_rate > 0.15) {
        alpha = 0.4;
    }
    else {
        alpha = 0.8;
    }

    _current_temp = _current_temp*alpha;
    // */
}

//! Method that implements the overall SA algorithm
void sa_engine::run_sa_engine(int inj_index)
{
#ifdef LEARNING_BASED_MOVE
	// create the initial grid of weights
	create_weight_grid(1.0); // 1mm // TODO
#endif
	// set seed for pseudo-random generator
	srand((unsigned int) time(NULL));

	// define injection point constraint
	if (!_constrain_injection_point && (inj_index < -1 || inj_index >= (int)_injection_points.injection_points.size())) {
		fprintf(stderr, "\033[1;%dmrun_sa_engine: invalid inj_index.\nExiting...\033[0m\n", 31);
		std::exit(-1);
	}

#ifdef SA_DBG
	fprintf(stderr, "\033[1;%dm\nRunning SA engine for injection point %d\033[0;m\n", 31, inj_index);
#endif

	if (_constrain_injection_point) {
		set_injection_point_constraint(inj_index);
	}

	// create initial solution
	{
		profiler init_sol("Creating initial solution and temp");
    	create_initial_solution();
	}

    _current_temp = _init_temp;

	bool run_force_based_move_only = false;//true;

	_single_change = false;
    double curr_cost = compute_cost_v100(_current_state);

	_best_placement_so_far = _current_state;
	_best_cost_so_far = curr_cost;

	_current_powers = _power_allocator->get_final_source_powers();
	_current_fluence = _power_allocator->get_final_fluence();

// #ifdef PRINT_SOURCES
	std::ofstream src_file;
	if (_print_sources) {
		// if (_sa_placement_file == "")
		// 	src_file.open("src_placements_with_time.txt");
		// else
			src_file.open(_sa_placement_file);

		src_file << _current_state.size() << std::endl;

		print_to_xml(src_file, _current_state);
		// for (unsigned ss = 0; ss < _current_state.size(); ss++)
		// {
		// 	src_file << _current_state[ss] << std::endl;
		// }
		src_file << std::endl;
	}

	if (_print_top_placements) {
		_top_placements.push(make_pair(curr_cost, _current_state));
	}
// #endif


	unsigned num_temp_update = 0;

    while (true)
    {
        fprintf(stderr, "\033[1;%dm\nCurrent temperature is: %g\033[0;m\n", 31, _current_temp);
#ifdef SA_LOG
        fprintf(stdout, "Current temperature is: %g \n", _current_temp);
#endif
		// Experimental fitting parameters
		//double a1 = 3367, b1 = 6943, c1 = 5056, a2 = 3.43e5, b2 = 3.457e4, c2 = 5329;
		//double x_param = curr_cost/static_cast<double>(_current_state.size());
		//if (_num_moves > (a1*std::exp(-std::pow((x_param - b1)/c1, 2.0)) + a2*std::exp(-std::pow((x_param - b2)/c2, 2.0))))
        //if (_current_temp < 1)//0.0001611*(curr_cost/static_cast<double>(_current_state.size()))) // 1) // cooling threshold reached

		if (_num_moves > _max_num_moves) // at least 600 moves
		{
			//if (_tumor_volume < 50000) // small tumor volume (less than 50cm3) // TODO: make sure this is in mm^3 for general cases
			//{
				if (_num_unaccepted_since_last_acceptance > _max_unaccepted_since_last_acceptance) // TODO: make this as input
				//if (_terminate_based_on_effectiveness)
				{
					break;
				}
			//}
			//else
			//{
			//	if (_num_unaccepted_since_last_acceptance > 400 ) { //num_temp_update >= 5 && _num_unaccepted_since_last_acceptance > 200) {
			//		break;
			//	}
			//}
		}

        unsigned i = 0;
        while (true)
        {
            if (i >= 10*std::pow(_current_state.size(), 1 + _moves_per_temp)) // dependent on the number of sources
            {
                break;
            }
			i++;

			std::cout << "Move number: " << _num_moves << endl;

            // create next move
            unsigned change_idx = 0;
			_chose_force_based = false;

			change_idx = generate_move(false);
            _num_moves++;

            if (is_legal(run_force_based_move_only || _chose_force_based, change_idx))
            {
				double next_cost;
				{
					profiler comp_cost_prof("Computing new cost");
					if (_single_change) _power_allocator->backup_matrix_single_source(_change_idx);
					else _power_allocator->backup_matrix();
                	next_cost = compute_cost_v100(_next_state);//, false, "");
				}
#ifdef SA_LOG
				std::cout << "Current cost: " << curr_cost << std::endl;
				std::cout << "Next cost: " << next_cost << std::endl;
#endif
#ifdef RECOMPUTE
				if (is_move_accepted(next_cost - curr_cost)){
					profiler recomp_cost_prof("Re-computing new cost");
					next_cost = compute_cost_v100_no_fm();//, false, "");
#ifdef SA_LOG
					std::cout << "Current cost: " << curr_cost << std::endl;
					std::cout << "Next cost: " << next_cost << std::endl;
#endif                          
				}
#endif

#ifdef LEARNING_BASED_MOVE
				if (!_ignore_move && !run_force_based_move_only) // TODO: need to check if force based then update for all sources
				{
					update_weight(change_idx, curr_cost, next_cost);
                }
#endif
				{
					profiler accept_prof("Accepting move and updating");

					if (next_cost < _best_cost_so_far) {
						_best_placement_so_far = _next_state;
						_best_cost_so_far = next_cost;
					}

					if (_print_top_placements) {
						assert (_top_placements.size() <= NUM_TOP_PLACEMENTS);
						if (_top_placements.top().first > next_cost) {
							if (_top_placements.size() == NUM_TOP_PLACEMENTS)
								_top_placements.pop();
							_top_placements.push(make_pair(next_cost, _next_state));
						}
					}

					if (accept_move(next_cost - curr_cost))
					{
						_power_allocator->reset_matrix_backup_flag();

						_num_accepted_moves++;

						if ((next_cost - curr_cost) > 1e-5 ||
							(next_cost - curr_cost) < -DOUBLE_ACCURACY) { // if cost diff is exactly 0 dont reset counter

							_num_unaccepted_since_last_acceptance = 0;
						}

						if (_curr_move_type == MoveType::FORCE_MULTISOURCE ||
							_curr_move_type == MoveType::FORCE_CLOSEST) { //_chose_force_based)
							_num_accepted_force_based++;
						}

						_current_state = _next_state;
						curr_cost = next_cost;

						for (unsigned ss = 0; ss < _current_state.size(); ss++) {
							_source_lengths[ss] = _current_state[ss].length;
						}

						_current_powers = _power_allocator->get_final_source_powers();
						_current_fluence = _power_allocator->get_final_fluence();
// #ifdef PRINT_SOURCES
                        if (_print_sources) {
							// for (unsigned ss = 0; ss < _current_state.size(); ss++)
							// {
							// 	src_file << _current_state[ss] << std::endl;
							// }
							print_to_xml(src_file, _current_state);
							src_file << std::endl;
						}
// #endif
#ifdef SA_LOG
						std::cout << "Move range: " << _curr_move_range << std::endl;
#endif
					}
					else {
						_num_unaccepted_since_last_acceptance++;
						if (_single_change) _power_allocator->restore_matrix_single_source(_change_idx);
						else _power_allocator->restore_matrix();
					}
				}
            }
			else {
				_num_unaccepted_since_last_acceptance++;
			}
        }

		{
			profiler update_prof("Updating temperature and probs");
			
        	// Update the temperature
        	update_temperature();

			// Update move types probabilites probabilites and re-initialize effectiveness
			update_move_types_probabilities();

			num_temp_update++;
    	}
	}

	if (_best_cost_so_far < curr_cost) {
		_final_solution = _best_placement_so_far;
#ifdef SA_LOG
		std::cout << "Resetting to best solution" << std::endl;
#endif
	}
	else {
    	_final_solution = _current_state;
	}

	for (auto line : _final_solution)
		line.update_length_and_dir();

#ifdef SA_LOG
    std::cout << "Final solution is: " << std::endl;
    for (auto line : _final_solution)
        std::cout << line << "          length " << get_source_length(line) << std::endl;
    _single_change = false; // ensure we run FM for all sources
	compute_cost_v100(_final_solution);//, true, "");

    std::cout << "Number of moves: " << _num_moves << std::endl;
    std::cout << "Number of legal moves: " << _num_legal_moves << " ==> " << (100.0*_num_legal_moves)/_num_moves << "%" << std::endl;
    std::cout << "Number of moves rejected from tumor: " << _num_rejected_tumor << std::endl;
    std::cout << "Number of moves rejected from intersections: " << _num_rejected_overlaps << std::endl;
    std::cout << "Number of moves rejected from injection point: " << _num_rejected_inj_point << std::endl;
    std::cout << "Number of moves rejected from source removal: " << _num_rejected_removals << std::endl;
    std::cout << "Number of accepted moves: " << _num_accepted_moves <<
                " ====> " << (100.0*_num_accepted_moves)/_num_legal_moves << "%" << std::endl;
    std::cout << "Number of accepted hill climbs: " << _num_accepted_climbs << std::endl;
	std::cout << "Number of accepted force-based moves: " << _num_accepted_force_based << std::endl;
	std::cout << "Number of RESET_BEST moves taken: " << _num_reset_to_best_moves << std::endl;
#endif

// #ifdef PRINT_SOURCES
	if (_print_sources)
		src_file.close();
// #endif

	if (_print_top_placements) {
		cout << "\nPrinting top placements...\n\n";

		ofstream top_placement_file (_final_sa_placement_file);

		top_placement_file << "Top " << _top_placements.size() << " placements are listed:\n\n";

		while (_top_placements.size() > 0) {
			top_placement_file << "Rank " << _top_placements.size() << " placement:\n";
			
			print_to_xml(top_placement_file, _top_placements.top().second);
			_top_placements.pop();
			top_placement_file << "\n\n";
		}

		top_placement_file.close();
	}

}

//! Method to get the final solution
std::vector<Src_Abstract*> sa_engine::get_placement_solution() const
{
	vector<Src_Abstract*> sol;
	for (unsigned i = 0; i < _final_solution.size(); i++) {
        // _final_solution[i].update_length_and_dir(); 
		if (_final_solution[i].length < 1e-6) {
			Src_Point* p = new Src_Point (_final_solution[i].end0);
			sol.push_back(p);
		} else {
			Src_Line* l = new Src_Line (_final_solution[i]);
			sol.push_back(l);
		}
	}
    return sol;
}

//! Helper method: checks if the new move is legal
bool sa_engine::is_legal(bool run_force_based_move_only, unsigned src_change_idx)
{
#ifdef SA_DBG
    std::cout << std::endl << "sa_engine::inside is_legal" << std::endl;
#endif
	if (_next_state.size() == 0) {
#ifdef SA_DBG
		std::cout << "Invalid removal, removed the last source." << std::endl;
#endif
		_num_rejected_removals++;
		return false;
	}

	vector<unsigned> change_indices;
	if (!run_force_based_move_only)
	{
		change_indices.push_back(src_change_idx);
	}
	else
	{
		for (unsigned i = 0; i < _next_state.size(); i++)
		{
			change_indices.push_back(i);
		}
	}

	for (unsigned s = 0; s < change_indices.size(); s++)
	{
		unsigned change_idx = change_indices[s];

		// check if valid injection point
		if (_constrain_injection_point && !is_valid_injection(_next_state[change_idx])) {
#ifdef SA_DBG
			std::cout << "Invalid injection " << _next_state[change_idx] << std::endl;
#endif
			_num_rejected_inj_point++;
			return false;
		}

		// check if in tumor
		if (get_source_length(_next_state[change_idx]) < 1e-6) // point source
		{
			if (!is_pt_in_tumor(_next_state[change_idx].end0))
			{
				_num_rejected_tumor++;
				return false;
			}
		}
		else
		{
			if (!is_line_in_tumor(_next_state[change_idx]))
			{
				_num_rejected_tumor++;
#ifdef SA_DBG
				std::cout << "Line " << _next_state[change_idx] << " is not in tumor " << std::endl;
#endif
				return false;
			}
		}
#ifdef SA_DBG
		std::cout << "line is in tumor" << std::endl;
#endif


		// check if no overlaps
		for (unsigned i = 0; i < _next_state.size(); i++)
		{
			if (i != change_idx)
			{
				if (do_intersect(_next_state[change_idx], _next_state[i]))
				{
					_num_rejected_overlaps++;
#ifdef SA_DBG
		std::cout << "line intersects with source " << _next_state[i] << std::endl;
#endif
					return false;
				}
			}
		}
#ifdef SA_DBG
		std::cout << "sa_engine::line does not intersect with any other source" << std::endl;
#endif
	}

    _num_legal_moves++;
#ifdef SA_DBG
    std::cout << "sa_engine::End of is_legal" << std::endl << std::endl;
#endif
    return true;
}

//! Helper method: get tetrahedron id enclosing a point
unsigned sa_engine::get_tetra_enclosing_point(SP_Point & p)
{
	if (p._tetra == -1) {
		int iOriginalSTDIN_FILENO = -1;
		int iOriginalSTDOUT_FILENO = -1;
		int iOriginalSTDERR_FILENO = -1;
		if (!RUN_NIGHTLY) Util::redirect_standard_streams_to_DEVNULL(&iOriginalSTDIN_FILENO, &iOriginalSTDOUT_FILENO, &iOriginalSTDERR_FILENO);
    	unsigned tetra = _fullmonte_engine->get_tetra_enclosing_point(p.get_xcoord(), p.get_ycoord(), p.get_zcoord());
		if (!RUN_NIGHTLY) Util::restore_standard_streams(&iOriginalSTDIN_FILENO, &iOriginalSTDOUT_FILENO, &iOriginalSTDERR_FILENO);
		p._tetra = (int) tetra;
		return tetra;
	} else {
		return (unsigned) p._tetra;
	}
}

//! Helper method: checks if a point is inside the tumor
bool sa_engine::is_pt_in_tumor(SP_Point & p)
{
    // TODO: May be slow
    // get the tetra id enclosing the pt
	unsigned tetra = get_tetra_enclosing_point(p);

    // check if the tetra is a tumor element
    // TODO: may need to check if the tetra are 1 based or 0 based index
    if (find(_tumor_indices.begin(), _tumor_indices.end(), tetra) != _tumor_indices.end())
    {
        return true;
    }
    return false;
}


//! Helper method: checks if a line is inside the tumor
bool sa_engine::is_line_in_tumor(SP_Line & l)
{
    // TODO: only checks end points right now. This is not enough to
    // ensure that the whole line is inside the tumor
    if (is_pt_in_tumor(l.end0) && is_pt_in_tumor(l.end1))
    {
        return true;
    }
    return false;
}

//! Helper method: checks if a line is inside the mesh
bool sa_engine::is_in_mesh(SP_Line & l)
{
	// Only checks end points
	unsigned tetra = get_tetra_enclosing_point(l.end0);
	if (tetra == 0)
	{
		return false;
	}

	tetra = get_tetra_enclosing_point(l.end1);
	if (tetra == 0)
	{
		return false;
	}

	return true;
}

//! Helper method: checks if a point is inside the mesh
bool sa_engine::is_in_mesh(SP_Point & p)
{
	unsigned tetra = get_tetra_enclosing_point(p);
	if (tetra == 0)
	{
		return false;
	}

	return true;
}

//! Helper method: checks if 4 pts are coplanar
bool sa_engine::is_coplanar(const SP_Point & pt1, const SP_Point & pt2,
                            const SP_Point & pt3, const SP_Point & pt4)
{
    /*
     * Methodology:
     * 1- Get any three points and compute plane between them
     * 2- Check if the 4th point is on the plane
     * 3- If yes, return true and set plane to the found plane
     * 4- Else, return false
     */

    // To find the plane get the two vectors that connect 2 of the 3 pts
    double  v1_x = static_cast<double>(pt2.get_xcoord() - pt1.get_xcoord());
    double  v1_y = static_cast<double>(pt2.get_ycoord() - pt1.get_ycoord());
    double  v1_z = static_cast<double>(pt2.get_zcoord() - pt1.get_zcoord());
    double  v2_x = static_cast<double>(pt3.get_xcoord() - pt1.get_xcoord());
    double  v2_y = static_cast<double>(pt3.get_ycoord() - pt1.get_ycoord());
    double  v2_z = static_cast<double>(pt3.get_zcoord() - pt1.get_zcoord());

    // plane equation coefficients are computed from the cross product of the two vectors
    double  p_x = (v1_y*v2_z - v1_z*v2_y);
    double  p_y = (v1_z*v2_x - v1_x*v2_z);
    double  p_z = (v1_x*v2_y - v1_y*v2_x);

    double  d = -p_x*pt1.get_xcoord() - p_y*pt1.get_ycoord() - p_z*pt1.get_zcoord();

#ifdef SA_DBG
    std::cout << "Plane equation is: " << p_x << "*x + " << p_y << "*y + " << p_z
            << "*z + " << d << " = 0" << std::endl;
#endif

    // check if the forth pt is on the plane
    if (abs((p_x*pt4.get_xcoord() + p_y*pt4.get_ycoord() + p_z*pt4.get_zcoord()) + d) < 1e-6) // equals zero
    {
        return true;
    }

    return false;
}

//! Helper method: checks if two lines intersect
bool sa_engine::do_intersect(const SP_Line & line1, const SP_Line & line2)
{
    // check first if they are coplanar
    bool coplanar = is_coplanar(line1.end0, line1.end1, line2.end0, line2.end1);

    if (!coplanar)
    {
#ifdef SA_DBG
        std::cout << "Lines " << line1 << " and " << line2 << " are not coplanar" << std::endl;
#endif
        return false;
    }

    // Since they are coplanar, we can check if their xy-projections intersect
    // 1- find the orientations needed
    int o1 = get_orientation(line1.end0, line1.end1, line2.end0);
    int o2 = get_orientation(line1.end0, line1.end1, line2.end1);
    int o3 = get_orientation(line2.end0, line2.end1, line1.end0);
    int o4 = get_orientation(line2.end0, line2.end1, line1.end1);

#ifdef SA_DBG
    std::cout << "Orientations " << o1 << " " << o2 << " " << o3 << " "
        << o4 << std::endl;
#endif
    if (o1 != o2 && o3 != o4)
    {
        return true;
    }

    // check if any are colinear
    if (o1 == 0 && is_on_line(line1, line2.end0))
    {
        return true;
    }
    if (o2 == 0 && is_on_line(line1, line2.end1))
    {
        return true;
    }
    if (o3 == 0 && is_on_line(line2, line1.end0))
    {
        return true;
    }
    if (o4 == 0 && is_on_line(line2, line1.end1))
    {
        return true;
    }

    return false;
}

//! Helper method: get the orientation of 3 pts
//! returns 0 if collinear, 1 if cw, -1 if ccw
int sa_engine::get_orientation(const SP_Point& p1, const SP_Point & p2, const SP_Point & p3)
{
    // We can ignore the z-coordinate since we have checked that the lines are coplanar,
    // which implies that if their xy-projection intersect then they intersect

    double value = static_cast<double>((p2.get_ycoord() - p1.get_ycoord())*(p3.get_xcoord() - p2.get_xcoord()))
           - static_cast<double>((p3.get_ycoord() - p2.get_ycoord())*(p2.get_xcoord() - p1.get_xcoord()));

    if (abs(value) < 1e-6) // colinear
    {
        return 0;
    }

    return value > 0 ? 1 : 2;
}

//! Helper method: checks if a point lies on a line (assuming they are colinear)
bool sa_engine::is_on_line(const SP_Line & line, const SP_Point & pt)
{
#ifdef SA_DBG
    std::cout << "Checking if " << pt << " is on the line " << line << std::endl;
#endif

    if(pt.get_xcoord() <= fmax(line.end0.get_xcoord(), line.end1.get_xcoord()) + 1e-6 &&
       pt.get_xcoord() >= fmin(line.end0.get_xcoord(), line.end1.get_xcoord()) - 1e-6 &&
       pt.get_ycoord() >= fmin(line.end0.get_ycoord(), line.end1.get_ycoord()) - 1e-6 &&
       pt.get_ycoord() <= fmax(line.end0.get_ycoord(), line.end1.get_ycoord()) + 1e-6)
    {
        return true;
    }

    return false;
}

//! Helper method: returns a random perpendicular unit vector to the given source
void sa_engine::get_rand_perpendicular(const SP_Line & src, double & dir_x, double & dir_y, double & dir_z)
{
    // https://math.stackexchange.com/questions/2347215/how-to-find-a-random-unit-vector-orthogonal-to-a-random-unit-vector-in-3d/2347293
    //    a- get the vector components of the line source
    double l1_x = static_cast<double>(src.end1.get_xcoord() - src.end0.get_xcoord());
    double l1_y = static_cast<double>(src.end1.get_ycoord() - src.end0.get_ycoord());
    double l1_z = static_cast<double>(src.end1.get_zcoord() - src.end0.get_zcoord());

    //    b- generate vector, p, perpendicular to that vector and normalize it
    double p_x = l1_y - l1_z, p_y = l1_z - l1_x, p_z = l1_x - l1_y;
    if (abs(p_x) < 1e-9 && abs(p_y) < 1e-9 && abs(p_z) < 1e-9) // regenarate another vector
    {
        if (abs(l1_x) > 1e-9)
        {
            p_x = l1_y;
            p_y = -l1_x;
            p_z = 0.0;
        }
        else if (abs(l1_y) > 1e-9)
        {
            p_x = 0;
            p_y = l1_z;
            p_z = -l1_y;
        }
        else // they can't all be 0, otherwise it is a point source
        {
            p_x = l1_z;
            p_y = 0;
            p_z = -l1_x;
        }
    }

    double length_p = sqrt(p_x*p_x + p_y*p_y + p_z*p_z);
    double pn_x = p_x/length_p; // normal x component
    double pn_y = p_y/length_p;
    double pn_z = p_z/length_p;

    //     c- take the cross product of l1xp and generate vector v
    double v_x = l1_y*p_z - l1_z*p_y;
    double v_y = p_x*l1_z - p_z*l1_x;
    double v_z = l1_x*p_y - l1_y*p_x;

    // get the normal components of v
    double length_v = sqrt(v_x*v_x + v_y*v_y + v_z*v_z);
    double vn_x = v_x/length_v;
    double vn_y = v_y/length_v;
    double vn_z = v_z/length_v;

    //    d- generate a random angle between 0 and 2*PI
    double theta = (2*PI*rand())/RAND_MAX;

    //    e- random perpendicular vector = pn*cos(theta) + vn*sin(theta)
    dir_x = pn_x*cos(theta) + vn_x*sin(theta);
    dir_y = pn_y*cos(theta) + vn_y*sin(theta);
    dir_z = pn_z*cos(theta) + vn_z*sin(theta);

    //    f- normalize it
    double length_dir = sqrt(dir_x*dir_x + dir_y*dir_y + dir_z*dir_z);
    dir_x /= length_dir;
    dir_y /= length_dir;
    dir_z /= length_dir;
}

//! Helper method: returns the length of a given source
double sa_engine::get_source_length(const SP_Line& line)
{
    return line.end0.get_distance_from(line.end1);
}

#ifdef LEARNING_BASED_MOVE
//! Helper method: initialize grid of weights for learning based move
void sa_engine::create_weight_grid(double voxel_size)
{
	_grid_voxel_dim = voxel_size;

	// TODO: coordinates should be input from the mesh
	_mesh_x_min = 20; _mesh_x_max = 160;
	_mesh_y_min = 20; _mesh_y_max = 200;
	_mesh_z_min = 20; _mesh_z_max = 150;

	_grid_x_size = static_cast<unsigned>(ceil((_mesh_x_max - _mesh_x_min)/voxel_size));
	_grid_y_size = static_cast<unsigned>(ceil((_mesh_y_max - _mesh_y_min)/voxel_size));
	_grid_z_size = static_cast<unsigned>(ceil((_mesh_z_max - _mesh_z_min)/voxel_size));


	_history_weights.resize(_grid_x_size*_grid_y_size*_grid_z_size);

	std::fill(_history_weights.begin(), _history_weights.end(), 0.0); // initially all locations are of equal weights

	_num_max_regions = 4; // TODO should be an input
}

//! Helper method: update the weight of the voxel containing the current source after evaluating the actual cost
void sa_engine::update_weight(unsigned change_idx, double old_cost, double new_cost)
{

	// get the moved source
	SP_Line moved = _next_state[change_idx];

	double source_length = moved.end0.get_distance_from(moved.end1);

	SP_Point source_pos;
	if (source_length < 1e-3) // point source
	{
		source_pos = moved.end0;
	}
	else
	{
		source_pos.set_xcoord((moved.end0.get_xcoord() + moved.end1.get_xcoord())/2);
		source_pos.set_ycoord((moved.end0.get_ycoord() + moved.end1.get_ycoord())/2);
		source_pos.set_zcoord((moved.end0.get_zcoord() + moved.end1.get_zcoord())/2);
	}

	unsigned x_idx = static_cast<unsigned>(floor((source_pos.get_xcoord() - _mesh_x_min)/_grid_voxel_dim));
	unsigned y_idx = static_cast<unsigned>(floor((source_pos.get_ycoord() - _mesh_y_min)/_grid_voxel_dim));
	unsigned z_idx = static_cast<unsigned>(floor((source_pos.get_zcoord() - _mesh_z_min)/_grid_voxel_dim));

	unsigned voxel_idx = x_idx*_grid_y_size*_grid_z_size + y_idx*_grid_z_size + z_idx;

	double new_weight = _history_weights[voxel_idx] + (old_cost - new_cost)/old_cost;

	_history_weights[voxel_idx] = abs(new_cost - old_cost) > 1e-7 ? new_weight
			: _history_weights[voxel_idx]; // if cost didnt change, dont change weight

	// update the set of max histories that will randomly choose from
	if (new_cost < old_cost || _max_history.size() == 0)
	{
		if (_max_history.size() < _num_max_regions)
		{
			_max_history.push_back(std::make_pair(voxel_idx, new_weight));
		}
		else
		{
			if (_max_history[_num_max_regions - 1].second < new_weight)
			{
				_max_history[_num_max_regions - 1] = std::make_pair(voxel_idx, new_weight);

				std::sort(_max_history.begin(), _max_history.end(), [] (const std::pair<unsigned, double> & w1,
						const std::pair<unsigned, double> & w2) -> bool {
							return w1.second > w2.second;
						});
			}
		}
	}
}

//! Helper method: generate a new move for the source with change_idx that is based on history
void sa_engine::generate_learned_move(unsigned change_idx, double length)
{
	// Look at the num_regions highest weights and choose a region randomly

	// TODO: Weight the max history and use all of them to generate the new position

	unsigned rand_max_idx = static_cast<unsigned>(rand() % _max_history.size());
	std::pair<unsigned, double> new_region = _max_history[rand_max_idx];

	// compute the x,y and z indices of the voxel
	unsigned voxel_idx = new_region.first;
	unsigned grid_yz = _grid_y_size * _grid_z_size;
	unsigned x_idx = voxel_idx/grid_yz;
	unsigned y_idx = (voxel_idx - x_idx*grid_yz)/_grid_z_size;
	unsigned z_idx = voxel_idx - x_idx*grid_yz - y_idx*_grid_z_size;


	// generate a random point inside the voxel
	SP_Point random_center;
	random_center.set_xcoord(static_cast<float>(_mesh_x_min + x_idx*_grid_voxel_dim + (1.0*rand()/RAND_MAX)*_grid_voxel_dim));
	random_center.set_ycoord(static_cast<float>(_mesh_y_min + y_idx*_grid_voxel_dim + (1.0*rand()/RAND_MAX)*_grid_voxel_dim));
	random_center.set_zcoord(static_cast<float>(_mesh_z_min + z_idx*_grid_voxel_dim + (1.0*rand()/RAND_MAX)*_grid_voxel_dim));

	// generate a random direction
	double phi = ((1.0*rand())/RAND_MAX)*2*M_PI;
	double theta = ((1.0*rand())/RAND_MAX)*M_PI;

	SP_Point end0, end1;
	end0.set_xcoord(static_cast<float>(random_center.get_xcoord() - (length/2)*sin(phi)*cos(theta)));
	end0.set_ycoord(static_cast<float>(random_center.get_ycoord() - (length/2)*sin(phi)*sin(theta)));
	end0.set_zcoord(static_cast<float>(random_center.get_zcoord() - (length/2)*cos(phi)));
	end1.set_xcoord(static_cast<float>(random_center.get_xcoord() + (length/2)*sin(phi)*cos(theta)));
	end1.set_ycoord(static_cast<float>(random_center.get_ycoord() + (length/2)*sin(phi)*sin(theta)));
	end1.set_zcoord(static_cast<float>(random_center.get_zcoord() + (length/2)*cos(phi)));

	SP_Line new_src(end0, end1);
	_next_state[change_idx] = new_src;
	_source_lengths[change_idx] = length;
}
#endif

//! Helper method: generates a new move for the sources based on the force-based approach
void sa_engine::generate_force_move()
{
	profiler force_move("Generate force based move");
	// 1- Need a way to find closest source to each tetra
	// 2- Each tetra will contribute uniformly to the direction +dir if overdosed OAR, -dir if underdose Tumor and 0 otherwise
	// NOTE: might be slow if everytime we are going over all tetra every single time. Look only into wm, gm and tumor tetra

#ifdef SA_DBG
	std::cout << std::endl << "Generating force based move." << std::endl << "Current state is:" << std::endl;
	for(unsigned i = 0; i < _current_state.size(); i++)
		std::cout << _current_state[i] << std::endl;
	std::cout << std::endl;
#endif

	_next_state = _current_state;

	// Compute fluence of current state of sources // TODO: might be avoidable to save time
	vector<double> fluence = _current_fluence;

	double num_packets = _power_allocator->get_num_packets();

	/*
    // create a vector of sources
	std::vector<Source::Abstract*> sources(_current_state.size());
	for (unsigned i = 0; i < _current_state.size(); i++)
	{
		auto src = _current_state[i];

		if (_source_lengths[i] < 1e-6) // point source
		{
			Source::Point *p = new Source::Point();
			p->position({src.end0.get_xcoord(), src.end0.get_ycoord(), src.end0.get_zcoord()});
			sources[i] = p;
		}
		else
		{
			Source::Line * line = new Source::Line();
			line->endpoint(0, {src.end0.get_xcoord(), src.end0.get_ycoord(), src.end0.get_zcoord()});
			line->endpoint(1, {src.end1.get_xcoord(), src.end1.get_ycoord(), src.end1.get_zcoord()});
			sources[i] = line;
		}
		sources[i]->power(static_cast<float>(_current_powers[i]));
	}
	{
		profiler FM ("Running FM in force based move");
		_fullmonte_engine->run_composite_fullmonte(sources, fluence, false, "", num_packets);
	}

	for (auto s : sources) {
		delete s;
	}
	*/

	// Loop over all important tetra
	for (unsigned i = 0; i < _oar_and_tumor_indices.size(); i++)
	{

		bool overdosed = false, underdosed = false;
		double overdose = 0.0, underdose = 0.0;

		unsigned tet_idx = _oar_and_tumor_indices[i];

		// check if tetra is over- or under dosed
		if (_min_dose[tet_idx] < DOUBLE_ACCURACY) // OAR element
		{
			if (static_cast<double>(fluence[tet_idx]) > (num_packets/1000000)*_max_dose[tet_idx] + DOUBLE_ACCURACY) // not overdosed
			{
				overdosed = true;
				overdose = (static_cast<double>(fluence[tet_idx]) - (num_packets/1000000)*_max_dose[tet_idx]);
						//*(_oar_and_tumor_volumes[i]/_sum_oar_tumor_volumes); // 1e-5
												// is to divide by approximately the max value of the fluence so the magnitude of the
												// move is less than 1
			}
			else
			{
				continue;
			}
		}
		else // tumor element
		{
			//std::cout << "Fluence: " << fluence[tet_idx] << " min_dose: " << (num_packets/1000000)*_min_dose[tet_idx] << std::endl;
			//std::cout << _data->get_list_of_tetrahedra()[tet_idx]->get_material_id() << std::endl;
			//std::cout << setprecision(10) << fixed << _min_dose[tet_idx] << std::endl;
			if (static_cast<double>(fluence[tet_idx]) < (num_packets/1000000)*_min_dose[tet_idx] + DOUBLE_ACCURACY) // not underdosed
			{
			    //std::cout << "Fluence: " << fluence[tet_idx] << " min_dose: " << (num_packets/1000000)*_min_dose[tet_idx] << std::endl;
				underdosed = true;
				underdose = ((num_packets/1000000)*_min_dose[tet_idx] + DOUBLE_ACCURACY - static_cast<double>(fluence[tet_idx]));
							//*(_oar_and_tumor_volumes[i]/_sum_oar_tumor_volumes); // 1e-5
												// is to divide by approximately the max value of the fluence so the magnitude of the
			}
			else
			{
				continue;
			}
		}

		// compute centroid of tetra
		tetrahedron *tet = _data->get_list_of_tetrahedra()[tet_idx];
		SP_Point centroid = tet->compute_centroid_of();

#ifdef FORCE_DBG
		std::cout << std::endl << "Centroid is: " << centroid << std::endl;
#endif

		// get the effective penetration depth for move magnitude
		unsigned mat_id = tet->get_material_id() - 1;

		double mu_a = _data->get_list_of_materials()[mat_id]->get_mu_a();
		double mu_s = _data->get_list_of_materials()[mat_id]->get_mu_s();
		double g = _data->get_list_of_materials()[mat_id]->get_g();
		double mu_eff = std::sqrt(3*mu_a*(mu_a + mu_s*(1-g)));
		double eff_pen = 1/mu_eff;


		// find closest source to centroid
		int closest_src_idx = -1;
		double closest_src_dist = _INFINITY_;
		std::vector<double> dist_to_sources(_current_state.size());
		for (unsigned j = 0; j < _current_state.size(); j++)
		{
			double curr_dist = _current_state[j].get_distance_pt_line(centroid);
			dist_to_sources[j] = curr_dist;
#ifdef FORCE_DBG
			std::cout << "curr_dist = " << curr_dist << std::endl;
#endif
			if (curr_dist < closest_src_dist)
			{
				closest_src_idx = j;
				closest_src_dist = curr_dist;
			}
		}
		if (closest_src_idx == -1)
		{
			fprintf(stderr, "\033[1;%dmgen_force_move::could not find closest source.\nExiting...\033[0m\n", 31);
			std::exit(-1);
		}
#ifdef FORCE_DBG
		std::cout << "Closest source is of index: " << closest_src_idx << " with distance: " << closest_src_idx << std::endl;
		std::cout << "The source is: " << _next_state[closest_src_idx] << std::endl;
#endif

		bool multi_source_push = _curr_move_type == MoveType::FORCE_MULTISOURCE;
		for (unsigned j = 0; j < _next_state.size(); j++)
		{
			if (!multi_source_push) {
				j = closest_src_idx;
			}

			// get direction to move along
			SP_Point move_dir;

			// 1- get the current line and its length
			SP_Line curr_line = _next_state[j];//closest_src_idx];
			double length = get_source_length(curr_line);

			if (length < 1e-8)
			{
				move_dir = (curr_line.end1 - centroid)/dist_to_sources[j];//closest_src_dist;
			}

			else
			{
				// 2- get the vector of the current line and its direction
				SP_Point curr_vec = curr_line.end1 - curr_line.end0;

				SP_Point curr_dir = curr_vec/length;

				// The following is based on computations done in written document
				SP_Point pt_minus_end0 = centroid - curr_line.end0;
				double curr_dot = sp_dot(curr_dir, pt_minus_end0);

				SP_Point u = curr_line.end0 + (curr_dot*curr_dir);

				move_dir = u - centroid;
				double norm_dir = u.get_distance_from(centroid);
				move_dir = move_dir/norm_dir;
			}
#ifdef FORCE_DBG
			std::cout << (underdosed ? "Pulling " : "Pushing ")  <<
				"source in the direction: " << (underdosed ? -1*move_dir : move_dir) << std::endl;
#endif

			// modify the direction of curr_source

			if (multi_source_push)
			{
				// Make push and pull distance dependent on amount of dose difference and effective penetration depth
				if (overdosed) // push source
				{
					_curr_move_range = overdose*eff_pen;
					_next_state[j].end0 = curr_line.end0 + ((1.0/pow(dist_to_sources[j],2))*overdose*eff_pen)*move_dir;
					_next_state[j].end1 = curr_line.end1 + ((1.0/pow(dist_to_sources[j],2))*overdose*eff_pen)*move_dir;
				}
				else if (underdosed) // pull source
				{
					_curr_move_range = underdose*eff_pen;
					_next_state[j].end0 = curr_line.end0 - ((1.0/pow(dist_to_sources[j],2))*underdose*eff_pen)*move_dir;
					_next_state[j].end1 = curr_line.end1 - ((1.0/pow(dist_to_sources[j],2))*underdose*eff_pen)*move_dir;
				} // else do nothing`
			}
			else
			{
				//double rand_dist = (_max_tumor_radius/1.2)*((1.0*rand())/RAND_MAX);
				double rand_dist = (_curr_max_move)*((1.0*rand())/RAND_MAX);
				_curr_move_range = rand_dist;
				if (overdosed) // push source
				{
					_next_state[j].end0 = curr_line.end0 + (rand_dist*overdose)*move_dir;
					_next_state[j].end1 = curr_line.end1 + (rand_dist*overdose)*move_dir;
				}
				else if (underdosed) // pull source
				{
					_next_state[j].end0 = curr_line.end0 - (rand_dist*underdose)*move_dir;
					_next_state[j].end1 = curr_line.end1 - (rand_dist*underdose)*move_dir;
				} // else do nothing`
			}
#ifdef FORCE_DBG
			std::cout << "New source moved to: " << _next_state[closest_src_idx] << std::endl;
#endif
			if (!multi_source_push) {
				break;
			}
		}
	}

#ifdef SA_DBG
	std::cout << std::endl << "End of force based move." << std::endl << "Next state is:" << std::endl;
	for(unsigned i = 0; i < _next_state.size(); i++)
		std::cout << _next_state[i] << std::endl;
	std::cout << std::endl;
#endif
}


//! Helper method: generates a new move by removing the least powered source
void sa_engine::generate_removal_move()
{
	// find the source with the least power
	//vector<double> _current_powers;
	double min_power = _current_powers[0];
	unsigned min_id = 0;
	for (unsigned i = 1; i < _current_powers.size(); i++) {
		if (_current_powers[i] < min_power) {
			min_power = _current_powers[i];
			min_id = i;
		}
	}

	// remove source
	// directly remove from vectors, init_length remains unchanged since sorted.
	cout << "sa_engine::generate_removal_move removed source " << min_id << endl;
	_next_state.erase(_next_state.begin() + min_id);
	//_source_lengths.erase(_source_lengths.begin() + min_id);
}

//! This method returns the next move type based on probabilites
sa_engine::MoveType sa_engine::get_next_move_type()
{
	// generate a uniform randome number between 0 and 1
	double rand_num = (1.0*rand())/RAND_MAX;

	if (rand_num <= _move_type_probability[MoveType::RANDOM]) {
		return MoveType::RANDOM;
	} else if (rand_num <= _move_type_probability[MoveType::RANDOM] +
			 			   _move_type_probability[MoveType::FORCE_MULTISOURCE]) {
		return MoveType::FORCE_MULTISOURCE;
	} else if (rand_num <= _move_type_probability[MoveType::RANDOM] +
						   _move_type_probability[MoveType::FORCE_MULTISOURCE] +
						   _move_type_probability[MoveType::FORCE_CLOSEST]) {
		return MoveType::FORCE_CLOSEST;
	} else if (rand_num <= _move_type_probability[MoveType::RANDOM] +
						   _move_type_probability[MoveType::FORCE_MULTISOURCE] +
						   _move_type_probability[MoveType::FORCE_CLOSEST] +
						   _move_type_probability[MoveType::DISPLACE]) {
		return MoveType::DISPLACE;
	}
	else if (rand_num <= _move_type_probability[MoveType::RANDOM] +
						 _move_type_probability[MoveType::FORCE_MULTISOURCE] +
						 _move_type_probability[MoveType::FORCE_CLOSEST] +
						 _move_type_probability[MoveType::DISPLACE] +
						 _move_type_probability[MoveType::RESET_BEST]) {
		return MoveType::RESET_BEST;
	}
	else if (rand_num <= _move_type_probability[MoveType::RANDOM] +
						 _move_type_probability[MoveType::FORCE_MULTISOURCE] +
						 _move_type_probability[MoveType::FORCE_CLOSEST] +
						 _move_type_probability[MoveType::DISPLACE] +
						 _move_type_probability[MoveType::RESET_BEST] +
						 _move_type_probability[MoveType::LENGTH]) {
		return MoveType::LENGTH;
	}
	else if (rand_num <= _move_type_probability[MoveType::RANDOM] +
						 _move_type_probability[MoveType::FORCE_MULTISOURCE] +
						 _move_type_probability[MoveType::FORCE_CLOSEST] +
						 _move_type_probability[MoveType::DISPLACE] +
						 _move_type_probability[MoveType::RESET_BEST] +
						 _move_type_probability[MoveType::LENGTH] +
						 _move_type_probability[MoveType::REMOVE_SOURCE]) {
		return MoveType::REMOVE_SOURCE;
	}
	else {
		return MoveType::RELOCATE;
	}
}

//! This method updates the move types probabilites after each temperature update
void sa_engine::update_move_types_probabilities()
{
	// Compute the total effectiveness
	double sum_effectiveness = 0.0;
	bool terminate = true; // terminate if all move types' effectivenesses are 0
	for (auto it = _move_type_effectiveness.begin(); it != _move_type_effectiveness.end(); it++) {
		sum_effectiveness += it->second;

		terminate = terminate & (it->second < DOUBLE_ACCURACY);
	}

	_terminate_based_on_effectiveness = terminate;

#ifdef SA_LOG
	cout << "Current RANDOM effectiveness: " << _move_type_effectiveness[MoveType::RANDOM] << endl;
	cout << "Current FORCE_MULTISOURCE effectiveness: " << _move_type_effectiveness[MoveType::FORCE_MULTISOURCE] << endl;
	cout << "Current FORCE_CLOSEST effectiveness: " << _move_type_effectiveness[MoveType::FORCE_CLOSEST] << endl;
	cout << "Current DISPLACE effectiveness: " << _move_type_effectiveness[MoveType::DISPLACE] << endl;
	cout << "Current RESET_BEST effectiveness: " << _move_type_effectiveness[MoveType::RESET_BEST] << endl;
	cout << "Current LENGTH effectiveness: " << _move_type_effectiveness[MoveType::LENGTH] << endl;
	cout << "Current REMOVE_SOURCE effectiveness: " << _move_type_effectiveness[MoveType::REMOVE_SOURCE] << endl;
	cout << "Current RELOCATE effectiveness: " << _move_type_effectiveness[MoveType::RELOCATE] << endl;
#endif

	auto new_probs = _move_type_probability;
	double sum_probs = 0.0;
	if (sum_effectiveness > DOUBLE_ACCURACY) // if sum is zero use same probabilities as last iteration
	{
		// update probabilites based one lambda*p_old + (1-lambda)*p_new
		for (auto it = _move_type_probability.begin(); it != _move_type_probability.end(); it++) {
			//double old_prob = it->second;
			double new_prob = _move_type_effectiveness[it->first]/sum_effectiveness;

			if (new_prob < DOUBLE_ACCURACY && (it->first == MoveType::RESET_BEST ||
							it->first == MoveType::LENGTH))
			{
				new_prob = 0.0;
			}
			else if (new_prob < DOUBLE_ACCURACY) // give it a chance of 10% of surviving)
			{
				new_prob = 0.1;
			}

			// for source removal or relocation, do once at a time
			if (it->first == MoveType::REMOVE_SOURCE || it->first == MoveType::RELOCATE) new_prob = 0.0;

			//it->second = _lambda_smooth*old_prob + (1-_lambda_smooth)*new_prob;
			sum_probs += new_prob;   // if threshold of 10% is set sum will not be 1. Need to fix all probabilities
			new_probs[it->first] = new_prob;

			// re-initialize effectiveness
			_move_type_effectiveness[it->first] = 0.0;
		}
		for (auto it = _move_type_probability.begin(); it != _move_type_probability.end(); it++) {
			double old_prob = it->second;
			double new_prob = new_probs[it->first]/sum_probs;

			it->second = _lambda_smooth*old_prob + (1-_lambda_smooth)*new_prob;
		}
	}
	else {
		long unsigned num_zero_source = (long unsigned)_max_num_sources - _current_state.size(); 
		for (unsigned i = 0; i < _current_powers.size(); i++) {
			if (_current_powers[i] <= DOUBLE_ACCURACY) num_zero_source++;
		}

		// re-initialize the probabilities to 20% for all
			_move_type_probability[MoveType::RANDOM] = 0.2;
			_move_type_probability[MoveType::FORCE_CLOSEST] = 0.2;
			_move_type_probability[MoveType::FORCE_MULTISOURCE] = 0.2;
			_move_type_probability[MoveType::DISPLACE] = 0.2;
			_move_type_probability[MoveType::LENGTH] = 0.0;
			_move_type_probability[MoveType::RESET_BEST] = 0.0;

		if (num_zero_source >= _max_num_sources - 1) {
			_move_type_probability[MoveType::REMOVE_SOURCE] = 0.0;
			_move_type_probability[MoveType::RELOCATE] = 0.2;
		} else if (num_zero_source > 0) {
			_move_type_probability[MoveType::REMOVE_SOURCE] = 0.1;
			_move_type_probability[MoveType::RELOCATE] = 0.1;
		} else {
			_move_type_probability[MoveType::REMOVE_SOURCE] = 0.2;
			_move_type_probability[MoveType::RELOCATE] = 0.0;
		}

		_move_type_effectiveness[MoveType::RANDOM] = 0.0;
		_move_type_effectiveness[MoveType::FORCE_CLOSEST] = 0.0;
		_move_type_effectiveness[MoveType::FORCE_MULTISOURCE] = 0.0;
		_move_type_effectiveness[MoveType::DISPLACE] = 0.0;
		_move_type_effectiveness[MoveType::RESET_BEST] = 0.0;
		_move_type_effectiveness[MoveType::LENGTH] = 0.0;
		_move_type_effectiveness[MoveType::REMOVE_SOURCE] = 0.0;
		_move_type_effectiveness[MoveType::RELOCATE] = 0.0;

	}

#ifdef SA_LOG
	cout << "Current RANDOM probability: " << _move_type_probability[MoveType::RANDOM] << endl;
	cout << "Current FORCE_MULTISOURCE probability: " << _move_type_probability[MoveType::FORCE_MULTISOURCE] << endl;
	cout << "Current FORCE_CLOSEST probability: " << _move_type_probability[MoveType::FORCE_CLOSEST] << endl;
	cout << "Current DISPLACE probability: " << _move_type_probability[MoveType::DISPLACE] << endl;
	cout << "Current RESET_BEST probability: " << _move_type_probability[MoveType::RESET_BEST] << endl;
	cout << "Current LENGTH probability: " << _move_type_probability[MoveType::LENGTH] << endl;
	cout << "Current REMOVE_SOURCE probability: " << _move_type_probability[MoveType::REMOVE_SOURCE] << endl;
	cout << "Current RELOCATE probability: " << _move_type_probability[MoveType::RELOCATE] << endl;
#endif
}

//! Helper method: returns the average distance between sources.
double sa_engine::get_average_distance_sources(const std::vector<SP_Line>& sources) const
{
	double avg_dist = 0.0;
	unsigned num_dists = 0;
	for (unsigned i = 0; i < sources.size(); i++) {
		for (unsigned j = i + 1; j < sources.size(); j++) {
			num_dists++;
			avg_dist += std::min(sources[i].get_distance_pt_line(sources[j].end0),
								 sources[i].get_distance_pt_line(sources[j].end1));
		}
	}

	return avg_dist/num_dists;
}

//! Helper method: generate a move by randomly changing the length of the source
void sa_engine::generate_rand_length_move(unsigned change_idx) {

    // generate a random integer length (with max equal to init length)
    double rand_length = 10*ceil((0.1*rand()/RAND_MAX)*_max_tumor_radius);

	double curr_length = get_source_length(_next_state[change_idx]);
	SP_Line curr_line = _next_state[change_idx];

	SP_Point curr_vec = curr_line.end1 - curr_line.end0;

	SP_Point curr_dir;
	if (curr_length > DOUBLE_ACCURACY) {
		curr_dir = curr_vec/curr_length;
	}
	else {
		double phi = ((1.0*rand())/RAND_MAX)*2*M_PI;
		double theta = ((1.0*rand())/RAND_MAX)*M_PI;

		curr_dir.set_xcoord(static_cast<float>(sin(phi)*cos(theta)));
		curr_dir.set_ycoord(static_cast<float>(sin(phi)*sin(theta)));
		curr_dir.set_zcoord(static_cast<float>(cos(phi)));

	}
	curr_line.end1 = curr_line.end0 + (rand_length*curr_dir);


	_next_state[change_idx] = curr_line;
			
#ifdef SINGLE_FULLMONTE
	_single_change = true;
#endif
	_change_idx = change_idx;
}

//! Helper method: returns if a source is within the current injection point constraint
bool sa_engine::is_valid_injection(const SP_Line & src) {
#ifdef SA_DBG
	std::cout << "sa_engine::inside is_valid_injection" << std::endl;
#endif
	// point sources always can pass through injection
	if (src.end0.get_distance_from(src.end1) <= DOUBLE_ACCURACY) return true;

	// Find intersection between src extended line and plane defined by injection point
	SP_Point intersection;
	if (!src.get_intersection_with_plane(_current_inj_normal, _current_inj_center, intersection)) {
		return false;
	}

	// Find distance between intersection and injection center
	double distance = intersection.get_distance_from(_current_inj_center);

	// Check if distance exceeds radius
	return (distance <= _current_inj_diameter/2);
}

//! Helper method: returns the maximum possible distance to displace given the injection point constraint
void sa_engine::get_rand_dist(const SP_Line & src, const SP_Point & dir, double & rand_dist) const {
#ifdef SA_DBG
	std::cout << "sa_engine::inside get_rand_dist" << std::endl;
#endif
	// 1. Find intersection between src extended line and plane defined by injection point
	SP_Point intersection;
	if (!src.get_intersection_with_plane(_current_inj_normal, _current_inj_center, intersection)) {
		return;
	}
	if (intersection.get_distance_from(_current_inj_center) > _current_inj_diameter/2) {
		return;
	}

	// 2. Find projection of dir onto the plane
	// projection = unit vector of projected direction
	SP_Point projection = sp_cross(dir, _current_inj_normal);
	projection = sp_normalize(sp_cross(_current_inj_normal, projection));

	// 3. Find intersection between line and circle
	// line = intersection + l * projection
	// Find distance between circle center and line
	SP_Point cross_product = sp_cross(projection, _current_inj_center - intersection);
	double proj_dist = sp_norm(cross_product);
	double dist_intersection_center = sp_norm(_current_inj_center - intersection);
	double dot_dir = sp_dot(_current_inj_center - intersection, projection);

	double l = ((dot_dir < 0) ? -1 : 1) * sqrt(dist_intersection_center * dist_intersection_center - proj_dist * proj_dist) +
				sqrt(_current_inj_diameter*_current_inj_diameter/4 - proj_dist * proj_dist);

	// rand_dist = cos(theta) * l = dot(projection, dir) * l
	rand_dist = min(rand_dist, sp_dot(projection, dir) * l);
#ifdef SA_DBG
	std::cout << "rand_dist = " << rand_dist << std::endl;
#endif
}

//! Helper method: randomly rotates the source around end0
void sa_engine::rand_rotate_source(SP_Line & src, double length) {
#ifdef SA_DBG
	std::cout << "sa_engine::inside rand_rotate_source" << std::endl;
#endif
	if (_constrain_injection_point) {
		// randomly choose a valid injection point
		SP_Line n = SP_Line(_current_inj_center, _current_inj_center + _current_inj_normal);
		SP_Point dir = sp_normalize(n.get_rand_perpendicular_vector());
		double r = _current_inj_diameter/2*rand()/RAND_MAX;
		SP_Point inj_point = _current_inj_center + r * dir;

		// extend to find end1
		dir = sp_normalize(src.end0 - inj_point);
		src.end1 = src.end0 + length * dir;
#ifdef SA_DBG
		std::cout << "src = " << src << ", inj_center = " << _current_inj_center << std::endl;
#endif
	} else {
		// To get another point that is length away, get two random angles and calculate coordinates
		double phi = ((1.0*rand())/RAND_MAX)*2*M_PI;
		double theta = ((1.0*rand())/RAND_MAX)*M_PI;

		SP_Point end1;
		end1.set_xcoord(static_cast<float>(src.end0.get_xcoord() + length*sin(phi)*cos(theta)));
		end1.set_ycoord(static_cast<float>(src.end0.get_ycoord() + length*sin(phi)*sin(theta)));
		end1.set_zcoord(static_cast<float>(src.end0.get_zcoord() + length*cos(phi)));

		src.end1 = end1;
	}
	src.update_length_and_dir();
}

//! Helper method: set the current injection point constraint
void sa_engine::set_injection_point_constraint(unsigned ind) {
	Parser_injection_point temp = _injection_points.injection_points[ind];
	_current_inj_diameter = temp.diameter;
	_current_inj_normal = temp.normal;
	sp_normalize(_current_inj_normal);
	_current_inj_center = temp.center;

	cout << "Running with injection constraint at " << _current_inj_center << " normal " << _current_inj_normal << " with diameter " << _current_inj_diameter << "mm.\n";
}


//! Helper method: force injection constraint to true and get injection constraints
void sa_engine::enable_injection_constraint() {
	_constrain_injection_point = true;
	_injection_points = _fparser->get_injection_points();
	set_injection_point_constraint(0);
}

//! Helper method: reads SA parameters from _fparser
void sa_engine::read_parameters() {
	unordered_map<string, string> sa_params_map = _fparser->get_sa_params();

	if (sa_params_map.find("beta") != sa_params_map.end()) {
        _beta = atof(sa_params_map["beta"].c_str()); 
    }
    else {
        _beta = 0.02; // default value
    }

	if (sa_params_map.find("lambda") != sa_params_map.end()) {
        _lambda_smooth = atof(sa_params_map["lambda"].c_str()); 
    }
    else {
        _lambda_smooth = 0.3; // default value
    }

	if (sa_params_map.find("mpt") != sa_params_map.end()) {
        _moves_per_temp = atof(sa_params_map["mpt"].c_str()); 
    }
    else {
        _moves_per_temp = 0.0; // default value
    }

	if (sa_params_map.find("sa_placement_file") != sa_params_map.end()) {
		_sa_placement_file = (_file_path_override ? string(PDT_SPACE_SRC_DIR) : "") + sa_params_map["sa_placement_file"];
	} else {
		_sa_placement_file = ""; // Default doesn't print sources
	}

	if (sa_params_map.find("sa_num_packets") != sa_params_map.end()) {
		_num_packets = atof(sa_params_map["sa_num_packets"].c_str());
	} else {
		_num_packets = _parent_plan->get_num_packets() / 10; // Default power allocation value / 10
	}

	if (sa_params_map.find("max_src_distance") != sa_params_map.end()) {
        _max_src_spacing = atof(sa_params_map["max_src_distance"].c_str()); 
    }
    else {
        _max_src_spacing = 15.0; // default value
    }
	if (sa_params_map.find("min_src_distance") != sa_params_map.end()) {
        _min_src_spacing = atof(sa_params_map["min_src_distance"].c_str()); 
    }
    else {
        _min_src_spacing = 10.0; // default value
    }

	if (sa_params_map.find("distance_to_boundary") != sa_params_map.end()) {
        _dist_to_boundary = atof(sa_params_map["distance_to_boundary"].c_str()); 
    }
    else {
        _dist_to_boundary = 8.0; // default value
    }

	if (sa_params_map.find("output_top_placements") != sa_params_map.end()) {
		_final_sa_placement_file = sa_params_map["output_top_placements"];
	} else {
		_final_sa_placement_file = "";
	}
}
