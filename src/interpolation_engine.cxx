/** 
 *
 *
 * @author: William Kingsford (namespace created by Abed Yassine)
 * @data: October 18th, 2019
 *
 * @file interpolation_engine.cxx
 * @brief: contains implementations for the interpolation_engine methods
 */


#include "interpolation_engine.h"
#include "profiler.h"

#include <cmath>

namespace SP_IE 
{
RaisedCosLUT *RaisedCosLUT::_s_instance = 0;

double sinc(double x) {
    if (x == 0) {
        return 1;
    }
    else {
        return sin(M_PI * x) / (M_PI * x);
    }
}

double raised_cosine(double x, double beta) {
    if (abs(x) == 1.0 / (2.0 * beta)) {
        return (M_PI / (4.0)) * sinc(1/(2.0*beta));
    }
    else {
        return sinc(x) * cos(M_PI*beta*x) / (1.0 - 4.0*beta*beta*x*x);
    }
}

double integrate_midpoint(std::function<double(double)> func, double l, double r, unsigned num_pts)
{
    /*
     * This integration assumes that the delta_x is fixed. i.e, all points along the line are equidistant  
     *
     */

    double integration = 0.0;
    
    double temp_r = r; 
    r = std::max(r, l); 
    l = std::min(temp_r, l);

    if (num_pts == 0)
    {
        fprintf(stderr, "\033[1;%dmintegrate_midpoint::num_points is zero ==> cannot integrate.\033[0;m\n", 31); 
        std::exit(-1); 
    }

    double delta_x = (r - l)/num_pts; 
    
    double x = l + 0.5*delta_x; 
    for (unsigned i = 0; i < num_pts; i++)
    {
        integration += func(x); 
        x += delta_x;
    }

    return integration*delta_x;
}

Eigen::RowVectorXd integrate_midpoint(std::function<Eigen::RowVectorXd(Eigen::RowVectorXd)> func,
        Eigen::RowVectorXd l, Eigen::RowVectorXd r, unsigned num_pts)
{
    Eigen::RowVectorXd vec = r - l;
    double delta_x = vec.norm()/num_pts; 
    
    std::cout << "l: " << l << std::endl;
    vec.normalize();
    
    Eigen::RowVectorXd x = l + 0.5*delta_x*vec; 
    
    Eigen::RowVectorXd integration = func(x);
    
    for (unsigned i = 1; i < num_pts; i++)
    {
        std::cout << "x: " << x << std::endl;
        x += delta_x*vec;
        integration += func(x); 
    }

    return integration*delta_x;
}

double evaluate_interpolation(double x, double x_min, double x_max, double delta_x, 
                              vector<double> &f, int N, bool use_raised_cosine, double beta) {
    // determine nearest sample point index to the left of x
    int idx = (int) floor((x - x_min)/delta_x);
    // convolution will include N sample points on each side of x
    int begin = idx - N + 1;
    int end = idx + N;

    // ensure sum is within the range of the sample function
    if (begin < 0) {
        std::cout << "WARNING: Sum in evaluate_interpolation is being pruned " <<
         " due to going outside of range (x = " << x << 
         ", x_min = " << x_min << ", x_max = " << x_max << ")" << std::endl;
        begin = 0;
    }
    if (end >= static_cast<int>(f.size())) {
        std::cout << "WARNING: Sum in evaluate_interpolation is being " <<
            " pruned due to going outside of range (x = " << x << 
            ", x_min = " << x_min << ", x_max = " << x_max << ")" << std::endl;
        end = static_cast<int>(f.size()) - 1;
    }
    
    double ret_val = 0;
    double a = (x - x_min)/delta_x; // pre-computing for efficiency
    if (!use_raised_cosine) { // use sinc
        for (int i=begin; i<=end; i++) {
            ret_val += f[i] * sinc(a - (double)i);
        }
    }
    else { // use raised cosine
        for (int i=begin; i<=end; i++) {
            ret_val += f[i] * raised_cosine(a - (double)i, beta);
        }
    }

    return ret_val;
}

double evaluate_interpolation(double x[3], double x_min[3], double x_max[3], double delta_x, 
                              vector<vector<vector<double>>> &f, int N, 
                              bool use_raised_cosine, double beta) {
    // determine nearest sample point index to the left of x
    int idx[3], begin[3], end[3];

    // number of entries in f array across each dimension
    int length[3];
    length[0] = static_cast<int>(f.size());
    length[1] = static_cast<int>(f[0].size());
    length[2] = static_cast<int>(f[0][0].size());
    for (int i=0; i<3; i++) {
        idx[i] = (int) floor((x[i] - x_min[i])/delta_x);

        // convolution will include N sample points on each side of x
        begin[i] = idx[i] - N + 1;
        end[i] = idx[i] + N;

        // ensure sum is within the range of the sample function
        if (begin[i] < 0) {
            std::cout << "WARNING: Sum in evaluate_interpolation is being " << 
                "pruned due to going outside of range (x[" << i << "] = " << x[i] << 
                ", x_min[" << i << "] = " << x_min[i] << ", x_max[" << i << "] = " << x_max[i] << ")" << std::endl;
            begin[i] = 0;
        }
        if (end[i] >= static_cast<int>(f.size())) {
            std::cout << "WARNING: Sum in evaluate_interpolation is being " <<
                "pruned due to going outside of range (x[" << i << "] = " << x[i] << 
                ", x_min[" << i << "] = " << x_min[i] << ", x_max[" << i << "] = " << x_max[i] << ")" << std::endl;
            end[i] = length[i] - 1;
        }
    }
    
    double ret_val = 0;
    // pre-computing for efficiency
    double a = (x[0] - x_min[0])/delta_x; 
    double b = (x[1] - x_min[1])/delta_x; 
    double c = (x[2] - x_min[2])/delta_x; 

    RaisedCosLUT *lut = RaisedCosLUT::instance();
    // check if initialized already; if not, initialize with default settings
    if (!lut->isInitialized()) {
        lut->initialize((double)(N+1), 100.0, beta); // range of [-(N+1),N+1], resolution of 100
    }

    if (!use_raised_cosine) { // use sinc
        for (int i=begin[0]; i<=end[0]; i++) {
            for (int j=begin[1]; j<=end[1]; j++) {
                for (int k=begin[2]; k<=end[2]; k++) {
                    ret_val += f[i][j][k] * sinc(a - (double)i) * 
                                            sinc(b - (double)j) *
                                            sinc(c - (double)k);
                }
            }
        }
    }
    else { // use raised cosine
        for (int i=begin[0]; i<=end[0]; i++) {
            for (int j=begin[1]; j<=end[1]; j++) {
                for (int k=begin[2]; k<=end[2]; k++) {
                    // we know that abs(a-(double)i) < N+1, so we don't need to worry about
                    // going out of the array's bounds
                    ret_val += f[i][j][k] * lut->get(a-(double)i) *
                                            lut->get(b-(double)j) *
                                            lut->get(c-(double)k);
                    //ret_val += f[i][j][k] * raised_cosine(a-(double)i, beta) *
                    //                        raised_cosine(b-(double)j, beta) *
                    //                        raised_cosine(c-(double)k, beta);
                }
            }
        }
    }

    return ret_val;
}

// overloaded version for vector-valued functions of 2 variables
Eigen::RowVectorXd evaluate_interpolation(double x[2], double x_min[2],
                              double delta_x, 
                              source_cache &grid_sources, int N, 
                              bool use_raised_cosine, double beta) {
    // determine nearest sample point index to the left of x
    int idx[2], begin[2], end[2];

    
    // number of entries in f array across each dimension
    //int length[2];
    //length[0] = f.size();
    //length[1] = f[0].size();
    for (int i=0; i<2; i++) {
        idx[i] = (int) floor((x[i] - x_min[i])/delta_x);

        // convolution will include N sample points on each side of x
        begin[i] = idx[i] - N + 1;
        end[i] = idx[i] + N;

        // TODO: no longer need to check bounds now that we're using source_cache
        // ensure sum is within the range of the sample function
        /*if (begin[i] < 0) {
            std::cout << "WARNING: Sum in evaluate_interpolation is being "
                 "pruned due to going outside of range (x[" << i << "] = " << 
                 x[i] << ", x_min[" << i << "] = " << x_min[i] << ", x_max[" << i << "] = " << x_max[i] << ")" << std::endl;
            begin[i] = 0;
        }
        if (end[i] >= f.size()) {
            std::cout << "WARNING: Sum in evaluate_interpolation is being "
                "pruned due to going outside of range (x[" << i << "] = " << 
                x[i] << ", x_min[" << i << "] = " << x_min[i] << ", x_max[" << i << "] = " << x_max[i] << ")" << std::endl;
            end[i] = length[i] - 1;
        }*/
    }
    
    // initialize return value
    Eigen::RowVectorXd ret_val = Eigen::RowVectorXd::Zero(grid_sources.get_num_tetras());
    // pre-computing for efficiency
    double a = (x[0] - x_min[0])/delta_x; 
    double b = (x[1] - x_min[1])/delta_x; 
    double weight;

    RaisedCosLUT *lut = RaisedCosLUT::instance();
    // check if initialized already; if not, initialize with default settings
    if (!lut->isInitialized()) {
        //TODO: currently making larger than we'll ever need, should adjust this so it can
        // check if the size is too small (for when N changes)
        //lut->initialize((double)(N+1), 100.0, beta); // range of [-(N+1),N+1], resolution of 100
        lut->initialize((double)(11), 100.0, beta); // range of [-(11),11], resolution of 100
    }

    // TODO: using only linear interpolation for now
    
    // parameters for linear interpolation
    double t_x, t_y; 
    int n_x, n_y;
    n_x = static_cast<int>(floor(a));
    n_y = static_cast<int>(floor(b));
    t_x = a - (double)n_x;
    t_y = b - (double)n_y;

    //cout << "Evaluating linear interpolation: t_x = " << t_x << ", t_y = " << t_y << endl;
    // TODO: no longer need to cast n_x, n_y to int

    if (!use_raised_cosine) { // use linear interpolation
        ret_val = (1.0 - t_y) * (
                       (1.0 - t_x) * grid_sources.get_source((int)n_x, (int)n_y) +
                       t_x * grid_sources.get_source((int)n_x + 1, (int)n_y) ) +
                  t_y * (
                       (1.0 - t_x) * grid_sources.get_source((int)n_x, (int)n_y+1) +
                       t_x * grid_sources.get_source((int)n_x + 1, (int)n_y+1) );
        // TODO: old code for sinc
        /*
        for (int i=begin[0]; i<=end[0]; i++) {
            for (int j=begin[1]; j<=end[1]; j++) {
                for (int k=begin[2]; k<=end[2]; k++) {
                    weight = sinc(a - (double)i) * sinc(b - (double)j) * sinc(c - (double)k);
                    ret_val += weight * f[i][j][k];
                }
            }
        }*/
    }
    else { // use raised cosine
        for (int i=begin[0]; i<=end[0]; i++) {
            for (int j=begin[1]; j<=end[1]; j++) {
                // we know that abs(a-(double)i) < N+1, so we don't need to worry about
                // going out of the array's bounds
                weight = lut->get(a-(double)i) *
                         lut->get(b-(double)j);
                //ret_val += weight * f[i][j][k];
                ret_val += weight * grid_sources.get_source(i, j);
            }
        }
    }

    return ret_val;
}

// TODO: replace double[2] with std::vector
vector<Eigen::RowVectorXd> evaluate_interpolation_gradient(double x[2], double x_min[2],
                              double delta_x, 
                              source_cache &grid_sources, int N, 
                              bool /*use_raised_cosine*/, double beta) {
    // TODO: don't need the below now that we're using source_cache
    /*
    // determine nearest sample point index to the left of x
    int idx[2], begin[2], end[2];

    // number of entries in f array across each dimension
    int length[2];
    length[0] = f.size();
    length[1] = f[0].size();
    for (int i=0; i<2; i++) {
        idx[i] = (int) floor((x[i] - x_min[i])/delta_x);

        // convolution will include N sample points on each side of x
        begin[i] = idx[i] - N + 1;
        end[i] = idx[i] + N;

        // ensure sum is within the range of the sample function
        if (begin[i] < 0) {
            std::cout << "WARNING: Sum in evaluate_interpolation is being " << 
                "pruned due to going outside of range (x[" << i << "] = " << 
                x[i] << ", x_min[" << i << "] = " << x_min[i] << ", x_max[" << i << "] = " << x_max[i] << ")" << std::endl;
            begin[i] = 0;
        }
        if (end[i] >= f.size()) {
            std::cout << "WARNING: Sum in evaluate_interpolation is being " << 
                "pruned due to going outside of range (x[" << i << "] = " << 
                x[i] << ", x_min[" << i << "] = " << x_min[i] << ", x_max[" << i << "] = " << x_max[i] << ")" << std::endl;
            end[i] = length[i] - 1;
        }
    }*/
    
    // initialize return value
    // TODO: replace 2 by the number of components in f
    vector<Eigen::RowVectorXd> ret_val(2, Eigen::RowVectorXd::Zero(grid_sources.get_num_tetras()));
    // pre-computing for efficiency
    double a = (x[0] - x_min[0])/delta_x; 
    double b = (x[1] - x_min[1])/delta_x; 

    RaisedCosLUT *lut = RaisedCosLUT::instance();
    // check if initialized already; if not, initialize with default settings
    if (!lut->isInitialized()) {
        lut->initialize((double)(N+1), 100.0, beta); // range of [-(N+1),N+1], resolution of 100
    }

    // TODO: using only linear interpolation for now
    
    // parameters for linear interpolation
    double t_x, t_y; 
    int n_x, n_y;
    n_x = static_cast<int>(floor(a));
    n_y = static_cast<int>(floor(b));
    t_x = a - (double)n_x;
    t_y = b - (double)n_y;

    //cout << "Evaluating linear interpolation: t_x = " << t_x << ", t_y = " << t_y << endl;
    // TODO: use intermediate variables to make this less ugly (e.g. variables so we don't call
    // grid_sources.get_source so often)
    ret_val[0] = (1.0 / delta_x) * ( (1.0 - t_y) * (
                grid_sources.get_source(n_x + 1, n_y) - grid_sources.get_source(n_x, n_y) ) +
                 t_y * (
                grid_sources.get_source(n_x + 1, n_y+1) - grid_sources.get_source(n_x, n_y+1) )
                           );
    ret_val[1] = (1.0 / delta_x) * ( 
                  ( (1.0 - t_x) * grid_sources.get_source(n_x, n_y+1) +
                   t_x * grid_sources.get_source(n_x + 1, n_y+1) )
                  -
                  ( (1.0 - t_x) * grid_sources.get_source(n_x, n_y) +
                   t_x * grid_sources.get_source(n_x + 1, n_y) )
                     );
    /*ret_val[0] = (1.0 / delta_x) * ( (1.0 - t_y) * (
                               f[n_x + 1][n_y] - f[n_x][n_y] ) +
                             t_y * (
                               f[n_x + 1][n_y+1] - f[n_x][n_y+1] )
                           );
    ret_val[1] = (1.0 / delta_x) * ( 
                  ( (1.0 - t_x) * f[n_x][n_y+1] +
                   t_x * f[n_x + 1][n_y+1] )
                  -
                  ( (1.0 - t_x) * f[n_x][n_y] +
                   t_x * f[n_x + 1][n_y] )
                     );*/

    // TODO: uncomment, add an option for linear interpolation
    /*
    double weight;
    if (!use_raised_cosine) { // use sinc
        for (int i=begin[0]; i<=end[0]; i++) {
            for (int j=begin[1]; j<=end[1]; j++) {
                for (int k=begin[2]; k<=end[2]; k++) {
                    weight = sinc(a - (double)i) * sinc(b - (double)j) * sinc(c - (double)k);
                    ret_val += weight * f[i][j][k];
                }
            }
        }
    }
    else { // use raised cosine
        for (int i=begin[0]; i<=end[0]; i++) {
            for (int j=begin[1]; j<=end[1]; j++) {
                for (int k=begin[2]; k<=end[2]; k++) {
                    // we know that abs(a-(double)i) < N+1, so we don't need to worry about
                    // going out of the array's bounds
                    weight = lut->get(a-(double)i) *
                             lut->get(b-(double)j) *
                             lut->get(c-(double)k);
                    ret_val += weight * f[i][j][k];
                }
            }
        }
    }*/

    return ret_val;
}

// overloaded version for vector-valued functions of 3 variables
Eigen::RowVectorXd evaluate_interpolation(double x[3], double x_min[3], double x_max[3],
                              double delta_x, 
                              vector<vector<vector<Eigen::RowVectorXd>>> &f, int N, 
                              bool /*use_raised_cosine*/, double beta) {
    // determine nearest sample point index to the left of x
    int idx[3], begin[3], end[3];

    // number of entries in f array across each dimension
    int length[3];
    length[0] = static_cast<int>(f.size());
    length[1] = static_cast<int>(f[0].size());
    length[2] = static_cast<int>(f[0][0].size());
    for (int i=0; i<3; i++) {
        idx[i] = (int) floor((x[i] - x_min[i])/delta_x);

        // convolution will include N sample points on each side of x
        begin[i] = idx[i] - N + 1;
        end[i] = idx[i] + N;

        // ensure sum is within the range of the sample function
        if (begin[i] < 0) {
            std::cout << "WARNING: Sum in evaluate_interpolation is being " << 
                "pruned due to going outside of range (x[" << i << "] = " << 
                x[i] << ", x_min[" << i << "] = " << x_min[i] << ", x_max[" << i << "] = " << x_max[i] << ")" << std::endl;
            begin[i] = 0;
        }
        if (end[i] >= static_cast<int>(f.size())) {
            std::cout << "WARNING: Sum in evaluate_interpolation is being " << 
                "pruned due to going outside of range (x[" << i << "] = " << 
                x[i] << ", x_min[" << i << "] = " << x_min[i] << ", x_max[" << i << "] = " << x_max[i] << ")" << std::endl;
            end[i] = length[i] - 1;
        }
    }
    
    // initialize return value
    Eigen::RowVectorXd ret_val = Eigen::RowVectorXd::Zero(f[0][0][0].size());
    // pre-computing for efficiency
    double a = (x[0] - x_min[0])/delta_x; 
    double b = (x[1] - x_min[1])/delta_x; 
    double c = (x[2] - x_min[2])/delta_x; 

    RaisedCosLUT *lut = RaisedCosLUT::instance();
    // check if initialized already; if not, initialize with default settings
    if (!lut->isInitialized()) {
        lut->initialize((double)(N+1), 100.0, beta); // range of [-(N+1),N+1], resolution of 100
    }

    // TODO: using only linear interpolation for now
    
    // parameters for linear interpolation
    double t_x, t_y, t_z; 
    double n_x, n_y, n_z;
    n_x = floor(a);
    n_y = floor(b);
    n_z = floor(c);
    t_x = a - n_x;
    t_y = b - n_y;
    t_z = c - n_z;

    cout << "Evaluating linear interpolation..." << endl;
    ret_val = (1.0 - t_z) * (
                  (1.0 - t_y) * (
                       (1.0 - t_x) * f[(int)n_x][(int)n_y][(int)n_z] +
                       t_x * f[(int)n_x + 1][(int)n_y][(int)n_z] ) +
                  t_y * (
                       (1.0 - t_x) * f[(int)n_x][(int)n_y+1][(int)n_z] +
                       t_x * f[(int)n_x + 1][(int)n_y+1][(int)n_z] ) ) +
              t_z * (
                  (1.0 - t_y) * (
                       (1.0 - t_x) * f[(int)n_x][(int)n_y][(int)n_z+1] +
                       t_x * f[(int)n_x + 1][(int)n_y][(int)n_z+1] ) +
                  t_y * (
                       (1.0 - t_x) * f[(int)n_x][(int)n_y+1][(int)n_z+1] +
                       t_x * f[(int)n_x + 1][(int)n_y+1][(int)n_z+1] ) );

    // TODO: uncomment, add an option for linear interpolation
    /*
    double weight;
    if (!use_raised_cosine) { // use sinc
        for (int i=begin[0]; i<=end[0]; i++) {
            for (int j=begin[1]; j<=end[1]; j++) {
                for (int k=begin[2]; k<=end[2]; k++) {
                    weight = sinc(a - (double)i) * sinc(b - (double)j) * sinc(c - (double)k);
                    ret_val += weight * f[i][j][k];
                }
            }
        }
    }
    else { // use raised cosine
        for (int i=begin[0]; i<=end[0]; i++) {
            for (int j=begin[1]; j<=end[1]; j++) {
                for (int k=begin[2]; k<=end[2]; k++) {
                    // we know that abs(a-(double)i) < N+1, so we don't need to worry about
                    // going out of the array's bounds
                    weight = lut->get(a-(double)i) *
                             lut->get(b-(double)j) *
                             lut->get(c-(double)k);
                    ret_val += weight * f[i][j][k];
                }
            }
        }
    }*/

    return ret_val;
}

Eigen::RowVectorXd evaluate_interpolation(const Eigen::RowVectorXd & pt, 
                                const Eigen::RowVectorXd & x_min, 
                                double delta_x,
                                source_cache& grid_sources)
{
    // initialize return value
    Eigen::RowVectorXd ret_val = Eigen::RowVectorXd::Zero(grid_sources.get_num_tetras());
    // pre-computing for efficiency
    double a = (pt[0] - x_min[0])/delta_x; 
    double b = (pt[1] - x_min[1])/delta_x; 
    double c = (pt[2] - x_min[2])/delta_x; 

    // parameters for linear interpolation
    double t_x, t_y, t_z; 
    int n_x, n_y, n_z;
    n_x = static_cast<int>(floor(a));
    n_y = static_cast<int>(floor(b));
    n_z = static_cast<int>(floor(c));
    t_x = a - (double)n_x;
    t_y = b - (double)n_y;
    t_z = c - (double)n_z;

    cout << "Evaluating linear interpolation..." << endl;
    ret_val = (1.0 - t_z) * (
                  (1.0 - t_y) * (
                       (1.0 - t_x) * grid_sources.get_source(n_x, n_y, n_z) +
                       t_x * grid_sources.get_source(n_x + 1, n_y, n_z) ) +
                  t_y * (
                       (1.0 - t_x) * grid_sources.get_source(n_x, n_y+1, n_z) +
                       t_x * grid_sources.get_source(n_x + 1, n_y+1, n_z) ) ) +
              t_z * (
                  (1.0 - t_y) * (
                       (1.0 - t_x) * grid_sources.get_source(n_x, n_y, n_z+1) +
                       t_x * grid_sources.get_source(n_x + 1, n_y, n_z+1) ) +
                  t_y * (
                       (1.0 - t_x) * grid_sources.get_source(n_x, n_y+1, n_z+1) +
                       t_x * grid_sources.get_source(n_x + 1, n_y+1, n_z+1) ) );

    return ret_val;
}

void sample_function(std::function<double(double)> &f,
                               double x_min,
                               double x_max,
                               double step,
                               vector<double> & func_values) {
    // allocate memory
    int n_samples = (int)floor((x_max - x_min)/step);
    func_values.resize(n_samples);

    // sample function
    for (int i = 0; i<n_samples; i++) {
        func_values[i] = f(x_min + step*i);
    }
}

// overloaded 3d version
void sample_function(std::function<double(double, double, double)> &f,
                               double x_min[3],
                               double x_max[3],
                               double step,
                               vector<vector<vector<double>>> & func_values) {
    // allocate memory
    int n_samples[3];
    for (int i=0; i<3; i++) {
        n_samples[i] = (int)floor((x_max[i] - x_min[i])/step);
    }
    func_values.resize(n_samples[0], vector<vector<double>>(n_samples[1],
                                                            vector<double>(n_samples[2])));
    // sample function
    for (int i = 0; i<n_samples[0]; i++) {
        for (int j = 0; j<n_samples[1]; j++) {
            for (int k = 0; k<n_samples[2]; k++) {
                func_values[i][j][k] = f(x_min[0] + step*i, x_min[1] + step*j, x_min[2] + step*k);
            }
        }
    }
}

vector<float> interpolate_line_source(const SP_Line& line, const vector<double> & x_min, 
                    source_cache & grid_sources, unsigned num_pts, double delta_x)
{
    // first define the function that would compute the fluence from a point source
    std::function<Eigen::RowVectorXd(Eigen::RowVectorXd)> fluence_pt = [&](Eigen::RowVectorXd t)->Eigen::RowVectorXd 
    {
        // 1- define delta_x TODO
        Eigen::RowVectorXd xmin(3); 
        for (unsigned i = 0; i < 3; i++) {
            xmin(i) = x_min[i]; 
        }
        // 2- Call evaluate interpolation for point sources that make use of grid_sources
        return SP_IE::evaluate_interpolation(t, xmin, delta_x, grid_sources); 
    };

    Eigen::RowVectorXd l(3), r(3); 
    l(0) = static_cast<double>(line.end0.get_xcoord());
    l(1) = static_cast<double>(line.end0.get_ycoord());
    l(2) = static_cast<double>(line.end0.get_zcoord());
    r(0) = static_cast<double>(line.end1.get_xcoord());
    r(1) = static_cast<double>(line.end1.get_ycoord());
    r(2) = static_cast<double>(line.end1.get_zcoord());
    Eigen::RowVectorXd fluence_vals = integrate_midpoint(fluence_pt, l, r, num_pts); 

    // TODO: might be expensive
    vector<float> fluence;
    for (unsigned i = 0; i < fluence_vals.size(); i++) {
        fluence.push_back(static_cast<float>(fluence_vals(i))); 
    }

    return fluence;
}

};
