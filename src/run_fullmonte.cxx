/**
 * @author: Abed Yassine
 * @date: September 26th, 2019
 * \brief implementation of the wrapper class of FullMonteSW
 */

#include "read_mesh.h"
#include "src_placer.h"
#include "profiler.h"

#include "file_parser.h"
#include "run_fullmonte.h"

#include <sstream>

using namespace std;

//! Default constructor
WrapperFullMonteSW::WrapperFullMonteSW() : 
    _mesh(nullptr), _materials(nullptr), _kernel(nullptr), _tetraCache(nullptr), _enclosing(nullptr)
{
}

/**
 * Regular constructor 
 * @param mesh_file [in] input file of the mesh to read
 * @param fparser [in] pointer to XML parser
 * @param read_vtk [in] specifies if the input file is in .mesh or .vtk format
 */
WrapperFullMonteSW::WrapperFullMonteSW(string mesh_file, Parser* fparser, bool read_vtk, bool use_cuda) : 
    _mesh_file(mesh_file), _fparser(fparser), _read_vtk(read_vtk), _use_cuda(use_cuda)
{
    if (read_vtk) {
        VTKMeshReader R; 
        R.filename(mesh_file); 
        R.read(); 
        _mesh = R.mesh(); 
    }
    else {
        TIMOSMeshReader R; 
        R.filename(mesh_file); 
        R.read(); 
        _mesh = R.mesh(); 
    }

    create_material_set();
    read_parameters();

    if (_use_cuda && !USE_CUDA) {
        fprintf(stderr, "\033[1;%dmWrapperFullMonteSW::CUDA not supported. Exiting...\033[0m\n", 31); 
        std::exit(-1); 
    }
    _kernel = nullptr;

#if USE_CUDA
    _kernel_cuda = nullptr;
    if (!_use_cuda) {
        _kernel = new TetraVolumeKernel(); 
        _kernel->geometry(_mesh); 
        _kernel->materials(_materials); 

        _kernel->roulettePrWin(_roulette_pr_win); 
        _kernel->rouletteWMin(_roulette_w_min);
        _kernel->maxSteps(_max_steps);
        _kernel->maxHits(_max_hits); 
        _kernel->packetCount(1000000); 
        _kernel->threadCount(MAX_THREAD_COUNT); 
        _kernel->randSeed(_rand_seed); 
        _EVF.kernel(_kernel); 
    } else {
        _kernel_cuda = new TetraCUDAVolumeKernel(); 
        _kernel_cuda->geometry(_mesh); 
        _kernel_cuda->materials(_materials); 

        _kernel_cuda->roulettePrWin(_roulette_pr_win); 
        _kernel_cuda->rouletteWMin(_roulette_w_min);
        _kernel_cuda->maxSteps(_max_steps);
        _kernel_cuda->maxHits(_max_hits); 
        _kernel_cuda->packetCount(1000000); 
        _kernel_cuda->randSeed(_rand_seed); 
        _EVF.kernel(_kernel_cuda); 
    }
#else
    _kernel = new TetraVolumeKernel(); 
    _kernel->geometry(_mesh); 
    _kernel->materials(_materials); 

    _kernel->roulettePrWin(_roulette_pr_win); 
    _kernel->rouletteWMin(_roulette_w_min);
    _kernel->maxSteps(_max_steps);
    _kernel->maxHits(_max_hits); 
    _kernel->packetCount(1000000); 
    _kernel->threadCount(MAX_THREAD_COUNT); 
    _kernel->randSeed(_rand_seed); 
    _EVF.kernel(_kernel); 
#endif

    _EVF.inputEnergy(); 
    _EVF.outputFluence(); 
    
    //_mesh_writer.mesh(_mesh); 
    _mesh_writer = nullptr;
    
    _tetraCache = new TetraLookupCache(); 
    _enclosing = new TetraEnclosingPointByLinearSearch(); // TODO: update this to use rtree 
    _enclosing->mesh(_mesh); 
}

/** 
 * Runs FullMonteSW with a composite source consisting of <sources>
 * @param sources           [in] the sources to pass to FullMonte
 * @param fluence           [output] the resulting fluence vector from FullMonte 
 * @param dump_data_to_File [in] specifies whether to dump the results to a file or not
 * @param file_name         [in] if dump_data_to_file is true, specify the output file name with this parameter
 * @param num_packets       [in] number of packets to run
 */
void WrapperFullMonteSW::run_composite_fullmonte(const vector<Source::Abstract*>& sources, 
                            vector<float>& fluence, bool dump_data_to_file, std::string file_name, 
                            long long unsigned num_packets)
{
    // _kernel->packetCount(num_packets); 
    //_kernel->packetCount(1000000*sources.size()); 
    Source::Composite composite_source; 

    // Add all sources to the composite source 
    for (auto & source : sources) {
        composite_source.add(source); 
    }

    OutputDataCollection* ODC;
#if USE_CUDA
    if (!_use_cuda) {
        start_fullmonte_async(&composite_source, num_packets);
        ODC = get_fm_result();
    } else {
        start_fullmonte_async_cuda(&composite_source, num_packets);
        ODC = get_fm_result_cuda();
    }
#else
    start_fullmonte_async(&composite_source, num_packets);
    ODC = get_fm_result();
#endif
    
    // conversion from energy to fluence
    _EVF.data(ODC->getByName("VolumeEnergy")); 
    _EVF.update();

    const OutputData* result = _EVF.result(); 

    const SpatialMap<float>* sMap = dynamic_cast<const SpatialMap<float>*>(result); 
    if (!sMap) 
    {
        fprintf(stderr, "\033[1;%dmrun_composite_fullmonte::failed to cast results....\033[0m\n", 31); 
        std::exit(-1); 
    }
    for (unsigned i = 0; i < sMap->dim(); i++)
    {
        fluence.push_back(sMap->get(i)); 
    }
    
    if (dump_data_to_file)
    {
        if (_mesh_writer)
            delete _mesh_writer;

        _mesh_writer = new VTKMeshWriter(); 
        _mesh_writer->mesh(_mesh); 

        if (file_name == "") {
            _mesh_writer->filename(_mesh_file+"_composite.vtk"); 
        } else {
            _mesh_writer->filename(file_name);
        }

        int randd = rand();
        stringstream ss; 
        ss << "Fluence_" << randd;
        _mesh_writer->addData(ss.str().c_str(), _EVF.result()); 
        _mesh_writer->write(); 
    }
}

/** 
 * Runs the sources if FullMonteSW one at a time with unit power to generate a dose matrix 
 * @param sources           [in] sources to run in FullMonteSW
 * @param fluences          [out] Results dose matrix of fluence values 
 * @param dump_data_to_file [in] if true, the results will be written to a file 
 * @param file_name         [in] if dump_data_to_file is true, specify the output file name with this 
 * @param num_packets       [in] number of packets to run
 */
void WrapperFullMonteSW::run_multiple_fullmonte(const vector<Source::Abstract*>& sources, 
                                vector<vector<float>> & fluences,
                                bool dump_data_to_file, std::string file_name, 
                                long long unsigned num_packets)
{

	// clear fluences 
	for (unsigned i = 0; i < fluences.size(); i++)
	{
		fluences[i].clear(); 
		fluences[i].resize(0);
	}
	fluences.clear(); 
	fluences.resize(0);


    // _kernel->packetCount(num_packets); 
    unsigned idx = 0; 
    if (dump_data_to_file) {
        if (_mesh_writer)
            delete _mesh_writer;

        _mesh_writer = new VTKMeshWriter(); 
        _mesh_writer->mesh(_mesh); 

        if (file_name == "") {
            _mesh_writer->filename(_mesh_file+"_multiple.vtk");
        } else {
            _mesh_writer->filename(file_name);  
        }
    }

    for (auto & source : sources) 
    {
        cout << "Currently running source number: " << idx << endl;

        OutputDataCollection* ODC;
#if USE_CUDA
        if (!_use_cuda) {
            start_fullmonte_async(source, num_packets);
            ODC = get_fm_result();
        } else {
            start_fullmonte_async_cuda(source, num_packets);
            ODC = get_fm_result_cuda();
        }
#else
        start_fullmonte_async(source, num_packets);
        ODC = get_fm_result();
#endif

        // conversion from energy to fluence
        _EVF.data(ODC->getByName("VolumeEnergy")); 
        _EVF.update();

        stringstream fluence_name; 
        fluence_name << "Fluence" << idx;
    
        const OutputData* result = _EVF.result(); 

        const SpatialMap<float>* sMap = dynamic_cast<const SpatialMap<float>*>(result); 
        if (!sMap) 
        {
            fprintf(stderr, "\033[1;%dmrun_multiple_fullmonte::failed to cast results....\033[0m\n", 31);
            std::exit(-1); 
        }
        vector<float> fluence; 
        for (unsigned i = 0; i < sMap->dim(); i++)
        {
            fluence.push_back(sMap->get(i)); 
        }
        fluences.push_back(fluence); 

        if (dump_data_to_file)
        {
            _mesh_writer->addData(fluence_name.str().c_str(), _EVF.result()->clone()); 
        }
        idx++;
    }
   
    if (dump_data_to_file) {
        _mesh_writer->write(); 
    }
}

/** 
 * Runs the sources if FullMonteSW one at a time with unit power to generate a dose matrix 
 * @param idx               [in] index of the source to run in FullMonteSW
 * @param source            [in] source to run in FullMonteSW
 * @param fluences          [out] Results dose matrix of fluence values 
 * @param dump_data_to_file [in] if true, the results will be written to a file 
 * @param file_name         [in] if dump_data_to_file is true, specify the output file name with this 
 * @param num_packets       [in] number of packets to run
 */
void WrapperFullMonteSW::run_single_fullmonte(unsigned idx, Source::Abstract* source, 
                                vector<vector<float>> & fluences,
                                bool dump_data_to_file, std::string file_name, 
                                long long unsigned num_packets)
{

    // _kernel->packetCount(num_packets); 
    if (dump_data_to_file) {
        if (_mesh_writer)
            delete _mesh_writer;

        _mesh_writer = new VTKMeshWriter(); 
        _mesh_writer->mesh(_mesh); 

        if (file_name == "") {
            _mesh_writer->filename(_mesh_file+"_multiple.vtk");
        } else {
            _mesh_writer->filename(file_name);  
        }
    }

    cout << "Currently running source number: " << idx << endl;

    OutputDataCollection* ODC;
#if USE_CUDA
    if (!_use_cuda) {
        start_fullmonte_async(source, num_packets);
        ODC = get_fm_result();
    } else {
        start_fullmonte_async_cuda(source, num_packets);
        ODC = get_fm_result_cuda();
    }
#else
    start_fullmonte_async(source, num_packets);
    ODC = get_fm_result();
#endif

    // conversion from energy to fluence
    _EVF.data(ODC->getByName("VolumeEnergy")); 
    _EVF.update();

    stringstream fluence_name; 
    fluence_name << "Fluence" << idx;

    const OutputData* result = _EVF.result(); 

    const SpatialMap<float>* sMap = dynamic_cast<const SpatialMap<float>*>(result); 
    if (!sMap) 
    {
        fprintf(stderr, "\033[1;%dmrun_multiple_fullmonte::failed to cast results....\033[0m\n", 31);
        std::exit(-1); 
    }
    for (unsigned i = 0; i < sMap->dim(); i++)
    {
        fluences[idx][i] = sMap->get(i); 
    }

    if (dump_data_to_file)
    {
        _mesh_writer->addData(fluence_name.str().c_str(), _EVF.result()->clone()); 
    }
   
    if (dump_data_to_file) {
        _mesh_writer->write(); 
    }
}

/** 
 * Returns the ID of the tetra that encloses the point with the given coordinates 
 */
unsigned WrapperFullMonteSW::get_tetra_enclosing_point(float xcoord, float ycoord, float zcoord)
{
    // create a point 
    const std::array<float, 3> point {
        xcoord, 
        ycoord, 
        zcoord
    };

    unsigned id = 0; 
    _enclosing->point(point); 


    // search cache first 
    bool in_cache = _enclosing->searchCache(_tetraCache); 

    if (!in_cache)
    {
        _enclosing->update(); 
    }

    id = _enclosing->tetra().value(); 

    _tetraCache->update(_enclosing->tetra());

    return id; 
}

//! Returns a cloned object of the wrapper class 
WrapperFullMonteSW* WrapperFullMonteSW::clone() const
{
    WrapperFullMonteSW* fm_clone = new WrapperFullMonteSW(_mesh_file, _fparser, _read_vtk, _use_cuda); 
    return fm_clone;
}

//! writes the mesh to a file in a .vtk format
void WrapperFullMonteSW::write_mesh(string file_name)
{
    if (_mesh_writer)
            delete _mesh_writer;

        _mesh_writer = new VTKMeshWriter(); 
        _mesh_writer->mesh(_mesh); 

        if (file_name == "") {
            _mesh_writer->filename(_mesh_file+"_out.vtk"); 
        } else {
            _mesh_writer->filename(file_name);
        }

        _mesh_writer->write(); 
}

void WrapperFullMonteSW::start_fullmonte_async(const Source::Abstract* source, long long unsigned num_packets)
{
    profiler fm_async ("Running FM async");
    _kernel->packetCount(num_packets);

    _kernel->source(source); 
    
        // Running the kernel
    cout << "Async start" << endl;
    _kernel->startAsync();

    while(!_kernel->done())
    {
        cout << "Progress: " << setw(8) << fixed << setprecision(3) << 
                100.0f*_kernel->progressFraction() << "%" << endl << flush;
        usleep(1000000);
    }

    _kernel->finishAsync();
    cout << "Async Finished." << endl;
}
#if USE_CUDA
// TODO: running GPU means CPU is idle
void WrapperFullMonteSW::start_fullmonte_async_cuda(const Source::Abstract* source, long long unsigned num_packets)
{
    profiler cuda_async ("Running CUDA FM async");
    _kernel_cuda->packetCount(num_packets);

    _kernel_cuda->source(source); 
    
        // Running the kernel
    cout << "CUDA Async start" << endl;
    _kernel_cuda->startAsync();

    // TODO: _kernel_cuda does not return valid progressFraction, commenting out
    // while(!_kernel_cuda->done())
    // {
    //     cout << "Progress: " << setw(8) << fixed << setprecision(3) << 
    //             100.0f*_kernel_cuda->progressFraction() << "%" << endl << flush;
    //     usleep(10000);  // Reduce usleep time for CUDA
    // }

    _kernel_cuda->finishAsync();
    cout << "CUDA Async Finished." << endl;
}
#endif

OutputDataCollection* WrapperFullMonteSW::get_fm_result()
{
    profiler get_fm_results ("Fetching fm result");
    return _kernel->results(); 
}
#if USE_CUDA
OutputDataCollection* WrapperFullMonteSW::get_fm_result_cuda()
{
    profiler get_cuda_results ("Fetching fm result CUDA");
    return _kernel_cuda->results(); 
}
#endif

void WrapperFullMonteSW::run_multiple_fullmonte_with_detectors(const vector<Source::Abstract*>& sources, 
                            std::vector<std::vector<float>> & detected_weights, 
                            float detector_radius, float detector_na, 
                            long long unsigned num_packets)
{
    /**
     * Go over sources one by one and launch a kernel with the others being detectors
     * 
     */ 

    // _kernel->packetCount(num_packets); 

    detected_weights.clear(); 
    detected_weights.resize(sources.size()); 
    for (unsigned s = 0; s < sources.size(); s++) 
    {
        detected_weights[s].resize(sources.size()); 

        Source::Composite composite_source; 
        
        // Current source is the light source and rest are detectors (if they are only line sources)
        composite_source.add(sources[s]);

        std::vector<Source::CylDetector *> detectors(sources.size());
        
        for (unsigned j = 0; j < sources.size(); j++) 
        {
            if (j == s || dynamic_cast<Source::Point*>(sources[j]) != nullptr) 
            {
                detected_weights[s][j] = 0.0f; // can't detect from self and from point sources (as they are not modeled in FullMonte)
                continue; 
            }
            Source::Line* curr_line = dynamic_cast<Source::Line*>(sources[j]); 
            
            detectors[j] = new Source::CylDetector();
            detectors[j]->endpoint(0, curr_line->endpoint(0));
            detectors[j]->endpoint(1, curr_line->endpoint(1));
            detectors[j]->radius(detector_radius); 
            detectors[j]->numericalAperture(detector_na); 
            
            detectors[j]->detectionType("ODE"); 

            composite_source.add(detectors[j]); 
        }

        if (!_use_cuda) {
            start_fullmonte_async(&composite_source, num_packets);
        } else {
#if USE_CUDA
            start_fullmonte_async_cuda(&composite_source, num_packets);
#endif
        }

        for (unsigned j = 0; j < sources.size(); j++) 
        {
            if (j != s && dynamic_cast<Source::Point*>(sources[j]) == nullptr) 
            {
                detected_weights[s][j] = detectors[j]->detectedWeight();
            
                delete detectors[j]; 
            }
        }
    }
}

void WrapperFullMonteSW::create_material_set() {
    bool matched_boundary;
    const vector <Parser_material> materials = _fparser->get_materials(matched_boundary);
    if (matched_boundary) {
        fprintf(stderr, "\033[1;%dmWarning: FullMonte may not support matched boundary correctly.\033[0m\n", 33);
    }

    if (_materials) delete _materials;

    _materials = new MaterialSet();
    _materials->resize((unsigned) materials.size());
    _materials->matchedBoundary(matched_boundary);

    for (const auto& m : materials) {
        Material* p = new Material();

		p->scatteringCoeff((float) m.mu_s);
		p->absorptionCoeff((float) m.mu_a);
		p->anisotropy((float) m.g);
		p->refractiveIndex((float) m.n);

        _materials->set(m.id, p);
    }
}

void WrapperFullMonteSW::read_parameters() {
    unordered_map <string, string> parameters_map = _fparser->get_fm_params();

	if (parameters_map.find("use_cuda") != parameters_map.end()) {
		_use_cuda = (parameters_map["use_cuda"] == "true");
	} else {
		_use_cuda = false; // Default power allocation value / 10
	}

    if (parameters_map.find("max_hits") != parameters_map.end()) {
        _max_hits = atoi(parameters_map["max_hits"].c_str());
    } else {
        _max_hits = 1000;
    }

    if (parameters_map.find("roulette_pr_win") != parameters_map.end()) {
        _roulette_pr_win = (float) atof(parameters_map["roulette_pr_win"].c_str());
    } else {
        _roulette_pr_win = 0.1f;
    }

    if (parameters_map.find("roulette_w_min") != parameters_map.end()) {
        _roulette_w_min = (float) atof(parameters_map["roulette_w_min"].c_str());
    } else {
        _roulette_w_min = 0.00001f;
    }

    if (parameters_map.find("max_steps") != parameters_map.end()) {
        _max_steps = atoi(parameters_map["max_steps"].c_str());
    } else {
        _max_steps = 100000;
    }

    if (parameters_map.find("rand_seed") != parameters_map.end()) {
        _rand_seed = atoi(parameters_map["rand_seed"].c_str());
    } else {
        _rand_seed = 2;
    }
}
