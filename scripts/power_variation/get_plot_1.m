% Script to plot comparison plots of unguardbanded dvh between baseline and power variation optimized results.

run_1
clear all;
dvh_grey = zeros(500, 2);
dvh_white = zeros(500, 2);
dvh_tumor = zeros(500, 2);
data = load('T1_no_guard_dvh.mat');

dvh_grey(:,1) = data.no_guardband_dvh(:, 3);
dvh_grey(:,2) = data.no_guard_opt_dvh(:, 3);
dvh_white(:,1) = data.no_guardband_dvh(:, 4);
dvh_white(:,2) = data.no_guard_opt_dvh(:, 4);
dvh_tumor(:,1) = data.no_guardband_dvh(:, 5);
dvh_tumor(:,2) = data.no_guard_opt_dvh(:, 5);

figure(1)
plot(dvh_grey)
title('Grey Matter Dose Volume Histogram')
xlabel('Dosage Relative to Threshold (%)')
ylabel('Volume (mm^3)')
legend('Nominal Optimized DVH', 'Uncertainty Optimized DVH')
grid
figure(2)
plot(dvh_white)
title('White Matter Dose Volume Histogram')
xlabel('Dosage Relative to Threshold (%)')
ylabel('Volume (mm^3)')
legend('Nominal Optimized DVH', 'Uncertainty Optimized DVH')
grid
figure(3)
plot(dvh_tumor)
title('Tumour Dose Volume Histogram')
xlabel('Dosage Relative to Threshold (%)')
ylabel('Volume (%)')
legend('Nominal Optimized DVH', 'Uncertainty Optimized DVH')
grid
