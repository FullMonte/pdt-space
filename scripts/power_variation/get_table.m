% Script to grab matlab files from 9 tumour cases (after alling append_files.py) and put the data into tables.
% Specifically for power variation experiments, modify for other occations.

run_1
run_2
run_3
run_4
run_5
run_6
run_7
run_8
run_9
clear all;
N = 9;
table_guard = zeros(N*4, 5);
table_no_guard = zeros(N*4, 5);
data_guard = cell(1, N);
data_no_guard = cell(1, N);
opt_guard = zeros(N*4, 5);
opt_no_guard = zeros(N*4, 5);

for i = 1:N
    data_guard{i} = load(sprintf("T%d_guard_v100.mat", i));
    data_no_guard{i} = load(sprintf("T%d_no_guard_v100.mat", i));
    table_guard((i-1)*4+1, :) = data_guard{1,i}.guardbanded_v100;
    table_guard((i-1)*4+2, :) = data_guard{1,i}.guardbanded_max_v100;
    table_guard((i-1)*4+3, :) = data_guard{1,i}.guardbanded_min_v100;
    table_guard((i-1)*4+4, :) = table_guard((i-1)*4+2, :) - table_guard((i-1)*4+3, :);
    table_no_guard((i-1)*4+1, :) = data_no_guard{1,i}.no_guardband_v100;
    table_no_guard((i-1)*4+2, :) = data_no_guard{1,i}.no_guard_max_v100;
    table_no_guard((i-1)*4+3, :) = data_no_guard{1,i}.no_guard_min_v100;
    table_no_guard((i-1)*4+4, :) = table_no_guard((i-1)*4+2, :) - table_no_guard((i-1)*4+3, :);
    
    opt_guard((i-1)*4+1, :) = data_guard{1,i}.guardbanded_opt_v100;
    opt_guard((i-1)*4+2, :) = data_guard{1,i}.guardbanded_max_opt_v100;
    opt_guard((i-1)*4+3, :) = data_guard{1,i}.guardbanded_min_opt_v100;
    opt_guard((i-1)*4+4, :) = opt_guard((i-1)*4+2, :) - opt_guard((i-1)*4+3, :);
    opt_no_guard((i-1)*4+1, :) = data_no_guard{1,i}.no_guard_opt_v100;
    opt_no_guard((i-1)*4+2, :) = data_no_guard{1,i}.no_guard_max_opt_v100;
    opt_no_guard((i-1)*4+3, :) = data_no_guard{1,i}.no_guard_min_opt_v100;
    opt_no_guard((i-1)*4+4, :) = opt_no_guard((i-1)*4+2, :) - opt_no_guard((i-1)*4+3, :);
end

