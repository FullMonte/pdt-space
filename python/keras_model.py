"""
@author: Abed Yassine
@date: August 26th, 2020

This module contains the keras model training functionalities 
"""

from mycolors import bcolors
import training_data as td
import common as ps_helpers

import tensorflow as tf
import tensorflow.keras as keras
import tensorflow.keras.layers as layers 
import pandas as pd

from scipy.stats import reciprocal
from sklearn.model_selection import RandomizedSearchCV

import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages

import math
import numpy as np
import pdb
import os

def r_square(y_true, y_pred): 
    """ 
    Calculates the coefficient of determination (R^2) for regression
    """
    SS_res = keras.backend.sum(keras.backend.square(y_true - y_pred))
    SS_tot = keras.backend.sum(keras.backend.square(y_true - keras.backend.mean(y_true)))
    return (1 - SS_res/(SS_tot + keras.backend.epsilon()))

def my_mape(y_true, y_pred): 
    #pdb.set_trace()
    abs_err = keras.backend.mean(tf.abs(y_true - y_pred)/y_true)
    return 100*abs_err 

def build_model(num_features, hidden_layers=3, hidden_units=33, regularization_rate=0.000016818093007199846): #2168634810943757):
    """
    This method creates the layers and model of the keras layers
    
    Default parameters are from a hyper parameter sweep

    @num_features: number of features in the input layer
    """ 
    
    # Define keras inputs 
    inputs = keras.Input(num_features)

    x = inputs
    for i in range (hidden_layers):
        x = layers.Dense(hidden_units, activation="tanh",
            kernel_regularizer=keras.regularizers.l2(regularization_rate), 
            #kernel_initializer="random_uniform"
            )(x)
        #hidden_units = hidden_units/10
    #hidden_units = 5
    #for i in range (hidden_layers):
    #    x = layers.Dense(hidden_units, activation="tanh",
    #        kernel_regularizer=keras.regularizers.l2(regularization_rate))(x)
    #    hidden_units = hidden_units*10
    
    # Define the outputs. 2 for mu_a and mu_s
    outputs = layers.Dense(2, activation="relu",
        kernel_regularizer=keras.regularizers.l2(regularization_rate))(x)
    #outputs = layers.Dense(2,
    #    kernel_regularizer=keras.regularizers.l2(regularization_rate))(x)
    #outputs = layers.LeakyReLU()(outputs)

    model = keras.Model(inputs, outputs)

    model.summary()
    keras.utils.plot_model(model, "keras_model.png", show_shapes=True)

    # Compile the model 
    model.compile(loss="mean_squared_error", # Can use huber_loss
                    optimizer=keras.optimizers.Adam(beta_1=0.8, beta_2=0.9999), 
                    metrics=["mean_squared_error", r_square, "mean_absolute_error", "mean_absolute_percentage_error"], 
                    run_eagerly=False) # accuracy doesn't make sense in regression
    return model

def CV_search_parameters(X_train, Y_train, X_val, Y_val):
    keras_reg = keras.wrappers.scikit_learn.KerasRegressor(build_model)

    param_distribs = {
        "num_features": [X_train.shape[1]],
        "hidden_layers": [0, 1, 2, 3],
        "hidden_units": np.arange(int(math.sqrt(X_train.shape[1])),int(X_train.shape[1]/2)),
        "regularization_rate": reciprocal(1e-5,1e-1),
    }

    rnd_search_cv = RandomizedSearchCV(keras_reg, param_distribs,
        n_iter=20, cv=5, random_state=10, n_jobs=None)

    X = np.concatenate((X_train, X_val), axis=0)
    Y = np.concatenate((Y_train, Y_val), axis=0)
    rnd_search_cv.fit(X, Y, epochs=600)#, validation_data=(X_val, Y_val))

    print ("Best params: " + str(rnd_search_cv.best_params_))
    print ("Best Score: " + str(rnd_search_cv.best_score_))

    return rnd_search_cv.best_estimator_.model, rnd_search_cv.best_params_
