"""
@author: Abed Yassine 
@date: Sept 11th, 2020

This script reads the log output from the rt_feedback pdt-space training models task to read the following
1- model type
2- Using pca? 
3- Using feature normalization
4- what features are cut
5- r_squared values 

It outputs a csv file containing all the information 

It expects as input the directory containing all log files and the output csv file to write to. If the file doesn't exist it creates one.
"""

from os import sys
from os import path
from os import listdir
import numpy as np
import csv
import copy
import json
import ast

if len(sys.argv) < 3:
	print("Please supply the following arguments in the following order:")
	print("1- Log directory")
	print("2- Output csv file")
	sys.exit()

in_dir = sys.argv[1]
out_file = sys.argv[2]

if not path.exists(in_dir):
	print("Input logs dir "+in_dir+" doesn't exist")
	sys.exit()


all_files = [f for f in listdir(in_dir) if path.isfile(path.join(in_dir, f))]

print(len(all_files))

dict_data = []

for i in range(len(all_files)): 
    f = all_files[i] 
    row = {}

    row['Test ID'] = i+1

    if 'keras' in f:
        row['Model'] = 'Keras NN'
    elif 'lightgbm' in f: 
        row['Model'] = 'Lightgbm'
    else:
        row['Model'] = 'invalid'

    if 'w_pca' in f:
        row['PCA?'] = True
    else:
        row['PCA?'] = False

    if 'w_normalization' in f:
        row['Feature Normalization?'] = True
    else:
        row['Feature Normalization?'] = False

    row['All Features?'] = False
    if '_m.log' in f:
        row['Remove Feature'] = 'mu_effs'
    elif '_o.log' in f:
        row['Remove Feature'] = 'dot_products'
    elif '_d.log' in f:
        row['Remove Feature'] = 'detected_weights'
    elif '_c.log' in f:
        row['Remove Feature'] = 'cross_products'
    elif '_n.log' in f:
        row['Remove Feature'] = '-'
        row['All Features?'] = True
    elif '_md.log' in f:
        row['Remove Feature'] = 'mu_effs and detected_weights'
    elif '_mo.log' in f:
        row['Remove Feature'] = 'mu_effs and dot_products'
    elif '_mc.log' in f:
        row['Remove Feature'] = 'mu_effs and cross_products'
    elif '_mdc.log' in f:
        row['Remove Feature'] = 'mu_effs and detected_weights and cross_products'
    elif '_mdo.log' in f:
        row['Remove Feature'] = 'mu_effs and detected_weights and dot_products'
    elif '_mdoc.log' in f:
        row['Remove Feature'] = 'all'
    elif '_moc.log' in f:
        row['Remove Feature'] = 'mu_effs and cross_products and dot_products'
    elif '_dc.log' in f:
        row['Remove Feature'] = 'detected_weights and cross_products'
    elif '_doc.log' in f:
        row['Remove Feature'] = 'detected_weights and cross_products and dot_products'
    elif '_do.log' in f:
        row['Remove Feature'] = 'detected_weights and dot_products'
    elif '_oc.log' in f:
        row['Remove Feature'] = 'cross_products and dot_products'
    else:
        row['Remove Feature'] = 'invalid'

    in_log = open(path.join(in_dir, f), 'r')

    row['Train r_squared'] = 'NA'
    row['Validation r_squared'] = 'NA'
    row['Train l2 cost'] = 'NA'
    row['Validation l2 cost'] = 'NA'
    for line in in_log:
        if 'keras' in f:
            if 'Model output: ' in line:
                try:
                    temp_dict = ast.literal_eval(line.split('Model output: ')[1].strip())
                    row['Train r_squared'] = temp_dict['r_square'][-1]
                    row['Validation r_squared'] = temp_dict['val_r_square'][-1]
                    row['Train l2 cost'] = temp_dict['loss'][-1]
                    row['Validation l2 cost'] = temp_dict['val_loss'][-1]
                    row['Train l1 cost'] = temp_dict['mean_absolute_error'][-1]
                    row['Validation l1 cost'] = temp_dict['val_mean_absolute_error'][-1]
                    row['Train mape cost'] = temp_dict['mean_absolute_percentage_error'][-1]
                    row['Validation mape cost'] = temp_dict['val_mean_absolute_percentage_error'][-1]
                except ValueError:
                    row['Train r_squared'] = 'NA'
                    row['Validation r_squared'] = 'NA'
                    row['Train l2 cost'] = 'NA'
                    row['Validation l2 cost'] = 'NA'
                    row['Train l1 cost'] = 'NA'
                    row['Validation l1 cost'] = 'NA'
                    row['Train mape cost'] = 'NA'
                    row['Validation mape cost'] = 'NA'
        if 'lightgbm' in f:
            if 'r_square for train ' in line:
                row['Train r_squared'] = line.split(': ')[1].strip()
            if 'r_square for validation ' in line:
                row['Validation r_squared'] = line.split(': ')[1].strip()
            if 'mse for train ' in line:
                row['Train l2 cost'] = line.split(': ')[1].strip()
            if 'mse for validation ' in line:
                row['Validation l2 cost'] = line.split(': ')[1].strip()
            if 'mae for train ' in line:
                row['Train l1 cost'] = line.split(': ')[1].strip()
            if 'mae for validation ' in line:
                row['Validation l1 cost'] = line.split(': ')[1].strip()
            if 'mape for train ' in line:
                row['Train mape cost'] = line.split(': ')[1].strip()
            if 'mape for validation ' in line:
                row['Validation mape cost'] = line.split(': ')[1].strip()
    dict_data.append(row)

csv_cols = ['Test ID', 'Model', 'PCA?', 'Feature Normalization?', 'All Features?', 'Remove Feature', 'Train r_squared', 'Validation r_squared', 'Train l2 cost', 'Validation l2 cost', 'Train l1 cost', 'Validation l1 cost', 'Train mape cost', 'Validation mape cost']
with open (out_file, 'w') as out_write:
    writer = csv.DictWriter(out_write, fieldnames=csv_cols)
    writer.writeheader()
    for data in dict_data:
        writer.writerow(data)
