"""
@author: Abed Yassine
@date: August 25th, 2020

This module contains a function that reads the training data for RtFeedback and returns 70% as the training data,
 20% as the validation data and 10% as the test data


Uses python3.5 and later
If a module does not exist, install it as follows:
> pip install <module_name> 

If a module exists, but you want the latest version, type the following:
> pip install --upgrade <module_name> 

"""

from mycolors import bcolors
import common as ps_helpers

from os import sys
from os import path
import numpy as np
import csv
import copy
import pdb
import math

from sklearn.model_selection import train_test_split

np.set_printoptions(threshold=sys.maxsize) 

def read_data(filename, normalize_labels=False, cut_mu_eff=False, cut_detected_weights=False, cut_dot_products=False, 
    cut_cross_products=False, train_for_mu_eff=False, shuffle=True):
    """ 
    This method reads the training data from <filename> and returns a set of train, validation and test data
    Every cut_<feature> is a boolean to indicate removing a certain feature from the data or not.
    """
    in_file = filename

    if not path.exists(in_file):
        print (bcolors.FAIL+"Input file "+in_file+" does not exist"+bcolors.ENDC)
        sys.exit(-1)
    print (bcolors.WARNING+"Reading training data"+bcolors.ENDC)

    max_srcs = 21
    min_srcs = 3
    max_diff_srcs = int(((max_srcs - 1)*(max_srcs))/2.0)
    min_diff_srcs = int(((min_srcs - 1)*(min_srcs))/2.0)
    max_num_features = 1 + 1 + max_srcs + max_diff_srcs + 3*max_diff_srcs + max_diff_srcs + max_srcs*max_srcs + max_srcs 
    min_num_features = 1 + 1 + min_srcs + min_diff_srcs + 3*min_diff_srcs + min_diff_srcs + min_srcs*min_srcs + min_srcs 

    with open(in_file) as csvfile:
        csvreader = csv.reader(csvfile, delimiter=',')
        
        # get number of samples (number of lines in file)
        num_t_samples = sum(1 for row in csvreader)
        csvfile.seek(0)

        # create array with maximum possible size 
        t_data = np.zeros((num_t_samples,  max_num_features))
        t_labels = np.zeros((num_t_samples, 2))
        feature_names = np.empty(max_num_features,dtype=object)

        row_idx = 0
        for row in csvreader:
            t_data[row_idx][0] = float(row[0])
            if row_idx == 500:
                feature_names[0] = 'tumor_volume'
            t_data[row_idx][1] = float(row[1])
            if row_idx == 500:
                feature_names[1] = 'num_srcs'
            num_srcs = int(row[1])
            num_diff_srcs = int(((num_srcs - 1)*(num_srcs))/2)
            for i in range(num_srcs): 
                t_data[row_idx][2 + i] = float(row[2 + i])
                if row_idx == 500:
                    feature_names[2+i] = 'length_'+str(i)

            for i in range(num_diff_srcs):
                t_data[row_idx][2 + max_srcs + i] = float(row[2 + num_srcs + i])
                if row_idx == 500:
                    feature_names[2+max_srcs+i] = 'src_det_sep_'+str(i)
            for i in range(3*num_diff_srcs):
                t_data[row_idx][2 + max_srcs + max_diff_srcs + i] = float(row[2 + num_srcs + num_diff_srcs + i])
                if row_idx == 500:
                    feature_names[2+max_srcs+max_diff_srcs+i] = 'cross_product_'+str(int(i/3.0))+'_'+str(i%3 + 1)
            for i in range(num_diff_srcs):
                t_data[row_idx][2 + max_srcs + 4*max_diff_srcs + i] = float(row[2 + num_srcs + 4*num_diff_srcs + i])
                if row_idx == 500:
                    feature_names[2+max_srcs+4*max_diff_srcs+i] = 'dot_product_'+str(i)
            for i in range(num_srcs*num_srcs):
                t_data[row_idx][2 + max_srcs + 5*max_diff_srcs + i] = float(row[2 + num_srcs + 5*num_diff_srcs + i])
                if row_idx == 500:
                    feature_names[2+max_srcs+5*max_diff_srcs+i] = 'detected_weight_'+str(i)
            for i in range(num_srcs):
                t_data[row_idx][2 + max_srcs + 5*max_diff_srcs + max_srcs*max_srcs + i] = float(row[2 + num_srcs + 5*num_diff_srcs + num_srcs*num_srcs + i])
                if row_idx == 500:
                    feature_names[2+max_srcs+5*max_diff_srcs+max_srcs*max_srcs+i] = 'mu_eff_'+str(i)
            t_labels[row_idx][0] = float(row[-2])
            t_labels[row_idx][1] = float(row[-1])
            row_idx += 1
            
            
    print (bcolors.UNDERLINE+bcolors.OKGREEN+"Done reading training data"+bcolors.ENDC)
    
    # Delete nan rows. 
    nan_indices = np.argwhere(np.isnan(t_data))
    t_labels = np.delete(t_labels, nan_indices[:,0], axis=0)
    t_data = np.delete(t_data, nan_indices[:,0], axis=0)
    num_t_samples = t_data.shape[0]

    if train_for_mu_eff:
        cut_mu_eff = True
        temp_t_labels = np.empty(shape=(num_t_samples,1))
        for i in range(num_t_samples):
            temp_t_labels[i,0] = math.sqrt(3*t_labels[i,0]*(t_labels[i,0] + t_labels[i,1]*0.2))
        t_labels = temp_t_labels


    # normalize labels 
    max_mus = np.empty(shape=(t_labels.shape[1]));
    if normalize_labels:
        for i in range(t_labels.shape[1]):
            max_mus[i] = max(t_labels[:, i])
            t_labels[:,i] = t_labels[:,i]/max_mus[i]

    #ps_helpers.plot_scatter(t_data, t_labels, feature_names)
    #sys.exit(1)

    if cut_mu_eff:
        first_idx = 1 + 1 + max_srcs + 5*max_diff_srcs + max_srcs*max_srcs
        cut_inds = np.arange(first_idx, first_idx + max_srcs)
        t_data = np.delete(t_data, cut_inds, axis = 1)
        feature_names = np.delete(feature_names, cut_inds, axis = 0)
        max_num_features = max_num_features - max_srcs
        min_num_features = min_num_features - min_srcs
    if cut_detected_weights:
        first_idx = 1 + 1 + max_srcs + 5*max_diff_srcs
        cut_inds = np.arange(first_idx, first_idx + max_srcs*max_srcs)
        t_data = np.delete(t_data, cut_inds, axis = 1)
        feature_names = np.delete(feature_names, cut_inds, axis = 0)
        max_num_features = max_num_features - max_srcs*max_srcs
        min_num_features = min_num_features - min_srcs*min_srcs
    if cut_dot_products:
        first_idx = 1 + 1 + max_srcs + 4*max_diff_srcs
        cut_inds = np.arange(first_idx, first_idx + max_diff_srcs)
        t_data = np.delete(t_data, cut_inds, axis = 1)
        feature_names = np.delete(feature_names, cut_inds, axis = 0)
        max_num_features = max_num_features - max_diff_srcs
        min_num_features = min_num_features - min_diff_srcs
    if cut_cross_products:
        first_idx = 1 + 1 + max_srcs + max_diff_srcs
        cut_inds = np.arange(first_idx, first_idx + 3*max_diff_srcs)
        t_data = np.delete(t_data, cut_inds, axis = 1)
        feature_names = np.delete(feature_names, cut_inds, axis = 0)
        max_num_features = max_num_features - 3*max_diff_srcs
        min_num_features = min_num_features - 3*min_diff_srcs

    # Split the data into train, validation and test data 
    if (t_labels.shape[0] == 1):
        X_train = t_data
        labels_train = t_labels
        X_val = np.empty(shape=(0, max_num_features))
        y_val = np.empty(shape=(0, t_labels.shape[1]))
        X_test = np.empty(shape=(0, max_num_features))
        y_test = np.empty(shape=(0, t_labels.shape[1]))
    else:
        X_train, X_non_train, labels_train, labels_non_train = train_test_split(t_data, t_labels, test_size=0.3, random_state=10, 
                    shuffle=shuffle)
        X_val, X_test, y_val, y_test  = train_test_split(X_non_train, labels_non_train, test_size=0.33, random_state=10,
                shuffle=shuffle)

    print(bcolors.OKGREEN+'Number of Training samples is: '+str(X_train.shape[0])+bcolors.ENDC)
    print(bcolors.OKGREEN+'Number of Validation samples is: '+str(X_val.shape[0])+bcolors.ENDC)
    print(bcolors.OKGREEN+'Number of Test samples is: '+str(X_test.shape[0])+bcolors.ENDC)

    return X_train, labels_train, X_val, y_val, X_test, y_test, X_train.shape[0], max_num_features, min_num_features, feature_names, max_mus

def read_test_data(filename = '', normalize_labels=False, cut_mu_eff=False, cut_detected_weights=False, cut_dot_products=False, cut_cross_products=False):
    X_train, y_train, X_val, y_val, X_test, y_test, num_samples, max_num_features, min_num_features, feature_names, max_mus = \
            read_data(filename=filename, normalize_labels=normalize_labels, cut_mu_eff=cut_mu_eff, cut_detected_weights=cut_detected_weights, \
                    cut_dot_products=cut_dot_products, cut_cross_products=cut_cross_products,
                    shuffle = False)

    X = np.append(X_train, X_val, axis=0)
    y = np.append(y_train, y_val, axis=0)
     
    X = np.append(X, X_test, axis=0)
    y = np.append(y, y_test, axis=0)

    print(bcolors.OKGREEN+'Number of Test samples is: '+str(X.shape[0])+bcolors.ENDC)
    return X, y, max_mus

def read_data_3_srcs_only(filename, normalize_labels=False, cut_mu_eff=False, cut_detected_weights=False, cut_dot_products=False, 
    cut_cross_products=False, train_for_mu_eff=False):
    """ 
    This method reads the training data from <filename> and returns a set of train, validation and test data
    Every cut_<feature> is a boolean to indicate removing a certain feature from the data or not.
    It only considers 3 sources 
    """
    in_file = filename

    if not path.exists(in_file):
        print (bcolors.FAIL+"Input file "+in_file+" does not exist"+bcolors.ENDC)
        sys.exit(-1)
    print (bcolors.WARNING+"Reading training data"+bcolors.ENDC)

    max_srcs = 3
    min_srcs = 3
    max_diff_srcs = int(((max_srcs - 1)*(max_srcs))/2.0)
    min_diff_srcs = int(((min_srcs - 1)*(min_srcs))/2.0)
    max_num_features = 1 +  max_srcs + max_diff_srcs + 3*max_diff_srcs + max_diff_srcs + max_srcs*(max_srcs - 1) + 1
    min_num_features = 1 +  min_srcs + min_diff_srcs + 3*min_diff_srcs + min_diff_srcs + min_srcs*(min_srcs - 1) + 1

    with open(in_file) as csvfile:
        csvreader = csv.reader(csvfile, delimiter=',')
        
        # get number of samples (number of lines in file)
        num_t_samples = sum(1 for row in csvreader)
        csvfile.seek(0)

        # create array with maximum possible size 
        t_data = np.zeros((num_t_samples,  max_num_features))
        t_labels = np.zeros((num_t_samples, 2))
        feature_names = np.empty(max_num_features,dtype=object)

        row_idx = 0
        for row in csvreader:
            tumor_vol = float(row[0])
            num_srcs = int(row[1])
            num_diff_srcs = int(((num_srcs - 1)*(num_srcs))/2)


            src_lengths = np.array([float(i) for i in row[2:2+num_srcs]])
            src_det_seps = np.empty(shape=(num_srcs, num_srcs), dtype=float)
            dot_products = np.empty(shape=(num_srcs, num_srcs), dtype=float)
            cross_products = np.empty(shape=(num_srcs, num_srcs,3), dtype=float)
            detected_weights = np.empty(shape=(num_srcs, num_srcs), dtype=float)
            recovered_mu_effs = np.array([float(i) for i in row[2+num_srcs+5*num_diff_srcs+num_srcs*num_srcs:-2]])
            
            idx = 2+num_srcs
            for i in range(num_srcs-1):
                src_det_seps[i,i] = 0
                temp = [float(j) for j in row[idx:idx+num_srcs-1-i]]
                src_det_seps[i,i+1:] = temp
                src_det_seps[i+1:,i] = temp
                idx = idx + num_srcs - 1 - i
            src_det_seps[num_srcs-1,num_srcs-1] = 0

            idx = 2+num_srcs+num_diff_srcs
            for i in range(num_srcs-1):
                cross_products[i,i,:] = [0,0,0]
                temp = [float(j) for j in row[idx:idx+3*(num_srcs-1-i)]]
                idx2 = 0
                for j in range(i+1, num_srcs):
                    cross_products[i,j,:] = temp[idx2:idx2+3]  
                    cross_products[j,i,:] = temp[idx2:idx2+3]
                    idx2 = idx2 + 3
                idx = idx + 3*(num_srcs - 1 - i)
            cross_products[num_srcs-1,num_srcs-1,:] = [0,0,0]
            
            idx = 2+num_srcs+4*num_diff_srcs
            for i in range(num_srcs-1):
                dot_products[i,i] = 0
                temp = [float(j) for j in row[idx:idx+num_srcs-1-i]]
                dot_products[i,i+1:] = temp
                dot_products[i+1:,i] = temp
                idx = idx + num_srcs - 1 - i
            dot_products[num_srcs-1,num_srcs-1] = 0

            idx = 2+num_srcs+5*num_diff_srcs
            for i in range(num_srcs):
                temp = [float(j) for j in row[idx:idx+num_srcs]]
                detected_weights[i,:] = temp
                idx = idx + num_srcs
            
            # extract 3 sources based on having the max detected weight between 2 sources, and the second mask between the third
            # and of the two sources
            max_d_weight_ids = np.unravel_index(np.argmax(detected_weights, axis=None),detected_weights.shape)
            src_ids = np.empty(3, dtype=int)
            src_ids[0] = max_d_weight_ids[0]
            src_ids[1] = max_d_weight_ids[1]

            # find another max weight in src_ids[0]
            if num_srcs > 3:
                temp = detected_weights[src_ids[0:2],:]
                m = np.zeros(temp.shape, dtype=bool)
                m[0,src_ids[1]]=True
                m[1,src_ids[0]]=True
                temp2 = np.ma.array(temp, mask=m)
                src_ids[2] = np.unravel_index(np.argmax(temp2, axis=None), temp2.shape)[1]
                src_ids = np.sort(src_ids)
            else:
                src_ids = np.array([0,1,2])
            
            num_srcs = 3
            num_diff_srcs = int(((num_srcs - 1)*(num_srcs))/2)
            t_data[row_idx][0] = float(row[0])
            if row_idx == 0:
                feature_names[0] = 'tumor_volume'
                for i in range(num_srcs):
                    feature_names[1+i] = 'length_'+str(i)

            for i in range(num_srcs): 
                t_data[row_idx][1 + i] = src_lengths[src_ids[i]]

            
            idx = 1+max_srcs
            for i in range(num_srcs):
                for j in range(i+1, num_srcs):
                    t_data[row_idx][idx] = src_det_seps[src_ids[i], src_ids[j]]
                    if (row_idx == 0):
                        feature_names[idx] = 'src_det_sep_'+str(i)+'_'+str(j)
                    idx = idx + 1

            idx = 1+max_srcs+max_diff_srcs
            for i in range(num_srcs):
                for j in range(i+1, num_srcs):
                    t_data[row_idx, idx:idx+3] = cross_products[src_ids[i], src_ids[j]]
                    if (row_idx == 0):
                        feature_names[idx:idx+3] = ['cross_product_'+str(i)+'_'+str(j)+'_0', 
                                                    'cross_product_'+str(i)+'_'+str(j)+'_1',
                                                    'cross_product_'+str(i)+'_'+str(j)+'_2']
                    idx = idx + 3
            
            idx = 1+max_srcs+4*max_diff_srcs
            for i in range(num_srcs):
                for j in range(i+1, num_srcs):
                    t_data[row_idx][idx] = dot_products[src_ids[i], src_ids[j]]
                    if (row_idx == 0):
                        feature_names[idx] = 'dot_product_'+str(i)+'_'+str(j)
                    idx = idx + 1

            idx = 1+max_srcs+5*max_diff_srcs
            for i in range(num_srcs):
                for j in range(num_srcs):
                    if (j == i):
                        continue
                    t_data[row_idx][idx] = detected_weights[src_ids[i], src_ids[j]]
                    if (row_idx == 0):
                        feature_names[idx] = 'detected_weight_'+str(i)+'_'+str(j)
                    idx = idx + 1

            t_data[row_idx][1+max_srcs+5*max_diff_srcs+num_srcs*(num_srcs-1)] = recovered_mu_effs[np.nonzero(recovered_mu_effs)].mean()
            if (row_idx == 0):
                feature_names[1+max_srcs+5*max_diff_srcs+num_srcs*(num_srcs-1)] = 'recovered_mu_eff'
            t_labels[row_idx][0] = float(row[-2])
            t_labels[row_idx][1] = float(row[-1])
            row_idx += 1
            
    print (bcolors.UNDERLINE+bcolors.OKGREEN+"Done reading training data"+bcolors.ENDC)
    
    # Delete nan rows. TODO CHECK WHY THIS IS HAPPENING
    nan_indices = np.argwhere(np.isnan(t_data))
    t_labels = np.delete(t_labels, nan_indices[:,0], axis=0)
    t_data = np.delete(t_data, nan_indices[:,0], axis=0)
    num_t_samples = t_data.shape[0]

    if train_for_mu_eff:
        cut_mu_eff = True
        temp_t_labels = np.empty(shape=(num_t_samples,1))
        for i in range(num_t_samples):
            temp_t_labels[i,0] = math.sqrt(3*t_labels[i,0]*(t_labels[i,0] + t_labels[i,1]*0.2))
        t_labels = temp_t_labels


    # normalize labels 
    max_mus = np.empty(shape=(t_labels.shape[1]));
    if normalize_labels:
        for i in range(t_labels.shape[1]):
            max_mus[i] = max(t_labels[:, i])
            t_labels[:,i] = t_labels[:,i]/max_mus[i]

    if cut_mu_eff:
        first_idx = 1 + max_srcs + 5*max_diff_srcs + max_srcs*(max_srcs-1)
        cut_inds = first_idx
        t_data = np.delete(t_data, cut_inds, axis = 1)
        feature_names = np.delete(feature_names, cut_inds, axis = 0)
        max_num_features = max_num_features - 1
        min_num_features = min_num_features - 1
    if cut_detected_weights:
        first_idx = 1 + max_srcs + 5*max_diff_srcs
        cut_inds = np.arange(first_idx, first_idx + max_srcs*(max_srcs-1))
        t_data = np.delete(t_data, cut_inds, axis = 1)
        feature_names = np.delete(feature_names, cut_inds, axis = 0)
        max_num_features = max_num_features - max_srcs*(max_srcs-1)
        min_num_features = min_num_features - min_srcs*(min_srcs-1)
    if cut_dot_products:
        first_idx = 1 +  max_srcs + 4*max_diff_srcs
        cut_inds = np.arange(first_idx, first_idx + max_diff_srcs)
        t_data = np.delete(t_data, cut_inds, axis = 1)
        feature_names = np.delete(feature_names, cut_inds, axis = 0)
        max_num_features = max_num_features - max_diff_srcs
        min_num_features = min_num_features - min_diff_srcs
    if cut_cross_products:
        first_idx = 1 +  max_srcs + max_diff_srcs
        cut_inds = np.arange(first_idx, first_idx + 3*max_diff_srcs)
        t_data = np.delete(t_data, cut_inds, axis = 1)
        feature_names = np.delete(feature_names, cut_inds, axis = 0)
        max_num_features = max_num_features - 3*max_diff_srcs
        min_num_features = min_num_features - 3*min_diff_srcs

    # Split the data into train, validation and test data 
    X_train, X_non_train, labels_train, labels_non_train = train_test_split(t_data, t_labels, test_size=0.3, random_state=10)
    X_val, X_test, y_val, y_test  = train_test_split(X_non_train, labels_non_train, test_size=0.33, random_state=10)

    print(bcolors.OKGREEN+'Number of Training samples is: '+str(X_train.shape[0])+bcolors.ENDC)
    print(bcolors.OKGREEN+'Number of Validation samples is: '+str(X_val.shape[0])+bcolors.ENDC)
    print(bcolors.OKGREEN+'Number of Test samples is: '+str(X_test.shape[0])+bcolors.ENDC)

    return X_train, labels_train, X_val, y_val, X_test, y_test, X_train.shape[0], max_num_features, min_num_features, feature_names, max_mus

if __name__ == '__main__':
    read_data(os.path.join(ps_helpers.get_current_location(), 'rt_feedback_training_data_1e7_packets.csv'))
    sys.exit(0)
