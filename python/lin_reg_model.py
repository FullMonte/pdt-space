"""
@author: Abed Yassine
@date: Sept 7th, 2020

This module contains the linear regression training functionalities
"""

from mycolors import bcolors
import training_data as td
import common as ps_helpers 

from sklearn.linear_model import LinearRegression
from sklearn.linear_model import Ridge
from sklearn.metrics import mean_squared_error
import pandas as pd 

from scipy.stats import reciprocal 
from sklearn.model_selection import RandomizedSearchCV

import numpy as np
import pdb
import matplotlib.pyplot as plt


def train(X_train, Y_train, X_val, Y_val, X_test, Y_test, regularize=False, reg_alpha=0.5):
    if regularize:
        lin_reg = Ridge(alpha=reg_alpha)
    else:
        lin_reg = LinearRegression()

    lin_reg.fit(X_train, Y_train)

    history = {}
    history['coeffs'] = lin_reg.coef_
    history['intercept'] = lin_reg.intercept_
    history['train_score'] = lin_reg.score(X_train, Y_train)
    history['val_score'] = lin_reg.score(X_val, Y_val)
    history['test_score'] = lin_reg.score(X_test, Y_test)

    return history
