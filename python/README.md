Directory Structure
-------------------

This directory contains all modules needed to train a model to recover the optical properties from the detected light weight.

The file descriptions are as follows:
  - ``mycolors.py``: A module that define CLI colors to use while printing 
  - ``common.py``: A module that defines common and helper methods
  - ``training_data.py``: A module to the training data file (A csv file generated by ``pdt_space``)
  - ``keras_model.py``: A module that defines the functions needed to train a neural network using Keras API
  - ``lightgbm_model.py``: A module that defines the functions needed to train a boosting algorithm using lighgbm framework
  - ``pdt_space_rt_feedback_ml.py``: The main module to import

Requirements
------------
The modules are written and tested in ``Python3.5``. The following python packages are required:
- keras
- tensorflow
- sklearn
- setuptools
- numpy 
- wheel 
- matplotlib
- lighgbm
- pdb
- os
- math
- scipy
- pandas
- seaborn
- graphviz

How to run
----------
You can type in CLI the following:
```
python pdt_space_rt_feedback_ml.py
```
This will run with the default model `neural networks`.  

Or you can run python and use ``import pdt_space_rt_feedback_ml as pml``, and the call ``pml.rtfeedback_ML'' method with the desired parameters (check module). 


Package Installation
--------------------
To install all dependencies, run:
```
sh install_dependcies.sh
```
 - **Note:** It is recommended to run these modules in a python virtual environment as follows:
   ```
   > sudo apt install python3.5-venv
   > python3.5 -m venv /path/to/desired/directory
   > source /path/to/desired/directory/bin/activate
   ```

Known Issues
------------
- In scikit-learn version > 0.21.2, edit line 761 in ``sklearn/model_selection/_search.py`` to remove the nested clone, and make it one clone only. 
