"""
@author: Abed Yassine
@date: Sept 2nd, 2020

This module contains the lightgbm model training functionalities 
"""

from mycolors import bcolors
import training_data as td
import common as ps_helpers

import lightgbm as lgb
from sklearn.metrics import mean_squared_error 
from sklearn.metrics import r2_score
from sklearn.metrics import mean_absolute_error
#from sklearn.metrics import mean_absolute_percentage_error
import pandas as pd
import pickle as pk

from scipy.stats import reciprocal
from sklearn.model_selection import RandomizedSearchCV

import numpy as np
import os
import pdb
import matplotlib.pyplot as plt
import seaborn as sns
sns.set(style="whitegrid")


def train(X_train, Y_train, X_val, Y_val, X_test, Y_test, feature_names):
    """
    Since lightgbm does not support multi-outputs. We create a regressor for each output

    @return a dictionary of models with keys being the output index
    """
    # These parameters are found with cv search 
    hyper_params = {
        0: {
            'task': 'train',
            'boosting_type': 'gbdt', 
            'objective': 'regression',
            'lambda_l2': 0.0036388262327167733,
            'metric': ['l2', 'l1'],#, 'auc'],
            'learning_rate': 0.0274070874134328, 
            'feature_fraction': 0.9, 
            'bagging_fraction': 1.0, 
            'bagging_freq': 20, 
            'verbose': 1, 
            'max_depth': 4, 
            'num_leaves': 7,
            'max_bin': 512, #Use default 255
            'n_estimators': 500
        },
        1: {
            'task': 'train',
            'boosting_type': 'gbdt', 
            'objective': 'regression',
            'lambda_l2': 0.0036388262327167733,
            'metric': ['l2', 'l1'],#, 'auc'],
            'learning_rate': 0.0274070874134328, 
            'feature_fraction': 0.9, 
            'bagging_fraction': 1.0, 
            'bagging_freq': 20, 
            'verbose': 1, 
            'max_depth': 4, 
            'num_leaves': 7,
            'max_bin': 512,
            'n_estimators': 500
        }
    }

    gbms = {} 
    for i in range(Y_train.shape[1]):
        gbm = lgb.LGBMRegressor(**hyper_params[i])

        print("")
        gbm.fit(X_train, Y_train[:, i], eval_set=[(X_val, Y_val[:,i]), (X_train, Y_train[:,i])], 
            eval_metric=['l2', 'l1'], early_stopping_rounds=100)
        gbms[str(i)] = gbm

        # save the model
        pk.dump(gbm, open(os.path.join(ps_helpers.get_current_location(), 'LGBM_model_output_'+str(i)+'.txt'), 'wb')) #, num_iteration=gbm.best_iteration_)


    # TODO For MSE and MAE we need to normalize the labels 
    # Predictions of test
    compute_metric(Y_test, X_test, gbms, 'test', 'mse', mean_squared_error)
    compute_metric(Y_test, X_test, gbms, 'test', 'r_square', r2_score)
    compute_metric(Y_test, X_test, gbms, 'test', 'mape', ps_helpers.mean_absolute_percentage_error)
    compute_metric(Y_test, X_test, gbms, 'test', 'mae', mean_absolute_error)

    print ("")
    compute_metric(Y_val, X_val, gbms, 'validation', 'mse', mean_squared_error)
    compute_metric(Y_val, X_val, gbms, 'validation', 'r_square', r2_score)
    compute_metric(Y_val, X_val, gbms, 'validation', 'mape', ps_helpers.mean_absolute_percentage_error)
    compute_metric(Y_val, X_val, gbms, 'validation', 'mae', mean_absolute_error)

    print ("")
    compute_metric(Y_train, X_train, gbms, 'train', 'mse', mean_squared_error)
    compute_metric(Y_train, X_train, gbms, 'train', 'r_square', r2_score)
    compute_metric(Y_train, X_train, gbms, 'train', 'mape', ps_helpers.mean_absolute_percentage_error)
    compute_metric(Y_train, X_train, gbms, 'train', 'mae', mean_absolute_error)
    
    # plotting feature importance 
    plot_feature_importance(gbms, Y_train.shape[1], feature_names)

    # plotting learning curves
    plot_learning_curves(gbms, Y_train.shape[1])

    # plotting trees
    plot_trees(gbms, Y_train.shape[1])
    
    #plt.show()
    return gbms

def evaluate_test_data(X_test, y_test):
    """
    This method loads the saved model and evaluates it on the given test data
    """
    gbms = {}
    for i in range(y_test.shape[1]):
        gbm = pk.load(open(os.path.join(ps_helpers.get_current_location(), 'LGBM_model_output_'+str(i)+'.txt'), 'rb'))
        gbms[str(i)] = gbm

    y_pred = compute_metric(y_test, X_test, gbms, 'test', 'mse', mean_squared_error)
    compute_metric(y_test, X_test, gbms, 'test', 'r_square', r2_score)
    compute_metric(y_test, X_test, gbms, 'test', 'mape', ps_helpers.mean_absolute_percentage_error)
    compute_metric(y_test, X_test, gbms, 'test', 'mae', mean_absolute_error)
    
    return y_pred

def plot_feature_importance(gbm_models, num_outputs, feature_names, tuned=False, num=20):  # Number of features to plot
    if feature_names.shape[0] < num:
        num = feature_names.shape[0]
    if tuned:
        prefix = 'Tuned'
    else:
        prefix = ''
    # plotting feature importance
    for i in range(num_outputs):
        gbm = gbm_models[str(i)]
        feature_imp = pd.DataFrame({'Value': gbm.feature_importances_, 'Feature': feature_names})
        curr_fig = plt.figure(figsize=(40,20))
        #sns.set(font_scale=5)
        sns.barplot(x='Value', y='Feature', data=feature_imp.sort_values(by='Value', ascending=False)[0:num])
        tit = prefix+'LightGBM_feature_importance_'+ps_helpers.get_output_name(i)
        plt.tight_layout()
        #lgb.plot_importance(gbm)
        #plt.savefig('output_plots/'+prefix+'LightGBM_feature_importance_output_'+str(i)+'.png')
        
        ps_helpers.save_fig(curr_fig, os.path.join(ps_helpers.get_current_location(), 'output_plots/'+tit))
        plt.clf()

def plot_learning_curves(gbm_models, num_outputs, tuned=False):
    if tuned:
        prefix = 'Tuned'
    else:
        prefix = ''
    # plotting learning curve
    for i in range(num_outputs):
        gbm = gbm_models[str(i)]
        lgb.plot_metric(gbm, metric='l2')
        plt.savefig(os.path.join(ps_helpers.get_current_location(), 
                    'output_plots/'+prefix+'LightGBM_learning_curve_output_'+str(i)+'.png'))
        plt.clf()

def plot_trees(gbm_models, num_outputs, tuned=False):
    if tuned:
        prefix = 'Tuned'
    else:
        prefix = ''
    # plotting trees
    for i in range(num_outputs):
        gbm = gbm_models[str(i)]
        lgb.plot_tree(gbm)
        plt.savefig(os.path.join(ps_helpers.get_current_location(), 
                    'output_plots/'+prefix+'LightGBM_tree_output_'+str(i)+'.png'))
        plt.clf()

def CV_search_parameters(X_train, Y_train, X_val, Y_val):
    hyper_params = {
        'learning_rate': reciprocal(1e-4,1e-1), 
        'lambda_l2': reciprocal(1e-5, 1.0),
        'feature_fraction': [0.6, 0.7, 0.8, 0.9, 1.0], 
        'bagging_fraction': [0.6, 0.7, 0.8, 0.9, 1.0], 
        'bagging_freq': [5, 10, 15, 20], 
        'max_depth': [3,4,5], 
        'num_leaves': [7,15,31],
        'max_bin': [100, 255, 512],
        'n_estimators': [100, 200, 300, 400, 500],
    }

    gbms = {} 
    best_params = {}
    for i in range(Y_train.shape[1]):
        print ('Output '+str(i))
        gbm = lgb.LGBMRegressor(task='train', boosting_type='gbdt', objective='regression', metric=['l2', 'l1'], verbose=1)

        print("")
        rnd_search_cv = RandomizedSearchCV(gbm, hyper_params,
            n_iter=10, cv=3, random_state=10)
        rnd_search_cv.fit(X_train, Y_train[:, i], eval_set=[(X_val, Y_val[:,i]), (X_train, Y_train[:,i])], 
            eval_metric=['l2', 'l1'], early_stopping_rounds=100)

        #gbm.fit(X_train, Y_train[:, i], eval_set=[(X_val, Y_val[:,i]), (X_train, Y_train[:,i])], 
        #    eval_metric='l2', early_stopping_rounds=100)
        gbms[str(i)] = rnd_search_cv.best_estimator_
        best_params[str(i)] = rnd_search_cv.best_params_


    return gbms, best_params


def compute_metric(y_true, X_dat, gbms, data_type="train", metric_type="mse", metric_func=mean_squared_error, no_color=False):
    y_pred_all = np.empty(y_true.shape)
    for i in range(y_true.shape[1]):
        print(bcolors.OKGREEN+'Printing results for output ' + ps_helpers.get_output_name(i) + bcolors.ENDC)
        gbm = gbms[str(i)]
        if isinstance(gbm, lgb.Booster):
            y_pred = gbm.eval(lgb.Dataset(X_dat), 'test')
        else:
            y_pred = gbm.predict(X_dat, num_iteration=gbm.best_iteration_)
        y_pred_all[:, i] = y_pred
    
        to_print = ""
        if not no_color:
            to_print = bcolors.OKBLUE

        to_print += 'The ' + metric_type + ' for ' + data_type + ' prediction is: ' 
        to_print += str(round(metric_func(y_true[:,i], y_pred_all[:,i]),5))

        if not no_color:
            to_print += bcolors.ENDC

        print(to_print)
    return y_pred_all
