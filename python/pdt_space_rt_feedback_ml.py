"""
@author: Abed Yassine
@date: September 2nd, 2020

This module contains the main function to train a model to recover mu_a and mu_s


Needs python 3.5 or higher
"""

from mycolors import bcolors
import training_data as td
import common as ps_helpers
import keras_model
import lightgbm_model
import lin_reg_model
import analytical_model
import svm_model

import tensorflow.keras as keras 
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
from sklearn.metrics import mean_squared_error
from sklearn.metrics import r2_score
from sklearn.metrics import mean_absolute_error
import pickle as pk

import math
import numpy as np
import pdb
from os import sys
import logging
import csv
from os import path

from optparse import OptionParser

def rtfeedback_ML(filename, with_pca=True, normalize=True, model_type='ann', cut_mu_eff=False, 
            cut_detected_weights=False, cut_dot_products=False, cut_cross_products=False, sweep=False, 
            train_for_mu_eff = False): 
    """ 
    This method is the main function to build and train a model

    @param filename: input file that contains the training data
    @with_pca: a boolean that specifies if we should reduce the number of features first 
    @normalize: a boolean that specifies if we should normalize the input features to be with mean 0 and std_dev of 1 
        (helps in speeding up convergence)
    @model_type: a string that specifies the type of a model to build. Options: 'ann', 'boosting', 'svm', 'lin_reg'
    @sweep: a boolean that indicates whether to sweep hyperparameters or not
    @cut_<feature_name>: a boolean that indicates whether to remove a certain feature from the training data or not 
    """
    normalize_labels = False
    if model_type == 'ann':
        normalize_labels = True; 
    # Read inputs 
    shuffle = True
    if model_type == 'analytical':
        shuffle = False
    X_train, y_train, X_val, y_val, X_test, y_test, num_test_samples, max_num_features, min_num_features, feature_names, max_mus = \
                td.read_data(filename, normalize_labels=normalize_labels, cut_mu_eff = cut_mu_eff, 
                             cut_detected_weights = cut_detected_weights, \
                             cut_dot_products = cut_dot_products, cut_cross_products = cut_cross_products,  \
                             train_for_mu_eff = train_for_mu_eff, shuffle = shuffle)
    X_train_used = np.copy(X_train)
    X_val_used = np.copy(X_val)
    X_test_used = np.copy(X_test)
   
    # Reduce features
    if with_pca:
        X_train_used, pca, feature_names = ps_helpers.decompose_inputs(X_train_used, min_num_features, feature_names)
        X_val_used = pca.transform(X_val_used)
        X_test_used = pca.transform(X_test_used)

    # Normalize data
    if normalize:
        X_train_used, normalizer = ps_helpers.normalize_data(X_train_used)
        X_val_used = ps_helpers.normalize_data_only(X_val_used, normalizer)
        X_test_used = ps_helpers.normalize_data_only(X_test_used, normalizer)

    #ps_helpers.plot_multi_imshow(X_train_used, 5, 5, 'Training_data_non_pca')
    if model_type.lower() == 'ann':
        print (bcolors.HEADER+"Training using Keras API for neural networks"+bcolors.ENDC)

        keras_m = keras_model.build_model(num_features=X_train_used.shape[1])
        
        if sweep:
            # Hyper parameter tuning
            optimized_model, best_params = keras_model.CV_search_parameters(X_train_used, y_train, X_val_used, y_val)

            # save the model
            #optimized_model.save('mu_a_mu_s_model_tuned.h5')
       
            history = optimized_model.fit(X_train_used, y_train, epochs=30, validation_data=(X_val_used, y_val))
        
            optimized_model.evaluate(X_test_used, y_test)

            # Plot validation and train accuracy
            # ps_helpers.plot_model_loss(history.history, 'Tuned Neural network train and validation loss')
            print ('Model output: ' + str(history.history))
            print ('Best params: ' + str(best_params))
        else:
            # Train the model 
            #pdb.set_trace() 
            history = keras_m.fit(X_train_used, y_train, batch_size = 64, epochs=600, validation_data=(X_val_used, y_val))
            
            # Save the model 
            dic = {
                'max_mus': max_mus,
                'history': history.history,
                'keras_m_weights': keras_m.get_weights()
            }
            pk.dump(dic, open(path.join(ps_helpers.get_current_location(), 'KERAS_model.txt'),'wb'))

            keras_m.evaluate(X_test_used, y_test)

            # Plot validation and train accuracy
            ps_helpers.plot_model_loss(history.history, 'Neural network train and validation loss')

            y_pred = keras_m.predict(X_val_used)

            for i in range(y_pred.shape[1]):
                y_pred[:,i] = max_mus[i]*y_pred[:,i]
                y_val[:,i] = max_mus[i]*y_val[:,i]
                ps_helpers.compute_metric(y_val[:,i], y_pred[:,i], 'mse', ps_helpers.get_output_name(i))
                ps_helpers.compute_metric(y_val[:,i], y_pred[:,i], 'mae', ps_helpers.get_output_name(i))
                ps_helpers.compute_metric(y_val[:,i], y_pred[:,i], 'mape', ps_helpers.get_output_name(i))
                ps_helpers.compute_metric(y_val[:,i], y_pred[:,i], 'r2', ps_helpers.get_output_name(i))

            ps_helpers.plot_y_vs_fit(y_val, y_pred, model_type="ann")
            #ps_helpers.plot_residual_plots(X_val_used, y_val, y_pred, feature_names, model_type="ann")
    
    elif model_type.lower() == 'boosting':
        print (bcolors.HEADER+"Training using lightgbm framework for random forest learning"+bcolors.ENDC)

        if sweep:
            # Hyper parameter tuning
            optimized_models, best_params = lightgbm_model.CV_search_parameters(X_train_used, y_train, X_val_used, y_val)

            for i in range(y_train.shape[1]):
                gbm = optimized_models[str(i)]

                print("")
                gbm.fit(X_train_used, y_train[:, i], eval_set=[(X_val_used, y_val[:,i]), (X_train_used, y_train[:,i])], 
                    eval_metric='l2', early_stopping_rounds=100)

                # save the model
                #gbm.booster_.save_model('LGBM_tuned_model_output_'+str(i)+'.txt')


            lightgbm_model.compute_metric(y_test, X_test_used, optimized_models, 'test', 'mse', mean_squared_error, no_color=True)
            lightgbm_model.compute_metric(y_test, X_test_used, optimized_models, 'test', 'mae', mean_absolute_error, no_color=True)
            lightgbm_model.compute_metric(y_test, X_test_used, optimized_models, 'test', 'r_square', r2_score, no_color=True)
            lightgbm_model.compute_metric(y_test, X_test_used, optimized_models, 'test', 'mape', 
                    ps_helpers.mean_absolute_percentage_error, no_color=True)
            lightgbm_model.compute_metric(y_val, X_val_used, optimized_models, 'validation', 'mse', mean_squared_error, no_color=True)
            lightgbm_model.compute_metric(y_val, X_val_used, optimized_models, 'validation', 'mae', mean_absolute_error, no_color=True)
            lightgbm_model.compute_metric(y_val, X_val_used, optimized_models, 'validation', 'r_square', r2_score, no_color=True)
            lightgbm_model.compute_metric(y_val, X_val_used, optimized_models, 'validation', 'mape', 
                    ps_helpers.mean_absolute_percentage_error, no_color=True)
            lightgbm_model.compute_metric(y_train, X_train_used, optimized_models, 'train', 'mse', mean_squared_error, no_color=True)
            lightgbm_model.compute_metric(y_train, X_train_used, optimized_models, 'train', 'mae', mean_absolute_error, no_color=True)
            lightgbm_model.compute_metric(y_train, X_train_used, optimized_models, 'train', 'r_square', r2_score, no_color=True)
            lightgbm_model.compute_metric(y_train, X_train_used, optimized_models, 'train', 'mape', 
                    ps_helpers.mean_absolute_percentage_error, no_color=True)

            history = optimized_models
            print ('')
            print('Best params: ' + str(best_params))

        else:
            history = lightgbm_model.train(X_train_used, y_train, X_val_used, y_val, X_test_used, y_test, feature_names) 
            y_pred_all = np.empty(y_val.shape)
            for i in range(y_val.shape[1]):
                gbm = history[str(i)]
                y_pred = gbm.predict(X_val_used, num_iteration=gbm.best_iteration_)
                y_pred_all[:, i] = y_pred
            ps_helpers.plot_y_vs_fit(y_val, y_pred_all, model_type="boosting")
            #ps_helpers.plot_residual_plots(X_val_used, y_val, y_pred_all, feature_names, model_type="boosting")
    
        print ('Model: ' + str(history))

    elif model_type.lower() == 'svm':
        print (bcolors.HEADER+"Training using an svm model"+bcolors.ENDC)
        if sweep:
            # Hyper parameter tuning
            history, best_params = svm_model.CV_search_parameters(X_train_used, y_train, X_val_used, y_val, X_test_used, y_test)

            print(best_params)
        else:
            history = svm_model.train(X_train_used, y_train, X_val_used, y_val, X_test_used, y_test)
        print (history)
    elif model_type.lower() == 'lin_reg':
        print (bcolors.HEADER+"Training using an linear regression model"+bcolors.ENDC)

        history = lin_reg_model.train(X_train_used, y_train, X_val_used, y_val, X_test_used, y_test, False)
        print (history)
    elif model_type.lower() == 'analytical':
        print (bcolors.HEADER+'Recovering mu_a and mu_s using analytical solutions'+bcolors.ENDC)

        history = analytical_model.find_mu_a_mu_s_analytical(X_train_used, y_train, X_val_used, y_val, X_test_used, y_test,
                    fix_value='mean')
    else:
        print (bcolors.FAIL+"Unsupported model type:" +model_type+bcolors.ENDC)
        sys.exit(-1)

    #plt.show()
    return history

def rtfeedback_inference(filename='', model_type='boosting', pca=False, normalize=False,
            cut_mu_eff = False, cut_detected_weights=False, cut_dot_products=False, cut_cross_products=False):
    """ 
    This method only loads a saved model and do inference on the data read from filename
    """
    # Read inputs 
    normalize_labels = False
    if model_type == 'ann':
        normalize_labels = True; 
    X_test, y_test, max_mus_curr = td.read_test_data(filename, cut_mu_eff = cut_mu_eff, cut_detected_weights = cut_detected_weights, \
                             cut_dot_products = cut_dot_products, cut_cross_products = cut_cross_products, \
                             normalize_labels=normalize_labels)
    X_test_used = np.copy(X_test)
   
    # Reduce features
    if pca:
        X_test_used = ps_helpers.pca_load_and_transform(X_test_used)

    # Normalize data
    if normalize:
        X_test_used = ps_helpers.normalizer_load_and_transform(X_test_used)

    if model_type == 'ann':
        print(bcolors.HEADER+'Inference using Keras neural network'+bcolors.ENDC)
        keras_m = keras_model.build_model(num_features=X_test_used.shape[1])
        
        dic = pk.load(open(path.join(ps_helpers.get_current_location(), 'KERAS_model.txt'), 'rb'))
        keras_m.set_weights(dic['keras_m_weights'])
        max_mus = dic['max_mus']

        y_pred = keras_m.predict(X_test_used)

        for i in range(y_pred.shape[1]):
            y_pred[:,i] = max_mus[i]*y_pred[:,i]
            y_test[:,i] = max_mus_curr[i]*y_test[:,i]
            ps_helpers.compute_metric(y_test[:,i], y_pred[:,i], 'mse', ps_helpers.get_output_name(i))
            ps_helpers.compute_metric(y_test[:,i], y_pred[:,i], 'mae', ps_helpers.get_output_name(i))
            ps_helpers.compute_metric(y_test[:,i], y_pred[:,i], 'mape', ps_helpers.get_output_name(i))
            ps_helpers.compute_metric(y_test[:,i], y_pred[:,i], 'r2', ps_helpers.get_output_name(i))
        
        #ps_helpers.plot_y_vs_fit(y_test, y_pred, model_type="ann")
        
        return y_pred

    elif model_type == 'boosting':
        print(bcolors.HEADER+'Inference using LightGBM gradient boosting'+bcolors.ENDC)
        y_pred = lightgbm_model.evaluate_test_data(X_test_used, y_test)
        return y_pred
    elif model_type == 'analytical':
        print(bcolors.HEADER+'Inference using analytical model'+bcolors.ENDC)
        y_pred = rtfeedback_ML(filename, with_pca=pca, normalize=normalize, model_type='analytical', 
            sweep=False, cut_mu_eff = cut_mu_eff, cut_detected_weights=cut_detected_weights,
            cut_dot_products=cut_dot_products, cut_cross_products=cut_cross_products)
        return y_pred['Fixing mu_a']
    else:
        print(bcolors.FAIL+'Unsupported model type for inference'+bcolors.ENDC)
        sys.exit(-1)

def main():
    parser = OptionParser(usage="usage: %prog [options]")
    parser.add_option("-m", "--model_type", action="store", dest="model_type", type="string",
                        default="ann", help="Model type: options are ['ann', 'boosting', and 'analytical']")
    parser.add_option("-t", "--train", action="store_true", dest="train", default=False, 
        help="If defined, then it will run the training algorithm, otherwise the program will perform inference")
    parser.add_option("-d", "--test_data", action="store", dest="filename", 
        help="A csv file path and name that contains the training/test data. This is a required parameter", type="string",
        default="")
    parser.add_option("-o", "--output_file", action="store", dest="output_file", 
        help="A csv file path and name to output the recovered optical properties in case of using inference", 
        default="", type="string")
    parser.add_option("-s", "--sweep", action="store_true", default=False, dest="sweep", 
        help="If passed, then the program will run the training algorithm while sweeping across different hyperparameters and features")
    parser.add_option("-l", "--log_directory", action="store", default="", dest="log_directory",
        help="If sweep = True, please specify the directory to store the log files in", type="string")

    (options, args) = parser.parse_args()
    if options.filename == "":
        print(bcolors.FAIL+"Please specify the csv file containing the training/test data using -d argument"+bcolors.ENDC)
        return 

    if not options.sweep:
        data_file = options.filename
        if options.train:
            if options.model_type == 'ann':
                rtfeedback_ML(data_file, 
                    with_pca=False, normalize=True, model_type='ann', 
                    sweep=False, cut_mu_eff = False, cut_detected_weights=True, cut_dot_products=True, cut_cross_products=True)
            elif options.model_type == 'boosting':
                rtfeedback_ML(data_file, 
                   with_pca=False, normalize=False, model_type='boosting', 
                   sweep=False, cut_mu_eff = False, cut_detected_weights=False, cut_dot_products=False, cut_cross_products=True)
            else:
                rtfeedback_ML(data_file, 
                  with_pca=False, normalize=False, model_type='analytical', 
                  sweep=False, cut_mu_eff = False, cut_detected_weights=False, cut_dot_products=False, cut_cross_products=False)
        else:
            if options.output_file == "":
                print (bcolors.FAIL + "Please specify the output csv file to write the results to using -o option"+bcolors.ENDC)
                return
            if options.model_type == 'ann':
                y_pred = rtfeedback_inference(data_file, 
                     pca=False, normalize=True, model_type='ann', 
                     cut_mu_eff = False, cut_detected_weights=True, cut_dot_products=True, cut_cross_products=True)
            elif options.model_type == 'boosting':
                y_pred = rtfeedback_inference(data_file, pca=False, normalize=False, 
                    model_type='boosting', 
                    cut_mu_eff = False, cut_detected_weights=False, cut_dot_products=False, cut_cross_products=True)
            else:
                y_pred = rtfeedback_inference(data_file, pca=False, normalize=False, model_type='analytical',
                    cut_mu_eff=False, cut_detected_weights=False, cut_dot_products=False, cut_cross_products=False)
            with open (options.output_file, 'w') as out_write:
                writer = csv.writer(out_write, delimiter=',')
                for i in range(y_pred.shape[0]):
                    writer.writerow(y_pred[i,:])
    else:
        # Sweeping every thing 
        td_filename = options.filename

        model_types = ['boosting', 'ann']
        pca = [False, True]
        normalize = [True, False]
        
        # The following list indicates which feature to remove: 
        # m = mu_eff, d = detected_weights, o = dot products, c = cross products, n = none    
        cut_features = ['m', 'd', 'o', 'c', 'md', 'mo', 'mc', 'do', 'dc' , 'oc', 'mdo', 'mdc', 'doc', 'moc', 'mdoc', 'n']

        log_dir = options.log_directory
        if log_dir == "":
            print(bcolors.FAIL+'Please specify the directory to store the log files in'+bcolors.ENDC)
            return

        os.makedirs(log_dir, exist_ok=True)

        for model_type in model_types:
            for p in pca:
                for n in normalize:
                    for f in cut_features:
                        logfile = log_dir+'/tuning_'
                        if (model_type == 'ann'):
                            logfile += 'keras_'
                        else:
                            logfile += 'lightgbm_'
                        if p:
                            logfile += 'w_pca_'
                        else:
                            logfile += 'no_pca_'
                        if n:
                            logfile += 'w_normalization_'
                        else:
                            logfile += 'no_normalization_'
                        logfile += 'cutting_'+ f + '.log'

                        cm = False
                        cd = False
                        co = False
                        cc = False
                        if 'm' in f:
                            cm = True
                        if 'd' in f:
                            cd = True
                        if 'o' in f:
                            co = True
                        if 'c' in f:
                            cc = True

                        log = open(logfile, "w")
                        sys.stdout = log

                        rtfeedback_ML(filename=td_filename, sweep=True, model_type=model_type, with_pca=p, normalize=n, 
                                        cut_mu_eff=cm, cut_detected_weights=cd, cut_dot_products=co, cut_cross_products=cc)

if __name__ == '__main__':
    main()
