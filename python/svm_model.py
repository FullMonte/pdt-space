"""
@author: Abed Yassine
@date: Sept 7th, 2020

This module contains the SVM training functionalities
"""

from mycolors import bcolors
import training_data as td
import common as ps_helpers 

from sklearn.svm import SVR
from sklearn.multioutput import MultiOutputRegressor
import pandas as pd 

from scipy.stats import reciprocal 
from sklearn.model_selection import RandomizedSearchCV
from sklearn.model_selection import GridSearchCV

import numpy as np
import pdb
import matplotlib.pyplot as plt


def train(X_train, Y_train, X_val, Y_val, X_test, Y_test):
    svm = MultiOutputRegressor(SVR(kernel = 'rbf', C = 0.1, gamma = 'auto')) # These parameters are found with cv search

    svm.fit(X_train, Y_train)

    history = {}
    #history['coeffs'] = svm.coef_
    #history['intercept'] = svm.intercept_
    history['train_score'] = svm.score(X_train, Y_train)
    history['val_score'] = svm.score(X_val, Y_val)
    history['test_score'] = svm.score(X_test, Y_test)

    return history

def CV_search_parameters(X_train, Y_train, X_val, Y_val, X_test, Y_test):
    hyper_params = {
        'estimator__kernel': ['linear', 'poly', 'rbf'],
        'estimator__degree': [2, 3, 4, 5, 6],
        'estimator__gamma': ['scale', 'auto'],
        'estimator__coef0': [0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0],
        'estimator__C': [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0]
    }

    svm = SVR()
    svm_multi = MultiOutputRegressor(svm)
    rnd_search_cv = GridSearchCV(svm_multi, hyper_params, verbose=5)
    rnd_search_cv.fit(X_train, Y_train)

    best_estimator = rnd_search_cv.best_estimator_

    history = {}
    history['train_score'] = best_estimator.score(X_train, Y_train)
    history['val_score'] = best_estimator.score(X_val, Y_val)
    history['test_score'] = best_estimator.score(X_test, Y_test)

    return history, rnd_search_cv.best_params_
