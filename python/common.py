"""
@author: Abed Yassine
@date: September 2nd, 2020

This module contains functionalities to do PCA and normalize the features (0 mean and 1 std dev)

it also contains functionalities to plot the data using imshow 
"""

from mycolors import bcolors
import training_data as td

from sklearn.decomposition import PCA
from sklearn.metrics import mean_squared_error
from sklearn.metrics import mean_absolute_error
from sklearn.metrics import r2_score
import tensorflow.keras.layers as layers
import pandas as pd
import pickle as pk

import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages

import math
import numpy as np
import pdb
import os

def get_current_location():
    return os.path.dirname(os.path.abspath(__file__))

def get_output_name(i=0):
    if i == 0:
        return '\u03BC\u2090'
    else:
        return '\u03BC\u209B'

def decompose_inputs(inputs, min_num_features, feature_names):
    """ 
    This methods performs a PCA on the inputs to reduce number of features
    """
    NUM_COMPONENTS = min_num_features # This is the minimum number of features for a tumor with 3 sources
    
    # Create a PCA instance (to be returned)
    pca = PCA(n_components=NUM_COMPONENTS)

    X_train_pca = pca.fit_transform(inputs)

    old_feature_names = feature_names
    feature_names = np.empty(NUM_COMPONENTS, dtype=object)
    for i in range(NUM_COMPONENTS):
        feature_names[i] = 'PCA-'+str(i)
    print(pd.DataFrame(pca.components_, columns=old_feature_names, index=feature_names))

    pk.dump(pca, open(os.path.join(get_current_location(), 'pca_model.pkl'), 'wb'))
    
    return X_train_pca, pca, feature_names

def pca_load_and_transform(inputs):
    """
    This method loads the pca model and transform the inputs
    """
    pca = pk.load(open(os.path.join(get_current_location(), 'pca_model.pkl'), 'rb'))

    return pca.transform(inputs)

def normalize_data(inputs):
    # Adapt a normalizer and normalize the data
    normalizer = layers.experimental.preprocessing.Normalization(axis=-1)
    normalizer.adapt(inputs)

    temp_n = normalize_data_only(inputs, normalizer)

    pk.dump(normalizer, open(os.path.join(get_current_location(), 'normalizer_model.pkl'), 'wb'))

    return temp_n, normalizer

def normalize_data_only(inputs, normalizer):
    # Normalize the data with the given normalizer
    outputs = normalizer(inputs)

    # convert nan values in normalized data to zero
    temp_n = np.nan_to_num(outputs)
   
    return temp_n

def normalizer_load_and_transform(inputs):
    """
    This method loads the normalizer model and transform the inputs
    """
    normalizer = pk.load(open(os.path.join(get_current_location(), 'normalizer_model.pkl'), 'rb'))

    # Normalize the data with the given normalizer
    outputs = normalizer(inputs)

    # convert nan values in normalized data to zero
    temp_n = np.nan_to_num(outputs)
   
    return temp_n

def plot_multi_imshow(arr, num_rows, num_cols, title):
    """ 
    This method plots the heatmap of multiple rows of np array arr in a num_rows*num_cols grid
    The rows are chosen randomly

    @arr [in]: Input array 
    @num_rows [in]: Number of rows to divide the array to 
    @num_cols [in]: Number of columns to divide the array to 
    @title [in]: Title of the figure to be saved
    """
    fig, ax1 = plt.subplots(num_rows, num_cols)
    fig.subplots_adjust(right = 0.8, top=0.3)
    cbar_ax = fig.add_axes([0.95, 0.15, 0.02, 0.7])
    #plt.set_cmap('plasma')
    num_pixels = int(np.ceil(np.sqrt(arr.shape[1])))
    for i in range(num_rows):
        for j in range(num_cols):
            row_idx = np.random.random_integers(0, 375) #arr.shape[0] - 1)
            row_temp = arr[row_idx]
            row_temp = np.resize(row_temp, (num_pixels,num_pixels))
            im = ax1[i,j].imshow(row_temp, vmin=0, vmax=1) 
            ax1[i,j].set_title(str(row_idx))
            ax1[i,j].axis('off')
    fig.colorbar(im, cax=cbar_ax)
    fig.suptitle(title)
    plt.tight_layout()

    save_fig(fig, title)
    plt.clf()

def save_fig(curr_fig, curr_title, pdf=False, svg=False):
    """
    Saves curr_fig with to curr_title.pdf
    """
    if svg:
        curr_fig.savefig(os.path.join(get_current_location(), curr_title+'.svg'), format='svg')
    if pdf:
        pp = PdfPages(os.path.join(get_current_location(), curr_title+'.pdf'))
        pp.savefig(curr_fig)
        pp.close()
    plt.savefig(os.path.join(get_current_location(), curr_title+'.png'))

def plot_model_loss(history, title):
    dir_outputs = os.path.join(get_current_location(), 'output_plots/')
    os.makedirs(dir_outputs, exist_ok=True)
    fig = plt.figure()
    pd.DataFrame(history).plot(figsize=(8,5))
    plt.grid(True)
    plt.gca().set_ylim(0, 2.1)

    save_fig(fig, dir_outputs+title, pdf=False, svg=False)
    plt.clf()

def plot_scatter(X, Y, feature_names):
    """
    This method is to plot a scattering plot of the features against the outputs
    @param X: data to plot features from
    @param Y: output
    """
    dir_outputs = {
        0: os.path.join(get_current_location(), 'feature_scattering_plots/output_0'),
        1: os.path.join(get_current_location(), 'feature_scattering_plots/output_1')
    }
    os.makedirs(dir_outputs[0], exist_ok=True)
    os.makedirs(dir_outputs[1], exist_ok=True)
    for i in range(Y.shape[1]):
        for f in range(0,X.shape[1]):
            curr_fig = plt.scatter(X[:,f], Y[:,i], marker='*')
            tit = 'Feature_' + feature_names[f] + '_' + get_output_name(i)
            plt.title(tit)
            save_fig(curr_fig, dir_outputs[i]+'/'+tit, pdf=False, svg=False)
            plt.clf()

def mean_absolute_percentage_error(y_true, y_pred):
    # calculates the mean absolute percentage error 
    y_ture, y_pred = np.array(y_true), np.array(y_pred)

    return np.mean(np.abs((y_true - y_pred)/y_true))*100

def plot_y_vs_fit(y_true, y_pred, model_type="ann"): 
    """
    This method plots the predicted y values vs the actual values to study the significance of the r2_score
    """
    dir_outputs = {
        0: os.path.join(get_current_location(), 'output_plots/output_0'),
        1: os.path.join(get_current_location(), 'output_plots/output_1')
    }
    os.makedirs(dir_outputs[0], exist_ok=True)
    os.makedirs(dir_outputs[1], exist_ok=True)
    for i in range(y_true.shape[1]):
        curr_fig = plt.scatter(y_true[:,i], y_pred[:,i], marker='*')
        plt.plot(y_true[:,i], y_true[:, i],color='red')
        tit = model_type+'_true_vs_fit_' + get_output_name(i)
        plt.title(tit)
        save_fig(curr_fig, dir_outputs[i]+'/'+tit, pdf=False, svg=False)
        plt.clf()

def plot_residual_plots(X, y_true, y_pred, feature_names, model_type="ann"): 
    """
    This method plots the residual against each feature to study the significance of r2_score
    """
    dir_outputs = {
        0: os.path.join(get_current_location(), 'residual_plots/output_0/'+model_type),
        1: os.path.join(get_current_location(), 'residual_plots/output_1/'+model_type)
    }
    os.makedirs(dir_outputs[0], exist_ok=True)
    os.makedirs(dir_outputs[1], exist_ok=True)
    for i in range(y_true.shape[1]):
        residuals = y_true[:, i] - y_pred[:, i]
        for f in range(X.shape[1]):
            curr_fig = plt.scatter(X[:,f], residuals, marker='*')
            tit = 'Feature_'+feature_names[f]+'_residuals_'+get_output_name(i)
            plt.title(tit)
            save_fig(curr_fig, dir_outputs[i]+'/'+tit, pdf=False, svg=False)
            plt.clf()

def compute_metric(y_true, y_pred, metric='mse', data_name=get_output_name(0)):
    if metric == 'mse':
        err = mean_squared_error(y_true, y_pred)
    elif metric == 'mae':
        err = mean_absolute_error(y_true, y_pred)
    elif metric == 'mape':
        err = mean_absolute_percentage_error(y_true, y_pred)
    elif metric == 'r2':
        if (y_true.shape[0] == 1):
            err = 0
        else:
            err = r2_score(y_true, y_pred)
    else:
        print(bcolors.FAIL+'compute_metric::unsupported metric'+bcolors.ENDC)
    
    print(bcolors.OKBLUE+'The ' + metric + ' for prediction of ' + data_name + ' is: ' + str(round(err,5)) + bcolors.ENDC)

    return err
