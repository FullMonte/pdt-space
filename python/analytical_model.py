"""
@author: Abed Yassine
@date: Sept 23rd, 2020
"""

from mycolors import bcolors
import training_data as td
import common as ps_helpers 

from sklearn.metrics import mean_squared_error
from sklearn.metrics import mean_absolute_error
from sklearn.metrics import r2_score
import pandas as pd 

import numpy as np
import math
import pdb
import matplotlib.pyplot as plt
import sys


def find_mu_a_mu_s_analytical(X_train, Y_train, X_val, Y_val, X_test, Y_test, fix_value='mean'):
    """ 
    This module analytically sets one of mu_a or mu_s to their mean value and solves for the other from the mu_eff formula
    """

    mean_mu_s = 10.0
    mean_mu_a = 0.18
    g = 0.8

    # get all data together. From X, get only mu_eff, which is the average of non-zero mu_effs 
    X = np.zeros(X_train.shape[0] + X_val.shape[0] + X_test.shape[0])

    max_srcs = 21
    t = X_train.shape[0]
    f = X_train.shape[1]
    #pdb.set_trace()
    for i in range(t):
        mu_effs = X_train[i, f - max_srcs:]
        X[i] = mu_effs[np.nonzero(mu_effs)].mean()
    t = X_val.shape[0]
    #pdb.set_trace()
    for i in range(t):
        mu_effs = X_val[i, f - max_srcs:]
        X[X_train.shape[0] + i] = mu_effs[np.nonzero(mu_effs)].mean()
    t = X_test.shape[0]
    #pdb.set_trace()
    for i in range(t):
        mu_effs = X_test[i, f - max_srcs:]
        X[X_train.shape[0] + X_val.shape[0] + i] = mu_effs[np.nonzero(mu_effs)].mean()

    Y_true = np.append(Y_train, Y_val, axis=0)
    Y_true = np.append(Y_true, Y_test, axis=0)


    # Fix mu_a
    Y_mu_a = np.zeros(Y_true.shape)
    mu_a_random = np.random.normal(mean_mu_a, 0.1*mean_mu_a, Y_true.shape[0])
    for i in range(Y_mu_a.shape[0]):
        if fix_value == 'mean':
            mu_a = mean_mu_a
        elif fix_value == 'normal':
            mu_a = mu_a_random[i]
        else:
            print (bcolors.FAIL+'analytical solution::unknown value for fix_value'+bcolors.ENDC)
            sys.exit(-1)
        Y_mu_a[i, 0] = mu_a
        Y_mu_a[i, 1] = (X[i]*X[i] - 3*mu_a*mu_a)/(3*mu_a*(1-g))

    # Fix mu_s
    Y_mu_s = np.zeros(Y_true.shape)
    mu_s_random = np.random.normal(mean_mu_s, 0.1*mean_mu_s, Y_true.shape[0])
    for i in range(Y_mu_s.shape[0]):
        if fix_value == 'mean':
            mu_s = mean_mu_s
            mu_s_p = mu_s*(1-g)
        elif fix_value == 'normal':
            mu_s = mu_s_random[i]
            mu_s_p = mu_s*(1-g)
        else:
            print (bcolors.FAIL+'analytical solution::unknown value for fix_value'+bcolors.ENDC)
            sys.exit(-1)
        Y_mu_s[i, 1] = mu_s

        sq = math.sqrt(mu_s_p*mu_s_p + (4/3.0)*X[i]*X[i])
        root_1 = (-1*mu_s_p - sq)/2.0
        root_2 = (-1*mu_s_p + sq)/2.0

        if root_1 > 0 and root_2 > 0:
            print("Both roots are positive")
            Y_mu_s[i, 0] = math.min(math.abs(root_1 - mean_mu_a), math.abs(root_2 - mean_mu_a))
        elif root_2 > 0:
            Y_mu_s[i, 0] = root_2
        elif root_1 > 0:
            Y_mu_s[i, 0] == root_1
        else:
            print(bcolors.FAIL+"Both roots are negative"+bcolors.ENDC)
            print(str(i) + ' ' + str(X[i]) + ' '  + str(root_1) + ' ' + str(root_2))
            sys.exit(-1)

    
    print(bcolors.OKGREEN+"Analytical solution with Fixing mu_a"+bcolors.ENDC)
    print(bcolors.OKBLUE+'The mse for mu_a predictions is: ' + str(round(mean_squared_error(Y_true[:,0], Y_mu_a[:,0]), 5)) + bcolors.ENDC)
    print(bcolors.OKBLUE+'The mae for mu_a predictions is: ' + str(round(mean_absolute_error(Y_true[:,0], Y_mu_a[:,0]), 5)) + bcolors.ENDC)
    print(bcolors.OKBLUE+'The r2 for mu_a predictions is: ' + str(round(r2_score(Y_true[:,0], Y_mu_a[:,0]), 5)) + bcolors.ENDC)
    print(bcolors.OKBLUE+'The mape for mu_a predictions is: ' + str(round(ps_helpers.mean_absolute_percentage_error(Y_true[:,0], Y_mu_a[:,0]), 5)) + bcolors.ENDC)
    print(bcolors.OKBLUE+'The mse for mu_s predictions is: ' + str(round(mean_squared_error(Y_true[:,1], Y_mu_a[:,1]), 5)) + bcolors.ENDC)
    print(bcolors.OKBLUE+'The mae for mu_s predictions is: ' + str(round(mean_absolute_error(Y_true[:,1], Y_mu_a[:,1]), 5)) + bcolors.ENDC)
    print(bcolors.OKBLUE+'The r2 for mu_s predictions is: ' + str(round(r2_score(Y_true[:,1], Y_mu_a[:,1]), 5)) + bcolors.ENDC)
    print(bcolors.OKBLUE+'The mape for mu_s predictions is: ' + str(round(ps_helpers.mean_absolute_percentage_error(Y_true[:,1], Y_mu_a[:,1]), 5)) + bcolors.ENDC)
    print(bcolors.OKBLUE+'The mse for all predictions is: ' + str(round(mean_squared_error(Y_true, Y_mu_a), 5)) + bcolors.ENDC)
    print(bcolors.OKBLUE+'The mae for all predictions is: ' + str(round(mean_absolute_error(Y_true, Y_mu_a), 5)) + bcolors.ENDC)
    print(bcolors.OKBLUE+'The r2 for all predictions is: ' + str(round(r2_score(Y_true, Y_mu_a), 5)) + bcolors.ENDC)
    print(bcolors.OKBLUE+'The mape for all predictions is: ' + str(round(ps_helpers.mean_absolute_percentage_error(Y_true, Y_mu_a), 5)) + bcolors.ENDC)
    print(bcolors.OKGREEN+"Analytical solution with Fixing mu_s"+bcolors.ENDC)
    print(bcolors.OKBLUE+'The mse for mu_a predictions is: ' + str(round(mean_squared_error(Y_true[:,0], Y_mu_s[:,0]), 5)) + bcolors.ENDC)
    print(bcolors.OKBLUE+'The mae for mu_a predictions is: ' + str(round(mean_absolute_error(Y_true[:,0], Y_mu_s[:,0]), 5)) + bcolors.ENDC)
    print(bcolors.OKBLUE+'The r2 for mu_a predictions is: ' + str(round(r2_score(Y_true[:,0], Y_mu_s[:,0]), 5)) + bcolors.ENDC)
    print(bcolors.OKBLUE+'The mape for mu_a predictions is: ' + str(round(ps_helpers.mean_absolute_percentage_error(Y_true[:,0], Y_mu_s[:,0]), 5)) + bcolors.ENDC)
    print(bcolors.OKBLUE+'The mse for mu_s predictions is: ' + str(round(mean_squared_error(Y_true[:,1], Y_mu_s[:,1]), 5)) + bcolors.ENDC)
    print(bcolors.OKBLUE+'The mae for mu_s predictions is: ' + str(round(mean_absolute_error(Y_true[:,1], Y_mu_s[:,1]), 5)) + bcolors.ENDC)
    print(bcolors.OKBLUE+'The r2 for mu_s predictions is: ' + str(round(r2_score(Y_true[:,1], Y_mu_s[:,1]), 5)) + bcolors.ENDC)
    print(bcolors.OKBLUE+'The mape for mu_s predictions is: ' + str(round(ps_helpers.mean_absolute_percentage_error(Y_true[:,1], Y_mu_s[:,1]), 5)) + bcolors.ENDC)
    print(bcolors.OKBLUE+'The mse for all predictions is: ' + str(round(mean_squared_error(Y_true, Y_mu_s), 5)) + bcolors.ENDC)
    print(bcolors.OKBLUE+'The mae for all predictions is: ' + str(round(mean_absolute_error(Y_true, Y_mu_s), 5)) + bcolors.ENDC)
    print(bcolors.OKBLUE+'The r2 for all predictions is: ' + str(round(r2_score(Y_true, Y_mu_s), 5)) + bcolors.ENDC)
    print(bcolors.OKBLUE+'The mape for all predictions is: ' + str(round(ps_helpers.mean_absolute_percentage_error(Y_true, Y_mu_s), 5)) + bcolors.ENDC)
    
    ps_helpers.plot_y_vs_fit(Y_true, Y_mu_a, model_type='analytical_fixed_mu_a_'+fix_value)
    ps_helpers.plot_y_vs_fit(Y_true, Y_mu_s, model_type='analytical_fixed_mu_s_'+fix_value)

    return {'Fixing mu_a': Y_mu_a, 
            'Fixing mu_s': Y_mu_s}
